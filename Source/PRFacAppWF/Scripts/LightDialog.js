﻿jQuery(document).ready(function ($) {

    $('.popup-exit').click(function () {
        clearPopup();

    });

    $('.popup-overlay').click(function () {
        clearPopup();
    });


    $(document).keyup(function (e) {
        if (e.keyCode == 27 && $('html').hasClass('overlay')) {
            clearPopup();
        }
    });
    $.fn.center = function () {
        //alert('wind ht: ' + $(window).height());
        //alert('wind wdth: ' + $(window).width());
        //alert('top: ' + this.height() );
        //alert('left: ' + this.width());

        var top = (parseInt($(window).height()) - ( parseInt(this.height()) - 20)) / 2;
        var left = (parseInt($(window).width()) - parseInt(this.width())) / 2;
        this.css("position", "absolute");
        this.css("top", top+ "px");
        this.css("left", left + "px");
        return this;
    }

    $(".popup").center(); // on page load div will be entered                                               

    $(window).resize(function () { // whatever the screen size this
        $(".popup").center();
    });
});