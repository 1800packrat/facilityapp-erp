﻿function initControlls() {

    $("#FacilityOpHourStartDateIcon").click(function () {
        $("#FacilityOpHourStartDate").focus();
    });

    $('#FacilityOpHourStartDate').datepicker({
        onSelect: function (dateText, inst) {
            $("#FacilityOpHourEndDate").datepicker("option", "minDate",
            $("#FacilityOpHourStartDate").datepicker("getDate").addDays(1));
        }
    });

    $("#FacilityOpHourEndDateIcon").click(function () {
        $("#FacilityOpHourEndDate").focus();
    });

    $('#FacilityOpHourEndDate').datepicker({
        onSelect: function (dateText, inst) {
            $("#FacilityOpHourStartDate").datepicker("option", "maxDate",
            $("#FacilityOpHourEndDate").datepicker("getDate").addDays(-1));
        }
    });

    $('#FacilityOpHourStartDate').datepicker("setDate", new Date().ToDate());
    $('#FacilityOpHourEndDate').datepicker("setDate", new Date().addYears(1).ToDate());

    $("form").on("submit", function (event) {
        event.preventDefault();
    });

    $('#btnShow').click(function () {
        if (validateDates()) {
            addAndGetFacilityOperatingHours();
        }
    });

    $('#btnReset').click(function () {
        $('#FacilityOpHourStartDate').val(new Date().ToDate());
        $('#FacilityOpHourEndDate').val(new Date().addYears(1).ToDate());

        addAndGetFacilityOperatingHours();
    });

}

function validateDates() {
    var rtVal = true;

    if ($('#FacilityOpHourStartDate').val() == '') {
        alert('Please enter valid start date.')
        $('#FacilityOpHourStartDate').focus();
        rtVal = false;
    }
    if ($('#FacilityOpHourEndDate').val() == '') {
        alert('Please enter valid end date.');
        $('#FacilityOpHourEndDate').focus();
        rtVal = false;
    }

    return rtVal;
}

function addAndGetFacilityOperatingHours() {
    $("#list").clearGridData();

    $("#list").jqGrid('setGridParam', {
        mtype: 'GET', editurl: URLHelper.saveFacilityHourChangeUrl, onclickSubmit: batchSumbit, url: URLHelper.addAndGetFacilityOperatingHourUrl, datatype: 'json'
                                , postData: {
                                    rowIds: jQuery("#list").jqGrid('getGridParam', 'selarrrow'),
                                    startDate: function () { return $('#FacilityOpHourStartDate').val(); },
                                    endDate: function () { return $('#FacilityOpHourEndDate').val(); }
                                }
    }).trigger("reloadGrid");
}

function initHrChangeGrid() {

    var checkStartEndTime = function (value, colname) {
        var startTime = $("input#StartTime").val();
        var endTime = $("input#EndTime").val();

        if (startTime != '' && endTime != '') {
            var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
            var endTimeDt = new Date("1/1/1970" + ' ' + endTime);

            if (startTimeDt > endTimeDt) {
                return [false, "Start time should be less than End time.", ""];
            }
        }

        return [true, "", ""];
    };

    var updateDialog = {
        url: URLHelper.saveFacilityHourChangeUrl
                , closeAfterEdit: true
                , reloadAfterSubmit: true
                , closeAfterAdd: true
                , closeAfterEdit: true
                , afterShowForm: function (formid) {
                    GetWorkingHours(formid);

                    $("select#StoreOpen", formid).change(function (o) {
                        $("input#StartTime", formid).val('12:00 AM');
                        $("input#EndTime", formid).val('12:00 AM');
                        GetWorkingHours(formid);
                    });
                }
                , onclickSubmit: function (params) {
                    var ajaxData = {};
                    var list = $("#list");
                    var selectedRow = list.getGridParam("selrow");

                    rowData = list.getRowData(selectedRow);

                    ajaxData = { rowIds: jQuery("#list").jqGrid('getGridParam', 'selarrrow'), CalDate: rowData.CalDate };

                    return ajaxData;
                }
                , afterComplete: function (o) {
                }
                , modal: true
                , width: "400"
    };

    var emptyMsgDiv = $("<div  style='text-align:center; padding: 30px;font-size: 16px;'><span>No records found</span></div>");

    $("#list").jqGrid({
        colNames: ['RowID', 'CalDateIndex', 'Date', 'Store Status', 'Start Time', 'End Time',
              'Operating Hours', 'WeekDay']
                    , colModel: [
                            { name: 'RowID', index: 'RowID', key: true, editable: false, hidedlg: true, hidden: true },
                            { name: 'CalDate', index: 'CalDate', editable: true, editrules: { edithidden: false }, hidedlg: true, hidden: true, search: false },
                            { name: 'CalDateDisp', index: 'CalDateDisp', width: 80, editable: false, hidedlg: true, hidden: false, search: false, align: 'center' },
                            { name: 'StoreOpen', index: 'StoreOpen', width: 40, editable: true, edittype: 'select', formatter: 'select', editrules: { required: true }, formoptions: { elmsuffix: ' *' }, editoptions: { value: { O: 'Open', C: 'Closed' } }, width: "40", search: false, align: 'center' },
                            {
                                name: 'StartTime', index: 'StartTime', width: 100, editable: true, formatter: 'text', edittype: 'text', editrules: { custom: true, custom_func: checkStartEndTime, time: true, required: true },
                                editoptions: {
                                    dataInit: function (element) {
                                        $(element).timepicker({
                                            minTime: '12:00 AM',
                                            maxTime: '11:30 PM',
                                            timeFormat: 'h:i A',
                                            step: 15
                                        }).on('changeTime', function () {
                                            ///Here editmodlist is a id of the popup
                                            GetWorkingHours($('#editmodlist'));
                                        });
                                    }
                                },
                                formoptions: { elmsuffix: ' *' }, search: false, align: 'center'
                            },
                            {
                                name: 'EndTime', index: 'EndTime', width: 100, editable: true, edittype: 'text', editrules: { custom: true, custom_func: checkStartEndTime, time: true, required: true },
                                editoptions: {
                                    dataInit: function (element) {
                                        $(element).timepicker({
                                            minTime: '12:00 AM',
                                            maxTime: '11:30 PM',
                                            timeFormat: 'h:i A',
                                            step: 15
                                        }).on('changeTime', function () {
                                            ///Here editmodlist is a id of the popup
                                            GetWorkingHours($('#editmodlist'));
                                        });
                                    }
                                },
                                formoptions: { elmsuffix: ' *' }, search: false, align: 'center'
                            },
                            { name: 'FacilityOperatingTimeMinutesDisp', index: 'FacilityOperatingTimeMinutesDisp', width: 80, editable: true, edittype: 'custom', editoptions: { custom_element: myelem, custom_value: myvalue }, search: false, align: 'center' },
                            { name: 'WeekDay', index: 'WeekDay', editable: false, hidden: false, stype: 'select', searchoptions: { value: "0:All Days;8:Weekends;15:WeekDays;1:Sunday;2:Monday;3:Tuesday;4:Wednesday;5:Thursday;6:Friday;7:Saturday;9:Holidays", clearSearch: false }, align: 'center' }
                    ]
                , pager: 'listPager'
                , autowidth: true
                , shrinkToFit: true
                , rowNum: 20
                , rowList: [10, 20, 30, 60, 365]
                , sortname: 'CalDate'
                , sortorder: "asc"
                , viewrecords: true
                , caption: 'Facility Operating Hour Change'
                , gridview: true
                , id: "RowID"
                , height: "100%"
                , multiselect: true
                , multiboxonly: true
                , datatype: 'local'
                , loadError: function (xhr, st, err) {
                    if (xhr.status == "200") return false;
                    var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                        modal: false,
                        title: 'Error',
                        maxwidth: '600',
                        width: '600',
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
                , gridComplete: function () {
                    var ids = $("#list").getDataIDs();
                    for (var i = 0; i < ids.length; i++) {
                        var cell = $("#list").getCell(ids[i], "WeekDay");
                        if (cell == "Sunday" || cell == "Saturday") {
                            $(this).jqGrid('setRowData', ids[i], false, { background: '#e0effa' });
                            // $("#" + ids)[i].style.backgroundColor = "yellow"
                        }

                        var dtVal = $("#list").getCell(ids[i], "CalDateDisp");
                        var dt = new Date(dtVal);
                        var currentdate = new Date();
                        currentdate.setHours(0, 0, 0, 0);

                        if (currentdate >= dt) {
                            var rowID = $("#list").getCell(ids[i], "RowID");
                            var o = $("input:checkbox[id=jqg_list_" + rowID + "]");
                            $(o).remove();
                        }
                    }
                }
                , emptyrecords: 'no records found'
                , loadComplete: function () {
                    var ts = this;
                    if (ts.p.reccount === 0) {
                        $(this).hide();
                        emptyMsgDiv.show();
                        $('#modalUpdateselectedhours').hide();
                    } else {
                        $(this).show();
                        emptyMsgDiv.hide();
                        $('#modalUpdateselectedhours').show();
                    }
                }
                , ondblClickRow: function (rowid, iRow, iCol, e) {
                    if (isFullAccess == "True") {
                        var grid = $('#list');
                        var calDate = grid.jqGrid('getCell', rowid, 'CalDateDisp');
                        var dt = new Date(calDate);
                        var currentdate = new Date();
                        if (currentdate < dt)
                            $("#list").editGridRow(rowid, updateDialog);
                    }
                }
    }).navGrid('#listPager',
                   {
                       edit: false, add: false, del: false, search: false, refresh: false
                   },
                   updateDialog,
                   null,
                   null);

    emptyMsgDiv.insertAfter($("#list").parent());

    jQuery("#modalUpdateselectedhours").click(function () {
        var rows = jQuery("#list").jqGrid('getGridParam', 'selarrrow');

        if (rows.length > 0) {
            jQuery("#list").jqGrid('editGridRow', rows, {
                height: 300,
                width: 450,
                closeAfterEdit: true,
                reloadAfterSubmit: true,
                afterShowForm: function (formid) {

                    GetWorkingHours(formid, (rows.length > 1));

                    $("select#StoreOpen", formid).change(function (o) {
                        $("input#StartTime", formid).val('12:00 AM');
                        $("input#EndTime", formid).val('12:00 AM');
                        GetWorkingHours(formid);
                    });
                }
                , onclickSubmit: batchSumbit
                , url: URLHelper.saveFacilityHourChangeUrl
                , afterComplete: function (o) { }
            });
        } else alert("Please Select a Row");
    });

    jQuery("#list").jqGrid('filterToolbar');

     $("#list").setGridWidth(Math.round($(window).width() - 100, true));
}

function GetWorkingHours(formid, isMultipleSelected) {
    //When user select multiple check box from UI and trying to update all at once, by that time we are showing default values as 07AM to 05PM.
    if (isMultipleSelected) {
        $("select#StoreOpen", formid).val('O')
        $("input#StartTime", formid).val('07:00 AM');
        $("input#EndTime", formid).val('05:00 PM');
    }

    var startTime = $("input#StartTime", formid).val();
    var endTime = $("input#EndTime", formid).val();
    var storeOpen = $("select#StoreOpen", formid).val();
    var workingHr = '0 hr 0 min';

    if (storeOpen == "O" && startTime != '' && endTime != '') {
        var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
        var endTimeDt = new Date("1/1/1970" + ' ' + endTime);

        if (startTimeDt < endTimeDt) {
            var differenceDt = new Date(endTimeDt - startTimeDt)

            var hours = differenceDt.getUTCHours();
            var minutes = differenceDt.getUTCMinutes();
            workingHr = hours + ' hr ' + minutes + ' min';
        }
    }

    $("span#FacilityOperatingTimeMinutesDisp", formid).html(workingHr);
}

function myelem(value, options) {
    var el = document.createElement("span");
    el.type = "span";
    el.className = 'oprationHoursDuration'
    el.innerHTML = value;
    return el;
}

function myvalue(elem, operation, value) {
    if (operation === 'get') {
        return $(elem).val();
    } else if (operation === 'set') {
        $('span', elem).val(value);
    }
}

var batchSumbit = function (params) {
    var ajaxData = {};
    var data;
    var rowids = jQuery("#list").jqGrid('getGridParam', 'selarrrow');

    if (rowids != null) {
        return ajaxData = { rowIds: rowids };
    }
}

$(document).ready(function () {
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    initControlls();

    initHrChangeGrid();

    addAndGetFacilityOperatingHours();
});

Date.prototype.ToDate = function () {
    return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
}

Date.prototype.ToTime = function () {
    var hr = parseInt(this.getHours());
    var ampm = hr > 11 ? 'PM' : 'AM';
    var hr = hr > 12 ? (hr - 12) : hr;
    var mm = parseInt(this.getMinutes()) > 9 ? this.getMinutes() : '0' + this.getMinutes();
    return hr + ":" + mm + " " + ampm;
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

Date.prototype.addYears = function (years) {
    var dat = new Date(this.valueOf());
    dat.setYear(dat.getFullYear() + years);
    return dat;
}