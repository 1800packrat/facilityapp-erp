﻿var err = {
    TodateGrater: 'To Date cannot be before From Date',
    RequireTodate: 'Must enter From and To Dates'
};

$("form").on("submit", function (event) {
    event.preventDefault();
    if (ValidateDate($('#FromDate').val(), $('#ToDate').val()) == false)
        return false;

    SearchTruckStatus($(this).serialize());
});

function ValidateDate(fromDate, toDate) {
    if (fromDate.length > 0 && toDate.length > 0 && new Date(fromDate) > new Date(toDate)) {
        $('#errToDate').html(err.TodateGrater);
        $('#errToDate').show();

        return false;
    } else if ((fromDate.length == 0 && toDate.length > 0) || (fromDate.length > 0 && toDate.length == 0)) {
        $('#errToDate').html(err.RequireTodate);
        $('#errToDate').show();
        return false;
    } else
        $('#errToDate').hide();
    return true;
}

function getFormData() {
    var ob = {
        FromDate: $('#FromDate').val(),
        ToDate: $('#ToDate').val(),
        Facility: $('#Facility').val(),
        TruckStatus: $('#TruckStatus').val(),
        TruckWithNoStatus: $('#TruckWithNoStatus').is(':checked')
    };
    return ob;
}

function getPostData() {
    var postData = jQuery("#tblTruckStatusList").jqGrid('getGridParam', 'postData');

    var postDt = {
        filter: getFormData(),
        page: postData.page,
        rows: postData.rows,
        sidx: postData.sidx,
        sord: postData.sord
    }
    return postDt;
}

function SearchTruckStatus(formdata) {
    $('#progress').show();
    jQuery('#tblTruckStatusList').trigger("reloadGrid");
}

function ExportTruckStatusToExcel() {
    var gridParams = getPostData();

    var ob = "FromDate=" + $('#FromDate').val() + "&ToDate=" + $('#ToDate').val() + "&Facility=" + $('#Facility').val() 
        + "&TruckStatus=" + $('#TruckStatus').val() + "&TruckWithNoStatus=" + $('#TruckWithNoStatus').is(':checked') 
        + "&sidx=" + gridParams.sidx + "&sord=" + gridParams.sord;

    window.open("/FleetStatusReport/ExportTruckStatus?" + ob, "_blank");
}

$(document).ready(function () {
    $("#FromDateIcon").click(function () {
        $("#FromDate").focus();
    });

    $("#ToDateIcon").click(function () {
        $("#ToDate").focus();
    });

    $('#FromDate').datepicker({
        maxDate: 0,
        onSelect: function (date) {
            return ValidateDate(date, $('#ToDate').val());
        }
    });

    $('#ToDate').datepicker({
        maxDate: 0,
        onSelect: function (date) {
            return ValidateDate($('#FromDate').val(), date);
        }
    });

    $('#progress').show();

    $('#tblTruckStatusList').jqGrid({
        url: URLHelper.SearchFleetStatusUrl,
        datatype: 'json',
        mtype: 'POST', //
        colNames: ['Facility', 'Entry Date', 'Entered By', 'FleetID', 'Status', 'Comments'],
        colModel: [
            { name: 'Facility', index: 'Facility', width: 90, sortable: true },
            { name: 'EntryDateDisp', index: 'EntryDateDisp', align: "center", width: 60, sortable: true },
            { name: 'EnterdBy', index: 'EnterdBy', width: 70, sortable: true },
            { name: 'FleetID', index: 'FleetID', width: 90, sortable: true },
            { name: 'TruckStatus', index: 'TruckStatus', width: 120, sortable: true },
            { name: 'Comments', index: 'Comments', sortable: true }
        ],
        //define how pages are displayed and paged
        pager: $("#ptblTruckStatusList"),
        page: 1, // In case this is a select column rebind
        sortname: "Facility", // Initially sorted on
        viewrecords: true,
        sortorder: "asc",
        //onSortCol: function (index, columnIndex, sortOrder) {
        //},
        //grouping: true,
        //width: jQuery("#SQLInstantResults").width(),
        autowidth: true,
        hidegrid: false,
        forceFit: true, /* fit all columns with in the specified grid area */
        height: 'auto',
        scrollOffset: 0, /* remove scrollbar */
        beforeRequest: function () {
            jQuery('#tblTruckStatusList').setGridParam({ postData: { filter: JSON.stringify(getFormData()) } });
        },
        loadComplete: function () {
            $('#progress').hide();
        },
        jsonReader: {
            root: 'rows',
            id: 'Facility',
            repeatitems: false
        },
        loadError: function () {
            $('#progress').hide();
            alert('Error occurred while loading data, Please reload page and try again!');
        },
        //onSelectRow: function(){},
        //onSelectAll: function(){},
        altRows: true,
        altclass: "GridAltClass",
        //toppager: true,
        rowNum: 25,
        rowList: [25, 50, 75, 100],
        caption: "Fleet Status Results <div class='cust-action'><a href='#' onclick='ExportTruckStatusToExcel(); return false;'>Export to Excel&nbsp;<img src='../Images/go-arrow-icon.png' width='10' height='10' alt=''/></a></div>"
    }).jqGrid('navGrid', '#ptblTruckStatusList', { cloneToTop: true, edit: false, add: false, del: false, search: false, refresh: false });

});