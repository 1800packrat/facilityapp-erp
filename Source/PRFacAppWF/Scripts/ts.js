/// <reference path="Libraries\jquery.d.ts" />;
var facilityAppLogin;
(function (facilityAppLogin) {
    var URLHelper = (function () {
        function URLHelper() {
        }
        URLHelper.ValidateLogin = '';
        URLHelper.Dashboard = '../../Dashboard.aspx';
        return URLHelper;
    }());
    facilityAppLogin.URLHelper = URLHelper;
    var login = (function () {
        function login() {
            var _this = this;
            this.handleEvt = function (e) {
                var key = e.which || e.charCode || e.keyCode;
                if (key == 13) {
                    _this.submit();
                }
            };
            this.btnLogin = document.getElementById('btnLogin');
            this.btnLogin.onclick = function (e) { _this.submit(); };
            document.addEventListener("keydown", this.handleEvt);
        }
        login.prototype.validate = function () {
            ///Implement username and password validateion
            if ($('#txtLoginId').val() == '') {
                $('#lblError').html("Please enter user name.");
                $('#txtLoginId').focus();
                return false;
            }
            if ($('#txtPassword').val() == '') {
                $('#lblError').html("Please enter password.");
                $('#txtPassword').focus();
                return false;
            }
            $('#lblError').html('');
            return true;
        };
        login.prototype.submit = function () {
            if (this.validate()) {
                var request = {
                    LoginName: $('#txtLoginId').val(),
                    Password: $('#txtPassword').val(),
                    IsRememberMe: $('#chkRememberme').prop('checked')
                };
                this.sendRequest(request);
            }
        };
        login.prototype.successCallbackLogin = function (result) {
            if (result.success == true) {
                $.blockUI;
                window.location.href = result.data;
            }
            else {
                //write your logic to show errors
                $('#lblError').html(result.message);
            }
        };
        login.prototype.errorCallbackLogin = function (result) {
            //Show what is the error you got from valid login page
            $('#lblError').html(result.message);
        };
        login.prototype.sendRequest = function (postData) {
            var ajaxSettings = {
                url: facilityAppLogin.URLHelper.ValidateLogin,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: this.successCallbackLogin,
                error: this.errorCallbackLogin
            };
            $.ajax(ajaxSettings);
        };
        return login;
    }());
    facilityAppLogin.login = login;
})(facilityAppLogin || (facilityAppLogin = {}));
$(document).ready(function () {
    var oLogin = new facilityAppLogin.login();
});
/// <reference path="Libraries\jquery.d.ts" />;
var facilityAppForgotPassword;
(function (facilityAppForgotPassword) {
    var URLHelper = (function () {
        function URLHelper() {
        }
        URLHelper.ForgotPassword = '';
        URLHelper.redirectLogin = '/Home/Logout';
        return URLHelper;
    }());
    facilityAppForgotPassword.URLHelper = URLHelper;
    var forgotpassword = (function () {
        function forgotpassword() {
            var _this = this;
            this.btnSendEmail = document.getElementById('btnSendEmail');
            this.btnSendEmail.onclick = function (e) { _this.submit(); };
            this.btnCancel = document.getElementById('btnCancel');
            this.btnCancel.onclick = function (e) { window.location.href = URLHelper.redirectLogin; };
        }
        forgotpassword.prototype.validate = function () {
            //validations here
            if ($('#txtUserName').val() == '') {
                $('#lblError').html("Please enter user name.");
                $('#txtUserName').focus();
                return false;
            }
            $('#lblError').html('');
            return true;
        };
        forgotpassword.prototype.submit = function () {
            if (this.validate()) {
                var request = {
                    UserName: $('#txtUserName').val()
                };
                this.sendRequest(request);
            }
        };
        forgotpassword.prototype.sendRequest = function (postData) {
            var ajaxSettings = {
                url: facilityAppForgotPassword.URLHelper.ForgotPassword,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: this.successCallbackLogin,
                error: this.errorCallbackLogin
            };
            $.ajax(ajaxSettings);
        };
        forgotpassword.prototype.successCallbackLogin = function (result) {
            if (result.success == true) {
                if (result.message === "Auto") {
                    var conf = confirm('New password has been sent to your email! Please click ok to redirect to Login page.');
                    if (conf == true)
                        window.location.href = result.data;
                }
                else {
                    var conf = confirm('Your request has been sent to Help Desk and will be processed shortly. Please click OK to redirect to login page.');
                    if (conf == true)
                        window.location.href = result.data;
                }
            }
            else {
                //write your logic to show errors
                alert(result.message);
            }
        };
        forgotpassword.prototype.errorCallbackLogin = function (result) {
            alert(result.message);
            //$('#lblError').html(result.message);
        };
        return forgotpassword;
    }());
    facilityAppForgotPassword.forgotpassword = forgotpassword;
})(facilityAppForgotPassword || (facilityAppForgotPassword = {}));
$(document).ready(function () {
    var oForgotPassword = new facilityAppForgotPassword.forgotpassword();
});
/// <reference path="Libraries\jquery.d.ts" />;
var facilityAppChangePassword;
(function (facilityAppChangePassword) {
    var URLHelper = (function () {
        function URLHelper() {
        }
        URLHelper.changePassword = '';
        URLHelper.redirectLogin = '/Home/Logout';
        return URLHelper;
    }());
    facilityAppChangePassword.URLHelper = URLHelper;
    var changePassword = (function () {
        function changePassword() {
            var _this = this;
            this.oldPassword = '';
            this.newPassword = '';
            this.retypeNewPassword = '';
            this.btnLogin = document.getElementById('btnSignIn');
            this.btnLogin.onclick = function (e) { _this.submit(); };
            this.btnCancel = document.getElementById('btnCancel');
            this.btnCancel.onclick = function (e) { window.location.href = URLHelper.redirectLogin; };
        }
        ;
        changePassword.prototype.validatePassword = function () {
            //validations here
            if ($('#txtOldPassword').val() == '') {
                $('#lblError').html("Please enter current password.");
                $('#txtOldPassword').focus();
                return false;
            }
            if ($('#txtNewPassword').val() == '') {
                $('#lblError').html("Please enter new password.");
                $('#txtNewPassword').focus();
                return false;
            }
            if ($('#txtRetypeNewPassword').val() == '') {
                $('#lblError').html("Please enter confirm password.");
                $('#txtRetypeNewPassword').focus();
                return false;
            }
            //if ($('#txtNewPassword').val() != '') {
            //    var password = $('#txtNewPassword').val();
            //    if (!(password.match(/[A-Z]/) && password.match(/\d+/) && password.match(/.[!,@@,#,$,%,\^,&,*,?,_,~]/) && (password.length > 6))) {
            //        $('#lblError').html("This is not a strong password \n A strong password is at least 7 characters long and contains uppercase and lowercase letters, numerals, and symbols.");
            //        $('#txtNewPassword').focus();
            //        $('#txtNewPassword').val('');
            //        $('#txtRetypeNewPassword').val('');
            //        return false;
            //    }
            //}
            //if ($('#txtNewPassword').val() !== $('#txtRetypeNewPassword').val()) {
            //    $('#lblError').html("New password and confirm password should match.");
            //    $('#txtNewPassword').val('');
            //    $('#txtRetypeNewPassword').val('');
            //    return false;
            //}
            //if ($('#txtOldPassword').val() === $('#txtNewPassword').val()) {
            //    $('#lblError').html("Current password and new password should not be same, please try different password.");
            //    $('#txtNewPassword').val('');
            //    $('#txtRetypeNewPassword').val('');
            //    return false;
            //}
            $('#lblError').html('');
            return true;
        };
        changePassword.prototype.submit = function () {
            if (this.validatePassword()) {
                var request = {
                    OldPassword: $('#txtOldPassword').val(),
                    NewPassword: $('#txtNewPassword').val(),
                    RetypeNewPassword: $('#txtRetypeNewPassword').val()
                };
                this.sendRequest(request);
            }
        };
        changePassword.prototype.sendRequest = function (postData) {
            var ajaxSettings = {
                url: facilityAppChangePassword.URLHelper.changePassword,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: this.successCallbackLogin,
                error: this.errorCallbackLogin
            };
            $.ajax(ajaxSettings);
        };
        changePassword.prototype.successCallbackLogin = function (result) {
            if (result.success == true) {
                var conf = confirm('Password changed successfully. Click OK to redirect to the Login page');
                if (conf == true)
                    window.location.href = result.data;
            }
            else {
                //write your logic to show errors
                $('#lblError').html(result.message);
                $('#txtOldPassword').val('');
                $('#txtNewPassword').val('');
                $('#txtRetypeNewPassword').val('');
            }
        };
        changePassword.prototype.errorCallbackLogin = function (result) {
            $('#lblError').html(result.message);
            //Show what is the error you got from valid login page
        };
        return changePassword;
    }());
    facilityAppChangePassword.changePassword = changePassword;
})(facilityAppChangePassword || (facilityAppChangePassword = {}));
$(document).ready(function () {
    var oChangePassword = new facilityAppChangePassword.changePassword();
});
/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqgrid.d.ts" />
/// <reference path="libraries\jquery.validation.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="libraries\jquery.tmpl.d.ts" />
/// <reference path="driverMaintenance.ts" />
var driverScheduleModule;
(function (driverScheduleModule) {
    var URLHelper = (function () {
        function URLHelper() {
        }
        URLHelper.loadDriverScheduleInfo = '';
        URLHelper.getDriverScheduleHours = '';
        URLHelper.saveDriverScheduleHours = '';
        URLHelper.getDriversList = '';
        URLHelper.saveWeeklyDriverSdhedule = '';
        URLHelper.saveDriverRoverTransfer = '';
        URLHelper.saveDriverPremanentTransfer = '';
        URLHelper.getDriverRoverFacility = '';
        URLHelper.updateDriverRoverFacilityInfo = '';
        return URLHelper;
    }());
    driverScheduleModule.URLHelper = URLHelper;
    (function (editDriverOptions) {
        editDriverOptions[editDriverOptions["driverInformation"] = 1] = "driverInformation";
        editDriverOptions[editDriverOptions["driverScheduleHours"] = 2] = "driverScheduleHours";
        editDriverOptions[editDriverOptions["rover"] = 3] = "rover";
        editDriverOptions[editDriverOptions["transfer"] = 4] = "transfer";
    })(driverScheduleModule.editDriverOptions || (driverScheduleModule.editDriverOptions = {}));
    var editDriverOptions = driverScheduleModule.editDriverOptions;
    //copied from enum AccessLevels
    (function (accessLevels) {
        accessLevels[accessLevels["PrimaryFacility"] = 4] = "PrimaryFacility";
        accessLevels[accessLevels["Rover"] = 6] = "Rover";
    })(driverScheduleModule.accessLevels || (driverScheduleModule.accessLevels = {}));
    var accessLevels = driverScheduleModule.accessLevels;
    var driverSchedule = (function () {
        function driverSchedule() {
            var _this = this;
            this.gridId = "list";
            this.gridListId = "listPager";
            this.dsGrid = null;
            this.dsGridList = null;
            this.dsGrid = $("#" + this.gridId);
            //this.ddlDriverList = document.getElementById('ddlDriversList');
            this.ddlEditDriverOptions = document.getElementById('ddlEditDriverOptions');
            this.editDriverInformation = $('#editDriverInformation');
            this.editDriverRover = $('#editDriverRover');
            this.editDriverTransfer = $('#editDriverTransfer');
            this.editDriverScheduleHours = $('#editDriverScheduleHours');
            this.noneReasonDriverScheduleMsg = $('#noneReasonDriverScheduleMsg');
            this.editDriverScheduleHoursTabs = $('#editDriverScheduleHoursTabs');
            this.modalUpdateselectedhours = $('#modalUpdateselectedhours');
            //this.ddlDriverList.onchange = (e) => { e.preventDefault(); this.loadGridOnDriverChange(e); };
            this.ddlEditDriverOptions.onchange = function (e) { e.preventDefault(); _this.onChangeEditDriverOptions(e); };
            this.btnSaveEditDriverInfo = $('#modalSaveEditDriverInfo');
            this.btnSaveEditDriverInfo.unbind("click").click(function (e) {
                e.preventDefault();
                _this.onClickSaveDriverInfo();
            });
            var driverScheduleTabOptions = {
                show: this.onChangeDriverScheduleTabOptions.bind(this)
            };
            this.editDriverScheduleHoursTabs.tabs(driverScheduleTabOptions);
            //this.modalSchduleHours = document.getElementById('modalSchduleHours');
            //this.modalSchduleHours.onclick = (e) => { e.preventDefault(); this.onsaveWeeklyScheduleHours(); };
            this.addDriverWeeklySchedule = document.getElementById('addDriverWeeklySchedule');
            this.addDriverWeeklySchedule.onclick = function (e) { e.preventDefault(); _this.onClickAddDriverWeeklySchedule(); };
            this.tmplAddWeeklyDriverSchedule = $('#tmplAddWeeklyDriverSchedule');
            this.tBodyDriverWeeklyScheduleHours = $('#tBodyDriverWeeklyScheduleHours');
            this.driverWeeklyScheduleFromDate = $('#WeeklyScheduleFromDate');
            this.imgStartDateIcon = document.getElementById('FromDateIcon');
            this.imgStartDateIcon.onclick = function (e) { e.preventDefault(); _this.driverWeeklyScheduleFromDate.focus(); };
            this.driverWeeklyScheduleFromDate.datepicker({
                minDate: new Date(),
                onSelect: this.onChangeDriverWeeklyScheduleFromDate.bind(this)
            });
            this.driverWeeklyScheduleToDate = $('#WeeklyScheduleToDate');
            this.imgToDateIcon = document.getElementById('ToDateIcon');
            this.imgToDateIcon.onclick = function (e) { e.preventDefault(); _this.driverWeeklyScheduleToDate.focus(); };
            this.driverWeeklyScheduleToDate.datepicker({
                minDate: new Date(),
                onSelect: this.onChangeDriverWeeklyScheduleToDate.bind(this)
            });
            //Rover date pickers and other functionalities
            var txtRoverToDate = $('#driverRoverSendToDate');
            this.driverRoverSendToDateIcon = document.getElementById('driverRoverSendToDateIcon');
            this.driverRoverSendToDateIcon.onclick = function (e) { e.preventDefault(); txtRoverToDate.focus(); };
            txtRoverToDate.datepicker({
                minDate: new Date(),
                onSelect: function (dateText, inst) {
                    txtRoverBackDate.datepicker("option", "minDate", txtRoverToDate.datepicker("getDate"));
                    this.AddRoverHintMessage();
                }.bind(this)
            });
            var txtRoverBackDate = $('#driverRoverBackOnDate');
            this.driverRoverBackonDateIcon = document.getElementById('driverRoverBackOnDateIcon');
            this.driverRoverBackonDateIcon.onclick = function (e) { e.preventDefault(); txtRoverBackDate.focus(); };
            txtRoverBackDate.datepicker({
                onSelect: function (dateText, inst) {
                    txtRoverToDate.datepicker("option", "maxDate", txtRoverBackDate.datepicker("getDate"));
                    this.AddRoverToHintMessage();
                }.bind(this)
            });
            this.ddlDriverRoverFacilites = document.getElementById('ddlDriverRoverFacilites');
            this.ddlDriverRoverFacilites.onchange = function (e) { e.preventDefault(); _this.onChangeDriverRoverOptions(e, _this); };
            this.ddlWeeklyDriverScheduleType = document.getElementById('ddlWeeklyDriverScheduleType');
            this.ddlWeeklyDriverScheduleType.onchange = function (e) { e.preventDefault(); _this.onChangeWeeklyDriverScheduleType(e); };
            //Premanent transfer date picker
            var txtTransferToDate = $('#permanentDriverTransferDate');
            this.permanentDriverTransferDateIcon = document.getElementById('permanentDriverTransferDateIcon');
            this.permanentDriverTransferDateIcon.onclick = function (e) { e.preventDefault(); txtTransferToDate.focus(); };
            txtTransferToDate.datepicker({
                minDate: new Date(),
                onSelect: function (dateText, inst) {
                    var driverName = $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html();
                    $('#dvDriverTransMessage').html("* <strong>" + driverName + "</strong> will be available on <strong>" + new Date(dateText).toDateMMDDYYYY() + "</strong>");
                }
            });
            this.ddlDriverTransferPermFacilites = document.getElementById('ddlDriverTransferPermFacilites');
            this.ddlDriverTransferPermFacilites.onchange = function (e) { e.preventDefault(); _this.onChangeDriverTransferOptions(e); };
            this.tBodyDriverRoverFacilityList = $('#tBodyDriverRoverFacilityList');
            this.editDriverRoverFacilityTabs = $('#editDriverRoverFacilityTabs');
            this.editDriverRoverFacilityTabs.removeClass('ui-widget');
            var roverTabOptions = {
                show: this.onChangeRoverTabOptions.bind(this)
            };
            this.editDriverRoverFacilityTabs.tabs(roverTabOptions);
            this.modalJqgridClose = document.getElementById('modalJqgridClose');
            this.modalJqgridClose.onclick = function (e) {
                var driverMaintenanceObjLocal = new driverMaintenanceModule.driverMaintenance();
                driverMaintenanceObjLocal.validateAndSubmit();
            };
        }
        driverSchedule.prototype.onChangeWeeklyDriverScheduleType = function (evet) {
            var currentTarget = $(evet.currentTarget);
            var selectedOpt = $("option:selected", currentTarget);
            var FromDate = new Date(this.driverWeeklyScheduleFromDate.val());
            var noOfDaystoAdd = selectedOpt.data("durationdays");
            var ToDate = new Date(FromDate).addDays(noOfDaystoAdd);
            //ToDate.setDate(FromDate.getDate() + noOfDaystoAdd);
            this.driverWeeklyScheduleFromDate.val(FromDate.toDateMMDDYYYY());
            this.driverWeeklyScheduleToDate.val(ToDate.toDateMMDDYYYY());
        };
        driverSchedule.prototype.onChangeDriverWeeklyScheduleFromDate = function (date) {
            this.ddlWeeklyDriverScheduleType.selectedIndex = 0;
            this.driverWeeklyScheduleToDate.datepicker("option", "minDate", date);
        };
        driverSchedule.prototype.onChangeDriverWeeklyScheduleToDate = function (date) {
            this.ddlWeeklyDriverScheduleType.selectedIndex = 0;
            this.driverWeeklyScheduleFromDate.datepicker("option", "maxDate", date);
        };
        driverSchedule.prototype.onChangeDriverRoverOptions = function (evet, parent) {
            var currentTarget = $(evet.currentTarget);
            if ($(currentTarget).val() != "-1") {
                $('#lblDriverRoverSendTo').text($('option:selected', this.ddlDriverRoverFacilites).text());
            }
            else {
                $('#lblDriverRoverSendTo').text('Facility');
            }
            parent.AddRoverHintMessage();
        };
        driverSchedule.prototype.onChangeDriverTransferOptions = function (event) {
            var currentTarget = $(event.currentTarget);
            if ($(currentTarget).val() != "-1") {
                $('#lblDriverTransferPermArriveAt').text($('option:selected', this.ddlDriverTransferPermFacilites).text());
            }
            else {
                $('#lblDriverTransferPermArriveAt').text('Facility');
            }
        };
        driverSchedule.prototype.AddRoverHintMessage = function () {
            var selectedFacilityId = $('#ddlDriverRoverFacilites').val();
            var sendDate = $('#driverRoverSendToDate').val();
            var msgDisplay = '';
            if (selectedFacilityId && selectedFacilityId > 0 && sendDate.length > 0) {
                var selectedFacilityName = $('option:selected', this.ddlDriverRoverFacilites).text();
                var driverName = $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html();
                msgDisplay = "* <strong>" + driverName + "</strong> will be available in <strong>" + selectedFacilityName + "</strong> on <strong>" + sendDate + "</strong>";
            }
            $('#dvDriverRoverMessage').html(msgDisplay);
        };
        driverSchedule.prototype.AddRoverToHintMessage = function () {
            var selectedFacilityId = $('#ddlDriverRoverFacilites').val();
            var sendDate = $('#driverRoverBackOnDate').val();
            var msgDisplay = '';
            if (selectedFacilityId && selectedFacilityId > 0 && sendDate.length > 0) {
                var selectedFacilityName = $('#lblDriverRoverBackOn', $('#addrover-driver-schedule')).text();
                var driverName = $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html();
                msgDisplay = "* <strong>" + driverName + "</strong> will be available in <strong>" + selectedFacilityName + "</strong> on <strong>" + sendDate + "</strong>";
            }
            $('#dvDriverRoverToMessage').html(msgDisplay);
        };
        driverSchedule.prototype.onChangeDriverScheduleTabOptions = function (evt, ui) {
            this.modalUpdateselectedhours.hide();
            this.btnSaveEditDriverInfo.hide();
            //Need to check with user waether to reset form or not
            if (ui.index == 1) {
                var width = window.innerWidth * 0.81;
                this.dsGrid.jqGrid('setGridWidth', width);
                this.reloadDSGrid();
                this.modalUpdateselectedhours.show();
            }
            else if (ui.index == 0) {
                this.btnSaveEditDriverInfo.show();
            }
        };
        driverSchedule.prototype.onChangeRoverTabOptions = function (evt, ui) {
            //Need to check with user waether to reset form or not
            if (ui.index == 0) {
                //alert('refresh add form');
                this.resetRoverTransferForm();
                this.btnSaveEditDriverInfo.show();
            }
            else if (ui.index == 1) {
                this.getDriverRoverFacilities();
                this.btnSaveEditDriverInfo.hide();
            }
        };
        driverSchedule.prototype.getDriverRoverFacilities = function () {
            var data = { DriverId: this.driverId };
            this.sendRequestToGetDriverRoverFacilitiesXHR(data);
        };
        driverSchedule.prototype.sendRequestToGetDriverRoverFacilitiesXHR = function (postData) {
            var ajaxSettings = {
                url: driverScheduleModule.URLHelper.getDriverRoverFacility,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackGetDriverRoverFacilities.bind(this))
                .fail(this.errorCallbackGetDriverRoverFacilities);
        };
        driverSchedule.prototype.successCallbackGetDriverRoverFacilities = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                //this.editDriverInformation.html(result);
                this.bindDriverRoverFacilityDataToUI(result.data);
            }
            $.blockUI;
        };
        driverSchedule.prototype.errorCallbackGetDriverRoverFacilities = function (result) {
            facilityApp.site.showError('Error occurred while fetching driver rover facility information. Please reload the page and try again!');
        };
        driverSchedule.prototype.bindDriverRoverFacilityDataToUI = function (html) {
            var _this = this;
            this.tBodyDriverRoverFacilityList.html(html);
            //Bind events for edit and save
            //$(".icon-driverroverfacility-add", this.tBodyDriverRoverFacilityList).click((e) => { e.preventDefault(); this.inlineAddDriverFacility(e); });
            $(".icon-driverroverfacility-edit", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineEditDriverRoverFacility(e); });
            $(".icon-driverroverfacility-delete", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineDeleteDriverRoverFacility(e); });
            $(".icon-driverroverfacility-save", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineSaveDriverRoverFacility(e); });
            $(".icon-driverroverfacility-cancel", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineCacnelDriverRoverFacility(e); });
            $(".icon-driverroverfacility-StartDate", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineStartDateRoverCalendar(e); });
            $(".icon-driverroverfacility-EndDate", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineEndDateRoverCalendar(e); });
            $(".txt-driverroverfacility-StartDate", this.tBodyDriverRoverFacilityList).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
            $(".txt-driverroverfacility-EndDate", this.tBodyDriverRoverFacilityList).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
        };
        driverSchedule.prototype.inlineSaveDriverRoverFacility = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = $(evt.currentTarget).closest('tr');
            var index = currentTarget.attr('data-index');
            var startDate = $(".driverroverfacility-driverstartdate", thisTr).val();
            var endDate = $(".driverroverfacility-driverenddate", thisTr).val();
            var dtstartDate = new Date(startDate);
            var dtendDate = new Date(endDate);
            if (dtstartDate > dtendDate) {
                facilityApp.site.showError('End Date should be greater than Start Date.');
                return;
            }
            var postData = {
                DriverId: $(".driverroverfacility-driverid", thisTr).val(),
                UserFacilityID: $(".driverroverfacility-userfacilityid", thisTr).val(),
                StoreRowID: $("option:selected", $(".driverroverfacility-storerowid", thisTr)).data('storerowid'),
                DriverStartDate: startDate,
                DriverEndDate: endDate,
                DFStatus: driverMaintenanceFormModule.DriverFacilityStatus.Updated
            };
            this.sendRequestInlineSaveDriverRoverFacilityXHR(postData);
        };
        driverSchedule.prototype.inlineDeleteDriverRoverFacility = function (evt) {
            if (confirm('Are you sure do you want to delete this record?')) {
                var currentTarget = $(evt.currentTarget);
                var thisTr = $(evt.currentTarget).closest('tr');
                var index = currentTarget.attr('data-index');
                var postData = {
                    DriverId: $(".driverroverfacility-driverid", thisTr).val(),
                    UserFacilityID: $(".driverroverfacility-userfacilityid", thisTr).val(),
                    StoreRowID: $("option:selected", $(".driverroverfacility-storerowid", thisTr)).data('storerowid'),
                    DriverStartDate: $(".driverroverfacility-driverstartdate", thisTr).val(),
                    DriverEndDate: new Date().toDateMMDDYYYY(),
                    DFStatus: driverMaintenanceFormModule.DriverFacilityStatus.Deleted
                };
                this.sendRequestInlineSaveDriverRoverFacilityXHR(postData);
            }
        };
        driverSchedule.prototype.sendRequestInlineSaveDriverRoverFacilityXHR = function (postData) {
            var ajaxSettings = {
                url: driverScheduleModule.URLHelper.updateDriverRoverFacilityInfo,
                type: 'POST',
                traditional: true,
                data: JSON.stringify(postData),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                async: true
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackInlineSaveDriverRoverFacility.bind(this))
                .fail(this.errorCallbackInlineSaveDriverRoverFacility);
        };
        driverSchedule.prototype.successCallbackInlineSaveDriverRoverFacility = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                this.bindDriverRoverFacilityDataToUI(result.data);
            }
            $.blockUI;
        };
        driverSchedule.prototype.errorCallbackInlineSaveDriverRoverFacility = function (result) {
            facilityApp.site.showError('Error occurred while fetching driver rover facility information. Please reload the page and try again!');
        };
        driverSchedule.prototype.inlineEditDriverRoverFacility = function (evt) {
            var inEditMode = $('span[class*="icon-driverroverfacility-save"]', this.tBodyDriverRoverFacilityList).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                var currentTarget = $(evt.currentTarget);
                //let driverFacilityId = currentTarget.attr('data-driverfacility');
                var thisTr = $(evt.currentTarget).closest('tr');
                var index = currentTarget.attr('data-index');
                $('.viewData', thisTr).hide();
                $('.editData', thisTr).show();
                $('.icon-driverroverfacility-edit', thisTr).hide();
                $('.icon-driverroverfacility-delete', thisTr).hide();
                currentTarget.hide();
                $(".actions ul li:nth-child(3)").addClass("disabled").attr("aria-disabled", "true").addClass("disable-anchor-click");
                $('.icon-driverroverfacility-save', thisTr).show();
                $('.icon-driverroverfacility-cancel', thisTr).show();
                var rowstartDate = new Date($('#DriverFacility\\[' + index + '\\]\\.DriverStartDate', this.tBodyDriverRoverFacilityList).val());
                var now = new Date();
                now.setHours(0, 0, 0, 0);
                if (rowstartDate < now) {
                    // selected date is in the past
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNo', this.tBodyDriverRoverFacilityList).prop('disabled', true);
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel', this.tBodyDriverRoverFacilityList).prop('disabled', true);
                }
                else {
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNo', this.tBodyDriverRoverFacilityList).prop('disabled', false);
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel', this.tBodyDriverRoverFacilityList).prop('disabled', false);
                }
            }
            else {
                facilityApp.site.showError('Please save / discard existing changes!');
            }
        };
        driverSchedule.prototype.inlineStartDateRoverCalendar = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = $(evt.currentTarget).closest('td');
            var txtStartDate = $(thisTr).find('.txt-driverroverfacility-StartDate');
            var canEditStartdate = $(txtStartDate).attr('data-canedit');
            if (canEditStartdate == "True") {
                $(txtStartDate).focus();
                $(txtStartDate).datepicker({ minDate: 0 });
            }
        };
        driverSchedule.prototype.inlineEndDateRoverCalendar = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = $(evt.currentTarget).closest('td');
            var txtEndDate = $(thisTr).find('.txt-driverroverfacility-EndDate');
            $(txtEndDate).focus();
            $(txtEndDate).datepicker({ minDate: 0 });
        };
        driverSchedule.prototype.dateBeforeShow = function (ele, inst) {
            var canEdit = $(ele).attr('data-canedit');
            return canEdit == "True";
        };
        driverSchedule.prototype.inlineCacnelDriverRoverFacility = function (evt) {
            var currentTarget = $(evt.currentTarget);
            //let driverFacilityId = currentTarget.attr('data-driverfacility');
            var thisTr = $(evt.currentTarget).closest('tr');
            var index = currentTarget.attr('data-index');
            $('.viewData', thisTr).show();
            $('.editData', thisTr).hide();
            $('.icon-driverroverfacility-edit', thisTr).show();
            $('.icon-driverroverfacility-delete', thisTr).show();
            currentTarget.hide();
            $('.icon-driverroverfacility-save', thisTr).hide();
            $('.icon-driverroverfacility-cancel', thisTr).hide();
            if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() == 'New') {
                thisTr.remove();
            }
            var inEditMode = $('span[class*="icon-driverroverfacility-save"]', this.tBodyDriverRoverFacilityList).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                $(".actions ul li:nth-child(3)").attr("aria-disabled", "false").removeClass("disabled").removeClass("disable-anchor-click");
            }
        };
        driverSchedule.prototype.onClickAddDriverWeeklySchedule = function () {
            var _this = this;
            //To build new row we need index, that index we are maintiaining in tbody tag. This needs to be increased when user adds new row
            var maxrowindex = this.tBodyDriverWeeklyScheduleHours.data('maxrowindex');
            ///Dont add more than 7 rows to schedule, because per week only 7 days if he wants to add different schedule for all days
            if (maxrowindex >= 6) {
                facilityApp.site.showError("Maximun number of rows added to schedule.");
                return;
            }
            var weeklySch = { index: ++maxrowindex };
            this.tBodyDriverWeeklyScheduleHours.append(this.tmplAddWeeklyDriverSchedule.tmpl(weeklySch));
            this.tBodyDriverWeeklyScheduleHours.data('maxrowindex', maxrowindex);
            $('.timepicker', this.tBodyDriverWeeklyScheduleHours).timepicker({
                timeFormat: 'h:mm p',
                minTime: '12:00 AM',
                maxTime: '11:30 PM',
                interval: 15,
                change: this.onChangeWeeklyTime
            });
            ///Unbind previous event and try to bind it again, other wise event may fire multiple times
            $('.weeklyscheduleday', this.tBodyDriverWeeklyScheduleHours).unbind("change").change(function (e) {
                e.preventDefault();
                _this.onChangeWeeklyScheduleCheckBox(e);
            });
            ///Unbind previous event and try to bind it again, other wise event may fire multiple times
            $('.weeklyscheduleselectall', this.tBodyDriverWeeklyScheduleHours).unbind("click").click(function (e) {
                e.preventDefault();
                _this.onChangeWeeklyScheduleSelectAll(e);
            });
            ///Unbind previous event and try to bind it again, other wise event may fire multiple times
            $('.weeklyschedue-daywork', this.tBodyDriverWeeklyScheduleHours).unbind("change").change(function (e) {
                e.preventDefault();
                _this.onChangeWeeklyScheduleStatusChange(e);
            });
        };
        driverSchedule.prototype.onChangeWeeklyScheduleStatusChange = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTable = currentTarget.closest('table');
            var thisTr = currentTarget.closest('tr');
            if (currentTarget.val() == 'S') {
                $('.starttime-picker', thisTr).val("7:00 AM").change();
                $('.endtime-picker', thisTr).val("5:00 PM").change();
            }
            else
                $('.timepicker', thisTr).val("12:00 AM").change();
        };
        driverSchedule.prototype.onChangeWeeklyScheduleSelectAll = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var ischecked = currentTarget.is(':checked');
            var thisTable = currentTarget.closest('table');
            var thisTr = currentTarget.closest('tr');
            var rowIndex = thisTr.data('rowindex');
            var excludetrclassname = 'weeklyscheduleselectall_' + rowIndex;
            $('.weeklyscheduleday', thisTr).each(function (i, e) {
                var thisElementClass = e.className.split(' ').filter(function (v, ind, arr) { return v.indexOf("weeklyschedule-") == 0; });
                $.each(thisElementClass, function (ia, clname) {
                    var anyCheckedOtherThisElement = $('.' + clname).not($('.' + clname, thisTr));
                    if (!(anyCheckedOtherThisElement && anyCheckedOtherThisElement.is(':checked') == true)) {
                        $('.' + clname, thisTr).prop("checked", true).trigger('change');
                    }
                });
            });
            /*
            if (ischecked) {
                let rowIndex = thisTr.data('rowindex');
                let excludetrclassname = 'weeklyscheduleselectall_' + rowIndex;
                //Uncheck other select all box
                $('.weeklyscheduleselectall:not(.' + excludetrclassname + ')').prop("checked", false).trigger('change');
            }

            $('.weeklyscheduleday', thisTr).prop("checked", ischecked).trigger('change');*/
        };
        driverSchedule.prototype.onChangeWeeklyScheduleCheckBox = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var ischecked = currentTarget.is(':checked');
            var dayofweek = currentTarget.data('weekday');
            var thisTable = currentTarget.closest('table');
            var thisTr = currentTarget.closest('tr');
            var startTime = "-";
            var endTime = "-";
            var workingHr = '0 hr 0 min';
            if (ischecked) {
                startTime = $('.starttime-picker', thisTr).val();
                endTime = $('.endtime-picker', thisTr).val();
                var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
                var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
                if (startTimeDt < endTimeDt) {
                    var differenceDt = new Date(endTimeDt - startTimeDt);
                    var hours = differenceDt.getUTCHours();
                    var minutes = differenceDt.getUTCMinutes();
                    workingHr = hours + ' hr ' + minutes + ' min';
                }
            }
            else {
            }
            var summaryRow = $('#tFootDriverWeeklyScheduleHours');
            var summaryStartRow = $('.summary-starttime-row', summaryRow);
            var summaryEndRow = $('.summary-endtime-row', summaryRow);
            var summaryTotalRow = $('.summary-totaltime-row', summaryRow);
            $('.' + dayofweek, summaryStartRow).html(startTime);
            $('.' + dayofweek, summaryEndRow).html(endTime);
            $('.' + dayofweek, summaryTotalRow).html(workingHr);
            if (ischecked) {
                //console.log(currentTarget);
                var rowIndex = thisTr.data('rowindex');
                var excludetrclassname = '.weeklyschedule-' + dayofweek;
                var otherCheckBoxes = $(excludetrclassname).not(currentTarget);
                $.each(otherCheckBoxes, function (i, o) {
                    var uncheckTr = $(o).closest('tr');
                    //$('.weeklyscheduleselectall', uncheckTr).prop("checked", false);
                    $(o).prop("checked", false);
                });
            }
        };
        driverSchedule.prototype.onChangeWeeklyTime = function () {
            var element = $(this);
            ///time-error class just removes red border, just visual indicator to show to select time
            if (element.length <= 0) {
                element.addClass('time-error');
            }
            else {
                element.removeClass('time-error');
            }
            //Get timepicker TR based on this object, and also get start and end time form this row and calculate working hours 
            var thisTr = element.closest('tr');
            var startTime = $('.starttime-picker', thisTr).val();
            var endTime = $('.endtime-picker', thisTr).val();
            var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
            var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
            var workingHr = '0 hr 0 min';
            if (startTimeDt < endTimeDt) {
                var differenceDt = new Date(endTimeDt - startTimeDt);
                var hours = differenceDt.getUTCHours();
                var minutes = differenceDt.getUTCMinutes();
                workingHr = hours + ' hr ' + minutes + ' min';
            }
            thisTr.find('span.totalworkinghours').text(workingHr);
            //Once you update time, then we have to make sure summary gets updated
            $('.weeklyscheduleday:checked', thisTr).trigger('change');
        };
        driverSchedule.prototype.unloadGrid = function () {
            $.jgrid.gridUnload(this.gridId);
            this.dsGrid = $("#" + this.gridId);
            this.dsGridList = $("#" + this.gridListId);
        };
        driverSchedule.prototype.bindGridControl = function (parent) {
            var _this = this;
            var columnNames = ['RowID', 'DriverScheduleDateIndex', 'Schedule Date', 'DriverID', 'Status', 'Start Time', 'End Time', 'Operating Hours', 'WeekDay'];
            var columnModel = [
                { name: 'RowID', index: 'RowID', editable: false, hidedlg: true, hidden: true },
                { name: 'DriverScheduleDateIndex', index: 'DriverScheduleDateIndex', width: 40, key: true, editable: true, editrules: { edithidden: false }, hidedlg: true, hidden: true, search: false },
                { name: 'ScheduleDate', index: 'ScheduleDate', width: 80, editable: false, hidedlg: true, hidden: false, search: false, align: 'center' },
                { name: 'DriverID', index: 'DriverID', width: 40, editable: false, editrules: { edithidden: false }, hidedlg: true, hidden: true },
                {
                    name: 'DayWork', index: 'DayWork', width: 40, editable: true, edittype: 'select', formatter: 'select', editrules: {
                        required: true
                    }, editoptions: {
                        value: { S: 'Scheduled', C: 'Off', O: 'On-Call' },
                        dataEvents: [
                            {
                                type: 'change',
                                fn: parent.dayWorkChanged.bind(parent)
                            }]
                    }, search: false, align: 'center'
                },
                {
                    name: 'ScheduleStartTime', index: 'ScheduleStartTime', width: 100, editable: true, edittype: 'text', search: false, align: 'center', editrules: { custom: true, custom_func: parent.checkStartEndTime, time: true, required: true },
                    editoptions: {
                        dataInit: function (element) {
                            $(element).timepicker({
                                timeFormat: 'h:mm p',
                                minTime: '12:00 AM',
                                maxTime: '11:30 PM',
                                interval: 15,
                                change: parent.endTimeChangeAssignOpertingHours.bind(this)
                            });
                        }
                    }, formoptions: { elmsuffix: ' *' }
                },
                {
                    name: 'ScheduleEndTime', index: 'ScheduleEndTime', width: 100, editable: true, edittype: 'text', search: false, align: 'center',
                    editrules: { custom: true, custom_func: parent.checkStartEndTime, time: true, required: true },
                    editoptions: {
                        dataInit: function (element) {
                            $(element).timepicker({
                                timeFormat: 'h:mm p',
                                minTime: '12:00 AM',
                                maxTime: '11:30 PM',
                                interval: 15,
                                change: parent.endTimeChangeAssignOpertingHours.bind(this)
                            });
                        }
                    },
                    formoptions: { elmsuffix: ' *' }
                },
                { name: 'DriverWorkingHours', index: 'DriverWorkingHours', width: 100, editable: true, edittype: 'custom', search: false, align: 'center', editoptions: { custom_element: this.getWorkingHoursElement, custom_value: this.getWorkingHoursValue } },
                { name: 'WeekDay', index: 'WeekDay', editable: false, hidden: false, stype: 'select', editoptions: { value: "0:All Days;8:Weekends;15:WeekDays;1:Sunday;2:Monday;3:Tuesday;4:Wednesday;5:Thursday;6:Friday;7:Saturday" }, align: 'center' }
            ];
            var jqGridOptions = {
                datatype: "local",
                pager: 'listPager',
                colNames: columnNames,
                colModel: columnModel,
                rowNum: 30,
                rowList: [10, 20, 30, 60, 365],
                sortname: 'ScheduleDate',
                sortorder: "asc",
                viewrecords: true,
                caption: 'Driver Schedule Hours',
                autoWidth: false,
                gridview: true,
                id: "RowID",
                height: "100%",
                multiselect: true,
                multiboxonly: true,
                toolbar: [true, "top"],
                emptyrecords: 'no records found',
                gridComplete: this.gridCompleteLocal.bind(this),
                loadComplete: this.gridLoadComplete.bind(this),
                loadError: this.gridLoadError.bind(this),
                ondblClickRow: this.gridDblClickRow.bind(this)
            };
            this.updateDialog = {
                url: driverScheduleModule.URLHelper.saveDriverScheduleHours,
                closeAfterEdit: true,
                reloadAfterSubmit: true,
                closeAfterAdd: true,
                afterShowForm: this.updateDialogAfterShowForm.bind(this),
                onclickSubmit: this.updateDialogOnclickSubmit.bind(this),
                afterComplete: this.updateDialogAfterComplete.bind(this),
                modal: true,
                width: "400"
            };
            this.dsGrid.jqGrid(jqGridOptions).navGrid(this.dsGridList, {
                edit: false, add: false, del: false, search: false, refresh: false
            }, this.updateDialog, null, null);
            jQuery("#modalUpdateselectedhours").click(function (e) {
                e.preventDefault();
                _this.onUpdateDriverSchedule(e, _this);
            });
            this.dsGrid.jqGrid('filterToolbar');
        };
        driverSchedule.prototype.onChangeEditDriverOptions = function (evt) {
            //   console.log(evt);
            var currentTarget = $(evt.currentTarget);
            this.changeEditDriverOptions(currentTarget.val());
        };
        driverSchedule.prototype.changeEditDriverOptions = function (optionsVal, selectedTabIndex) {
            //Hide all sections and show only required section in below conditional statement
            this.editDriverInformation.hide();
            this.editDriverRover.hide();
            this.editDriverTransfer.hide();
            this.editDriverScheduleHours.hide();
            this.noneReasonDriverScheduleMsg.hide();
            this.btnSaveEditDriverInfo.hide();
            this.modalUpdateselectedhours.hide();
            //this.modalSchduleHours.hide();
            if (optionsVal == editDriverOptions.driverInformation) {
                this.getDriverInformation();
                this.editDriverInformation.show();
                this.btnSaveEditDriverInfo.show();
            }
            else if (optionsVal == editDriverOptions.rover) {
                this.resetRoverTransferForm();
                this.editDriverRover.show();
                this.btnSaveEditDriverInfo.show();
                this.editDriverRoverFacilityTabs.tabs('select', (selectedTabIndex ? selectedTabIndex : 0));
            }
            else if (optionsVal == editDriverOptions.transfer) {
                this.resetTransferForm();
                this.editDriverTransfer.show();
                this.btnSaveEditDriverInfo.show();
            }
            else if (optionsVal == editDriverOptions.driverScheduleHours) {
                this.editDriverScheduleHours.show();
                //this.modalUpdateselectedhours.show();
                this.btnSaveEditDriverInfo.show();
                //By default select first tab
                this.editDriverScheduleHoursTabs.tabs('select', 0);
                //this.modalSchduleHours.show();
                //Reset the form with fresh values, so that user can see empty form.
                this.resetDriverScheduleWeeklyStatusForm();
                this.reloadDSGrid();
            }
            else {
                this.noneReasonDriverScheduleMsg.show();
            }
        };
        driverSchedule.prototype.resetRoverTransferForm = function () {
            $('#ddlDriverRoverFacilites').val('-1');
            $('#driverRoverSendToDate').val('');
            $('#driverRoverSendToDate').datepicker("option", {
                minDate: new Date(),
                maxDate: null
            });
            $('#driverRoverBackOnDate').val('');
            $('#driverRoverBackOnDate').datepicker("option", {
                minDate: new Date(),
                maxDate: null
            });
            $('#lblDriverRoverSendTo').text('Facility');
            $('#dvDriverRoverMessage').html('');
            $('#dvDriverRoverToMessage').html('');
        };
        driverSchedule.prototype.resetTransferForm = function () {
            this.ddlDriverTransferPermFacilites.value = "-1";
            $('#permanentDriverTransferDate').val('');
            $('#permanentDriverTransferDate').datepicker("option", {
                minDate: new Date(),
                maxDate: null
            });
            $('#lblDriverTransferPermArriveAt').text('Facility');
            $('#dvDriverTransMessage').html('');
        };
        driverSchedule.prototype.resetDriverScheduleWeeklyStatusForm = function () {
            this.tBodyDriverWeeklyScheduleHours.empty();
            ///Reset index to -1, so that user can add new entries
            this.tBodyDriverWeeklyScheduleHours.data("maxrowindex", -1);
            ///Empty summary data
            $('.summary-starttime-row td span').html('-');
            $('.summary-endtime-row td span').html('-');
            $('.summary-totaltime-row td span').html('-');
            //console.log(this.thStartDate);
            this.driverWeeklyScheduleFromDate.val(this.thStartDate);
            this.ddlWeeklyDriverScheduleType.selectedIndex = 0;
            var selectedOpt = $("option:selected", $('#ddlWeeklyDriverScheduleType'));
            var FromDate = new Date(this.driverWeeklyScheduleFromDate.val());
            var noOfDaystoAdd = selectedOpt.data("durationdays");
            var ToDate = new Date(FromDate).addDays(noOfDaystoAdd);
            this.driverWeeklyScheduleToDate.val(ToDate.toDateMMDDYYYY());
            ///Add one row to add weekly hours
            this.onClickAddDriverWeeklySchedule();
        };
        /**
         * Save weekly schedule hours START
         */
        driverSchedule.prototype.onsaveWeeklyScheduleHours = function () {
            if (this.validateWeeklyScheduleHours()) {
                var dt = $("#frmAddEditDriverScheduleHours").serializeArray();
                var data = {};
                $.each(dt, function (index, value) {
                    var vl = value.value;
                    if (value.name.indexOf('Time') > 0) {
                        var timeDt = new Date("1/1/1970" + ' ' + value.value);
                        vl = (timeDt.getHours() + ":" + timeDt.getMinutes() + ":00");
                    }
                    else if (value.value == "on") {
                        vl = true;
                    }
                    data[value.name] = vl;
                });
                data["UserID"] = this.driverId;
                this.onSaveWeeklyScheduleHoursXHR(data);
            }
        };
        driverSchedule.prototype.onSaveWeeklyScheduleHoursXHR = function (data) {
            var ajaxSettings = {
                url: driverScheduleModule.URLHelper.saveWeeklyDriverSdhedule,
                type: 'POST',
                traditional: true,
                data: JSON.stringify(data),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                async: true,
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackOnSaveWeeklyScheduleHoursXHR.bind(this))
                .fail(this.errorCallbackOnSaveWeeklyScheduleHoursXHR);
        };
        driverSchedule.prototype.successCallbackOnSaveWeeklyScheduleHoursXHR = function (response) {
            if (response.success == false) {
                facilityApp.site.showError(response.message);
            }
            this.changeEditDriverOptions(editDriverOptions.driverScheduleHours);
            /*
            if (result == "error") {
                alert('Error occurred while saving driver schedule hours. Please reload the page and try again!');
            } else {
               // alert('Drivers hours has been scheduled for the selected dates.');

                //After save just refresh page so that user may not submit again with save meause
                this.changeEditDriverOptions(editDriverOptions.driverScheduleHours);
            }*/
            //As per User requirements, after save data we should close popup window
            this.modalJqgridClose.click();
            $.blockUI;
        };
        driverSchedule.prototype.errorCallbackOnSaveWeeklyScheduleHoursXHR = function () {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        };
        driverSchedule.prototype.validateWeeklyScheduleHours = function () {
            var daywisecheckboxes = $('.weeklyscheduleday:checked', this.tBodyDriverWeeklyScheduleHours);
            var errmsg = "";
            var fromdate = new Date(this.driverWeeklyScheduleFromDate.val());
            var todate = new Date(this.driverWeeklyScheduleToDate.val());
            //Its an extra condition to make sure Start date should be less than End date
            if (fromdate > todate) {
                errmsg = "From date should be less than To date.";
            }
            if (daywisecheckboxes.length <= 0) {
                errmsg = errmsg + "\n Please select at least one day to schedule.";
            }
            var allschedulerows = $('.weeklyschedule-row', this.tBodyDriverWeeklyScheduleHours);
            $.each(allschedulerows, function (i, o) {
                var hasAnyDayScheduled = $('.weeklyscheduleday:checked', $(o));
                if (hasAnyDayScheduled.length > 0) {
                    var startTime = $('.starttime-picker', $(o));
                    var endTime = $('.endtime-picker', $(o));
                    var flg = true;
                    if (startTime.val().legth == 0) {
                        startTime.addClass('time-error');
                        flg = false;
                    }
                    if (endTime.val().length == 0) {
                        endTime.addClass('time-error');
                        flg = false;
                    }
                    if (flg == false) {
                        errmsg = errmsg + "\n Please select Start time and End time in the driver schedule row: " + (i + 1);
                    }
                    var startTimeDt = new Date("1/1/1970" + ' ' + startTime.val());
                    var endTimeDt = new Date("1/1/1970" + ' ' + endTime.val());
                    var workingHr = '0 hr 0 min';
                    //console.log(startTimeDt);
                    //console.log(endTimeDt);
                    if (startTimeDt > endTimeDt) {
                        startTime.addClass('time-error');
                        endTime.addClass('time-error');
                        flg = false;
                        errmsg = errmsg + "\n End time should be greater than Start time in driver schedule row: " + (i + 1);
                    }
                }
            });
            if (errmsg.length > 0) {
                facilityApp.site.showAlert(errmsg);
                return false;
            }
            else {
                return true;
            }
        };
        /**
         * Save weekly schedule hours END
         */
        driverSchedule.prototype.getDriverInformation = function () {
            var request = {
                Id: this.driverId
            };
            this.sendRequestForLoadUserInfo(request);
        };
        driverSchedule.prototype.sendRequestForLoadUserInfo = function (postData) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.loadUserInfo,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackLoadUserInfo.bind(this))
                .fail(this.errorCallbackLoadUserInfo);
        };
        driverSchedule.prototype.successCallbackLoadUserInfo = function (result) {
            if (result == "error") {
                facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
            }
            else {
                this.editDriverInformation.html(result);
                var ddlStatus = $(result).find('#StatusID');
                if ($('option:selected', ddlStatus).val() == '2') {
                    facilityApp.site.showConfrimWithOkCallback('Do you want to make the existing driver active?', this.callBackMethodForActiveConfirmation);
                }
            }
            $.blockUI;
        };
        driverSchedule.prototype.callBackMethodForActiveConfirmation = function () {
            $('#StatusID option[value=2]').removeAttr('selected');
            $('#StatusID option[value=1]').attr('selected', 'selected');
        };
        //bindDatePicker() {
        //    this.imgStartDateIcon = document.getElementById('driverAddStartDateIcon');
        //    let txtStartDate = $('#driverAddStartDate');
        //    this.imgStartDateIcon.onclick = (e) => { e.preventDefault(); txtStartDate.focus(); }
        //    txtStartDate.datepicker({ minDate: 0 });
        //}
        driverSchedule.prototype.errorCallbackLoadUserInfo = function (result) {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        };
        driverSchedule.prototype.onClickSaveDriverInfo = function () {
            var optionsVal = this.ddlEditDriverOptions.value;
            if (optionsVal == editDriverOptions.driverInformation) {
                this.sendRequestToSaveDriverDetails();
            }
            else if (optionsVal == editDriverOptions.rover) {
                this.sendRequestToSaveRover();
            }
            else if (optionsVal == editDriverOptions.transfer) {
                this.sendRequestToSaveRover();
            }
            else if (optionsVal == editDriverOptions.driverScheduleHours) {
                this.onsaveWeeklyScheduleHours();
            }
        };
        driverSchedule.prototype.sendRequestToSaveRover = function () {
            if (this.validatePremanentAndRoverData()) {
                var ajaxSettings = {
                    url: driverScheduleModule.URLHelper.saveDriverRoverTransfer,
                    type: 'POST',
                    traditional: true,
                    data: JSON.stringify(this.getRoverAndTransferData()),
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    async: true,
                };
                $.ajax(ajaxSettings)
                    .then(this.successCallbackToSendRequestTosaveDriverRoverTransfer.bind(this))
                    .fail(this.errorCallbackToSaveDriverRoverTransfer);
            }
        };
        driverSchedule.prototype.successCallbackToSendRequestTosaveDriverRoverTransfer = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                var optionsVal = this.ddlEditDriverOptions.value;
                if (optionsVal == editDriverOptions.rover) {
                    this.resetRoverTransferForm();
                }
                else if (optionsVal == editDriverOptions.transfer) {
                    this.resetTransferForm();
                }
                //As per User requirements, after save data we should close popup window
                this.modalJqgridClose.click();
            }
            $.blockUI;
        };
        driverSchedule.prototype.errorCallbackToSaveDriverRoverTransfer = function (result) {
            facilityApp.site.showError('Error occurred while saving driver information. Please reload the page and try again!');
        };
        driverSchedule.prototype.validatePremanentAndRoverData = function () {
            var optionsVal = this.ddlEditDriverOptions.value;
            var isValid = true;
            var msg = new Array();
            if (optionsVal == editDriverOptions.transfer) {
                if ($('option:selected', this.ddlDriverTransferPermFacilites).val() == '-1') {
                    //facilityApp.site.showError('Please select facility to transfer.');
                    msg.push('Please select facility to transfer.');
                    isValid = false;
                }
                if ($("#permanentDriverTransferDate").val() == '') {
                    //facilityApp.site.showError("Available in date cannot be empty.");
                    msg.push('Available in date cannot be empty.');
                    //$("#permanentDriverTransferDate").focus();
                    isValid = false;
                }
            }
            else if (optionsVal == editDriverOptions.rover) {
                if ($('option:selected', this.ddlDriverRoverFacilites).val() == '-1') {
                    //facilityApp.site.showError('Please select facility to transfer.');
                    msg.push('Please select facility to transfer.');
                    isValid = false;
                }
                if ($("#driverRoverSendToDate").val() == '') {
                    //facilityApp.site.showError("Available in date cannot be empty.");
                    msg.push('Available in date cannot be empty.');
                    //$("#driverRoverSendToDate").focus();
                    isValid = false;
                }
                if ($("#driverRoverBackOnDate").val() == '') {
                    //facilityApp.site.showError("Back in date cannot be empty.");
                    msg.push('Back in date cannot be empty.');
                    //$("#driverRoverBackOnDate").focus();
                    isValid = false;
                }
            }
            if (isValid == false) {
                facilityApp.site.showError(msg.join('<br/>'));
            }
            return isValid;
        };
        driverSchedule.prototype.getRoverAndTransferData = function () {
            var optionsVal = this.ddlEditDriverOptions.value;
            var sStartdate;
            var sEnddate;
            //var FromFacilityID; you can get this from session
            var sToFacilityID;
            var sFK_AccessLevelID;
            var sFk_UserId; //this.driverId
            if (optionsVal == editDriverOptions.transfer) {
                sStartdate = $('#permanentDriverTransferDate').val();
                sToFacilityID = this.ddlDriverTransferPermFacilites.value;
                sFK_AccessLevelID = accessLevels.PrimaryFacility;
                sFk_UserId = this.driverId;
            }
            else if (optionsVal == editDriverOptions.rover) {
                sStartdate = $('#driverRoverSendToDate').val();
                sEnddate = $('#driverRoverBackOnDate').val();
                sToFacilityID = this.ddlDriverRoverFacilites.value;
                sFK_AccessLevelID = accessLevels.Rover;
                sFk_UserId = this.driverId;
            }
            return {
                StartDate: sStartdate,
                EndDate: sEnddate,
                ToFacilityID: sToFacilityID,
                FK_AccessLevelID: sFK_AccessLevelID,
                FK_UserID: sFk_UserId
            };
        };
        driverSchedule.prototype.sendRequestToSaveDriverDetails = function () {
            if (this.validateEditDriverDetailsForm()) {
                var dt = $("#frmEditDriverDetails").serializeArray();
                var data = {};
                $.each(dt, function (index, value) {
                    data[value.name] = value.value;
                });
                var ajaxSettings = {
                    url: driverMaintenanceFormModule.URLHelper.saveDriverDetails,
                    type: 'POST',
                    traditional: true,
                    data: JSON.stringify(data),
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    async: true,
                };
                $.ajax(ajaxSettings)
                    .then(this.successCallbackToSaveDriverDetails.bind(this))
                    .fail(this.errorCallbackToSaveDriverDetails);
            }
        };
        driverSchedule.prototype.successCallbackToSaveDriverDetails = function (result) {
            if (result.success == false) {
                facilityApp.site.showError('Error occurred while saving driver information. Please reload the page and try again!');
            }
            else {
                //alert("Driver details has been updated successfully.");
                //As per User requirements, after save data we should close popup window
                this.modalJqgridClose.click();
            }
            $.blockUI;
        };
        driverSchedule.prototype.errorCallbackToSaveDriverDetails = function (result) {
            facilityApp.site.showError('Error occurred while saving driver information. Please reload the page and try again!');
        };
        driverSchedule.prototype.validateEditDriverDetailsForm = function () {
            var isValid = true;
            var Password = $('#txtPassword');
            var errMsgArray = new Array();
            //validate first name
            if (String.isNullOrEmpty($('#driverFirstName').val())) {
                $('#driverFirstName').css("border", "1px solid red");
                $("#driverFirstName").attr('title', 'Please enter first name.');
                errMsgArray.push('Please enter first name.');
                $('#driverFirstName').focus();
                isValid = false;
            }
            else {
                $('#driverFirstName').css("border", "1px #bababa solid");
            }
            //validate last name
            if (String.isNullOrEmpty($('#driverLastName').val())) {
                $('#driverLastName').css("border", "1px solid red");
                $("#driverLastName").attr('title', 'Please enter last name.');
                errMsgArray.push('Please enter last name.');
                $('#driverLastName').focus();
                isValid = false;
            }
            else {
                $('#driverLastName').css("border", "1px #bababa solid");
            }
            //Validate Email
            if (String.isNullOrEmpty($('#driverEmail').val()) || !this.validateEmail($('#driverEmail').val())) {
                $('#driverEmail').css("border", "1px solid red");
                $("#driverEmail").attr('title', 'Please enter valid email address.');
                errMsgArray.push('Please enter valid email address.');
                $('#driverEmail').focus();
                isValid = false;
            }
            else {
                $('#driverEmail').css("border", "1px #bababa solid");
            }
            //validate license
            if ($('#LicenseClassID').val() == '') {
                $('#LicenseClassID').css("border", "1px solid red");
                $("#LicenseClassID").attr('title', 'Please select the license class.');
                errMsgArray.push('Please select the license class.');
                $('#LicenseClassID').focus();
                isValid = false;
            }
            else {
                $('#LicenseClassID').css("border", "1px #bababa solid");
            }
            //validate driver status.
            if ($('#StatusID').val() == '') {
                $('#StatusID').css("border", "1px solid red");
                $("#StatusID").attr('title', 'Please select the driver status.');
                errMsgArray.push('Please select the driver status.');
                $('#StatusID').focus();
                isValid = false;
            }
            else {
                $('#StatusID').css("border", "1px #bababa solid");
            }
            if (String.isNullOrEmpty(Password.val())) {
                Password.css("border", "1px solid red");
                Password.attr('title', 'Please enter password.');
                errMsgArray.push('Please enter password.');
                Password.focus();
                isValid = false;
            }
            else if (String.hasWhiteSpace(Password.val())) {
                Password.css("border", "1px solid red");
                Password.attr('title', 'Password should not contain spaces.');
                errMsgArray.push('Password should not contain spaces.');
                Password.focus();
                isValid = false;
            }
            else if (Password.val().length < driverMaintenanceFormModule.DefaultVariables.PasswordMinLength) {
                var errorMsg = 'Password must be at least ' + driverMaintenanceFormModule.DefaultVariables.PasswordMinLength + ' characters.';
                Password.css("border", "1px solid red");
                Password.attr('title', errorMsg);
                errMsgArray.push(errorMsg);
                Password.focus();
                isValid = false;
            }
            else {
                Password.css("border", "1px #bababa solid");
            }
            if (isValid == false) {
                facilityApp.site.showError(errMsgArray.join("<br/>"));
            }
            return isValid;
        };
        driverSchedule.prototype.validateEmail = function (email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
            ;
        };
        driverSchedule.prototype.updateDialogAfterShowForm = function (formid) {
            this.getWorkingHours(formid, true, false);
        };
        driverSchedule.prototype.updateDialogOnclickSubmit = function (params) {
            return { dates: this.dsGrid.jqGrid('getGridParam', 'selarrrow'), driverID: this.driverId };
        };
        driverSchedule.prototype.updateDialogAfterComplete = function (response) {
            var json = jQuery.parseJSON(response.responseText);
            //console.log(json);
            if (json.success == false) {
                facilityApp.site.showError(json.message);
            }
        };
        driverSchedule.prototype.onUpdateDriverScheduleAfterComplete = function (response) {
            var json = jQuery.parseJSON(response.responseText);
            //console.log(json);
            if (json.success == false) {
                facilityApp.site.showError(json.message);
            }
        };
        driverSchedule.prototype.getWorkingHours = function (formid, onload, isMultipleSelected) {
            //When user select multiple check box from UI and trying to update all at once, by that time we are showing default values as 07AM to 05PM.
            if (onload && isMultipleSelected) {
                $("select#DayWork", formid).val('S');
                $("input#ScheduleStartTime", formid).val('07:00 AM');
                $("input#ScheduleEndTime", formid).val('05:00 PM');
            }
            var startTime = $("input#ScheduleStartTime", formid).val();
            var endTime = $("input#ScheduleEndTime", formid).val();
            var DayWork = $("select#DayWork", formid).val();
            var workingHr = '0 hr 0 min';
            //console.log('DayWork: ' + DayWork + '; startTime: ' + startTime + '; endtime: ' + endTime);
            //Here status S-Scheduled, O-Off, C-On-Call
            if ((DayWork != 'O' || DayWork != 'C') && startTime != '' && endTime != '') {
                var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
                var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
                if (startTimeDt < endTimeDt) {
                    var differenceDt = new Date(endTimeDt - startTimeDt);
                    var hours = differenceDt.getUTCHours();
                    var minutes = differenceDt.getUTCMinutes();
                    workingHr = hours + ' hr ' + minutes + ' min';
                }
            }
            $("span#DriverWorkingHours", formid).html(workingHr);
        };
        driverSchedule.prototype.getWorkingHoursElement = function (value, options) {
            var el = document.createElement("span");
            //el.type = "span";
            el.className = 'oprationHoursDuration';
            el.innerHTML = value;
            return el;
        };
        driverSchedule.prototype.getWorkingHoursValue = function (elem, operation, value) {
            if (operation === 'get') {
                return $(elem).val();
            }
            else if (operation === 'set') {
                //$('span', elem).val(value);
                $(elem, 'span').each(function (index, element) { $(this).text(value); });
            }
        };
        driverSchedule.prototype.endTimeChangeAssignOpertingHours = function (value, colname) {
            var startTime = $("input#ScheduleStartTime").val();
            var endTime = $("input#ScheduleEndTime").val();
            var DayWork = $("select#DayWork").val();
            var workingHr = '0 hr 0 min';
            //Here status S-Scheduled, O-Off, C-On-Call
            if ((DayWork != 'O' || DayWork != 'C') && startTime != '' && endTime != '') {
                var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
                var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
                if (startTimeDt < endTimeDt) {
                    var differenceDt = new Date(endTimeDt - startTimeDt);
                    var hours = differenceDt.getUTCHours();
                    var minutes = differenceDt.getUTCMinutes();
                    workingHr = hours + ' hr ' + minutes + ' min';
                }
            }
            $("span#DriverWorkingHours").html(workingHr);
        };
        driverSchedule.prototype.dayWorkChanged = function (e) {
            if ($(e.target).val() == "C" || $(e.target).val() == "O") {
                $("#ScheduleStartTime").val('12:00 AM');
                $("#ScheduleEndTime").val('12:00 AM');
            }
            else {
                $("#ScheduleStartTime").val('07:00 AM');
                $("#ScheduleEndTime").val('05:00 PM');
            }
            this.displayWorkingHours($("#ScheduleStartTime").val(), $("#ScheduleEndTime").val());
        };
        driverSchedule.prototype.displayWorkingHours = function (startTime, endTime) {
            var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
            var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
            var workingHr = '0 hr 0 min';
            if (startTimeDt < endTimeDt) {
                var differenceDt = new Date(endTimeDt - startTimeDt);
                var hours = differenceDt.getUTCHours();
                var minutes = differenceDt.getUTCMinutes();
                workingHr = hours + ' hr ' + minutes + ' min';
            }
            $("#DriverWorkingHours").html(workingHr);
        };
        driverSchedule.prototype.checkStartEndTime = function (value, colname) {
            var startTime = $("input#ScheduleStartTime").val();
            var endTime = $("input#ScheduleEndTime").val();
            if (startTime != '' && endTime != '') {
                var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
                var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
                if (startTimeDt > endTimeDt) {
                    return [false, "Start time should be less than End time.", ""];
                }
            }
            return [true, "", ""];
        };
        ;
        driverSchedule.prototype.onUpdateDriverSchedule = function (evt, parent) {
            var rows = this.dsGrid.jqGrid('getGridParam', 'selarrrow');
            if (rows.length > 0) {
                this.dsGrid.jqGrid('editGridRow', rows, {
                    url: URLHelper.saveDriverScheduleHours,
                    height: 240,
                    closeAfterEdit: true,
                    reloadAfterSubmit: true,
                    afterShowForm: this.onUpdateDriverScheduleAfterShowForm.bind(this),
                    onclickSubmit: this.onUpdateDriverScheduleOnclickSubmit.bind(this),
                    afterComplete: this.onUpdateDriverScheduleAfterComplete.bind(this)
                });
            }
            else
                facilityApp.site.showAlert("Please Select a Row.");
        };
        driverSchedule.prototype.onUpdateDriverScheduleAfterShowForm = function (formid) {
            $("select#DayWork", formid).val("S");
            this.getWorkingHours(formid, true, true);
        };
        driverSchedule.prototype.onUpdateDriverScheduleOnclickSubmit = function (params) {
            var ajaxData = {};
            var rowids = this.dsGrid.jqGrid('getGridParam', 'selarrrow');
            ajaxData = { dates: rowids, driverID: this.driverId };
            return ajaxData;
        };
        driverSchedule.prototype.adjustHourChangeDimentions = function () {
            var grid = $('#gbox_' + this.gridId).parent();
            var gridParentWidth = grid.width();
            this.dsGrid.jqGrid('setGridWidth', gridParentWidth);
            //var gridParentHeight = grid.height();
            //if (gridParentHeight <= 800) {
            //    var height = ($(window).height() - $(".modal-box").outerHeight()) - 100;
            //}
        };
        driverSchedule.prototype.gridCompleteLocal = function () {
            var ids = this.dsGrid.getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var cell = this.dsGrid.getCell(ids[i], "WeekDay");
                if (cell == "Sunday" || cell == "Saturday") {
                    this.dsGrid.jqGrid('setRowData', ids[i], false, { background: '#e0effa' });
                }
            }
            //disable check boxes which are less than todays date
            var jqgList = $("input:checkbox[id^=jqg_list_]");
            $.each(jqgList, function (i, o) {
                var dt = new Date(o.id.replace('jqg_list_', ''));
                var currentdate = new Date();
                currentdate.setHours(0, 0, 0, 0);
                if (currentdate > dt)
                    $(o).remove();
            });
        };
        driverSchedule.prototype.reloadDSGrid = function () {
            this.dsGrid.jqGrid("clearGridData", true);
            this.dsGrid.jqGrid('setGridParam', {
                mtype: 'GET',
                url: driverScheduleModule.URLHelper.getDriverScheduleHours,
                editurl: driverScheduleModule.URLHelper.saveDriverScheduleHours,
                onclickSubmit: this.submitUpdatedDriverSchedule,
                datatype: 'json',
                postData: {
                    dates: this.dsGrid.jqGrid('getGridParam', 'selarrrow'),
                    startDate: this.thStartDate,
                    driverID: this.driverId
                }
            }).trigger("reloadGrid");
        };
        driverSchedule.prototype.submitUpdatedDriverSchedule = function () {
            //returning respective parameters to Jquery Ajax call
            var ajaxData = {};
            var rowIds = this.dsGrid.jqGrid('getGridParam', 'selarrrow');
            ajaxData = { dates: rowIds, driverID: this.driverId };
            return ajaxData;
        };
        driverSchedule.prototype.submitUpdateDriverForSingleRecord = function (rows) {
            return { dates: rows, driverID: this.driverId };
        };
        driverSchedule.prototype.gridDblClickRow = function (rowid, iRow, iCol, e) {
            var dt = new Date(rowid);
            var currentdate = new Date();
            currentdate.setHours(0, 0, 0, 0); //removing any hours there to current date.
            if (currentdate <= dt)
                this.dsGrid.editGridRow(rowid, this.updateDialog);
        };
        driverSchedule.prototype.gridLoadComplete = function () {
            //setTimeout(function () { driverSchedule.prototype.setGridHeight(); }, 200);
        };
        driverSchedule.prototype.gridLoadError = function (xhr, st, err) {
            if (xhr.status == "200")
                return false;
            var error = eval('(' + xhr.responseText + ')');
            $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                width: '600',
                buttons: {
                    Ok: function () {
                        //$(this).dialog("close");
                    }
                }
            });
        };
        driverSchedule.prototype.setGridHeight = function () {
            var wheight = window.innerHeight - 420;
            $('.ui-jqgrid-bdiv').not($('.ui-jqgrid-bdiv', document.getElementById("list"))).height(wheight);
        };
        driverSchedule.prototype.showDriverSchedulePopup = function (driverID, name, thstartDate, selectedDriverOptions, mainForm) {
            this.bindGridControl(this);
            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popupDriverSchedule').fadeIn();
            var driverHead = "";
            if (driverID > 0) {
                driverHead = name;
            }
            else {
                driverHead = "Driver";
            }
            $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html(driverHead);
            //assigning variables to respective values
            this.driverId = driverID;
            this.thStartDate = thstartDate;
            //this.adjustHourChangeDimentions();
            this.ddlEditDriverOptions.value = selectedDriverOptions;
            var selectTabIndex = 0;
            if (selectedDriverOptions == editDriverOptions.rover)
                selectTabIndex = 1;
            //If user trying to edit rover records, we should pass tab index 1 other wise default value that is "0"
            this.changeEditDriverOptions(selectedDriverOptions, selectTabIndex);
            this.adjustPopupPosition();
        };
        driverSchedule.prototype.adjustPopupPosition = function () {
            var ht = $('html').offset().top;
            ht = ht <= 0 ? (-1 * ht) : ht;
            $(".modal-box").css({
                top: ht,
                left: ($(window).width() - $(".modal-box").outerWidth()) / 2
            });
        };
        driverSchedule.prototype.initPopup = function () {
            $(".js-modal-close, .modal-overlay").click(function (e) {
                e.preventDefault();
                $(".modal-box, .modal-overlay").fadeOut(500, function () {
                    $(".modal-overlay").remove();
                });
            });
            $(window).resize(function () {
                $(".modal-box").css({
                    top: 5,
                    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                });
            });
            $(window).resize();
            // this.initEvents();
        };
        driverSchedule.prototype.loadGridOnDriverChange = function (event) {
            this.driverId = event.target.value;
            this.reloadDSGrid();
        };
        driverSchedule.prototype.clearJqGridCustom = function () {
            var objRows = $('#list tr').remove();
        };
        return driverSchedule;
    }());
    driverScheduleModule.driverSchedule = driverSchedule;
})(driverScheduleModule || (driverScheduleModule = {}));
$(document).ready(function () {
    var oDriverForm = new driverScheduleModule.driverSchedule();
    oDriverForm.initPopup();
});
/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="Libraries\jquery.blockUI.d.ts" />;
var facilityApp;
(function (facilityApp) {
    var site = (function () {
        function site() {
            this.initSiteEvents();
        }
        site.prototype.initSiteEvents = function () {
            $(document).ajaxStart($.blockUI)
                .ajaxStop($.unblockUI);
            $(".ui-draggable").draggable({
                handle: ".ui-draggable-handle"
            });
        };
        site.showError = function (outputMsg) {
            this.alert(outputMsg, 'Error', undefined);
        };
        site.showAlert = function (outputMsg) {
            this.alert(outputMsg, 'Alert', undefined);
        };
        site.showConfrimWithOkCallback = function (outputMsg, okCallbackMethod) {
            this.confrim(outputMsg, 'Confirm', okCallbackMethod, undefined);
        };
        site.showConfrim = function (outputMsg, okCallbackMethod, cancelBackMethod) {
            this.confrim(outputMsg, 'Confirm', okCallbackMethod, cancelBackMethod);
        };
        site.alert = function (outputMsg, titleMsg, onCloseCallback) {
            if (!titleMsg)
                titleMsg = 'Alert';
            if (!outputMsg)
                outputMsg = 'Unexpected error occurred.';
            $("<div class='site-alert-popup'></div>").html(outputMsg).dialog({
                title: titleMsg,
                //resizable: false,
                modal: true,
                minWidth: 800,
                position: 'top',
                minHeight: 220,
                buttons: {
                    "OK": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                    if (onCloseCallback)
                        onCloseCallback();
                    /* Cleanup node(s) from DOM */
                    $(this).dialog('destroy').remove();
                },
                open: function (event, ui) { $('.ui-widget-overlay').bind('click', function () { $(".site-alert-popup").dialog('close'); }); }
            });
        };
        site.confrim = function (outputMsg, titleMsg, onOkCallback, onCancelCallbak) {
            if (!titleMsg)
                titleMsg = 'Confrim';
            if (!outputMsg)
                outputMsg = 'Unexpected error occurred.';
            $("<div class='site-alert-popup'></div>").html(outputMsg).dialog({
                title: titleMsg,
                //resizable: false,
                modal: true,
                minWidth: 800,
                position: 'top',
                minHeight: 220,
                buttons: {
                    "Ok": function () {
                        if (onOkCallback)
                            onOkCallback();
                        $(this).dialog("close");
                    },
                    "Cancel": function () {
                        if (onCancelCallbak)
                            onCancelCallbak();
                        $(this).dialog("close");
                    }
                },
                close: function () {
                    /* Cleanup node(s) from DOM */
                    $(this).dialog('destroy').remove();
                }
            });
        };
        return site;
    }());
    facilityApp.site = site;
})(facilityApp || (facilityApp = {}));
String.isNullOrEmpty = function (val) {
    if (val === undefined || val === null || val.trim() === '') {
        return true;
    }
    return false;
};
String.hasWhiteSpace = function (val) {
    return val.indexOf(' ') >= 0;
};
Date.prototype.toDateMMDDYYYY = function () {
    var year = this.getFullYear();
    var month = (1 + this.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = this.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
    //return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
};
Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + days);
    var dd = this.getDate();
    var mm = this.getMonth() + 1;
    var y = this.getFullYear();
    return new Date(mm + '/' + dd + '/' + y);
};
$(document).ready(function () {
    new facilityApp.site();
});
/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="driverMaintenanceForm.ts" />
/// <reference path="driverSchedule.ts" />
/// <reference path="site.ts" />
var driverMaintenanceModule;
(function (driverMaintenanceModule) {
    var URLHelper = (function () {
        function URLHelper() {
        }
        URLHelper.getDriverScheduleList = '';
        return URLHelper;
    }());
    driverMaintenanceModule.URLHelper = URLHelper;
    var driverMaintenance = (function () {
        function driverMaintenance() {
            var _this = this;
            this.driverMaintenanceFormObj = new driverMaintenanceFormModule.driverMaintenanceForm();
            this.btnSearch = document.getElementById('btnSearch');
            this.btnReset = document.getElementById('btnReset');
            this.btnAddNewUser = document.getElementById('btnAddNewUser');
            this.btnPrint = document.getElementById('btnPrint');
            this.imgStartDateIcon = document.getElementById('StartDateIcon');
            var txtStartDate = $('#StartDate');
            this.btnSearch.onclick = function (e) { e.preventDefault(); _this.validateAndSubmit(); };
            this.btnReset.onclick = function (e) { e.preventDefault(); _this.resetForm(); };
            this.btnAddNewUser.onclick = function (e) {
                e.preventDefault();
                var driverMaintenanceFormObjLocal = new driverMaintenanceFormModule.driverMaintenanceForm();
                driverMaintenanceFormObjLocal.showEditPopup(0, "", _this);
            };
            this.btnPrint.onclick = function (e) {
                e.preventDefault();
                _this.handlePrintFuncationality();
            };
            //this.assignEditMethodToBtn();
            this.imgStartDateIcon.onclick = function (e) { e.preventDefault(); txtStartDate.focus(); };
            txtStartDate.datepicker({});
            //window.onresize = this.adjustGridHeight;
        }
        driverMaintenance.prototype.handlePrintFuncationality = function () {
            var data = $('.cust-grid-jqgrid').clone(); // $('.tblDriverScheduleHrs').html();
            data.find('#dvDriverScheduleList').height('100%');
            data.find('#dvDriverScheduleList').css('overflow', 'visible');
            data.find('tbody').css('overflow', 'visible');
            data.find('th').each(function () { $(this).css('top', '0px'); });
            data.find('a').each(function () {
                $(this).removeAttr("href");
            });
            data.find('#imgPrevious').remove();
            data.find('#imgNext').remove();
            var mywindow = window.open('', 'Packrat Drivers', 'height=400,width=600');
            mywindow.document.write('<html><head><title>Packrat Drivers</title>');
            mywindow.document.write('<link href="/Content/css/footable.core.css" rel="stylesheet" media="print"/><link href="/Content/css/footable.standalone.css" rel="stylesheet" media="print" /> <style type="text/css" >  @page { size: landscape; } @media print { table thead tr th.center,  table tbody tr td.center { text-align: center !important; } table th, table td { border:1px solid #ccc; padding: 0.005em; } } </style>');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data.html());
            mywindow.document.write('</body></html>');
            mywindow.print();
            mywindow.close();
        };
        driverMaintenance.prototype.resetForm = function () {
            var reqDate = (new Date()).toDateMMDDYYYY();
            var request = {
                StartDate: reqDate
            };
            if (request.StartDate != undefined) {
                this.sendRequest(request);
                $('#StartDate').val(reqDate);
            }
        };
        driverMaintenance.prototype.onDriverScheduleGridLoad = function () {
            var _this = this;
            this.imgNextBtn = document.getElementById('imgNext');
            this.imgNextBtn.onclick = function (e) { e.preventDefault(); _this.nextAndSubmit(); };
            this.imgPreviousBtn = document.getElementById('imgPrevious');
            this.imgPreviousBtn.onclick = function (e) { e.preventDefault(); _this.previousAndSubmit(); };
            //this.adjustGridHeight();
        };
        //adjustGridHeight(): void {
        //var wheight = (window.innerHeight - 410);
        //$('#dvDriverScheduleList').css("height", wheight);
        //}
        driverMaintenance.prototype.validateAndSubmit = function () {
            var request = {
                StartDate: $('#StartDate').val()
            };
            if (request.StartDate != undefined)
                this.sendRequest(request);
            else
                facilityApp.site.showError("Please select date and Submit.");
        };
        driverMaintenance.prototype.nextAndSubmit = function () {
            var StartDate = new Date($('#StartDate').val());
            StartDate.setDate(StartDate.getDate() + 7);
            var reqDate = (StartDate.getMonth() + 1) + "/" + StartDate.getDate() + "/" + StartDate.getFullYear();
            var request = {
                StartDate: reqDate
            };
            if (request.StartDate != undefined) {
                this.sendRequest(request);
                $('#StartDate').val(reqDate);
                this.assignEditMethodToBtn();
            }
        };
        driverMaintenance.prototype.previousAndSubmit = function () {
            var StartDate = new Date($('#StartDate').val());
            StartDate.setDate(StartDate.getDate() - 7);
            var reqDate = (StartDate.getMonth() + 1) + "/" + StartDate.getDate() + "/" + StartDate.getFullYear();
            var request = {
                StartDate: reqDate
            };
            if (request.StartDate != undefined) {
                this.sendRequest(request);
                $('#StartDate').val(reqDate);
                this.assignEditMethodToBtn();
            }
        };
        driverMaintenance.prototype.sendRequest = function (postData) {
            var ajaxSettings = {
                url: driverMaintenanceModule.URLHelper.getDriverScheduleList,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallback.bind(this))
                .fail(this.errorCallback)
                .done(this.onDriverScheduleGridLoad.bind(this));
        };
        driverMaintenance.prototype.successCallback = function (result) {
            if (result == "error") {
                facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
            }
            else {
                $('#dvDriverScheduleList').html(result);
                this.assignEditMethodToBtn();
                //fixed header
                $(".new-fixed-header").tableHeadFixer();
            }
            $.blockUI;
        };
        driverMaintenance.prototype.errorCallback = function (result) {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        };
        driverMaintenance.prototype.assignEditMethodToBtn = function () {
            var _this = this;
            $('.editDriverDetails').click(function (evt) {
                evt.preventDefault();
                var currentTarget = $(evt.currentTarget);
                var driverId = currentTarget.attr('data-driverId');
                var drivername = currentTarget.attr('data-driverfullname');
                //let driverMaintenanceFormObjLocal = new driverMaintenanceFormModule.driverMaintenanceForm();
                //driverMaintenanceFormObjLocal.showEditPopup(driverId, drivername, this, driverScheduleModule.editDriverOptions.driverScheduleHours);
                var thStartDate = $('#StartDate').val();
                var driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
                driverMaintenanceFormObjLocal.showDriverSchedulePopup(driverId, drivername, thStartDate, driverScheduleModule.editDriverOptions.driverInformation, _this);
            });
            $('.editDriverLicenseDetails').click(function (evt) {
                evt.preventDefault();
                var currentTarget = $(evt.currentTarget);
                var driverId = currentTarget.attr('data-driverId');
                var drivername = currentTarget.attr('data-driverfullname');
                //let driverMaintenanceFormObjLocal = new driverMaintenanceFormModule.driverMaintenanceForm();
                //driverMaintenanceFormObjLocal.showEditPopup(driverId, drivername, this, driverScheduleModule.editDriverOptions.driverScheduleHours);
                var thStartDate = $('#StartDate').val();
                var driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
                driverMaintenanceFormObjLocal.showDriverSchedulePopup(driverId, drivername, thStartDate, driverScheduleModule.editDriverOptions.driverInformation, _this);
            });
            $('.editDriverScheduleHours').click(function (evt) {
                evt.preventDefault();
                var currentTarget = $(evt.currentTarget);
                var driverId = currentTarget.attr('data-driverId');
                var drivername = currentTarget.attr('data-driverfullname');
                //let thisTd = $(evt.currentTarget).closest('td');
                //var thStartDate = thisTd.closest('table').find('th').eq(thisTd.index()).text();
                var thStartDate = currentTarget.attr('data-date');
                var datastatus = currentTarget.attr('data-status');
                //console.log('111111111111111111 : ' + thStartDate);
                var editDriverOptions = (datastatus == "RoverOut" ? driverScheduleModule.editDriverOptions.rover : driverScheduleModule.editDriverOptions.driverScheduleHours);
                var driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
                driverMaintenanceFormObjLocal.showDriverSchedulePopup(driverId, drivername, thStartDate, editDriverOptions, _this);
            });
            $('.driver-schedule-truckstatistics').click(function (evt) {
                evt.preventDefault();
                var currentTarget = $(evt.currentTarget).find('img');
                var tmp = currentTarget.attr('src');
                currentTarget.attr('src', currentTarget.attr('data-togglesrc'));
                currentTarget.attr('data-togglesrc', tmp);
                $('.truck-totalhours').toggleClass('hide');
            });
        };
        return driverMaintenance;
    }());
    driverMaintenanceModule.driverMaintenance = driverMaintenance;
})(driverMaintenanceModule || (driverMaintenanceModule = {}));
$(document).ready(function () {
    var oDM = new driverMaintenanceModule.driverMaintenance();
    oDM.validateAndSubmit();
});
/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jquery.steps.d.ts" />
/// <reference path="libraries\jquery.validation.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="driverMaintenance.ts" />
var driverMaintenanceFormModule;
(function (driverMaintenanceFormModule) {
    var URLHelper = (function () {
        function URLHelper() {
        }
        URLHelper.loadUserInfo = '';
        URLHelper.getDuplicateDrivers = '';
        //public static getDirverSecurity: string = '';
        //public static getDirverFacilities: string = '';
        URLHelper.saveDriverDetails = '';
        URLHelper.checkUserNameAvailability = '';
        URLHelper.SuggesstedUserName = '';
        URLHelper.addDriverFacility = '';
        URLHelper.assingDriverToFacility = '';
        return URLHelper;
    }());
    driverMaintenanceFormModule.URLHelper = URLHelper;
    var DefaultVariables = (function () {
        function DefaultVariables() {
        }
        DefaultVariables.PasswordMinLength = 0;
        return DefaultVariables;
    }());
    driverMaintenanceFormModule.DefaultVariables = DefaultVariables;
    (function (DriverFacilityStatus) {
        DriverFacilityStatus[DriverFacilityStatus["None"] = 0] = "None";
        DriverFacilityStatus[DriverFacilityStatus["New"] = 1] = "New";
        DriverFacilityStatus[DriverFacilityStatus["Updated"] = 2] = "Updated";
        DriverFacilityStatus[DriverFacilityStatus["Deleted"] = 3] = "Deleted";
    })(driverMaintenanceFormModule.DriverFacilityStatus || (driverMaintenanceFormModule.DriverFacilityStatus = {}));
    var DriverFacilityStatus = driverMaintenanceFormModule.DriverFacilityStatus;
    var DAAccessLevel = (function () {
        function DAAccessLevel() {
        }
        DAAccessLevel.PrimaryFacility = 'PrimaryFacility';
        DAAccessLevel.SecondaryFacility = 'SecondaryFacility';
        DAAccessLevel.Rover = 'Rover';
        return DAAccessLevel;
    }());
    driverMaintenanceFormModule.DAAccessLevel = DAAccessLevel;
    var driverMaintenanceForm = (function () {
        function driverMaintenanceForm() {
            this.drSteps = null;
            this.glbDriverID = 0;
            //this.drSteps = $("#wzAddDriver");
            this.glbDriverID = 0;
        }
        driverMaintenanceForm.prototype.addEditDriver = function (driverID) {
            this.glbDriverID = driverID;
            var request = {
                Id: driverID
            };
            this.sendRequestForLoadUserInfo(request);
        };
        driverMaintenanceForm.prototype.initAddDriverWizard = function (isEdit) {
            var oJQueryStepsOptions = {
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                autoFocus: true,
                enableCancelButton: true,
                enableAllSteps: isEdit,
                labels: { cancel: "Close" },
                onStepChanging: this.onStepChanging.bind(this),
                onFinished: this.onFinishedDriverForm.bind(this),
                onStepChanged: this.onStepChanged.bind(this),
                onCanceled: this.onCanceled.bind(this),
            };
            this.drSteps.steps(oJQueryStepsOptions);
        };
        driverMaintenanceForm.prototype.onCanceled = function () {
            $(".modal-box1, .modal-overlay").fadeOut(500, function () {
                //Since we have two model-boxes in UI, make sure hide respected popup only
                $(".modal-box", $('#frmSaveDriverDetails')).hide();
                // console.log('cancelled');
                $('#wzDrUserInformation').empty();
                $('#wzDrSecurity').empty();
                $('#wzDrFacilityAssignment').empty();
                //$('#tBodyDriverFacilityList').remove('tr');
            });
        };
        driverMaintenanceForm.prototype.onStepChanging = function (event, currentIndex, newIndex) {
            var currentObject = this.drSteps.steps("getStep", currentIndex);
            //If driver form is in add mode then only look for duplicate uses 
            if (currentIndex == 0) {
                ///client is moving from first to second step
                //VAlidate form and send return 
                var isFormValid = this.validateDriverFormFirstStep();
                if (isFormValid) {
                    if (this.glbDriverID == 0) {
                        var request = {
                            LastName: $('#driverLastName').val(),
                            StartDate: $('#StartDate').val()
                        };
                        this.sendRequestForGetDuplicateDrivers(request);
                    }
                    return true;
                }
                else
                    return false;
            }
            else if (currentObject.title == "Matching Drivers") {
                if (newIndex > currentIndex) {
                    var expectedDriver = $('input[name=UserID]:checked', $('#tblDuplicateDrivers'));
                    if (expectedDriver && expectedDriver.length > 0) {
                        var canuseredit = expectedDriver.attr('datacanuseredit');
                        var primaryfacilityname = expectedDriver.attr('dataprimaryfacilityname');
                        var primaryFacilityStoreNo = parseInt(expectedDriver.attr('dataprimaryfacilitystoreno'));
                        var selectedDriverName = expectedDriver.attr('datadriverfirstname') + " " + expectedDriver.attr('datadriverlastname');
                        var selectedDriverId = parseInt(expectedDriver.attr('datadriverid'));
                        //If driver is not associated with any facility, then ask him to do here
                        if (primaryFacilityStoreNo <= 0) {
                            var confirmmsg = selectedDriverName + " is not associated with any facility, do you want to assing him to current facility and continue to edit?";
                            facilityApp.site.showConfrim(confirmmsg, this.assingDriverToThisFacilityAndEdit.bind(this, selectedDriverId, selectedDriverName), this.callbackCancelAssingDriverToThisFacilityAndEdit.bind(this, selectedDriverName));
                        }
                        else if (canuseredit == "False") {
                            facilityApp.site.showError("This driver is associated with '" + primaryfacilityname + "', Please contact '" + primaryfacilityname + "' OM to edit '" + selectedDriverName + "' details.");
                        }
                        else {
                            var confirmmsg = "Are you sure you want to edit " + selectedDriverName;
                            facilityApp.site.showConfrimWithOkCallback(confirmmsg, this.okCallbackToEditDriver.bind(this, selectedDriverId, selectedDriverName));
                        }
                        expectedDriver.prop('checked', false);
                        return false;
                    }
                }
                else
                    return true;
            }
            else if (currentObject.title == "Security" && currentIndex < newIndex) {
            }
            return true;
        };
        driverMaintenanceForm.prototype.okCallbackToEditDriver = function (selectedDriverId, selectedDriverName) {
            this.onCanceled();
            var thStartDate = $('#StartDate').val();
            var driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
            driverMaintenanceFormObjLocal.showDriverSchedulePopup(selectedDriverId, selectedDriverName, thStartDate, driverScheduleModule.editDriverOptions.driverInformation, this);
        };
        driverMaintenanceForm.prototype.callbackCancelAssingDriverToThisFacilityAndEdit = function (driverName) {
            facilityApp.site.showError("Without assinging " + driverName + " to any facility, you can not edit his details. Please contact administrator for further help.");
            //DE-SELECT ALL CHECK BOXES
            var expectedDriver = $('input[name=UserID]:checked', $('#tblDuplicateDrivers'));
            expectedDriver.prop('checked', false);
        };
        driverMaintenanceForm.prototype.assingDriverToThisFacilityAndEdit = function (driverId, driverName) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.assingDriverToFacility,
                type: 'GET',
                data: { driverId: driverId, driverName: driverName },
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackAssingDriverToThisFacilityAndEdit.bind(this))
                .fail(this.errorCallbackAssingDriverToThisFacilityAndEdit);
        };
        driverMaintenanceForm.prototype.successCallbackAssingDriverToThisFacilityAndEdit = function (respose) {
            if (respose.success == false) {
                facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
            }
            else {
                this.onCanceled();
                var thStartDate = $('#StartDate').val();
                var driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
                driverMaintenanceFormObjLocal.showDriverSchedulePopup(respose.data.driverId, respose.data.driverName, thStartDate, driverScheduleModule.editDriverOptions.driverInformation, this);
            }
        };
        driverMaintenanceForm.prototype.errorCallbackAssingDriverToThisFacilityAndEdit = function () {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        };
        driverMaintenanceForm.prototype.onStepChanged = function (event, currentIndex) {
            var currentObject = this.drSteps.steps("getStep", currentIndex);
            if (currentObject.title == "Security") {
                //let UserName = $('#driverFirstName').val() + $('#driverLastName').val();
                //$('#txtUserName').val(UserName);
                this.sendRequestToSuggesstedUserName($('#driverFirstName').val(), $('#driverLastName').val());
            }
        };
        driverMaintenanceForm.prototype.onFinishedDriverForm = function (event, currentIndex) {
            //var StoreAccess = $('select[id*="DAAccessLevel"] option:selected', $('#tBodyDriverFacilityList')).filter(function (index) {
            //    if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() != DriverFacilityStatus.Deleted) {
            //        if ($('#DriverFacility\\[' + index + '\\]\\.DAAccessLevelDisp').text().trim() == 'PrimaryFacility' && $('#DriverFacility\\[' + index + '\\]\\.DriverEndDateDisp').text() == '')
            //            return this;
            //    }
            //});
            //if (StoreAccess.length == 0) {
            //    facilityApp.site.showError('• At least one primary facility should be associated with drive without end date !\n');
            //}
            //else {
            var isUserNameAndPasswordValid = this.validateUserNameAndPasswordBeforeMoving();
            if (isUserNameAndPasswordValid) {
                //write driver information saving funcationality 
                var request = {};
                //$('select[id*="DAAccessLevel"]', $('#tBodyDriverFacilityList')).each(function (idx, select) { $(select).prop("disabled", false); });
                //$('select[id*="StoreNo"]', $('#tBodyDriverFacilityList')).each(function (idx, select) { $(select).prop("disabled", false); });
                this.sendRequestToSaveDriverDetails(request);
            }
            //} else {
            //    this.imgUserNameNotAvailable.style.visibility = 'visible';
            //    this.imgUserNameNotAvailable.style.display = 'block';
            //    this.imgUserNameAvailable.style.visibility = 'hidden';
            //    this.imgUserNameAvailable.style.display = 'none';
            //    $('#hfCheckBtnClicked').val('false');
            //}
            //}
        };
        //#region Add Driver Frist Step Start - LoadUserInformation
        driverMaintenanceForm.prototype.sendRequestForLoadUserInfo = function (postData) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.loadUserInfo,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackLoadUserInfo.bind(this))
                .fail(this.errorCallbackLoadUserInfo);
        };
        driverMaintenanceForm.prototype.successCallbackLoadUserInfo = function (result) {
            if (result == "error") {
                facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
            }
            else {
                //$('#wzDrUserInformation').html(result);
                $('#dvDriverMaintenanceForm').html(result);
                ///After userform loads into UI, then bind Widzet
                this.drSteps = $("#wzAddDriver");
                var UserId = $('#PK_UserId').val();
                if (UserId == 0)
                    this.initAddDriverWizard(false);
                else {
                    this.initAddDriverWizard(true);
                    $('.steps li').each(function (idx, li) {
                        if (!$(li).hasClass("current"))
                            $(li).addClass("done");
                    });
                }
                //After widget binds, then bind all events for the form buttons and links
                this.bindAddEditDriverFormEvents();
                //this.drSteps.steps({enableAllSteps: true});
                //fixed header
                $(".new-fixed-header", this.drSteps).tableHeadFixer();
            }
            $.blockUI;
        };
        driverMaintenanceForm.prototype.bindAddEditDriverFormEvents = function () {
            ///Driver Information tab events
            //this.bindDatePicker();
            //Driver security tab events
            this.attachClickToBtnCheckUsernameAvailable();
            //Driver Facility assignment events
            //this.bindEventsForAddEditDriverFacilities();
        };
        //bindDatePicker() {
        //    this.imgStartDateIcon = document.getElementById('driverAddStartDateIcon');
        //    let txtStartDate = $('#driverAddStartDate');
        //    this.imgStartDateIcon.onclick = (e) => { e.preventDefault(); txtStartDate.focus(); }
        //    txtStartDate.datepicker({ minDate: 0 });
        //}
        driverMaintenanceForm.prototype.errorCallbackLoadUserInfo = function (result) {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        };
        driverMaintenanceForm.prototype.validateDriverFormFirstStep = function () {
            ///Validation needs to be implemented
            var dvError = $('#lblError');
            var isValid = true;
            //validate first name
            if (String.isNullOrEmpty($('#driverFirstName').val())) {
                dvError.text('Please enter first name.');
                $('#driverFirstName').css("border", "1px solid red");
                $("#driverFirstName").attr('title', 'Please enter first name.');
                $('#driverFirstName').focus();
                isValid = false;
            }
            else {
                $('#driverFirstName').css("border", "1px #bababa solid");
            }
            //validate last name
            if (String.isNullOrEmpty($('#driverLastName').val())) {
                dvError.text('Please enter last name.');
                $('#driverLastName').css("border", "1px solid red");
                $("#driverLastName").attr('title', 'Please enter last name.');
                $('#driverLastName').focus();
                isValid = false;
            }
            else {
                $('#driverLastName').css("border", "1px #bababa solid");
            }
            //Validate Email
            if (String.isNullOrEmpty($('#driverEmail').val()) || !this.validateEmail($('#driverEmail').val())) {
                dvError.text('Please enter email address.');
                $('#driverEmail').css("border", "1px solid red");
                $("#driverEmail").attr('title', 'Please enter valid email address.');
                $('#driverEmail').focus();
                isValid = false;
            }
            else {
                $('#driverEmail').css("border", "1px #bababa solid");
            }
            //validate facility
            //if ($('#PrimaryFacilityID').val() == '') {
            //    dvError.text('Please select the Primary Facility.');
            //    $('#PrimaryFacilityID').focus();
            //    return false;
            //}
            //validate license
            if ($('#LicenseClassID').val() == '') {
                dvError.text('Please select the license class.');
                $('#LicenseClassID').css("border", "1px solid red");
                $("#LicenseClassID").attr('title', 'Please select the license class.');
                $('#LicenseClassID').focus();
                isValid = false;
            }
            else {
                $('#LicenseClassID').css("border", "1px #bababa solid");
            }
            //validate driver status.
            if ($('#StatusID').val() == '') {
                dvError.text('Please select the driver status.');
                $('#StatusID').css("border", "1px solid red");
                $("#StatusID").attr('title', 'Please select the driver status.');
                $('#StatusID').focus();
                isValid = false;
            }
            else {
                $('#StatusID').css("border", "1px #bababa solid");
            }
            dvError.text('');
            return isValid;
        };
        //#endregion Add Driver Frist Step end
        driverMaintenanceForm.prototype.validateEmail = function (email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
            ;
        };
        //#region Add Driver Second Step Start - GetDuplicateDrivers
        driverMaintenanceForm.prototype.sendRequestForGetDuplicateDrivers = function (postData) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.getDuplicateDrivers,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackGetDuplicateDrivers.bind(this))
                .fail(this.errorCallbackGetDuplicateDrivers);
        };
        driverMaintenanceForm.prototype.successCallbackGetDuplicateDrivers = function (result) {
            var secondstep = this.drSteps.steps("getStep", 1);
            var machingdriversExist = false;
            if (secondstep.title == "Matching Drivers") {
                machingdriversExist = true;
            }
            if (result.success == true && result.data == 'noduplicatedriversfound') {
                //Navigate to next tab
                if (machingdriversExist) {
                    this.drSteps.steps("next");
                    this.drSteps.steps("remove", 1);
                }
            }
            else if (result.success == true) {
                if (!machingdriversExist) {
                    this.drSteps.steps("insert", 1, {
                        title: "Matching Drivers",
                        id: "MatchingDrivers",
                        content: '<div id="wzDrDuplicateDrivers"></div>'
                    });
                }
                $('#wzDrDuplicateDrivers').html(result.data);
                this.drSteps.steps("setStep", 1);
            }
            else {
                facilityApp.site.showError('Error occurred while checking duplicate driver information. Please reload the page and try again!');
            }
            $.blockUI;
        };
        driverMaintenanceForm.prototype.errorCallbackGetDuplicateDrivers = function (result) {
            facilityApp.site.showError('Error occurred while checking duplicate driver information. Please reload the page and try again!');
        };
        //#endregion Add Driver Second Step end GetDuplicateDrivers
        driverMaintenanceForm.prototype.attachClickToBtnCheckUsernameAvailable = function () {
            //this.btnCheckUsernameAvailable = $('#btnCheckUsernameAvailable');
            ////console.log(this.glbDriverID);
            //if (this.glbDriverID <= 0) {
            //    this.btnCheckUsernameAvailable.show();
            //    this.btnCheckUsernameAvailable.click((e) => { e.preventDefault(); this.checkUserNamePasswordEmptyAndMakeCall(); });
            //} else {
            //    this.btnCheckUsernameAvailable.hide();
            //}
            var parent = this;
            $('#txtUserName').bind("change", function (event) {
                event.preventDefault();
                parent.checkUserNamePasswordEmptyAndMakeCall();
                //$('#hfCheckBtnClicked').val('false');
                this.imgUserNameNotAvailable = document.getElementById('imgUserNameNotAvailable');
                this.imgUserNameAvailable = document.getElementById('imgUserNameAvailable');
                this.imgUserNameAvailable.style.visibility = 'hidden';
                this.imgUserNameNotAvailable.style.visibility = 'hidden';
                this.imgUserNameAvailable.style.display = 'none';
                this.imgUserNameNotAvailable.style.display = 'none';
                // $('#btnCheckUsernameAvailable').show();
                $('input[name=SuggesstestName]:checked').prop('checked', false);
            });
            $('#txtPassword').bind("change", function (event) {
                if ($('#txtPassword').length > 0) {
                    $('#txtPassword').css("border", "1px #bababa solid");
                }
                else {
                    $('#txtPassword').css("border", "red");
                }
            });
        };
        driverMaintenanceForm.prototype.validateUserNameAndPasswordBeforeMoving = function () {
            var errMsgArray = new Array();
            var dvError = $('#lblDriverSecurityError');
            var UserName = $('#txtUserName');
            var Password = $('#txtPassword');
            var isValid = true;
            if (String.isNullOrEmpty(UserName.val())) {
                //dvError.text('Please enter Username.');
                UserName.css("border", "1px solid red");
                UserName.attr('title', 'Please enter Username.');
                UserName.focus();
                isValid = false;
                errMsgArray.push('Please enter Username.');
            }
            else if (String.hasWhiteSpace(UserName.val())) {
                //dvError.text('Username should not contain spaces.');
                UserName.css("border", "1px solid red");
                UserName.attr('title', 'Username should not contain spaces.');
                UserName.focus();
                errMsgArray.push('Username should not contain spaces.');
                isValid = false;
            }
            else {
                UserName.css("border", "1px #bababa solid");
            }
            if (String.isNullOrEmpty(Password.val())) {
                //dvError.text('Please enter Password.');
                Password.css("border", "1px solid red");
                Password.attr('title', 'Please enter Password.');
                errMsgArray.push('Please enter Password.');
                Password.focus();
                isValid = false;
            }
            else if (String.hasWhiteSpace(Password.val())) {
                //dvError.text('Password should not contain spaces.');
                Password.css("border", "1px solid red");
                Password.attr('title', 'Password should not contain spaces.');
                errMsgArray.push('Password should not contain spaces.');
                Password.focus();
                isValid = false;
            }
            else if (Password.val().length < driverMaintenanceFormModule.DefaultVariables.PasswordMinLength) {
                var errorMsg = 'Password must be at least ' + driverMaintenanceFormModule.DefaultVariables.PasswordMinLength + ' characters.';
                dvError.html(errorMsg);
                Password.css("border", "1px solid red");
                Password.attr('title', errorMsg);
                errMsgArray.push(errorMsg);
                Password.focus();
                isValid = false;
            }
            else {
                Password.css("border", "1px #bababa solid");
            }
            if ($('#hfCheckBtnClicked').val() == "false") {
                if (String.isNullOrEmpty(UserName.val())) {
                    //dvError.text('Please enter Username.');
                    UserName.css("border", "1px solid red");
                    UserName.attr('title', 'Please enter Username.');
                    errMsgArray.push('Please enter Username.');
                    UserName.focus();
                    isValid = false;
                }
                else if (String.hasWhiteSpace(UserName.val())) {
                    //dvError.text('Username should not contain spaces.');
                    $('#txtUserName').css("border", "1px solid red");
                    $("#txtUserName").attr('title', 'Username should not contain spaces.');
                    errMsgArray.push('Username should not contain spaces.');
                    $('#txtUserName').focus();
                    isValid = false;
                }
                else {
                    //dvError.text('Please check for available Username.');
                    $('#txtUserName').css("border", "1px solid red");
                    $("#txtUserName").attr('title', 'Please check for available Username.');
                    errMsgArray.push('Please check for available Username.');
                    $('#txtUserName').focus();
                    isValid = false;
                }
            }
            if (isValid) {
                dvError.html('');
            }
            else {
                facilityApp.site.showError(errMsgArray.join("<br/>"));
            }
            return isValid;
        };
        driverMaintenanceForm.prototype.checkUserNamePasswordEmptyAndMakeCall = function () {
            var dvError = $('#lblDriverSecurityError');
            var UserName = $('#txtUserName').val();
            if (String.isNullOrEmpty(UserName)) {
                //dvError.text('Please enter user name.');
                $('#txtUserName').css("border", "1px solid red");
                $("#txtUserName").attr('title', 'Please enter Username.');
                $('#txtUserName').focus();
                return false;
            }
            else if (String.hasWhiteSpace(UserName)) {
                //dvError.text('User name should not contain spaces.');
                $('#txtUserName').css("border", "1px solid red");
                $("#txtUserName").attr('title', 'Username should not contain spaces.');
                $('#txtUserName').focus();
                return false;
            }
            else {
                dvError.text('');
                this.sendRequestToCheckUserNameAvailable(UserName);
            }
        };
        driverMaintenanceForm.prototype.sendRequestToCheckUserNameAvailable = function (userName) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.checkUserNameAvailability,
                type: 'GET',
                data: { UserName: userName },
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackcheckUserName.bind(this))
                .fail(this.errorCallbackcheckUserName);
        };
        driverMaintenanceForm.prototype.sendRequestToSuggesstedUserName = function (FirstName, LastName) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.SuggesstedUserName,
                type: 'GET',
                data: { FirstName: FirstName, LastName: LastName },
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackSuggesstedUserName.bind(this))
                .fail(this.errorCallbackSuggesstedUserName);
        };
        driverMaintenanceForm.prototype.successCallbackSuggesstedUserName = function (result) {
            this.imgUserNameNotAvailable = document.getElementById('imgUserNameNotAvailable');
            this.imgUserNameAvailable = document.getElementById('imgUserNameAvailable');
            if (result.success == true) {
                $('#hdrSuggestUserName').show();
                //  $('#txtUserName').val(result.message);
                $('#hfCheckBtnClicked').val('true');
                $('#txtUserName').css("border", "1px #bababa solid");
                // $('#btnCheckUsernameAvailable').hide();
                this.imgUserNameAvailable.style.visibility = 'hidden';
                this.imgUserNameNotAvailable.style.visibility = 'hidden';
                this.imgUserNameAvailable.style.display = 'none';
                this.imgUserNameNotAvailable.style.display = 'none';
                $('#suggestedName').html('');
                var parent_1 = this;
                $.each(JSON.parse(result.data), function (key, value) {
                    if (key == 0) {
                        $('#txtUserName').val(value);
                    }
                    var label = document.createElement("label");
                    var radio = document.createElement("input");
                    radio.type = "radio";
                    radio.name = "SuggesstestName";
                    radio.value = value;
                    if (key == 0)
                        radio.checked = true;
                    radio.onchange = function (e) {
                        e.preventDefault();
                        var currentTarget = $(e.currentTarget);
                        $('#txtUserName').val(currentTarget.val());
                        $('#hfCheckBtnClicked').val('true');
                        parent_1.imgUserNameNotAvailable = document.getElementById('imgUserNameNotAvailable');
                        parent_1.imgUserNameAvailable = document.getElementById('imgUserNameAvailable');
                        parent_1.imgUserNameAvailable.style.visibility = 'hidden';
                        parent_1.imgUserNameNotAvailable.style.visibility = 'hidden';
                        parent_1.imgUserNameAvailable.style.display = 'none';
                        parent_1.imgUserNameNotAvailable.style.display = 'none';
                    };
                    radio.style.marginRight = "5px";
                    label.appendChild(radio);
                    label.appendChild(document.createTextNode(value));
                    var radio_home = document.getElementById("suggestedName");
                    radio_home.appendChild(label);
                    // document.getElementById("myDiv").style.marginRight = "50px";
                });
            }
            else {
                // $('#btnCheckUsernameAvailable').show();
                $('#hfCheckBtnClicked').val('false');
            }
        };
        driverMaintenanceForm.prototype.errorCallbackSuggesstedUserName = function (result) {
            facilityApp.site.showError('Error occurred while checking username availability. Please reload the page and try again!');
        };
        driverMaintenanceForm.prototype.successCallbackcheckUserName = function (result) {
            this.imgUserNameNotAvailable = document.getElementById('imgUserNameNotAvailable');
            this.imgUserNameAvailable = document.getElementById('imgUserNameAvailable');
            if (result.success == false) {
                this.imgUserNameNotAvailable.style.visibility = 'visible';
                this.imgUserNameNotAvailable.style.display = 'block';
                this.imgUserNameAvailable.style.visibility = 'hidden';
                this.imgUserNameAvailable.style.display = 'none';
                $('#hfCheckBtnClicked').val('false');
            }
            else {
                this.imgUserNameAvailable.style.visibility = 'visible';
                this.imgUserNameNotAvailable.style.visibility = 'hidden';
                this.imgUserNameAvailable.style.display = 'block';
                this.imgUserNameNotAvailable.style.display = 'none';
                $('#hfCheckBtnClicked').val('true');
                $('#txtUserName').css("border", "1px #bababa solid");
            }
            $.blockUI;
        };
        driverMaintenanceForm.prototype.errorCallbackcheckUserName = function (result) {
            facilityApp.site.showError('Error occurred while checking username availability. Please reload the page and try again!');
        };
        //#endregion Add Driver Third Step end GetDirverSecurity
        driverMaintenanceForm.prototype.bindEventsForAddEditDriverFacilities = function () {
            var _this = this;
            $(".icon-driverfacility-add", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineAddDriverFacility(e); });
            $(".icon-driverfacility-edit", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineEditDriverFacility(e); });
            $(".icon-driverfacility-delete", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineDeleteDriverFacility(e); });
            $(".icon-driverfacility-save", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineSaveDriverFacility(e); });
            $(".icon-driverfacility-cancel", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineCacnelDriverFacility(e); });
            $(".icon-driverfacility-StartDate", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineStartDateCalendar(e); });
            $(".icon-driverfacility-EndDate", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineEndDateCalendar(e); });
            $(".txt-driverfacility-StartDate", $('#wzDrFacilityAssignment')).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
            $(".txt-driverfacility-EndDate", $('#wzDrFacilityAssignment')).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
            $(".dd-driverfacility-DAAccessLevel", $('#wzDrFacilityAssignment')).change(function (e) { e.preventDefault(); _this.inlineDAAccessLevelChange(e); });
        };
        driverMaintenanceForm.prototype.inlineDAAccessLevelChange = function (evt) {
            var currentTarget = $(evt.currentTarget);
            if (currentTarget.val() == DAAccessLevel.Rover) {
                //console.log(currentTarget.val());
                var thisTr = $(evt.currentTarget).closest('tr');
                var txtEndDate = $(thisTr).find('.txt-driverfacility-EndDate');
                if (txtEndDate.val().length <= 0) {
                    txtEndDate.val((new Date()).toDateMMDDYYYY());
                }
            }
        };
        driverMaintenanceForm.prototype.dateBeforeShow = function (ele, inst) {
            var canEdit = $(ele).attr('data-canedit');
            return canEdit == "True";
        };
        driverMaintenanceForm.prototype.bindEventsForAddEditDriverFacilitiesForTr = function (thisrow) {
            var _this = this;
            $(".icon-driverfacility-edit", thisrow).click(function (e) { e.preventDefault(); _this.inlineEditDriverFacility(e); });
            $(".icon-driverfacility-delete", thisrow).click(function (e) { e.preventDefault(); _this.inlineDeleteDriverFacility(e); });
            $(".icon-driverfacility-save", thisrow).click(function (e) { e.preventDefault(); _this.inlineSaveDriverFacility(e); });
            $(".icon-driverfacility-cancel", thisrow).click(function (e) { e.preventDefault(); _this.inlineCacnelDriverFacility(e); });
            $(".icon-driverfacility-StartDate", thisrow).click(function (e) { e.preventDefault(); _this.inlineStartDateCalendar(e); });
            $(".icon-driverfacility-EndDate", thisrow).click(function (e) { e.preventDefault(); _this.inlineEndDateCalendar(e); });
            $(".txt-driverfacility-StartDate", thisrow).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
            $(".txt-driverfacility-EndDate", thisrow).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
            $(".dd-driverfacility-DAAccessLevel", thisrow).change(function (e) { e.preventDefault(); _this.inlineDAAccessLevelChange(e); });
        };
        driverMaintenanceForm.prototype.inlineAddDriverFacility = function (evt) {
            ///Get total number of facilitiy rows in the UI, when you want to add another facility, please get total facilities and send count
            ///This count will be used to build new "tr" by using next index
            var inEditMode = $('span[class*="icon-driverfacility-save"]', $('#tBodyDriverFacilityList')).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                var requestFA = {
                    Index: $('#tBodyDriverFacilityList').find('tr').length
                };
                this.sendRequestForGetAddDirverFacility(requestFA);
            }
            else {
                facilityApp.site.showError('Please save / discard existing changes!');
            }
        };
        /*Add User Facility START*/
        driverMaintenanceForm.prototype.sendRequestForGetAddDirverFacility = function (postData) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.addDriverFacility,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackGetAddDirverFacility.bind(this))
                .fail(this.errorCallbackGetAddDirverFacility);
        };
        driverMaintenanceForm.prototype.successCallbackGetAddDirverFacility = function (result) {
            if (result.success == false) {
                facilityApp.site.showError('Error occurred while fetching driver facilities information. Please reload the page and try again!');
            }
            else {
                $('#tBodyDriverFacilityList').prepend(result.data);
                //Get current TR index and use it to clieck edit button
                var trBlock = $(result.data);
                var rowIndex = $(trBlock[0]).attr("data-rowindex");
                this.bindEventsForAddEditDriverFacilitiesForTr($('#DriverFacility_' + rowIndex));
                $(".icon-driverfacility-edit", $('#DriverFacility_' + rowIndex)).click();
            }
            $.blockUI;
        };
        driverMaintenanceForm.prototype.errorCallbackGetAddDirverFacility = function (result) {
            facilityApp.site.showError('Error occurred while getting driver facilities information. Please reload the page and try again!');
        };
        /*Add User Facility END*/
        driverMaintenanceForm.prototype.inlineEditDriverFacility = function (evt) {
            var inEditMode = $('span[class*="icon-driverfacility-save"]', $('#tBodyDriverFacilityList')).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                var currentTarget = $(evt.currentTarget);
                //let driverFacilityId = currentTarget.attr('data-driverfacility');
                var thisTr = $(evt.currentTarget).closest('tr');
                var index = currentTarget.attr('data-index');
                $('.viewData', thisTr).hide();
                $('.editData', thisTr).show();
                $('.icon-driverfacility-edit', thisTr).hide();
                $('.icon-driverfacility-delete', thisTr).hide();
                currentTarget.hide();
                $(".actions ul li:nth-child(3)").addClass("disabled").attr("aria-disabled", "true").addClass("disable-anchor-click");
                $('.icon-driverfacility-save', thisTr).show();
                $('.icon-driverfacility-cancel', thisTr).show();
                var rowstartDate = new Date($('#DriverFacility\\[' + index + '\\]\\.DriverStartDate').val());
                var now = new Date();
                now.setHours(0, 0, 0, 0);
                if (rowstartDate < now) {
                    // selected date is in the past
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNo').prop('disabled', true);
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel').prop('disabled', true);
                }
                else {
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNo').prop('disabled', false);
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel').prop('disabled', false);
                }
            }
            else {
                facilityApp.site.showError('Please save / discard existing changes!');
            }
        };
        driverMaintenanceForm.prototype.inlineCacnelDriverFacility = function (evt) {
            var currentTarget = $(evt.currentTarget);
            //let driverFacilityId = currentTarget.attr('data-driverfacility');
            var thisTr = $(evt.currentTarget).closest('tr');
            var index = currentTarget.attr('data-index');
            $('.viewData', thisTr).show();
            $('.editData', thisTr).hide();
            $('.icon-driverfacility-edit', thisTr).show();
            $('.icon-driverfacility-delete', thisTr).show();
            currentTarget.hide();
            $('.icon-driverfacility-save', thisTr).hide();
            $('.icon-driverfacility-cancel', thisTr).hide();
            if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() == 'New') {
                thisTr.remove();
            }
            var inEditMode = $('span[class*="icon-driverfacility-save"]', $('#tBodyDriverFacilityList')).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                $(".actions ul li:nth-child(3)").attr("aria-disabled", "false").removeClass("disabled").removeClass("disable-anchor-click");
            }
        };
        driverMaintenanceForm.prototype.inlineSaveDriverFacility = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = currentTarget.closest('tr');
            //let driverFacilityId = currentTarget.attr('data-driverfacility');
            var index = currentTarget.attr('data-index');
            $('.viewData', thisTr).show();
            $('.editData', thisTr).hide();
            ///Here need to update updated values to show updated content
            $('.icon-driverfacility-delete', thisTr).show();
            $('.icon-driverfacility-edit', thisTr).show();
            $('.icon-driverfacility-cancel', thisTr).hide();
            currentTarget.hide();
            var inEditMode = $('span[class*="editData"]', $('#tBodyDriverFacilityList')).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                $(".actions ul li:nth-child(3)").attr("aria-disabled", "false").removeClass("disabled").removeClass("disable-anchor-click");
            }
            ///Check any data is updated or not, if the data is updated then update status and respected fields
            var isValid = this.inlineValidateEditRow(evt); //-- GURU this needs to implemented
            if (isValid) {
                var isRowUpdated = this.inlineIsRowUpdated(evt); //-- GURU this needs to implemented
                if (isRowUpdated) {
                    var selectedStore = $("option:selected", $('#DriverFacility\\[' + index + '\\]\\.StoreNo'));
                    $('#DriverFacility\\[' + index + '\\]\\.DFStatus').val(DriverFacilityStatus.Updated);
                    $('#DriverFacility\\[' + index + '\\]\\.StoreRowID', thisTr).val(selectedStore.attr('data-storerowid'));
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNoDisp', thisTr).html(selectedStore.attr('data-storeno'));
                    $('#DriverFacility\\[' + index + '\\]\\.FriendlyNameDisp').html(selectedStore.attr('data-dispstorename'));
                    var selectedStoreAccess = $("option:selected", $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel'));
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevelDisp').html(selectedStoreAccess.attr('data-displayname'));
                    $('#DriverFacility\\[' + index + '\\]\\.DriverStartDateDisp', thisTr).html($('#DriverFacility\\[' + index + '\\]\\.DriverStartDate').val());
                    $('#DriverFacility\\[' + index + '\\]\\.DriverEndDateDisp', thisTr).html($('#DriverFacility\\[' + index + '\\]\\.DriverEndDate').val());
                }
            }
            else {
                this.inlineEditDriverFacility(evt);
            }
        };
        driverMaintenanceForm.prototype.inlineIsRowUpdated = function (evt) {
            return true;
        };
        driverMaintenanceForm.prototype.inlineValidateEditRow = function (evt) {
            var returnValue;
            var strMessage = '';
            var currentTarget = $(evt.currentTarget);
            var thisTr = currentTarget.closest('tr');
            var idx = currentTarget.attr('data-index');
            var startDate = new Date($('#DriverFacility\\[' + idx + '\\]\\.DriverStartDate').val());
            var endDate = new Date($('#DriverFacility\\[' + idx + '\\]\\.DriverEndDate').val());
            returnValue = true;
            if (startDate > endDate) {
                strMessage = '• End Date should be greater than start date!\n';
                returnValue = false;
            }
            var selectedStoreAccess = $("option:selected", $('#DriverFacility\\[' + idx + '\\]\\.DAAccessLevel'));
            var StoreAccess = selectedStoreAccess.attr('data-displayname');
            var selectedStore = $("option:selected", $('#DriverFacility\\[' + idx + '\\]\\.StoreNo'));
            var storeno = selectedStore.attr('data-storerowid');
            var existingStoreAccess = [];
            if (StoreAccess == DAAccessLevel.Rover) {
                var parent_2 = this;
                var duplicatesStoreAccess = $('select[id*="DAAccessLevel"] option:selected', $('#tBodyDriverFacilityList')).filter(function (index) {
                    if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() != DriverFacilityStatus.Deleted) {
                        if (index != parseInt(idx)) {
                            var value = $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevelDisp').text().trim();
                            if (StoreAccess == value) {
                                var otherStartDate = $('#DriverFacility\\[' + index + '\\]\\.DriverStartDateDisp').text();
                                var otherEndDate = $('#DriverFacility\\[' + index + '\\]\\.DriverEndDateDisp').text();
                                var currentStartDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverStartDate').val();
                                var currentEndDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverEndDate').val();
                                if (parent_2.dateCheck(otherStartDate, otherEndDate, currentStartDate, currentEndDate)) {
                                    return $(this);
                                }
                            }
                        }
                    }
                });
                //console.log(existingStoreAccess);
                if (duplicatesStoreAccess.length != 0) {
                    strMessage = strMessage + '• Rover Facility already selected for selected date!\n';
                    returnValue = false;
                }
            }
            else if (StoreAccess == DAAccessLevel.PrimaryFacility) {
                var parent_3 = this;
                var duplicatesStoreAccess_1 = $('select[id*="DAAccessLevel"] option:selected', $('#tBodyDriverFacilityList')).filter(function (index) {
                    if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() != DriverFacilityStatus.Deleted) {
                        if (index != parseInt(idx)) {
                            var value = $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevelDisp').text().trim();
                            if (StoreAccess == value) {
                                var otherStartDate = $('#DriverFacility\\[' + index + '\\]\\.DriverStartDateDisp').text();
                                var otherEndDate = $('#DriverFacility\\[' + index + '\\]\\.DriverEndDateDisp').text();
                                var currentStartDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverStartDate').val();
                                var currentEndDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverEndDate').val();
                                if (parent_3.dateCheck(otherStartDate, otherEndDate, currentStartDate, currentEndDate)) {
                                    return $(this);
                                }
                            }
                        }
                    }
                });
                if (duplicatesStoreAccess_1.length != 0) {
                    strMessage = strMessage + '• Primary Facility already selected for selected date!\n';
                    returnValue = false;
                }
            }
            var existing = [];
            var parentScope = this;
            var duplicates = $('input[id*="StoreRowID"]', $('#tBodyDriverFacilityList')).filter(function (index) {
                if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() != DriverFacilityStatus.Deleted) {
                    if (index != parseInt(idx)) {
                        var value = $('#DriverFacility\\[' + index + '\\]\\.StoreRowID').val();
                        if (storeno == value) {
                            var otherStartDate = $('#DriverFacility\\[' + index + '\\]\\.DriverStartDateDisp').text();
                            var otherEndDate = $('#DriverFacility\\[' + index + '\\]\\.DriverEndDateDisp').text();
                            var currentStartDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverStartDate').val();
                            var currentEndDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverEndDate').val();
                            if (parentScope.dateCheck(otherStartDate, otherEndDate, currentStartDate, currentEndDate)) {
                                return $(this);
                            }
                        }
                    }
                }
            });
            if (duplicates.length != 0) {
                strMessage = strMessage + '• Selected Facility already associated for selected date!\n';
                returnValue = false;
            }
            if (returnValue == false)
                facilityApp.site.showError(strMessage);
            return returnValue;
        };
        driverMaintenanceForm.prototype.dateCheck = function (existingStartDate, existingEndDate, currentStartDate, currentEndDate) {
            var eSDate, eEDate, cSDate, cEDate;
            if (currentEndDate == '') {
                currentEndDate = currentStartDate;
                cEDate = Date.parse(currentEndDate); //tEndA
                cEDate.setFullYear(cEDate.getFullYear() + 10);
            }
            else
                cEDate = Date.parse(currentEndDate); //tEndA    
            if (existingEndDate == '') {
                existingEndDate = existingStartDate;
                eEDate = Date.parse(existingEndDate); //tEndB
                eEDate.setFullYear(eEDate.getFullYear() + 10);
            }
            else
                eEDate = Date.parse(existingEndDate); //tEndB
            eSDate = Date.parse(existingStartDate); //tStartB
            cSDate = Date.parse(currentStartDate); //tStartA
            return (eSDate >= cSDate && eSDate <= cEDate || cSDate >= eSDate && cSDate <= eEDate);
        };
        driverMaintenanceForm.prototype.inlineDeleteDriverFacility = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = currentTarget.closest('tr');
            var index = currentTarget.attr('data-index');
            ///When you delete facility, mark DFStatus flag to "Deleted" and hide element from the UI
            $('#DriverFacility\\[' + index + '\\]\\.DFStatus').val(DriverFacilityStatus.Deleted);
            thisTr.hide();
        };
        driverMaintenanceForm.prototype.inlineStartDateCalendar = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = $(evt.currentTarget).closest('td');
            var txtStartDate = $(thisTr).find('.txt-driverfacility-StartDate');
            var canEditStartdate = $(txtStartDate).attr('data-canedit');
            if (canEditStartdate == "True") {
                $(txtStartDate).focus();
                $(txtStartDate).datepicker({ minDate: 0 });
            }
        };
        driverMaintenanceForm.prototype.inlineEndDateCalendar = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = $(evt.currentTarget).closest('td');
            var txtEndDate = $(thisTr).find('.txt-driverfacility-EndDate');
            $(txtEndDate).focus();
            $(txtEndDate).datepicker({ minDate: 0 });
        };
        //#endregion Add Driver Fourth Step end GetDriverFacilities
        //#region Add Driver Final Step Start - SaveDriverDetails
        driverMaintenanceForm.prototype.sendRequestToSaveDriverDetails = function (postData) {
            var dt = $("#frmSaveDriverDetails").serializeArray();
            var data = {};
            $.each(dt, function (index, value) {
                data[value.name] = value.value;
            });
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.saveDriverDetails,
                type: 'POST',
                traditional: true,
                data: JSON.stringify(data),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                async: true,
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackToSaveDriverDetails.bind(this))
                .fail(this.errorCallbackToSaveDriverDetails);
        };
        driverMaintenanceForm.prototype.successCallbackToSaveDriverDetails = function (result) {
            if (result.success == false) {
                facilityApp.site.showError('Error occurred while saving driver information. Please reload the page and try again!');
            }
            else {
                //Hide popscreen and reload main grid here
                $('.js-modal-close').click();
                this.mainDriverMaintenace.validateAndSubmit();
            }
            $.blockUI;
        };
        driverMaintenanceForm.prototype.errorCallbackToSaveDriverDetails = function (result) {
            facilityApp.site.showError('Error occurred while saving driver information. Please reload the page and try again!');
        };
        //#endregion Add Driver Fourth Step end GetDriverFacilities
        driverMaintenanceForm.prototype.showEditPopup = function (driverID, name, mainForm) {
            this.mainDriverMaintenace = mainForm;
            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popup').fadeIn();
            var driverHead = "";
            if (driverID > 0) {
                driverHead = "Edit " + name + " Information";
            }
            else {
                driverHead = "Add New Driver";
            }
            $('#popupAddEditDriverHead').text(driverHead);
            this.addEditDriver(driverID);
        };
        driverMaintenanceForm.prototype.initPopup = function () {
            var _this = this;
            $(".js-modal-close, .modal-overlay").click(function (e) {
                e.preventDefault();
                $(".modal-box, .modal-overlay").fadeOut(500, function () {
                    $(".modal-overlay").remove();
                });
                //console.log('close');
                _this.cleanAddEditPopup();
            });
            $(window).resize(function () {
                $(".modal-box").css({
                    top: 50,
                    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                });
            });
            $(window).resize();
        };
        driverMaintenanceForm.prototype.cleanAddEditPopup = function () {
            $('#wzDrUserInformation').empty();
            $('#wzDrSecurity').empty();
            $('#wzDrFacilityAssignment').empty();
        };
        return driverMaintenanceForm;
    }());
    driverMaintenanceFormModule.driverMaintenanceForm = driverMaintenanceForm;
})(driverMaintenanceFormModule || (driverMaintenanceFormModule = {}));
$(document).ready(function () {
    var oDriverForm = new driverMaintenanceFormModule.driverMaintenanceForm();
    oDriverForm.initPopup();
    //oDriverForm.initAddDriverWizard();
});
//# sourceMappingURL=ts.js.map