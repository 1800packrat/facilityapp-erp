﻿function initControls() {

    $("form").on("submit", function (event) {
        event.preventDefault();
        loadTransferReasonListData();
    });

    $('#modalSavehours').click(function () {
        saveASData();
    });
}

function initPopup() {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $('a[data-modal-id]').click(function (e) {
        e.preventDefault();

        $("body").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7);
        $('#popup').fadeIn();
    });

    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });
    $(window).resize(function () {
        $(".modal-box").css({
            top: 100,
            left: ($(window).width() - $(".modal-box").outerWidth()) / 2
        });
    });
    $(window).resize();
}

function showAddEditPopup(ReasonId) {
    $("#hidTransferReasonId").val(ReasonId);
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popup').fadeIn();
    $.ajax({
        url: URLHelper.GetTransferReasonbyIdUrl,
        data: {
            TransferReasonId: ReasonId
        },
        success: function (retVal) {
            if (retVal == "error") {
                alert('Error occurred while loading information. Please reload the page and try again!');
            } else {
                $('#TransferReasontxt').val(retVal);
            }
        },
        error: function (error) {
            alert('Error occurred while loading information. Please reload the page and try again!');
        }
    })

}

function loadTransferReasonListData() {
    $.ajax({
        url: URLHelper.SearchTransferReasonsUrl,
        data: {
            SearchKey: $('#SearchKey').val(),
            SortCol: $('#hidSortCol').val(),
            SortDir: $('#hidSortDir').val(),
        },
        success: function (retVal) {
            if (retVal == "error") {
                alert('Error occurred while loading information. Please reload the page and try again!');
            } else {
                $('#divTransferReasons').html(retVal);
            }
        },
        error: function (error) {
            alert('Error occurred while loading information. Please reload the page and try again!');
        }
    });
}

function SetActiveInactive(ReasonId, Status) {
    var st = 'Active';
    if (Status == 1)
        st = 'Inactive';
    var result = confirm("Are you sure you want to make it " + st + " ?");
    if (result == true) {
        $.ajax({
            url: URLHelper.SetTransferReasonActiveInactiveUrl,
            data: {
                TransferReasonId: ReasonId,
                status: Status
            },
            success: function (retVal) {
                if (retVal == "error") {
                    alert('Error occurred while loading information. Please reload the page and try again!');
                } else {
                    loadTransferReasonListData();
                }
            },
            error: function (error) {
                alert('Error occurred while loading information. Please reload the page and try again!');
            }
        });
    }
}

function SavebuttonClick() {
    if ($('#TransferReasontxt').val() != '') {
        $("#ValidateReason").html('');
        $.ajax({
            url: URLHelper.SaveTransferReasonwithIdUrl,
            data: {
                TransferReason: $('#TransferReasontxt').val(),
                TransferReasonId: $("#hidTransferReasonId").val()
            },
            success: function (retVal) {
                if (retVal == "error") {
                    alert('Error occurred while saving information. Please reload the page and try again!');
                } else {
                    alert('Truck Transfer Reason saved successfully!');
                    loadTransferReasonListData();
                    $(".modal-box, .modal-overlay").fadeOut(500, function () {
                        $(".modal-overlay").remove();
                    });
                }
            },
            error: function (error) {
                alert('Error occurred while saving information. Please reload the page and try again!');
            }
        });
    }
    else {
        $("#ValidateReason").html('Please enter reason.');
    }
}


$(document).ready(function () {
    $.ajaxSetup({ cache: false });

    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    loadTransferReasonListData();

    initControls();

    initPopup();

    //SortColumn('');
});

Date.prototype.ToDate = function () {
    return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
}

Date.prototype.ToTime = function () {
    var hr = parseInt(this.getHours());
    var ampm = hr > 11 ? 'PM' : 'AM';
    var hr = hr > 12 ? (hr - 12) : hr;
    var mm = parseInt(this.getMinutes()) > 9 ? this.getMinutes() : '0' + this.getMinutes();
    return hr + ":" + mm + " " + ampm;
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}