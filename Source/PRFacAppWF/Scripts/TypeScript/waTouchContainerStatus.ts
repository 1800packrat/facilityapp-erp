﻿/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqgrid.d.ts" />
/// <reference path="libraries\jquery.validation.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="libraries\jquery.tmpl.d.ts" />
/// <reference path="site.ts" />

module waTouchContainerStatusModule {

    export class URLHelper {
        public static searchContainerStatusList: string = '';
        public static setStatusActiveAndInactive: string = '';
        public static addEditContainerStatus: string = '';
    }

    export class getAccessLevel {
        public static isStageStatusFullAccess: string = '';
    }

    export interface localData {
        key: string;
        data: any;
    }

    export class waTouchContainerStatus {
        //private isContainerStatusFullAccess = window.isStageStatusFullAccess;

        public gridId: string = "list";
        public gridListId: string = "listPager";

        public dsGrid: JQuery = null;
        public dsGridList: JQuery = null;

        public btnSearch: HTMLElement;
        public btnReset: HTMLElement;
        public btnAddNewStatus: HTMLElement;
        public btnSaveContainerStatus: HTMLElement;

        public $searchdata: localData;

        constructor() {
            this.$searchdata = { key: "", data: {} };

            this.dsGrid = $("#" + this.gridId);

            this.btnSearch = document.getElementById('btnSearch');
            this.btnSearch.onclick = (e) => { e.preventDefault(); this.validateAndSubmit(false); }

            this.btnAddNewStatus = document.getElementById('btnAddNewStatus');
            this.btnAddNewStatus.onclick = (e) => { e.preventDefault(); this.addNewContainerStatus() };

            this.btnSaveContainerStatus = document.getElementById('modelSaveContainerStatusInfo');
            this.btnSaveContainerStatus.onclick = (e) => { e.preventDefault(); this.saveContainerStatus(); }

            this.btnReset = document.getElementById('btnReset');
            this.btnReset.onclick = (e) => { $('#txtSearchStatus').val(''); this.validateAndSubmit(true); $('#showHideButton').trigger('click'); }
        }

        validateAndSubmit(isReset): void {
            let searchText = {
                StatusName: $('#txtSearchStatus').val()
            };

            let existingDataKey = this.$searchdata.key;
            let newSearchKey = this.getSearchKey();

            ///Before we fire a query to server, look for data in inmemory for this given date range. if it exists then filter on the local data. This saves SL calls

            if (this.$searchdata.data != null && !isReset) {
                this.filterDataSetAndLoadGrid();
            }
            else {
                this.sendRequestToContainerStatusDetails(searchText);
            }
        }

        sendRequestToContainerStatusDetails(postData) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: waTouchContainerStatusModule.URLHelper.searchContainerStatusList,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbacksendRequestToContainerStatusDetails.bind(this))
                .fail(this.errorCallbacksendRequestToContainerStatusDetails);
        }

        successCallbacksendRequestToContainerStatusDetails(result: any) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                this.$searchdata.data = result.Data;
                this.$searchdata.key = this.getSearchKey();

                ///Before load new data, we have to unbind and bind new controlles
                this.unloadAndBindGrid();

                ///once we get data, then store it in local dataset and then call below method to apply local filters. and then populate grid.
                this.filterDataSetAndLoadGrid();

            }
            $.blockUI;
        }

        errorCallbacksendRequestToContainerStatusDetails(result: any) {
            facilityApp.site.showError('Error occurred while fetching stating areas. Please reload the page and try again!');
        }

        unloadAndBindGrid() {
            $.jgrid.gridUnload(this.gridId);

            //Initiate grid controlles and load the data
            this.dsGrid = $("#" + this.gridId);

            this.dsGridList = $("#" + this.gridListId);
        }

        filterDataSetAndLoadGrid() {
            ///Before load new data, we have to unbind and bind new controlles
            this.unloadAndBindGrid();

            let txtSearchStatus = $.trim($('#txtSearchStatus').val());
            let filterDataFromLocalDS = $.grep(this.$searchdata.data.rows, function (o: any, i) {
                return (txtSearchStatus.length == 0 || o.ActionName.toLowerCase().indexOf(txtSearchStatus.toLowerCase()) >= 0);
            });

            var response = { page: this.$searchdata.data.page, total: this.$searchdata.data.total, records: this.$searchdata.data.records, rows: filterDataFromLocalDS };

            this.bindGridControl(response);
        }

        getSearchKey(): string {
            return $('#txtSearchStatus').val();
        }

        addNewContainerStatus(): void {
            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popupEditContainerStatus').fadeIn();

            $('#containerStatusHeader').html("Add Information");
            this.clearAllControlsInAddEditForm();
        }

        validateData(): boolean {
            let editDvPopup = $('#dvEditContainerStatus');

            if ($('#DispStatusName', editDvPopup).val() == '') {
                facilityApp.site.showAlert('Please enter staging area name.');
                $('DispStatusName', editDvPopup).focus();
                return false;
            }

            return true;
        }

        saveContainerStatus(): void {

            if (this.validateData()) {
                let editDvPopup = $('#dvEditContainerStatus');

                let postData = {
                    ActionID: $('#hfPKId', editDvPopup).val(),
                    Status: $('#Status', editDvPopup).val(),
                    ActionCategoryID: $('#hfActionCategoryID', editDvPopup).val(),
                    ActionName: $('#DispStatusName', editDvPopup).val(),
                    Description: $('#txtStatusDesc', editDvPopup).val(),
                    IsGlobalStatus: $('#hfIsGlobalStatus', editDvPopup).val()
                }

                let ajaxSettings: JQueryAjaxSettings = {
                    url: waTouchContainerStatusModule.URLHelper.addEditContainerStatus,
                    type: 'POST',
                    data: JSON.stringify(postData),
                    traditional: true,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8'
                };

                $.ajax(ajaxSettings)
                    .then(this.successCallbackaddEditContainerStatus.bind(this))
                    .fail(this.errorCallbackaddEditContainerStatus);
            }
        }

        successCallbackaddEditContainerStatus(result: any): void {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                //After save data, we need to reload grid. To reload grid, we have to clear local data variable. so that system will get new data from server.
                this.$searchdata.key = "";
                this.validateAndSubmit(true);

                //After save data, we need to close popup
                $('.js-modal-close').click();
            }
        }

        errorCallbackaddEditContainerStatus(): void {
            facilityApp.site.showError("Error occurred while saving staging area, please reload and try again!");
        }

        init(): void {
            this.validateAndSubmit(true);
            this.initPopup();
        }

        initPopup(): void {
            $(".js-modal-close, .modal-overlay").click((e) => {
                e.preventDefault();

                $(".modal-box, .modal-overlay").fadeOut(500, function () {
                    $(".modal-overlay").remove();
                });
            });

            $(window).resize(function () {
                $(".modal-box").css({
                    top: 5,
                    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                });
            });
            $(window).resize();
        }

        SetActiveInactive(PkId, Status, isGlobalStatus, Parent): void {
            facilityApp.site.showConfrimWithOkCallback("Are you sure you want to make it " + Status + " ?", this.callBackActiveToggleConfirm.bind(this, PkId, Status, isGlobalStatus));
        }

        callBackActiveToggleConfirm(PkId: number, status: string, isGlobalStatus: number): void {
            let postData = {
                PKID: PkId,
                Status: status,
                IsGlobalStatus: isGlobalStatus
            };

            let ajaxSettings: JQueryAjaxSettings = {
                url: waTouchContainerStatusModule.URLHelper.setStatusActiveAndInactive,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbackToggleActiveStatus.bind(this))
                .fail(this.errorCallbackToggleActiveStatus);
        }

        successCallbackToggleActiveStatus(result: any): void {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                this.$searchdata.key = "";
                this.validateAndSubmit(true);
            }
        }

        errorCallbackToggleActiveStatus(): void {
            facilityApp.site.showError("Error occurred while changing status, please reload and try again!");
        }

        showEditPopup(RowId: number, Status: string, StatusCatergoryId: number, Description: string, DispActionName: string, IsGlobalStatus: number): void {
            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popupEditContainerStatus').fadeIn();

            $('#txtStatusDesc', $('#dvEditContainerStatus')).val(Description);
            $('#Status', $('#dvEditContainerStatus')).val(Status);
            //$('#StatusCategory', $('#dvEditContainerStatus')).val(StatusCatergoryId);
            $('#DispStatusName', $('#dvEditContainerStatus')).val(DispActionName);

            $('#hfPKId', $('#dvEditContainerStatus')).val(RowId);
            $('#hfIsGlobalStatus', $('#dvEditContainerStatus')).val(IsGlobalStatus);
            $('#hfActionCategoryID', $('#dvEditContainerStatus')).val(StatusCatergoryId);
            $('#containerStatusHeader').html('Edit <b>' + DispActionName + '</b> Information');
        }

        clearAllControlsInAddEditForm(): void {
            $('#txtStatusDesc', $('#dvEditContainerStatus')).val("");
            //$('#Status', $('#dvEditContainerStatus')).val(-1);

            $('#hfActionCategoryID', $('#dvEditContainerStatus')).val(1);
            $('#DispStatusName', $('#dvEditContainerStatus')).val("");

            $('#hfPKId', $('#dvEditContainerStatus')).val(0);
        }

        formatEditButton(cellvalue, options, rowObject): string {
            let retString = "<input type='button' class='editContainerStatus' value='Edit' data-Id='"
                + rowObject.ActionID + "' data-status='" + rowObject.Status + "' data-ActionCategoryID='" + rowObject.ActionCategoryID
                + "' data-Description='" + rowObject.Description + "' data-dispactionname='" + rowObject.ActionName
                + "' data-isglobalstatus='" + rowObject.IsGlobalStatus + "'/>"
            if (waTouchContainerStatusModule.getAccessLevel.isStageStatusFullAccess == "False") {
                if (rowObject.Status == null || rowObject.Status == "N") {
                    retString = retString + "<input disabled class='btnActive' value='Active' data-isglobalstatus='" + rowObject.IsGlobalStatus + "' data-value='" + rowObject.ActionID + "' type='button' style='margin: 2px;' />";
                }
                else if (rowObject.Status == "A") {
                    retString = retString + "<input disabled class='btnActive'  value= 'InActive' data- isglobalstatus='" + rowObject.IsGlobalStatus + "' data- value='" + rowObject.ActionID + "' type= 'button'  style= 'margin: 2px;' />";
                }
            }
            else {
                if (rowObject.Status == null || rowObject.Status == "N") {
                    retString = retString + "<input class='btnActive' value='Active' data-isglobalstatus='" + rowObject.IsGlobalStatus + "' data-value='" + rowObject.ActionID + "' type='button' style='margin: 2px;' />";
                }
                else if (rowObject.Status == "A") {
                    retString = retString + "<input class='btnActive' value='InActive' data-isglobalstatus='" + rowObject.IsGlobalStatus + "' data-value='" + rowObject.ActionID + "' type='button'  style='margin: 2px;' />";
                }
            }
            return retString;
        }

        formatStatusLevel(cellvalue, options, rowObject): string {
            let sts = "Facility Specific";
            if (rowObject.IsGlobalStatus == 1) {
                sts = "Global";
            }

            return sts;
        }

        formatStatus(cellvalue, options, rowObject): string {
            let sts = "InActive";
            if (rowObject.Status == 'A') {
                sts = "Active";
            }

            return sts;
        }

        bindGridControl(data): void {
            let columnNames: string[] = ['RowID', 'Stage Area', 'Description', 'Category', 'Status Level', 'Status', '', 'IsGlobalStatus'];
            let columnModel: JQueryJqGridColumn[] = [
                { name: 'ActionID', index: 'ActionID', editable: false, hidedlg: true, hidden: true, search: false }
                , { name: 'ActionName', index: 'ActionName', editable: false, search: false, align: 'center', sortable: true }
                , { name: 'Description', index: 'Description', editable: false, search: false, align: 'center', sortable: true }
                , { name: 'ActionCategoryName', index: 'ActionCategoryName', search: false, align: 'center', sortable: true }
                , { name: 'StatusLevel', index: 'StatusLevel', editable: false, hidedlg: true, align: 'center', hidden: false, search: false, formatter: this.formatStatusLevel.bind(this) }
                , { name: 'Status', index: 'Status', editable: false, hidedlg: true, align: 'center', hidden: false, search: false, formatter: this.formatStatus.bind(this) }
                , { name: '', index: '', hidden: false, search: false, formatter: this.formatEditButton.bind(this), align: 'center', sortable: false }
                , { name: 'IsGlobalStatus', index: 'IsGlobalStatus', editable: false, hidedlg: true, hidden: true, search: false }
            ]

            let jqGridOptions: JQueryJqGridOptions = {
                datatype: "local"
                , data: data.rows
                , loadonce: true
                , pager: 'listPager'
                , colNames: columnNames
                , colModel: columnModel
                , rowNum: 20
                , rowList: [10, 20, 30, 60, 365]
                //, sortname: 'ActionID'
                //, sortorder: "asc"
                , viewrecords: true
                , caption: 'Stage Areas'
                , autoWidth: false
                , gridview: true
                , id: "RowID"
                , height: "100%"
                , width: null
                //, multiselect: true
                //, multiboxonly: true
                //  , toolbar: [true, "top"]
                , emptyrecords: 'no records found'
                , gridComplete: this.gridCompleteLocal.bind(this)
                , loadComplete: this.gridLoadComplete.bind(this)
                , loadError: this.gridLoadError.bind(this)
                //, ondblClickRow: this.gridDblClickRow.bind(this)
            };

            this.dsGrid.jqGrid(jqGridOptions).navGrid(
                this.dsGridList
                , {
                    edit: false, add: false, del: false, search: false, refresh: false
                }
                , null
                , null
                , null
            );
            //Removing filter toolbar
            // this.dsGrid.jqGrid('filterToolbar');
        }

        gridCompleteLocal(): void {
        }

        gridLoadComplete(): void {
            if (this.dsGrid.getGridParam('records') === 0) {
                $('#' + this.gridId + ' tbody').html("<div style='text-align:center; padding: 30px'>No records found</div>");
            }
            else {

                $('.editContainerStatus').unbind('click').click((evt) => {
                    evt.preventDefault();

                    let currentElement = $(evt.currentTarget);
                    let Id = currentElement.data('id');
                    let Status = currentElement.data('status');
                    let StatusCatergoryID = currentElement.data('actioncategoryid');
                    let Description = currentElement.data('description');
                    let DispActionName = currentElement.data('dispactionname');
                    let IsGlobalStatus = currentElement.data('isglobalstatus');

                    this.showEditPopup(Id, Status, StatusCatergoryID, Description, DispActionName, IsGlobalStatus);
                });

                $('.btnActive').unbind('click').click((evt) => {
                    evt.preventDefault();

                    let currentElement = $(evt.currentTarget);
                    let pkId = currentElement.data('value');
                    let isGlobalStatus = currentElement.data('isglobalstatus');
                    let Status = currentElement.val();

                    this.SetActiveInactive(pkId, Status, isGlobalStatus, this);
                });
            }
        }

        gridLoadError(xhr: any, st: any, err: any): boolean {
            if (xhr.status == "200") return false;
            var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                width: '600',
                buttons: {
                    Ok: function () {
                        //$(this).dialog("close");
                    }
                }
            });
        }

        reloadDSGrid(): void {
            this.dsGrid.jqGrid("clearGridData", true);

            this.dsGrid.jqGrid('setGridParam', {
                mtype: 'GET'
                , url: waTouchContainerStatusModule.URLHelper.searchContainerStatusList
                , datatype: 'json'
                , postData: {
                    StatusName: $('#txtSearchStatus').val()
                }
            }).trigger("reloadGrid");
        }
    }
}

$(document).ready(function () {
    var owaTouchContainerStatus = new waTouchContainerStatusModule.waTouchContainerStatus();
    owaTouchContainerStatus.init();
});