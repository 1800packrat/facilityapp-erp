﻿/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqgrid.d.ts" />
/// <reference path="libraries\jquery.validation.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="libraries\jquery.tmpl.d.ts" />
/// <reference path="libraries\lib.d.ts" />
/// <reference path="site.ts" />


// SFERP-TODO-CTRMV-- Remove class --TG-563
module weeklyDieselPriceModule {

    export class URLHelper {
        public static searchDieselPricesUrl: string = '';
        public static saveDieselPricesURL: string = '';
        public static getDieselPriceURL: string = '';
    }

    export class getAccessLevel {
        public static isStageStatusFullAccess: string = '';
    }

    export interface localData {
        page: number;
        total: number;
        records: number,
        rows: any;
    }

    export interface DieselPrice {
        ID: number,
        BeginDate: string,
        EndDate: string,
        DieselPrice: number,
        FuelAdjustmentRate: number,
        LDMMultiplier: number,
        LocalMultiplier: number,
        ActualTransCost: number,
        BaseTransCost: number,
        LocalTransSubsidy: number,
        RepositioningFee: number
    }
    
    export class weeklyDieselPrice {
        public gridId: string = "list";
        public gridListId: string = "listPager";

        public dsGrid: JQuery = null;
        public dsGridList: JQuery = null;

        public btnSearch: HTMLElement;
        public btnReset: HTMLElement;
        public btnAddNewDieselPrice: HTMLElement;
        public WeeklyDieselPriceFilterCriteria: JQuery;

        public $searchdata: localData;

        public LocalTransSubsidyDefaultValue: number;

        showHideButton: HTMLElement;
        hShowHide: HTMLElement;
        dvEditWeeklyDieselPrice: JQuery;
        dvEditWeeklyDieselPriceTmpl: JQuery;
        modelSaveWeeklyDieselPrice: JQuery;

        constructor() {
            this.$searchdata = { page: 0, total: 0, records: 0, rows: 0 };

            this.dsGrid = $("#" + this.gridId);

            this.WeeklyDieselPriceFilterCriteria = $('#WeeklyDieselPriceFilterCriteria');

            this.btnSearch = document.getElementById('btnSearch');
            this.btnSearch.onclick = (e) => { e.preventDefault(); this.validateAndSubmit(); }
            
            this.btnReset = document.getElementById('btnReset');
            this.btnReset.onclick = (e) => { e.preventDefault(); this.resetFilterForm(); }

            this.btnAddNewDieselPrice = document.getElementById('btnAddNewDieselPrice');
            this.btnAddNewDieselPrice.onclick = (e) => { e.preventDefault(); this.onAddDieselPrice(); }

            this.showHideButton = document.getElementById('showHideButton');
            this.showHideButton.onclick = (e) => {
                e.preventDefault(); $(e.target).toggleClass("ui-icon-circle-triangle-n ui-icon-circle-triangle-s"); $('.form-list-watouches').slideToggle();
            }

            this.hShowHide = document.getElementById('hShowHide');
            this.hShowHide.onclick = (e) => {
                e.preventDefault(); $(e.target).toggleClass("ui-icon-circle-triangle-n ui-icon-circle-triangle-s"); $('.form-list-watouches').slideToggle();
            }

            this.dvEditWeeklyDieselPrice = $('#dvEditWeeklyDieselPrice');
            this.dvEditWeeklyDieselPriceTmpl = $('#dvEditWeeklyDieselPriceTmpl');

            this.bindFilterDatepicker();

            this.modelSaveWeeklyDieselPrice = $('#modelSaveWeeklyDieselPrice');
            this.modelSaveWeeklyDieselPrice.unbind('click').click((e) => {
                e.preventDefault();

                this.onUpdateWeeklyDieselPrice(e, this);
            });

            this.LocalTransSubsidyDefaultValue = $('#LocalTransSubsidyDefaultValue').val();
        }

        onUpdateWeeklyDieselPrice(e, obj): void {
            let dieselPriceModel: DieselPrice = this.getDieselPriceFromForm();

            if (this.isValidDieselForm(dieselPriceModel)) {
                let postData = { DieselPrice: dieselPriceModel };
                this.saveDieselPrice(postData);
            } 
        }

        getDieselPriceFromForm(): DieselPrice {

            //console.log($('.dtBeginDate', this.dvEditWeeklyDieselPrice));

            let dmodel: DieselPrice = {
                ID: $('#ID', this.dvEditWeeklyDieselPrice).val(),
                BeginDate: $('.dtBeginDate', this.dvEditWeeklyDieselPrice).val(),
                EndDate: $('.dtEndDate', this.dvEditWeeklyDieselPrice).val(),
                DieselPrice: $('#DieselPrice', this.dvEditWeeklyDieselPrice).val(),
                FuelAdjustmentRate: $('#FuelAdjustmentRate', this.dvEditWeeklyDieselPrice).val(),
                LDMMultiplier: $('#LDMMultiplier', this.dvEditWeeklyDieselPrice).val(),
                LocalMultiplier: $('#LocalMultiplier', this.dvEditWeeklyDieselPrice).val(),
                ActualTransCost: $('#ActualTransCost', this.dvEditWeeklyDieselPrice).val(),
                BaseTransCost: $('#BaseTransCost', this.dvEditWeeklyDieselPrice).val(),
                LocalTransSubsidy: $('#LocalTransSubsidy', this.dvEditWeeklyDieselPrice).val(),
                RepositioningFee: $('#RepositioningFee', this.dvEditWeeklyDieselPrice).val()
            };

            return dmodel;
        }

        isValidDieselForm(model: DieselPrice): boolean {
            let validMessage = '';
            //If it is new entry validate dates range
            if (!(model.BeginDate.length > 0)) {
                validMessage = "Please select begin date";
            }
            if (!(model.EndDate.length > 0)) {
                validMessage += "<br /> Please select end date";
            }
            if (new Date(model.BeginDate) > new Date(model.EndDate)) {
                validMessage += "<br /> End date should be grater than Begin date";
            }
            if (!model.DieselPrice) {
                validMessage += "<br /> Please enter valid diesel price";
            }
            if (!model.FuelAdjustmentRate) {
                validMessage += "<br /> Please enter valid fuel adjustment rate";
            }
            if (!model.LocalMultiplier) {
                validMessage += "<br /> Please enter valid local multiplier";
            }
            if (!model.LDMMultiplier) {
                validMessage += "<br /> Please enter valid LDM multiplier";
            }
            if (!model.ActualTransCost) {
                validMessage += "<br /> Please enter valid actual transcost";
            }
            if (!model.BaseTransCost) {
                validMessage += "<br /> Please enter valid base transcost";
            }
            if (!model.LocalTransSubsidy) {
                validMessage += "<br /> Please enter valid local transcost";
            }
            if (!model.RepositioningFee) {
                validMessage += "<br /> Please enter valid repositioning fee";
            }

            if (validMessage.length > 0)
            {
                facilityApp.site.showError(validMessage);
                return false;
            }

            return true;
        }

        saveDieselPrice(postData) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: weeklyDieselPriceModule.URLHelper.saveDieselPricesURL,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbacksaveDieselPrice.bind(this))
                .fail(this.errorCallbacksaveDieselPrice);
        }

        successCallbacksaveDieselPrice(result: any) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                $('.js-modal-close').click();

                $('#btnSearch').trigger('click');
            }
            $.blockUI;
        }

        errorCallbacksaveDieselPrice(result: any) {
            facilityApp.site.showError('Error occurred while saving diesel price. Please reload the page and try again!');
        }

        bindFilterDatepicker(): void {
            let txtBeginDate = $('.dtBeginDate', this.WeeklyDieselPriceFilterCriteria);
            let txtBeginDateIconIcon = $('#BeginDateIcon', this.WeeklyDieselPriceFilterCriteria);
            txtBeginDateIconIcon.unbind("click").click((e) => {
                e.preventDefault();
                txtBeginDate.focus();
            });
            txtBeginDate.datepicker();

            let txtEndDate = $('.dtEndDate', this.WeeklyDieselPriceFilterCriteria);
            let txtEndDateIconIcon = $('#EndDateIcon', this.WeeklyDieselPriceFilterCriteria);
            txtEndDateIconIcon.unbind("click").click((e) => {
                e.preventDefault();
                txtEndDate.focus();
            });
            txtEndDate.datepicker();
        }

        resetFilterForm(): void {            
            $('#WeeklyDieselPriceFilterCriteria')[0].reset();

            $('#btnSearch').click();
        }

        validateAndSubmit(): void {
            let searchText = {
                BeginDate: $('#BeginDate').val(),
                EndDate: $('#EndDate').val(),
            };

            ///Before we fire a query to server, look for data in inmemory for this given date range. if it exists then filter on the local data. This saves SL calls
            this.sendRequestToDieselPrice(searchText);
        }

        sendRequestToDieselPrice(postData) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: weeklyDieselPriceModule.URLHelper.searchDieselPricesUrl,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbacksendRequestToDieselPrice.bind(this))
                .fail(this.errorCallbacksendRequestToDieselPrice);
        }

        successCallbacksendRequestToDieselPrice(result: any) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                ///Before load new data, we have to unbind and bind new controlles
                this.unloadAndBindGrid();

                this.$searchdata = {
                    page: result.data.page,
                    total: result.data.total,
                    records: result.data.records,
                    rows: result.data.rows
                }

                ///once we get data, then store it in local dataset and then call below method to apply local filters. and then populate grid.
                this.bindGridControl(this.$searchdata);
            }
            $.blockUI;
        }

        errorCallbacksendRequestToDieselPrice(result: any) {
            facilityApp.site.showError('Error occurred while fetching diesel price. Please reload the page and try again!');
        }

        unloadAndBindGrid() {
            $.jgrid.gridUnload(this.gridId);

            //Initiate grid controlles and load the data
            this.dsGrid = $("#" + this.gridId);

            this.dsGridList = $("#" + this.gridListId);
        }

        showAddEditForm(dieselPriceModel: DieselPrice) {
            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popupWeeklyDieselPrice').fadeIn();

            let editFormTmpl: any = this.dvEditWeeklyDieselPriceTmpl.tmpl(dieselPriceModel);
            this.dvEditWeeklyDieselPrice.html(editFormTmpl);

            facilityApp.site.bindAcceptOnlyDecimalEvent();

            //Edit mode we should not allow user to edit dates
            if (dieselPriceModel.ID <= 0) {

                $('.dprice-edit-popuphead').html('Add');

                let txtBeginDate = $('.dtBeginDate', this.dvEditWeeklyDieselPrice);
                txtBeginDate.removeAttr('disabled');

                let txtBeginDateIconIcon = $('#BeginDateIcon', this.dvEditWeeklyDieselPrice);
                txtBeginDateIconIcon.unbind("click").click((e) => {
                    e.preventDefault();
                    txtBeginDate.focus();
                });
                txtBeginDate.datepicker({
                    minDate: new Date(),
                    onSelect: function (dateText, inst) {
                        txtEndDate.datepicker("option", "minDate", txtBeginDate.datepicker("getDate"));
                    }.bind(this)
                });

                let txtEndDate = $('.dtEndDate', this.dvEditWeeklyDieselPrice);
                txtEndDate.removeAttr('disabled');

                let txtEndDateIconIcon = $('#EndDateIcon', this.dvEditWeeklyDieselPrice);
                txtEndDateIconIcon.unbind("click").click((e) => {
                    e.preventDefault();
                    txtEndDate.focus();
                });
                txtEndDate.datepicker({
                    minDate: new Date(),
                    onSelect: function (dateText, inst) {
                        txtBeginDate.datepicker("option", "maxDate", txtEndDate.datepicker("getDate"));
                    }.bind(this)
                });
            } else {
                $('.dtBeginDate', this.dvEditWeeklyDieselPrice).attr('disabled', 'disabled');
                $('.dtEndDate', this.dvEditWeeklyDieselPrice).attr('disabled', 'disabled');

                $('.dprice-edit-popuphead').html('Edit');
            }
        }

        onAddDieselPrice() {
            let addForm: DieselPrice = {
                ID: 0,
                BeginDate: '',
                EndDate: '',
                DieselPrice: 0,
                FuelAdjustmentRate: 0,
                LDMMultiplier: 0,
                LocalMultiplier: 0,
                ActualTransCost: 0,
                BaseTransCost: 0,
                LocalTransSubsidy: this.LocalTransSubsidyDefaultValue,
                RepositioningFee: 0
            };

            this.showAddEditForm(addForm);
        }

        sendRequestToGetDieselPrice(id) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: weeklyDieselPriceModule.URLHelper.getDieselPriceURL,
                type: 'GET',
                data: { Id: id },
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbacksendRequestToGetDieselPrice.bind(this))
                .fail(this.errorCallbacksendRequestToGetDieselPrice);
        }

        successCallbacksendRequestToGetDieselPrice(result: any) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                ///Before load new data, we have to unbind and bind new controlles
                this.unloadAndBindGrid();

                this.$searchdata = {
                    page: result.data.page,
                    total: result.data.total,
                    records: result.data.records,
                    rows: result.data.rows
                }

                ///once we get data, then store it in local dataset and then call below method to apply local filters. and then populate grid.
                this.bindGridControl(this.$searchdata);
            }
            $.blockUI;
        }

        errorCallbacksendRequestToGetDieselPrice(result: any) {
            facilityApp.site.showError('Error occurred while fetching diesel price. Please reload the page and try again!');
        }

        init(): void {
            this.validateAndSubmit();
            this.initPopup();
        }

        initPopup(): void {
            $(".js-modal-close, .modal-overlay").click((e) => {
                e.preventDefault();

                $(".modal-box, .modal-overlay").fadeOut(500, function () {
                    $(".modal-overlay").remove();
                });
            });

            $(window).resize(function () {
                $(".modal-box").css({
                    top: 5,
                    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                });
            });
            $(window).resize();
        }

        formatEditButton(cellvalue, options, rowObject): string {        
            return "<input type='button' class='editWeeklyDieselPrice' value='Edit'"
                + "data-id='" + rowObject.ID + "'"
                + "data-begindate='" + rowObject.BeginDateDisp + "'"
                + "data-enddate='" + rowObject.EndDateDisp + "'"
                + "data-dieselprice='" + rowObject.DieselPrice + "'"
                + "data-fueladjustmentrate='" + rowObject.FuelAdjustmentRate + "'"
                + "data-ldmmultiplier='" + rowObject.LDMMultiplier + "'"
                + "data-localmultiplier='" + rowObject.LocalMultiplier + "'"
                + "data-actualtranscost='" + rowObject.ActualTransCost + "'"
                + "data-basetranscost='" + rowObject.BaseTransCost + "'"
                + "data-localtranssubsidy='" + rowObject.LocalTransSubsidy + "'"
                + "data-repositioningfee='" + rowObject.RepositioningFee + "'"
                + "/>"
        }
        
        bindGridControl(data): void {
            let columnNames: string[] = ['ID', 'Begin Date', 'Begin Date', 'End Date', 'End Date', 'Price', 'Fuel Adjustment Rate', 'LDM Multiplier', 'Local Multiplier', 'Actual TransCost', 'Base TransCost', 'Repositioning Fee', 'Local Trans Subsidy', 'Action'];
            let columnModel: JQueryJqGridColumn[] = [
                { name: 'ID', index: 'ID', editable: false, hidedlg: true, key: true, hidden: true, search: false }
                , { name: 'BeginDate', index: 'BeginDate', editable: false, search: false, align: 'center', sortable: true, formatter: 'date' }
                , { name: 'BeginDateDisp', index: 'BeginDateDisp', editable: false, search: false, align: 'center', sortable: true, hidden: true }
                , { name: 'EndDate', index: 'EndDate', editable: false, search: false, align: 'center', sortable: true, formatter: 'date' }
                , { name: 'EndDateDisp', index: 'EndDateDisp', editable: false, search: false, align: 'center', sortable: true, hidden: true }
                , { name: 'DieselPrice', index: 'DieselPrice', search: false, align: 'center', sortable: true }
                , { name: 'FuelAdjustmentRate', index: 'FuelAdjustmentRate', editable: false, hidedlg: true, align: 'center', hidden: false, search: false  }
                , { name: 'LDMMultiplier', index: 'LDMMultiplier', editable: false, hidedlg: true, align: 'center', hidden: false, search: false }
                , { name: 'LocalMultiplier', index: 'LocalMultiplier', editable: false, hidedlg: true, align: 'center', hidden: false, search: false }
                , { name: 'ActualTransCost', index: 'ActualTransCost', editable: false, hidedlg: true, align: 'center', hidden: false, search: false }
                , { name: 'BaseTransCost', index: 'BaseTransCost', editable: false, hidedlg: true, align: 'center', hidden: false, search: false }
                , { name: 'RepositioningFee', index: 'RepositioningFee', editable: false, hidedlg: true, align: 'center', hidden: false, search: false }
                , { name: 'LocalTransSubsidy', index: 'LocalTransSubsidy', editable: false, hidedlg: true, align: 'center', hidden: false, search: false }
                , { name: 'Action', index: 'Action', width: 40, editable: false, hidedlg: true, align: 'center', sortable: false, hidden: false, search: false, formatter: this.formatEditButton.bind(this) }
            ]

            let jqGridOptions: JQueryJqGridOptions = {
                datatype: "local"
                , data: data.rows
                , loadonce: true
                , pager: 'listPager'
                , colNames: columnNames
                , colModel: columnModel
                , rowNum: 20
                , rowList: [10, 20, 30, 60, 365]
                //, sortname: 'ActionID'
                //, sortorder: "asc"
                , viewrecords: true
                , caption: 'Weekly Diesel Price'
                , autoWidth: false
                , gridview: true
                , id: "ID"
                , sortname: "ID"
                , sortorder: "desc"
                , height: "100%"
                , width: null
                //, multiselect: true
                //, multiboxonly: true
                //  , toolbar: [true, "top"]
                , emptyrecords: 'no records found'
                , gridComplete: this.gridCompleteLocal.bind(this)
                , loadComplete: this.gridLoadComplete.bind(this)
                , loadError: this.gridLoadError.bind(this)
                //, ondblClickRow: this.gridDblClickRow.bind(this)
            };

            this.dsGrid.jqGrid(jqGridOptions).navGrid(
                this.dsGridList
                , {
                    edit: false, add: false, del: false, search: false, refresh: false
                }
                , null
                , null
                , null
            );
        }

        gridCompleteLocal(): void {
        }

        gridLoadComplete(): void {
            if (this.dsGrid.getGridParam('records') === 0) {
                $('#' + this.gridId + ' tbody').html("<div style='text-align:center; padding: 30px'>No records found</div>");
            }
            else {
                $('.editWeeklyDieselPrice').unbind('click').click((evt) => {
                    evt.preventDefault();

                    let currentElement = $(evt.currentTarget);
                    let editFormModel: DieselPrice = {
                        ID: currentElement.data('id'),
                        BeginDate: currentElement.data('begindate'),
                        EndDate: currentElement.data('enddate'),
                        DieselPrice: currentElement.data('dieselprice'),
                        FuelAdjustmentRate: currentElement.data('fueladjustmentrate'),
                        LDMMultiplier: currentElement.data('ldmmultiplier'),
                        LocalMultiplier: currentElement.data('localmultiplier'),
                        ActualTransCost: currentElement.data('actualtranscost'),
                        BaseTransCost: currentElement.data('basetranscost'),
                        LocalTransSubsidy: currentElement.data('localtranssubsidy'),
                        RepositioningFee: currentElement.data('repositioningfee')
                    };
                    
                    this.showAddEditForm(editFormModel);
                });
            }
        }

        gridLoadError(xhr: any, st: any, err: any): boolean {
            if (xhr.status == "200") return false;
            var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                width: '600',
                buttons: {
                    Ok: function () {
                        //$(this).dialog("close");
                    }
                }
            });
        }

        reloadDSGrid(): void {
            this.dsGrid.jqGrid("clearGridData", true);

            this.dsGrid.jqGrid('setGridParam', {
                mtype: 'GET'
                , url: weeklyDieselPriceModule.URLHelper.searchDieselPricesUrl
                , datatype: 'json'
                , postData: {
                    StatusName: $('#txtSearchStatus').val()
                }
            }).trigger("reloadGrid");
        }
    }
}

$(document).ready(function () {
    var oweeklyDieselPrice = new weeklyDieselPriceModule.weeklyDieselPrice();
    oweeklyDieselPrice.init();
});