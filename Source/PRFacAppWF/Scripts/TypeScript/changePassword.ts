﻿/// <reference path="Libraries\jquery.d.ts" />;

module facilityAppChangePassword {
    export class URLHelper {
        public static changePassword: string = '';
        public static redirectLogin: string = '/Home/Logout';
    }

    export class changePassword {
        public oldPassword: string = '';
        public newPassword: string = '';
        public retypeNewPassword: string = ''
        btnLogin: HTMLElement;
        btnCancel: HTMLElement;

        constructor() {
            this.btnLogin = document.getElementById('btnSignIn');
            this.btnLogin.onclick = (e) => { this.submit(); }

            this.btnCancel = document.getElementById('btnCancel');
            this.btnCancel.onclick = (e) => { window.location.href = URLHelper.redirectLogin; }
        };

        validatePassword(): boolean {
            //validations here
            if ($('#txtOldPassword').val() == '') {
                $('#lblError').html("Please enter current password.");
                $('#txtOldPassword').focus();
                return false;
            }
            if ($('#txtNewPassword').val() == '') {
                $('#lblError').html("Please enter new password.");
                $('#txtNewPassword').focus();
                return false;
            }
            if ($('#txtRetypeNewPassword').val() == '') {
                $('#lblError').html("Please enter confirm password.");
                $('#txtRetypeNewPassword').focus();
                return false;
            }
            //if ($('#txtNewPassword').val() != '') {
            //    var password = $('#txtNewPassword').val();

            //    if (!(password.match(/[A-Z]/) && password.match(/\d+/) && password.match(/.[!,@@,#,$,%,\^,&,*,?,_,~]/) && (password.length > 6))) {
            //        $('#lblError').html("This is not a strong password \n A strong password is at least 7 characters long and contains uppercase and lowercase letters, numerals, and symbols.");
            //        $('#txtNewPassword').focus();
            //        $('#txtNewPassword').val('');
            //        $('#txtRetypeNewPassword').val('');
            //        return false;
            //    }
            //}
            //if ($('#txtNewPassword').val() !== $('#txtRetypeNewPassword').val()) {
            //    $('#lblError').html("New password and confirm password should match.");
            //    $('#txtNewPassword').val('');
            //    $('#txtRetypeNewPassword').val('');
            //    return false;
            //}
            //if ($('#txtOldPassword').val() === $('#txtNewPassword').val()) {
            //    $('#lblError').html("Current password and new password should not be same, please try different password.");
            //    $('#txtNewPassword').val('');
            //    $('#txtRetypeNewPassword').val('');
            //    return false;
            //}
            $('#lblError').html('');
            return true;
        }

        submit() {
            if (this.validatePassword()) {
                var request = {
                    OldPassword: $('#txtOldPassword').val(),
                    NewPassword: $('#txtNewPassword').val(),
                    RetypeNewPassword: $('#txtRetypeNewPassword').val()
                }
                this.sendRequest(request);
            }
        }

        sendRequest(postData) {
            var ajaxSettings: JQueryAjaxSettings = {
                url: facilityAppChangePassword.URLHelper.changePassword,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: this.successCallbackLogin,
                error: this.errorCallbackLogin
            };

            $.ajax(ajaxSettings);
        }

        successCallbackLogin(result: any) {
            if (result.success == true) {
                var conf = confirm('Password changed successfully. Click OK to redirect to the Login page');
                if (conf == true)
                    window.location.href = result.data;
            } else {
                //write your logic to show errors
                $('#lblError').html(result.message);
                $('#txtOldPassword').val('');
                $('#txtNewPassword').val('');
                $('#txtRetypeNewPassword').val('');
            }
        }

        errorCallbackLogin(result: any) {
            $('#lblError').html(result.message);
            //Show what is the error you got from valid login page
        }
    }
}
$(document).ready(function () {
    var oChangePassword = new facilityAppChangePassword.changePassword();
});

