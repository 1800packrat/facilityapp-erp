﻿/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqgrid.d.ts" />
/// <reference path="libraries\jquery.validation.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="libraries\jquery.tmpl.d.ts" />
/// <reference path="site.ts" />
/// <reference path="libraries\html2canvas.d.ts" />
/// <reference path="libraries\jspdf.d.ts" />

//import jsPDF = require("jspdf");
//import { jsPDF } from "jspdf";
declare var jsPDF: any;

module waTouchesGridModule {
    export class URLHelper {
        public static searchTouchList: string = '';
        public static saveEditTouchInfoURL: string = '';
        public static getBOLURL: string = '';
        public static editQorURL: string = '';
        public static getEditTouchInfoURL: string = '';
        public static touchTicketURL: string = '';
        public static validateUnitName: string = '';
        public static getTouchHistory: string = '';
    }

    export interface gridData {
        rows: any;
        total: number;
        page: number;
        records: number;
    }

    export interface localData {
        key: string;
        data: any;
    }

    export enum WATouchStatus {
        Open = 0, ///This status will not exists in DB, but this we will get it from SiteLink
        Scheduled = 1,
        Stage = 2,
        Completed = 3,
        OnHold = 4
    }

    export enum touchTypes {
        warehouse = 1,
        transportation = 2,
        staging = 3,
        all = 4,
        none = 5,
        custom = 6,
        needsAction = 7
    }

    export class AlertColorCode {
        static type1 = "#F1C1C1";
        static type2 = "#E78787";
    }

    export class TouchTypeDesc {
        static TranportationTouches = {
            DE: 'DE',
            RF: 'RF',
            CC: 'CC',
            DF: 'DF',
            RE: 'RE'
        }
        static WarehouseTouches = {
            WA: 'WA',
            TO: 'TO',
            TI: 'TI'
        }
        static Stage = {
            IBO: 'IBO',
            OBO: 'OBO'
        }
    }

    export class waTouchesGrid {
        public gridId: string = "list";
        public gridListId: string = "listPager";
        public gridTitle: string = "TOUCHES";
        public dsGrid: JQuery = null;
        public dsGridList: JQuery = null;
        public btnSearch: HTMLElement;
        public btnReset: HTMLElement;
        public btnPrint: HTMLElement;
        public btnPrintPDF: HTMLElement;
        public $searchdata: localData;

        imgStartDateIcon: HTMLElement;
        imgEndDateIcon: HTMLElement;
        WarehouseTouchFilterCriteria: JQuery;

        showHideButton: HTMLElement;
        hShowHide: HTMLElement;
        ddlTouchType: JQuery;

        btnSaveTouchInfo: HTMLElement;

        constructor() {
            this.dsGrid = $("#" + this.gridId);

            this.$searchdata = { key: "", data: {} };

            $('#edittouchInfo').tabs({
                activate: function (event, ui) {
                    if (ui.newTab.index() == 1) {
                        $('#popupEditTouchDetails').find("#modelSaveTouchInfo").hide();
                    }
                    else {
                        $('#popupEditTouchDetails').find("#modelSaveTouchInfo").show();
                    }
                }
            });

            this.btnSearch = document.getElementById('btnSearch');
            this.btnSearch.onclick = (e) => { e.preventDefault(); this.validateAndSubmit(); $('#showHideButton').trigger('click'); }
            $('#IsShowPriorNotCompletedTouches').change((e) => { e.preventDefault(); this.validateAndSubmit(); });

            this.btnReset = document.getElementById('btnReset');
            this.btnReset.onclick = (e) => {
                window.location.reload(); 
            }

            this.btnPrint = document.getElementById('btnPrint');
            this.btnPrint.onclick = (e) => {
                e.preventDefault();
                if (this.dsGrid.getGridParam('records') === 0) {
                    facilityApp.site.showError("There are no touches to print.");
                } else {
                    this.handlePrintFunctionality();
                }
            }

            this.showHideButton = document.getElementById('showHideButton');
            this.showHideButton.onclick = (e) => {
                e.preventDefault(); $(e.target).toggleClass("ui-icon-circle-triangle-n ui-icon-circle-triangle-s"); $('.form-list-watouches').slideToggle();
            }

            this.hShowHide = document.getElementById('hShowHide');
            this.hShowHide.onclick = (e) => {
                e.preventDefault(); $(e.target).toggleClass("ui-icon-circle-triangle-n ui-icon-circle-triangle-s"); $('.form-list-watouches').slideToggle();
            }

            //Start date datepicker initilization
            let txtStartDate = $('#StartDate');
            this.imgStartDateIcon = document.getElementById('StartDateIcon');
            this.imgStartDateIcon.onclick = (e) => { e.preventDefault(); txtStartDate.focus(); }
            txtStartDate.datepicker({ 
                onSelect: function (dateText, inst) {
                    txtEndDate.datepicker("option", "minDate", txtStartDate.datepicker("getDate"));
                }.bind(this)
            });

            //End date datepicker binding
            let txtEndDate = $('#EndDate');
            this.imgEndDateIcon = document.getElementById('EndDateIcon');
            this.imgEndDateIcon.onclick = (e) => { e.preventDefault(); txtEndDate.focus(); }
            txtEndDate.datepicker({
                onSelect: function (dateText, inst) {
                    txtStartDate.datepicker("option", "maxDate", txtEndDate.datepicker("getDate"));
                }.bind(this)
            });

            this.WarehouseTouchFilterCriteria = $('#WarehouseTouchFilterCriteria');

            this.ddlTouchType = $('#TouchType');
            this.ddlTouchType.change((e) => { e.preventDefault(); this.onChangeTouchTypeChange(e, this); });
            this.ddlTouchType.val(touchTypes.needsAction);
            this.checkNeedsActionCheckBoxes();

            this.btnSaveTouchInfo = document.getElementById('modelSaveTouchInfo');
            this.btnSaveTouchInfo.onclick = (e) => {
                e.preventDefault(); 
                this.validateAndSaveTouchInfo();
            }

            //Dropdown selection changing according to checkboxes selection.
            $('.chkTouchType').unbind("change").change((e) => {
                this.selectCustomFromDropDown(e, this);
            });
        }

        selectCustomFromDropDown(evet, parent): void {
            let checkedVals: Array<string> = <any>$('input:checkbox:checked.chkTouchType').map(function () {
                return $(this).attr('data-touchtype');
            }).get();

            if (checkedVals.length == 10) {
                $('#TouchType', '#divFilterWareHouseTouches').val(4); //selecting All if all the checkboxes were selected
            }
            else if (checkedVals.length == 0) {
                $('#TouchType', '#divFilterWareHouseTouches').val(5); //selecting None if all the checkboxes were not selected
            }
            else if ((checkedVals.indexOf('DE') > -1) && (checkedVals.indexOf('RF') > -1) && (checkedVals.indexOf('CC') > -1) && (checkedVals.indexOf('DF') > -1) && (checkedVals.indexOf('RE') > -1) && checkedVals.length == 5) {
                $('#TouchType', '#divFilterWareHouseTouches').val(2);
            }
            else if ((checkedVals.indexOf('WA') > -1) && (checkedVals.indexOf('TO') > -1) && (checkedVals.indexOf('TI') > -1) && (checkedVals.indexOf('IBO') > -1) && (checkedVals.indexOf('OBO') > -1) && checkedVals.length == 5) {
                $('#TouchType', '#divFilterWareHouseTouches').val(1);
            }
            else if ((checkedVals.indexOf('DE') > -1) && (checkedVals.indexOf('DF') > -1) && (checkedVals.indexOf('WA') > -1) && (checkedVals.indexOf('TO') > -1) && (checkedVals.indexOf('IBO') > -1) && (checkedVals.indexOf('OBO') > -1) && checkedVals.length == 6) {
                $('#TouchType', '#divFilterWareHouseTouches').val(3);
            }
            else {
                $('#TouchType', '#divFilterWareHouseTouches').val(6);
            }
        }

        handlePrintPDFFunctionality(): void {
            var data = $('.cust-grid-jqgrid').clone();// $('.tblDriverScheduleHrs').html();
            $(data).find('.ui-jqgrid-titlebar').remove();

            data.find('.ui-common-table').css('table-layout', 'fixed');
            data.find('.ui-common-table').css('width', '100%');
            data.find('#divFilterWareHouseTouches').height('100%');
            data.find('#divFilterWareHouseTouches').css('overflow', 'visible');
            data.find('tbody').css('overflow', 'visible');
            data.find('th').each(function () { $(this).css('top', '0px'); });
            $(data).find('td').css('font-size', '15px');
            $(data).find('td').css('white-space', 'normal');
            $(data).removeAttr('class');
            $(data).removeAttr('style');

            data.find('.ui-jqgrid-btable tbody tr:first').remove();
            data.find('.ui-jqgrid-btable tbody tr').css("background-color", "");
            $(data).find('.ui-jqgrid-htable thead tr').clone().prependTo(data.find('.ui-jqgrid-btable tbody'));
            $(data).find('.ui-jqgrid-htable').remove();

            $(data).find('.printHide').remove();
            $(data).find('.printShow').css('display', '');
            $(data).find('th').css('white-space', 'normal');
            $(data).find('th').css('height', '43px');
            $(data).find('th').css('font-size', '17px');
            data.find('a').each(function () {
                $(this).removeAttr("href");
            });
            data.find('#pg_listPager').remove();

            //creating multiple tables in order to divided the pages in pdf viewer
            var $main = $('.ui-jqgrid-btable'),
                $head = $main.find('thead tr:first'),
                $extraRows = $main.find('tbody tr:gt(35)');

            for (var i = 0; i < $extraRows.length; i = i + 35) {
                $('<table>').append($head.clone(), $extraRows.slice(i, i + 35)).appendTo($main.parent());
            }

            var doc = new jsPDF('l', 'mm', 'a4');

            var specialElementHandlers = {
                '#editor': function (element, renderer) {
                    return true;
                }
            };

            var margins = {
                top: 80,
                bottom: 60,
                left: 40,
                width: 522
            };

            var quotes = $('#mainContent').html(data.html());

            doc.addHTML($('#mainContent'), 10, 10, { retina: true }, function () {
                doc.addPage();
                doc.addHTML($('table').get(0), function () {
                    doc.autoPrint();
                    doc.output('dataurlnewwindow');
                });
            });

            $('#mainContent').html('');
        }

        handlePrintFunctionality(): void {
            var data = $('.cust-grid-jqgrid').clone();// $('.tblDriverScheduleHrs').html();
            data.find('.ui-common-table').css('table-layout', 'fixed');
            data.find('.ui-common-table').css('width', '100%');
            data.find('#divFilterWareHouseTouches').height('100%');
            data.find('#divFilterWareHouseTouches').css('overflow', 'visible');
            data.find('tbody').css('overflow', 'visible');
            data.find('th').each(function () { $(this).css('top', '0px'); });

            ////hiding unit number column form print 
            data.find('.ui-jqgrid-btable tbody tr:first').remove();
            data.find('.ui-jqgrid-btable tbody tr').css("background-color", "");
            $(data).find('.ui-jqgrid-htable thead tr').clone().prependTo(data.find('.ui-jqgrid-btable tbody'));
            $(data).find('.ui-jqgrid-htable').remove();
            $(data).find('.printHide').remove();
            $(data).find('.printShow').css('display', '')

            data.find('a').each(function () {
                $(this).removeAttr("href");
            });
            data.find('#pg_listPager').remove();

            var mywindow = window.open('', 'Touch List', 'height=400,width=600');
            mywindow.document.write('<html><head><title>Touch List</title>');
            mywindow.document.write('<link href="/Content/css/footable.core.css" rel="stylesheet" media="print"/><link href="/Content/css/footable.standalone.css" rel="stylesheet" media="print" /> <style type="text/css" >   @media print { @page { size: landscape; width: 9000px; }, table thead tr th.center,  table tbody tr td.center { text-align: center !important; } table th, table td { border:1px solid #ccc; padding: 0.005em; } } </style>');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data.html());
            mywindow.document.write('</body></html>');

            mywindow.print();
            mywindow.close();
        }

        validateAndSaveTouchInfo(): void {
            console.log("TDB - flow check, validateAndSaveTouchInfo"); 
            let vEditTouchDetails = $('#dvEditTouchDetails');
            let key = $('#hfTouchKey', vEditTouchDetails).val();
            let touchInfo = this.getTouchInfoByKey(key);
            let hdnUnitNumberValidationReq = $('#hdnUnitNumberValidationReq').val() == "true" ? true : false;

            //hdnUnitNumberValidationReq flag will be true when unit name is required for touch completion. Now we need this unit name for only IBO touch completion
            if (hdnUnitNumberValidationReq) {
                let postData = {
                    unitName: $.trim($('#UnitNumber', vEditTouchDetails).val())
                };

                if (postData.unitName == undefined || postData.unitName.length <= 0) {
                    facilityApp.site.showError("Please enter unit name to complete touch.");
                }
                else {
                    if (postData.unitName == "000016" || postData.unitName == "000012" || postData.unitName == "000008") {
                        facilityApp.site.showConfrimWithOkCallback("'" + postData.unitName + "' is default unit name, Do you want to continue with default unit?", this.saveTouchInfo.bind(this));
                    }
                    else {
                        let ajaxSettings: JQueryAjaxSettings = {
                            url: waTouchesGridModule.URLHelper.validateUnitName,
                            type: 'POST',
                            data: JSON.stringify(postData),
                            traditional: true,
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8'
                        };

                        $.ajax(ajaxSettings)
                            .then(this.successCBValidateUnitName.bind(this, postData))
                            .fail(this.errorCBValidateUnitName);
                    }
                }
            } else {
                this.saveTouchInfo();
            }
        }

        successCBValidateUnitName(postData: any, result: any): void {
            console.log("TDB - flow check, successCBValidateUnitName");
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                if (result.data == true) {
                    this.saveTouchInfo();
                } else {
                    facilityApp.site.showConfrimWithOkCallback("'" + postData.unitName + "' unit is not available in the inventory, Do you want to cotinue with this unit?", this.saveTouchInfo.bind(this));
                }
            }
        }

        errorCBValidateUnitName(): void {
            facilityApp.site.showError("Error occurred while saving touch information, please reload and try again!");
        }

        saveTouchInfo(): void {
            console.log("TDB - flow check, saveTouchInfo"); 
            let editDvPopup = $('#dvEditTouchDetails');
            let key = $('#hfTouchKey', editDvPopup).val();

            let touchInfo = this.getTouchInfoByKey(key);

            if (touchInfo) {
                let actionId = $('#ddEditAction option:selected', $('#dvEditTouchDetails')).val();
                let isChangeAction = (actionId > 0 && touchInfo.ActionId != actionId); ///This should compare old and new values

                let actioncategoryname = $('#ddEditAction option:selected', $('#dvEditTouchDetails')).data('actioncategoryname');
                let unitname = touchInfo.UnitName;
                let hdnUnitNumberValidationReq = $('#hdnUnitNumberValidationReq').val() == "true" ? true : false;
                if (hdnUnitNumberValidationReq && touchInfo.TouchType == TouchTypeDesc.Stage.IBO && actioncategoryname == 'Completed') {
                    unitname = $('#UnitNumber', $('#dvEditTouchDetails')).val();
                }

                let postData = {
                    QORId: touchInfo.QORId,
                    TouchTypeFull: touchInfo.TouchTypeFull,
                    SequenceNum: touchInfo.SequenceNum,
                    StarsId: touchInfo.StarsId,
                    StarsUnitId: touchInfo.StarsUnitId,
                    UnitName: unitname,
                    ScheduledDate: touchInfo.DispScheduledDate,
                    SLScheduledDate: touchInfo.DispSLScheduledDate,
                    TouchStatusID: touchInfo.TouchStatusID,
                    TouchStatus: touchInfo.TouchStatus,
                    SLTouchStatus: touchInfo.SLTouchStatus,
                    FirstName: touchInfo.FirstName,
                    LastName: touchInfo.LastName,
                    PhoneNumber: touchInfo.PhoneNumber,
                    TouchTime: touchInfo.TouchTime,
                    CustAddress1: touchInfo.CustAddress1,
                    CustAddress2: touchInfo.CustAddress2,
                    CustCity: touchInfo.CustCity,
                    CustRegion: touchInfo.CustRegion,
                    CustZip: touchInfo.CustZip,
                    FromFirstName: touchInfo.FromFirstName,
                    FromLastName: touchInfo.FromLastName,
                    FromPhone: touchInfo.FromPhone,
                    FromAddr1: touchInfo.FromAddr1,
                    FromAddr2: touchInfo.FromAddr2,
                    FromCity: touchInfo.FromCity,
                    FromState: touchInfo.FromState,
                    FromZip: touchInfo.FromZip,
                    ToFirstName: touchInfo.ToFirstName,
                    ToLastName: touchInfo.ToLastName,
                    ToPhone: touchInfo.ToPhone,
                    ToAddr1: touchInfo.ToAddr1,
                    ToAddr2: touchInfo.ToAddr2,
                    ToCity: touchInfo.ToCity,
                    ToState: touchInfo.ToState,
                    ToZip: touchInfo.ToZip,
                    Comments: $('#Notes', editDvPopup).val(),
                    ActionID: $('#ddEditAction option:selected', $('#dvEditTouchDetails')).val(),
                    ActionName: $('#ddEditAction option:selected', $('#dvEditTouchDetails')).text(),
                    TouchActionCategory: actioncategoryname,
                    ByPassActionChangeToSyncData: $('#ddEditAction option:selected', $('#dvEditTouchDetails')).data('bypassactionchangetosyncdata'),
                    IsChangeAction: isChangeAction
                }

                let ajaxSettings: JQueryAjaxSettings = {
                    url: waTouchesGridModule.URLHelper.saveEditTouchInfoURL,
                    type: 'POST',
                    data: JSON.stringify(postData),
                    traditional: true,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8'
                };

                $.ajax(ajaxSettings)
                    .then(this.successCallbacksaveTouchInfo.bind(this))
                    .fail(this.errorCallbacksaveTouchInfo);
            }
            else {
                facilityApp.site.showAlert('Error occurred while saving touch information, Please reload page and try again!');
            }
        }

        successCallbacksaveTouchInfo(result: any): void {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                //After save data, we need to reload grid. To reload grid, we have to clear local data variable. so that system will get new data from server.
                this.$searchdata.key = "";
                this.validateAndSubmit();

                //After save data, we need to close popup
                $('.js-modal-close').click();
            }
        }

        errorCallbacksaveTouchInfo(): void {
            facilityApp.site.showError("Error occurred while completing touch, please reload and try again!");
        }

        onChangeTouchTypeChange(evet, parent): void {
            let optionsValue = $(evet.currentTarget).val()

            this.clearTouchTypeSelection(false);

            if (optionsValue == touchTypes.transportation) {
                this.checkTransportationCheckBoxes();
            }
            else if (optionsValue == touchTypes.warehouse) {
                this.checkWareHouseCheckBoxes();
            }
            else if (optionsValue == touchTypes.staging) {
                this.checkStagingCheckBoxes();
            }
            else if (optionsValue == touchTypes.all) {
                this.checkAllorNoneCheckBoxes(true);
            }
            else if (optionsValue == touchTypes.none) {
                this.checkAllorNoneCheckBoxes(false);
            }
            else if (optionsValue == touchTypes.needsAction) {
                this.checkNeedsActionCheckBoxes();
            }
        }

        clearTouchTypeSelection(isChecked): void {
            $('#IsDETouches').prop('checked', isChecked);
            $('#IsRFTouches').prop('checked', isChecked);
            $('#IsCCToches').prop('checked', isChecked);
            $('#IsDFTouches').prop('checked', isChecked);
            $('#IsRETouches').prop('checked', isChecked);
            $('#IsWATouches').prop('checked', isChecked);
            $('#IsLDMOutTouches').prop('checked', isChecked);
            $('#IsLDMInTouches').prop('checked', isChecked);
            $('#IsIBOTouches').prop('checked', isChecked);
            $('#IsOBOTouches').prop('checked', isChecked);
        }

        checkTransportationCheckBoxes(): void {
            $('#IsDETouches').prop('checked', true);
            $('#IsRFTouches').prop('checked', true);
            $('#IsCCToches').prop('checked', true);
            $('#IsDFTouches').prop('checked', true);
            $('#IsRETouches').prop('checked', true);
        };

        checkWareHouseCheckBoxes(): void {
            $('#IsWATouches').prop('checked', true);
            $('#IsLDMOutTouches').prop('checked', true);
            $('#IsLDMInTouches').prop('checked', true);

            $('#IsIBOTouches').prop('checked', true);
            $('#IsOBOTouches').prop('checked', true);
        };

        checkStagingCheckBoxes(): void {
            $('#IsDETouches').prop('checked', true);
            $('#IsDFTouches').prop('checked', true);

            $('#IsWATouches').prop('checked', true);
            $('#IsLDMOutTouches').prop('checked', true); 

            $('#IsIBOTouches').prop('checked', true);
            $('#IsOBOTouches').prop('checked', true);
        };

        checkNeedsActionCheckBoxes(): void {
            $('#IsDETouches').prop('checked', true);
            $('#IsDFTouches').prop('checked', true);

            $('#IsWATouches').prop('checked', true);
            $('#IsLDMOutTouches').prop('checked', true);
            $('#IsLDMInTouches').prop('checked', true);

            $('#IsIBOTouches').prop('checked', true);
            $('#IsOBOTouches').prop('checked', true);
        };

        checkAllorNoneCheckBoxes(isChecked): void {
            this.clearTouchTypeSelection(isChecked);
        };

        validateAndSubmit(): any {
            let filterReq = {
                StartDate: $('#StartDate').val(),
                EndDate: $('#EndDate').val(),
                IsShowPriorNotCompletedTouches: $('#IsShowPriorNotCompletedTouches').is(':checked')
            };

            let existingDataKey = this.$searchdata.key;
            let newSearchKey = this.getSearchKey();

            ///Before we fire a query to server, look for data in inmemory for this given date range. if it exists then filter on the local data. This saves SL calls
            if (existingDataKey == newSearchKey) {
                this.filterDataSetAndLoadGrid();
            }
            else {
                this.sendRequestToGetSLTouchesXHR(filterReq);
            }
        }

        filterDataSetAndLoadGrid() {
            //alert("test search for touch");
            ///Before load new data, we have to unbind and bind new controlles
            this.unloadAndBindGrid();

            let startDate = new Date($('#StartDate').val());
            let endDate = new Date($('#EndDate').val());

            let firstName = $.trim($('#FirstName').val());
            let lastName = $.trim($('#LastName').val());

            let status = $('#Status').val();
            let statusText = $("#Status option:selected").text();

            let touchIdentificationNumber = $.trim($('#TouchIdentificationNumber').val());

            //Touch Status
            let isShowCompletedTouches = $('#IsShowCompletedTouches').is(':checked');
            let isShowPriorNotCompletedTouches = $('#IsShowPriorNotCompletedTouches').is(':checked');

            //Touch Types
            let isDETouches = $('#IsDETouches').is(':checked');
            let isRFTouches = $('#IsRFTouches').is(':checked');
            let isCCTouches = $('#IsCCToches').is(':checked');
            let isDFTouches = $('#IsDFTouches').is(':checked');
            let isRETouches = $('#IsRETouches').is(':checked');
            let isWATouches = $('#IsWATouches').is(':checked');
            let isLDMOutTouches = $('#IsLDMOutTouches').is(':checked');
            let isLDMInTouches = $('#IsLDMInTouches').is(':checked');
            let isIBOTouches = $('#IsIBOTouches').is(':checked');
            let isOBOTouches = $('#IsOBOTouches').is(':checked');

            let todayDateTime = new Date();
            let todayDate = new Date(todayDateTime.toDateMMDDYYYY());

            let filterDataFromLocalDS = $.grep(this.$searchdata.data.rows, function (o: any, i) {
                return (
                    (isShowPriorNotCompletedTouches == true &&
                        (o.DispTouchStatus != "Completed" && 
                            ((new Date(o.DispScheduledDate)) < todayDate) &&
                            (o.TouchType == TouchTypeDesc.WarehouseTouches.WA ||
                                o.TouchType == TouchTypeDesc.WarehouseTouches.TI ||
                                o.TouchType == TouchTypeDesc.WarehouseTouches.TO ||
                                o.TouchType == TouchTypeDesc.Stage.IBO ||
                                o.TouchType == TouchTypeDesc.Stage.OBO))) ||
                    ((firstName.length == 0 || o.FirstName.toLowerCase().indexOf(firstName.toLowerCase()) >= 0)
                        && (lastName.length == 0 || o.LastName.toLowerCase().indexOf(lastName.toLowerCase()) >= 0)
                        && ((isDETouches == true && o.TouchType == TouchTypeDesc.TranportationTouches.DE)
                            || (isRFTouches == true && o.TouchType == TouchTypeDesc.TranportationTouches.RF)
                            || (isCCTouches == true && o.TouchType == TouchTypeDesc.TranportationTouches.CC)
                            || (isDFTouches == true && o.TouchType == TouchTypeDesc.TranportationTouches.DF)
                            || (isRETouches == true && o.TouchType == TouchTypeDesc.TranportationTouches.RE)
                            || (isWATouches == true && o.TouchType == TouchTypeDesc.WarehouseTouches.WA)
                            || (isLDMOutTouches == true && o.TouchType == TouchTypeDesc.WarehouseTouches.TO)
                            || (isLDMInTouches == true && o.TouchType == TouchTypeDesc.WarehouseTouches.TI)
                            || (isIBOTouches == true && o.TouchType == TouchTypeDesc.Stage.IBO)
                            || (isOBOTouches == true && o.TouchType == TouchTypeDesc.Stage.OBO)) 
                        && (status == -1 || o.DispTouchStatus.toLowerCase().indexOf(statusText.toLowerCase()) >= 0 || (status == 0 && (o.DispTouchStatus != "OnHold" || (o.DispTouchStatus == "OnHold" && o.AlertLevel == 2)) && o.DispTouchStatus != "Completed"))
                        && (startDate <= (new Date(o.DispScheduledDate)) && (new Date(o.DispScheduledDate)) <= endDate)
                        && (touchIdentificationNumber.length == 0 || (touchIdentificationNumber.length >= 3 && ((o.StarsId + '').indexOf(touchIdentificationNumber) >= 0 || (o.QORId + '').indexOf(touchIdentificationNumber) >= 0 || o.UnitName.toLowerCase().indexOf(touchIdentificationNumber.toLowerCase()) >= 0)))))
            });

            var response = { page: this.$searchdata.data.page, total: this.$searchdata.data.total, records: this.$searchdata.data.records, rows: filterDataFromLocalDS };

            this.bindGridControl(response);
        }

        getSearchKey(): string {
            return new Date($('#StartDate').val()).toDateMMDDYYYY() + '-' + new Date($('#EndDate').val()).toDateMMDDYYYY();
        }

        init(): void {
            this.validateAndSubmit();
            this.initPopup();
        }

        initPopup(): void {
            $(".js-modal-close, .modal-overlay").click((e) => {
                e.preventDefault();

                $(".modal-box, .modal-overlay").fadeOut(500, function () {
                    $(".modal-overlay").remove();
                });
            });

            $(window).resize(function () {
                $(".modal-box").css({
                    top: 5,
                    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                });
            });
            $(window).resize();
        }

        sendRequestToGetSLTouchesXHR(postData) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: waTouchesGridModule.URLHelper.searchTouchList,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbackSendRequestToGetSLTouchesXHR.bind(this))
                .fail(this.errorCallbackSendRequestToGetSLTouchesXHR);
        }

        successCallbackSendRequestToGetSLTouchesXHR(result: any) { 
            console.log(result);
            if (result.success == false) { 
                facilityApp.site.showError(result.message);
            } else { 
                this.$searchdata.data = result.data;
                this.$searchdata.key = this.getSearchKey();

                ///Before load new data, we have to unbind and bind new controlles
                this.unloadAndBindGrid(); 

                ///once we get data, then store it in local dataset and then call below method to apply local filters. and then populate grid.
                this.filterDataSetAndLoadGrid();
                //console.log("successCallbackSendRequestToGetSLTouchesXHR-33");
            }
            $.blockUI;
        }

        errorCallbackSendRequestToGetSLTouchesXHR(result: any) { 
            console.log(result);
            facilityApp.site.showError('Error occurred while fetching Salesforce touches. Please reload the page and try again!');
        }

        unloadAndBindGrid() {
            $.jgrid.gridUnload(this.gridId);

            //Initiate grid controlles and load the data
            this.dsGrid = $("#" + this.gridId);
            this.dsGridList = $("#" + this.gridListId);
        }

        getTouchInfoByKey(key: string): any {
            let filterTouchInfo = $.grep(this.$searchdata.data.rows, function (o: any, i) {
                return o.TouchKey == key;
            });

            if (filterTouchInfo && filterTouchInfo.length > 0) {
                return filterTouchInfo[0];
            }

            return undefined;
        }

        editTouchDetails(key: string): void {
            this.bindGridControl(this);

            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popupEditTouchDetails').fadeIn();

            let touchInfo = this.getTouchInfoByKey(key);

            if (touchInfo) {
                let dvEditTouchDetails = $('#dvEditTouchDetails');
                $('#unitNumberTextField', dvEditTouchDetails).addClass('hide');
                $('#UnitNumber', dvEditTouchDetails).val(touchInfo.UnitName);
                $('#hdnUnitNumberValidationReq', dvEditTouchDetails).val("false");

                let custFullName = touchInfo.FirstName + ' ' + touchInfo.LastName;

                $('.touch-edit-popuphead', $('#popupEditTouchDetails')).html(custFullName);
                $('#CustomerName', dvEditTouchDetails).html(custFullName);
                $('#OrderNumber', dvEditTouchDetails).html(touchInfo.QORId.toString());

                let tt = touchInfo.TouchType;
                if (tt == TouchTypeDesc.WarehouseTouches.TI)
                    tt = "LDM In";
                else if (tt == TouchTypeDesc.WarehouseTouches.TO)
                    tt = "LDM Out";

                $('#SpanTouchType', dvEditTouchDetails).html(tt);
                $('#SpanTouchPreviousAction', dvEditTouchDetails).html(touchInfo.ActionNameWithCategory);
                $('#lblStatusName', dvEditTouchDetails).html(touchInfo.DispTouchStatus);

                $('#hfTouchKey', dvEditTouchDetails).val(key);

                //getting touch history
                let postdataforhistory = {
                    QORID: touchInfo.QORId,
                    TouchType: touchInfo.TouchType,
                    SeqNumber: touchInfo.SequenceNum
                };
                this.getTouchHistoryXHR(postdataforhistory);

                if (touchInfo.SLTouchStatus != 'Completed') {
                    //If there is any conflict then show the messsage in popup 
                    if (touchInfo.IsConflictWithScheduleDate == true) {
                        let editmsg = "<p>This touch has been rescheduled to " + touchInfo.DispSLScheduledDate + ". Please click on submit to sync Salesforce information with FacilityApp.</p>";
                        $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).removeClass("infomsg").addClass("errormsg").html(editmsg).show();
                    } else if (touchInfo.IsTouchMissed == true) {
                        let editmsg = "<p>This touch has been unscheduled. Please return to stack to sync Salesforce information with FacilityApp.</p>";
                        $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).removeClass("infomsg").addClass("errormsg").html(editmsg).show();
                    } else {
                        $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).hide();
                    }

                    let postdataforedit = {
                        touchType: touchInfo.TouchType,
                        statusID: touchInfo.TouchStatusID,
                        touchScheduleDate: touchInfo.DispScheduledDate
                    };

                    //need to change this object to action object.
                    let actionObj = {
                        actionName: touchInfo.ActionName,
                        actionID: touchInfo.ActionID,
                        key: key
                    }

                    this.getEditTouchInfoXHR(postdataforedit, actionObj);
                } else {
                    let editmsg = "<p>This touch has been completed in Salesforce. You cannot change touch status.</p>";
                    $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).addClass("infomsg").removeClass("errormsg").html(editmsg).show();

                    let ddEditAction = $('#ddEditAction', $('#dvEditTouchDetails'));
                    ddEditAction.empty();

                    let opt = '<option value="0" data-actioncategoryname="-1">No actions can be made on completed touch</option>';
                    ddEditAction.append(opt);

                    ddEditAction.prop("disabled", true);
                }

                $('#Notes', $('#dvEditTouchDetails')).val(touchInfo.Comments);
                $('#edittouchInfo').tabs({
                    active: "0"
                });
            }
            else {
                facilityApp.site.showError('Error occurred while fetching touch info. Please reload the page and try again!');
            }
        }

        getTouchTicket(qorId, touchType, seqNumber): void {
            alert('In progress...');
        }

        ////SFERP-TODO-CTRMV-- Remove methods --TG - 579
        getBOLDocs(qorId): void {

            let bolReq = {
                qorid: qorId
            };

            this.getBOLDocsXHR(bolReq);
        }  

        getBOLDocsXHR(postData) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: waTouchesGridModule.URLHelper.getBOLURL,
                type: 'GET',
                data: postData,
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbackGetBOLDocsXHR.bind(this))
                .fail(this.errorCallbackGetBOLDocsXHR);
        }

        successCallbackGetBOLDocsXHR(result: any) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                let width = window.innerWidth * .75;;
                let height = window.innerHeight * .75;
                let left = (screen.width - width) / 2;
                let top = (screen.height - height) / 2;
                let params = 'width=' + width + ', height=' + height;
                params += ', top=' + top + ', left=' + left;
                params += ', directories=no';
                params += ', location=no';
                params += ', menubar=no';
                params += ', resizable=yes';
                params += ', scrollbars=yes';
                params += ', status=no';
                params += ', toolbar=no';

                window.open(result.data, '_blank', params);
            }
            $.blockUI;
        }

        errorCallbackGetBOLDocsXHR(result: any) {
            facilityApp.site.showError('Error occurred while fetching BOL document link. Please reload the page and try again!');
        }

        getEditTouchInfoXHR(postData, actionObj) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: waTouchesGridModule.URLHelper.getEditTouchInfoURL,
                type: 'GET',
                data: postData,
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successCBEditTouchInfoXHR.bind(this, actionObj))
                .fail(this.errorCBEditTouchInfoXHR);
        }

        successCBEditTouchInfoXHR(actionObj: any, result: any) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                let ddEditAction = $('#ddEditAction', $('#dvEditTouchDetails'));
                //Before refresh dropdown make it empty and re-populate stats from result.data
                ddEditAction.empty(); 

                if (result.data.length > 0) {
                    ddEditAction.prop("disabled", false);
                    let opt = '<option value="0" data-actioncategoryname="-1">--Please Select Action--</option>';
                    ddEditAction.append(opt);

                    var uniqueCategoryActions = result.data.reduce(function (result, current) {
                        result[current.ActionCategoryName] = result[current.ActionCategoryName] || [];
                        result[current.ActionCategoryName].push(current);
                        return result;
                    }, {});

                    $.each(uniqueCategoryActions, function (key, value) {
                        let group: any;
                        if ($(value).length > 1) {
                            group = $('<optgroup label="' + key + '" />');
                            $.each(value, function () {
                                $('<option value="' + this.ActionID + '" data-actioncategoryname="' + this.ActionCategoryName + '">' + this.ActionName + '</option>').appendTo(group);
                            });
                        }
                        else {
                            $.each(value, function () {
                                group = $('<option value="' + this.ActionID + '" data-actioncategoryname="' + this.ActionCategoryName + '">' + this.ActionName + '</option>');
                            });
                        }

                        group.appendTo(ddEditAction);
                    });

                    ddEditAction.unbind("change").change((e) => { e.preventDefault(); this.showHideUnitNameField(e); });
                } else {
                    let ByPassActionChangeToSyncData = false;
                    let touchInfo = this.getTouchInfoByKey(actionObj.key);

                    //If the error message is visible but there are no actions are available for this touch, then change message
                    if (touchInfo.IsConflictWithScheduleDate == true) {
                        let editmsg = "<p>This touch has been rescheduled to " + touchInfo.DispSLScheduledDate + ". Please click on Submit to sync Salesforce information with FacilityApp.</p>";
                        $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).removeClass("infomsg").addClass("errormsg").html(editmsg).show();
                        ByPassActionChangeToSyncData = true;
                    } else if (touchInfo.IsTouchMissed == true) {
                        let editmsg = "<p>This touch has been unscheduled. Please click on Submit to sync Salesforce information with FacilityApp.</p>";
                        $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).removeClass("infomsg").addClass("errormsg").html(editmsg).show();
                        ByPassActionChangeToSyncData = true;
                    }

                    let opt = '<option value="0" data-actioncategoryname="-1" data-bypassactionchangetosyncdata=' + ByPassActionChangeToSyncData + '>There are no further actions available for this touch</option>';
                    ddEditAction.append(opt);

                    ddEditAction.prop("disabled", true);
                }
            }
            $.blockUI;
        }

        errorCBEditTouchInfoXHR(result: any) {
            facilityApp.site.showError('Error occurred fetching touch deatils. Please reload the page and try again!');
        }

        showHideUnitNameField(evet) {
            let optionCatValue = $('option:selected', $(evet.currentTarget)).data('actioncategoryname');
            let vEditTouchDetails = $('#dvEditTouchDetails');
            let key = $('#hfTouchKey', vEditTouchDetails).val();

            let touchInfo = this.getTouchInfoByKey(key);

            if (touchInfo.TouchType == TouchTypeDesc.Stage.IBO && optionCatValue == 'Completed') {
                $('#unitNumberTextField', vEditTouchDetails).removeClass('hide');
                $('#UnitNumber', vEditTouchDetails).val(touchInfo.UnitName);
                $('#hdnUnitNumberValidationReq', vEditTouchDetails).val("true");
            } else {
                $('#unitNumberTextField', vEditTouchDetails).addClass('hide');
                $('#hdnUnitNumberValidationReq', vEditTouchDetails).val("false");
            }
        }

        getTouchHistoryXHR(postData) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: waTouchesGridModule.URLHelper.getTouchHistory,
                type: 'GET',
                data: postData,
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successTouchHistoryXHR.bind(this))
                .fail(this.errorTouchHistoryXHR);
        }

        successTouchHistoryXHR(result: any) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {

                $('#tBodyTouchHistory').html(result.data.touchHistory);
            }
            $.blockUI;
        }

        errorTouchHistoryXHR(result: any) {
            facilityApp.site.showError('Error occurred fetching touch deatils. Please reload the page and try again!');
        }

        formatTouchStatus(cellvalue, options, rowObject): string {
            ///Since we are using type script, we cannot bind direct click event to edit record here. 
            ////To handle click event, we are adding class "orderEditAction" here and binding click event in gridloadcomplete event. --If you find any better approach please replace this logic
            return "<a href='#'  class='orderEditAction'" + "' data-key='" + rowObject.TouchKey + "')\" title='" + cellvalue + "'>" + cellvalue + "</a>";
        }

        ////SFERP-TODO-CTUPD  -- Need to link Salesforce webpage in future - TBD.
        formatOrderNumber(cellvalue, options, rowObject): string {
            //Commented by Sohan for furture reference
            //let url = waTouchesGridModule.URLHelper.editQorURL + "?qorid=" + rowObject.QORId;
            //return "<a onclick='$.blockUI();' title='" + rowObject.QORId + "' href='" + url + "')\">" + rowObject.QORId + "</a>";
            return "<a class='showDisableMsg' title='" + rowObject.QORId + "' href='#')\">" + rowObject.QORId + "</a>";
        } 

        formatUnitName(cellvalue, options, rowObject): string {
            return cellvalue + (rowObject.IsZippyShellQuote ? " (ZM)" : "");
        }

        formatComments(cellvalue, options, rowObject): string {
            if (cellvalue && cellvalue.length > 0)
                return $.jgrid.htmlEncode(cellvalue);
            else
                return "";
        }

        ////SFERP-TODO-CTUPD -- may not needed this action after getting data from Salesforce - TBD.
        formatScheduleDate(cellvalue, options, rowObject): string {
            let a = '';
            if (rowObject.IsConflictWithScheduleDate == true || rowObject.IsTouchMissed == true) {
                let title;
                if (rowObject.IsTouchMissed == true) {
                    title = "This has been unscheduled.";
                } else {
                    title = "This has been rescheduled to " + rowObject.DispSLScheduledDate;
                }
                a = "&nbsp;<img alt='CONFLICT' src='../Images/MediaLoot/icn_alert_warning.png' class='orderEditAction watouches-alert-pointer' title='" + title + "' data-key='" + rowObject.TouchKey + "')\" />";

            }

            return "<span title=" + cellvalue + ">" + cellvalue + "</span>" + a;
        }

        formatTouchType(cellvalue, options, rowObject): string {
            if (cellvalue && cellvalue.length > 0 && cellvalue == TouchTypeDesc.WarehouseTouches.TI)
                return "LDM In";
            else if (cellvalue && cellvalue.length > 0 && cellvalue == TouchTypeDesc.WarehouseTouches.TO)
                return "LDM Out";
            else
                return cellvalue;
        }

        formatStarsID(cellvalue, options, rowObject): string {
            return cellvalue > 0 ? cellvalue : "";
        }

        ////SFERP-TODO-CTUPD -- Need to uncomment below line - TBD.
        formatBOL(cellvalue, options, rowObject): string { 
            let lnk = "";
            //Commented by Sohan for furture reference        
            //if (rowObject.TouchType == TouchTypeDesc.WarehouseTouches.TI || rowObject.TouchType == TouchTypeDesc.WarehouseTouches.TO)
            //    lnk = "<a title='Click here to see BOL' class='orderBOLAction' data-qorId='" + rowObject.QORId + "' data-touchType='" + rowObject.TouchType + "' data-seqNumber ='" + rowObject.SequenceNum + "' href='#')\">BOL</a>"
            //else { 
            //    let ttlnk = waTouchesGridModule.URLHelper.touchTicketURL + "?touchType=" + rowObject.TouchType + "&qorid=" + rowObject.QORId;
            //    lnk += "<a title = 'Click here to generate touch ticket' target='_blank' href='" + ttlnk + "')>TT</a>"
            //}
             
            if (rowObject.TouchType == TouchTypeDesc.WarehouseTouches.TI || rowObject.TouchType == TouchTypeDesc.WarehouseTouches.TO)
                lnk = "<a title='Click here to see BOL' class='showDisableMsg' data-qorId='" + rowObject.QORId + "' data-touchType='" + rowObject.TouchType + "' data-seqNumber ='" + rowObject.SequenceNum + "' href='#')\">BOL</a>"
            else {
                let ttlnk = waTouchesGridModule.URLHelper.touchTicketURL + "?touchType=" + rowObject.TouchType + "&qorid=" + rowObject.QORId;
                lnk += "<a title = 'Click here to generate touch ticket' class='showDisableMsg' target='_blank' href='#')>TT</a>"
            } 

            return lnk;
        }

        bindGridControl(data): void {
            let columnNames: string[] = ['AlertLevel', 'TouchKey', 'FirstName', 'LastName', 'Order Number', 'Stars ID',
                'StarsUnitID', 'Unit Number', 'Phone', 'Schedule Date', 'DispSLScheduledDate', 'Touch Time',
                'Provided ETA', 'SLTouchStatus', 'LocalTouchStatus', 'Status', 'ActionId', 'ActionName', 'Action', 'TouchStatusID', 'Touch Type',
                'TouchTypeFull', 'Docs', 'Comments', 'SequenceNum', 'IsConflictWithScheduleDate', 'IsTouchMissed', 'TouchActionCategory', 'DispTouchActionCategory', '', 'IsZippyShellQuote'];

            let columnModel: JQueryJqGridColumn[] = [
                {
                    name: 'AlertLevel', index: 'AlertLevel', editable: false, hidedlg: true, hidden: true, search: false
                },
                {
                    name: 'TouchKey', index: 'TouchKey', editable: false, hidedlg: true, hidden: true, search: false
                },
                {
                    name: 'FirstName', index: 'FirstName', editable: false, hidden: false, classes: 'printHide'
                },
                {
                    name: 'LastName', index: 'LastName', editable: false, hidden: false
                },
                { 
                    name: 'QORId', index: 'QORId', editable: false, hidden: false
                    ////SFERP-TODO-CTUPD -- Need to uncomment below line - TBD.
                    //name: 'QORId', index: 'QORId', editable: false, hidden: false, align: 'center', search: false, formatter: this.formatOrderNumber.bind(this), classes: 'printHide', title: false
                },
                {
                    name: 'StarsId', index: 'StarsId', editable: false, hidden: false, align: 'center', search: false, formatter: this.formatStarsID.bind(this), classes: 'printHide'
                },
                {
                    name: 'StarsUnitId', index: 'StarsUnitId', editable: false, hidden: true
                },
                {
                    name: 'UnitName', index: 'UnitName', editable: false, hidden: false, align: 'center', search: false, formatter: this.formatUnitName.bind(this)
                },
                {
                    name: 'Phonenumber', index: 'Phonenumber', editable: false, hidden: false, align: 'center', search: false, classes: 'printHide'
                },
                {
                    name: 'DispScheduledDate', index: 'DispScheduledDate', align: 'center', editable: false, hidden: false, formatter: this.formatScheduleDate.bind(this), title: false
                },
                {
                    name: 'DispSLScheduledDate', index: 'DispSLScheduledDate', hidden: true
                },
                {
                    name: 'TouchTime', index: 'TouchTime', editable: false, hidden: false, align: 'center'
                },
                {
                    name: 'ETAActualTime', index: 'ETAActualTime', editable: false, hidden: false, align: 'center', search: false
                },
                {
                    name: 'SLTouchStatus', index: 'SLTouchStatus', editable: false, hidden: true
                },
                {
                    name: 'LocalTouchStatus', index: 'LocalTouchStatus', editable: false, hidden: true
                },
                {
                    name: 'DispTouchStatus', index: 'DispTouchStatus', editable: false, hidden: false, align: 'center', search: false
                },
                {
                    name: 'ActionID', index: 'ActionID', editable: false, hidden: true
                },
                {
                    name: 'ActionName', index: 'ActionName', editable: false, hidden: true
                },
                {
                    name: 'ActionNameWithCategory', index: 'ActionNameWithCategory', editable: false, hidden: false, align: 'center', search: false, formatter: this.formatTouchStatus.bind(this), title: false
                },
                {
                    name: 'TouchStatusID', index: 'TouchStatusID', editable: false, hidden: true
                },
                {
                    name: 'TouchType', index: 'TouchType', editable: false, hidden: false, align: 'center', search: false, formatter: this.formatTouchType.bind(this)
                },
                {
                    name: 'TouchTypeFull', index: 'TouchTypeFull', editable: false, hidden: true
                },
                {
                    name: 'BOL', index: 'BOL', editable: false, sortable: false, hidden: false, align: 'center', search: false, title: false, formatter: this.formatBOL.bind(this), classes: 'printHide'//, width: 200
                },
                {
                    name: 'Comments', index: 'Comments', editable: false, hidden: false, align: 'center', classes: "textInDiv", search: false, formatter: this.formatComments.bind(this)
                },
                {
                    name: 'SequenceNum', index: 'SequenceNum', editable: false, hidden: true
                },
                {
                    name: 'IsConflictWithScheduleDate', index: 'IsConflictWithScheduleDate', editable: false, hidden: true
                },
                {
                    name: 'IsTouchMissed', index: 'IsTouchMissed', editable: false, hidden: true
                },
                {
                    name: 'TouchActionCategory', index: 'TouchActionCategory', editable: false, hidden: true
                },
                {
                    name: 'DispTouchActionCategory', index: 'DispTouchActionCategory', editable: false, hidden: true
                },
                {
                    name: 'BlankField', index: 'BlankField', editable: false, hidden: true, classes: 'printShow'
                },
                {
                    name: 'IsZippyShellQuote', index: 'IsZippyShellQuote', editable: false, hidden: true
                }
            ];

            let jqGridOptions: JQueryJqGridOptions = {
                datatype: "local"
                , data: data.rows
                , pager: 'listPager'
                , caption: "<strong>" + this.gridTitle + "&nbsp;-&nbsp;<span id='totalRecordsCount'>0</span></strong>"
                , colNames: columnNames
                , colModel: columnModel
                , rowNum: 100
                , rowList: [10, 50, 100, 500]
                //, sortname: 'TouchStatus'
                //, sortorder: "desc"
                , id: "RowID"
                , height: "100%"
                //, width: "100%"

                , viewrecords: true
                , autoWidth: true
                , gridview: true
                //, multiselect: true
                //, multiboxonly: true
                , width: null
                , shrinkToFit: true

                //toolbar : [true, "top"]
                , emptyrecords: 'no records found'
                , gridComplete: this.gridCompleteLocal.bind(this)
                , loadComplete: this.gridLoadComplete.bind(this)
                , loadError: this.gridLoadError.bind(this)
                //, ondblClickRow: this.gridDblClickRow.bind(this)
            };

            this.dsGrid.jqGrid(jqGridOptions).navGrid(
                this.dsGridList
                , {
                    edit: false, add: false, del: false, search: false, refresh: false
                }
                , null
                , null
                , null
            );

            this.dsGrid.jqGrid('sortGrid', 'AlertLevel', true, 'desc');

            this.dsGrid.jqGrid("setLabel", "StarsId", false, "printHide");
            this.dsGrid.jqGrid("setLabel", "Phonenumber", false, "printHide");
            this.dsGrid.jqGrid("setLabel", "QORId", false, "printHide");
            this.dsGrid.jqGrid("setLabel", "FirstName", false, "printHide");
            this.dsGrid.jqGrid("setLabel", "BOL", false, "printHide");
            this.dsGrid.jqGrid("setLabel", "BlankField", false, "printShow");
        }

        gridCompleteLocal(): void {
            $('#totalRecordsCount').html(this.dsGrid.jqGrid('getGridParam', 'records'));

            let isShowPriorNotCompletedTouches = $('#IsShowPriorNotCompletedTouches').is(':checked');

            var ids = this.dsGrid.getDataIDs();

            if (isShowPriorNotCompletedTouches == true) {
                for (var i = 0; i < ids.length; i++) {
                    var cell = this.dsGrid.getCell(ids[i], "Status");
                    var alertType = this.dsGrid.getCell(ids[i], "AlertLevel");

                    if (cell != WATouchStatus.Completed) {
                        if (alertType == 1)
                            this.dsGrid.jqGrid('setRowData', ids[i], false, { background: AlertColorCode.type1 });
                        if (alertType == 2)
                            this.dsGrid.jqGrid('setRowData', ids[i], false, { background: AlertColorCode.type2 });
                    }
                }
            }
        }

        gridLoadComplete(): void {
            if (this.dsGrid.getGridParam('records') === 0) {
            }

            ////SFERP-TODO-CTUPD
            //Below is dummy message created by Sohan for temprary purpose.
            $('.showDisableMsg').unbind('click').click((evt) => {
                evt.preventDefault();
                alert('TBD - Update Link to open the Salesforce page/document');
                return false;                
            });

            ////SFERP-TODO-CTUPD
            $('.orderEditAction').unbind('click').click((evt) => {
                evt.preventDefault();

                let currentElement = $(evt.currentTarget);
                let key = currentElement.data('key');

                this.editTouchDetails(key);
            });

            ////SFERP-TODO-CTUPD -- Update link from SalesForce - TBD
            $('.orderBOLAction').unbind('click').click((evt) => { 
                evt.preventDefault();

                let currentElement = $(evt.currentTarget);
                let qorId = currentElement.data('qorid');

                this.getBOLDocs(qorId);
            });

            ////SFERP-TODO-CTUPD
            $('.orderTouchTicketAction').unbind('click').click((evt) => { 
                alert("TBD - Disabled currently, Need to get data/pdf from Salesforce");
                return; 

                ////SFERP-TODO-CTUPD -- Need to uncomment below line - TBD.

                //evt.preventDefault();

                //let currentElement = $(evt.currentTarget);
                //let qorId = currentElement.data('qorid');
                //let touchType = currentElement.data('touchtype');
                //let seqNumber = currentElement.data('seqnumber');

                //this.getTouchTicket(qorId, touchType, seqNumber);
            });
        }

        gridLoadError(xhr: any, st: any, err: any): boolean {
            if (xhr.status == "200") return false;

            console.log("Load Error" + xhr.status + "-" + xhr.responseText);  //Testing purpose only

            var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                width: '600',
                buttons: {
                    Ok: function () {
                        //$(this).dialog("close");
                    }
                }
            });
        }
    }
}
$(document).ready(function () {
    var owaTouchesGrid = new waTouchesGridModule.waTouchesGrid();
    owaTouchesGrid.init();
});
