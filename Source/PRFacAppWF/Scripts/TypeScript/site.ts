﻿/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="Libraries\jquery.blockUI.d.ts" />;

module facilityApp {
    export class site {
        constructor() {
            this.initSiteEvents();
        }

        initSiteEvents() {
            window.onpageshow = function (event) {
                if (event.persisted) {
                    window.location.reload()
                }
            };

            $(document).ajaxStart($.blockUI)
                //.ajaxError(function (e, xhr, opts) {
                //if (options.SessionOut.StatusCode == xhr.status) {
                //    document.location.href = options.SessionOut.RedirectUrl;
                //    return;
                //}
                //})
                .ajaxStop($.unblockUI);

            $(".ui-draggable").draggable({
                handle: ".ui-draggable-handle"
            });
        }

        public static bindSpaceTripEvent() {
            $('.noEmptySpaceAllowed').keyup(function () {
                $(this).val($(this).val().replace(/ +?/g, ''));
            });
        }
        public static bindAcceptOnlyDecimalEvent() {
            $('.acceptOnlyDecimal').keyup(function () {
                var position = this.selectionStart - 1;
                //remove all but number and .
                var fixed = this.value.replace(/[^0-9\.]/g, '');
                if (fixed.charAt(0) === '.')                  //can't start with .
                {
                    //fixed = fixed.slice(1);
                    fixed = "0.";
                }

                var pos = fixed.indexOf(".") + 1;
                if (pos >= 0)               //avoid more than one .
                    fixed = fixed.substr(0, pos) + fixed.slice(pos).replace('.', '');

                if (this.value !== fixed) {
                    this.value = fixed;
                    this.selectionStart = position;
                    this.selectionEnd = position;
                }
            });
        }

        public static showError(outputMsg): void {
            this.alert(outputMsg, 'Error', undefined);
        }

        public static showAlert(outputMsg): void {
            this.alert(outputMsg, 'Alert', undefined);
        }

        public static showAlertRoleMismatch(outputMsg, title): void {
            this.alert(outputMsg, title, undefined);
        }

        public static showConfrimWithOkCallback(outputMsg, okCallbackMethod): void {
            this.confrim(outputMsg, 'Confirm', okCallbackMethod, undefined);
        }

        public static showConfrim(outputMsg, okCallbackMethod, cancelBackMethod): void {
            this.confrim(outputMsg, 'Confirm', okCallbackMethod, cancelBackMethod);
        }

        public static alert(outputMsg, titleMsg, onCloseCallback): void {
            if (!titleMsg)
                titleMsg = 'Alert';

            if (!outputMsg)
                outputMsg = 'Unexpected error occurred.';

            $("<div class='site-alert-popup'></div>").html(outputMsg).dialog({
                title: titleMsg,
                //resizable: false,
                modal: true,
                minWidth: 800,
                position: {
                    my: "center bottom",
                    at: "center top"
                },
                minHeight: 220,
                zIndex: 999999,
                stack: false,
                buttons: {
                    "OK": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                    if (onCloseCallback)
                        onCloseCallback();

                    /* Cleanup node(s) from DOM */
                    $(this).dialog('destroy').remove();
                },
                open: function (event, ui) {
                    $('.ui-widget-overlay').bind('click', function () { $(".site-alert-popup").dialog('close'); });
                   
                    $(this).closest(".ui-dialog")
                        .find(".ui-dialog-titlebar-close")
                        .removeClass("ui-dialog-titlebar-close")
                        .addClass("ui-dialog-titlebar-closeCustom");
                }
            });
        }

        public static confrim(outputMsg, titleMsg, onOkCallback, onCancelCallbak): void {
            if (!titleMsg)
                titleMsg = 'Confrim';

            if (!outputMsg)
                outputMsg = 'Unexpected error occurred.';

            $("<div class='site-alert-popup'></div>").html(outputMsg).dialog({
                title: titleMsg,
                //resizable: false,
                position: {
                    my: "center bottom",
                    at: "center top"
                },
                modal: true,
                minWidth: 800,
                //position: 'top',
                minHeight: 220,
                zIndex: 999999,
                stack: false,
                buttons: {
                    "Continue": function () {
                        if (onOkCallback)
                            onOkCallback();

                        $(this).dialog("close");
                    },
                    "Cancel": function () {
                        if (onCancelCallbak)
                            onCancelCallbak();

                        $(this).dialog("close");
                    }
                },
                close: function () {
                    /* Cleanup node(s) from DOM */
                    $(this).dialog('destroy').remove();
                },
                open: function (event, ui) {
                    $(this).closest(".ui-dialog")
                        .find(".ui-dialog-titlebar-close")
                        .removeClass("ui-dialog-titlebar-close")
                        .addClass("ui-dialog-titlebar-closeCustom");
                }
            });
        }

    }
}

function require(path: string) { }

interface Date {
    toDateMMDDYYYY(): string;
    addDays(days: number): Date;
}

interface StringConstructor {
    isNullOrEmpty: (val: any) => boolean;
    hasWhiteSpace: (val: any) => boolean;
}

String.isNullOrEmpty = function (val: any): boolean {
    if (val === undefined || val === null || val.trim() === '') {
        return true;
    }
    return false;
};

String.hasWhiteSpace = function (val: any): boolean {
    return val.indexOf(' ') >= 0;
}

Date.prototype.toDateMMDDYYYY = function () {
    var year = this.getFullYear();
    var month = (1 + this.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = this.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;

    //return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
}

Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + days);

    var dd = this.getDate();
    var mm = this.getMonth() + 1;
    var y = this.getFullYear();

    return new Date(mm + '/' + dd + '/' + y);
}

$(document).ready(function () {
    new facilityApp.site();
});