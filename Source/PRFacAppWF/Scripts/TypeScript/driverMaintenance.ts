﻿/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="driverMaintenanceForm.ts" />
/// <reference path="driverSchedule.ts" />
/// <reference path="site.ts" />

module driverMaintenanceModule {
    export class URLHelper {
        public static getDriverScheduleList: string = '';
    }

    export class driverMaintenance {
        btnSearch: HTMLElement;
        btnReset: HTMLElement;
        btnAddNewUser: HTMLElement;
        btnPrint: HTMLElement;
        imgStartDateIcon: HTMLElement;
        driverMaintenanceFormObj: any;
        imgNextBtn: any;
        imgPreviousBtn: any;
        aEditDetails: HTMLElement;

        constructor() {
            this.driverMaintenanceFormObj = new driverMaintenanceFormModule.driverMaintenanceForm();

            this.btnSearch = document.getElementById('btnSearch');
            this.btnReset = document.getElementById('btnReset');
            this.btnAddNewUser = document.getElementById('btnAddNewUser');
            this.btnPrint = document.getElementById('btnPrint');
            this.imgStartDateIcon = document.getElementById('StartDateIcon');
            let txtStartDate = $('#StartDate');
            this.btnSearch.onclick = (e) => { e.preventDefault(); this.validateAndSubmit(); }
            this.btnReset.onclick = (e) => { e.preventDefault(); this.resetForm(); }
            this.btnAddNewUser.onclick = (e) => {
                e.preventDefault();

                let driverMaintenanceFormObjLocal = new driverMaintenanceFormModule.driverMaintenanceForm();
                driverMaintenanceFormObjLocal.showEditPopup(0, "", this);
            }

            this.btnPrint.onclick = (e) => {
                e.preventDefault();

                this.handlePrintFuncationality();
            }
            //this.assignEditMethodToBtn();

            this.imgStartDateIcon.onclick = (e) => { e.preventDefault(); txtStartDate.focus(); }
            txtStartDate.datepicker({});

            //window.onresize = this.adjustGridHeight;
        }

        handlePrintFuncationality(): void {
            var data = $('.cust-grid-jqgrid').clone();// $('.tblDriverScheduleHrs').html();
            data.find('#dvDriverScheduleList').height('100%');
            data.find('#dvDriverScheduleList').css('overflow', 'visible');
            data.find('tbody').css('overflow', 'visible');
            data.find('th').each(function () { $(this).css('top', '0px'); });
            data.find('a').each(function () {
                $(this).removeAttr("href");
            });
            data.find('#imgPrevious').remove();
            data.find('#imgNext').remove();            
            var mywindow = window.open('', 'Packrat Drivers', 'height=400,width=600');
            mywindow.document.write('<html><head><title>Packrat Drivers</title>');
            mywindow.document.write('<link href="/Content/css/footable.core.css" rel="stylesheet" media="print"/><link href="/Content/css/footable.standalone.css" rel="stylesheet" media="print" /> <style type="text/css" >  @page { size: landscape; } @media print { table thead tr th.center,  table tbody tr td.center { text-align: center !important; } table th, table td { border:1px solid #ccc; padding: 0.005em; } } </style>');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data.html());
            mywindow.document.write('</body></html>');

            mywindow.print();
            mywindow.close();
        }

        resetForm(): void {
            let reqDate = (new Date()).toDateMMDDYYYY();
            let request = {
                StartDate: reqDate
            }

            if (request.StartDate != undefined) {
                this.sendRequest(request);
                $('#StartDate').val(reqDate);
            }
        }

        onDriverScheduleGridLoad(): void {
            this.imgNextBtn = document.getElementById('imgNext');
            this.imgNextBtn.onclick = (e) => { e.preventDefault(); this.nextAndSubmit(); }

            this.imgPreviousBtn = document.getElementById('imgPrevious');
            this.imgPreviousBtn.onclick = (e) => { e.preventDefault(); this.previousAndSubmit(); }

            //this.adjustGridHeight();
        }

        //adjustGridHeight(): void {
            //var wheight = (window.innerHeight - 410);
            //$('#dvDriverScheduleList').css("height", wheight);
        //}

        public validateAndSubmit(): void {
            var request = {
                StartDate: $('#StartDate').val()
            }

            if (request.StartDate != undefined)
                this.sendRequest(request);
            else
                facilityApp.site.showError("Please select date and Submit.");
        }

        nextAndSubmit(): void {
            let StartDate = new Date($('#StartDate').val());
            StartDate.setDate(StartDate.getDate() + 7)

            let reqDate = (StartDate.getMonth() + 1) + "/" + StartDate.getDate() + "/" + StartDate.getFullYear();

            var request = {
                StartDate: reqDate
            }

            if (request.StartDate != undefined) {
                this.sendRequest(request);
                $('#StartDate').val(reqDate);
                this.assignEditMethodToBtn();
            }
        }

        previousAndSubmit(): void {
            let StartDate = new Date($('#StartDate').val());
            StartDate.setDate(StartDate.getDate() - 7)

            let reqDate = (StartDate.getMonth() + 1) + "/" + StartDate.getDate() + "/" + StartDate.getFullYear();

            var request = {
                StartDate: reqDate
            }

            if (request.StartDate != undefined) {
                this.sendRequest(request);
                $('#StartDate').val(reqDate);
                this.assignEditMethodToBtn();
            }
        }

        sendRequest(postData) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: driverMaintenanceModule.URLHelper.getDriverScheduleList,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successCallback.bind(this))
                .fail(this.errorCallback)
                .done(this.onDriverScheduleGridLoad.bind(this));
        }

        successCallback(result: any) {
            if (result == "error") {
                facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
            } else {
                $('#dvDriverScheduleList').html(result);

                this.assignEditMethodToBtn();

                //fixed header
                $(".new-fixed-header").tableHeadFixer();
            }
            $.blockUI;
        }

        errorCallback(result: any) {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        }

        assignEditMethodToBtn(): void {
            $('.editDriverDetails').click((evt) => {
                evt.preventDefault();

                let currentTarget = $(evt.currentTarget);
                let driverId = currentTarget.attr('data-driverId');
                let drivername = currentTarget.attr('data-driverfullname');
                //let driverMaintenanceFormObjLocal = new driverMaintenanceFormModule.driverMaintenanceForm();
                //driverMaintenanceFormObjLocal.showEditPopup(driverId, drivername, this, driverScheduleModule.editDriverOptions.driverScheduleHours);
                let thStartDate = $('#StartDate').val();
                let driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
                driverMaintenanceFormObjLocal.showDriverSchedulePopup(driverId, drivername, thStartDate, driverScheduleModule.editDriverOptions.driverInformation, this);
            });

            $('.editDriverLicenseDetails').click((evt) => {
                evt.preventDefault();

                let currentTarget = $(evt.currentTarget);
                let driverId = currentTarget.attr('data-driverId');
                let drivername = currentTarget.attr('data-driverfullname');

                //let driverMaintenanceFormObjLocal = new driverMaintenanceFormModule.driverMaintenanceForm();
                //driverMaintenanceFormObjLocal.showEditPopup(driverId, drivername, this, driverScheduleModule.editDriverOptions.driverScheduleHours);
                let thStartDate = $('#StartDate').val();
                let driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
                driverMaintenanceFormObjLocal.showDriverSchedulePopup(driverId, drivername, thStartDate, driverScheduleModule.editDriverOptions.driverInformation, this);
            });
            

            $('.editDriverScheduleHours').click((evt) => {
                evt.preventDefault();

                let currentTarget = $(evt.currentTarget);
                let driverId = currentTarget.attr('data-driverId');
                let drivername = currentTarget.attr('data-driverfullname');

                //let thisTd = $(evt.currentTarget).closest('td');
                //var thStartDate = thisTd.closest('table').find('th').eq(thisTd.index()).text();

                var thStartDate = currentTarget.attr('data-date');
                var datastatus = currentTarget.attr('data-status');
                //console.log('111111111111111111 : ' + thStartDate);

                var editDriverOptions: driverScheduleModule.editDriverOptions = (datastatus == "RoverOut" ? driverScheduleModule.editDriverOptions.rover : driverScheduleModule.editDriverOptions.driverScheduleHours);

                let driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
                driverMaintenanceFormObjLocal.showDriverSchedulePopup(driverId, drivername, thStartDate, editDriverOptions, this);
            });

            $('.driver-schedule-truckstatistics').click((evt) => {
                evt.preventDefault();

                let currentTarget = $(evt.currentTarget).find('img');
                let tmp = currentTarget.attr('src');
                currentTarget.attr('src', currentTarget.attr('data-togglesrc'));
                currentTarget.attr('data-togglesrc', tmp);

                $('.truck-totalhours').toggleClass('hide');
            });
        }
    }
}

$(document).ready(function () {
    var oDM = new driverMaintenanceModule.driverMaintenance();

    oDM.validateAndSubmit();
});