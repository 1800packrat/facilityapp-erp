﻿/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqgrid.d.ts" />
/// <reference path="libraries\jquery.validation.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="libraries\jquery.tmpl.d.ts" />
/// <reference path="libraries\error.messages.d.ts" />
/// <reference path="driverMaintenance.ts" />

module driverScheduleModule {
    export class URLHelper {
        public static loadDriverScheduleInfo: string = '';
        public static getDriverScheduleHours: string = '';
        public static saveDriverScheduleHours: string = '';
        public static getDriversList: string = '';
        public static saveWeeklyDriverSdhedule: string = '';
        public static saveDriverRoverTransfer: string = '';
        public static saveDriverPremanentTransfer: string = '';
        public static getDriverRoverFacility: string = '';
        public static updateDriverRoverFacilityInfo: string = '';
        public static checkAndGetDriverLockedLoadsByDate: string = '';
        public static checkAndGetDriverLockedLoadsByDateForWeeklySchedule: string = '';
    }

    export interface gridData {
        rows: any;
        total: number;
        page: number;
        records: number;
    }

    export enum editDriverOptions {
        driverInformation = 1,
        driverScheduleHours = 2,
        rover = 3,
        transfer = 4,
    }
    //copied from enum AccessLevels
    export enum accessLevels {
        PrimaryFacility = 4,
        Rover = 6
    }

    export class driverSchedule {
        public gridId: string = "list";
        public gridListId: string = "listPager";
        public dsGrid: JQuery = null;
        public dsGridList: JQuery = null;
        public driverId: number;
        //public driverName: string;
        public thStartDate: any;
        public ddlDriverList: any;
        public updateDialog: any;

        public ddlEditDriverOptions: any;

        public editDriverInformation: JQuery;
        public editDriverRover: JQuery;
        public editDriverTransfer: JQuery;
        public editDriverScheduleHours: JQuery;

        public btnSaveEditDriverInfo: JQuery;

        public modalUpdateselectedhours: JQuery;
        public editDriverScheduleHoursTabs: JQuery;
        public editDriverRoverFacilityTabs: JQuery;

        public addDriverWeeklySchedule: any;

        //public modalSchduleHours: any;

        public tmplAddWeeklyDriverSchedule: JQuery;
        public tBodyDriverWeeklyScheduleHours: JQuery;

        imgStartDateIcon: HTMLElement;
        imgToDateIcon: HTMLElement;

        driverRoverSendToDateIcon: HTMLElement;
        driverRoverBackonDateIcon: HTMLElement;
        ddlDriverRoverFacilites: any;

        permanentDriverTransferDateIcon: HTMLElement;
        ddlDriverTransferPermFacilites: any;
        ddlWeeklyDriverScheduleType: any;

        driverWeeklyScheduleFromDate: any;
        driverWeeklyScheduleToDate: any;

        noneReasonDriverScheduleMsg: JQuery;
        tBodyDriverRoverFacilityList: JQuery;

        modalJqgridClose: HTMLElement;

        //This variable will be used to check driver has locked loads for a given day or not
        hdnDriverLockedLoadDates = [];

        constructor() {
            this.dsGrid = $("#" + this.gridId);

            //this.ddlDriverList = document.getElementById('ddlDriversList');

            this.ddlEditDriverOptions = document.getElementById('ddlEditDriverOptions');
            this.editDriverInformation = $('#editDriverInformation');
            this.editDriverRover = $('#editDriverRover');
            this.editDriverTransfer = $('#editDriverTransfer');
            this.editDriverScheduleHours = $('#editDriverScheduleHours');
            this.noneReasonDriverScheduleMsg = $('#noneReasonDriverScheduleMsg');

            this.editDriverScheduleHoursTabs = $('#editDriverScheduleHoursTabs');
            this.modalUpdateselectedhours = $('#modalUpdateselectedhours');

            //this.ddlDriverList.onchange = (e) => { e.preventDefault(); this.loadGridOnDriverChange(e); };
            this.ddlEditDriverOptions.onchange = (e) => { e.preventDefault(); this.onChangeEditDriverOptions(e); };

            this.btnSaveEditDriverInfo = $('#modalSaveEditDriverInfo');
            this.btnSaveEditDriverInfo.unbind("click").click((e) => {
                e.preventDefault();
                this.onClickSaveDriverInfo();
            });

            let driverScheduleTabOptions: JQueryUI.TabsOptions = {
                activate: this.onChangeDriverScheduleTabOptions.bind(this),
            };
            this.editDriverScheduleHoursTabs.tabs(driverScheduleTabOptions);

            //this.modalSchduleHours = document.getElementById('modalSchduleHours');
            //this.modalSchduleHours.onclick = (e) => { e.preventDefault(); this.onsaveWeeklyScheduleHours(); };

            this.addDriverWeeklySchedule = document.getElementById('addDriverWeeklySchedule');
            this.addDriverWeeklySchedule.onclick = (e) => { e.preventDefault(); this.onClickAddDriverWeeklySchedule(); };

            this.tmplAddWeeklyDriverSchedule = $('#tmplAddWeeklyDriverSchedule');
            this.tBodyDriverWeeklyScheduleHours = $('#tBodyDriverWeeklyScheduleHours');

            this.driverWeeklyScheduleFromDate = $('#WeeklyScheduleFromDate');
            this.imgStartDateIcon = document.getElementById('FromDateIcon');
            this.imgStartDateIcon.onclick = (e) => { e.preventDefault(); this.driverWeeklyScheduleFromDate.focus(); }
            this.driverWeeklyScheduleFromDate.datepicker({
                minDate: new Date(),
                onSelect: this.onChangeDriverWeeklyScheduleFromDate.bind(this)
            });

            this.driverWeeklyScheduleToDate = $('#WeeklyScheduleToDate');
            this.imgToDateIcon = document.getElementById('ToDateIcon');
            this.imgToDateIcon.onclick = (e) => { e.preventDefault(); this.driverWeeklyScheduleToDate.focus(); }
            this.driverWeeklyScheduleToDate.datepicker({
                minDate: new Date(),
                onSelect: this.onChangeDriverWeeklyScheduleToDate.bind(this)
            });

            //Rover date pickers and other functionalities
            let txtRoverToDate = $('#driverRoverSendToDate');
            this.driverRoverSendToDateIcon = document.getElementById('driverRoverSendToDateIcon');
            this.driverRoverSendToDateIcon.onclick = (e) => { e.preventDefault(); txtRoverToDate.focus(); }
            txtRoverToDate.datepicker({
                minDate: new Date(),
                onSelect: function (dateText, inst) {
                    txtRoverBackDate.datepicker("option", "minDate", txtRoverToDate.datepicker("getDate"));
                    this.AddRoverHintMessage();
                }.bind(this)
            });

            let txtRoverBackDate = $('#driverRoverBackOnDate');
            this.driverRoverBackonDateIcon = document.getElementById('driverRoverBackOnDateIcon');
            this.driverRoverBackonDateIcon.onclick = (e) => { e.preventDefault(); txtRoverBackDate.focus(); }
            txtRoverBackDate.datepicker({
                onSelect: function (dateText, inst) {
                    txtRoverToDate.datepicker("option", "maxDate", txtRoverBackDate.datepicker("getDate"));
                    this.AddRoverToHintMessage();
                }.bind(this)
            });

            this.ddlDriverRoverFacilites = document.getElementById('ddlDriverRoverFacilites');
            this.ddlDriverRoverFacilites.onchange = (e) => { e.preventDefault(); this.onChangeDriverRoverOptions(e, this); };

            this.ddlWeeklyDriverScheduleType = document.getElementById('ddlWeeklyDriverScheduleType');
            this.ddlWeeklyDriverScheduleType.onchange = (e) => { e.preventDefault(); this.onChangeWeeklyDriverScheduleType(e); };

            //Premanent transfer date picker
            let txtTransferToDate = $('#permanentDriverTransferDate');
            this.permanentDriverTransferDateIcon = document.getElementById('permanentDriverTransferDateIcon');
            this.permanentDriverTransferDateIcon.onclick = (e) => { e.preventDefault(); txtTransferToDate.focus(); }
            txtTransferToDate.datepicker({
                minDate: new Date(),
                onSelect: function (dateText, inst) {
                    let driverName = $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html();
                    $('#dvDriverTransMessage').html("* <strong>" + driverName + "</strong> will be available on <strong>" + new Date(dateText).toDateMMDDYYYY() + "</strong>");
                }
            });

            this.ddlDriverTransferPermFacilites = document.getElementById('ddlDriverTransferPermFacilites');
            this.ddlDriverTransferPermFacilites.onchange = (e) => { e.preventDefault(); this.onChangeDriverTransferOptions(e); };

            this.tBodyDriverRoverFacilityList = $('#tBodyDriverRoverFacilityList');

            this.editDriverRoverFacilityTabs = $('#editDriverRoverFacilityTabs');
            this.editDriverRoverFacilityTabs.removeClass('ui-widget');
            let roverTabOptions: JQueryUI.TabsOptions = {
                activate: this.onChangeRoverTabOptions.bind(this)
            };
            this.editDriverRoverFacilityTabs.tabs(roverTabOptions);

            this.modalJqgridClose = document.getElementById('modalJqgridClose');
            this.modalJqgridClose.onclick = (e) => {
                let driverMaintenanceObjLocal = new driverMaintenanceModule.driverMaintenance();
                driverMaintenanceObjLocal.validateAndSubmit();
            };
        }

        onChangeWeeklyDriverScheduleType(evet): void {
            let currentTarget = $(evet.currentTarget);
            let selectedOpt = $("option:selected", currentTarget);
            let FromDate = new Date(this.driverWeeklyScheduleFromDate.val());
            let noOfDaystoAdd = selectedOpt.data("durationdays");
            let ToDate = new Date(FromDate).addDays(noOfDaystoAdd);
            //ToDate.setDate(FromDate.getDate() + noOfDaystoAdd);


            this.driverWeeklyScheduleFromDate.val(FromDate.toDateMMDDYYYY());
            this.driverWeeklyScheduleToDate.val(ToDate.toDateMMDDYYYY());
        }

        onChangeDriverWeeklyScheduleFromDate(date): void {
            this.ddlWeeklyDriverScheduleType.selectedIndex = 0;
            this.driverWeeklyScheduleToDate.datepicker("option", "minDate", date);
        }

        onChangeDriverWeeklyScheduleToDate(date): void {
            this.ddlWeeklyDriverScheduleType.selectedIndex = 0;
            this.driverWeeklyScheduleFromDate.datepicker("option", "maxDate", date);
        }

        onChangeDriverRoverOptions(evet, parent): void {
            let currentTarget = $(evet.currentTarget)
            if ($(currentTarget).val() != "-1") {
                $('#lblDriverRoverSendTo').text($('option:selected', this.ddlDriverRoverFacilites).text());
            }
            else {
                $('#lblDriverRoverSendTo').text('Facility');
            }
            parent.AddRoverHintMessage();
        }

        onChangeDriverTransferOptions(event): void {
            let currentTarget = $(event.currentTarget)
            if ($(currentTarget).val() != "-1") {
                $('#lblDriverTransferPermArriveAt').text($('option:selected', this.ddlDriverTransferPermFacilites).text());
            }
            else {
                $('#lblDriverTransferPermArriveAt').text('Facility');
            }
        }

        AddRoverHintMessage(): void {
            var selectedFacilityId = $('#ddlDriverRoverFacilites').val();
            var sendDate = $('#driverRoverSendToDate').val();
            var msgDisplay = '';

            if (selectedFacilityId && selectedFacilityId > 0 && sendDate.length > 0) {
                let selectedFacilityName = $('option:selected', this.ddlDriverRoverFacilites).text();

                let driverName = $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html();
                msgDisplay = "* <strong>" + driverName + "</strong> will be available in <strong>" + selectedFacilityName + "</strong> on <strong>" + sendDate + "</strong>";
            }

            $('#dvDriverRoverMessage').html(msgDisplay);
        }

        AddRoverToHintMessage(): void {
            var selectedFacilityId = $('#ddlDriverRoverFacilites').val();
            var sendDate = $('#driverRoverBackOnDate').val();
            var msgDisplay = '';

            if (selectedFacilityId && selectedFacilityId > 0 && sendDate.length > 0) {
                let selectedFacilityName = $('#lblDriverRoverBackOn', $('#addrover-driver-schedule')).text();

                let driverName = $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html();
                msgDisplay = "* <strong>" + driverName + "</strong> will be available in <strong>" + selectedFacilityName + "</strong> on <strong>" + sendDate + "</strong>";
            }

            $('#dvDriverRoverToMessage').html(msgDisplay);
        }

        onChangeDriverScheduleTabOptions(evt, ui): void {
            this.modalUpdateselectedhours.hide();
            this.btnSaveEditDriverInfo.hide();
            //console.log(evt);
            //console.log(ui.newTab.index());

            //Need to check with user waether to reset form or not
            if (ui.newTab.index() == 1) ///If index is 1 => means day wise schedule shows in the UI
            {
                var width = window.innerWidth * 0.85;
                this.dsGrid.jqGrid('setGridWidth', width);

                this.reloadDSGrid();

                this.modalUpdateselectedhours.show();
            } else if (ui.newTab.index() == 0) {
                this.btnSaveEditDriverInfo.show();
            }
        }

        onChangeRoverTabOptions(evt, ui): void {

            //Need to check with user waether to reset form or not
            if (ui.newTab.index() == 0) ///If index is 1 => means day wise schedule shows in the UI
            {
                //alert('refresh add form');
                this.resetRoverTransferForm();
                this.btnSaveEditDriverInfo.show();
            } else if (ui.newTab.index() == 1) {
                this.getDriverRoverFacilities();

                this.btnSaveEditDriverInfo.hide();
            }
        }

        getDriverRoverFacilities(): void {
            var data = { DriverId: this.driverId };

            this.sendRequestToGetDriverRoverFacilitiesXHR(data);
        }

        sendRequestToGetDriverRoverFacilitiesXHR(postData) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: driverScheduleModule.URLHelper.getDriverRoverFacility,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbackGetDriverRoverFacilities.bind(this))
                .fail(this.errorCallbackGetDriverRoverFacilities);
        }

        successCallbackGetDriverRoverFacilities(result: any) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                //this.editDriverInformation.html(result);
                this.bindDriverRoverFacilityDataToUI(result.data);
            }
            $.blockUI;
        }

        errorCallbackGetDriverRoverFacilities(result: any) {
            facilityApp.site.showError('Error occurred while fetching driver rover facility information. Please reload the page and try again!');
        }

        bindDriverRoverFacilityDataToUI(html): void {
            this.tBodyDriverRoverFacilityList.html(html);

            //Bind events for edit and save
            //$(".icon-driverroverfacility-add", this.tBodyDriverRoverFacilityList).click((e) => { e.preventDefault(); this.inlineAddDriverFacility(e); });
            $(".icon-driverroverfacility-edit", this.tBodyDriverRoverFacilityList).click((e) => { e.preventDefault(); this.inlineEditDriverRoverFacility(e); });
            $(".icon-driverroverfacility-delete", this.tBodyDriverRoverFacilityList).click((e) => { e.preventDefault(); this.inlineDeleteDriverRoverFacility(e); });
            $(".icon-driverroverfacility-save", this.tBodyDriverRoverFacilityList).click((e) => { e.preventDefault(); this.inlineSaveDriverRoverFacility(e); });
            $(".icon-driverroverfacility-cancel", this.tBodyDriverRoverFacilityList).click((e) => { e.preventDefault(); this.inlineCacnelDriverRoverFacility(e); });

            $(".icon-driverroverfacility-StartDate", this.tBodyDriverRoverFacilityList).click((e) => { e.preventDefault(); this.inlineStartDateRoverCalendar(e); });
            $(".icon-driverroverfacility-EndDate", this.tBodyDriverRoverFacilityList).click((e) => { e.preventDefault(); this.inlineEndDateRoverCalendar(e); });

            $(".txt-driverroverfacility-StartDate", this.tBodyDriverRoverFacilityList).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
            $(".txt-driverroverfacility-EndDate", this.tBodyDriverRoverFacilityList).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
        }

        inlineSaveDriverRoverFacility(evt): void {
            let currentTarget = $(evt.currentTarget);
            let thisTr = $(evt.currentTarget).closest('tr');
            let index = currentTarget.attr('data-index');

            let startDate = $(".driverroverfacility-driverstartdate", thisTr).val();
            let endDate = $(".driverroverfacility-driverenddate", thisTr).val();

            let dtstartDate = new Date(startDate);
            let dtendDate = new Date(endDate);

            if (dtstartDate > dtendDate) {
                facilityApp.site.showError('End Date should be greater than Start Date.');
                return;
            }

            let postData = {
                DriverId: $(".driverroverfacility-driverid", thisTr).val(),
                UserFacilityID: $(".driverroverfacility-userfacilityid", thisTr).val(),
                StoreRowID: $("option:selected", $(".driverroverfacility-storerowid", thisTr)).data('storerowid'),
                DriverStartDate: startDate,
                DriverEndDate: endDate,
                DFStatus: driverMaintenanceFormModule.DriverFacilityStatus.Updated
            };

            this.sendRequestInlineSaveDriverRoverFacilityXHR(postData);
        }

        inlineDeleteDriverRoverFacility(evt): void {
            if (confirm('Are you sure do you want to delete this record?')) {
                let currentTarget = $(evt.currentTarget);
                let thisTr = $(evt.currentTarget).closest('tr');
                let index = currentTarget.attr('data-index');

                let postData = {
                    DriverId: $(".driverroverfacility-driverid", thisTr).val(),
                    UserFacilityID: $(".driverroverfacility-userfacilityid", thisTr).val(),
                    StoreRowID: $("option:selected", $(".driverroverfacility-storerowid", thisTr)).data('storerowid'),
                    DriverStartDate: $(".driverroverfacility-driverstartdate", thisTr).val(),
                    DriverEndDate: new Date().toDateMMDDYYYY(),
                    DFStatus: driverMaintenanceFormModule.DriverFacilityStatus.Deleted
                };

                this.sendRequestInlineSaveDriverRoverFacilityXHR(postData);
            }
        }

        sendRequestInlineSaveDriverRoverFacilityXHR(postData) {

            let ajaxSettings: JQueryAjaxSettings = {
                url: driverScheduleModule.URLHelper.updateDriverRoverFacilityInfo
                , type: 'POST'
                , traditional: true
                , data: JSON.stringify(postData)
                , dataType: "json"
                , contentType: 'application/json; charset=utf-8'
                , async: true
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbackInlineSaveDriverRoverFacility.bind(this))
                .fail(this.errorCallbackInlineSaveDriverRoverFacility);
        }

        successCallbackInlineSaveDriverRoverFacility(result: any) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                this.bindDriverRoverFacilityDataToUI(result.data);
            }
            $.blockUI;
        }

        errorCallbackInlineSaveDriverRoverFacility(result: any) {
            facilityApp.site.showError('Error occurred while fetching driver rover facility information. Please reload the page and try again!');
        }

        inlineEditDriverRoverFacility(evt): void {
            var inEditMode = $('span[class*="icon-driverroverfacility-save"]', this.tBodyDriverRoverFacilityList).filter(function (index) {
                if ($(this).css('display') == 'inline') return $(this);
            });

            if (inEditMode.length == 0) {
                let currentTarget = $(evt.currentTarget);
                //let driverFacilityId = currentTarget.attr('data-driverfacility');
                let thisTr = $(evt.currentTarget).closest('tr');
                let index = currentTarget.attr('data-index');
                $('.viewData', thisTr).hide();
                $('.editData', thisTr).show();
                $('.icon-driverroverfacility-edit', thisTr).hide();
                $('.icon-driverroverfacility-delete', thisTr).hide();
                currentTarget.hide();
                $(".actions ul li:nth-child(3)").addClass("disabled").attr("aria-disabled", "true").addClass("disable-anchor-click");
                $('.icon-driverroverfacility-save', thisTr).show();
                $('.icon-driverroverfacility-cancel', thisTr).show();

                var rowstartDate = new Date($('#DriverFacility\\[' + index + '\\]\\.DriverStartDate', this.tBodyDriverRoverFacilityList).val());
                var now = new Date();
                now.setHours(0, 0, 0, 0);
                if (rowstartDate < now) {
                    // selected date is in the past
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNo', this.tBodyDriverRoverFacilityList).prop('disabled', true)
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel', this.tBodyDriverRoverFacilityList).prop('disabled', true)
                }
                else {
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNo', this.tBodyDriverRoverFacilityList).prop('disabled', false)
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel', this.tBodyDriverRoverFacilityList).prop('disabled', false)
                }
            }
            else {
                facilityApp.site.showError('Please save / discard existing changes!');
            }
        }

        inlineStartDateRoverCalendar(evt): void {
            let currentTarget = $(evt.currentTarget);
            let thisTr = $(evt.currentTarget).closest('td');
            let txtStartDate = $(thisTr).find('.txt-driverroverfacility-StartDate');
            let canEditStartdate = $(txtStartDate).attr('data-canedit');
            if (canEditStartdate == "True") {
                $(txtStartDate).focus();
                $(txtStartDate).datepicker({ minDate: 0 });
            }
        }

        inlineEndDateRoverCalendar(evt): void {
            let currentTarget = $(evt.currentTarget);
            let thisTr = $(evt.currentTarget).closest('td');
            let txtEndDate = $(thisTr).find('.txt-driverroverfacility-EndDate');
            $(txtEndDate).focus();
            $(txtEndDate).datepicker({ minDate: 0 });
        }

        dateBeforeShow(ele, inst): any {
            let canEdit = $(ele).attr('data-canedit');
            return canEdit == "True";
        }

        inlineCacnelDriverRoverFacility(evt): void {

            let currentTarget = $(evt.currentTarget);
            //let driverFacilityId = currentTarget.attr('data-driverfacility');
            let thisTr = $(evt.currentTarget).closest('tr');
            let index = currentTarget.attr('data-index');
            $('.viewData', thisTr).show();
            $('.editData', thisTr).hide();
            $('.icon-driverroverfacility-edit', thisTr).show();
            $('.icon-driverroverfacility-delete', thisTr).show();

            currentTarget.hide();

            $('.icon-driverroverfacility-save', thisTr).hide();
            $('.icon-driverroverfacility-cancel', thisTr).hide();
            if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() == 'New') {
                thisTr.remove();
            }

            var inEditMode = $('span[class*="icon-driverroverfacility-save"]', this.tBodyDriverRoverFacilityList).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });

            if (inEditMode.length == 0) {
                $(".actions ul li:nth-child(3)").attr("aria-disabled", "false").removeClass("disabled").removeClass("disable-anchor-click");
            }

        }

        public onClickAddDriverWeeklySchedule(): void {
            //To build new row we need index, that index we are maintiaining in tbody tag. This needs to be increased when user adds new row
            let maxrowindex = this.tBodyDriverWeeklyScheduleHours.data('maxrowindex');

            ///Dont add more than 7 rows to schedule, because per week only 7 days if he wants to add different schedule for all days
            if (maxrowindex >= 6) {
                facilityApp.site.showError("Maximun number of rows added to schedule.");
                return;
            }

            let weeklySch = { index: ++maxrowindex };

            this.tBodyDriverWeeklyScheduleHours.append(this.tmplAddWeeklyDriverSchedule.tmpl(weeklySch));
            this.tBodyDriverWeeklyScheduleHours.data('maxrowindex', maxrowindex);

            //$('.timepicker', this.tBodyDriverWeeklyScheduleHours).timepicker({
            //    timeFormat: 'h:mm p',
            //    minTime: '12:00 AM',
            //    maxTime: '11:30 PM',
            //    interval: 15,
            //    change: this.onChangeWeeklyTime
            //});
            $('.timepicker', this.tBodyDriverWeeklyScheduleHours).timepicker({
                minTime: '12:00 AM', // 11:45:00 AM,
                maxTime: '11:30 PM',
                timeFormat: 'h:i A',
                step: 15
            });
            $('.timepicker', this.tBodyDriverWeeklyScheduleHours).unbind('changeTime').on('changeTime', this.onChangeWeeklyTime);


            ///Unbind previous event and try to bind it again, other wise event may fire multiple times
            $('.weeklyscheduleday', this.tBodyDriverWeeklyScheduleHours).unbind("change").change((e) => {
                e.preventDefault();
                this.onChangeWeeklyScheduleCheckBox(e);
            });

            ///Unbind previous event and try to bind it again, other wise event may fire multiple times
            $('.weeklyscheduleselectall', this.tBodyDriverWeeklyScheduleHours).unbind("click").click((e) => {
                e.preventDefault();
                this.onChangeWeeklyScheduleSelectAll(e);
            });

            ///Unbind previous event and try to bind it again, other wise event may fire multiple times
            $('.weeklyschedue-daywork', this.tBodyDriverWeeklyScheduleHours).unbind("change").change((e) => {
                e.preventDefault();
                this.onChangeWeeklyScheduleStatusChange(e);
            });
        }

        public onChangeWeeklyScheduleStatusChange(evt): void {
            let currentTarget = $(evt.currentTarget);
            let thisTable = currentTarget.closest('table');
            var thisTr = currentTarget.closest('tr');

            if (currentTarget.val() == 'S') {
                $('.starttime-picker', thisTr).val("7:00 AM").change();
                $('.endtime-picker', thisTr).val("5:00 PM").change();
            } else
                $('.timepicker', thisTr).val("12:00 AM").change();
        }

        public onChangeWeeklyScheduleSelectAll(evt): void {
            let currentTarget = $(evt.currentTarget);
            let ischecked = currentTarget.is(':checked');
            let thisTable = currentTarget.closest('table');
            var thisTr = currentTarget.closest('tr');

            let rowIndex = thisTr.data('rowindex');
            let excludetrclassname = 'weeklyscheduleselectall_' + rowIndex;

            $('.weeklyscheduleday', thisTr).each(function (i, e) {
                var thisElementClass = e.className.split(' ').filter(function (v, ind, arr) { return v.indexOf("weeklyschedule-") == 0; });

                $.each(thisElementClass, function (ia, clname) {
                    let anyCheckedOtherThisElement = $('.' + clname).not($('.' + clname, thisTr));

                    if (!(anyCheckedOtherThisElement && anyCheckedOtherThisElement.is(':checked') == true)) {
                        $('.' + clname, thisTr).prop("checked", true).trigger('change');
                    }
                });
            });

            /*
            if (ischecked) {
                let rowIndex = thisTr.data('rowindex');
                let excludetrclassname = 'weeklyscheduleselectall_' + rowIndex;
                //Uncheck other select all box
                $('.weeklyscheduleselectall:not(.' + excludetrclassname + ')').prop("checked", false).trigger('change');
            }

            $('.weeklyscheduleday', thisTr).prop("checked", ischecked).trigger('change');*/
        }

        public onChangeWeeklyScheduleCheckBox(evt): void {
            let currentTarget = $(evt.currentTarget);
            let ischecked = currentTarget.is(':checked');
            let dayofweek = currentTarget.data('weekday');
            let thisTable = currentTarget.closest('table');
            let thisTr = currentTarget.closest('tr');
            let startTime = "-";
            let endTime = "-";
            let workingHr = '0 hr 0 min';

            if (ischecked) {
                startTime = $('.starttime-picker', thisTr).val();
                endTime = $('.endtime-picker', thisTr).val();

                let startTimeDt: any = new Date("1/1/1970" + ' ' + startTime);
                let endTimeDt: any = new Date("1/1/1970" + ' ' + endTime);
                if (startTimeDt < endTimeDt) {
                    let differenceDt = new Date(endTimeDt - startTimeDt);

                    let hours = differenceDt.getUTCHours();
                    let minutes = differenceDt.getUTCMinutes();
                    workingHr = hours + ' hr ' + minutes + ' min';
                }
            } else { //If uncheck checkbox then we have to make sure All checkbox should be unchecked
                //$('.weeklyscheduleselectall', thisTr).prop("checked", false);
            }

            let summaryRow = $('#tFootDriverWeeklyScheduleHours');
            let summaryStartRow = $('.summary-starttime-row', summaryRow);
            let summaryEndRow = $('.summary-endtime-row', summaryRow);
            let summaryTotalRow = $('.summary-totaltime-row', summaryRow);

            $('.' + dayofweek, summaryStartRow).html(startTime);
            $('.' + dayofweek, summaryEndRow).html(endTime);
            $('.' + dayofweek, summaryTotalRow).html(workingHr);

            if (ischecked) {
                //console.log(currentTarget);
                let rowIndex = thisTr.data('rowindex');
                let excludetrclassname = '.weeklyschedule-' + dayofweek;
                let otherCheckBoxes = $(excludetrclassname).not(currentTarget);

                $.each(otherCheckBoxes, function (i, o) {
                    let uncheckTr = $(o).closest('tr');

                    //$('.weeklyscheduleselectall', uncheckTr).prop("checked", false);
                    $(o).prop("checked", false);
                });
            }
        }

        public onChangeWeeklyTime(): void {
            let element = $(this);


            ///time-error class just removes red border, just visual indicator to show to select time
            if (element.length <= 0) {
                element.addClass('time-error');
            } else {
                element.removeClass('time-error');
            }

            //Get timepicker TR based on this object, and also get start and end time form this row and calculate working hours 
            let thisTr = element.closest('tr');
            let startTime = $('.starttime-picker', thisTr).val();
            let endTime = $('.endtime-picker', thisTr).val();

            let startTimeDt: any = new Date("1/1/1970" + ' ' + startTime);
            let endTimeDt: any = new Date("1/1/1970" + ' ' + endTime);
            let workingHr = '0 hr 0 min';
            //console.log(element);
            //console.log(thisTr);
            //console.log('starttime: ' + startTime + '; endtime: ' + endTime);

            if (startTimeDt < endTimeDt) {
                let differenceDt = new Date(endTimeDt - startTimeDt);

                let hours = differenceDt.getUTCHours();
                let minutes = differenceDt.getUTCMinutes();
                workingHr = hours + ' hr ' + minutes + ' min';
            }
            thisTr.find('span.totalworkinghours').text(workingHr);

            //Once you update time, then we have to make sure summary gets updated
            $('.weeklyscheduleday:checked', thisTr).trigger('change');
        }

        public unloadGrid() {
            $.jgrid.gridUnload(this.gridId);

            this.dsGrid = $("#" + this.gridId);
            this.dsGridList = $("#" + this.gridListId);
        }

        bindGridControl(parent): void {
            let columnNames: string[] = ['RowID', 'DriverScheduleDateIndex', 'Schedule Date', 'DriverID', 'Status', 'Start Time', 'End Time', 'Operating Hours', 'WeekDay'];
            let columnModel: JQueryJqGridColumn[] = [
                { name: 'RowID', index: 'RowID', editable: false, hidedlg: true, hidden: true }
                , { name: 'DriverScheduleDateIndex', index: 'DriverScheduleDateIndex', width: 40, key: true, editable: true, editrules: { edithidden: false }, hidedlg: true, hidden: true, search: false }
                , { name: 'DriverScheduleDateIndex', index: 'DriverScheduleDateIndex', width: 80, editable: false, hidedlg: true, hidden: false, search: false, align: 'center', datefmt: '' }
                , { name: 'DriverID', index: 'DriverID', width: 40, editable: false, editrules: { edithidden: false }, hidedlg: true, hidden: true }
                , {
                    name: 'DayWork', index: 'DayWork', width: 40, editable: true, edittype: 'select', formatter: 'select', editrules: {
                        required: true

                    }, editoptions: {
                        value: { S: 'Scheduled', C: 'Off', O: 'On-Call' },
                        dataEvents: [
                            {
                                type: 'change',
                                fn: parent.dayWorkChanged.bind(parent)
                            }]
                    }, search: false, align: 'center'
                }
                , {
                    name: 'ScheduleStartTime', index: 'ScheduleStartTime', width: 100, editable: true, edittype: 'text', search: false, align: 'center', editrules: { custom: true, custom_func: parent.checkStartEndTime, time: true, required: true },
                    editoptions: {
                        dataInit: function (element) {
                            //$(element).timepicker({
                            //    timeFormat: 'hh:mm p',
                            //    minTime: '12:00 AM',
                            //    maxTime: '11:30 PM',
                            //    interval: 15,
                            //    change: parent.endTimeChangeAssignOpertingHours.bind(this)
                            //});

                            $(element).timepicker({
                                minTime: '12:00 AM', // 11:45:00 AM,
                                maxTime: '11:30 PM',
                                timeFormat: 'h:i A',
                                step: 15
                            });
                            $(element).unbind('changeTime').on('changeTime', parent.endTimeChangeAssignOpertingHours);
                        }
                    }, formoptions: { elmsuffix: ' *' }
                }
                , {
                    name: 'ScheduleEndTime', index: 'ScheduleEndTime', width: 100, editable: true, edittype: 'text', search: false, align: 'center',
                    editrules: { custom: true, custom_func: parent.checkStartEndTime, time: true, required: true },
                    editoptions: {
                        dataInit: function (element) {
                            //$(element).timepicker({
                            //    timeFormat: 'hh:mm p',
                            //    minTime: '12:00 AM',
                            //    maxTime: '11:30 PM',
                            //    interval: 15,
                            //    change: parent.endTimeChangeAssignOpertingHours.bind(this)
                            //});
                            $(element).timepicker({
                                minTime: '12:00 AM', // 11:45:00 AM,
                                maxTime: '11:30 PM',
                                timeFormat: 'h:i A',
                                step: 15
                            });
                            $(element).unbind('changeTime').on('changeTime', parent.endTimeChangeAssignOpertingHours);
                        }
                    },
                    formoptions: { elmsuffix: ' *' }
                }
                , { name: 'DispDriverWorkingHours', index: 'DispDriverWorkingHours', width: 100, editable: true, edittype: 'custom', search: false, align: 'center', editoptions: { custom_element: this.getWorkingHoursElement, custom_value: this.getWorkingHoursValue } }
                , { name: 'WeekDay', index: 'WeekDay', editable: false, hidden: false, stype: 'select', searchoptions: { value: "0:All Days;8:Weekends;15:WeekDays;1:Sunday;2:Monday;3:Tuesday;4:Wednesday;5:Thursday;6:Friday;7:Saturday;9:Holidays", clearSearch: false }, align: 'center' }
            ]

            let jqGridOptions: JQueryJqGridOptions = {
                datatype: "local"
                , pager: 'listPager'
                , colNames: columnNames
                , colModel: columnModel
                , rowNum: 30
                , rowList: [10, 20, 30, 60, 365]
                , sortname: 'DriverScheduleDateIndex'
                , sortorder: "asc"
                , caption: 'Driver Schedule Hours'
                , id: "DriverScheduleDateIndex"
                , height: "100%"
                //, viewrecords: true
                //, autoWidth: true
                //, gridview: true
                //, multiselect: true
                //, multiboxonly: true
                //, width: null
                //, shrinkToFit: true
                //, width: 920
                , viewrecords: true
                , autoWidth: false
                , gridview: true
                , multiselect: true
                , multiboxonly: true

                //, toolbar: [true, "top"]
                , emptyrecords: 'no records found'
                , gridComplete: this.gridCompleteLocal.bind(this)
                , loadComplete: this.gridLoadComplete.bind(this)
                , loadError: this.gridLoadError.bind(this)
                //, ondblClickRow: this.gridDblClickRow.bind(this)
            };

            this.updateDialog = {
                url: driverScheduleModule.URLHelper.saveDriverScheduleHours
                , closeAfterEdit: true
                , reloadAfterSubmit: true
                , closeAfterAdd: true
                , afterShowForm: this.updateDialogAfterShowForm.bind(this)
                , onclickSubmit: this.updateDialogOnclickSubmit.bind(this)
                , afterComplete: this.updateDialogAfterComplete.bind(this)
                , modal: true
                , zIndex: 999
                , height: 300
                , width: 450,
            };

            this.dsGrid.jqGrid(jqGridOptions).navGrid(
                this.dsGridList
                , {
                    edit: false, add: false, del: false, search: false, refresh: false
                }
                , this.updateDialog
                , null
                , null
            );

            jQuery("#modalUpdateselectedhours").unbind('click').click((e) => {
                e.preventDefault();

                this.onUpdateDriverSchedule(e, this);
            });

            this.dsGrid.jqGrid('filterToolbar');
        }

        onChangeEditDriverOptions(evt): void {
            //   console.log(evt);
            let currentTarget = $(evt.currentTarget);

            this.changeEditDriverOptions(currentTarget.val());
        }

        changeEditDriverOptions(optionsVal: editDriverOptions, selectedTabIndex?: number): void {
            //Hide all sections and show only required section in below conditional statement
            this.editDriverInformation.hide();
            this.editDriverRover.hide();
            this.editDriverTransfer.hide();
            this.editDriverScheduleHours.hide();
            this.noneReasonDriverScheduleMsg.hide();

            this.btnSaveEditDriverInfo.hide();
            this.modalUpdateselectedhours.hide();
            //this.modalSchduleHours.hide();

            if (optionsVal == editDriverOptions.driverInformation) {
                this.getDriverInformation();
                this.editDriverInformation.show();
                this.btnSaveEditDriverInfo.show();
            } else if (optionsVal == editDriverOptions.rover) {
                this.resetRoverTransferForm();
                this.editDriverRover.show();
                this.btnSaveEditDriverInfo.show();

                this.editDriverRoverFacilityTabs.tabs({ active: (selectedTabIndex ? selectedTabIndex : 0) });
            } else if (optionsVal == editDriverOptions.transfer) {
                this.resetTransferForm();
                this.editDriverTransfer.show();
                this.btnSaveEditDriverInfo.show();
            } else if (optionsVal == editDriverOptions.driverScheduleHours) {
                this.editDriverScheduleHours.show();
                //this.modalUpdateselectedhours.show();
                this.btnSaveEditDriverInfo.show();

                //By default select first tab
                this.editDriverScheduleHoursTabs.tabs({ active: 0 });

                //this.modalSchduleHours.show();
                //Reset the form with fresh values, so that user can see empty form.
                this.resetDriverScheduleWeeklyStatusForm();
                this.reloadDSGrid();
            } else {
                this.noneReasonDriverScheduleMsg.show();
            }
        }

        resetRoverTransferForm(): void {
            $('#ddlDriverRoverFacilites').val('-1');
            $('#driverRoverSendToDate').val('');
            $('#driverRoverSendToDate').datepicker("option", {
                minDate: new Date(),
                maxDate: null
            });

            $('#driverRoverBackOnDate').val('');
            $('#driverRoverBackOnDate').datepicker("option", {
                minDate: new Date(),
                maxDate: null
            });

            $('#lblDriverRoverSendTo').text('Facility');
            $('#dvDriverRoverMessage').html('');

            $('#dvDriverRoverToMessage').html('');
        }

        resetTransferForm(): void {
            this.ddlDriverTransferPermFacilites.value = "-1";

            $('#permanentDriverTransferDate').val('');
            $('#permanentDriverTransferDate').datepicker("option", {
                minDate: new Date(),
                maxDate: null
            });

            $('#lblDriverTransferPermArriveAt').text('Facility');
            $('#dvDriverTransMessage').html('');
        }

        resetDriverScheduleWeeklyStatusForm(): void {
            this.tBodyDriverWeeklyScheduleHours.empty();
            ///Reset index to -1, so that user can add new entries
            this.tBodyDriverWeeklyScheduleHours.data("maxrowindex", -1);

            ///Empty summary data
            $('.summary-starttime-row td span').html('-');
            $('.summary-endtime-row td span').html('-');
            $('.summary-totaltime-row td span').html('-');

            //console.log(this.thStartDate);

            this.driverWeeklyScheduleFromDate.val(this.thStartDate);
            this.ddlWeeklyDriverScheduleType.selectedIndex = 0;

            let selectedOpt = $("option:selected", $('#ddlWeeklyDriverScheduleType'));
            let FromDate = new Date(this.driverWeeklyScheduleFromDate.val());
            let noOfDaystoAdd = selectedOpt.data("durationdays");
            let ToDate = new Date(FromDate).addDays(noOfDaystoAdd);
            this.driverWeeklyScheduleToDate.val(ToDate.toDateMMDDYYYY());

            ///Add one row to add weekly hours
            this.onClickAddDriverWeeklySchedule();
        }

        /**
         * Save weekly schedule hours START
         */
        onsaveWeeklyScheduleHours() {
            if (this.validateWeeklyScheduleHours()) {
                var dt = $("#frmAddEditDriverScheduleHours").serializeArray();
                var data = {};
                $.each(dt, function (index, value) {
                    var vl: any = value.value;

                    if (value.name.indexOf('Time') > 0) {
                        let timeDt: any = new Date("1/1/1970" + ' ' + value.value);
                        vl = (timeDt.getHours() + ":" + timeDt.getMinutes() + ":00");
                    } else if (value.value == "on") {
                        vl = true;
                    }
                    data[value.name] = vl
                });
                data["UserID"] = this.driverId;

                //this.onSaveWeeklyScheduleHoursXHR(data);
                this.onSaveWeeklyScheduleHoursCheckConflictXHR(data);
            }
        }

        onSaveWeeklyScheduleHoursCheckConflictXHR(data: any): void {
            let ajaxSettings: JQueryAjaxSettings = {
                url: driverScheduleModule.URLHelper.checkAndGetDriverLockedLoadsByDateForWeeklySchedule
                , type: 'POST'
                , traditional: true
                , data: JSON.stringify(data)
                , dataType: "json"
                , contentType: 'application/json; charset=utf-8'
                , async: true,
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbackOnSaveWeeklyScheduleHoursCheckConflictXHR.bind(this, data))
                .fail(this.errorCallbackOnSaveWeeklyScheduleHoursCheckConflictXHR);
        }

        successCallbackOnSaveWeeklyScheduleHoursCheckConflictXHR(data: any, response: any): void {
            if (response.success == false) {
                facilityApp.site.showError(response.message);
            } else {

                console.log(response);
                console.log(data);

                let hdnDriverLockedLoadDatesWeeklySchedule = $.map(response.data, function (v, i) {
                    return (new Date(v)).toDateMMDDYYYY();
                });

                if (hdnDriverLockedLoadDatesWeeklySchedule && hdnDriverLockedLoadDatesWeeklySchedule.length > 0) {
                    let msg = hdnDriverLockedLoadDatesWeeklySchedule.join(",");
                    msg = "There is an issue with the driver routes for the following dates: " + msg + ".  The driver start time isn’t the same as the route start time or the route ends after the driver end time.<br/><br/>Press the continue button if you want to make the changes or cancel to not make the changes.  If you decide to accept the changes, please visit the Local Dispatch page and optimize the route to provide more accurate ETAs.";

                    facilityApp.site.confrim(msg, "Warning!", this.onSaveWeeklyScheduleHoursXHR.bind(this, data), this.cancleUpdateDriverScheduleHoursWeekly.bind(this));
                }
                //If there are no conflicts, then continue for save driver schedule hours
                else {
                    this.onSaveWeeklyScheduleHoursXHR(data);
                }
            }

            $.blockUI;
        }

        cancleUpdateDriverScheduleHoursWeekly() {
            //dont need to do any action on cancle... just stay on same page and display edit form
        }

        errorCallbackOnSaveWeeklyScheduleHoursCheckConflictXHR(): void {
            facilityApp.site.showError('Error occurred while saving driver schedule hours. Please reload the page and try again!');
        }

        onSaveWeeklyScheduleHoursXHR(data: any): void {
            let ajaxSettings: JQueryAjaxSettings = {
                url: driverScheduleModule.URLHelper.saveWeeklyDriverSdhedule
                , type: 'POST'
                , traditional: true
                , data: JSON.stringify(data)
                , dataType: "json"
                , contentType: 'application/json; charset=utf-8'
                , async: true,
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbackOnSaveWeeklyScheduleHoursXHR.bind(this))
                .fail(this.errorCallbackOnSaveWeeklyScheduleHoursXHR);
        }

        successCallbackOnSaveWeeklyScheduleHoursXHR(response: any): void {
            if (response.success == false) {
                facilityApp.site.showError(response.message);
            }
            this.changeEditDriverOptions(editDriverOptions.driverScheduleHours);
            /*
            if (result == "error") {
                alert('Error occurred while saving driver schedule hours. Please reload the page and try again!');
            } else {
               // alert('Drivers hours has been scheduled for the selected dates.');

                //After save just refresh page so that user may not submit again with save meause
                this.changeEditDriverOptions(editDriverOptions.driverScheduleHours);
            }*/

            //As per User requirements, after save data we should close popup window
            this.modalJqgridClose.click();

            $.blockUI;
        }

        errorCallbackOnSaveWeeklyScheduleHoursXHR(): void {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        }

        validateWeeklyScheduleHours(): boolean {
            let daywisecheckboxes = $('.weeklyscheduleday:checked', this.tBodyDriverWeeklyScheduleHours);
            var errmsg = "";

            let fromdate = new Date(this.driverWeeklyScheduleFromDate.val());
            let todate = new Date(this.driverWeeklyScheduleToDate.val());

            //Its an extra condition to make sure Start date should be less than End date
            if (fromdate > todate) {
                errmsg = "From date should be less than To date."
            }
            if (daywisecheckboxes.length <= 0) {
                errmsg = errmsg + "\n Please select at least one day to schedule."
            }
            let allschedulerows = $('.weeklyschedule-row', this.tBodyDriverWeeklyScheduleHours);

            $.each(allschedulerows, function (i, o) {
                let hasAnyDayScheduled = $('.weeklyscheduleday:checked', $(o));
                if (hasAnyDayScheduled.length > 0) {
                    let startTime = $('.starttime-picker', $(o));
                    let endTime = $('.endtime-picker', $(o));
                    let flg = true;

                    if (startTime.val().legth == 0) {
                        startTime.addClass('time-error');
                        flg = false;
                    }
                    if (endTime.val().length == 0) {
                        endTime.addClass('time-error');
                        flg = false;
                    }

                    if (flg == false) {
                        errmsg = errmsg + "\n Please select Start time and End time in the driver schedule row: " + (i + 1);
                    }

                    let startTimeDt: any = new Date("1/1/1970" + ' ' + startTime.val());
                    let endTimeDt: any = new Date("1/1/1970" + ' ' + endTime.val());
                    let workingHr = '0 hr 0 min';

                    //console.log(startTimeDt);
                    //console.log(endTimeDt);

                    if (startTimeDt > endTimeDt) {
                        startTime.addClass('time-error');
                        endTime.addClass('time-error');
                        flg = false;
                        errmsg = errmsg + "\n End time should be greater than Start time in driver schedule row: " + (i + 1);
                    }
                }
            });

            if (errmsg.length > 0) {
                facilityApp.site.showAlert(errmsg);
                return false;
            } else {
                return true;
            }
        }

        /**
         * Save weekly schedule hours END
         */

        getDriverInformation(): void {
            var request = {
                Id: this.driverId
            }

            this.sendRequestForLoadUserInfo(request);
        }

        sendRequestForLoadUserInfo(postData) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.loadUserInfo,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.successCallbackLoadUserInfo.bind(this))
                .fail(this.errorCallbackLoadUserInfo);
        }

        successCallbackLoadUserInfo(result: any) {
            if (result == "error") {
                facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
            } else {
                this.editDriverInformation.html(result);

                let ddlStatus = $(result).find('#StatusID');

                if ($('option:selected', ddlStatus).val() == '2') {
                    facilityApp.site.showConfrimWithOkCallback('Do you want to make the existing driver active?', this.callBackMethodForActiveConfirmation);
                }
            }
            $.blockUI;
        }

        callBackMethodForActiveConfirmation(): void {
            $('#StatusID option[value=2]').removeAttr('selected');
            $('#StatusID option[value=1]').attr('selected', 'selected');
        }

        //bindDatePicker() {
        //    this.imgStartDateIcon = document.getElementById('driverAddStartDateIcon');
        //    let txtStartDate = $('#driverAddStartDate');
        //    this.imgStartDateIcon.onclick = (e) => { e.preventDefault(); txtStartDate.focus(); }
        //    txtStartDate.datepicker({ minDate: 0 });
        //}

        errorCallbackLoadUserInfo(result: any) {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        }

        onClickSaveDriverInfo(): void {
            var optionsVal = this.ddlEditDriverOptions.value;

            if (optionsVal == editDriverOptions.driverInformation) {
                this.sendRequestToSaveDriverDetails();
            } else if (optionsVal == editDriverOptions.rover) {
                this.sendRequestToSaveRover();
            } else if (optionsVal == editDriverOptions.transfer) {
                this.sendRequestToSaveRover();
            } else if (optionsVal == editDriverOptions.driverScheduleHours) {
                this.onsaveWeeklyScheduleHours();
            }
        }

        sendRequestToSaveRover(): void {
            if (this.validatePremanentAndRoverData()) {
                let ajaxSettings: JQueryAjaxSettings = {
                    url: driverScheduleModule.URLHelper.saveDriverRoverTransfer
                    , type: 'POST'
                    , traditional: true
                    , data: JSON.stringify(this.getRoverAndTransferData())
                    , dataType: "json"
                    , contentType: 'application/json; charset=utf-8'
                    , async: true,
                };

                $.ajax(ajaxSettings)
                    .then(this.successCallbackToSendRequestTosaveDriverRoverTransfer.bind(this))
                    .fail(this.errorCallbackToSaveDriverRoverTransfer);
            }
        }

        successCallbackToSendRequestTosaveDriverRoverTransfer(result: any) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            } else {
                var optionsVal = this.ddlEditDriverOptions.value;

                if (optionsVal == editDriverOptions.rover) {
                    this.resetRoverTransferForm();
                } else if (optionsVal == editDriverOptions.transfer) {
                    this.resetTransferForm();
                }

                //As per User requirements, after save data we should close popup window
                this.modalJqgridClose.click();
                //alert("Driver details has been updated successfully.");
            }
            $.blockUI;
        }

        errorCallbackToSaveDriverRoverTransfer(result: any) {
            facilityApp.site.showError('Error occurred while saving driver information. Please reload the page and try again!');
        }

        validatePremanentAndRoverData(): boolean {
            var optionsVal = this.ddlEditDriverOptions.value;
            var isValid = true;
            let msg = new Array();
            if (optionsVal == editDriverOptions.transfer) {
                if ($('option:selected', this.ddlDriverTransferPermFacilites).val() == '-1') {
                    //facilityApp.site.showError('Please select facility to transfer.');
                    msg.push('Please select facility to transfer.');
                    isValid = false;
                }
                if ($("#permanentDriverTransferDate").val() == '') {
                    //facilityApp.site.showError("Available in date cannot be empty.");
                    msg.push('Available in date cannot be empty.');
                    //$("#permanentDriverTransferDate").focus();
                    isValid = false;
                }
            }
            else if (optionsVal == editDriverOptions.rover) {
                if ($('option:selected', this.ddlDriverRoverFacilites).val() == '-1') {
                    //facilityApp.site.showError('Please select facility to transfer.');
                    msg.push('Please select facility to transfer.');
                    isValid = false;
                }
                if ($("#driverRoverSendToDate").val() == '') {
                    //facilityApp.site.showError("Available in date cannot be empty.");
                    msg.push('Available in date cannot be empty.');
                    //$("#driverRoverSendToDate").focus();
                    isValid = false;
                }
                if ($("#driverRoverBackOnDate").val() == '') {
                    //facilityApp.site.showError("Back in date cannot be empty.");
                    msg.push('Back in date cannot be empty.');
                    //$("#driverRoverBackOnDate").focus();
                    isValid = false;
                }
            }

            if (isValid == false) {
                facilityApp.site.showError(msg.join('<br/>'));
            }

            return isValid;
        }

        getRoverAndTransferData(): any {
            var optionsVal = this.ddlEditDriverOptions.value;

            var sStartdate;
            var sEnddate;
            //var FromFacilityID; you can get this from session
            var sToFacilityID;
            var sFK_AccessLevelID;
            var sFk_UserId;//this.driverId

            if (optionsVal == editDriverOptions.transfer) {
                sStartdate = $('#permanentDriverTransferDate').val();
                sToFacilityID = this.ddlDriverTransferPermFacilites.value;
                sFK_AccessLevelID = accessLevels.PrimaryFacility;
                sFk_UserId = this.driverId;
            }
            else if (optionsVal == editDriverOptions.rover) {
                sStartdate = $('#driverRoverSendToDate').val();
                sEnddate = $('#driverRoverBackOnDate').val();
                sToFacilityID = this.ddlDriverRoverFacilites.value;
                sFK_AccessLevelID = accessLevels.Rover;
                sFk_UserId = this.driverId;
            }

            return {
                StartDate: sStartdate,
                EndDate: sEnddate,
                ToFacilityID: sToFacilityID,
                FK_AccessLevelID: sFK_AccessLevelID,
                FK_UserID: sFk_UserId
            }
        }

        sendRequestToSaveDriverDetails() {
            if (this.validateEditDriverDetailsForm()) {
                var dt = $("#frmEditDriverDetails").serializeArray();
                var data = {};
                $.each(dt, function (index, value) {
                    data[value.name] = value.value
                });

                let ajaxSettings: JQueryAjaxSettings = {
                    url: driverMaintenanceFormModule.URLHelper.saveDriverDetails
                    , type: 'POST'
                    , traditional: true
                    , data: JSON.stringify(data)
                    , dataType: "json"
                    , contentType: 'application/json; charset=utf-8'
                    , async: true,
                };

                $.ajax(ajaxSettings)
                    .then(this.successCallbackToSaveDriverDetails.bind(this))
                    .fail(this.errorCallbackToSaveDriverDetails);
            }
        }

        successCallbackToSaveDriverDetails(result: any) {
            if (result.success == false) {
                if (result.data == "RoleMismatch") {
                    facilityApp.site.showAlertRoleMismatch(result.message, "Edit Driver Information");
                }
                else
                    facilityApp.site.showError(result.message);
            } else {
                //alert("Driver details has been updated successfully.");

                //As per User requirements, after save data we should close popup window
                this.modalJqgridClose.click();
            }
            $.blockUI;
        }

        errorCallbackToSaveDriverDetails(result: any) {
            facilityApp.site.showError('Error occurred while saving driver information. Please reload the page and try again!');
        }

        validateEditDriverDetailsForm(): boolean {
            var isValid = true;
            let Password = $('#txtPassword');
            let errMsgArray = new Array();

            //validate first name
            if (String.isNullOrEmpty($('#driverFirstName').val())) {
                $('#driverFirstName').css("border", "1px solid red");
                $("#driverFirstName").attr('title', 'Please enter first name.');
                errMsgArray.push('Please enter first name.');
                $('#driverFirstName').focus();
                isValid = false;
            }
            else {
                $('#driverFirstName').css("border", "1px #bababa solid");
            }

            //validate last name
            if (String.isNullOrEmpty($('#driverLastName').val())) {
                $('#driverLastName').css("border", "1px solid red");
                $("#driverLastName").attr('title', 'Please enter last name.');
                errMsgArray.push('Please enter last name.');
                $('#driverLastName').focus();
                isValid = false;
            }
            else {
                $('#driverLastName').css("border", "1px #bababa solid");
            }

            //Validate Email
            if (String.isNullOrEmpty($('#driverEmail').val()) || !this.validateEmail($('#driverEmail').val())) {
                $('#driverEmail').css("border", "1px solid red");
                $("#driverEmail").attr('title', 'Please enter valid email address.');
                errMsgArray.push('Please enter valid email address.');
                $('#driverEmail').focus();
                isValid = false;
            } else {
                $('#driverEmail').css("border", "1px #bababa solid");
            }

            //validate license
            if ($('#LicenseClassID').val() == '') {
                $('#LicenseClassID').css("border", "1px solid red");
                $("#LicenseClassID").attr('title', 'Please select the license class.');
                errMsgArray.push('Please select the license class.');
                $('#LicenseClassID').focus();
                isValid = false;
            } else {
                $('#LicenseClassID').css("border", "1px #bababa solid");
            }

            //validate driver status.
            if ($('#StatusID').val() == '') {
                $('#StatusID').css("border", "1px solid red");
                $("#StatusID").attr('title', 'Please select the driver status.');
                errMsgArray.push('Please select the driver status.');
                $('#StatusID').focus();
                isValid = false;
            }
            else {
                $('#StatusID').css("border", "1px #bababa solid");
            }

            if (String.isNullOrEmpty(Password.val())) {
                Password.css("border", "1px solid red");
                Password.attr('title', 'Please enter password.');
                errMsgArray.push('Please enter password.');
                Password.focus();
                isValid = false;
            }
            else if (String.hasWhiteSpace(Password.val())) {
                Password.css("border", "1px solid red");
                Password.attr('title', 'Password should not contain spaces.');
                errMsgArray.push('Password should not contain spaces.');
                Password.focus();
                isValid = false;
            }
            else if (Password.val().length < driverMaintenanceFormModule.DefaultVariables.PasswordMinLength) {
                let errorMsg = 'Password must be at least ' + driverMaintenanceFormModule.DefaultVariables.PasswordMinLength + ' characters.';
                Password.css("border", "1px solid red");
                Password.attr('title', errorMsg);
                errMsgArray.push(errorMsg);
                Password.focus();
                isValid = false;
            }
            else {
                Password.css("border", "1px #bababa solid");
            }

            if (isValid == false) {
                facilityApp.site.showError(errMsgArray.join("<br/>"));
            }

            return isValid;
        }

        validateEmail(email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);;
        }

        updateDialogAfterShowForm(formid: any): any {
            this.getWorkingHours(formid, true, false);
        }

        updateDialogOnclickSubmit(params: any): any {

            return { dates: this.dsGrid.jqGrid('getGridParam', 'selarrrow'), driverID: this.driverId };
        }

        updateDialogAfterComplete(response: any): void {
            let json = jQuery.parseJSON(response.responseText);
            //console.log(json);
            if (json.success == false) {
                facilityApp.site.showError(json.message);
            }
        }

        onUpdateDriverScheduleAfterComplete(response): void {
            let json = jQuery.parseJSON(response.responseText);
            //console.log(json);
            if (json.success == false) {
                facilityApp.site.showError(json.message);
            }
        }

        getWorkingHours(formid, onload: boolean, isMultipleSelected: boolean): any {
            //When user select multiple check box from UI and trying to update all at once, by that time we are showing default values as 07AM to 05PM.
            if (onload && isMultipleSelected) {
                $("select#DayWork", formid).val('S')
                $("input#ScheduleStartTime", formid).val('07:00 AM');
                $("input#ScheduleEndTime", formid).val('05:00 PM');
            }

            let startTime = $("input#ScheduleStartTime", formid).val();
            let endTime = $("input#ScheduleEndTime", formid).val();
            let DayWork = $("select#DayWork", formid).val();
            let workingHr = '0 hr 0 min';

            //console.log('DayWork: ' + DayWork + '; startTime: ' + startTime + '; endtime: ' + endTime);

            //Here status S-Scheduled, O-Off, C-On-Call
            if ((DayWork != 'O' || DayWork != 'C') && startTime != '' && endTime != '') {
                let startTimeDt: any = new Date("1/1/1970" + ' ' + startTime);
                let endTimeDt: any = new Date("1/1/1970" + ' ' + endTime);

                if (startTimeDt < endTimeDt) {
                    let differenceDt = new Date(endTimeDt - startTimeDt);

                    let hours = differenceDt.getUTCHours();
                    let minutes = differenceDt.getUTCMinutes();
                    workingHr = hours + ' hr ' + minutes + ' min';
                }
            }

            $("span#DispDriverWorkingHours", formid).html(workingHr);
        }

        getWorkingHoursElement(value, options): any {
            var el = document.createElement("span");
            //el.type = "span";
            el.className = 'oprationHoursDuration'
            el.innerHTML = value;
            return el;
        }

        getWorkingHoursValue(elem, operation, value): any {
            if (operation === 'get') {
                return $(elem).val();
            } else if (operation === 'set') {
                //$('span', elem).val(value);
                $(elem, 'span').each(function (index, element) { $(this).text(value); });
            }
        }

        endTimeChangeAssignOpertingHours(value, colname): any {

            let startTime = $("input#ScheduleStartTime").val();
            let endTime = $("input#ScheduleEndTime").val();
            let DayWork = $("select#DayWork").val();
            let workingHr = '0 hr 0 min';

            //Here status S-Scheduled, O-Off, C-On-Call
            if ((DayWork != 'O' || DayWork != 'C') && startTime != '' && endTime != '') {
                let startTimeDt: any = new Date("1/1/1970" + ' ' + startTime);
                let endTimeDt: any = new Date("1/1/1970" + ' ' + endTime);

                if (startTimeDt < endTimeDt) {
                    let differenceDt = new Date(endTimeDt - startTimeDt);

                    let hours = differenceDt.getUTCHours();
                    let minutes = differenceDt.getUTCMinutes();
                    workingHr = hours + ' hr ' + minutes + ' min';
                }
            }

            $("span#DispDriverWorkingHours").html(workingHr);
        }

        dayWorkChanged(e): any {
            if ($(e.target).val() == "C" || $(e.target).val() == "O") {
                $("#ScheduleStartTime").val('12:00 AM');
                $("#ScheduleEndTime").val('12:00 AM');
            }
            else {
                $("#ScheduleStartTime").val('07:00 AM');
                $("#ScheduleEndTime").val('05:00 PM');
            }

            this.displayWorkingHours($("#ScheduleStartTime").val(), $("#ScheduleEndTime").val());
        }

        displayWorkingHours(startTime, endTime): any {
            let startTimeDt: any = new Date("1/1/1970" + ' ' + startTime);
            let endTimeDt: any = new Date("1/1/1970" + ' ' + endTime);
            let workingHr = '0 hr 0 min';

            if (startTimeDt < endTimeDt) {
                let differenceDt = new Date(endTimeDt - startTimeDt);

                let hours = differenceDt.getUTCHours();
                let minutes = differenceDt.getUTCMinutes();
                workingHr = hours + ' hr ' + minutes + ' min';
            }

            $("#DispDriverWorkingHours").html(workingHr);
        }

        checkStartEndTime(value, colname): any {
            var startTime = $("input#ScheduleStartTime").val();
            var endTime = $("input#ScheduleEndTime").val();
            let DayWork = $("select#DayWork").val();

            if (DayWork == 'S') {
                if (startTime != '' && endTime != '') {
                    var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
                    var endTimeDt = new Date("1/1/1970" + ' ' + endTime);

                    if (startTimeDt >= endTimeDt) {
                        return [false, "Start time should be less than End time.", ""];
                    }
                }
            }

            return [true, "", ""];
        };

        onUpdateDriverSchedule(evt: any, parent): void {
            var rows = this.dsGrid.jqGrid('getGridParam', 'selarrrow');

            if (rows.length > 0) {
                this.dsGrid.jqGrid('editGridRow', rows, {
                    url: URLHelper.saveDriverScheduleHours
                    , height: 300
                    , width: 450
                    , zIndex: 1001
                    , stack: true
                    , closeAfterEdit: true
                    , reloadAfterSubmit: true
                    , beforeSubmit: this.onUpdateDriverScheduleBeforeSubmit.bind(this)
                    , afterShowForm: this.onUpdateDriverScheduleAfterShowForm.bind(this)
                    , onclickSubmit: this.onUpdateDriverScheduleOnclickSubmit.bind(this)
                    , afterComplete: this.onUpdateDriverScheduleAfterComplete.bind(this)
                });
            } else
                facilityApp.site.showAlert("Please select a row.");
        }

        onUpdateDriverScheduleBeforeSubmit(postdata, formid): any {
            let anyLoadFoundForThisDates = [];

            let rowids = this.dsGrid.jqGrid('getGridParam', 'selarrrow');

            anyLoadFoundForThisDates = $.grep(rowids, this.grepCompareArrayForDates.bind(this));

            //Confirmation message needs to display only when user is trying to update schedule hours.
            //In other case any way we dont allow user to inactive driver schedule schedule hours
            if (anyLoadFoundForThisDates.length > 0 && postdata.DayWork == "S") {
                let msg = anyLoadFoundForThisDates.join(",");

                msg = "There is an issue with the driver routes for the following dates: " + msg + ".  The driver start time isn’t the same as the route start time or the route ends after the driver end time.<br/><br/>Press the continue button if you want to make the changes or cancel to not make the changes.  If you decide to accept the changes, please visit the Local Dispatch page and optimize the route to provide more accurate ETAs.";

                facilityApp.site.confrim(msg, "Warning!", this.continueUpdateDriverScheduleHours.bind(this), this.cancleUpdateDriverScheduleHours.bind(this));
                return [false, ''];
            } else
                return [true, ''];
        }

        continueUpdateDriverScheduleHours() {
            //If user wants to cotinue to update schedule hourse, then we dont need to keep this list. 
            //If we dont clear it then again it shows confirmation message
            this.hdnDriverLockedLoadDates = [];

            //after making hdnDriverLockedLoadDates array to empty, please click on sData button to submit form
            $('#sData').click();
        }

        cancleUpdateDriverScheduleHours() {
            //dont need to do any action on cancle... just stay on same page and display edit form
            $("#FormError").css("display", "none");
        }

        grepCompareArrayForDates(el, i): any {
            let dt = ((new Date(el)).toDateMMDDYYYY());
            return $.inArray(dt, this.hdnDriverLockedLoadDates) >= 0;
        }

        onUpdateDriverScheduleAfterShowForm(formid): void {
            let rowids = this.dsGrid.jqGrid('getGridParam', 'selarrrow');

            $("select#DayWork", formid).val("S");
            this.getWorkingHours(formid, true, (rowids.length > 1));

            formid.closest(".ui-jqdialog").closest(".ui-jqdialog").position({
                my: 'center',
                at: 'center',
                of: window
            });

            ///Fire a call to get driver load dates, and store it in local variable and use it when user submitting form
            let ajaxData = { dates: rowids, driverID: this.driverId };
            this.checkAndGetDriverLockedLoadsByDateXHR(ajaxData);
        }

        onUpdateDriverScheduleOnclickSubmit(params: any): any {
            let rowids = this.dsGrid.jqGrid('getGridParam', 'selarrrow');

            let ajaxData = { dates: rowids, driverID: this.driverId };

            return ajaxData;
        }

        checkAndGetDriverLockedLoadsByDateXHR(postData) {
            let ajaxSettings: JQueryAjaxSettings = {
                url: driverScheduleModule.URLHelper.checkAndGetDriverLockedLoadsByDate,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: "json",
                contentType: 'application/json; charset=utf-8'
            };

            $.ajax(ajaxSettings)
                .then(this.succCallbackCheckAndGetDriverLockedLoadsByDate.bind(this))
                .fail(this.errCallbackCheckAndGetDriverLockedLoadsByDate);
        }

        succCallbackCheckAndGetDriverLockedLoadsByDate(result: any) {
            if (result.success == false) {
                this.hdnDriverLockedLoadDates = [];
                facilityApp.site.showError('Error occurred while loading edit form to update driver schedule hours, Please reload and try again!');
            } else {

                this.hdnDriverLockedLoadDates = $.map(result.data, function (v, i) {
                    return (new Date(v)).toDateMMDDYYYY();
                });
            }
            $.blockUI;
        }

        errCallbackCheckAndGetDriverLockedLoadsByDate(result: any) {
            this.hdnDriverLockedLoadDates = [];
        }

        adjustHourChangeDimentions(): void {
            var grid = $('#gbox_' + this.gridId).parent();
            var gridParentWidth = grid.width();
            this.dsGrid.jqGrid('setGridWidth', gridParentWidth);

            //var gridParentHeight = grid.height();
            //if (gridParentHeight <= 800) {
            //    var height = ($(window).height() - $(".modal-box").outerHeight()) - 100;
            //}
        }

        gridCompleteLocal(): void {
            let ids = this.dsGrid.getDataIDs();
            for (let i = 0; i < ids.length; i++) {
                let cell = this.dsGrid.getCell(ids[i], "WeekDay");
                if (cell == "Sunday" || cell == "Saturday") {
                    this.dsGrid.jqGrid('setRowData', ids[i], false, { background: '#e0effa' });
                }
            }

            //disable check boxes which are less than todays date
            var jqgList = $("input:checkbox[id^=jqg_list_]");
            $.each(jqgList, function (i, o) {
                var dt = new Date(o.id.replace('jqg_list_', ''));
                var currentdate = new Date();
                currentdate.setHours(0, 0, 0, 0);
                if (currentdate > dt)
                    $(o).remove();
            });
        }

        reloadDSGrid(): void {
            this.dsGrid.jqGrid("clearGridData", true);

            this.dsGrid.jqGrid('setGridParam', {
                mtype: 'GET'
                , url: driverScheduleModule.URLHelper.getDriverScheduleHours
                , editurl: driverScheduleModule.URLHelper.saveDriverScheduleHours
                , onclickSubmit: this.submitUpdatedDriverSchedule
                , datatype: 'json'
                , postData: {
                    dates: this.dsGrid.jqGrid('getGridParam', 'selarrrow'),
                    startDate: this.thStartDate,
                    driverID: this.driverId
                }
            }).trigger("reloadGrid");
        }

        submitUpdatedDriverSchedule(): any {
            //returning respective parameters to Jquery Ajax call
            let ajaxData = {};

            let rowIds = this.dsGrid.jqGrid('getGridParam', 'selarrrow');

            ajaxData = { dates: rowIds, driverID: this.driverId };

            return ajaxData;
        }

        submitUpdateDriverForSingleRecord(rows): any {
            return { dates: rows, driverID: this.driverId };
        }

        gridDblClickRow(rowid, iRow, iCol, e): void {
            var dt = new Date(rowid);
            var currentdate = new Date();
            currentdate.setHours(0, 0, 0, 0);//removing any hours there to current date.
            if (currentdate <= dt)
                this.dsGrid.editGridRow(rowid, this.updateDialog);
        }

        gridLoadComplete(): void {
            //setTimeout(function () { driverSchedule.prototype.setGridHeight(); }, 200);
        }

        gridLoadError(xhr: any, st: any, err: any): boolean {
            if (xhr.status == "200") return false;
            var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                width: '600',
                buttons: {
                    Ok: function () {
                        //$(this).dialog("close");
                    }
                }
            });
        }

        setGridHeight() {
            var wheight = window.innerHeight - 420;
            $('.ui-jqgrid-bdiv').not($('.ui-jqgrid-bdiv', document.getElementById("list"))).height(wheight);
        }

        showDriverSchedulePopup(driverID, name, thstartDate, selectedDriverOptions: editDriverOptions, mainForm): void {
            this.bindGridControl(this);

            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popupDriverSchedule').fadeIn();

            let driverHead: string = "";
            if (driverID > 0) {
                driverHead = name;
            } else {
                driverHead = "Driver";
            }

            $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html(driverHead);

            //assigning variables to respective values
            this.driverId = driverID;
            this.thStartDate = thstartDate;

            //this.adjustHourChangeDimentions();

            this.ddlEditDriverOptions.value = selectedDriverOptions;

            let selectTabIndex = 0;
            if (selectedDriverOptions == editDriverOptions.rover)
                selectTabIndex = 1;

            //If user trying to edit rover records, we should pass tab index 1 other wise default value that is "0"
            this.changeEditDriverOptions(selectedDriverOptions, selectTabIndex);

            this.adjustPopupPosition();
        }

        adjustPopupPosition(): void {
            var offset = $(document).scrollTop();
            $(".modal-box").css({
                top: offset
            });

            //var ht = $('html').offset().top;
            //ht = ht <= 0 ? (-1 * ht) : ht;
            //$(".modal-box").css({
            //    top: ht,
            //    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
            //});
        }

        initPopup(): void {
            $(".js-modal-close, .modal-overlay").click((e) => {
                e.preventDefault();

                $(".modal-box, .modal-overlay").fadeOut(500, function () {
                    $(".modal-overlay").remove();
                });
            });

            $(window).resize(function () {
                $(".modal-box").css({
                    top: 5,
                    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                });
            });
            $(window).resize();

            // this.initEvents();
        }

        loadGridOnDriverChange(event: any): void {
            this.driverId = event.target.value;

            this.reloadDSGrid();
        }

        clearJqGridCustom(): void {
            var objRows = $('#list tr').remove();
        }
    }
}

$(document).ready(function () {
    var oDriverForm = new driverScheduleModule.driverSchedule();

    oDriverForm.initPopup();
});