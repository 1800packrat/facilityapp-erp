﻿/// <reference path="Libraries\jquery.d.ts" />;

module facilityAppForgotPassword {
    export class URLHelper {
        public static ForgotPassword: string = '';
        public static redirectLogin: string = '/Home/Logout';
    }

    export class forgotpassword {
        public userName: string;
        btnSendEmail: HTMLElement;
        btnCancel: HTMLElement;

        constructor() {
            this.btnSendEmail = document.getElementById('btnSendEmail');
            this.btnSendEmail.onclick = (e) => { this.submit(); }

            this.btnCancel = document.getElementById('btnCancel');
            this.btnCancel.onclick = (e) => { window.location.href = URLHelper.redirectLogin; }
        }

        validate(): boolean {            
            //validations here
            if ($('#txtUserName').val() == '') {
                $('#lblError').html("Please enter user name.");
                $('#txtUserName').focus();
                return false;
            }
            $('#lblError').html('');
            return true;
        }

        submit() {
            if (this.validate()) {
                var request = {
                    UserName: $('#txtUserName').val()
                }
                this.sendRequest(request);
            }
        }

        sendRequest(postData) {
            var ajaxSettings: JQueryAjaxSettings = {
                url: facilityAppForgotPassword.URLHelper.ForgotPassword,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: this.successCallbackLogin,
                error: this.errorCallbackLogin
            };

            $.ajax(ajaxSettings);
        }

        successCallbackLogin(result: any) {
            if (result.success == true) {
                if (result.message === "Auto") {
                    var conf = confirm('New password has been sent to your email! Please click ok to redirect to Login page.');
                    if (conf == true)
                        window.location.href = result.data;
                }
                else {
                    var conf = confirm('Your request has been sent to Help Desk and will be processed shortly. Please click OK to redirect to login page.');
                    if (conf == true)
                        window.location.href = result.data;
                }
            } else {
                //write your logic to show errors
                alert(result.message);
                //$('#lblError').html(result.message);
            }
        }

        errorCallbackLogin(result: any) {
            alert(result.message);
            //$('#lblError').html(result.message);
        }
    }
}

$(document).ready(function () {
    var oForgotPassword = new facilityAppForgotPassword.forgotpassword();
});