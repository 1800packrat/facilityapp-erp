﻿/// <reference path="Libraries\jquery.d.ts" />;

module facilityAppLogin {
    export class URLHelper {
        public static ValidateLogin: string = '';
        public static Dashboard: string = '../../Dashboard.aspx';
    }

    export class login {
        public frmLogin: any;
        public LoginName: string;
        public Password: string;
        public IsRememberMe: boolean;
        btnLogin: HTMLElement;

        constructor() {           
            this.btnLogin = document.getElementById('btnLogin');
            this.btnLogin.onclick = (e) => { this.submit(); }
            
            document.addEventListener("keydown", this.handleEvt);
        }

        handleEvt = (e) => {
            var key = e.which || e.charCode || e.keyCode;

            if (key == 13) {
                this.submit();
            }
        }


        validate(): boolean {            
            ///Implement username and password validateion
            if ($('#txtLoginId').val() == '') {
                $('#lblError').html("Please enter user name.");
                $('#txtLoginId').focus();
                return false;
            }
            if ($('#txtPassword').val() == '') {
                $('#lblError').html("Please enter password.");
                $('#txtPassword').focus();
                return false;
            }
            $('#lblError').html('');
            return true;
        }

        submit() {
            if (this.validate()) {
                var request = {
                    LoginName: $('#txtLoginId').val(),
                    Password: $('#txtPassword').val(),
                    IsRememberMe: $('#chkRememberme').prop('checked')
                }

                this.sendRequest(request);
            }
        }

        successCallbackLogin(result: any) {
            if (result.success == true) {
                $.blockUI;
                window.location.href = result.data;
            } else {
                //write your logic to show errors
                $('#lblError').html(result.message);
            }
        }

        errorCallbackLogin(result: any) {
            //Show what is the error you got from valid login page
            $('#lblError').html(result.message);
        }

        sendRequest(postData) {

            var ajaxSettings: JQueryAjaxSettings = {
                url: facilityAppLogin.URLHelper.ValidateLogin,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: this.successCallbackLogin,
                error: this.errorCallbackLogin
            };

            $.ajax(ajaxSettings);
        }
    }
}
$(document).ready(function () {
    var oLogin = new facilityAppLogin.login();
});