$(document).ready(function () {
    $("#btnOptimizeTouches").on("click", function (e) {
        // e.preventDefault();
        var hrefval = $(this).attr("panel");
        var selectedLoadID = $('option:selected', $('#dwAvailableDrivers')).attr('data-loadid');

        if (selectedLoadID > 0) {
            initOptimizationLoad();
            //LoadTouchForOptimization();
        }
        else {
            ShowAlert("Please select a driver to optimize!");
            return false;
        }
    });

    $("#btnToggleDetails").on("click", function (e) {
        e.preventDefault();
        //closeSidepage();
        //$("#map-menu").css("height", "");

        $('#allMapForms').toggle();

    }); // end close button event handler

    //Facility Filter checkbox change code to showhidemarkers
    $(".chkFacilityFilter").change(function () {
        showHideMarkers();
    });

});

function initOptimizationLoad() {
    $.ajax({
        url: URLHelper.InitOptimizationLoadUrl,
        data: { LoadId: $('option:selected', $('#dwAvailableDrivers')).attr('data-loadid') },
        beforeSend: function () {
            //$.blockUI({ message: '<h3>Just a moment...</h3>' })
        },
        success: function (result) {

            var isOptimized = $('#LoadIsOptimized', result);

            if (isOptimized != undefined && isOptimized.val() == "true") {
                initOptimizeStops(result);
            } else {
                initOptimizeTouches(result);
            }
        },
        error: function (error) {
        },
        complete: function () {

        }
    });
}

function getDrivers() {
    //return false;
    $.ajax({
        url: URLHelper.GetDriversUrl,
        data: {
            date: $("#txtDate").val()
        },
        beforeSend: function () {
            //$.blockUI({ message: '<h3>Just a moment...</h3>' })
        },
        success: function (retVal) {
            if (retVal == "Failure") {
                ShowAlert('Error occurred while loading drivers. Please reload the page and try again!');
            }
            else {
                //Need to build dropdown for drivers
                BuildDriversDropdown(retVal.driverLoadList);
                BuildDriversAndTouches(retVal);

                $(".chkMapDrivers").change(function () {
                    setAllFilterCheckBox();
                    showHideMarkers();
                });

                $(".chkTouchType").change(function () {
                    if (this.value == 'all' && this.checked == true) {
                        $(".chkTouchType").prop('checked', 'checked');
                        $(".chkMapDrivers").prop('checked', 'checked');
                    } else
                        setAllFilterCheckBox();

                    showHideMarkers();
                    AddressUpdater();
                });
            }
        },
        error: function (error) {
            //document.getElementById("divProgress").style.display = 'none;';
            ShowAlert('Error occurred while processing your request. Please reload the page and try again!');
        },
        complete: function () {
            //$.unblockUI();
        }
    });
}

function setAllFilterCheckBox() {

    var all = (($('.chkTouchTypeCount:checkbox', $('#MapTimeFilter')).length == $('.chkTouchTypeCount:checkbox:checked', $('#MapTimeFilter')).length) &&
                ($('.chkMapDrivers:checkbox', $('#dvpAvailableDriverDetails')).length == $('.chkMapDrivers:checkbox:checked', $('#dvpAvailableDriverDetails')).length));

    if (all == true)
        $('#chkAllFilter').prop('checked', true);
    else
        $('#chkAllFilter').prop('checked', false);
}

function DriverTouchesShowOnMap(driverId) {

    //GetMapRelatedInfo();
    $("#btnFlip").click();

    $(document).ajaxStop(function () {
        $(this).unbind("ajaxStop");

        $('#chkAllFilter').prop('checked', false);

        var timechk = $('.chkTouchTypeCount:checkbox', $('#MapTimeFilter'));
        $.each(timechk, function (index, tchk) {
            $(tchk).prop('checked', true);
        });

        var driverschk = $('.chkMapDrivers:checkbox', $('#dvpAvailableDriverDetails'));
        $.each(driverschk, function (index, dchk) {
            if (parseInt($(dchk).attr('data-driverid')) == parseInt(driverId)) {
                $(dchk).prop('checked', true);
            } else
                $(dchk).prop('checked', false);
        });

        $('#dwAvailableDrivers').val(parseInt(driverId)).trigger('change');

        showHideMarkers();

        bindAjaxStartEvent();
    });
}

function BuildDriversDropdown(drlist) {
    var dvDrivers = $("#dvpAvailableDrivers"); //Create select as a temp element and append options to this.

    $select = $("<select>", {
        'id': 'dwAvailableDrivers',
        'class': 'drivers',
        'name': 'drivers'
    });
    $select.append($('<option>', {
        'class': 'driver',
        'value': -1,
        'data-truckid': -1,
        'data-loadid': -1,
        'data-globalsitenumber': -1,
        'data-isloadlocked': false,
        'data-marketid': -1,
        'text': 'Select Driver'
    }))
    $.map(drlist, function (t) {
        return $select.append($('<option>', {
            'class': 'driver',
            'value': t.DriverId,
            'data-truckid': t.TruckID,
            'data-loadid': t.LoadId,
            'data-globalsitenumber': t.GlobalSiteNumber,
            'data-marketid': t.MarketID,
            'data-isloadlocked': t.IsLoadLocked,
            'data-DayWork': t.DayWork,
            'text': t.DriverName + ' - ' + t.GlobalSiteNumber
        }));
    });
    dvDrivers.html($select);

    $select.on('change', function (obj) { onMapDriverChange(this); });
}

function onMapDriverChange(obj) {
    var truckid = $('option:selected', obj).attr('data-truckid');
    var loadid = $('option:selected', obj).attr('data-loadid');

    if (truckid > 0 && loadid > 0) {
        $('#btnOptimizeTouches').show();
        $('#dwAvailableTrucks').attr('disabled', 'disabled');
    } else {
        $('#btnOptimizeTouches').hide();
        $('#dwAvailableTrucks').removeAttr('disabled');
    }

    var isloadlocked = $('option:selected', obj).attr('data-isloadlocked');
    if (isloadlocked == true || isloadlocked == 'true') {
        $('#btnUpdateMappoints').addClass('disabled');
        //$('#btnUpdateMappoints').hide();
    } else {
        $('#btnUpdateMappoints').removeClass('disabled');
        //$('#btnUpdateMappoints').show();
    }

    var driverdaywork = $('option:selected', obj).attr('data-DayWork');
    if (driverdaywork != 'S') {
        $('#btnUpdateMappoints').addClass('disabled');
        $('#btnUpdateMappoints').attr('data-DayWork', 'O')
        $('#btnOptimizeTouches').hide();
    } else {
        $('#btnUpdateMappoints').removeClass('disabled');
        $('#btnUpdateMappoints').attr('data-DayWork', 'S');
        $('#btnOptimizeTouches').show();
    }

    $('#dwAvailableTrucks').val(truckid);
}

function BuildDriversAndTouches(drlist) {
    //Driver list and there touches
    var dvDriverTouches = $("#dvpAvailableDriverDetails"); //Create select as a temp element and append options to this.
    $ul = $("<ul>", {
        'class': 'driversOverlay driverlist',
        'name': 'drivers'
    });
    var li = $('<li></li>');
    li.attr('data-truckid', -1);
    li.attr('data-loadid', -1);
    li.attr('data-driverid', -1);
    li.attr('data-isloadlocked', false);
    li.attr('show', 'true');
    $ul.append(li);

    var text = 'Unassigned touches' + " (" + drlist.TotalUnassignedTouches + " Touches)";
    var chkbox = $('<input />', { type: 'checkbox', class: 'chkMapDrivers', id: 'drUnassignedTouches' });
    chkbox.prop("checked", true);
    chkbox.attr('data-driverid', "0");
    li.append(chkbox);

    //li.append(chkbox);

    var aaa = $('<span></span>');
    aaa.addClass('mapdriver-name');
    aaa.text(text);

    li.append(aaa);
    //li.append(lock);

    var drloadlist = drlist.driverLoadList;
    console.log(drloadlist);
    $.map(drloadlist, function (t) {
        var li = $('<li></li>');
        li.attr('data-truckid', t.TruckID);
        li.attr('data-loadid', t.LoadId);
        li.attr('data-driverid', t.DriverId);
        li.attr('show', 'true');
        if (t.DayWork != 'S')
        {
            li.css("border", "1px solid red");
        }
        $ul.append(li);

        var driverName = t.DriverName + " - " + t.GlobalSiteNumber;

        var text = driverName + " (" + t.TouchCount + " Touches)";
        var chkbox = $('<input />', { type: 'checkbox', class: 'chkMapDrivers', id: 'dr' + t.DriverId });
        chkbox.attr('data-driverid', t.DriverId)
        chkbox.attr('data-DayWork', t.DayWork)
        chkbox.prop("checked", true);
        li.append(chkbox);

        //li.append(chkbox);

        var aaa = $('<span></span>');
        aaa.addClass('mapdriver-name');
        aaa.text(text);

        li.append(aaa);

        var lock;
        if (t.IsLoadLocked == 'true' || t.IsLoadLocked == true) {
            lock = $('<img/>');
            lock.prop('type', "image");
            lock.prop('src', "/Images/lock-icon.png");
            lock.addClass('btnLock');
            lock.click(function () { UnlockLoad(t.LoadId, this, 'mapmainsection', t.DriverId); });
            lock.attr('data-islocked', "true");
            //lock.prop('title', "Un-Lock");
            lock.css('width', "18").css('float', "right");
        } else {
            lock = $('<img/>');
            lock.prop('type', "image");
            lock.prop('src', "/Images/unlock-icon.png");
            lock.addClass('btnLock');
            //lock.click(function(){UnlockLoad(t.LoadId,  this, 'mapmainsection', t.DriverId);});
            lock.attr('data-islocked', "false");
            //lock.prop('title', "");
            lock.css('width', "18").css('float', "right");
        }

        li.append(lock);

        if (t.HasSLUpdates == true) {
            var infoIcon;
            infoIcon = $('<img/>');
            infoIcon.prop('title', "This load has updated touch information.");
            infoIcon.prop('type', "image");
            infoIcon.prop('src', "/Images/MediaLoot/icn_alert_warning.png");
            infoIcon.css('width', "18").css('float', "right");

            li.append(infoIcon);
        }
        if (t.HasDeletedTouchs == true) {
            var delIcon;
            delIcon = $('<img/>');
            delIcon.prop('type', "image");
            delIcon.prop('title', "This load has removed/rescheduled touches.");
            delIcon.prop('src', "/Images/MediaLoot/icn_trash.png");
            delIcon.css('width', "18").css('float', "right");

            li.append(delIcon);
        }
    });
    dvDriverTouches.html($ul);
}

function callBackLockMapMainSection(loadid, obj, lock) {

    $("#dwAvailableDrivers option").each(function (i) {
        if ($(this).attr('data-loadid') == loadid) {
            $(this).attr('data-isloadlocked', false);
        }
    });
    if (loadid == $('option:selected', $('#dwAvailableDrivers')).attr('data-loadid')) {
        $('#btnUpdateMappoints').removeClass('disabled');
    }

    $("#dvpAvailableDriverDetails>ul>li").each(function (i) {
        if ($(this).attr('data-loadid') == loadid) {
            var btnLock = $(this).find('.btnLock');
            btnLock.prop('src', "/Images/unlock-icon.png");
            btnLock.click(function () { });
            btnLock.attr('data-islocked', "false");
        }
    });
}

function afterOptimizationLockToMapMain(loadId, driverId) {
    $("#dwAvailableDrivers option").each(function (i) {
        if ($(this).attr('data-loadid') == loadId) {
            $(this).attr('data-isloadlocked', true);
        }
    });

    if (loadId == $('option:selected', $('#dwAvailableDrivers')).attr('data-loadid')) {
        $('#btnUpdateMappoints').addClass('disabled');
    }

    $("#dvpAvailableDriverDetails>ul>li").each(function (i) {
        if ($(this).attr('data-loadid') == loadId) {
            var btnLock = $(this).find('.btnLock');
            btnLock.prop('src', "/Images/lock-icon.png");
            btnLock.click(function () { UnlockLoad(loadId, this, 'mapmainsection', driverId); });
            btnLock.attr('data-islocked', "true");
        }
    });
}

function getTrucks() {
    $.ajax({
        url: URLHelper.GetTrucksUrl,
        data: {
            date: $("#txtDate").val()
        },
        beforeSend: function () {
            //$.blockUI({ message: '<h3>Just a moment...</h3>' })
        },
        success: function (retVal) {
            if (retVal == "Failure") {
                ShowAlert('Error occurred while loading trucks. Please reload the page and try again!');
            }
            else {
                BuildTrucksDropdown(retVal);
            }
        },
        error: function (error) {
            //document.getElementById("divProgress").style.display = 'none;';
            ShowAlert('Error occurred while loading trucks. Please reload the page and try again!');
        },
        complete: function () {
            //$.unblockUI();
        }
    });
}

function BuildTrucksDropdown(trlist) {
    var dvDrivers = $("#dvpAvailableTrucks");

    $select = $("<select>", {
        'id': 'dwAvailableTrucks',
        'class': 'trucks',
        'name': 'trucks'
    });//Create select as a temp element and append options to this.
    $select.append($('<option>', {
        'class': 'truck',
        'value': -1,
        'data-siteNumber': -1,
        'text': 'Select Truck'
    }));

    $.map(trlist, function (t) {
        var redColorClass = t.IsAlreadyInLoad == true ? ' redColorClass' : '';

        return $select.append($('<option>', {
            'class': 'truck' + redColorClass,
            'value': t.TruckId,
            'data-siteNumber': t.SiteNumber,
            'text': t.TruckNumber + ' - ' + t.SiteNumber
        }));
    });

    dvDrivers.html($select);

    $select.on('change', function (obj) { onMapTruckChange(this); });
}

function onMapTruckChange(obj) {

    var selectedTruckId = $(obj).val();

    var driversDWVal = $('#dwAvailableDrivers').val();

    if (driversDWVal > 0) {

        if ($(obj).val() == -1 || checkTruckAssignment(obj) == true) {
            $(obj).find('option:selected').addClass('redColorClass');
            $('option:selected', $('#dwAvailableDrivers')).attr('data-truckid', selectedTruckId);
        } else {
            var selTruckId = $('option:selected', $('#dwAvailableDrivers')).attr('data-truckid');
            $(obj).val(selTruckId);
        }
    } else {
        ShowAlert('Please select driver and then assign truck!');
        $(obj).val(-1);
    }
}

function checkTruckAssignment(truckObj) {
    var driverOptions = $('option', $('#dwAvailableDrivers'));
    var rtFlag = true;

    $.each(driverOptions, function (index, opt) {
        if ($(opt).attr('data-truckid') == $(truckObj).val()) {

            if (confirm('Are sure do you want to assign this truck to this load?. This truck is already assigned to another load.')) {
                rtFlag = true;
            } else {
                //ShowAlert('Cannot select one truck for multiple drivers. Please contact administrator!');
                rtFlag = false;
            }
            //ShowAlert('Cannot select one truck for multiple drivers. Please contact administrator!');

            //rtFlag = false;
        }
    });

    return rtFlag;
}

function saveDriverTouches() {
    var mapFormData = getSelectedMarkersData();

    //Need to do validation before we fire this query
    if (validateMapForm(mapFormData) == false)
        return false;

    if (validateTouchAddress() == false) {
        return false;
    }

    $.ajax({
        url: URLHelper.SaveDriverTouchesUrl,
        data: mapFormData,
        traditional: true,
        beforeSend: function () {
            //$.blockUI({ message: '<h3>Just a moment...</h3>' })
        },
        success: function (retVal) {
            if (retVal.indexOf("This touch cannot remove from this load") > -1) {
                ShowAlert('Error occurred while saving load for the following touches. Possible reasons are: <br/>' + retVal);
            } else if (retVal == "Failure") {
                ShowAlert('Error occurred while saving load. Please reload the page and try again!');
            }
            else {
                $("#btnRealod").click();
            }
        },
        error: function (error) {
            //document.getElementById("divProgress").style.display = 'none;';
            ShowAlert('Error occurred while processing your request. Please reload the page and try again!');
        },
        complete: function () {
            //$.unblockUI();
        }
    });
}

function validateMapForm(mapFormData) {
    var msg = '';
    var rtFlag = true;

    var selectedDriver = $('#dwAvailableDrivers').val();
    var selectedTruck = $('#dwAvailableTrucks').val();

    if (mapFormData.touchIdsAndOrderNumbers.length <= 0) {
        rtFlag = false;
        msg = 'Please select at least one touch.\n';
    }

    if (selectedDriver == '' || selectedDriver <= 0) {
        rtFlag = false;
        msg += 'Please select driver.\n';
    }
    if (selectedTruck == '' || selectedTruck <= 0) {
        rtFlag = false;
        msg += 'Please select truck.';
    }

    if (rtFlag == false) {
        ShowAlert(msg);
    }

    return rtFlag;
}

//var currentIndex;

function LoadTouchForOptimization() {
    $.ajax({
        url: URLHelper.FetchLoad,
        data: { LoadId: $('option:selected', $('#dwAvailableDrivers')).attr('data-loadid') },
        beforeSend: function () {
            //$.blockUI({ message: '<h3>Just a moment...</h3>' })
        },
        success: function (Touches) {

            initOptimizeTouches(Touches);

        },
        error: function (error) {
        },
        complete: function () {

        }
    });
}

function ReScheduleSkippedTouch(button) {
    var parentElement = $(button).closest('li');
    var loadId = $(button).closest('article').find('.hdnLoadAndDriverID').val();
    var touchId = $(parentElement).attr('data-touchid');
    $.ajax({
        url: URLHelper.ReScheduleSkippedTouch,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({ loadId: loadId, touchID: touchId }),
        success: function (Touches) {
            initOptimizeTouches(Touches);
        },
        error: function () {
            alert('Error occurred while processing the request, please reload again.');
        }
    });
}



function selectSelectedDriverInFilter() {
    var availableDriversDiv = $('#dvpAvailableDriverDetails');
    var driverId = $('option:selected', $('#dwAvailableDrivers')).val();

    $('#drUnassignedTouches', availableDriversDiv).prop('checked', true);
    $('#chkAllFilter').prop('checked', false);

    $('.chkMapDrivers:checkbox', availableDriversDiv).each(function (i, o) {
        if ($(o).attr('data-driverid') == driverId)
            $(o).prop('checked', true);
        else
            $(o).prop('checked', false);
    })

    showHideMarkers();
}

//function OptimizeTouches(value) {
//jQuery("#mapFormDetails").toggle("slow");
//jQuery("#mapOptimizeTouches").toggle("slow");

//if (value == "close") {
//    $("#map-menu").css("height", "");
//} else {
//    $("#map-menu").height('90%');
//}
//}


//function SaveSortTouches() {
//    SaveSortTouchesAndContinue(true);
//}

function ShowLeftNavMainMenu() {
    jQuery("#mapFormDetails").show("slow");
    jQuery("#mapOptimizeTouches").hide("slow");
    jQuery("#mapOptimizeStops").hide("slow");
}

function showOptimizedTouches() {
    jQuery("#mapFormDetails").hide("slow");
    jQuery("#mapOptimizeTouches").show("slow");
    jQuery("#mapOptimizeStops").hide("slow");
}

function ShowOptStops() {
    jQuery("#mapFormDetails").hide("slow");
    jQuery("#mapOptimizeTouches").hide("slow");
    jQuery("#mapOptimizeStops").show("slow");
}

function CancelTouchOptimization() {
    ShowLeftNavMainMenu();
}

function CancelStopOptimization() {
    var isLocked = $('#btnLock', $('#leftControlPanel_OptStops')).attr('data-islocked');//$('#btnLock').attr('data-islocked');

    //If the route is locked then we need to send back to left nav menu
    if (isLocked == true || isLocked == "true" || isLocked == 'True') {
        clearMapRouteRelatedVariables();
        ShowLeftNavMainMenu();
        showHideMarkers();
    } else
        LoadTouchForOptimization();
}

function addTrailer() {
    $('#dvAddTrailerInfo').toggle();
    $('#addTrailerLink').toggle();
}

function bindTrailerHookandUnHookEvents() {
    $('.trailerrowtoggle').hover(HandlerInTouch, HandlerOutTouch);
    $('.trailerrowdelete').click(HandlerDeleteTrailerTouch);
}

function HandlerInTouch(o) {
    var guid = $(this).attr('data-guid');
    $('.trailerrow_' + guid).show();
}

function HandlerOutTouch(o) {
    var guid = $(this).attr('data-guid');
    $('.trailerrow_' + guid).hide();
}

function HandlerDeleteTrailerTouch(o) {
    if (confirm('Are you sure do you want to remove trailer?')) {
        var trailerid = $(this).closest("li").attr('data-guid');

        $('#hookup_' + trailerid).remove();
        $('#unhook_' + trailerid).remove();

        //addTrailer();
    }
}

function validateTrailerStops() {

}

function getCurrentLoadInfo() {
    var currentLoadInfo = {
        loadDate: $('#txtDate').val(),
        driverId: $('#dwAvailableDrivers').val(),
        loadId: $('option:selected', $('#dwAvailableDrivers')).attr('data-loadid'),
        truckId: $('option:selected', $('#dwAvailableDrivers')).attr('data-truckid'),
        driverName: $("#dwAvailableDrivers option:selected").text(),
        truckName: $("#dwAvailableTrucks option:selected").text()
    };

    return currentLoadInfo;
}