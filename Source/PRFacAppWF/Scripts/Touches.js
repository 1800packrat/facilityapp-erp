﻿function CheckTouches() {
    var status = false;
    Parent = document.getElementById('ctl00_cphCenter_gvtouch');
    var items = Parent.getElementsByTagName('input');

    for (i = 0; i < items.length; i++) {
        if (items[i].type == "checkbox") {
            if (items[i].checked) {
                status = true;
            }
        }
    }

    if (status == false) {
        alert('please select a touch');
        return false;
    }
    else if (document.getElementById('ctl00_cphCenter_hdTouchType').value == "RE-Return Empty" || document.getElementById('ctl00_cphCenter_hdTouchType').value == "Out By owner") {
        var msg = "Touche(s) cannot be added after " + document.getElementById('ctl00_cphCenter_hdTouchType').value + " touch";
        alert(msg);
        return false;
    }
    else
        return true;
}

function storeTouchtype(touchstype, chkboxid, rowindex, editIdentifier) {
    Parent = document.getElementById('ctl00_cphCenter_gvtouch');
    var items = Parent.getElementsByTagName('input');

    for (i = 0; i < items.length; i++) {
        if (items[i].id != chkboxid && items[i].type == "checkbox") {
            if (items[i].checked) {
                items[i].checked = false;
            }
        }
    }

    document.getElementById('ctl00_cphCenter_hdTouchType').value = touchstype;
    document.getElementById('ctl00_cphCenter_hdSequenceNo').value = rowindex;
    document.getElementById('ctl00_cphCenter_hdneditIdentifier').value = editIdentifier;
    __doPostBack('upTouchDetails', '');


}

function openpopup() {
    alert("test");
}


////SEFRP-TODO-RMVNICD
//function CheckPayAsYouGoAndOpenPopup(PayAsYouGo, IsCCDetailsExists, TouchType, val1, val2, val3, status, containersize) {
//    if (PayAsYouGo == 1 && IsCCDetailsExists == 0 && TouchType == "DE-Deliver Empty") {
//        alert("Credit card information is not provided for this customer.");
//    }

//    var width = 950;
//    var height = 600;
//    var left = (screen.width - width) / 2;
//    var top = (screen.height - height) / 2;
//    var params = 'width=' + width + ', height=' + height;
//    params += ', top=' + top + ', left=' + left;
//    params += ', directories=no';
//    params += ', location=no';
//    params += ', menubar=no';
//    params += ', resizable=yes';
//    params += ', scrollbars=yes';
//    params += ', status=no';
//    params += ', toolbar=no';
//    newwin = window.open('../calpopup.aspx?ContainerType=' + containersize + '&eid=' + val1 + '&dis=' + val2 + '&date=' + val3 + '&type=' + TouchType + '&status=' + status, 'windowname5', params);
//}

function SaveApproverName() {
    if (confirm('You are about to edit a touch which is scheduled for today. Please contact the facility.') == false) return false;
}

function MakeFirstLetterCapital(inputcontrol) {
    var InputValue = inputcontrol.value;
    if (InputValue.length >= 1) {
        if (InputValue.length == 1) {
            inputcontrol.value = InputValue.toUpperCase();
        }
        else {
            inputcontrol.value = InputValue.substring(0, 1).toUpperCase() + InputValue.substring(1, inputcontrol.length);
        }
    }
}