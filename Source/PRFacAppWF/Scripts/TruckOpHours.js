﻿var TIInput = {};
var editTruckOpHrDialog;

$(document).ready(function () {
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    $('#modalSavehours').click(function () {
        saveTruckOpHoursData();
    });

    //This button only appear when we try to update Maintainance/Service records.
    jQuery("#backToService").click(function () {
        saveTruckOpHoursBackToService();
    });

    initControls();

    loadDataWithGridColumnHeaderAndData();
});

function editTruckOperationHours() {
    $("#dlgAddEditTruckOpHours").dialog("open");
}

function initControls() {
    $("#TruckOpHourStartDateIcon").click(function () {
        $("#TruckOpHourStartDate").focus();
    });
    $('#TruckOpHourStartDate').datepicker({
    });

    $("form").on("submit", function (event) {
        event.preventDefault();
        loadDataWithGridColumnHeaderAndData();
    });

    $("#MaintenOfflineDateIcon").click(function () {
        if ($(this).attr('disabled') != 'disabled')
            $("#MaintenOfflineDate").focus();
    });
    $('#MaintenOfflineDate').datepicker({
        minDate: new Date(),
        onSelect: function (dateText, inst) {
            $("#MaintenOnlineDate").datepicker("option", "minDate",
            $("#MaintenOfflineDate").datepicker("getDate").addDays(1));
        }
    });

    $("#MaintenOnlineDateIcon").click(function () {
        if ($(this).attr('disabled') != 'disabled')
            $("#MaintenOnlineDate").focus();
    });
    $('#MaintenOnlineDate').datepicker({
        onSelect: function (dateText, inst) {
            $("#MaintenOfflineDate").datepicker("option", "maxDate",
            $("#MaintenOnlineDate").datepicker("getDate"));

            $('#dvTransferDetails').html("* This truck will be available on <strong>" + new Date(dateText).ToDate() + "</strong>");
        }
    });

    $("#transPermLeaveFacilityIcon").click(function () {
        if ($(this).attr('disabled') != 'disabled')
            $("#transPermLeaveFacility").focus();
    });
    $('#transPermLeaveFacility').datepicker({
        minDate: new Date(),
        onSelect: function (dateText, inst) {
            $("#transPermArrFaciliity").datepicker("option", "minDate",
           $("#transPermLeaveFacility").datepicker("getDate"));
        }
    });

    $("#transPermArrFaciliityIcon").click(function () {
        if ($(this).attr('disabled') != 'disabled')
            $("#transPermArrFaciliity").focus();
    });
    $('#transPermArrFaciliity').datepicker({
        onSelect: function (dateText, inst) {
            $("#transPermLeaveFacility").datepicker("option", "maxDate",
                $("#transPermArrFaciliity").datepicker("getDate"));

            //$('#dvTransferPermDetails').text("*This truck will be available for selected facility from " + dateText);
            AddTransferHintMessage();
        }
    });


    $('#noneReasonMsg').show();
    $('#ddlTruckOperations').change(function () {
        //when user change the dropdown option then make this id to -1, other wise it will try to update existing record which cause conflicts
        $("#hidCAPTruckOperatingHourId").val(-1);
        showhideTruckOpControls();
    })

    $("#CancelTruckDetails").click(function () {
        $("#dlgEnterTruckDetails").css('display', 'none')//.hide();
        return false;
    });
    $("#imgClose").click(function () {
        $("#dlgEnterTruckDetails").css('display', 'none')//.hide();
        return false;
    });

    //$('#imgNext').click(function () {
    //    loadNextDataWithGridColumnHeaderAndData();
    //});

    //$('#imgPrevious').click(function () {
    //    loadPreviousDataWithGridColumnHeaderAndData();
    //});


    $('#ddlTransferPermFacilites').change(function () {
        if ($('option:selected', this).val() != "-1") {
            $('#lblTransferPermArriveAt').text($('option:selected', this).text());
        }
        else {
            $('#lblTransferPermArriveAt').text('Facility');
        }

        AddTransferHintMessage();
    });
}

function AddTransferHintMessage() {
    var selectedFacilityId = $('option:selected', $('#ddlTransferPermFacilites')).val();
    var selectedArrDate = $("#transPermArrFaciliity").val();
    var msgDisplay = '';

    if (selectedFacilityId && selectedFacilityId > 0 && selectedArrDate.length > 0)
    {
        var selectedFacilityName = $('option:selected', $('#ddlTransferPermFacilites')).text();

        msgDisplay = "* This truck will be available at <strong>" + selectedFacilityName + "</strong> on <strong>" + selectedArrDate + "</strong>";
    }
    
    $('#dvTransferPermDetails').html(msgDisplay);
}

function validateBeforeSubmit() {
    var presentTruckOHReasonID = $('#ddlTruckOperations :selected').val();

    if (presentTruckOHReasonID == -1) {
        alert('Please select reason from the dropdown.');
        return false;
    }

    if (presentTruckOHReasonID == "1") {
        if ($("#MaintenOfflineDate").val() == '') {
            alert("Offline date cannot be empty.");
            $("#MaintenOfflineDate").focus();
            return false;
        }
        if ($("#MaintenOnlineDate").val() == '') {
            alert("Online date cannot be empty.");
            $("#MaintenOnlineDate").focus()
            return false;
        }
        if (new Date($("#MaintenOfflineDate").val()) > new Date($("#MaintenOnlineDate").val())) {
            alert("Offline date cannot be greater than online date.");
            return false;
        }
    }
    else if (presentTruckOHReasonID == "6") {
        if ($('#ddlTransferPermFacilites :selected').val() == '-1') {
            alert('Please select facility to transfer.');
            return false;
        }

        if ($('#ddlTransferPermReason :selected').val() == '-1') {
            alert('Please select reason to transfer.');
            return false;
        }

        if ($('#ddlTransferPermFacilites :selected').val() == DbFacilityNumber) {
            alert('You cant transfer to same facility, please select another facility.');
            return false;
        }

        if ($("#transPermLeaveFacility").val() == '') {
            alert("Leave date cannot be empty.");
            $("#transPermLeaveFacility").focus();
            return false;
        }
        if ($("#transPermArrFaciliity").val() == '') {
            alert("Arrive date cannot be empty.");
            $("#transPermArrFaciliity").focus();
            return false;
        }
        if (new Date($("#transPermLeaveFacility").val()) > new Date($("#transPermArrFaciliity").val())) {
            alert("Leave date cannot be greater than arrive date.");
            return false;
        }
    }
    return true;
}

//function submitData() {
function saveTruckOpHoursData() {
    if (validateBeforeSubmit()) {
        $.ajax({
            url: URLHelper.saveTruckDetails,
            datatype: "json",
            type: 'POST',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ truckDetails: getData() }),
            success: function (data) {
                if (data.success == true) {
                    //close popup
                    $(".modal-box, .modal-overlay").fadeOut(500, function () {
                        $(".modal-overlay").remove();
                    });

                    //alert('Truck operating details have been saved successfully.');
                    loadDataWithGridColumnHeaderAndData();
                    $('#dlgEnterTruckDetails').css('display', 'none')//.hide();
                }
                else {
                    alert(data.error);
                }
            },
            error: function (data) {
                if (data.error != null) {
                    alert(data.error.toString());
                }
                else {
                    alert('Error occurred while displaying data. Please reload the page and try again!');
                }
            }
        });
    }
    //else {
    //    alert('Please check the start date.');
    //}
}

function saveTruckOpHoursBackToService() {

    var capTruckOperatingHourId = $('#hidCAPTruckOperatingHourId').val();

    if (capTruckOperatingHourId > 0) {
        $.ajax({
            url: URLHelper.saveTruckBackToService,
            datatype: "json",
            type: 'POST',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ CAPTruckOperatingHourId: capTruckOperatingHourId }),
            success: function (data) {
                if (data.success == true) {
                    //close popup
                    $(".modal-box, .modal-overlay").fadeOut(500, function () {
                        $(".modal-overlay").remove();
                    });

                    //alert('This truck is available now.');
                    loadDataWithGridColumnHeaderAndData();
                    //$('#dlgEnterTruckDetails').css('display', 'none')//.hide();
                }
                else {
                    alert(data.error);
                }
            },
            error: function (data) {
                if (data.error != null) {
                    alert(data.error.toString());
                }
                else {
                    alert('Error occurred while saving data. Please reload the page and try again!');
                }
            }
        });
    } else {
        alert('There is some error, please contact technical team.');
    }
}

function getPostData() {
    var postDt = {
        StartDate: $('#TruckOpHourStartDate').val()
    }
    return postDt;
}

function loadNextDataWithGridColumnHeaderAndData() {
    var onlyDate = $('#TruckOpHourStartDate').val();
    var nextDate = new Date(onlyDate).addDays(7);

    $('#TruckOpHourStartDate').val(nextDate.ToDate());

    $.ajax({
        url: URLHelper.SearchTruckOpHoursUrl,
        data: {
            StartDate: nextDate.ToDate()
        },
        success: function (retVal) {
            if (retVal == "error") {
                alert('Error occurred while fetching trucks information. Please reload the page and try again!');
            } else {
                console.log(retVal);
                loadPartialGridData(retVal);
            }
        },
        error: function (error) {
            alert('Error occurred while fetching trucks information. Please reload the page and try again!');
        }
    });
}

function loadPreviousDataWithGridColumnHeaderAndData() {
    var onlyDate = $('#TruckOpHourStartDate').val();
    var preDate = new Date(onlyDate).addDays(-7);

    $('#TruckOpHourStartDate').val(preDate.ToDate());

    $.ajax({
        url: URLHelper.SearchTruckOpHoursUrl,
        data: {
            StartDate: preDate.ToDate()
        },
        success: function (retVal) {
            if (retVal == "error") {
                alert('Error occurred while fetching trucks information. Please reload the page and try again!');
            } else {
                console.log(retVal); // SFERP-TODO-CTRMV
                loadPartialGridData(retVal);
            }
        },
        error: function (error) {
            alert('Error occurred while fetching trucks information. Please reload the page and try again!');
        }
    });
}

function loadDataWithGridColumnHeaderAndData() {
    $.ajax({
        url: URLHelper.SearchTruckOpHoursUrl,
        data: {
            StartDate: $('#TruckOpHourStartDate').val()
        },

        success: function (retVal) {
            if (retVal == "error") {
                alert('Error occurred while fetching trucks information. Please reload the page and try again!');
            } else {
                loadPartialGridData(retVal);
            }
        },
        error: function (error) {
            alert('Error occurred while fetching trucks information. Please reload the page and try again!');
        }
    });
}

function loadPartialGridData(html) {
    $('#dvtruckophours').html(html);


    $('#imgNext').unbind("click").click(function () {
        loadNextDataWithGridColumnHeaderAndData();
    });

    $('#imgPrevious').unbind("click").click(function () {
        loadPreviousDataWithGridColumnHeaderAndData();
    });
}

function getData() {
    var sStartdate;
    var sEnddate;
    var sTransferReturnStartDate;
    var sTransferReturnArrivalDate;
    var sTransferFacilityId;
    var sTransferReason

    if (document.getElementById("ddlTruckOperations").value == 1) {
        sStartdate = $('#MaintenOfflineDate').val();
        //sEnddate = new Date($('#MaintenOnlineDate').val()).addDays(-1);
        sEnddate = new Date($('#MaintenOnlineDate').val()).addDays(-1).ToDate();
    }
    else if (document.getElementById("ddlTruckOperations").value == 6) {
        sTransferReturnArrivalDate = $('#transPermLeaveFacility').val();
        sStartdate = $('#transPermArrFaciliity').val();
        sTransferFacilityId = $('#ddlTransferPermFacilites option:selected').val();
        sTransferReason = $('#ddlTransferPermReason').val();
    }

    return {
        CAPTruckOperatingHourId: $('#hidCAPTruckOperatingHourId').val(),
        //FacilityId: aa,  will get it from session code behind
        TransferFacilityId: sTransferFacilityId,
        TransferIn: sTransferReturnStartDate,
        TransferOut: sTransferReturnArrivalDate,
        TruckId: $('#hidtruckId').val(),
        TruckOHReasonId: document.getElementById("ddlTruckOperations").value,
        Startdate: sStartdate,
        Enddate: sEnddate,
        TransferReasonId: sTransferReason
    }

}

Date.prototype.ToDate = function () {
    return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
}

Date.prototype.ToTime = function () {
    var hr = parseInt(this.getHours());
    var ampm = hr > 11 ? 'PM' : 'AM';
    var hr = hr > 12 ? (hr - 12) : hr;
    var mm = parseInt(this.getMinutes()) > 9 ? this.getMinutes() : '0' + this.getMinutes();
    return hr + ":" + mm + " " + ampm;
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

function showAddEditOpHoursForTruck(truckHead, truckId) {
    $('#hidtruckId').val(truckId);
    $('#hidCAPTruckOperatingHourId').val(id);
    $('#dlgEnterTruckDetails').css('display', 'block');//.show();

    var a = $(this).closest('tr').find('td:first').text();

    resetAllControls();
}