﻿///<reference path="../jquery/jquery.d.ts" />

interface JQuery {
    printElement(options:any): JQuery;
}

interface JqPrintElementOptions {
    printMode: string;
    pageTitle: string;
}