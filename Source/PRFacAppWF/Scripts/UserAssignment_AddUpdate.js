﻿$(document).ready(function () {

    bindAjaxStartEvent();

    $('#btnSaveUserDetailsBottom, #btnSaveUserDetailsTop').click(function (e) {
        saveUserDetails();
    });

    $('#ddlFilterRegion').change(function () {
        filterRegions(this);

        handleSelectAllRadioBtn();
    });

    $('#Roles').change(function () {
        filterAccessApplications(this);
    });

    filterAccessApplications($('#Roles'));

    $("input[name*='module']:radio", $('.footable > tbody > tr').not(':first')).change(function () {
        handleSelectAllRadioBtn();
    });

    handleSelectAllRadioBtn();

    $(document).ready(function () {
        $('.rdPrimaryFacility').change(function () {
            $(':radio[value=' + $(this).val() + ']').not(this).prop('checked', false);
        });
    });
});

function resetUserForm() {
    $('#frmUserDetails')[0].reset();
    filterAccessApplications($('#Roles'));

    filterRegions($('#ddlFilterRegion'));
    handleSelectAllRadioBtn();
}

function filterAccessApplications(obj) {
    var hasFAAccess = $('option:selected', obj).attr('data-hasFacilityAppAccess');
    var hasDAAccess = $('option:selected', obj).attr('data-hasDriverAppAccess');

    $('.facility-application-accesscol', '#tblAssignUserFacility').show();
    $('.driver-application-accesscol', '#tblAssignUserFacility').show();

    if (hasFAAccess == "False") {
        $('.facility-application-accesscol', '#tblAssignUserFacility').hide();
    }
    if (hasDAAccess == "False") {
        $('.driver-application-accesscol', '#tblAssignUserFacility').hide();
    }
}

function checkChildRadios(FacilityRadio) {
    var accessLevelID = $(FacilityRadio).val();

    var rows = $('.footable > tbody > tr').not(':first');

    var selectedValue = $("#ddlFilterRegion").val();
    if (selectedValue == "-1") {
        $(rows).find("input[name^='module_FA_']").each(function () {
            if ($(this).val() == accessLevelID) {
                this.checked = "checked";
            }
        });
    }
    else {
        $(rows).find('td').filter(function () {
            return $(this).text() == selectedValue;
        }).parent('tr').find("input[name^='module_FA_']").each(function () {
            if ($(this).val() == accessLevelID) {
                this.checked = "checked";
            }
        });
    }
}

function checkChildRadiosForDriverAppAccess(FacilityRadio) {
    var accessLevelID = $(FacilityRadio).val();

    var rows = $('.footable > tbody > tr').not(':first');

    var selectedValue = $("#ddlFilterRegion").val();
    if (selectedValue == "-1") {
        $(rows).find("input[name^='module_DA_']").each(function () {
            if ($(this).val() == accessLevelID) {
                this.checked = "checked";
            }
        });
    }
    else {
        $(rows).find('td').filter(function () {
            return $(this).text() == selectedValue;
        }).parent('tr').find("input[name^='module_DA_']").each(function () {
            if ($(this).val() == accessLevelID) {
                this.checked = "checked";
            }
        });
    }
}

function filterRegions(ddlFilterRegion) {
    var selectedValue = $("#ddlFilterRegion").val();

    var rows = $('.footable > tbody > tr').not(':first');

    if (selectedValue == "-1") {
        rows.show();
    }
    else {
        rows.filter(function () {
            return $(this).attr('data-areaid') == selectedValue
        }).show();
        rows.not(function () {
            return $(this).attr('data-areaid') == selectedValue
        }).hide();
    }
}

function rtnSelectedValues() {
    var selectedFailities = [];

    var hasDAAccess = $('option:selected', $('#Roles')).attr('data-hasDriverAppAccess');
    var hasFAAccess = $('option:selected', $('#Roles')).attr('data-hasFacilityAppAccess');

    $('.footable > tbody').each(function () {

        var checked = $(this).find('input:radio:checked');

        if (hasFAAccess == "False") {
            checked = checked.filter(function (e) { return $($(this).parent().find('#hfApplicationTypeID')).val() != 1; });
        }

        if (hasDAAccess == "False") {
            checked = checked.filter(function (e) { return $($(this).parent().find('#hfApplicationTypeID')).val() != 2; });
        }

        if (checked.length > 0) {
            $(checked).each(function () {
                if ($(this).val() != '1') {
                    var hfFacilityId = $(this).parent().find('#hfFacilityId');
                    var hfApplicationTypeId = $(this).parent().find('#hfApplicationTypeID');
                    var modAccessRelation = {}
                    if (hfFacilityId.length > 0) {
                        modAccessRelation = {
                            FacilityID: $(hfFacilityId).val(),
                            AccessLevelID: $(this).val(),
                            ApplicationTypeID: $(hfApplicationTypeId).val()
                        };
                        selectedFailities.push(modAccessRelation);
                    }
                }
            })
        }
    });
    return selectedFailities;
}

function isEmailValid(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function rtnValidatedDetails() {
    var UserId = $('#hfUserId').val() == '' ? '0' : $('#hfUserId').val();
    var UniqueID = $('#hfUniqueID').val() == '' ? '0' : $('#hfUniqueID').val();
    var FirstName = $('#FirstName').val();
    var LastName = $('#LastName').val();
    var UserName = $('#Username').val();
    var Password = $('#Password').val();
    var EmailAddress = $('#EmailAddress').val();
    var RoleId = $('#Roles').val();
    var IsActive = $('#FAUserMaster_IsActive').prop('checked');
    var LicenseID = $('#LicenseClassID').val();

    if (FirstName == '') {
        alert('First name should not be empty.');
        $('#FirstName').focus();
        return;
    }
    if (LastName == '') {
        alert('Last name should not be empty');
        $('#LastName').focus();
        return;
    }
    if (UserName == '') {
        alert('User name should not be empty.');
        $('#Username').focus();
        return;
    }
    if (Password == '') {
        alert('Password should not be empty.');
        $('#Password').focus();
        return;
    }
    if (EmailAddress == '') {
        alert('Email address should not be empty.');
        $('#EmailAddress').focus();
        return;
    }
    if (!isEmailValid(EmailAddress)) {
        alert('Please enter valid email address.');
        $('#EmailAddress').focus();
        return;
    }
    if (RoleId == '' || RoleId == '-1') {
        alert('Please select the role.');
        $('#Roles').focus();
        return;
    }

    if (LicenseID == '' || LicenseID == '-1') {
        LicenseID = 0;
        //alert('Please select license type.');
        //$('#LicenseClassID').focus();
        //return;
    }

    var userDetails = {
        UserId: UserId,
        FirstName: FirstName,
        LastName: LastName,
        UserName: UserName,
        Password: Password,
        EmailAddress: EmailAddress,
        RoleId: RoleId,
        IsActive: IsActive,
        LicenseClassID: LicenseID,
        UniqueID: UniqueID
    };

    return userDetails;
}

function saveUserDetails() {
    var usrDetails = rtnValidatedDetails();//rewrite this validation
    var seltedFacilities = rtnSelectedValues();

    if (usrDetails === undefined) {
        return;
    }

    if (seltedFacilities.length == 0) {
        var result = confirm("No Access to all facilities by default.");
        if (result == false) {
            return false;
        }
    }

    //validating primary facility if there is atleast one secondary facility assigned to driver for driver app access.
    var driverAppSecondaryFacility = $.grep(seltedFacilities, function (e) { return e.AccessLevelID == 5; });

    if (driverAppSecondaryFacility.length != 0) {
        if ($.grep(seltedFacilities, function (e) { return e.AccessLevelID == 4; }).length == 0) {
            alert('Please select atleast one primary facility for driver app access.');
            return false;
        }
    }

    var postData = {
        userDetails: usrDetails,
        selectedFacilities: seltedFacilities
    }

    $.ajax({
        url: URLHelper.addEditUser,
        type: 'POST',
        data: JSON.stringify(postData),
        traditional: true,
        datatype: "json",
        contentType: 'application/json; charset=utf-8',
        beforeSend: function () {
        },
        success: function (result) {
            if (result.success == false) {
                if (result.message == "UserExists")// if not zero then there is data to display
                {
                    alert('User name already exists, please choose another.')
                }
                else {
                    alert(result.message);
                }
            }
            else {
                location.href = '/UserAssignment/Index'; //uncommented this line if we want to redirect back to role list page
            }
        },
        error: function (error) {
            alert('Error occurred while loading information. Please reload the page and try again!');
        },
        complete: function () {
        }
    });
}

function bindAjaxStartEvent() {
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
}

function handleSelectAllRadioBtn() {
    var totalModuleRow = $('.footable > tbody > tr').not(':first').filter(':visible');
    var rows = $('.footable > tbody > tr').not(':first').find(':radio:checked:visible');
    var singleValSelected = true;

    if (rows.length == totalModuleRow.length) {
        var prevSelectedAccPer = -1;
        rows.each(function () {
            var thisObjAccesslevel = $(this).val();
            if (prevSelectedAccPer != -1 && prevSelectedAccPer != thisObjAccesslevel) {
                //console.log(prevSelectedAccPer + '!= -1 && ' + prevSelectedAccPer + '!= ' + thisObjAccesslevel);
                singleValSelected = false;
            }
            prevSelectedAccPer = thisObjAccesslevel;
        });
    } else {
        singleValSelected = false;
    }


    if (singleValSelected == true && prevSelectedAccPer > 0) {
        if (prevSelectedAccPer == 4)
            $('#RdSecondaryFacility').prop("checked", true);
        else if (prevSelectedAccPer == 3)
            $('#RdReadAndWrite').prop("checked", true);
        else if (prevSelectedAccPer == 2)
            $('#RdRead').prop("checked", true);
        else if (prevSelectedAccPer == 1)
            $('#RdNoAccess').prop("checked", true);
    }
    else {
        $('#RdSecondaryFacility').prop("checked", false);
        $('#RdReadAndWrite').prop("checked", false);
        $('#RdRead').prop("checked", false);
        $('#RdNoAccess').prop("checked", false);
    }
}