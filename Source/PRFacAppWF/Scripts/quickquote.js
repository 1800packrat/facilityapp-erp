﻿
//// SFERP-TODO-CTRMV -- Remove this file after First QA release --TG-568

function setRefused() {
    if (document.getElementById("<%=chkCustRefused.ClientID%>").checked == true) {
        if (document.getElementById("<%=txtFirstName.ClientID%>").value.length == 0) {
            document.getElementById("<%=txtFirstName.ClientID%>").value = "Refused";
        }
        if (document.getElementById("<%=txtLastName.ClientID%>").value.length == 0) {
            document.getElementById("<%=txtLastName.ClientID%>").value = "Refused";
        }
        document.getElementById("<%=chkAddingnewCust.ClientID%>").checked = true;
        document.getElementById("<%=btnFindMatchingCust.ClientID%>").disabled = true;
    }
    else {
        document.getElementById("<%=txtFirstName.ClientID%>").value = "";
        document.getElementById("<%=txtLastName.ClientID%>").value = "";
        document.getElementById("<%=chkAddingnewCust.ClientID%>").checked = false;
        document.getElementById("<%=btnFindMatchingCust.ClientID%>").disabled = false;
    }
}
function SetMatchingCustStatus() {
    if (document.getElementById("<%=chkAddingnewCust.ClientID%>").checked == false) {
        document.getElementById("<%=btnFindMatchingCust.ClientID%>").disabled = false;
    } else {
        document.getElementById("<%=btnFindMatchingCust.ClientID%>").disabled = true;
        document.getElementById('ctl00_cphCenter_validateCust').style.display = 'none';
    }
}

function NextClick() {

    if (document.getElementById("<%=chkAddingnewCust.ClientID%>").checked == true) {
        document.getElementById('ctl00_cphCenter_validateCust').style.display = 'none';
        // $("ctl00_cphCenter_validateCust").removeAttr('disabled', 'disabled');
        // return true;

    }
    else if (document.getElementById("ctl00_cphCenter_hidMatchedCustomerflag").value == "1") {
        document.getElementById('ctl00_cphCenter_validateCust').style.display = 'none';
        //$("ctl00_cphCenter_validateCust").attr('disabled', 'disabled');
        // return false;
    }
    else {
        document.getElementById('ctl00_cphCenter_validateCust').style.display = 'inline';
    }
}

var flagbool = 0;


function validateName() {

    if ($("#ctl00_cphCenter_txtCompany").val() == "" && $("#ctl00_cphCenter_txtFirstName").val() == "") {
        flagbool = 1;
        //  $('#ctl00_cphCenter_validateFirstname').show();
    }
    else ($("#ctl00_cphCenter_txtCompany").val() != "")
    {

        flagbool = 0;

    }

    if (flagbool == 1) {
        $("#ctl00_cphCenter_validateHowHear").hide();
        $("#ctl00_cphCenter_rqf").hide();
        $("#ctl00_cphCenter_rqfZipTo").hide();
        $("#ctl00_cphCenter_validateDate").hide();
        flagbool = 0;
        return false;
    }

    else {
        return true;
    }
}


function validate() {
    var rbtnlst = $("#ctl00_cphCenter_rbLocation_2");

    if ($("#ctl00_cphCenter_txtZipFrom").val() == "") {
        $("#ctl00_cphCenter_rqf").show();
        flagbool = 1;
    }
    if (rbtnlst.attr("checked") != 'checked') {
        if ($("#ctl00_cphCenter_txtZipTo").val() == "") {
            $("#ctl00_cphCenter_rqfZipTo").show();
            flagbool = 1;
        }
    }
    if ($('#ctl00_cphCenter_revZipTo').css('display') == 'inline') {
        flagbool = 1;
    }
    if ($('#ctl00_cphCenter_revZip').css('display') == 'inline') {
        flagbool = 1;
    }
    if (flagbool == 1) {
        $("#ctl00_cphCenter_validateHowHear").hide();
        $('#ctl00_cphCenter_validateFirstname').hide();
        $("#ctl00_cphCenter_validateLastname").hide();
        $("#ctl00_cphCenter_validateDate").hide();
        flagbool = 0;
        return false;
    }
    else {
        return true;
    }
}