﻿function Storeinhf(element, slocationcode, hasActiveLedger) {
    document.getElementById("ctl00_cphCenter_hdTennantId").value = element;
    document.getElementById("ctl00_cphCenter_hdLocationCode").value = slocationcode;
    document.getElementById("ctl00_cphCenter_hdHasAciveLedger").value = hasActiveLedger;
}

function Storeinhfqoute(element, element2, element3, element4, element5, rentalType, rentalStatus, moveType, sLocationcode) {
    document.getElementById("ctl00_cphCenter_hdQORID").value = element;
    document.getElementById("ctl00_cphCenter_hdTennantId").value = element2;
    document.getElementById("ctl00_cphCenter_hdRentalId").value = element3;
    document.getElementById("ctl00_cphCenter_hdUnitId").value = element4;
    document.getElementById("ctl00_cphCenter_hdUnitName").value = element5;
    document.getElementById("ctl00_cphCenter_hdRentalType").value = rentalType;
    document.getElementById("ctl00_cphCenter_hdRentalStatus").value = rentalStatus;
    document.getElementById("ctl00_cphCenter_hdLocationCode").value = sLocationcode;
    document.getElementById("ctl00_cphCenter_hdMoveType").value = moveType;

    if (rentalStatus == "Cancelled") {
        document.getElementById("ctl00_cphCenter_btnquoteOkay").disabled = "disabled";
    } else {
        document.getElementById("ctl00_cphCenter_btnquoteOkay").disabled = "";
    }
    var Role = document.getElementById('<%=hdnRole.ClientID%>').value; alert(role);
    if (moveType != "" && Role != "LDMUser" && Role != "Administrator" && Role != "SuperAdmin") {
        alert("THIS CUSTOMER IS BEING SERVICED BY THE LONG DISTANCE MOVING TEAM, PLEASE TRANSFER THE CUSTOMER IMMEDIATELY");
        document.getElementById("ctl00_cphCenter_btnquoteOkay").disabled = "disabled";
    }
}

function Clearlist(element) {
    var grid = document.getElementById(element);

    if (grid.rows.length > 0) {
        //loop starts from 1. rows[0] points to the header.
        for (i = 1; i < grid.rows.length; i++) {
            //get the reference of first column
            cell = grid.rows[i].cells[0];

            //loop according to the number of childNodes in the cell
            for (j = 0; j < cell.childNodes.length; j++) {
                //if childNode type is CheckBox                 
                if (cell.childNodes[j].type == "radio") {
                    //assign the status of the Select All checkbox to the cell checkbox within the grid
                    cell.childNodes[j].checked = false;
                }
            }
        }
    }
    document.getElementById("ctl00_cphCenter_hdQORID").value = "";
    document.getElementById("ctl00_cphCenter_hdTennantId").value = "";
    document.getElementById("ctl00_cphCenter_hdRentalId").value = "";
    document.getElementById("ctl00_cphCenter_hdUnitId").value = "";
}