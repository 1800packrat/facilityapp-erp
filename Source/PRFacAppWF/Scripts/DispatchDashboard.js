﻿//// SFERP-TODO-CTRMV-- Remove this file after First QA release. --TG-578

function getFormData() {
    var ob = {
        facilityNumber: $('#FacilityId').val(),
        date: $('#Date').val()
    };
    return ob;
}

function SearchTruckStatus(formdata) {
    $('#progress').show();
    jQuery('#tblFacilitiesStatisticsList').trigger("reloadGrid");
}

function formatFacilitySelct(cellvalue, options, rowObject) {
    var aTag = "<a class='row-navigate' href='" + URLHelper.SelectFacilityUrl + "?loactionCode=" + rowObject.LocationCode
        + "&facilityName=" + cellvalue + "&date=" + rowObject.DateDisp + "' title='Click here to select facility and navigate to Route Optimization page' style='text-decoration: underline;'>" + cellvalue + "</a>";
    return aTag;
}

function formatDayLocked(cellvalue, options, rowObject) {
    return (cellvalue == true || cellvalue == "True") ? "Y" : "N";
}

$(document).ready(function () {
    $("form").on("submit", function (event) {
        event.preventDefault();

        $('#progress').show();

        $('#tblFacilitiesStatisticsList').jqGrid('setGridParam', { datatype: 'json' });
        $('#tblFacilitiesStatisticsList').trigger('reloadGrid', [{ page: 1 }]);
    });

    $("#DateIcon").click(function () {
        $("#Date").focus();
    });
    $('#Date').datepicker();
    $('#progress').show();
    
    jQuery('#tblFacilitiesStatisticsList').jqGrid({
        url: URLHelper.ListUrl,
        datatype: 'json',
        loadonce: true,
        mtype: 'POST', //
        colNames: ['FacilityId', 'LocationCode', 'DateDisp', 'FacilityName', 'Sr.OM', 'OM', 'DayLocked', 'DriversLocked', 'DriversUnlocked', 'TotalTouches', 'LockedTouches', 'UnlockedTouches', 'LockedTouches %', 'TotalTrucks', 'TrucksAvailable', 'TrucksNotAvailable'],
        colModel: [
            { name: 'FacilityId', index: 'FacilityId', hidden: true, key: true },
            { name: 'LocationCode', index: 'LocationCode', hidden: true, key: true },
            { name: 'DayLocked', index: 'DayLocked', hidden: true, key: true },
            { name: 'FacilityName', index: 'FacilityName', sortable: true, formatter: formatFacilitySelct },
            { name: 'SrOM', index: 'SrOM',  sortable: true },
            { name: 'OM', index: 'OM', sortable: true },
            { name: 'DayLocked', index: 'DayLocked', width: 90, align: "right", sortable: true, formatter: formatDayLocked },
            { name: 'Driverslocked', index: 'Driverslocked', width: 120, align: "right", sortable: true },
            { name: 'DriversUnlocked', index: 'DriversUnlocked', width: 120, align: "right", sortable: true },
            { name: 'TotalTouches', index: 'TotalTouches', width: 120, align: "right", sortable: true },
            { name: 'LockedTouches', index: 'LockedTouches', width: 120, align: "right", sortable: true },
            { name: 'UnlockedTouches', index: 'UnlockedTouches', width: 130, align: "right", sortable: true },
            { name: 'LockedTouchesPercent', index: 'LockedTouchesPercent', width: 140, align: "right", sortable: true },
            { name: 'TotalTrucks', index: 'TotalTrucks', width: 120, align: "right", sortable: true },
            { name: 'TrucksAvailable', index: 'TrucksAvailable', width: 120, align: "right", sortable: true },
            { name: 'TrucksNotAvailable', index: 'TrucksNotAvailable', width: 120, align: "right", sortable: true }
        ],
        //define how pages are displayed and paged
        pager: "ptblFacilitiesStatisticsList",
        page: 1, // In case this is a select column rebind
        sortname: "FacilityName", // Initially sorted on
        viewrecords: true,
        sortorder: "asc",
        //onSortCol: function (index, columnIndex, sortOrder) {
        //},
        //grouping: true,
        //width: jQuery("#SQLInstantResults").width(),
        autowidth: true,
        hidegrid: false,
        forceFit: true, /* fit all columns with in the specified grid area */
        height: 'auto',
        scrollOffset: 0, /* remove scrollbar */
        beforeRequest: function () {
            jQuery('#tblFacilitiesStatisticsList').setGridParam({ postData: getFormData() });
        },
        loadComplete: function (dt) {
            $('#progress').hide();
            if (dt.isfullaccess == false) {
                $('#tblFacilitiesStatisticsList').find('.row-navigate').addClass('not-active');
            } else {
                $('#tblFacilitiesStatisticsList').find('.row-navigate').removeClass('not-active');
            }
            
        },
        jsonReader: {
            root: 'rows',
            id: 'FacilityName',
            repeatitems: false
        },
        loadError: function () {
            $('#progress').hide();
            alert('Error occurred while loading data, Please reload page and try again!');
        },
        //onSelectRow: function(){},
        //onSelectAll: function(){},
        altRows: true,
        altclass: "GridAltClass",
        //toppager: true,
        rowNum: 25,
        rowList: [25, 50, 75, 100],
        caption: "Dispatch Dashboard Report"
    }).jqGrid('navGrid', '#ptblFacilitiesStatisticsList', { cloneToTop: true, edit: false, add: false, del: false, search: false, refresh: false });

});