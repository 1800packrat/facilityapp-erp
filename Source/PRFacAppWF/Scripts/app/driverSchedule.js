/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqgrid.d.ts" />
/// <reference path="libraries\jquery.validation.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="libraries\jquery.tmpl.d.ts" />
/// <reference path="libraries\error.messages.d.ts" />
/// <reference path="driverMaintenance.ts" />
var driverScheduleModule;
(function (driverScheduleModule) {
    var URLHelper = /** @class */ (function () {
        function URLHelper() {
        }
        URLHelper.loadDriverScheduleInfo = '';
        URLHelper.getDriverScheduleHours = '';
        URLHelper.saveDriverScheduleHours = '';
        URLHelper.getDriversList = '';
        URLHelper.saveWeeklyDriverSdhedule = '';
        URLHelper.saveDriverRoverTransfer = '';
        URLHelper.saveDriverPremanentTransfer = '';
        URLHelper.getDriverRoverFacility = '';
        URLHelper.updateDriverRoverFacilityInfo = '';
        URLHelper.checkAndGetDriverLockedLoadsByDate = '';
        URLHelper.checkAndGetDriverLockedLoadsByDateForWeeklySchedule = '';
        return URLHelper;
    }());
    driverScheduleModule.URLHelper = URLHelper;
    var editDriverOptions;
    (function (editDriverOptions) {
        editDriverOptions[editDriverOptions["driverInformation"] = 1] = "driverInformation";
        editDriverOptions[editDriverOptions["driverScheduleHours"] = 2] = "driverScheduleHours";
        editDriverOptions[editDriverOptions["rover"] = 3] = "rover";
        editDriverOptions[editDriverOptions["transfer"] = 4] = "transfer";
    })(editDriverOptions = driverScheduleModule.editDriverOptions || (driverScheduleModule.editDriverOptions = {}));
    //copied from enum AccessLevels
    var accessLevels;
    (function (accessLevels) {
        accessLevels[accessLevels["PrimaryFacility"] = 4] = "PrimaryFacility";
        accessLevels[accessLevels["Rover"] = 6] = "Rover";
    })(accessLevels = driverScheduleModule.accessLevels || (driverScheduleModule.accessLevels = {}));
    var driverSchedule = /** @class */ (function () {
        function driverSchedule() {
            var _this = this;
            this.gridId = "list";
            this.gridListId = "listPager";
            this.dsGrid = null;
            this.dsGridList = null;
            //This variable will be used to check driver has locked loads for a given day or not
            this.hdnDriverLockedLoadDates = [];
            this.dsGrid = $("#" + this.gridId);
            //this.ddlDriverList = document.getElementById('ddlDriversList');
            this.ddlEditDriverOptions = document.getElementById('ddlEditDriverOptions');
            this.editDriverInformation = $('#editDriverInformation');
            this.editDriverRover = $('#editDriverRover');
            this.editDriverTransfer = $('#editDriverTransfer');
            this.editDriverScheduleHours = $('#editDriverScheduleHours');
            this.noneReasonDriverScheduleMsg = $('#noneReasonDriverScheduleMsg');
            this.editDriverScheduleHoursTabs = $('#editDriverScheduleHoursTabs');
            this.modalUpdateselectedhours = $('#modalUpdateselectedhours');
            //this.ddlDriverList.onchange = (e) => { e.preventDefault(); this.loadGridOnDriverChange(e); };
            this.ddlEditDriverOptions.onchange = function (e) { e.preventDefault(); _this.onChangeEditDriverOptions(e); };
            this.btnSaveEditDriverInfo = $('#modalSaveEditDriverInfo');
            this.btnSaveEditDriverInfo.unbind("click").click(function (e) {
                e.preventDefault();
                _this.onClickSaveDriverInfo();
            });
            var driverScheduleTabOptions = {
                activate: this.onChangeDriverScheduleTabOptions.bind(this),
            };
            this.editDriverScheduleHoursTabs.tabs(driverScheduleTabOptions);
            //this.modalSchduleHours = document.getElementById('modalSchduleHours');
            //this.modalSchduleHours.onclick = (e) => { e.preventDefault(); this.onsaveWeeklyScheduleHours(); };
            this.addDriverWeeklySchedule = document.getElementById('addDriverWeeklySchedule');
            this.addDriverWeeklySchedule.onclick = function (e) { e.preventDefault(); _this.onClickAddDriverWeeklySchedule(); };
            this.tmplAddWeeklyDriverSchedule = $('#tmplAddWeeklyDriverSchedule');
            this.tBodyDriverWeeklyScheduleHours = $('#tBodyDriverWeeklyScheduleHours');
            this.driverWeeklyScheduleFromDate = $('#WeeklyScheduleFromDate');
            this.imgStartDateIcon = document.getElementById('FromDateIcon');
            this.imgStartDateIcon.onclick = function (e) { e.preventDefault(); _this.driverWeeklyScheduleFromDate.focus(); };
            this.driverWeeklyScheduleFromDate.datepicker({
                minDate: new Date(),
                onSelect: this.onChangeDriverWeeklyScheduleFromDate.bind(this)
            });
            this.driverWeeklyScheduleToDate = $('#WeeklyScheduleToDate');
            this.imgToDateIcon = document.getElementById('ToDateIcon');
            this.imgToDateIcon.onclick = function (e) { e.preventDefault(); _this.driverWeeklyScheduleToDate.focus(); };
            this.driverWeeklyScheduleToDate.datepicker({
                minDate: new Date(),
                onSelect: this.onChangeDriverWeeklyScheduleToDate.bind(this)
            });
            //Rover date pickers and other functionalities
            var txtRoverToDate = $('#driverRoverSendToDate');
            this.driverRoverSendToDateIcon = document.getElementById('driverRoverSendToDateIcon');
            this.driverRoverSendToDateIcon.onclick = function (e) { e.preventDefault(); txtRoverToDate.focus(); };
            txtRoverToDate.datepicker({
                minDate: new Date(),
                onSelect: function (dateText, inst) {
                    txtRoverBackDate.datepicker("option", "minDate", txtRoverToDate.datepicker("getDate"));
                    this.AddRoverHintMessage();
                }.bind(this)
            });
            var txtRoverBackDate = $('#driverRoverBackOnDate');
            this.driverRoverBackonDateIcon = document.getElementById('driverRoverBackOnDateIcon');
            this.driverRoverBackonDateIcon.onclick = function (e) { e.preventDefault(); txtRoverBackDate.focus(); };
            txtRoverBackDate.datepicker({
                onSelect: function (dateText, inst) {
                    txtRoverToDate.datepicker("option", "maxDate", txtRoverBackDate.datepicker("getDate"));
                    this.AddRoverToHintMessage();
                }.bind(this)
            });
            this.ddlDriverRoverFacilites = document.getElementById('ddlDriverRoverFacilites');
            this.ddlDriverRoverFacilites.onchange = function (e) { e.preventDefault(); _this.onChangeDriverRoverOptions(e, _this); };
            this.ddlWeeklyDriverScheduleType = document.getElementById('ddlWeeklyDriverScheduleType');
            this.ddlWeeklyDriverScheduleType.onchange = function (e) { e.preventDefault(); _this.onChangeWeeklyDriverScheduleType(e); };
            //Premanent transfer date picker
            var txtTransferToDate = $('#permanentDriverTransferDate');
            this.permanentDriverTransferDateIcon = document.getElementById('permanentDriverTransferDateIcon');
            this.permanentDriverTransferDateIcon.onclick = function (e) { e.preventDefault(); txtTransferToDate.focus(); };
            txtTransferToDate.datepicker({
                minDate: new Date(),
                onSelect: function (dateText, inst) {
                    var driverName = $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html();
                    $('#dvDriverTransMessage').html("* <strong>" + driverName + "</strong> will be available on <strong>" + new Date(dateText).toDateMMDDYYYY() + "</strong>");
                }
            });
            this.ddlDriverTransferPermFacilites = document.getElementById('ddlDriverTransferPermFacilites');
            this.ddlDriverTransferPermFacilites.onchange = function (e) { e.preventDefault(); _this.onChangeDriverTransferOptions(e); };
            this.tBodyDriverRoverFacilityList = $('#tBodyDriverRoverFacilityList');
            this.editDriverRoverFacilityTabs = $('#editDriverRoverFacilityTabs');
            this.editDriverRoverFacilityTabs.removeClass('ui-widget');
            var roverTabOptions = {
                activate: this.onChangeRoverTabOptions.bind(this)
            };
            this.editDriverRoverFacilityTabs.tabs(roverTabOptions);
            this.modalJqgridClose = document.getElementById('modalJqgridClose');
            this.modalJqgridClose.onclick = function (e) {
                var driverMaintenanceObjLocal = new driverMaintenanceModule.driverMaintenance();
                driverMaintenanceObjLocal.validateAndSubmit();
            };
        }
        driverSchedule.prototype.onChangeWeeklyDriverScheduleType = function (evet) {
            var currentTarget = $(evet.currentTarget);
            var selectedOpt = $("option:selected", currentTarget);
            var FromDate = new Date(this.driverWeeklyScheduleFromDate.val());
            var noOfDaystoAdd = selectedOpt.data("durationdays");
            var ToDate = new Date(FromDate).addDays(noOfDaystoAdd);
            //ToDate.setDate(FromDate.getDate() + noOfDaystoAdd);
            this.driverWeeklyScheduleFromDate.val(FromDate.toDateMMDDYYYY());
            this.driverWeeklyScheduleToDate.val(ToDate.toDateMMDDYYYY());
        };
        driverSchedule.prototype.onChangeDriverWeeklyScheduleFromDate = function (date) {
            this.ddlWeeklyDriverScheduleType.selectedIndex = 0;
            this.driverWeeklyScheduleToDate.datepicker("option", "minDate", date);
        };
        driverSchedule.prototype.onChangeDriverWeeklyScheduleToDate = function (date) {
            this.ddlWeeklyDriverScheduleType.selectedIndex = 0;
            this.driverWeeklyScheduleFromDate.datepicker("option", "maxDate", date);
        };
        driverSchedule.prototype.onChangeDriverRoverOptions = function (evet, parent) {
            var currentTarget = $(evet.currentTarget);
            if ($(currentTarget).val() != "-1") {
                $('#lblDriverRoverSendTo').text($('option:selected', this.ddlDriverRoverFacilites).text());
            }
            else {
                $('#lblDriverRoverSendTo').text('Facility');
            }
            parent.AddRoverHintMessage();
        };
        driverSchedule.prototype.onChangeDriverTransferOptions = function (event) {
            var currentTarget = $(event.currentTarget);
            if ($(currentTarget).val() != "-1") {
                $('#lblDriverTransferPermArriveAt').text($('option:selected', this.ddlDriverTransferPermFacilites).text());
            }
            else {
                $('#lblDriverTransferPermArriveAt').text('Facility');
            }
        };
        driverSchedule.prototype.AddRoverHintMessage = function () {
            var selectedFacilityId = $('#ddlDriverRoverFacilites').val();
            var sendDate = $('#driverRoverSendToDate').val();
            var msgDisplay = '';
            if (selectedFacilityId && selectedFacilityId > 0 && sendDate.length > 0) {
                var selectedFacilityName = $('option:selected', this.ddlDriverRoverFacilites).text();
                var driverName = $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html();
                msgDisplay = "* <strong>" + driverName + "</strong> will be available in <strong>" + selectedFacilityName + "</strong> on <strong>" + sendDate + "</strong>";
            }
            $('#dvDriverRoverMessage').html(msgDisplay);
        };
        driverSchedule.prototype.AddRoverToHintMessage = function () {
            var selectedFacilityId = $('#ddlDriverRoverFacilites').val();
            var sendDate = $('#driverRoverBackOnDate').val();
            var msgDisplay = '';
            if (selectedFacilityId && selectedFacilityId > 0 && sendDate.length > 0) {
                var selectedFacilityName = $('#lblDriverRoverBackOn', $('#addrover-driver-schedule')).text();
                var driverName = $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html();
                msgDisplay = "* <strong>" + driverName + "</strong> will be available in <strong>" + selectedFacilityName + "</strong> on <strong>" + sendDate + "</strong>";
            }
            $('#dvDriverRoverToMessage').html(msgDisplay);
        };
        driverSchedule.prototype.onChangeDriverScheduleTabOptions = function (evt, ui) {
            this.modalUpdateselectedhours.hide();
            this.btnSaveEditDriverInfo.hide();
            //console.log(evt);
            //console.log(ui.newTab.index());
            //Need to check with user waether to reset form or not
            if (ui.newTab.index() == 1) {
                var width = window.innerWidth * 0.85;
                this.dsGrid.jqGrid('setGridWidth', width);
                this.reloadDSGrid();
                this.modalUpdateselectedhours.show();
            }
            else if (ui.newTab.index() == 0) {
                this.btnSaveEditDriverInfo.show();
            }
        };
        driverSchedule.prototype.onChangeRoverTabOptions = function (evt, ui) {
            //Need to check with user waether to reset form or not
            if (ui.newTab.index() == 0) {
                //alert('refresh add form');
                this.resetRoverTransferForm();
                this.btnSaveEditDriverInfo.show();
            }
            else if (ui.newTab.index() == 1) {
                this.getDriverRoverFacilities();
                this.btnSaveEditDriverInfo.hide();
            }
        };
        driverSchedule.prototype.getDriverRoverFacilities = function () {
            var data = { DriverId: this.driverId };
            this.sendRequestToGetDriverRoverFacilitiesXHR(data);
        };
        driverSchedule.prototype.sendRequestToGetDriverRoverFacilitiesXHR = function (postData) {
            var ajaxSettings = {
                url: driverScheduleModule.URLHelper.getDriverRoverFacility,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackGetDriverRoverFacilities.bind(this))
                .fail(this.errorCallbackGetDriverRoverFacilities);
        };
        driverSchedule.prototype.successCallbackGetDriverRoverFacilities = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                //this.editDriverInformation.html(result);
                this.bindDriverRoverFacilityDataToUI(result.data);
            }
            $.blockUI;
        };
        driverSchedule.prototype.errorCallbackGetDriverRoverFacilities = function (result) {
            facilityApp.site.showError('Error occurred while fetching driver rover facility information. Please reload the page and try again!');
        };
        driverSchedule.prototype.bindDriverRoverFacilityDataToUI = function (html) {
            var _this = this;
            this.tBodyDriverRoverFacilityList.html(html);
            //Bind events for edit and save
            //$(".icon-driverroverfacility-add", this.tBodyDriverRoverFacilityList).click((e) => { e.preventDefault(); this.inlineAddDriverFacility(e); });
            $(".icon-driverroverfacility-edit", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineEditDriverRoverFacility(e); });
            $(".icon-driverroverfacility-delete", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineDeleteDriverRoverFacility(e); });
            $(".icon-driverroverfacility-save", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineSaveDriverRoverFacility(e); });
            $(".icon-driverroverfacility-cancel", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineCacnelDriverRoverFacility(e); });
            $(".icon-driverroverfacility-StartDate", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineStartDateRoverCalendar(e); });
            $(".icon-driverroverfacility-EndDate", this.tBodyDriverRoverFacilityList).click(function (e) { e.preventDefault(); _this.inlineEndDateRoverCalendar(e); });
            $(".txt-driverroverfacility-StartDate", this.tBodyDriverRoverFacilityList).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
            $(".txt-driverroverfacility-EndDate", this.tBodyDriverRoverFacilityList).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
        };
        driverSchedule.prototype.inlineSaveDriverRoverFacility = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = $(evt.currentTarget).closest('tr');
            var index = currentTarget.attr('data-index');
            var startDate = $(".driverroverfacility-driverstartdate", thisTr).val();
            var endDate = $(".driverroverfacility-driverenddate", thisTr).val();
            var dtstartDate = new Date(startDate);
            var dtendDate = new Date(endDate);
            if (dtstartDate > dtendDate) {
                facilityApp.site.showError('End Date should be greater than Start Date.');
                return;
            }
            var postData = {
                DriverId: $(".driverroverfacility-driverid", thisTr).val(),
                UserFacilityID: $(".driverroverfacility-userfacilityid", thisTr).val(),
                StoreRowID: $("option:selected", $(".driverroverfacility-storerowid", thisTr)).data('storerowid'),
                DriverStartDate: startDate,
                DriverEndDate: endDate,
                DFStatus: driverMaintenanceFormModule.DriverFacilityStatus.Updated
            };
            this.sendRequestInlineSaveDriverRoverFacilityXHR(postData);
        };
        driverSchedule.prototype.inlineDeleteDriverRoverFacility = function (evt) {
            if (confirm('Are you sure do you want to delete this record?')) {
                var currentTarget = $(evt.currentTarget);
                var thisTr = $(evt.currentTarget).closest('tr');
                var index = currentTarget.attr('data-index');
                var postData = {
                    DriverId: $(".driverroverfacility-driverid", thisTr).val(),
                    UserFacilityID: $(".driverroverfacility-userfacilityid", thisTr).val(),
                    StoreRowID: $("option:selected", $(".driverroverfacility-storerowid", thisTr)).data('storerowid'),
                    DriverStartDate: $(".driverroverfacility-driverstartdate", thisTr).val(),
                    DriverEndDate: new Date().toDateMMDDYYYY(),
                    DFStatus: driverMaintenanceFormModule.DriverFacilityStatus.Deleted
                };
                this.sendRequestInlineSaveDriverRoverFacilityXHR(postData);
            }
        };
        driverSchedule.prototype.sendRequestInlineSaveDriverRoverFacilityXHR = function (postData) {
            var ajaxSettings = {
                url: driverScheduleModule.URLHelper.updateDriverRoverFacilityInfo,
                type: 'POST',
                traditional: true,
                data: JSON.stringify(postData),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                async: true
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackInlineSaveDriverRoverFacility.bind(this))
                .fail(this.errorCallbackInlineSaveDriverRoverFacility);
        };
        driverSchedule.prototype.successCallbackInlineSaveDriverRoverFacility = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                this.bindDriverRoverFacilityDataToUI(result.data);
            }
            $.blockUI;
        };
        driverSchedule.prototype.errorCallbackInlineSaveDriverRoverFacility = function (result) {
            facilityApp.site.showError('Error occurred while fetching driver rover facility information. Please reload the page and try again!');
        };
        driverSchedule.prototype.inlineEditDriverRoverFacility = function (evt) {
            var inEditMode = $('span[class*="icon-driverroverfacility-save"]', this.tBodyDriverRoverFacilityList).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                var currentTarget = $(evt.currentTarget);
                //let driverFacilityId = currentTarget.attr('data-driverfacility');
                var thisTr = $(evt.currentTarget).closest('tr');
                var index = currentTarget.attr('data-index');
                $('.viewData', thisTr).hide();
                $('.editData', thisTr).show();
                $('.icon-driverroverfacility-edit', thisTr).hide();
                $('.icon-driverroverfacility-delete', thisTr).hide();
                currentTarget.hide();
                $(".actions ul li:nth-child(3)").addClass("disabled").attr("aria-disabled", "true").addClass("disable-anchor-click");
                $('.icon-driverroverfacility-save', thisTr).show();
                $('.icon-driverroverfacility-cancel', thisTr).show();
                var rowstartDate = new Date($('#DriverFacility\\[' + index + '\\]\\.DriverStartDate', this.tBodyDriverRoverFacilityList).val());
                var now = new Date();
                now.setHours(0, 0, 0, 0);
                if (rowstartDate < now) {
                    // selected date is in the past
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNo', this.tBodyDriverRoverFacilityList).prop('disabled', true);
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel', this.tBodyDriverRoverFacilityList).prop('disabled', true);
                }
                else {
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNo', this.tBodyDriverRoverFacilityList).prop('disabled', false);
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel', this.tBodyDriverRoverFacilityList).prop('disabled', false);
                }
            }
            else {
                facilityApp.site.showError('Please save / discard existing changes!');
            }
        };
        driverSchedule.prototype.inlineStartDateRoverCalendar = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = $(evt.currentTarget).closest('td');
            var txtStartDate = $(thisTr).find('.txt-driverroverfacility-StartDate');
            var canEditStartdate = $(txtStartDate).attr('data-canedit');
            if (canEditStartdate == "True") {
                $(txtStartDate).focus();
                $(txtStartDate).datepicker({ minDate: 0 });
            }
        };
        driverSchedule.prototype.inlineEndDateRoverCalendar = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = $(evt.currentTarget).closest('td');
            var txtEndDate = $(thisTr).find('.txt-driverroverfacility-EndDate');
            $(txtEndDate).focus();
            $(txtEndDate).datepicker({ minDate: 0 });
        };
        driverSchedule.prototype.dateBeforeShow = function (ele, inst) {
            var canEdit = $(ele).attr('data-canedit');
            return canEdit == "True";
        };
        driverSchedule.prototype.inlineCacnelDriverRoverFacility = function (evt) {
            var currentTarget = $(evt.currentTarget);
            //let driverFacilityId = currentTarget.attr('data-driverfacility');
            var thisTr = $(evt.currentTarget).closest('tr');
            var index = currentTarget.attr('data-index');
            $('.viewData', thisTr).show();
            $('.editData', thisTr).hide();
            $('.icon-driverroverfacility-edit', thisTr).show();
            $('.icon-driverroverfacility-delete', thisTr).show();
            currentTarget.hide();
            $('.icon-driverroverfacility-save', thisTr).hide();
            $('.icon-driverroverfacility-cancel', thisTr).hide();
            if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() == 'New') {
                thisTr.remove();
            }
            var inEditMode = $('span[class*="icon-driverroverfacility-save"]', this.tBodyDriverRoverFacilityList).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                $(".actions ul li:nth-child(3)").attr("aria-disabled", "false").removeClass("disabled").removeClass("disable-anchor-click");
            }
        };
        driverSchedule.prototype.onClickAddDriverWeeklySchedule = function () {
            var _this = this;
            //To build new row we need index, that index we are maintiaining in tbody tag. This needs to be increased when user adds new row
            var maxrowindex = this.tBodyDriverWeeklyScheduleHours.data('maxrowindex');
            ///Dont add more than 7 rows to schedule, because per week only 7 days if he wants to add different schedule for all days
            if (maxrowindex >= 6) {
                facilityApp.site.showError("Maximun number of rows added to schedule.");
                return;
            }
            var weeklySch = { index: ++maxrowindex };
            this.tBodyDriverWeeklyScheduleHours.append(this.tmplAddWeeklyDriverSchedule.tmpl(weeklySch));
            this.tBodyDriverWeeklyScheduleHours.data('maxrowindex', maxrowindex);
            //$('.timepicker', this.tBodyDriverWeeklyScheduleHours).timepicker({
            //    timeFormat: 'h:mm p',
            //    minTime: '12:00 AM',
            //    maxTime: '11:30 PM',
            //    interval: 15,
            //    change: this.onChangeWeeklyTime
            //});
            $('.timepicker', this.tBodyDriverWeeklyScheduleHours).timepicker({
                minTime: '12:00 AM',
                maxTime: '11:30 PM',
                timeFormat: 'h:i A',
                step: 15
            });
            $('.timepicker', this.tBodyDriverWeeklyScheduleHours).unbind('changeTime').on('changeTime', this.onChangeWeeklyTime);
            ///Unbind previous event and try to bind it again, other wise event may fire multiple times
            $('.weeklyscheduleday', this.tBodyDriverWeeklyScheduleHours).unbind("change").change(function (e) {
                e.preventDefault();
                _this.onChangeWeeklyScheduleCheckBox(e);
            });
            ///Unbind previous event and try to bind it again, other wise event may fire multiple times
            $('.weeklyscheduleselectall', this.tBodyDriverWeeklyScheduleHours).unbind("click").click(function (e) {
                e.preventDefault();
                _this.onChangeWeeklyScheduleSelectAll(e);
            });
            ///Unbind previous event and try to bind it again, other wise event may fire multiple times
            $('.weeklyschedue-daywork', this.tBodyDriverWeeklyScheduleHours).unbind("change").change(function (e) {
                e.preventDefault();
                _this.onChangeWeeklyScheduleStatusChange(e);
            });
        };
        driverSchedule.prototype.onChangeWeeklyScheduleStatusChange = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTable = currentTarget.closest('table');
            var thisTr = currentTarget.closest('tr');
            if (currentTarget.val() == 'S') {
                $('.starttime-picker', thisTr).val("7:00 AM").change();
                $('.endtime-picker', thisTr).val("5:00 PM").change();
            }
            else
                $('.timepicker', thisTr).val("12:00 AM").change();
        };
        driverSchedule.prototype.onChangeWeeklyScheduleSelectAll = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var ischecked = currentTarget.is(':checked');
            var thisTable = currentTarget.closest('table');
            var thisTr = currentTarget.closest('tr');
            var rowIndex = thisTr.data('rowindex');
            var excludetrclassname = 'weeklyscheduleselectall_' + rowIndex;
            $('.weeklyscheduleday', thisTr).each(function (i, e) {
                var thisElementClass = e.className.split(' ').filter(function (v, ind, arr) { return v.indexOf("weeklyschedule-") == 0; });
                $.each(thisElementClass, function (ia, clname) {
                    var anyCheckedOtherThisElement = $('.' + clname).not($('.' + clname, thisTr));
                    if (!(anyCheckedOtherThisElement && anyCheckedOtherThisElement.is(':checked') == true)) {
                        $('.' + clname, thisTr).prop("checked", true).trigger('change');
                    }
                });
            });
            /*
            if (ischecked) {
                let rowIndex = thisTr.data('rowindex');
                let excludetrclassname = 'weeklyscheduleselectall_' + rowIndex;
                //Uncheck other select all box
                $('.weeklyscheduleselectall:not(.' + excludetrclassname + ')').prop("checked", false).trigger('change');
            }

            $('.weeklyscheduleday', thisTr).prop("checked", ischecked).trigger('change');*/
        };
        driverSchedule.prototype.onChangeWeeklyScheduleCheckBox = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var ischecked = currentTarget.is(':checked');
            var dayofweek = currentTarget.data('weekday');
            var thisTable = currentTarget.closest('table');
            var thisTr = currentTarget.closest('tr');
            var startTime = "-";
            var endTime = "-";
            var workingHr = '0 hr 0 min';
            if (ischecked) {
                startTime = $('.starttime-picker', thisTr).val();
                endTime = $('.endtime-picker', thisTr).val();
                var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
                var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
                if (startTimeDt < endTimeDt) {
                    var differenceDt = new Date(endTimeDt - startTimeDt);
                    var hours = differenceDt.getUTCHours();
                    var minutes = differenceDt.getUTCMinutes();
                    workingHr = hours + ' hr ' + minutes + ' min';
                }
            }
            else {
                //$('.weeklyscheduleselectall', thisTr).prop("checked", false);
            }
            var summaryRow = $('#tFootDriverWeeklyScheduleHours');
            var summaryStartRow = $('.summary-starttime-row', summaryRow);
            var summaryEndRow = $('.summary-endtime-row', summaryRow);
            var summaryTotalRow = $('.summary-totaltime-row', summaryRow);
            $('.' + dayofweek, summaryStartRow).html(startTime);
            $('.' + dayofweek, summaryEndRow).html(endTime);
            $('.' + dayofweek, summaryTotalRow).html(workingHr);
            if (ischecked) {
                //console.log(currentTarget);
                var rowIndex = thisTr.data('rowindex');
                var excludetrclassname = '.weeklyschedule-' + dayofweek;
                var otherCheckBoxes = $(excludetrclassname).not(currentTarget);
                $.each(otherCheckBoxes, function (i, o) {
                    var uncheckTr = $(o).closest('tr');
                    //$('.weeklyscheduleselectall', uncheckTr).prop("checked", false);
                    $(o).prop("checked", false);
                });
            }
        };
        driverSchedule.prototype.onChangeWeeklyTime = function () {
            var element = $(this);
            ///time-error class just removes red border, just visual indicator to show to select time
            if (element.length <= 0) {
                element.addClass('time-error');
            }
            else {
                element.removeClass('time-error');
            }
            //Get timepicker TR based on this object, and also get start and end time form this row and calculate working hours 
            var thisTr = element.closest('tr');
            var startTime = $('.starttime-picker', thisTr).val();
            var endTime = $('.endtime-picker', thisTr).val();
            var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
            var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
            var workingHr = '0 hr 0 min';
            //console.log(element);
            //console.log(thisTr);
            //console.log('starttime: ' + startTime + '; endtime: ' + endTime);
            if (startTimeDt < endTimeDt) {
                var differenceDt = new Date(endTimeDt - startTimeDt);
                var hours = differenceDt.getUTCHours();
                var minutes = differenceDt.getUTCMinutes();
                workingHr = hours + ' hr ' + minutes + ' min';
            }
            thisTr.find('span.totalworkinghours').text(workingHr);
            //Once you update time, then we have to make sure summary gets updated
            $('.weeklyscheduleday:checked', thisTr).trigger('change');
        };
        driverSchedule.prototype.unloadGrid = function () {
            $.jgrid.gridUnload(this.gridId);
            this.dsGrid = $("#" + this.gridId);
            this.dsGridList = $("#" + this.gridListId);
        };
        driverSchedule.prototype.bindGridControl = function (parent) {
            var _this = this;
            var columnNames = ['RowID', 'DriverScheduleDateIndex', 'Schedule Date', 'DriverID', 'Status', 'Start Time', 'End Time', 'Operating Hours', 'WeekDay'];
            var columnModel = [
                { name: 'RowID', index: 'RowID', editable: false, hidedlg: true, hidden: true },
                { name: 'DriverScheduleDateIndex', index: 'DriverScheduleDateIndex', width: 40, key: true, editable: true, editrules: { edithidden: false }, hidedlg: true, hidden: true, search: false },
                { name: 'DriverScheduleDateIndex', index: 'DriverScheduleDateIndex', width: 80, editable: false, hidedlg: true, hidden: false, search: false, align: 'center', datefmt: '' },
                { name: 'DriverID', index: 'DriverID', width: 40, editable: false, editrules: { edithidden: false }, hidedlg: true, hidden: true },
                {
                    name: 'DayWork', index: 'DayWork', width: 40, editable: true, edittype: 'select', formatter: 'select', editrules: {
                        required: true
                    }, editoptions: {
                        value: { S: 'Scheduled', C: 'Off', O: 'On-Call' },
                        dataEvents: [
                            {
                                type: 'change',
                                fn: parent.dayWorkChanged.bind(parent)
                            }
                        ]
                    }, search: false, align: 'center'
                },
                {
                    name: 'ScheduleStartTime', index: 'ScheduleStartTime', width: 100, editable: true, edittype: 'text', search: false, align: 'center', editrules: { custom: true, custom_func: parent.checkStartEndTime, time: true, required: true },
                    editoptions: {
                        dataInit: function (element) {
                            //$(element).timepicker({
                            //    timeFormat: 'hh:mm p',
                            //    minTime: '12:00 AM',
                            //    maxTime: '11:30 PM',
                            //    interval: 15,
                            //    change: parent.endTimeChangeAssignOpertingHours.bind(this)
                            //});
                            $(element).timepicker({
                                minTime: '12:00 AM',
                                maxTime: '11:30 PM',
                                timeFormat: 'h:i A',
                                step: 15
                            });
                            $(element).unbind('changeTime').on('changeTime', parent.endTimeChangeAssignOpertingHours);
                        }
                    }, formoptions: { elmsuffix: ' *' }
                },
                {
                    name: 'ScheduleEndTime', index: 'ScheduleEndTime', width: 100, editable: true, edittype: 'text', search: false, align: 'center',
                    editrules: { custom: true, custom_func: parent.checkStartEndTime, time: true, required: true },
                    editoptions: {
                        dataInit: function (element) {
                            //$(element).timepicker({
                            //    timeFormat: 'hh:mm p',
                            //    minTime: '12:00 AM',
                            //    maxTime: '11:30 PM',
                            //    interval: 15,
                            //    change: parent.endTimeChangeAssignOpertingHours.bind(this)
                            //});
                            $(element).timepicker({
                                minTime: '12:00 AM',
                                maxTime: '11:30 PM',
                                timeFormat: 'h:i A',
                                step: 15
                            });
                            $(element).unbind('changeTime').on('changeTime', parent.endTimeChangeAssignOpertingHours);
                        }
                    },
                    formoptions: { elmsuffix: ' *' }
                },
                { name: 'DispDriverWorkingHours', index: 'DispDriverWorkingHours', width: 100, editable: true, edittype: 'custom', search: false, align: 'center', editoptions: { custom_element: this.getWorkingHoursElement, custom_value: this.getWorkingHoursValue } },
                { name: 'WeekDay', index: 'WeekDay', editable: false, hidden: false, stype: 'select', searchoptions: { value: "0:All Days;8:Weekends;15:WeekDays;1:Sunday;2:Monday;3:Tuesday;4:Wednesday;5:Thursday;6:Friday;7:Saturday;9:Holidays", clearSearch: false }, align: 'center' }
            ];
            var jqGridOptions = {
                datatype: "local",
                pager: 'listPager',
                colNames: columnNames,
                colModel: columnModel,
                rowNum: 30,
                rowList: [10, 20, 30, 60, 365],
                sortname: 'DriverScheduleDateIndex',
                sortorder: "asc",
                caption: 'Driver Schedule Hours',
                id: "DriverScheduleDateIndex",
                height: "100%"
                //, viewrecords: true
                //, autoWidth: true
                //, gridview: true
                //, multiselect: true
                //, multiboxonly: true
                //, width: null
                //, shrinkToFit: true
                //, width: 920
                ,
                viewrecords: true,
                autoWidth: false,
                gridview: true,
                multiselect: true,
                multiboxonly: true
                //, toolbar: [true, "top"]
                ,
                emptyrecords: 'no records found',
                gridComplete: this.gridCompleteLocal.bind(this),
                loadComplete: this.gridLoadComplete.bind(this),
                loadError: this.gridLoadError.bind(this)
                //, ondblClickRow: this.gridDblClickRow.bind(this)
            };
            this.updateDialog = {
                url: driverScheduleModule.URLHelper.saveDriverScheduleHours,
                closeAfterEdit: true,
                reloadAfterSubmit: true,
                closeAfterAdd: true,
                afterShowForm: this.updateDialogAfterShowForm.bind(this),
                onclickSubmit: this.updateDialogOnclickSubmit.bind(this),
                afterComplete: this.updateDialogAfterComplete.bind(this),
                modal: true,
                zIndex: 999,
                height: 300,
                width: 450,
            };
            this.dsGrid.jqGrid(jqGridOptions).navGrid(this.dsGridList, {
                edit: false, add: false, del: false, search: false, refresh: false
            }, this.updateDialog, null, null);
            jQuery("#modalUpdateselectedhours").unbind('click').click(function (e) {
                e.preventDefault();
                _this.onUpdateDriverSchedule(e, _this);
            });
            this.dsGrid.jqGrid('filterToolbar');
        };
        driverSchedule.prototype.onChangeEditDriverOptions = function (evt) {
            //   console.log(evt);
            var currentTarget = $(evt.currentTarget);
            this.changeEditDriverOptions(currentTarget.val());
        };
        driverSchedule.prototype.changeEditDriverOptions = function (optionsVal, selectedTabIndex) {
            //Hide all sections and show only required section in below conditional statement
            this.editDriverInformation.hide();
            this.editDriverRover.hide();
            this.editDriverTransfer.hide();
            this.editDriverScheduleHours.hide();
            this.noneReasonDriverScheduleMsg.hide();
            this.btnSaveEditDriverInfo.hide();
            this.modalUpdateselectedhours.hide();
            //this.modalSchduleHours.hide();
            if (optionsVal == editDriverOptions.driverInformation) {
                this.getDriverInformation();
                this.editDriverInformation.show();
                this.btnSaveEditDriverInfo.show();
            }
            else if (optionsVal == editDriverOptions.rover) {
                this.resetRoverTransferForm();
                this.editDriverRover.show();
                this.btnSaveEditDriverInfo.show();
                this.editDriverRoverFacilityTabs.tabs({ active: (selectedTabIndex ? selectedTabIndex : 0) });
            }
            else if (optionsVal == editDriverOptions.transfer) {
                this.resetTransferForm();
                this.editDriverTransfer.show();
                this.btnSaveEditDriverInfo.show();
            }
            else if (optionsVal == editDriverOptions.driverScheduleHours) {
                this.editDriverScheduleHours.show();
                //this.modalUpdateselectedhours.show();
                this.btnSaveEditDriverInfo.show();
                //By default select first tab
                this.editDriverScheduleHoursTabs.tabs({ active: 0 });
                //this.modalSchduleHours.show();
                //Reset the form with fresh values, so that user can see empty form.
                this.resetDriverScheduleWeeklyStatusForm();
                this.reloadDSGrid();
            }
            else {
                this.noneReasonDriverScheduleMsg.show();
            }
        };
        driverSchedule.prototype.resetRoverTransferForm = function () {
            $('#ddlDriverRoverFacilites').val('-1');
            $('#driverRoverSendToDate').val('');
            $('#driverRoverSendToDate').datepicker("option", {
                minDate: new Date(),
                maxDate: null
            });
            $('#driverRoverBackOnDate').val('');
            $('#driverRoverBackOnDate').datepicker("option", {
                minDate: new Date(),
                maxDate: null
            });
            $('#lblDriverRoverSendTo').text('Facility');
            $('#dvDriverRoverMessage').html('');
            $('#dvDriverRoverToMessage').html('');
        };
        driverSchedule.prototype.resetTransferForm = function () {
            this.ddlDriverTransferPermFacilites.value = "-1";
            $('#permanentDriverTransferDate').val('');
            $('#permanentDriverTransferDate').datepicker("option", {
                minDate: new Date(),
                maxDate: null
            });
            $('#lblDriverTransferPermArriveAt').text('Facility');
            $('#dvDriverTransMessage').html('');
        };
        driverSchedule.prototype.resetDriverScheduleWeeklyStatusForm = function () {
            this.tBodyDriverWeeklyScheduleHours.empty();
            ///Reset index to -1, so that user can add new entries
            this.tBodyDriverWeeklyScheduleHours.data("maxrowindex", -1);
            ///Empty summary data
            $('.summary-starttime-row td span').html('-');
            $('.summary-endtime-row td span').html('-');
            $('.summary-totaltime-row td span').html('-');
            //console.log(this.thStartDate);
            this.driverWeeklyScheduleFromDate.val(this.thStartDate);
            this.ddlWeeklyDriverScheduleType.selectedIndex = 0;
            var selectedOpt = $("option:selected", $('#ddlWeeklyDriverScheduleType'));
            var FromDate = new Date(this.driverWeeklyScheduleFromDate.val());
            var noOfDaystoAdd = selectedOpt.data("durationdays");
            var ToDate = new Date(FromDate).addDays(noOfDaystoAdd);
            this.driverWeeklyScheduleToDate.val(ToDate.toDateMMDDYYYY());
            ///Add one row to add weekly hours
            this.onClickAddDriverWeeklySchedule();
        };
        /**
         * Save weekly schedule hours START
         */
        driverSchedule.prototype.onsaveWeeklyScheduleHours = function () {
            if (this.validateWeeklyScheduleHours()) {
                var dt = $("#frmAddEditDriverScheduleHours").serializeArray();
                var data = {};
                $.each(dt, function (index, value) {
                    var vl = value.value;
                    if (value.name.indexOf('Time') > 0) {
                        var timeDt = new Date("1/1/1970" + ' ' + value.value);
                        vl = (timeDt.getHours() + ":" + timeDt.getMinutes() + ":00");
                    }
                    else if (value.value == "on") {
                        vl = true;
                    }
                    data[value.name] = vl;
                });
                data["UserID"] = this.driverId;
                //this.onSaveWeeklyScheduleHoursXHR(data);
                this.onSaveWeeklyScheduleHoursCheckConflictXHR(data);
            }
        };
        driverSchedule.prototype.onSaveWeeklyScheduleHoursCheckConflictXHR = function (data) {
            var ajaxSettings = {
                url: driverScheduleModule.URLHelper.checkAndGetDriverLockedLoadsByDateForWeeklySchedule,
                type: 'POST',
                traditional: true,
                data: JSON.stringify(data),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                async: true,
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackOnSaveWeeklyScheduleHoursCheckConflictXHR.bind(this, data))
                .fail(this.errorCallbackOnSaveWeeklyScheduleHoursCheckConflictXHR);
        };
        driverSchedule.prototype.successCallbackOnSaveWeeklyScheduleHoursCheckConflictXHR = function (data, response) {
            if (response.success == false) {
                facilityApp.site.showError(response.message);
            }
            else {
                console.log(response);
                console.log(data);
                var hdnDriverLockedLoadDatesWeeklySchedule = $.map(response.data, function (v, i) {
                    return (new Date(v)).toDateMMDDYYYY();
                });
                if (hdnDriverLockedLoadDatesWeeklySchedule && hdnDriverLockedLoadDatesWeeklySchedule.length > 0) {
                    var msg = hdnDriverLockedLoadDatesWeeklySchedule.join(",");
                    msg = "There is an issue with the driver routes for the following dates: " + msg + ".  The driver start time isn’t the same as the route start time or the route ends after the driver end time.<br/><br/>Press the continue button if you want to make the changes or cancel to not make the changes.  If you decide to accept the changes, please visit the Local Dispatch page and optimize the route to provide more accurate ETAs.";
                    facilityApp.site.confrim(msg, "Warning!", this.onSaveWeeklyScheduleHoursXHR.bind(this, data), this.cancleUpdateDriverScheduleHoursWeekly.bind(this));
                }
                else {
                    this.onSaveWeeklyScheduleHoursXHR(data);
                }
            }
            $.blockUI;
        };
        driverSchedule.prototype.cancleUpdateDriverScheduleHoursWeekly = function () {
            //dont need to do any action on cancle... just stay on same page and display edit form
        };
        driverSchedule.prototype.errorCallbackOnSaveWeeklyScheduleHoursCheckConflictXHR = function () {
            facilityApp.site.showError('Error occurred while saving driver schedule hours. Please reload the page and try again!');
        };
        driverSchedule.prototype.onSaveWeeklyScheduleHoursXHR = function (data) {
            var ajaxSettings = {
                url: driverScheduleModule.URLHelper.saveWeeklyDriverSdhedule,
                type: 'POST',
                traditional: true,
                data: JSON.stringify(data),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                async: true,
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackOnSaveWeeklyScheduleHoursXHR.bind(this))
                .fail(this.errorCallbackOnSaveWeeklyScheduleHoursXHR);
        };
        driverSchedule.prototype.successCallbackOnSaveWeeklyScheduleHoursXHR = function (response) {
            if (response.success == false) {
                facilityApp.site.showError(response.message);
            }
            this.changeEditDriverOptions(editDriverOptions.driverScheduleHours);
            /*
            if (result == "error") {
                alert('Error occurred while saving driver schedule hours. Please reload the page and try again!');
            } else {
               // alert('Drivers hours has been scheduled for the selected dates.');

                //After save just refresh page so that user may not submit again with save meause
                this.changeEditDriverOptions(editDriverOptions.driverScheduleHours);
            }*/
            //As per User requirements, after save data we should close popup window
            this.modalJqgridClose.click();
            $.blockUI;
        };
        driverSchedule.prototype.errorCallbackOnSaveWeeklyScheduleHoursXHR = function () {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        };
        driverSchedule.prototype.validateWeeklyScheduleHours = function () {
            var daywisecheckboxes = $('.weeklyscheduleday:checked', this.tBodyDriverWeeklyScheduleHours);
            var errmsg = "";
            var fromdate = new Date(this.driverWeeklyScheduleFromDate.val());
            var todate = new Date(this.driverWeeklyScheduleToDate.val());
            //Its an extra condition to make sure Start date should be less than End date
            if (fromdate > todate) {
                errmsg = "From date should be less than To date.";
            }
            if (daywisecheckboxes.length <= 0) {
                errmsg = errmsg + "\n Please select at least one day to schedule.";
            }
            var allschedulerows = $('.weeklyschedule-row', this.tBodyDriverWeeklyScheduleHours);
            $.each(allschedulerows, function (i, o) {
                var hasAnyDayScheduled = $('.weeklyscheduleday:checked', $(o));
                if (hasAnyDayScheduled.length > 0) {
                    var startTime = $('.starttime-picker', $(o));
                    var endTime = $('.endtime-picker', $(o));
                    var flg = true;
                    if (startTime.val().legth == 0) {
                        startTime.addClass('time-error');
                        flg = false;
                    }
                    if (endTime.val().length == 0) {
                        endTime.addClass('time-error');
                        flg = false;
                    }
                    if (flg == false) {
                        errmsg = errmsg + "\n Please select Start time and End time in the driver schedule row: " + (i + 1);
                    }
                    var startTimeDt = new Date("1/1/1970" + ' ' + startTime.val());
                    var endTimeDt = new Date("1/1/1970" + ' ' + endTime.val());
                    var workingHr = '0 hr 0 min';
                    //console.log(startTimeDt);
                    //console.log(endTimeDt);
                    if (startTimeDt > endTimeDt) {
                        startTime.addClass('time-error');
                        endTime.addClass('time-error');
                        flg = false;
                        errmsg = errmsg + "\n End time should be greater than Start time in driver schedule row: " + (i + 1);
                    }
                }
            });
            if (errmsg.length > 0) {
                facilityApp.site.showAlert(errmsg);
                return false;
            }
            else {
                return true;
            }
        };
        /**
         * Save weekly schedule hours END
         */
        driverSchedule.prototype.getDriverInformation = function () {
            var request = {
                Id: this.driverId
            };
            this.sendRequestForLoadUserInfo(request);
        };
        driverSchedule.prototype.sendRequestForLoadUserInfo = function (postData) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.loadUserInfo,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackLoadUserInfo.bind(this))
                .fail(this.errorCallbackLoadUserInfo);
        };
        driverSchedule.prototype.successCallbackLoadUserInfo = function (result) {
            if (result == "error") {
                facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
            }
            else {
                this.editDriverInformation.html(result);
                var ddlStatus = $(result).find('#StatusID');
                if ($('option:selected', ddlStatus).val() == '2') {
                    facilityApp.site.showConfrimWithOkCallback('Do you want to make the existing driver active?', this.callBackMethodForActiveConfirmation);
                }
            }
            $.blockUI;
        };
        driverSchedule.prototype.callBackMethodForActiveConfirmation = function () {
            $('#StatusID option[value=2]').removeAttr('selected');
            $('#StatusID option[value=1]').attr('selected', 'selected');
        };
        //bindDatePicker() {
        //    this.imgStartDateIcon = document.getElementById('driverAddStartDateIcon');
        //    let txtStartDate = $('#driverAddStartDate');
        //    this.imgStartDateIcon.onclick = (e) => { e.preventDefault(); txtStartDate.focus(); }
        //    txtStartDate.datepicker({ minDate: 0 });
        //}
        driverSchedule.prototype.errorCallbackLoadUserInfo = function (result) {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        };
        driverSchedule.prototype.onClickSaveDriverInfo = function () {
            var optionsVal = this.ddlEditDriverOptions.value;
            if (optionsVal == editDriverOptions.driverInformation) {
                this.sendRequestToSaveDriverDetails();
            }
            else if (optionsVal == editDriverOptions.rover) {
                this.sendRequestToSaveRover();
            }
            else if (optionsVal == editDriverOptions.transfer) {
                this.sendRequestToSaveRover();
            }
            else if (optionsVal == editDriverOptions.driverScheduleHours) {
                this.onsaveWeeklyScheduleHours();
            }
        };
        driverSchedule.prototype.sendRequestToSaveRover = function () {
            if (this.validatePremanentAndRoverData()) {
                var ajaxSettings = {
                    url: driverScheduleModule.URLHelper.saveDriverRoverTransfer,
                    type: 'POST',
                    traditional: true,
                    data: JSON.stringify(this.getRoverAndTransferData()),
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    async: true,
                };
                $.ajax(ajaxSettings)
                    .then(this.successCallbackToSendRequestTosaveDriverRoverTransfer.bind(this))
                    .fail(this.errorCallbackToSaveDriverRoverTransfer);
            }
        };
        driverSchedule.prototype.successCallbackToSendRequestTosaveDriverRoverTransfer = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                var optionsVal = this.ddlEditDriverOptions.value;
                if (optionsVal == editDriverOptions.rover) {
                    this.resetRoverTransferForm();
                }
                else if (optionsVal == editDriverOptions.transfer) {
                    this.resetTransferForm();
                }
                //As per User requirements, after save data we should close popup window
                this.modalJqgridClose.click();
                //alert("Driver details has been updated successfully.");
            }
            $.blockUI;
        };
        driverSchedule.prototype.errorCallbackToSaveDriverRoverTransfer = function (result) {
            facilityApp.site.showError('Error occurred while saving driver information. Please reload the page and try again!');
        };
        driverSchedule.prototype.validatePremanentAndRoverData = function () {
            var optionsVal = this.ddlEditDriverOptions.value;
            var isValid = true;
            var msg = new Array();
            if (optionsVal == editDriverOptions.transfer) {
                if ($('option:selected', this.ddlDriverTransferPermFacilites).val() == '-1') {
                    //facilityApp.site.showError('Please select facility to transfer.');
                    msg.push('Please select facility to transfer.');
                    isValid = false;
                }
                if ($("#permanentDriverTransferDate").val() == '') {
                    //facilityApp.site.showError("Available in date cannot be empty.");
                    msg.push('Available in date cannot be empty.');
                    //$("#permanentDriverTransferDate").focus();
                    isValid = false;
                }
            }
            else if (optionsVal == editDriverOptions.rover) {
                if ($('option:selected', this.ddlDriverRoverFacilites).val() == '-1') {
                    //facilityApp.site.showError('Please select facility to transfer.');
                    msg.push('Please select facility to transfer.');
                    isValid = false;
                }
                if ($("#driverRoverSendToDate").val() == '') {
                    //facilityApp.site.showError("Available in date cannot be empty.");
                    msg.push('Available in date cannot be empty.');
                    //$("#driverRoverSendToDate").focus();
                    isValid = false;
                }
                if ($("#driverRoverBackOnDate").val() == '') {
                    //facilityApp.site.showError("Back in date cannot be empty.");
                    msg.push('Back in date cannot be empty.');
                    //$("#driverRoverBackOnDate").focus();
                    isValid = false;
                }
            }
            if (isValid == false) {
                facilityApp.site.showError(msg.join('<br/>'));
            }
            return isValid;
        };
        driverSchedule.prototype.getRoverAndTransferData = function () {
            var optionsVal = this.ddlEditDriverOptions.value;
            var sStartdate;
            var sEnddate;
            //var FromFacilityID; you can get this from session
            var sToFacilityID;
            var sFK_AccessLevelID;
            var sFk_UserId; //this.driverId
            if (optionsVal == editDriverOptions.transfer) {
                sStartdate = $('#permanentDriverTransferDate').val();
                sToFacilityID = this.ddlDriverTransferPermFacilites.value;
                sFK_AccessLevelID = accessLevels.PrimaryFacility;
                sFk_UserId = this.driverId;
            }
            else if (optionsVal == editDriverOptions.rover) {
                sStartdate = $('#driverRoverSendToDate').val();
                sEnddate = $('#driverRoverBackOnDate').val();
                sToFacilityID = this.ddlDriverRoverFacilites.value;
                sFK_AccessLevelID = accessLevels.Rover;
                sFk_UserId = this.driverId;
            }
            return {
                StartDate: sStartdate,
                EndDate: sEnddate,
                ToFacilityID: sToFacilityID,
                FK_AccessLevelID: sFK_AccessLevelID,
                FK_UserID: sFk_UserId
            };
        };
        driverSchedule.prototype.sendRequestToSaveDriverDetails = function () {
            if (this.validateEditDriverDetailsForm()) {
                var dt = $("#frmEditDriverDetails").serializeArray();
                var data = {};
                $.each(dt, function (index, value) {
                    data[value.name] = value.value;
                });
                var ajaxSettings = {
                    url: driverMaintenanceFormModule.URLHelper.saveDriverDetails,
                    type: 'POST',
                    traditional: true,
                    data: JSON.stringify(data),
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    async: true,
                };
                $.ajax(ajaxSettings)
                    .then(this.successCallbackToSaveDriverDetails.bind(this))
                    .fail(this.errorCallbackToSaveDriverDetails);
            }
        };
        driverSchedule.prototype.successCallbackToSaveDriverDetails = function (result) {
            if (result.success == false) {
                if (result.data == "RoleMismatch") {
                    facilityApp.site.showAlertRoleMismatch(result.message, "Edit Driver Information");
                }
                else
                    facilityApp.site.showError(result.message);
            }
            else {
                //alert("Driver details has been updated successfully.");
                //As per User requirements, after save data we should close popup window
                this.modalJqgridClose.click();
            }
            $.blockUI;
        };
        driverSchedule.prototype.errorCallbackToSaveDriverDetails = function (result) {
            facilityApp.site.showError('Error occurred while saving driver information. Please reload the page and try again!');
        };
        driverSchedule.prototype.validateEditDriverDetailsForm = function () {
            var isValid = true;
            var Password = $('#txtPassword');
            var errMsgArray = new Array();
            //validate first name
            if (String.isNullOrEmpty($('#driverFirstName').val())) {
                $('#driverFirstName').css("border", "1px solid red");
                $("#driverFirstName").attr('title', 'Please enter first name.');
                errMsgArray.push('Please enter first name.');
                $('#driverFirstName').focus();
                isValid = false;
            }
            else {
                $('#driverFirstName').css("border", "1px #bababa solid");
            }
            //validate last name
            if (String.isNullOrEmpty($('#driverLastName').val())) {
                $('#driverLastName').css("border", "1px solid red");
                $("#driverLastName").attr('title', 'Please enter last name.');
                errMsgArray.push('Please enter last name.');
                $('#driverLastName').focus();
                isValid = false;
            }
            else {
                $('#driverLastName').css("border", "1px #bababa solid");
            }
            //Validate Email
            if (String.isNullOrEmpty($('#driverEmail').val()) || !this.validateEmail($('#driverEmail').val())) {
                $('#driverEmail').css("border", "1px solid red");
                $("#driverEmail").attr('title', 'Please enter valid email address.');
                errMsgArray.push('Please enter valid email address.');
                $('#driverEmail').focus();
                isValid = false;
            }
            else {
                $('#driverEmail').css("border", "1px #bababa solid");
            }
            //validate license
            if ($('#LicenseClassID').val() == '') {
                $('#LicenseClassID').css("border", "1px solid red");
                $("#LicenseClassID").attr('title', 'Please select the license class.');
                errMsgArray.push('Please select the license class.');
                $('#LicenseClassID').focus();
                isValid = false;
            }
            else {
                $('#LicenseClassID').css("border", "1px #bababa solid");
            }
            //validate driver status.
            if ($('#StatusID').val() == '') {
                $('#StatusID').css("border", "1px solid red");
                $("#StatusID").attr('title', 'Please select the driver status.');
                errMsgArray.push('Please select the driver status.');
                $('#StatusID').focus();
                isValid = false;
            }
            else {
                $('#StatusID').css("border", "1px #bababa solid");
            }
            if (String.isNullOrEmpty(Password.val())) {
                Password.css("border", "1px solid red");
                Password.attr('title', 'Please enter password.');
                errMsgArray.push('Please enter password.');
                Password.focus();
                isValid = false;
            }
            else if (String.hasWhiteSpace(Password.val())) {
                Password.css("border", "1px solid red");
                Password.attr('title', 'Password should not contain spaces.');
                errMsgArray.push('Password should not contain spaces.');
                Password.focus();
                isValid = false;
            }
            else if (Password.val().length < driverMaintenanceFormModule.DefaultVariables.PasswordMinLength) {
                var errorMsg = 'Password must be at least ' + driverMaintenanceFormModule.DefaultVariables.PasswordMinLength + ' characters.';
                Password.css("border", "1px solid red");
                Password.attr('title', errorMsg);
                errMsgArray.push(errorMsg);
                Password.focus();
                isValid = false;
            }
            else {
                Password.css("border", "1px #bababa solid");
            }
            if (isValid == false) {
                facilityApp.site.showError(errMsgArray.join("<br/>"));
            }
            return isValid;
        };
        driverSchedule.prototype.validateEmail = function (email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
            ;
        };
        driverSchedule.prototype.updateDialogAfterShowForm = function (formid) {
            this.getWorkingHours(formid, true, false);
        };
        driverSchedule.prototype.updateDialogOnclickSubmit = function (params) {
            return { dates: this.dsGrid.jqGrid('getGridParam', 'selarrrow'), driverID: this.driverId };
        };
        driverSchedule.prototype.updateDialogAfterComplete = function (response) {
            var json = jQuery.parseJSON(response.responseText);
            //console.log(json);
            if (json.success == false) {
                facilityApp.site.showError(json.message);
            }
        };
        driverSchedule.prototype.onUpdateDriverScheduleAfterComplete = function (response) {
            var json = jQuery.parseJSON(response.responseText);
            //console.log(json);
            if (json.success == false) {
                facilityApp.site.showError(json.message);
            }
        };
        driverSchedule.prototype.getWorkingHours = function (formid, onload, isMultipleSelected) {
            //When user select multiple check box from UI and trying to update all at once, by that time we are showing default values as 07AM to 05PM.
            if (onload && isMultipleSelected) {
                $("select#DayWork", formid).val('S');
                $("input#ScheduleStartTime", formid).val('07:00 AM');
                $("input#ScheduleEndTime", formid).val('05:00 PM');
            }
            var startTime = $("input#ScheduleStartTime", formid).val();
            var endTime = $("input#ScheduleEndTime", formid).val();
            var DayWork = $("select#DayWork", formid).val();
            var workingHr = '0 hr 0 min';
            //console.log('DayWork: ' + DayWork + '; startTime: ' + startTime + '; endtime: ' + endTime);
            //Here status S-Scheduled, O-Off, C-On-Call
            if ((DayWork != 'O' || DayWork != 'C') && startTime != '' && endTime != '') {
                var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
                var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
                if (startTimeDt < endTimeDt) {
                    var differenceDt = new Date(endTimeDt - startTimeDt);
                    var hours = differenceDt.getUTCHours();
                    var minutes = differenceDt.getUTCMinutes();
                    workingHr = hours + ' hr ' + minutes + ' min';
                }
            }
            $("span#DispDriverWorkingHours", formid).html(workingHr);
        };
        driverSchedule.prototype.getWorkingHoursElement = function (value, options) {
            var el = document.createElement("span");
            //el.type = "span";
            el.className = 'oprationHoursDuration';
            el.innerHTML = value;
            return el;
        };
        driverSchedule.prototype.getWorkingHoursValue = function (elem, operation, value) {
            if (operation === 'get') {
                return $(elem).val();
            }
            else if (operation === 'set') {
                //$('span', elem).val(value);
                $(elem, 'span').each(function (index, element) { $(this).text(value); });
            }
        };
        driverSchedule.prototype.endTimeChangeAssignOpertingHours = function (value, colname) {
            var startTime = $("input#ScheduleStartTime").val();
            var endTime = $("input#ScheduleEndTime").val();
            var DayWork = $("select#DayWork").val();
            var workingHr = '0 hr 0 min';
            //Here status S-Scheduled, O-Off, C-On-Call
            if ((DayWork != 'O' || DayWork != 'C') && startTime != '' && endTime != '') {
                var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
                var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
                if (startTimeDt < endTimeDt) {
                    var differenceDt = new Date(endTimeDt - startTimeDt);
                    var hours = differenceDt.getUTCHours();
                    var minutes = differenceDt.getUTCMinutes();
                    workingHr = hours + ' hr ' + minutes + ' min';
                }
            }
            $("span#DispDriverWorkingHours").html(workingHr);
        };
        driverSchedule.prototype.dayWorkChanged = function (e) {
            if ($(e.target).val() == "C" || $(e.target).val() == "O") {
                $("#ScheduleStartTime").val('12:00 AM');
                $("#ScheduleEndTime").val('12:00 AM');
            }
            else {
                $("#ScheduleStartTime").val('07:00 AM');
                $("#ScheduleEndTime").val('05:00 PM');
            }
            this.displayWorkingHours($("#ScheduleStartTime").val(), $("#ScheduleEndTime").val());
        };
        driverSchedule.prototype.displayWorkingHours = function (startTime, endTime) {
            var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
            var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
            var workingHr = '0 hr 0 min';
            if (startTimeDt < endTimeDt) {
                var differenceDt = new Date(endTimeDt - startTimeDt);
                var hours = differenceDt.getUTCHours();
                var minutes = differenceDt.getUTCMinutes();
                workingHr = hours + ' hr ' + minutes + ' min';
            }
            $("#DispDriverWorkingHours").html(workingHr);
        };
        driverSchedule.prototype.checkStartEndTime = function (value, colname) {
            var startTime = $("input#ScheduleStartTime").val();
            var endTime = $("input#ScheduleEndTime").val();
            var DayWork = $("select#DayWork").val();
            if (DayWork == 'S') {
                if (startTime != '' && endTime != '') {
                    var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
                    var endTimeDt = new Date("1/1/1970" + ' ' + endTime);
                    if (startTimeDt >= endTimeDt) {
                        return [false, "Start time should be less than End time.", ""];
                    }
                }
            }
            return [true, "", ""];
        };
        ;
        driverSchedule.prototype.onUpdateDriverSchedule = function (evt, parent) {
            var rows = this.dsGrid.jqGrid('getGridParam', 'selarrrow');
            if (rows.length > 0) {
                this.dsGrid.jqGrid('editGridRow', rows, {
                    url: URLHelper.saveDriverScheduleHours,
                    height: 300,
                    width: 450,
                    zIndex: 1001,
                    stack: true,
                    closeAfterEdit: true,
                    reloadAfterSubmit: true,
                    beforeSubmit: this.onUpdateDriverScheduleBeforeSubmit.bind(this),
                    afterShowForm: this.onUpdateDriverScheduleAfterShowForm.bind(this),
                    onclickSubmit: this.onUpdateDriverScheduleOnclickSubmit.bind(this),
                    afterComplete: this.onUpdateDriverScheduleAfterComplete.bind(this)
                });
            }
            else
                facilityApp.site.showAlert("Please select a row.");
        };
        driverSchedule.prototype.onUpdateDriverScheduleBeforeSubmit = function (postdata, formid) {
            var anyLoadFoundForThisDates = [];
            var rowids = this.dsGrid.jqGrid('getGridParam', 'selarrrow');
            anyLoadFoundForThisDates = $.grep(rowids, this.grepCompareArrayForDates.bind(this));
            //Confirmation message needs to display only when user is trying to update schedule hours.
            //In other case any way we dont allow user to inactive driver schedule schedule hours
            if (anyLoadFoundForThisDates.length > 0 && postdata.DayWork == "S") {
                var msg = anyLoadFoundForThisDates.join(",");
                msg = "There is an issue with the driver routes for the following dates: " + msg + ".  The driver start time isn’t the same as the route start time or the route ends after the driver end time.<br/><br/>Press the continue button if you want to make the changes or cancel to not make the changes.  If you decide to accept the changes, please visit the Local Dispatch page and optimize the route to provide more accurate ETAs.";
                facilityApp.site.confrim(msg, "Warning!", this.continueUpdateDriverScheduleHours.bind(this), this.cancleUpdateDriverScheduleHours.bind(this));
                return [false, ''];
            }
            else
                return [true, ''];
        };
        driverSchedule.prototype.continueUpdateDriverScheduleHours = function () {
            //If user wants to cotinue to update schedule hourse, then we dont need to keep this list. 
            //If we dont clear it then again it shows confirmation message
            this.hdnDriverLockedLoadDates = [];
            //after making hdnDriverLockedLoadDates array to empty, please click on sData button to submit form
            $('#sData').click();
        };
        driverSchedule.prototype.cancleUpdateDriverScheduleHours = function () {
            //dont need to do any action on cancle... just stay on same page and display edit form
            $("#FormError").css("display", "none");
        };
        driverSchedule.prototype.grepCompareArrayForDates = function (el, i) {
            var dt = ((new Date(el)).toDateMMDDYYYY());
            return $.inArray(dt, this.hdnDriverLockedLoadDates) >= 0;
        };
        driverSchedule.prototype.onUpdateDriverScheduleAfterShowForm = function (formid) {
            var rowids = this.dsGrid.jqGrid('getGridParam', 'selarrrow');
            $("select#DayWork", formid).val("S");
            this.getWorkingHours(formid, true, (rowids.length > 1));
            formid.closest(".ui-jqdialog").closest(".ui-jqdialog").position({
                my: 'center',
                at: 'center',
                of: window
            });
            ///Fire a call to get driver load dates, and store it in local variable and use it when user submitting form
            var ajaxData = { dates: rowids, driverID: this.driverId };
            this.checkAndGetDriverLockedLoadsByDateXHR(ajaxData);
        };
        driverSchedule.prototype.onUpdateDriverScheduleOnclickSubmit = function (params) {
            var rowids = this.dsGrid.jqGrid('getGridParam', 'selarrrow');
            var ajaxData = { dates: rowids, driverID: this.driverId };
            return ajaxData;
        };
        driverSchedule.prototype.checkAndGetDriverLockedLoadsByDateXHR = function (postData) {
            var ajaxSettings = {
                url: driverScheduleModule.URLHelper.checkAndGetDriverLockedLoadsByDate,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: "json",
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.succCallbackCheckAndGetDriverLockedLoadsByDate.bind(this))
                .fail(this.errCallbackCheckAndGetDriverLockedLoadsByDate);
        };
        driverSchedule.prototype.succCallbackCheckAndGetDriverLockedLoadsByDate = function (result) {
            if (result.success == false) {
                this.hdnDriverLockedLoadDates = [];
                facilityApp.site.showError('Error occurred while loading edit form to update driver schedule hours, Please reload and try again!');
            }
            else {
                this.hdnDriverLockedLoadDates = $.map(result.data, function (v, i) {
                    return (new Date(v)).toDateMMDDYYYY();
                });
            }
            $.blockUI;
        };
        driverSchedule.prototype.errCallbackCheckAndGetDriverLockedLoadsByDate = function (result) {
            this.hdnDriverLockedLoadDates = [];
        };
        driverSchedule.prototype.adjustHourChangeDimentions = function () {
            var grid = $('#gbox_' + this.gridId).parent();
            var gridParentWidth = grid.width();
            this.dsGrid.jqGrid('setGridWidth', gridParentWidth);
            //var gridParentHeight = grid.height();
            //if (gridParentHeight <= 800) {
            //    var height = ($(window).height() - $(".modal-box").outerHeight()) - 100;
            //}
        };
        driverSchedule.prototype.gridCompleteLocal = function () {
            var ids = this.dsGrid.getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var cell = this.dsGrid.getCell(ids[i], "WeekDay");
                if (cell == "Sunday" || cell == "Saturday") {
                    this.dsGrid.jqGrid('setRowData', ids[i], false, { background: '#e0effa' });
                }
            }
            //disable check boxes which are less than todays date
            var jqgList = $("input:checkbox[id^=jqg_list_]");
            $.each(jqgList, function (i, o) {
                var dt = new Date(o.id.replace('jqg_list_', ''));
                var currentdate = new Date();
                currentdate.setHours(0, 0, 0, 0);
                if (currentdate > dt)
                    $(o).remove();
            });
        };
        driverSchedule.prototype.reloadDSGrid = function () {
            this.dsGrid.jqGrid("clearGridData", true);
            this.dsGrid.jqGrid('setGridParam', {
                mtype: 'GET',
                url: driverScheduleModule.URLHelper.getDriverScheduleHours,
                editurl: driverScheduleModule.URLHelper.saveDriverScheduleHours,
                onclickSubmit: this.submitUpdatedDriverSchedule,
                datatype: 'json',
                postData: {
                    dates: this.dsGrid.jqGrid('getGridParam', 'selarrrow'),
                    startDate: this.thStartDate,
                    driverID: this.driverId
                }
            }).trigger("reloadGrid");
        };
        driverSchedule.prototype.submitUpdatedDriverSchedule = function () {
            //returning respective parameters to Jquery Ajax call
            var ajaxData = {};
            var rowIds = this.dsGrid.jqGrid('getGridParam', 'selarrrow');
            ajaxData = { dates: rowIds, driverID: this.driverId };
            return ajaxData;
        };
        driverSchedule.prototype.submitUpdateDriverForSingleRecord = function (rows) {
            return { dates: rows, driverID: this.driverId };
        };
        driverSchedule.prototype.gridDblClickRow = function (rowid, iRow, iCol, e) {
            var dt = new Date(rowid);
            var currentdate = new Date();
            currentdate.setHours(0, 0, 0, 0); //removing any hours there to current date.
            if (currentdate <= dt)
                this.dsGrid.editGridRow(rowid, this.updateDialog);
        };
        driverSchedule.prototype.gridLoadComplete = function () {
            //setTimeout(function () { driverSchedule.prototype.setGridHeight(); }, 200);
        };
        driverSchedule.prototype.gridLoadError = function (xhr, st, err) {
            if (xhr.status == "200")
                return false;
            var error = eval('(' + xhr.responseText + ')');
            $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                width: '600',
                buttons: {
                    Ok: function () {
                        //$(this).dialog("close");
                    }
                }
            });
        };
        driverSchedule.prototype.setGridHeight = function () {
            var wheight = window.innerHeight - 420;
            $('.ui-jqgrid-bdiv').not($('.ui-jqgrid-bdiv', document.getElementById("list"))).height(wheight);
        };
        driverSchedule.prototype.showDriverSchedulePopup = function (driverID, name, thstartDate, selectedDriverOptions, mainForm) {
            this.bindGridControl(this);
            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popupDriverSchedule').fadeIn();
            var driverHead = "";
            if (driverID > 0) {
                driverHead = name;
            }
            else {
                driverHead = "Driver";
            }
            $('.driver-schedule-popuphead', $('#popupDriverSchedule')).html(driverHead);
            //assigning variables to respective values
            this.driverId = driverID;
            this.thStartDate = thstartDate;
            //this.adjustHourChangeDimentions();
            this.ddlEditDriverOptions.value = selectedDriverOptions;
            var selectTabIndex = 0;
            if (selectedDriverOptions == editDriverOptions.rover)
                selectTabIndex = 1;
            //If user trying to edit rover records, we should pass tab index 1 other wise default value that is "0"
            this.changeEditDriverOptions(selectedDriverOptions, selectTabIndex);
            this.adjustPopupPosition();
        };
        driverSchedule.prototype.adjustPopupPosition = function () {
            var offset = $(document).scrollTop();
            $(".modal-box").css({
                top: offset
            });
            //var ht = $('html').offset().top;
            //ht = ht <= 0 ? (-1 * ht) : ht;
            //$(".modal-box").css({
            //    top: ht,
            //    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
            //});
        };
        driverSchedule.prototype.initPopup = function () {
            $(".js-modal-close, .modal-overlay").click(function (e) {
                e.preventDefault();
                $(".modal-box, .modal-overlay").fadeOut(500, function () {
                    $(".modal-overlay").remove();
                });
            });
            $(window).resize(function () {
                $(".modal-box").css({
                    top: 5,
                    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                });
            });
            $(window).resize();
            // this.initEvents();
        };
        driverSchedule.prototype.loadGridOnDriverChange = function (event) {
            this.driverId = event.target.value;
            this.reloadDSGrid();
        };
        driverSchedule.prototype.clearJqGridCustom = function () {
            var objRows = $('#list tr').remove();
        };
        return driverSchedule;
    }());
    driverScheduleModule.driverSchedule = driverSchedule;
})(driverScheduleModule || (driverScheduleModule = {}));
$(document).ready(function () {
    var oDriverForm = new driverScheduleModule.driverSchedule();
    oDriverForm.initPopup();
});
//# sourceMappingURL=driverSchedule.js.map