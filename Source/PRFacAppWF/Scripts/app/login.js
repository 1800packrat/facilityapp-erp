/// <reference path="Libraries\jquery.d.ts" />;
var facilityAppLogin;
(function (facilityAppLogin) {
    var URLHelper = /** @class */ (function () {
        function URLHelper() {
        }
        URLHelper.ValidateLogin = '';
        URLHelper.Dashboard = '../../Dashboard.aspx';
        return URLHelper;
    }());
    facilityAppLogin.URLHelper = URLHelper;
    var login = /** @class */ (function () {
        function login() {
            var _this = this;
            this.handleEvt = function (e) {
                var key = e.which || e.charCode || e.keyCode;
                if (key == 13) {
                    _this.submit();
                }
            };
            this.btnLogin = document.getElementById('btnLogin');
            this.btnLogin.onclick = function (e) { _this.submit(); };
            document.addEventListener("keydown", this.handleEvt);
        }
        login.prototype.validate = function () {
            ///Implement username and password validateion
            if ($('#txtLoginId').val() == '') {
                $('#lblError').html("Please enter user name.");
                $('#txtLoginId').focus();
                return false;
            }
            if ($('#txtPassword').val() == '') {
                $('#lblError').html("Please enter password.");
                $('#txtPassword').focus();
                return false;
            }
            $('#lblError').html('');
            return true;
        };
        login.prototype.submit = function () {
            if (this.validate()) {
                var request = {
                    LoginName: $('#txtLoginId').val(),
                    Password: $('#txtPassword').val(),
                    IsRememberMe: $('#chkRememberme').prop('checked')
                };
                this.sendRequest(request);
            }
        };
        login.prototype.successCallbackLogin = function (result) {
            if (result.success == true) {
                $.blockUI;
                window.location.href = result.data;
            }
            else {
                //write your logic to show errors
                $('#lblError').html(result.message);
            }
        };
        login.prototype.errorCallbackLogin = function (result) {
            //Show what is the error you got from valid login page
            $('#lblError').html(result.message);
        };
        login.prototype.sendRequest = function (postData) {
            var ajaxSettings = {
                url: facilityAppLogin.URLHelper.ValidateLogin,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: this.successCallbackLogin,
                error: this.errorCallbackLogin
            };
            $.ajax(ajaxSettings);
        };
        return login;
    }());
    facilityAppLogin.login = login;
})(facilityAppLogin || (facilityAppLogin = {}));
$(document).ready(function () {
    var oLogin = new facilityAppLogin.login();
});
//# sourceMappingURL=login.js.map