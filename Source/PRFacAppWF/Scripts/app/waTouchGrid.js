/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqgrid.d.ts" />
/// <reference path="libraries\jquery.validation.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="libraries\jquery.tmpl.d.ts" />
/// <reference path="site.ts" />
/// <reference path="libraries\html2canvas.d.ts" />
/// <reference path="libraries\jspdf.d.ts" />
var waTouchesGridModule;
(function (waTouchesGridModule) {
    var URLHelper = /** @class */ (function () {
        function URLHelper() {
        }
        URLHelper.searchTouchList = '';
        URLHelper.saveEditTouchInfoURL = '';
        URLHelper.getBOLURL = '';
        URLHelper.editQorURL = '';
        URLHelper.getEditTouchInfoURL = '';
        URLHelper.touchTicketURL = '';
        URLHelper.validateUnitName = '';
        URLHelper.getTouchHistory = '';
        return URLHelper;
    }());
    waTouchesGridModule.URLHelper = URLHelper;
    var WATouchStatus;
    (function (WATouchStatus) {
        WATouchStatus[WATouchStatus["Open"] = 0] = "Open";
        WATouchStatus[WATouchStatus["Scheduled"] = 1] = "Scheduled";
        WATouchStatus[WATouchStatus["Stage"] = 2] = "Stage";
        WATouchStatus[WATouchStatus["Completed"] = 3] = "Completed";
        WATouchStatus[WATouchStatus["OnHold"] = 4] = "OnHold";
    })(WATouchStatus = waTouchesGridModule.WATouchStatus || (waTouchesGridModule.WATouchStatus = {}));
    var touchTypes;
    (function (touchTypes) {
        touchTypes[touchTypes["warehouse"] = 1] = "warehouse";
        touchTypes[touchTypes["transportation"] = 2] = "transportation";
        touchTypes[touchTypes["staging"] = 3] = "staging";
        touchTypes[touchTypes["all"] = 4] = "all";
        touchTypes[touchTypes["none"] = 5] = "none";
        touchTypes[touchTypes["custom"] = 6] = "custom";
        touchTypes[touchTypes["needsAction"] = 7] = "needsAction";
    })(touchTypes = waTouchesGridModule.touchTypes || (waTouchesGridModule.touchTypes = {}));
    var AlertColorCode = /** @class */ (function () {
        function AlertColorCode() {
        }
        AlertColorCode.type1 = "#F1C1C1";
        AlertColorCode.type2 = "#E78787";
        return AlertColorCode;
    }());
    waTouchesGridModule.AlertColorCode = AlertColorCode;
    var TouchTypeDesc = /** @class */ (function () {
        function TouchTypeDesc() {
        }
        TouchTypeDesc.TranportationTouches = {
            DE: 'DE',
            RF: 'RF',
            CC: 'CC',
            DF: 'DF',
            RE: 'RE'
        };
        TouchTypeDesc.WarehouseTouches = {
            WA: 'WA',
            TO: 'TO',
            TI: 'TI'
        };
        TouchTypeDesc.Stage = {
            IBO: 'IBO',
            OBO: 'OBO'
        };
        return TouchTypeDesc;
    }());
    waTouchesGridModule.TouchTypeDesc = TouchTypeDesc;
    var waTouchesGrid = /** @class */ (function () {
        function waTouchesGrid() {
            var _this = this;
            this.gridId = "list";
            this.gridListId = "listPager";
            this.gridTitle = "TOUCHES";
            this.dsGrid = null;
            this.dsGridList = null;
            this.dsGrid = $("#" + this.gridId);
            this.$searchdata = { key: "", data: {} };
            $('#edittouchInfo').tabs({
                activate: function (event, ui) {
                    if (ui.newTab.index() == 1) {
                        $('#popupEditTouchDetails').find("#modelSaveTouchInfo").hide();
                    }
                    else {
                        $('#popupEditTouchDetails').find("#modelSaveTouchInfo").show();
                    }
                }
            });
            this.btnSearch = document.getElementById('btnSearch');
            this.btnSearch.onclick = function (e) { e.preventDefault(); _this.validateAndSubmit(); $('#showHideButton').trigger('click'); };
            $('#IsShowPriorNotCompletedTouches').change(function (e) { e.preventDefault(); _this.validateAndSubmit(); });
            this.btnReset = document.getElementById('btnReset');
            this.btnReset.onclick = function (e) {
                window.location.reload();
            };
            this.btnPrint = document.getElementById('btnPrint');
            this.btnPrint.onclick = function (e) {
                e.preventDefault();
                if (_this.dsGrid.getGridParam('records') === 0) {
                    facilityApp.site.showError("There are no touches to print.");
                }
                else {
                    _this.handlePrintFunctionality();
                }
            };
            this.showHideButton = document.getElementById('showHideButton');
            this.showHideButton.onclick = function (e) {
                e.preventDefault();
                $(e.target).toggleClass("ui-icon-circle-triangle-n ui-icon-circle-triangle-s");
                $('.form-list-watouches').slideToggle();
            };
            this.hShowHide = document.getElementById('hShowHide');
            this.hShowHide.onclick = function (e) {
                e.preventDefault();
                $(e.target).toggleClass("ui-icon-circle-triangle-n ui-icon-circle-triangle-s");
                $('.form-list-watouches').slideToggle();
            };
            //Start date datepicker initilization
            var txtStartDate = $('#StartDate');
            this.imgStartDateIcon = document.getElementById('StartDateIcon');
            this.imgStartDateIcon.onclick = function (e) { e.preventDefault(); txtStartDate.focus(); };
            txtStartDate.datepicker({
                onSelect: function (dateText, inst) {
                    txtEndDate.datepicker("option", "minDate", txtStartDate.datepicker("getDate"));
                }.bind(this)
            });
            //End date datepicker binding
            var txtEndDate = $('#EndDate');
            this.imgEndDateIcon = document.getElementById('EndDateIcon');
            this.imgEndDateIcon.onclick = function (e) { e.preventDefault(); txtEndDate.focus(); };
            txtEndDate.datepicker({
                onSelect: function (dateText, inst) {
                    txtStartDate.datepicker("option", "maxDate", txtEndDate.datepicker("getDate"));
                }.bind(this)
            });
            this.WarehouseTouchFilterCriteria = $('#WarehouseTouchFilterCriteria');
            this.ddlTouchType = $('#TouchType');
            this.ddlTouchType.change(function (e) { e.preventDefault(); _this.onChangeTouchTypeChange(e, _this); });
            this.ddlTouchType.val(touchTypes.needsAction);
            this.checkNeedsActionCheckBoxes();
            this.btnSaveTouchInfo = document.getElementById('modelSaveTouchInfo');
            this.btnSaveTouchInfo.onclick = function (e) {
                e.preventDefault();
                _this.validateAndSaveTouchInfo();
            };
            //Dropdown selection changing according to checkboxes selection.
            $('.chkTouchType').unbind("change").change(function (e) {
                _this.selectCustomFromDropDown(e, _this);
            });
        }
        waTouchesGrid.prototype.selectCustomFromDropDown = function (evet, parent) {
            var checkedVals = $('input:checkbox:checked.chkTouchType').map(function () {
                return $(this).attr('data-touchtype');
            }).get();
            if (checkedVals.length == 10) {
                $('#TouchType', '#divFilterWareHouseTouches').val(4); //selecting All if all the checkboxes were selected
            }
            else if (checkedVals.length == 0) {
                $('#TouchType', '#divFilterWareHouseTouches').val(5); //selecting None if all the checkboxes were not selected
            }
            else if ((checkedVals.indexOf('DE') > -1) && (checkedVals.indexOf('RF') > -1) && (checkedVals.indexOf('CC') > -1) && (checkedVals.indexOf('DF') > -1) && (checkedVals.indexOf('RE') > -1) && checkedVals.length == 5) {
                $('#TouchType', '#divFilterWareHouseTouches').val(2);
            }
            else if ((checkedVals.indexOf('WA') > -1) && (checkedVals.indexOf('TO') > -1) && (checkedVals.indexOf('TI') > -1) && (checkedVals.indexOf('IBO') > -1) && (checkedVals.indexOf('OBO') > -1) && checkedVals.length == 5) {
                $('#TouchType', '#divFilterWareHouseTouches').val(1);
            }
            else if ((checkedVals.indexOf('DE') > -1) && (checkedVals.indexOf('DF') > -1) && (checkedVals.indexOf('WA') > -1) && (checkedVals.indexOf('TO') > -1) && (checkedVals.indexOf('IBO') > -1) && (checkedVals.indexOf('OBO') > -1) && checkedVals.length == 6) {
                $('#TouchType', '#divFilterWareHouseTouches').val(3);
            }
            else {
                $('#TouchType', '#divFilterWareHouseTouches').val(6);
            }
        };
        waTouchesGrid.prototype.handlePrintPDFFunctionality = function () {
            var data = $('.cust-grid-jqgrid').clone(); // $('.tblDriverScheduleHrs').html();
            $(data).find('.ui-jqgrid-titlebar').remove();
            data.find('.ui-common-table').css('table-layout', 'fixed');
            data.find('.ui-common-table').css('width', '100%');
            data.find('#divFilterWareHouseTouches').height('100%');
            data.find('#divFilterWareHouseTouches').css('overflow', 'visible');
            data.find('tbody').css('overflow', 'visible');
            data.find('th').each(function () { $(this).css('top', '0px'); });
            $(data).find('td').css('font-size', '15px');
            $(data).find('td').css('white-space', 'normal');
            $(data).removeAttr('class');
            $(data).removeAttr('style');
            data.find('.ui-jqgrid-btable tbody tr:first').remove();
            data.find('.ui-jqgrid-btable tbody tr').css("background-color", "");
            $(data).find('.ui-jqgrid-htable thead tr').clone().prependTo(data.find('.ui-jqgrid-btable tbody'));
            $(data).find('.ui-jqgrid-htable').remove();
            $(data).find('.printHide').remove();
            $(data).find('.printShow').css('display', '');
            $(data).find('th').css('white-space', 'normal');
            $(data).find('th').css('height', '43px');
            $(data).find('th').css('font-size', '17px');
            data.find('a').each(function () {
                $(this).removeAttr("href");
            });
            data.find('#pg_listPager').remove();
            //creating multiple tables in order to divided the pages in pdf viewer
            var $main = $('.ui-jqgrid-btable'), $head = $main.find('thead tr:first'), $extraRows = $main.find('tbody tr:gt(35)');
            for (var i = 0; i < $extraRows.length; i = i + 35) {
                $('<table>').append($head.clone(), $extraRows.slice(i, i + 35)).appendTo($main.parent());
            }
            var doc = new jsPDF('l', 'mm', 'a4');
            var specialElementHandlers = {
                '#editor': function (element, renderer) {
                    return true;
                }
            };
            var margins = {
                top: 80,
                bottom: 60,
                left: 40,
                width: 522
            };
            var quotes = $('#mainContent').html(data.html());
            doc.addHTML($('#mainContent'), 10, 10, { retina: true }, function () {
                doc.addPage();
                doc.addHTML($('table').get(0), function () {
                    doc.autoPrint();
                    doc.output('dataurlnewwindow');
                });
            });
            $('#mainContent').html('');
        };
        waTouchesGrid.prototype.handlePrintFunctionality = function () {
            var data = $('.cust-grid-jqgrid').clone(); // $('.tblDriverScheduleHrs').html();
            data.find('.ui-common-table').css('table-layout', 'fixed');
            data.find('.ui-common-table').css('width', '100%');
            data.find('#divFilterWareHouseTouches').height('100%');
            data.find('#divFilterWareHouseTouches').css('overflow', 'visible');
            data.find('tbody').css('overflow', 'visible');
            data.find('th').each(function () { $(this).css('top', '0px'); });
            ////hiding unit number column form print 
            data.find('.ui-jqgrid-btable tbody tr:first').remove();
            data.find('.ui-jqgrid-btable tbody tr').css("background-color", "");
            $(data).find('.ui-jqgrid-htable thead tr').clone().prependTo(data.find('.ui-jqgrid-btable tbody'));
            $(data).find('.ui-jqgrid-htable').remove();
            $(data).find('.printHide').remove();
            $(data).find('.printShow').css('display', '');
            data.find('a').each(function () {
                $(this).removeAttr("href");
            });
            data.find('#pg_listPager').remove();
            var mywindow = window.open('', 'Touch List', 'height=400,width=600');
            mywindow.document.write('<html><head><title>Touch List</title>');
            mywindow.document.write('<link href="/Content/css/footable.core.css" rel="stylesheet" media="print"/><link href="/Content/css/footable.standalone.css" rel="stylesheet" media="print" /> <style type="text/css" >   @media print { @page { size: landscape; width: 9000px; }, table thead tr th.center,  table tbody tr td.center { text-align: center !important; } table th, table td { border:1px solid #ccc; padding: 0.005em; } } </style>');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data.html());
            mywindow.document.write('</body></html>');
            mywindow.print();
            mywindow.close();
        };
        waTouchesGrid.prototype.validateAndSaveTouchInfo = function () {
            console.log("TDB - flow check, validateAndSaveTouchInfo");
            var vEditTouchDetails = $('#dvEditTouchDetails');
            var key = $('#hfTouchKey', vEditTouchDetails).val();
            var touchInfo = this.getTouchInfoByKey(key);
            var hdnUnitNumberValidationReq = $('#hdnUnitNumberValidationReq').val() == "true" ? true : false;
            //hdnUnitNumberValidationReq flag will be true when unit name is required for touch completion. Now we need this unit name for only IBO touch completion
            if (hdnUnitNumberValidationReq) {
                var postData = {
                    unitName: $.trim($('#UnitNumber', vEditTouchDetails).val())
                };
                if (postData.unitName == undefined || postData.unitName.length <= 0) {
                    facilityApp.site.showError("Please enter unit name to complete touch.");
                }
                else {
                    if (postData.unitName == "000016" || postData.unitName == "000012" || postData.unitName == "000008") {
                        facilityApp.site.showConfrimWithOkCallback("'" + postData.unitName + "' is default unit name, Do you want to continue with default unit?", this.saveTouchInfo.bind(this));
                    }
                    else {
                        var ajaxSettings = {
                            url: waTouchesGridModule.URLHelper.validateUnitName,
                            type: 'POST',
                            data: JSON.stringify(postData),
                            traditional: true,
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8'
                        };
                        $.ajax(ajaxSettings)
                            .then(this.successCBValidateUnitName.bind(this, postData))
                            .fail(this.errorCBValidateUnitName);
                    }
                }
            }
            else {
                this.saveTouchInfo();
            }
        };
        waTouchesGrid.prototype.successCBValidateUnitName = function (postData, result) {
            console.log("TDB - flow check, successCBValidateUnitName");
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                if (result.data == true) {
                    this.saveTouchInfo();
                }
                else {
                    facilityApp.site.showConfrimWithOkCallback("'" + postData.unitName + "' unit is not available in the inventory, Do you want to cotinue with this unit?", this.saveTouchInfo.bind(this));
                }
            }
        };
        waTouchesGrid.prototype.errorCBValidateUnitName = function () {
            facilityApp.site.showError("Error occurred while saving touch information, please reload and try again!");
        };
        waTouchesGrid.prototype.saveTouchInfo = function () {
            console.log("TDB - flow check, saveTouchInfo");
            var editDvPopup = $('#dvEditTouchDetails');
            var key = $('#hfTouchKey', editDvPopup).val();
            var touchInfo = this.getTouchInfoByKey(key);
            if (touchInfo) {
                var actionId = $('#ddEditAction option:selected', $('#dvEditTouchDetails')).val();
                var isChangeAction = (actionId > 0 && touchInfo.ActionId != actionId); ///This should compare old and new values
                var actioncategoryname = $('#ddEditAction option:selected', $('#dvEditTouchDetails')).data('actioncategoryname');
                var unitname = touchInfo.UnitName;
                var hdnUnitNumberValidationReq = $('#hdnUnitNumberValidationReq').val() == "true" ? true : false;
                if (hdnUnitNumberValidationReq && touchInfo.TouchType == TouchTypeDesc.Stage.IBO && actioncategoryname == 'Completed') {
                    unitname = $('#UnitNumber', $('#dvEditTouchDetails')).val();
                }
                var postData = {
                    QORId: touchInfo.QORId,
                    TouchTypeFull: touchInfo.TouchTypeFull,
                    SequenceNum: touchInfo.SequenceNum,
                    StarsId: touchInfo.StarsId,
                    StarsUnitId: touchInfo.StarsUnitId,
                    UnitName: unitname,
                    ScheduledDate: touchInfo.DispScheduledDate,
                    SLScheduledDate: touchInfo.DispSLScheduledDate,
                    TouchStatusID: touchInfo.TouchStatusID,
                    TouchStatus: touchInfo.TouchStatus,
                    SLTouchStatus: touchInfo.SLTouchStatus,
                    FirstName: touchInfo.FirstName,
                    LastName: touchInfo.LastName,
                    PhoneNumber: touchInfo.PhoneNumber,
                    TouchTime: touchInfo.TouchTime,
                    CustAddress1: touchInfo.CustAddress1,
                    CustAddress2: touchInfo.CustAddress2,
                    CustCity: touchInfo.CustCity,
                    CustRegion: touchInfo.CustRegion,
                    CustZip: touchInfo.CustZip,
                    FromFirstName: touchInfo.FromFirstName,
                    FromLastName: touchInfo.FromLastName,
                    FromPhone: touchInfo.FromPhone,
                    FromAddr1: touchInfo.FromAddr1,
                    FromAddr2: touchInfo.FromAddr2,
                    FromCity: touchInfo.FromCity,
                    FromState: touchInfo.FromState,
                    FromZip: touchInfo.FromZip,
                    ToFirstName: touchInfo.ToFirstName,
                    ToLastName: touchInfo.ToLastName,
                    ToPhone: touchInfo.ToPhone,
                    ToAddr1: touchInfo.ToAddr1,
                    ToAddr2: touchInfo.ToAddr2,
                    ToCity: touchInfo.ToCity,
                    ToState: touchInfo.ToState,
                    ToZip: touchInfo.ToZip,
                    Comments: $('#Notes', editDvPopup).val(),
                    ActionID: $('#ddEditAction option:selected', $('#dvEditTouchDetails')).val(),
                    ActionName: $('#ddEditAction option:selected', $('#dvEditTouchDetails')).text(),
                    TouchActionCategory: actioncategoryname,
                    ByPassActionChangeToSyncData: $('#ddEditAction option:selected', $('#dvEditTouchDetails')).data('bypassactionchangetosyncdata'),
                    IsChangeAction: isChangeAction
                };
                var ajaxSettings = {
                    url: waTouchesGridModule.URLHelper.saveEditTouchInfoURL,
                    type: 'POST',
                    data: JSON.stringify(postData),
                    traditional: true,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8'
                };
                $.ajax(ajaxSettings)
                    .then(this.successCallbacksaveTouchInfo.bind(this))
                    .fail(this.errorCallbacksaveTouchInfo);
            }
            else {
                facilityApp.site.showAlert('Error occurred while saving touch information, Please reload page and try again!');
            }
        };
        waTouchesGrid.prototype.successCallbacksaveTouchInfo = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                //After save data, we need to reload grid. To reload grid, we have to clear local data variable. so that system will get new data from server.
                this.$searchdata.key = "";
                this.validateAndSubmit();
                //After save data, we need to close popup
                $('.js-modal-close').click();
            }
        };
        waTouchesGrid.prototype.errorCallbacksaveTouchInfo = function () {
            facilityApp.site.showError("Error occurred while completing touch, please reload and try again!");
        };
        waTouchesGrid.prototype.onChangeTouchTypeChange = function (evet, parent) {
            var optionsValue = $(evet.currentTarget).val();
            this.clearTouchTypeSelection(false);
            if (optionsValue == touchTypes.transportation) {
                this.checkTransportationCheckBoxes();
            }
            else if (optionsValue == touchTypes.warehouse) {
                this.checkWareHouseCheckBoxes();
            }
            else if (optionsValue == touchTypes.staging) {
                this.checkStagingCheckBoxes();
            }
            else if (optionsValue == touchTypes.all) {
                this.checkAllorNoneCheckBoxes(true);
            }
            else if (optionsValue == touchTypes.none) {
                this.checkAllorNoneCheckBoxes(false);
            }
            else if (optionsValue == touchTypes.needsAction) {
                this.checkNeedsActionCheckBoxes();
            }
        };
        waTouchesGrid.prototype.clearTouchTypeSelection = function (isChecked) {
            $('#IsDETouches').prop('checked', isChecked);
            $('#IsRFTouches').prop('checked', isChecked);
            $('#IsCCToches').prop('checked', isChecked);
            $('#IsDFTouches').prop('checked', isChecked);
            $('#IsRETouches').prop('checked', isChecked);
            $('#IsWATouches').prop('checked', isChecked);
            $('#IsLDMOutTouches').prop('checked', isChecked);
            $('#IsLDMInTouches').prop('checked', isChecked);
            $('#IsIBOTouches').prop('checked', isChecked);
            $('#IsOBOTouches').prop('checked', isChecked);
        };
        waTouchesGrid.prototype.checkTransportationCheckBoxes = function () {
            $('#IsDETouches').prop('checked', true);
            $('#IsRFTouches').prop('checked', true);
            $('#IsCCToches').prop('checked', true);
            $('#IsDFTouches').prop('checked', true);
            $('#IsRETouches').prop('checked', true);
        };
        ;
        waTouchesGrid.prototype.checkWareHouseCheckBoxes = function () {
            $('#IsWATouches').prop('checked', true);
            $('#IsLDMOutTouches').prop('checked', true);
            $('#IsLDMInTouches').prop('checked', true);
            $('#IsIBOTouches').prop('checked', true);
            $('#IsOBOTouches').prop('checked', true);
        };
        ;
        waTouchesGrid.prototype.checkStagingCheckBoxes = function () {
            $('#IsDETouches').prop('checked', true);
            $('#IsDFTouches').prop('checked', true);
            $('#IsWATouches').prop('checked', true);
            $('#IsLDMOutTouches').prop('checked', true);
            $('#IsIBOTouches').prop('checked', true);
            $('#IsOBOTouches').prop('checked', true);
        };
        ;
        waTouchesGrid.prototype.checkNeedsActionCheckBoxes = function () {
            $('#IsDETouches').prop('checked', true);
            $('#IsDFTouches').prop('checked', true);
            $('#IsWATouches').prop('checked', true);
            $('#IsLDMOutTouches').prop('checked', true);
            $('#IsLDMInTouches').prop('checked', true);
            $('#IsIBOTouches').prop('checked', true);
            $('#IsOBOTouches').prop('checked', true);
        };
        ;
        waTouchesGrid.prototype.checkAllorNoneCheckBoxes = function (isChecked) {
            this.clearTouchTypeSelection(isChecked);
        };
        ;
        waTouchesGrid.prototype.validateAndSubmit = function () {
            var filterReq = {
                StartDate: $('#StartDate').val(),
                EndDate: $('#EndDate').val(),
                IsShowPriorNotCompletedTouches: $('#IsShowPriorNotCompletedTouches').is(':checked')
            };
            var existingDataKey = this.$searchdata.key;
            var newSearchKey = this.getSearchKey();
            ///Before we fire a query to server, look for data in inmemory for this given date range. if it exists then filter on the local data. This saves SL calls
            if (existingDataKey == newSearchKey) {
                this.filterDataSetAndLoadGrid();
            }
            else {
                this.sendRequestToGetSLTouchesXHR(filterReq);
            }
        };
        waTouchesGrid.prototype.filterDataSetAndLoadGrid = function () {
            //alert("test search for touch");
            ///Before load new data, we have to unbind and bind new controlles
            this.unloadAndBindGrid();
            var startDate = new Date($('#StartDate').val());
            var endDate = new Date($('#EndDate').val());
            var firstName = $.trim($('#FirstName').val());
            var lastName = $.trim($('#LastName').val());
            var status = $('#Status').val();
            var statusText = $("#Status option:selected").text();
            var touchIdentificationNumber = $.trim($('#TouchIdentificationNumber').val());
            //Touch Status
            var isShowCompletedTouches = $('#IsShowCompletedTouches').is(':checked');
            var isShowPriorNotCompletedTouches = $('#IsShowPriorNotCompletedTouches').is(':checked');
            //Touch Types
            var isDETouches = $('#IsDETouches').is(':checked');
            var isRFTouches = $('#IsRFTouches').is(':checked');
            var isCCTouches = $('#IsCCToches').is(':checked');
            var isDFTouches = $('#IsDFTouches').is(':checked');
            var isRETouches = $('#IsRETouches').is(':checked');
            var isWATouches = $('#IsWATouches').is(':checked');
            var isLDMOutTouches = $('#IsLDMOutTouches').is(':checked');
            var isLDMInTouches = $('#IsLDMInTouches').is(':checked');
            var isIBOTouches = $('#IsIBOTouches').is(':checked');
            var isOBOTouches = $('#IsOBOTouches').is(':checked');
            var todayDateTime = new Date();
            var todayDate = new Date(todayDateTime.toDateMMDDYYYY());
            var filterDataFromLocalDS = $.grep(this.$searchdata.data.rows, function (o, i) {
                return ((isShowPriorNotCompletedTouches == true &&
                    (o.DispTouchStatus != "Completed" &&
                        ((new Date(o.DispScheduledDate)) < todayDate) &&
                        (o.TouchType == TouchTypeDesc.WarehouseTouches.WA ||
                            o.TouchType == TouchTypeDesc.WarehouseTouches.TI ||
                            o.TouchType == TouchTypeDesc.WarehouseTouches.TO ||
                            o.TouchType == TouchTypeDesc.Stage.IBO ||
                            o.TouchType == TouchTypeDesc.Stage.OBO))) ||
                    ((firstName.length == 0 || o.FirstName.toLowerCase().indexOf(firstName.toLowerCase()) >= 0)
                        && (lastName.length == 0 || o.LastName.toLowerCase().indexOf(lastName.toLowerCase()) >= 0)
                        && ((isDETouches == true && o.TouchType == TouchTypeDesc.TranportationTouches.DE)
                            || (isRFTouches == true && o.TouchType == TouchTypeDesc.TranportationTouches.RF)
                            || (isCCTouches == true && o.TouchType == TouchTypeDesc.TranportationTouches.CC)
                            || (isDFTouches == true && o.TouchType == TouchTypeDesc.TranportationTouches.DF)
                            || (isRETouches == true && o.TouchType == TouchTypeDesc.TranportationTouches.RE)
                            || (isWATouches == true && o.TouchType == TouchTypeDesc.WarehouseTouches.WA)
                            || (isLDMOutTouches == true && o.TouchType == TouchTypeDesc.WarehouseTouches.TO)
                            || (isLDMInTouches == true && o.TouchType == TouchTypeDesc.WarehouseTouches.TI)
                            || (isIBOTouches == true && o.TouchType == TouchTypeDesc.Stage.IBO)
                            || (isOBOTouches == true && o.TouchType == TouchTypeDesc.Stage.OBO))
                        && (status == -1 || o.DispTouchStatus.toLowerCase().indexOf(statusText.toLowerCase()) >= 0 || (status == 0 && (o.DispTouchStatus != "OnHold" || (o.DispTouchStatus == "OnHold" && o.AlertLevel == 2)) && o.DispTouchStatus != "Completed"))
                        && (startDate <= (new Date(o.DispScheduledDate)) && (new Date(o.DispScheduledDate)) <= endDate)
                        && (touchIdentificationNumber.length == 0 || (touchIdentificationNumber.length >= 3 && ((o.StarsId + '').indexOf(touchIdentificationNumber) >= 0 || (o.QORId + '').indexOf(touchIdentificationNumber) >= 0 || o.UnitName.toLowerCase().indexOf(touchIdentificationNumber.toLowerCase()) >= 0)))));
            });
            var response = { page: this.$searchdata.data.page, total: this.$searchdata.data.total, records: this.$searchdata.data.records, rows: filterDataFromLocalDS };
            this.bindGridControl(response);
        };
        waTouchesGrid.prototype.getSearchKey = function () {
            return new Date($('#StartDate').val()).toDateMMDDYYYY() + '-' + new Date($('#EndDate').val()).toDateMMDDYYYY();
        };
        waTouchesGrid.prototype.init = function () {
            this.validateAndSubmit();
            this.initPopup();
        };
        waTouchesGrid.prototype.initPopup = function () {
            $(".js-modal-close, .modal-overlay").click(function (e) {
                e.preventDefault();
                $(".modal-box, .modal-overlay").fadeOut(500, function () {
                    $(".modal-overlay").remove();
                });
            });
            $(window).resize(function () {
                $(".modal-box").css({
                    top: 5,
                    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                });
            });
            $(window).resize();
        };
        waTouchesGrid.prototype.sendRequestToGetSLTouchesXHR = function (postData) {
            var ajaxSettings = {
                url: waTouchesGridModule.URLHelper.searchTouchList,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackSendRequestToGetSLTouchesXHR.bind(this))
                .fail(this.errorCallbackSendRequestToGetSLTouchesXHR);
        };
        waTouchesGrid.prototype.successCallbackSendRequestToGetSLTouchesXHR = function (result) {
            console.log(result);
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                this.$searchdata.data = result.data;
                this.$searchdata.key = this.getSearchKey();
                ///Before load new data, we have to unbind and bind new controlles
                this.unloadAndBindGrid();
                ///once we get data, then store it in local dataset and then call below method to apply local filters. and then populate grid.
                this.filterDataSetAndLoadGrid();
                //console.log("successCallbackSendRequestToGetSLTouchesXHR-33");
            }
            $.blockUI;
        };
        waTouchesGrid.prototype.errorCallbackSendRequestToGetSLTouchesXHR = function (result) {
            console.log(result);
            facilityApp.site.showError('Error occurred while fetching Salesforce touches. Please reload the page and try again!');
        };
        waTouchesGrid.prototype.unloadAndBindGrid = function () {
            $.jgrid.gridUnload(this.gridId);
            //Initiate grid controlles and load the data
            this.dsGrid = $("#" + this.gridId);
            this.dsGridList = $("#" + this.gridListId);
        };
        waTouchesGrid.prototype.getTouchInfoByKey = function (key) {
            var filterTouchInfo = $.grep(this.$searchdata.data.rows, function (o, i) {
                return o.TouchKey == key;
            });
            if (filterTouchInfo && filterTouchInfo.length > 0) {
                return filterTouchInfo[0];
            }
            return undefined;
        };
        waTouchesGrid.prototype.editTouchDetails = function (key) {
            this.bindGridControl(this);
            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popupEditTouchDetails').fadeIn();
            var touchInfo = this.getTouchInfoByKey(key);
            if (touchInfo) {
                var dvEditTouchDetails = $('#dvEditTouchDetails');
                $('#unitNumberTextField', dvEditTouchDetails).addClass('hide');
                $('#UnitNumber', dvEditTouchDetails).val(touchInfo.UnitName);
                $('#hdnUnitNumberValidationReq', dvEditTouchDetails).val("false");
                var custFullName = touchInfo.FirstName + ' ' + touchInfo.LastName;
                $('.touch-edit-popuphead', $('#popupEditTouchDetails')).html(custFullName);
                $('#CustomerName', dvEditTouchDetails).html(custFullName);
                $('#OrderNumber', dvEditTouchDetails).html(touchInfo.QORId.toString());
                var tt = touchInfo.TouchType;
                if (tt == TouchTypeDesc.WarehouseTouches.TI)
                    tt = "LDM In";
                else if (tt == TouchTypeDesc.WarehouseTouches.TO)
                    tt = "LDM Out";
                $('#SpanTouchType', dvEditTouchDetails).html(tt);
                $('#SpanTouchPreviousAction', dvEditTouchDetails).html(touchInfo.ActionNameWithCategory);
                $('#lblStatusName', dvEditTouchDetails).html(touchInfo.DispTouchStatus);
                $('#hfTouchKey', dvEditTouchDetails).val(key);
                //getting touch history
                var postdataforhistory = {
                    QORID: touchInfo.QORId,
                    TouchType: touchInfo.TouchType,
                    SeqNumber: touchInfo.SequenceNum
                };
                this.getTouchHistoryXHR(postdataforhistory);
                if (touchInfo.SLTouchStatus != 'Completed') {
                    //If there is any conflict then show the messsage in popup 
                    if (touchInfo.IsConflictWithScheduleDate == true) {
                        var editmsg = "<p>This touch has been rescheduled to " + touchInfo.DispSLScheduledDate + ". Please click on submit to sync Salesforce information with FacilityApp.</p>";
                        $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).removeClass("infomsg").addClass("errormsg").html(editmsg).show();
                    }
                    else if (touchInfo.IsTouchMissed == true) {
                        var editmsg = "<p>This touch has been unscheduled. Please return to stack to sync Salesforce information with FacilityApp.</p>";
                        $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).removeClass("infomsg").addClass("errormsg").html(editmsg).show();
                    }
                    else {
                        $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).hide();
                    }
                    var postdataforedit = {
                        touchType: touchInfo.TouchType,
                        statusID: touchInfo.TouchStatusID,
                        touchScheduleDate: touchInfo.DispScheduledDate
                    };
                    //need to change this object to action object.
                    var actionObj = {
                        actionName: touchInfo.ActionName,
                        actionID: touchInfo.ActionID,
                        key: key
                    };
                    this.getEditTouchInfoXHR(postdataforedit, actionObj);
                }
                else {
                    var editmsg = "<p>This touch has been completed in Salesforce. You cannot change touch status.</p>";
                    $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).addClass("infomsg").removeClass("errormsg").html(editmsg).show();
                    var ddEditAction = $('#ddEditAction', $('#dvEditTouchDetails'));
                    ddEditAction.empty();
                    var opt = '<option value="0" data-actioncategoryname="-1">No actions can be made on completed touch</option>';
                    ddEditAction.append(opt);
                    ddEditAction.prop("disabled", true);
                }
                $('#Notes', $('#dvEditTouchDetails')).val(touchInfo.Comments);
                $('#edittouchInfo').tabs({
                    active: "0"
                });
            }
            else {
                facilityApp.site.showError('Error occurred while fetching touch info. Please reload the page and try again!');
            }
        };
        waTouchesGrid.prototype.getTouchTicket = function (qorId, touchType, seqNumber) {
            alert('In progress...');
        };
        ////SFERP-TODO-CTRMV-- Remove methods --TG - 579
        waTouchesGrid.prototype.getBOLDocs = function (qorId) {
            var bolReq = {
                qorid: qorId
            };
            this.getBOLDocsXHR(bolReq);
        };
        waTouchesGrid.prototype.getBOLDocsXHR = function (postData) {
            var ajaxSettings = {
                url: waTouchesGridModule.URLHelper.getBOLURL,
                type: 'GET',
                data: postData,
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackGetBOLDocsXHR.bind(this))
                .fail(this.errorCallbackGetBOLDocsXHR);
        };
        waTouchesGrid.prototype.successCallbackGetBOLDocsXHR = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                var width = window.innerWidth * .75;
                ;
                var height = window.innerHeight * .75;
                var left = (screen.width - width) / 2;
                var top_1 = (screen.height - height) / 2;
                var params = 'width=' + width + ', height=' + height;
                params += ', top=' + top_1 + ', left=' + left;
                params += ', directories=no';
                params += ', location=no';
                params += ', menubar=no';
                params += ', resizable=yes';
                params += ', scrollbars=yes';
                params += ', status=no';
                params += ', toolbar=no';
                window.open(result.data, '_blank', params);
            }
            $.blockUI;
        };
        waTouchesGrid.prototype.errorCallbackGetBOLDocsXHR = function (result) {
            facilityApp.site.showError('Error occurred while fetching BOL document link. Please reload the page and try again!');
        };
        waTouchesGrid.prototype.getEditTouchInfoXHR = function (postData, actionObj) {
            var ajaxSettings = {
                url: waTouchesGridModule.URLHelper.getEditTouchInfoURL,
                type: 'GET',
                data: postData,
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCBEditTouchInfoXHR.bind(this, actionObj))
                .fail(this.errorCBEditTouchInfoXHR);
        };
        waTouchesGrid.prototype.successCBEditTouchInfoXHR = function (actionObj, result) {
            var _this = this;
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                var ddEditAction_1 = $('#ddEditAction', $('#dvEditTouchDetails'));
                //Before refresh dropdown make it empty and re-populate stats from result.data
                ddEditAction_1.empty();
                if (result.data.length > 0) {
                    ddEditAction_1.prop("disabled", false);
                    var opt = '<option value="0" data-actioncategoryname="-1">--Please Select Action--</option>';
                    ddEditAction_1.append(opt);
                    var uniqueCategoryActions = result.data.reduce(function (result, current) {
                        result[current.ActionCategoryName] = result[current.ActionCategoryName] || [];
                        result[current.ActionCategoryName].push(current);
                        return result;
                    }, {});
                    $.each(uniqueCategoryActions, function (key, value) {
                        var group;
                        if ($(value).length > 1) {
                            group = $('<optgroup label="' + key + '" />');
                            $.each(value, function () {
                                $('<option value="' + this.ActionID + '" data-actioncategoryname="' + this.ActionCategoryName + '">' + this.ActionName + '</option>').appendTo(group);
                            });
                        }
                        else {
                            $.each(value, function () {
                                group = $('<option value="' + this.ActionID + '" data-actioncategoryname="' + this.ActionCategoryName + '">' + this.ActionName + '</option>');
                            });
                        }
                        group.appendTo(ddEditAction_1);
                    });
                    ddEditAction_1.unbind("change").change(function (e) { e.preventDefault(); _this.showHideUnitNameField(e); });
                }
                else {
                    var ByPassActionChangeToSyncData = false;
                    var touchInfo = this.getTouchInfoByKey(actionObj.key);
                    //If the error message is visible but there are no actions are available for this touch, then change message
                    if (touchInfo.IsConflictWithScheduleDate == true) {
                        var editmsg = "<p>This touch has been rescheduled to " + touchInfo.DispSLScheduledDate + ". Please click on Submit to sync Salesforce information with FacilityApp.</p>";
                        $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).removeClass("infomsg").addClass("errormsg").html(editmsg).show();
                        ByPassActionChangeToSyncData = true;
                    }
                    else if (touchInfo.IsTouchMissed == true) {
                        var editmsg = "<p>This touch has been unscheduled. Please click on Submit to sync Salesforce information with FacilityApp.</p>";
                        $('#smtTouchStatusCompleted', $('#dvEditTouchDetails')).removeClass("infomsg").addClass("errormsg").html(editmsg).show();
                        ByPassActionChangeToSyncData = true;
                    }
                    var opt = '<option value="0" data-actioncategoryname="-1" data-bypassactionchangetosyncdata=' + ByPassActionChangeToSyncData + '>There are no further actions available for this touch</option>';
                    ddEditAction_1.append(opt);
                    ddEditAction_1.prop("disabled", true);
                }
            }
            $.blockUI;
        };
        waTouchesGrid.prototype.errorCBEditTouchInfoXHR = function (result) {
            facilityApp.site.showError('Error occurred fetching touch deatils. Please reload the page and try again!');
        };
        waTouchesGrid.prototype.showHideUnitNameField = function (evet) {
            var optionCatValue = $('option:selected', $(evet.currentTarget)).data('actioncategoryname');
            var vEditTouchDetails = $('#dvEditTouchDetails');
            var key = $('#hfTouchKey', vEditTouchDetails).val();
            var touchInfo = this.getTouchInfoByKey(key);
            if (touchInfo.TouchType == TouchTypeDesc.Stage.IBO && optionCatValue == 'Completed') {
                $('#unitNumberTextField', vEditTouchDetails).removeClass('hide');
                $('#UnitNumber', vEditTouchDetails).val(touchInfo.UnitName);
                $('#hdnUnitNumberValidationReq', vEditTouchDetails).val("true");
            }
            else {
                $('#unitNumberTextField', vEditTouchDetails).addClass('hide');
                $('#hdnUnitNumberValidationReq', vEditTouchDetails).val("false");
            }
        };
        waTouchesGrid.prototype.getTouchHistoryXHR = function (postData) {
            var ajaxSettings = {
                url: waTouchesGridModule.URLHelper.getTouchHistory,
                type: 'GET',
                data: postData,
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successTouchHistoryXHR.bind(this))
                .fail(this.errorTouchHistoryXHR);
        };
        waTouchesGrid.prototype.successTouchHistoryXHR = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                $('#tBodyTouchHistory').html(result.data.touchHistory);
            }
            $.blockUI;
        };
        waTouchesGrid.prototype.errorTouchHistoryXHR = function (result) {
            facilityApp.site.showError('Error occurred fetching touch deatils. Please reload the page and try again!');
        };
        waTouchesGrid.prototype.formatTouchStatus = function (cellvalue, options, rowObject) {
            ///Since we are using type script, we cannot bind direct click event to edit record here. 
            ////To handle click event, we are adding class "orderEditAction" here and binding click event in gridloadcomplete event. --If you find any better approach please replace this logic
            return "<a href='#'  class='orderEditAction'" + "' data-key='" + rowObject.TouchKey + "')\" title='" + cellvalue + "'>" + cellvalue + "</a>";
        };
        ////SFERP-TODO-CTUPD  -- Need to link Salesforce webpage in future - TBD.
        waTouchesGrid.prototype.formatOrderNumber = function (cellvalue, options, rowObject) {
            //Commented by Sohan for furture reference
            //let url = waTouchesGridModule.URLHelper.editQorURL + "?qorid=" + rowObject.QORId;
            //return "<a onclick='$.blockUI();' title='" + rowObject.QORId + "' href='" + url + "')\">" + rowObject.QORId + "</a>";
            return "<a class='showDisableMsg' title='" + rowObject.QORId + "' href='#')\">" + rowObject.QORId + "</a>";
        };
        waTouchesGrid.prototype.formatUnitName = function (cellvalue, options, rowObject) {
            return cellvalue + (rowObject.IsZippyShellQuote ? " (ZM)" : "");
        };
        waTouchesGrid.prototype.formatComments = function (cellvalue, options, rowObject) {
            if (cellvalue && cellvalue.length > 0)
                return $.jgrid.htmlEncode(cellvalue);
            else
                return "";
        };
        ////SFERP-TODO-CTUPD -- may not needed this action after getting data from Salesforce - TBD.
        waTouchesGrid.prototype.formatScheduleDate = function (cellvalue, options, rowObject) {
            var a = '';
            if (rowObject.IsConflictWithScheduleDate == true || rowObject.IsTouchMissed == true) {
                var title = void 0;
                if (rowObject.IsTouchMissed == true) {
                    title = "This has been unscheduled.";
                }
                else {
                    title = "This has been rescheduled to " + rowObject.DispSLScheduledDate;
                }
                a = "&nbsp;<img alt='CONFLICT' src='../Images/MediaLoot/icn_alert_warning.png' class='orderEditAction watouches-alert-pointer' title='" + title + "' data-key='" + rowObject.TouchKey + "')\" />";
            }
            return "<span title=" + cellvalue + ">" + cellvalue + "</span>" + a;
        };
        waTouchesGrid.prototype.formatTouchType = function (cellvalue, options, rowObject) {
            if (cellvalue && cellvalue.length > 0 && cellvalue == TouchTypeDesc.WarehouseTouches.TI)
                return "LDM In";
            else if (cellvalue && cellvalue.length > 0 && cellvalue == TouchTypeDesc.WarehouseTouches.TO)
                return "LDM Out";
            else
                return cellvalue;
        };
        waTouchesGrid.prototype.formatStarsID = function (cellvalue, options, rowObject) {
            return cellvalue > 0 ? cellvalue : "";
        };
        ////SFERP-TODO-CTUPD -- Need to uncomment below line - TBD.
        waTouchesGrid.prototype.formatBOL = function (cellvalue, options, rowObject) {
            var lnk = "";
            //Commented by Sohan for furture reference        
            //if (rowObject.TouchType == TouchTypeDesc.WarehouseTouches.TI || rowObject.TouchType == TouchTypeDesc.WarehouseTouches.TO)
            //    lnk = "<a title='Click here to see BOL' class='orderBOLAction' data-qorId='" + rowObject.QORId + "' data-touchType='" + rowObject.TouchType + "' data-seqNumber ='" + rowObject.SequenceNum + "' href='#')\">BOL</a>"
            //else { 
            //    let ttlnk = waTouchesGridModule.URLHelper.touchTicketURL + "?touchType=" + rowObject.TouchType + "&qorid=" + rowObject.QORId;
            //    lnk += "<a title = 'Click here to generate touch ticket' target='_blank' href='" + ttlnk + "')>TT</a>"
            //}
            if (rowObject.TouchType == TouchTypeDesc.WarehouseTouches.TI || rowObject.TouchType == TouchTypeDesc.WarehouseTouches.TO)
                lnk = "<a title='Click here to see BOL' class='showDisableMsg' data-qorId='" + rowObject.QORId + "' data-touchType='" + rowObject.TouchType + "' data-seqNumber ='" + rowObject.SequenceNum + "' href='#')\">BOL</a>";
            else {
                var ttlnk = waTouchesGridModule.URLHelper.touchTicketURL + "?touchType=" + rowObject.TouchType + "&qorid=" + rowObject.QORId;
                lnk += "<a title = 'Click here to generate touch ticket' class='showDisableMsg' target='_blank' href='#')>TT</a>";
            }
            return lnk;
        };
        waTouchesGrid.prototype.bindGridControl = function (data) {
            var columnNames = ['AlertLevel', 'TouchKey', 'FirstName', 'LastName', 'Order Number', 'Stars ID',
                'StarsUnitID', 'Unit Number', 'Phone', 'Schedule Date', 'DispSLScheduledDate', 'Touch Time',
                'Provided ETA', 'SLTouchStatus', 'LocalTouchStatus', 'Status', 'ActionId', 'ActionName', 'Action', 'TouchStatusID', 'Touch Type',
                'TouchTypeFull', 'Docs', 'Comments', 'SequenceNum', 'IsConflictWithScheduleDate', 'IsTouchMissed', 'TouchActionCategory', 'DispTouchActionCategory', '', 'IsZippyShellQuote'];
            var columnModel = [
                {
                    name: 'AlertLevel', index: 'AlertLevel', editable: false, hidedlg: true, hidden: true, search: false
                },
                {
                    name: 'TouchKey', index: 'TouchKey', editable: false, hidedlg: true, hidden: true, search: false
                },
                {
                    name: 'FirstName', index: 'FirstName', editable: false, hidden: false, classes: 'printHide'
                },
                {
                    name: 'LastName', index: 'LastName', editable: false, hidden: false
                },
                {
                    name: 'QORId', index: 'QORId', editable: false, hidden: false
                    ////SFERP-TODO-CTUPD -- Need to uncomment below line - TBD.
                    //name: 'QORId', index: 'QORId', editable: false, hidden: false, align: 'center', search: false, formatter: this.formatOrderNumber.bind(this), classes: 'printHide', title: false
                },
                {
                    name: 'StarsId', index: 'StarsId', editable: false, hidden: false, align: 'center', search: false, formatter: this.formatStarsID.bind(this), classes: 'printHide'
                },
                {
                    name: 'StarsUnitId', index: 'StarsUnitId', editable: false, hidden: true
                },
                {
                    name: 'UnitName', index: 'UnitName', editable: false, hidden: false, align: 'center', search: false, formatter: this.formatUnitName.bind(this)
                },
                {
                    name: 'Phonenumber', index: 'Phonenumber', editable: false, hidden: false, align: 'center', search: false, classes: 'printHide'
                },
                {
                    name: 'DispScheduledDate', index: 'DispScheduledDate', align: 'center', editable: false, hidden: false, formatter: this.formatScheduleDate.bind(this), title: false
                },
                {
                    name: 'DispSLScheduledDate', index: 'DispSLScheduledDate', hidden: true
                },
                {
                    name: 'TouchTime', index: 'TouchTime', editable: false, hidden: false, align: 'center'
                },
                {
                    name: 'ETAActualTime', index: 'ETAActualTime', editable: false, hidden: false, align: 'center', search: false
                },
                {
                    name: 'SLTouchStatus', index: 'SLTouchStatus', editable: false, hidden: true
                },
                {
                    name: 'LocalTouchStatus', index: 'LocalTouchStatus', editable: false, hidden: true
                },
                {
                    name: 'DispTouchStatus', index: 'DispTouchStatus', editable: false, hidden: false, align: 'center', search: false
                },
                {
                    name: 'ActionID', index: 'ActionID', editable: false, hidden: true
                },
                {
                    name: 'ActionName', index: 'ActionName', editable: false, hidden: true
                },
                {
                    name: 'ActionNameWithCategory', index: 'ActionNameWithCategory', editable: false, hidden: false, align: 'center', search: false, formatter: this.formatTouchStatus.bind(this), title: false
                },
                {
                    name: 'TouchStatusID', index: 'TouchStatusID', editable: false, hidden: true
                },
                {
                    name: 'TouchType', index: 'TouchType', editable: false, hidden: false, align: 'center', search: false, formatter: this.formatTouchType.bind(this)
                },
                {
                    name: 'TouchTypeFull', index: 'TouchTypeFull', editable: false, hidden: true
                },
                {
                    name: 'BOL', index: 'BOL', editable: false, sortable: false, hidden: false, align: 'center', search: false, title: false, formatter: this.formatBOL.bind(this), classes: 'printHide' //, width: 200
                },
                {
                    name: 'Comments', index: 'Comments', editable: false, hidden: false, align: 'center', classes: "textInDiv", search: false, formatter: this.formatComments.bind(this)
                },
                {
                    name: 'SequenceNum', index: 'SequenceNum', editable: false, hidden: true
                },
                {
                    name: 'IsConflictWithScheduleDate', index: 'IsConflictWithScheduleDate', editable: false, hidden: true
                },
                {
                    name: 'IsTouchMissed', index: 'IsTouchMissed', editable: false, hidden: true
                },
                {
                    name: 'TouchActionCategory', index: 'TouchActionCategory', editable: false, hidden: true
                },
                {
                    name: 'DispTouchActionCategory', index: 'DispTouchActionCategory', editable: false, hidden: true
                },
                {
                    name: 'BlankField', index: 'BlankField', editable: false, hidden: true, classes: 'printShow'
                },
                {
                    name: 'IsZippyShellQuote', index: 'IsZippyShellQuote', editable: false, hidden: true
                }
            ];
            var jqGridOptions = {
                datatype: "local",
                data: data.rows,
                pager: 'listPager',
                caption: "<strong>" + this.gridTitle + "&nbsp;-&nbsp;<span id='totalRecordsCount'>0</span></strong>",
                colNames: columnNames,
                colModel: columnModel,
                rowNum: 100,
                rowList: [10, 50, 100, 500]
                //, sortname: 'TouchStatus'
                //, sortorder: "desc"
                ,
                id: "RowID",
                height: "100%"
                //, width: "100%"
                ,
                viewrecords: true,
                autoWidth: true,
                gridview: true
                //, multiselect: true
                //, multiboxonly: true
                ,
                width: null,
                shrinkToFit: true
                //toolbar : [true, "top"]
                ,
                emptyrecords: 'no records found',
                gridComplete: this.gridCompleteLocal.bind(this),
                loadComplete: this.gridLoadComplete.bind(this),
                loadError: this.gridLoadError.bind(this)
                //, ondblClickRow: this.gridDblClickRow.bind(this)
            };
            this.dsGrid.jqGrid(jqGridOptions).navGrid(this.dsGridList, {
                edit: false, add: false, del: false, search: false, refresh: false
            }, null, null, null);
            this.dsGrid.jqGrid('sortGrid', 'AlertLevel', true, 'desc');
            this.dsGrid.jqGrid("setLabel", "StarsId", false, "printHide");
            this.dsGrid.jqGrid("setLabel", "Phonenumber", false, "printHide");
            this.dsGrid.jqGrid("setLabel", "QORId", false, "printHide");
            this.dsGrid.jqGrid("setLabel", "FirstName", false, "printHide");
            this.dsGrid.jqGrid("setLabel", "BOL", false, "printHide");
            this.dsGrid.jqGrid("setLabel", "BlankField", false, "printShow");
        };
        waTouchesGrid.prototype.gridCompleteLocal = function () {
            $('#totalRecordsCount').html(this.dsGrid.jqGrid('getGridParam', 'records'));
            var isShowPriorNotCompletedTouches = $('#IsShowPriorNotCompletedTouches').is(':checked');
            var ids = this.dsGrid.getDataIDs();
            if (isShowPriorNotCompletedTouches == true) {
                for (var i = 0; i < ids.length; i++) {
                    var cell = this.dsGrid.getCell(ids[i], "Status");
                    var alertType = this.dsGrid.getCell(ids[i], "AlertLevel");
                    if (cell != WATouchStatus.Completed) {
                        if (alertType == 1)
                            this.dsGrid.jqGrid('setRowData', ids[i], false, { background: AlertColorCode.type1 });
                        if (alertType == 2)
                            this.dsGrid.jqGrid('setRowData', ids[i], false, { background: AlertColorCode.type2 });
                    }
                }
            }
        };
        waTouchesGrid.prototype.gridLoadComplete = function () {
            var _this = this;
            if (this.dsGrid.getGridParam('records') === 0) {
            }
            ////SFERP-TODO-CTUPD
            //Below is dummy message created by Sohan for temprary purpose.
            $('.showDisableMsg').unbind('click').click(function (evt) {
                evt.preventDefault();
                alert('TBD - Update Link to open the Salesforce page/document');
                return false;
            });
            ////SFERP-TODO-CTUPD
            $('.orderEditAction').unbind('click').click(function (evt) {
                evt.preventDefault();
                var currentElement = $(evt.currentTarget);
                var key = currentElement.data('key');
                _this.editTouchDetails(key);
            });
            ////SFERP-TODO-CTUPD -- Update link from SalesForce - TBD
            $('.orderBOLAction').unbind('click').click(function (evt) {
                evt.preventDefault();
                var currentElement = $(evt.currentTarget);
                var qorId = currentElement.data('qorid');
                _this.getBOLDocs(qorId);
            });
            ////SFERP-TODO-CTUPD
            $('.orderTouchTicketAction').unbind('click').click(function (evt) {
                alert("TBD - Disabled currently, Need to get data/pdf from Salesforce");
                return;
                ////SFERP-TODO-CTUPD -- Need to uncomment below line - TBD.
                //evt.preventDefault();
                //let currentElement = $(evt.currentTarget);
                //let qorId = currentElement.data('qorid');
                //let touchType = currentElement.data('touchtype');
                //let seqNumber = currentElement.data('seqnumber');
                //this.getTouchTicket(qorId, touchType, seqNumber);
            });
        };
        waTouchesGrid.prototype.gridLoadError = function (xhr, st, err) {
            if (xhr.status == "200")
                return false;
            console.log("Load Error" + xhr.status + "-" + xhr.responseText); //Testing purpose only
            var error = eval('(' + xhr.responseText + ')');
            $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                width: '600',
                buttons: {
                    Ok: function () {
                        //$(this).dialog("close");
                    }
                }
            });
        };
        return waTouchesGrid;
    }());
    waTouchesGridModule.waTouchesGrid = waTouchesGrid;
})(waTouchesGridModule || (waTouchesGridModule = {}));
$(document).ready(function () {
    var owaTouchesGrid = new waTouchesGridModule.waTouchesGrid();
    owaTouchesGrid.init();
});
//# sourceMappingURL=waTouchGrid.js.map