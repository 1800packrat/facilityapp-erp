/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="Libraries\jquery.blockUI.d.ts" />;
var facilityApp;
(function (facilityApp) {
    var site = /** @class */ (function () {
        function site() {
            this.initSiteEvents();
        }
        site.prototype.initSiteEvents = function () {
            window.onpageshow = function (event) {
                if (event.persisted) {
                    window.location.reload();
                }
            };
            $(document).ajaxStart($.blockUI)
                .ajaxStop($.unblockUI);
            $(".ui-draggable").draggable({
                handle: ".ui-draggable-handle"
            });
        };
        site.bindSpaceTripEvent = function () {
            $('.noEmptySpaceAllowed').keyup(function () {
                $(this).val($(this).val().replace(/ +?/g, ''));
            });
        };
        site.bindAcceptOnlyDecimalEvent = function () {
            $('.acceptOnlyDecimal').keyup(function () {
                var position = this.selectionStart - 1;
                //remove all but number and .
                var fixed = this.value.replace(/[^0-9\.]/g, '');
                if (fixed.charAt(0) === '.') {
                    //fixed = fixed.slice(1);
                    fixed = "0.";
                }
                var pos = fixed.indexOf(".") + 1;
                if (pos >= 0)
                    fixed = fixed.substr(0, pos) + fixed.slice(pos).replace('.', '');
                if (this.value !== fixed) {
                    this.value = fixed;
                    this.selectionStart = position;
                    this.selectionEnd = position;
                }
            });
        };
        site.showError = function (outputMsg) {
            this.alert(outputMsg, 'Error', undefined);
        };
        site.showAlert = function (outputMsg) {
            this.alert(outputMsg, 'Alert', undefined);
        };
        site.showAlertRoleMismatch = function (outputMsg, title) {
            this.alert(outputMsg, title, undefined);
        };
        site.showConfrimWithOkCallback = function (outputMsg, okCallbackMethod) {
            this.confrim(outputMsg, 'Confirm', okCallbackMethod, undefined);
        };
        site.showConfrim = function (outputMsg, okCallbackMethod, cancelBackMethod) {
            this.confrim(outputMsg, 'Confirm', okCallbackMethod, cancelBackMethod);
        };
        site.alert = function (outputMsg, titleMsg, onCloseCallback) {
            if (!titleMsg)
                titleMsg = 'Alert';
            if (!outputMsg)
                outputMsg = 'Unexpected error occurred.';
            $("<div class='site-alert-popup'></div>").html(outputMsg).dialog({
                title: titleMsg,
                //resizable: false,
                modal: true,
                minWidth: 800,
                position: {
                    my: "center bottom",
                    at: "center top"
                },
                minHeight: 220,
                zIndex: 999999,
                stack: false,
                buttons: {
                    "OK": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                    if (onCloseCallback)
                        onCloseCallback();
                    /* Cleanup node(s) from DOM */
                    $(this).dialog('destroy').remove();
                },
                open: function (event, ui) {
                    $('.ui-widget-overlay').bind('click', function () { $(".site-alert-popup").dialog('close'); });
                    $(this).closest(".ui-dialog")
                        .find(".ui-dialog-titlebar-close")
                        .removeClass("ui-dialog-titlebar-close")
                        .addClass("ui-dialog-titlebar-closeCustom");
                }
            });
        };
        site.confrim = function (outputMsg, titleMsg, onOkCallback, onCancelCallbak) {
            if (!titleMsg)
                titleMsg = 'Confrim';
            if (!outputMsg)
                outputMsg = 'Unexpected error occurred.';
            $("<div class='site-alert-popup'></div>").html(outputMsg).dialog({
                title: titleMsg,
                //resizable: false,
                position: {
                    my: "center bottom",
                    at: "center top"
                },
                modal: true,
                minWidth: 800,
                //position: 'top',
                minHeight: 220,
                zIndex: 999999,
                stack: false,
                buttons: {
                    "Continue": function () {
                        if (onOkCallback)
                            onOkCallback();
                        $(this).dialog("close");
                    },
                    "Cancel": function () {
                        if (onCancelCallbak)
                            onCancelCallbak();
                        $(this).dialog("close");
                    }
                },
                close: function () {
                    /* Cleanup node(s) from DOM */
                    $(this).dialog('destroy').remove();
                },
                open: function (event, ui) {
                    $(this).closest(".ui-dialog")
                        .find(".ui-dialog-titlebar-close")
                        .removeClass("ui-dialog-titlebar-close")
                        .addClass("ui-dialog-titlebar-closeCustom");
                }
            });
        };
        return site;
    }());
    facilityApp.site = site;
})(facilityApp || (facilityApp = {}));
function require(path) { }
String.isNullOrEmpty = function (val) {
    if (val === undefined || val === null || val.trim() === '') {
        return true;
    }
    return false;
};
String.hasWhiteSpace = function (val) {
    return val.indexOf(' ') >= 0;
};
Date.prototype.toDateMMDDYYYY = function () {
    var year = this.getFullYear();
    var month = (1 + this.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = this.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
    //return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
};
Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + days);
    var dd = this.getDate();
    var mm = this.getMonth() + 1;
    var y = this.getFullYear();
    return new Date(mm + '/' + dd + '/' + y);
};
$(document).ready(function () {
    new facilityApp.site();
});
//# sourceMappingURL=site.js.map