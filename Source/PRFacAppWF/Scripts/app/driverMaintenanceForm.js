/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jquery.steps.d.ts" />
/// <reference path="libraries\jquery.validation.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="driverMaintenance.ts" />
var driverMaintenanceFormModule;
(function (driverMaintenanceFormModule) {
    var URLHelper = /** @class */ (function () {
        function URLHelper() {
        }
        URLHelper.loadUserInfo = '';
        URLHelper.getDuplicateDrivers = '';
        //public static getDirverSecurity: string = '';
        //public static getDirverFacilities: string = '';
        URLHelper.saveDriverDetails = '';
        URLHelper.checkUserNameAvailability = '';
        URLHelper.SuggesstedUserName = '';
        URLHelper.addDriverFacility = '';
        URLHelper.assingDriverToFacility = '';
        return URLHelper;
    }());
    driverMaintenanceFormModule.URLHelper = URLHelper;
    var DefaultVariables = /** @class */ (function () {
        function DefaultVariables() {
        }
        DefaultVariables.PasswordMinLength = 0;
        return DefaultVariables;
    }());
    driverMaintenanceFormModule.DefaultVariables = DefaultVariables;
    var DriverFacilityStatus;
    (function (DriverFacilityStatus) {
        DriverFacilityStatus[DriverFacilityStatus["None"] = 0] = "None";
        DriverFacilityStatus[DriverFacilityStatus["New"] = 1] = "New";
        DriverFacilityStatus[DriverFacilityStatus["Updated"] = 2] = "Updated";
        DriverFacilityStatus[DriverFacilityStatus["Deleted"] = 3] = "Deleted";
    })(DriverFacilityStatus = driverMaintenanceFormModule.DriverFacilityStatus || (driverMaintenanceFormModule.DriverFacilityStatus = {}));
    var DAAccessLevel = /** @class */ (function () {
        function DAAccessLevel() {
        }
        DAAccessLevel.PrimaryFacility = 'PrimaryFacility';
        DAAccessLevel.SecondaryFacility = 'SecondaryFacility';
        DAAccessLevel.Rover = 'Rover';
        return DAAccessLevel;
    }());
    driverMaintenanceFormModule.DAAccessLevel = DAAccessLevel;
    var driverMaintenanceForm = /** @class */ (function () {
        function driverMaintenanceForm() {
            this.drSteps = null;
            this.glbDriverID = 0;
            //this.drSteps = $("#wzAddDriver");
            this.glbDriverID = 0;
        }
        driverMaintenanceForm.prototype.addEditDriver = function (driverID) {
            this.glbDriverID = driverID;
            var request = {
                Id: driverID
            };
            this.sendRequestForLoadUserInfo(request);
        };
        driverMaintenanceForm.prototype.initAddDriverWizard = function (isEdit) {
            var oJQueryStepsOptions = {
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                autoFocus: true,
                enableCancelButton: true,
                enableAllSteps: isEdit,
                labels: { cancel: "Close" },
                onStepChanging: this.onStepChanging.bind(this),
                onFinished: this.onFinishedDriverForm.bind(this),
                onStepChanged: this.onStepChanged.bind(this),
                onCanceled: this.onCanceled.bind(this),
            };
            this.drSteps.steps(oJQueryStepsOptions);
        };
        driverMaintenanceForm.prototype.onCanceled = function () {
            $(".modal-box1, .modal-overlay").fadeOut(500, function () {
                //Since we have two model-boxes in UI, make sure hide respected popup only
                $(".modal-box", $('#frmSaveDriverDetails')).hide();
                // console.log('cancelled');
                $('#wzDrUserInformation').empty();
                $('#wzDrSecurity').empty();
                $('#wzDrFacilityAssignment').empty();
                //$('#tBodyDriverFacilityList').remove('tr');
            });
        };
        driverMaintenanceForm.prototype.onStepChanging = function (event, currentIndex, newIndex) {
            var currentObject = this.drSteps.steps("getStep", currentIndex);
            //If driver form is in add mode then only look for duplicate uses 
            if (currentIndex == 0) {
                ///client is moving from first to second step
                //VAlidate form and send return 
                var isFormValid = this.validateDriverFormFirstStep();
                if (isFormValid) {
                    if (this.glbDriverID == 0) {
                        var request = {
                            LastName: $('#driverLastName').val(),
                            StartDate: $('#StartDate').val()
                        };
                        this.sendRequestForGetDuplicateDrivers(request);
                    }
                    return true;
                }
                else
                    return false;
            }
            else if (currentObject.title == "Matching Drivers") {
                if (newIndex > currentIndex) {
                    var expectedDriver = $('input[name=UserID]:checked', $('#tblDuplicateDrivers'));
                    if (expectedDriver && expectedDriver.length > 0) {
                        var canuseredit = expectedDriver.attr('datacanuseredit');
                        var primaryfacilityname = expectedDriver.attr('dataprimaryfacilityname');
                        var primaryFacilityStoreNo = parseInt(expectedDriver.attr('dataprimaryfacilitystoreno'));
                        var selectedDriverName = expectedDriver.attr('datadriverfirstname') + " " + expectedDriver.attr('datadriverlastname');
                        var selectedDriverId = parseInt(expectedDriver.attr('datadriverid'));
                        //If driver is not associated with any facility, then ask him to do here
                        if (primaryFacilityStoreNo <= 0) {
                            var confirmmsg = selectedDriverName + " is not associated with any facility, do you want to assign him to current facility and continue to edit?";
                            facilityApp.site.showConfrim(confirmmsg, this.assingDriverToThisFacilityAndEdit.bind(this, selectedDriverId, selectedDriverName), this.callbackCancelAssingDriverToThisFacilityAndEdit.bind(this, selectedDriverName));
                        }
                        else if (canuseredit == "False") {
                            facilityApp.site.showError("This driver is associated with '" + primaryfacilityname + "', Please contact '" + primaryfacilityname + "' OM to edit '" + selectedDriverName + "' details.");
                        }
                        else {
                            var confirmmsg = "Are you sure you want to edit " + selectedDriverName;
                            facilityApp.site.showConfrimWithOkCallback(confirmmsg, this.okCallbackToEditDriver.bind(this, selectedDriverId, selectedDriverName));
                        }
                        expectedDriver.prop('checked', false);
                        return false;
                    }
                }
                else
                    return true;
            }
            else if (currentObject.title == "Security" && currentIndex < newIndex) {
                //Validate UserName and Password, make call to check username available or not. if not stop the process
                //let isUserNameAndPasswordValid = this.validateUserNameAndPasswordBeforeMoving();
                //if (isUserNameAndPasswordValid) {
                //    return true;
                //} else {
                //    return false;
                //}
            }
            return true;
        };
        driverMaintenanceForm.prototype.okCallbackToEditDriver = function (selectedDriverId, selectedDriverName) {
            this.onCanceled();
            var thStartDate = $('#StartDate').val();
            var driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
            driverMaintenanceFormObjLocal.showDriverSchedulePopup(selectedDriverId, selectedDriverName, thStartDate, driverScheduleModule.editDriverOptions.driverInformation, this);
        };
        driverMaintenanceForm.prototype.callbackCancelAssingDriverToThisFacilityAndEdit = function (driverName) {
            facilityApp.site.showError("Without assign " + driverName + " to any facility, you can not edit his details. Please contact administrator for further help.");
            //DE-SELECT ALL CHECK BOXES
            var expectedDriver = $('input[name=UserID]:checked', $('#tblDuplicateDrivers'));
            expectedDriver.prop('checked', false);
        };
        driverMaintenanceForm.prototype.assingDriverToThisFacilityAndEdit = function (driverId, driverName) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.assingDriverToFacility,
                type: 'GET',
                data: { driverId: driverId, driverName: driverName },
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackAssingDriverToThisFacilityAndEdit.bind(this))
                .fail(this.errorCallbackAssingDriverToThisFacilityAndEdit);
        };
        driverMaintenanceForm.prototype.successCallbackAssingDriverToThisFacilityAndEdit = function (respose) {
            if (respose.success == false) {
                facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
            }
            else {
                this.onCanceled();
                var thStartDate = $('#StartDate').val();
                var driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
                driverMaintenanceFormObjLocal.showDriverSchedulePopup(respose.data.driverId, respose.data.driverName, thStartDate, driverScheduleModule.editDriverOptions.driverInformation, this);
            }
        };
        driverMaintenanceForm.prototype.errorCallbackAssingDriverToThisFacilityAndEdit = function () {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        };
        driverMaintenanceForm.prototype.onStepChanged = function (event, currentIndex) {
            var currentObject = this.drSteps.steps("getStep", currentIndex);
            if (currentObject.title == "Security") {
                //let UserName = $('#driverFirstName').val() + $('#driverLastName').val();
                //$('#txtUserName').val(UserName);
                this.sendRequestToSuggesstedUserName($('#driverFirstName').val(), $('#driverLastName').val());
            }
        };
        driverMaintenanceForm.prototype.onFinishedDriverForm = function (event, currentIndex) {
            //var StoreAccess = $('select[id*="DAAccessLevel"] option:selected', $('#tBodyDriverFacilityList')).filter(function (index) {
            //    if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() != DriverFacilityStatus.Deleted) {
            //        if ($('#DriverFacility\\[' + index + '\\]\\.DAAccessLevelDisp').text().trim() == 'PrimaryFacility' && $('#DriverFacility\\[' + index + '\\]\\.DriverEndDateDisp').text() == '')
            //            return this;
            //    }
            //});
            //if (StoreAccess.length == 0) {
            //    facilityApp.site.showError('• At least one primary facility should be associated with drive without end date !\n');
            //}
            //else {
            var isUserNameAndPasswordValid = this.validateUserNameAndPasswordBeforeMoving();
            if (isUserNameAndPasswordValid) {
                //write driver information saving funcationality 
                var request = {};
                //$('select[id*="DAAccessLevel"]', $('#tBodyDriverFacilityList')).each(function (idx, select) { $(select).prop("disabled", false); });
                //$('select[id*="StoreNo"]', $('#tBodyDriverFacilityList')).each(function (idx, select) { $(select).prop("disabled", false); });
                this.sendRequestToSaveDriverDetails(request);
            }
            //} else {
            //    this.imgUserNameNotAvailable.style.visibility = 'visible';
            //    this.imgUserNameNotAvailable.style.display = 'block';
            //    this.imgUserNameAvailable.style.visibility = 'hidden';
            //    this.imgUserNameAvailable.style.display = 'none';
            //    $('#hfCheckBtnClicked').val('false');
            //}
            //}
        };
        //#region Add Driver Frist Step Start - LoadUserInformation
        driverMaintenanceForm.prototype.sendRequestForLoadUserInfo = function (postData) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.loadUserInfo,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackLoadUserInfo.bind(this))
                .fail(this.errorCallbackLoadUserInfo);
        };
        driverMaintenanceForm.prototype.successCallbackLoadUserInfo = function (result) {
            if (result == "error") {
                facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
            }
            else {
                //$('#wzDrUserInformation').html(result);
                $('#dvDriverMaintenanceForm').html(result);
                ///After userform loads into UI, then bind Widzet
                this.drSteps = $("#wzAddDriver");
                var UserId = $('#PK_UserId').val();
                if (UserId == 0)
                    this.initAddDriverWizard(false);
                else {
                    this.initAddDriverWizard(true);
                    $('.steps li').each(function (idx, li) {
                        if (!$(li).hasClass("current"))
                            $(li).addClass("done");
                    });
                }
                //After widget binds, then bind all events for the form buttons and links
                this.bindAddEditDriverFormEvents();
                //this.drSteps.steps({enableAllSteps: true});
                //fixed header
                $(".new-fixed-header", this.drSteps).tableHeadFixer();
            }
            $.blockUI;
        };
        driverMaintenanceForm.prototype.bindAddEditDriverFormEvents = function () {
            ///Driver Information tab events
            //this.bindDatePicker();
            //Driver security tab events
            this.attachClickToBtnCheckUsernameAvailable();
            //Driver Facility assignment events
            //this.bindEventsForAddEditDriverFacilities();
        };
        //bindDatePicker() {
        //    this.imgStartDateIcon = document.getElementById('driverAddStartDateIcon');
        //    let txtStartDate = $('#driverAddStartDate');
        //    this.imgStartDateIcon.onclick = (e) => { e.preventDefault(); txtStartDate.focus(); }
        //    txtStartDate.datepicker({ minDate: 0 });
        //}
        driverMaintenanceForm.prototype.errorCallbackLoadUserInfo = function (result) {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        };
        driverMaintenanceForm.prototype.validateDriverFormFirstStep = function () {
            ///Validation needs to be implemented
            var dvError = $('#lblError');
            var isValid = true;
            //validate first name
            if (String.isNullOrEmpty($('#driverFirstName').val())) {
                dvError.text('Please enter first name.');
                $('#driverFirstName').css("border", "1px solid red");
                $("#driverFirstName").attr('title', 'Please enter first name.');
                $('#driverFirstName').focus();
                isValid = false;
            }
            else {
                $('#driverFirstName').css("border", "1px #bababa solid");
            }
            //validate last name
            if (String.isNullOrEmpty($('#driverLastName').val())) {
                dvError.text('Please enter last name.');
                $('#driverLastName').css("border", "1px solid red");
                $("#driverLastName").attr('title', 'Please enter last name.');
                $('#driverLastName').focus();
                isValid = false;
            }
            else {
                $('#driverLastName').css("border", "1px #bababa solid");
            }
            //Validate Email
            if (String.isNullOrEmpty($('#driverEmail').val()) || !this.validateEmail($('#driverEmail').val())) {
                dvError.text('Please enter email address.');
                $('#driverEmail').css("border", "1px solid red");
                $("#driverEmail").attr('title', 'Please enter valid email address.');
                $('#driverEmail').focus();
                isValid = false;
            }
            else {
                $('#driverEmail').css("border", "1px #bababa solid");
            }
            //validate facility
            //if ($('#PrimaryFacilityID').val() == '') {
            //    dvError.text('Please select the Primary Facility.');
            //    $('#PrimaryFacilityID').focus();
            //    return false;
            //}
            //validate license
            if ($('#LicenseClassID').val() == '') {
                dvError.text('Please select the license class.');
                $('#LicenseClassID').css("border", "1px solid red");
                $("#LicenseClassID").attr('title', 'Please select the license class.');
                $('#LicenseClassID').focus();
                isValid = false;
            }
            else {
                $('#LicenseClassID').css("border", "1px #bababa solid");
            }
            //validate driver status.
            if ($('#StatusID').val() == '') {
                dvError.text('Please select the driver status.');
                $('#StatusID').css("border", "1px solid red");
                $("#StatusID").attr('title', 'Please select the driver status.');
                $('#StatusID').focus();
                isValid = false;
            }
            else {
                $('#StatusID').css("border", "1px #bababa solid");
            }
            dvError.text('');
            return isValid;
        };
        //#endregion Add Driver Frist Step end
        driverMaintenanceForm.prototype.validateEmail = function (email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
            ;
        };
        //#region Add Driver Second Step Start - GetDuplicateDrivers
        driverMaintenanceForm.prototype.sendRequestForGetDuplicateDrivers = function (postData) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.getDuplicateDrivers,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackGetDuplicateDrivers.bind(this))
                .fail(this.errorCallbackGetDuplicateDrivers);
        };
        driverMaintenanceForm.prototype.successCallbackGetDuplicateDrivers = function (result) {
            var secondstep = this.drSteps.steps("getStep", 1);
            var machingdriversExist = false;
            if (secondstep.title == "Matching Drivers") {
                machingdriversExist = true;
            }
            if (result.success == true && result.data == 'noduplicatedriversfound') {
                //Navigate to next tab
                if (machingdriversExist) {
                    this.drSteps.steps("next");
                    this.drSteps.steps("remove", 1);
                }
            }
            else if (result.success == true) {
                if (!machingdriversExist) {
                    this.drSteps.steps("insert", 1, {
                        title: "Matching Drivers",
                        id: "MatchingDrivers",
                        content: '<div id="wzDrDuplicateDrivers"></div>'
                    });
                    this.drSteps.steps("previous");
                }
                $('#wzDrDuplicateDrivers').html(result.data);
                // this.drSteps.steps("setStep", 1);
                //this.drSteps.steps("previous");
            }
            else {
                facilityApp.site.showError('Error occurred while getting duplicate driver information. Please reload the page and try again!');
            }
            $.blockUI;
        };
        driverMaintenanceForm.prototype.errorCallbackGetDuplicateDrivers = function (result) {
            facilityApp.site.showError('Error occurred while checking duplicate driver information. Please reload the page and try again!');
        };
        //#endregion Add Driver Second Step end GetDuplicateDrivers
        driverMaintenanceForm.prototype.attachClickToBtnCheckUsernameAvailable = function () {
            //this.btnCheckUsernameAvailable = $('#btnCheckUsernameAvailable');
            ////console.log(this.glbDriverID);
            //if (this.glbDriverID <= 0) {
            //    this.btnCheckUsernameAvailable.show();
            //    this.btnCheckUsernameAvailable.click((e) => { e.preventDefault(); this.checkUserNamePasswordEmptyAndMakeCall(); });
            //} else {
            //    this.btnCheckUsernameAvailable.hide();
            //}
            var parent = this;
            $('#txtUserName').bind("change", function (event) {
                event.preventDefault();
                parent.checkUserNamePasswordEmptyAndMakeCall();
                //$('#hfCheckBtnClicked').val('false');
                this.imgUserNameNotAvailable = document.getElementById('imgUserNameNotAvailable');
                this.imgUserNameAvailable = document.getElementById('imgUserNameAvailable');
                this.imgUserNameAvailable.style.visibility = 'hidden';
                this.imgUserNameNotAvailable.style.visibility = 'hidden';
                this.imgUserNameAvailable.style.display = 'none';
                this.imgUserNameNotAvailable.style.display = 'none';
                // $('#btnCheckUsernameAvailable').show();
                $('input[name=SuggesstestName]:checked').prop('checked', false);
            });
            $('#txtPassword').bind("change", function (event) {
                if ($('#txtPassword').length > 0) {
                    $('#txtPassword').css("border", "1px #bababa solid");
                    //1px #bababa solid
                }
                else {
                    $('#txtPassword').css("border", "red");
                }
            });
        };
        driverMaintenanceForm.prototype.validateUserNameAndPasswordBeforeMoving = function () {
            var errMsgArray = new Array();
            var dvError = $('#lblDriverSecurityError');
            var UserName = $('#txtUserName');
            var Password = $('#txtPassword');
            var isValid = true;
            if (String.isNullOrEmpty(UserName.val())) {
                //dvError.text('Please enter Username.');
                UserName.css("border", "1px solid red");
                UserName.attr('title', 'Please enter Username.');
                UserName.focus();
                isValid = false;
                errMsgArray.push('Please enter Username.');
            }
            else if (String.hasWhiteSpace(UserName.val())) {
                //dvError.text('Username should not contain spaces.');
                UserName.css("border", "1px solid red");
                UserName.attr('title', 'Username should not contain spaces.');
                UserName.focus();
                errMsgArray.push('Username should not contain spaces.');
                isValid = false;
            }
            else {
                UserName.css("border", "1px #bababa solid");
            }
            if (String.isNullOrEmpty(Password.val())) {
                //dvError.text('Please enter Password.');
                Password.css("border", "1px solid red");
                Password.attr('title', 'Please enter Password.');
                errMsgArray.push('Please enter Password.');
                Password.focus();
                isValid = false;
            }
            else if (String.hasWhiteSpace(Password.val())) {
                //dvError.text('Password should not contain spaces.');
                Password.css("border", "1px solid red");
                Password.attr('title', 'Password should not contain spaces.');
                errMsgArray.push('Password should not contain spaces.');
                Password.focus();
                isValid = false;
            }
            else if (Password.val().length < driverMaintenanceFormModule.DefaultVariables.PasswordMinLength) {
                var errorMsg = 'Password must be at least ' + driverMaintenanceFormModule.DefaultVariables.PasswordMinLength + ' characters.';
                dvError.html(errorMsg);
                Password.css("border", "1px solid red");
                Password.attr('title', errorMsg);
                errMsgArray.push(errorMsg);
                Password.focus();
                isValid = false;
            }
            else {
                Password.css("border", "1px #bababa solid");
            }
            if ($('#hfCheckBtnClicked').val() == "false") {
                if (String.isNullOrEmpty(UserName.val())) {
                    //dvError.text('Please enter Username.');
                    UserName.css("border", "1px solid red");
                    UserName.attr('title', 'Please enter Username.');
                    errMsgArray.push('Please enter Username.');
                    UserName.focus();
                    isValid = false;
                }
                else if (String.hasWhiteSpace(UserName.val())) {
                    //dvError.text('Username should not contain spaces.');
                    $('#txtUserName').css("border", "1px solid red");
                    $("#txtUserName").attr('title', 'Username should not contain spaces.');
                    errMsgArray.push('Username should not contain spaces.');
                    $('#txtUserName').focus();
                    isValid = false;
                }
                else {
                    //dvError.text('Please check for available Username.');
                    $('#txtUserName').css("border", "1px solid red");
                    $("#txtUserName").attr('title', 'Please check for available Username.');
                    errMsgArray.push('Please check for available Username.');
                    $('#txtUserName').focus();
                    isValid = false;
                }
            }
            if (isValid) {
                dvError.html('');
            }
            else {
                facilityApp.site.showError(errMsgArray.join("<br/>"));
            }
            return isValid;
        };
        driverMaintenanceForm.prototype.checkUserNamePasswordEmptyAndMakeCall = function () {
            var dvError = $('#lblDriverSecurityError');
            var UserName = $('#txtUserName').val();
            if (String.isNullOrEmpty(UserName)) {
                //dvError.text('Please enter user name.');
                $('#txtUserName').css("border", "1px solid red");
                $("#txtUserName").attr('title', 'Please enter Username.');
                $('#txtUserName').focus();
                return false;
            }
            else if (String.hasWhiteSpace(UserName)) {
                //dvError.text('User name should not contain spaces.');
                $('#txtUserName').css("border", "1px solid red");
                $("#txtUserName").attr('title', 'Username should not contain spaces.');
                $('#txtUserName').focus();
                return false;
            }
            else {
                dvError.text('');
                this.sendRequestToCheckUserNameAvailable(UserName);
            }
        };
        driverMaintenanceForm.prototype.sendRequestToCheckUserNameAvailable = function (userName) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.checkUserNameAvailability,
                type: 'GET',
                data: { UserName: userName },
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackcheckUserName.bind(this))
                .fail(this.errorCallbackcheckUserName);
        };
        driverMaintenanceForm.prototype.sendRequestToSuggesstedUserName = function (FirstName, LastName) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.SuggesstedUserName,
                type: 'GET',
                data: { FirstName: FirstName, LastName: LastName },
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackSuggesstedUserName.bind(this))
                .fail(this.errorCallbackSuggesstedUserName);
        };
        driverMaintenanceForm.prototype.successCallbackSuggesstedUserName = function (result) {
            this.imgUserNameNotAvailable = document.getElementById('imgUserNameNotAvailable');
            this.imgUserNameAvailable = document.getElementById('imgUserNameAvailable');
            if (result.success == true) {
                $('#hdrSuggestUserName').show();
                //  $('#txtUserName').val(result.message);
                $('#hfCheckBtnClicked').val('true');
                $('#txtUserName').css("border", "1px #bababa solid");
                // $('#btnCheckUsernameAvailable').hide();
                this.imgUserNameAvailable.style.visibility = 'hidden';
                this.imgUserNameNotAvailable.style.visibility = 'hidden';
                this.imgUserNameAvailable.style.display = 'none';
                this.imgUserNameNotAvailable.style.display = 'none';
                $('#suggestedName').html('');
                var parent_1 = this;
                $.each(JSON.parse(result.data), function (key, value) {
                    if (key == 0) {
                        $('#txtUserName').val(value);
                    }
                    var label = document.createElement("label");
                    var radio = document.createElement("input");
                    radio.type = "radio";
                    radio.name = "SuggesstestName";
                    radio.value = value;
                    if (key == 0)
                        radio.checked = true;
                    radio.onchange = function (e) {
                        e.preventDefault();
                        var currentTarget = $(e.currentTarget);
                        $('#txtUserName').val(currentTarget.val());
                        $('#hfCheckBtnClicked').val('true');
                        parent_1.imgUserNameNotAvailable = document.getElementById('imgUserNameNotAvailable');
                        parent_1.imgUserNameAvailable = document.getElementById('imgUserNameAvailable');
                        parent_1.imgUserNameAvailable.style.visibility = 'hidden';
                        parent_1.imgUserNameNotAvailable.style.visibility = 'hidden';
                        parent_1.imgUserNameAvailable.style.display = 'none';
                        parent_1.imgUserNameNotAvailable.style.display = 'none';
                    };
                    radio.style.marginRight = "5px";
                    label.appendChild(radio);
                    label.appendChild(document.createTextNode(value));
                    var radio_home = document.getElementById("suggestedName");
                    radio_home.appendChild(label);
                    // document.getElementById("myDiv").style.marginRight = "50px";
                });
            }
            else {
                // $('#btnCheckUsernameAvailable').show();
                $('#hfCheckBtnClicked').val('false');
            }
        };
        driverMaintenanceForm.prototype.errorCallbackSuggesstedUserName = function (result) {
            facilityApp.site.showError('Error occurred while checking username availability. Please reload the page and try again!');
        };
        driverMaintenanceForm.prototype.successCallbackcheckUserName = function (result) {
            this.imgUserNameNotAvailable = document.getElementById('imgUserNameNotAvailable');
            this.imgUserNameAvailable = document.getElementById('imgUserNameAvailable');
            if (result.success == false) {
                this.imgUserNameNotAvailable.style.visibility = 'visible';
                this.imgUserNameNotAvailable.style.display = 'block';
                this.imgUserNameAvailable.style.visibility = 'hidden';
                this.imgUserNameAvailable.style.display = 'none';
                $('#hfCheckBtnClicked').val('false');
                //facilityApp.site.showError('Error occurred while fetching driver facilities information. Please reload the page and try again!');
            }
            else {
                this.imgUserNameAvailable.style.visibility = 'visible';
                this.imgUserNameNotAvailable.style.visibility = 'hidden';
                this.imgUserNameAvailable.style.display = 'block';
                this.imgUserNameNotAvailable.style.display = 'none';
                $('#hfCheckBtnClicked').val('true');
                $('#txtUserName').css("border", "1px #bababa solid");
            }
            $.blockUI;
        };
        driverMaintenanceForm.prototype.errorCallbackcheckUserName = function (result) {
            facilityApp.site.showError('Error occurred while checking username availability. Please reload the page and try again!');
        };
        //#endregion Add Driver Third Step end GetDirverSecurity
        driverMaintenanceForm.prototype.bindEventsForAddEditDriverFacilities = function () {
            var _this = this;
            $(".icon-driverfacility-add", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineAddDriverFacility(e); });
            $(".icon-driverfacility-edit", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineEditDriverFacility(e); });
            $(".icon-driverfacility-delete", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineDeleteDriverFacility(e); });
            $(".icon-driverfacility-save", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineSaveDriverFacility(e); });
            $(".icon-driverfacility-cancel", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineCacnelDriverFacility(e); });
            $(".icon-driverfacility-StartDate", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineStartDateCalendar(e); });
            $(".icon-driverfacility-EndDate", $('#wzDrFacilityAssignment')).click(function (e) { e.preventDefault(); _this.inlineEndDateCalendar(e); });
            $(".txt-driverfacility-StartDate", $('#wzDrFacilityAssignment')).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
            $(".txt-driverfacility-EndDate", $('#wzDrFacilityAssignment')).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
            $(".dd-driverfacility-DAAccessLevel", $('#wzDrFacilityAssignment')).change(function (e) { e.preventDefault(); _this.inlineDAAccessLevelChange(e); });
        };
        driverMaintenanceForm.prototype.inlineDAAccessLevelChange = function (evt) {
            var currentTarget = $(evt.currentTarget);
            if (currentTarget.val() == DAAccessLevel.Rover) {
                //console.log(currentTarget.val());
                var thisTr = $(evt.currentTarget).closest('tr');
                var txtEndDate = $(thisTr).find('.txt-driverfacility-EndDate');
                if (txtEndDate.val().length <= 0) {
                    txtEndDate.val((new Date()).toDateMMDDYYYY());
                }
            }
        };
        driverMaintenanceForm.prototype.dateBeforeShow = function (ele, inst) {
            var canEdit = $(ele).attr('data-canedit');
            return canEdit == "True";
        };
        driverMaintenanceForm.prototype.bindEventsForAddEditDriverFacilitiesForTr = function (thisrow) {
            var _this = this;
            $(".icon-driverfacility-edit", thisrow).click(function (e) { e.preventDefault(); _this.inlineEditDriverFacility(e); });
            $(".icon-driverfacility-delete", thisrow).click(function (e) { e.preventDefault(); _this.inlineDeleteDriverFacility(e); });
            $(".icon-driverfacility-save", thisrow).click(function (e) { e.preventDefault(); _this.inlineSaveDriverFacility(e); });
            $(".icon-driverfacility-cancel", thisrow).click(function (e) { e.preventDefault(); _this.inlineCacnelDriverFacility(e); });
            $(".icon-driverfacility-StartDate", thisrow).click(function (e) { e.preventDefault(); _this.inlineStartDateCalendar(e); });
            $(".icon-driverfacility-EndDate", thisrow).click(function (e) { e.preventDefault(); _this.inlineEndDateCalendar(e); });
            $(".txt-driverfacility-StartDate", thisrow).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
            $(".txt-driverfacility-EndDate", thisrow).datepicker({ minDate: 0, beforeShow: this.dateBeforeShow });
            $(".dd-driverfacility-DAAccessLevel", thisrow).change(function (e) { e.preventDefault(); _this.inlineDAAccessLevelChange(e); });
        };
        driverMaintenanceForm.prototype.inlineAddDriverFacility = function (evt) {
            ///Get total number of facilitiy rows in the UI, when you want to add another facility, please get total facilities and send count
            ///This count will be used to build new "tr" by using next index
            var inEditMode = $('span[class*="icon-driverfacility-save"]', $('#tBodyDriverFacilityList')).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                var requestFA = {
                    Index: $('#tBodyDriverFacilityList').find('tr').length
                };
                this.sendRequestForGetAddDirverFacility(requestFA);
            }
            else {
                facilityApp.site.showError('Please save / discard existing changes!');
            }
        };
        /*Add User Facility START*/
        driverMaintenanceForm.prototype.sendRequestForGetAddDirverFacility = function (postData) {
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.addDriverFacility,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackGetAddDirverFacility.bind(this))
                .fail(this.errorCallbackGetAddDirverFacility);
        };
        driverMaintenanceForm.prototype.successCallbackGetAddDirverFacility = function (result) {
            if (result.success == false) {
                facilityApp.site.showError('Error occurred while fetching driver facilities information. Please reload the page and try again!');
            }
            else {
                $('#tBodyDriverFacilityList').prepend(result.data);
                //Get current TR index and use it to clieck edit button
                var trBlock = $(result.data);
                var rowIndex = $(trBlock[0]).attr("data-rowindex");
                this.bindEventsForAddEditDriverFacilitiesForTr($('#DriverFacility_' + rowIndex));
                $(".icon-driverfacility-edit", $('#DriverFacility_' + rowIndex)).click();
            }
            $.blockUI;
        };
        driverMaintenanceForm.prototype.errorCallbackGetAddDirverFacility = function (result) {
            facilityApp.site.showError('Error occurred while getting driver facilities information. Please reload the page and try again!');
        };
        /*Add User Facility END*/
        driverMaintenanceForm.prototype.inlineEditDriverFacility = function (evt) {
            var inEditMode = $('span[class*="icon-driverfacility-save"]', $('#tBodyDriverFacilityList')).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                var currentTarget = $(evt.currentTarget);
                //let driverFacilityId = currentTarget.attr('data-driverfacility');
                var thisTr = $(evt.currentTarget).closest('tr');
                var index = currentTarget.attr('data-index');
                $('.viewData', thisTr).hide();
                $('.editData', thisTr).show();
                $('.icon-driverfacility-edit', thisTr).hide();
                $('.icon-driverfacility-delete', thisTr).hide();
                currentTarget.hide();
                $(".actions ul li:nth-child(3)").addClass("disabled").attr("aria-disabled", "true").addClass("disable-anchor-click");
                $('.icon-driverfacility-save', thisTr).show();
                $('.icon-driverfacility-cancel', thisTr).show();
                var rowstartDate = new Date($('#DriverFacility\\[' + index + '\\]\\.DriverStartDate').val());
                var now = new Date();
                now.setHours(0, 0, 0, 0);
                if (rowstartDate < now) {
                    // selected date is in the past
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNo').prop('disabled', true);
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel').prop('disabled', true);
                }
                else {
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNo').prop('disabled', false);
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel').prop('disabled', false);
                }
            }
            else {
                facilityApp.site.showError('Please save / discard existing changes!');
            }
        };
        driverMaintenanceForm.prototype.inlineCacnelDriverFacility = function (evt) {
            var currentTarget = $(evt.currentTarget);
            //let driverFacilityId = currentTarget.attr('data-driverfacility');
            var thisTr = $(evt.currentTarget).closest('tr');
            var index = currentTarget.attr('data-index');
            $('.viewData', thisTr).show();
            $('.editData', thisTr).hide();
            $('.icon-driverfacility-edit', thisTr).show();
            $('.icon-driverfacility-delete', thisTr).show();
            currentTarget.hide();
            $('.icon-driverfacility-save', thisTr).hide();
            $('.icon-driverfacility-cancel', thisTr).hide();
            if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() == 'New') {
                thisTr.remove();
            }
            var inEditMode = $('span[class*="icon-driverfacility-save"]', $('#tBodyDriverFacilityList')).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                $(".actions ul li:nth-child(3)").attr("aria-disabled", "false").removeClass("disabled").removeClass("disable-anchor-click");
            }
        };
        driverMaintenanceForm.prototype.inlineSaveDriverFacility = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = currentTarget.closest('tr');
            //let driverFacilityId = currentTarget.attr('data-driverfacility');
            var index = currentTarget.attr('data-index');
            $('.viewData', thisTr).show();
            $('.editData', thisTr).hide();
            ///Here need to update updated values to show updated content
            $('.icon-driverfacility-delete', thisTr).show();
            $('.icon-driverfacility-edit', thisTr).show();
            $('.icon-driverfacility-cancel', thisTr).hide();
            currentTarget.hide();
            var inEditMode = $('span[class*="editData"]', $('#tBodyDriverFacilityList')).filter(function (index) {
                if ($(this).css('display') == 'inline')
                    return $(this);
            });
            if (inEditMode.length == 0) {
                $(".actions ul li:nth-child(3)").attr("aria-disabled", "false").removeClass("disabled").removeClass("disable-anchor-click");
            }
            ///Check any data is updated or not, if the data is updated then update status and respected fields
            var isValid = this.inlineValidateEditRow(evt); //-- GURU this needs to implemented
            if (isValid) {
                var isRowUpdated = this.inlineIsRowUpdated(evt); //-- GURU this needs to implemented
                if (isRowUpdated) {
                    var selectedStore = $("option:selected", $('#DriverFacility\\[' + index + '\\]\\.StoreNo'));
                    $('#DriverFacility\\[' + index + '\\]\\.DFStatus').val(DriverFacilityStatus.Updated);
                    $('#DriverFacility\\[' + index + '\\]\\.StoreRowID', thisTr).val(selectedStore.attr('data-storerowid'));
                    $('#DriverFacility\\[' + index + '\\]\\.StoreNoDisp', thisTr).html(selectedStore.attr('data-storeno'));
                    $('#DriverFacility\\[' + index + '\\]\\.FriendlyNameDisp').html(selectedStore.attr('data-dispstorename'));
                    var selectedStoreAccess = $("option:selected", $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevel'));
                    $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevelDisp').html(selectedStoreAccess.attr('data-displayname'));
                    $('#DriverFacility\\[' + index + '\\]\\.DriverStartDateDisp', thisTr).html($('#DriverFacility\\[' + index + '\\]\\.DriverStartDate').val());
                    $('#DriverFacility\\[' + index + '\\]\\.DriverEndDateDisp', thisTr).html($('#DriverFacility\\[' + index + '\\]\\.DriverEndDate').val());
                }
            }
            else {
                this.inlineEditDriverFacility(evt);
            }
        };
        driverMaintenanceForm.prototype.inlineIsRowUpdated = function (evt) {
            return true;
        };
        driverMaintenanceForm.prototype.inlineValidateEditRow = function (evt) {
            var returnValue;
            var strMessage = '';
            var currentTarget = $(evt.currentTarget);
            var thisTr = currentTarget.closest('tr');
            var idx = currentTarget.attr('data-index');
            var startDate = new Date($('#DriverFacility\\[' + idx + '\\]\\.DriverStartDate').val());
            var endDate = new Date($('#DriverFacility\\[' + idx + '\\]\\.DriverEndDate').val());
            returnValue = true;
            if (startDate > endDate) {
                strMessage = '• End Date should be greater than start date!\n';
                returnValue = false;
            }
            var selectedStoreAccess = $("option:selected", $('#DriverFacility\\[' + idx + '\\]\\.DAAccessLevel'));
            var StoreAccess = selectedStoreAccess.attr('data-displayname');
            var selectedStore = $("option:selected", $('#DriverFacility\\[' + idx + '\\]\\.StoreNo'));
            var storeno = selectedStore.attr('data-storerowid');
            var existingStoreAccess = [];
            if (StoreAccess == DAAccessLevel.Rover) {
                var parent_2 = this;
                var duplicatesStoreAccess = $('select[id*="DAAccessLevel"] option:selected', $('#tBodyDriverFacilityList')).filter(function (index) {
                    if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() != DriverFacilityStatus.Deleted) {
                        if (index != parseInt(idx)) {
                            var value = $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevelDisp').text().trim();
                            if (StoreAccess == value) {
                                var otherStartDate = $('#DriverFacility\\[' + index + '\\]\\.DriverStartDateDisp').text();
                                var otherEndDate = $('#DriverFacility\\[' + index + '\\]\\.DriverEndDateDisp').text();
                                var currentStartDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverStartDate').val();
                                var currentEndDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverEndDate').val();
                                if (parent_2.dateCheck(otherStartDate, otherEndDate, currentStartDate, currentEndDate)) {
                                    return $(this);
                                }
                            }
                        }
                        //existingStoreAccess.push(value);
                    }
                });
                //console.log(existingStoreAccess);
                if (duplicatesStoreAccess.length != 0) {
                    strMessage = strMessage + '• Rover Facility already selected for selected date!\n';
                    returnValue = false;
                }
            }
            else if (StoreAccess == DAAccessLevel.PrimaryFacility) {
                var parent_3 = this;
                var duplicatesStoreAccess_1 = $('select[id*="DAAccessLevel"] option:selected', $('#tBodyDriverFacilityList')).filter(function (index) {
                    if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() != DriverFacilityStatus.Deleted) {
                        if (index != parseInt(idx)) {
                            var value = $('#DriverFacility\\[' + index + '\\]\\.DAAccessLevelDisp').text().trim();
                            if (StoreAccess == value) {
                                var otherStartDate = $('#DriverFacility\\[' + index + '\\]\\.DriverStartDateDisp').text();
                                var otherEndDate = $('#DriverFacility\\[' + index + '\\]\\.DriverEndDateDisp').text();
                                var currentStartDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverStartDate').val();
                                var currentEndDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverEndDate').val();
                                if (parent_3.dateCheck(otherStartDate, otherEndDate, currentStartDate, currentEndDate)) {
                                    return $(this);
                                }
                            }
                        }
                    }
                });
                if (duplicatesStoreAccess_1.length != 0) {
                    strMessage = strMessage + '• Primary Facility already selected for selected date!\n';
                    returnValue = false;
                }
            }
            var existing = [];
            var parentScope = this;
            var duplicates = $('input[id*="StoreRowID"]', $('#tBodyDriverFacilityList')).filter(function (index) {
                if ($('#DriverFacility\\[' + index + '\\]\\.DFStatus').val() != DriverFacilityStatus.Deleted) {
                    if (index != parseInt(idx)) {
                        var value = $('#DriverFacility\\[' + index + '\\]\\.StoreRowID').val();
                        if (storeno == value) {
                            var otherStartDate = $('#DriverFacility\\[' + index + '\\]\\.DriverStartDateDisp').text();
                            var otherEndDate = $('#DriverFacility\\[' + index + '\\]\\.DriverEndDateDisp').text();
                            var currentStartDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverStartDate').val();
                            var currentEndDate = $('#DriverFacility\\[' + idx + '\\]\\.DriverEndDate').val();
                            if (parentScope.dateCheck(otherStartDate, otherEndDate, currentStartDate, currentEndDate)) {
                                return $(this);
                            }
                        }
                    }
                }
            });
            if (duplicates.length != 0) {
                strMessage = strMessage + '• Selected Facility already associated for selected date!\n';
                returnValue = false;
            }
            if (returnValue == false)
                facilityApp.site.showError(strMessage);
            return returnValue;
        };
        driverMaintenanceForm.prototype.dateCheck = function (existingStartDate, existingEndDate, currentStartDate, currentEndDate) {
            var eSDate, eEDate, cSDate, cEDate;
            if (currentEndDate == '') {
                currentEndDate = currentStartDate;
                cEDate = Date.parse(currentEndDate); //tEndA
                cEDate.setFullYear(cEDate.getFullYear() + 10);
            }
            else
                cEDate = Date.parse(currentEndDate); //tEndA    
            if (existingEndDate == '') {
                existingEndDate = existingStartDate;
                eEDate = Date.parse(existingEndDate); //tEndB
                eEDate.setFullYear(eEDate.getFullYear() + 10);
            }
            else
                eEDate = Date.parse(existingEndDate); //tEndB
            eSDate = Date.parse(existingStartDate); //tStartB
            cSDate = Date.parse(currentStartDate); //tStartA
            return (eSDate >= cSDate && eSDate <= cEDate || cSDate >= eSDate && cSDate <= eEDate);
        };
        driverMaintenanceForm.prototype.inlineDeleteDriverFacility = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = currentTarget.closest('tr');
            var index = currentTarget.attr('data-index');
            ///When you delete facility, mark DFStatus flag to "Deleted" and hide element from the UI
            $('#DriverFacility\\[' + index + '\\]\\.DFStatus').val(DriverFacilityStatus.Deleted);
            thisTr.hide();
        };
        driverMaintenanceForm.prototype.inlineStartDateCalendar = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = $(evt.currentTarget).closest('td');
            var txtStartDate = $(thisTr).find('.txt-driverfacility-StartDate');
            var canEditStartdate = $(txtStartDate).attr('data-canedit');
            if (canEditStartdate == "True") {
                $(txtStartDate).focus();
                $(txtStartDate).datepicker({ minDate: 0 });
            }
        };
        driverMaintenanceForm.prototype.inlineEndDateCalendar = function (evt) {
            var currentTarget = $(evt.currentTarget);
            var thisTr = $(evt.currentTarget).closest('td');
            var txtEndDate = $(thisTr).find('.txt-driverfacility-EndDate');
            $(txtEndDate).focus();
            $(txtEndDate).datepicker({ minDate: 0 });
        };
        //#endregion Add Driver Fourth Step end GetDriverFacilities
        //#region Add Driver Final Step Start - SaveDriverDetails
        driverMaintenanceForm.prototype.sendRequestToSaveDriverDetails = function (postData) {
            var dt = $("#frmSaveDriverDetails").serializeArray();
            var data = {};
            $.each(dt, function (index, value) {
                data[value.name] = value.value;
            });
            var ajaxSettings = {
                url: driverMaintenanceFormModule.URLHelper.saveDriverDetails,
                type: 'POST',
                traditional: true,
                data: JSON.stringify(data),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                async: true,
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackToSaveDriverDetails.bind(this))
                .fail(this.errorCallbackToSaveDriverDetails);
        };
        driverMaintenanceForm.prototype.successCallbackToSaveDriverDetails = function (result) {
            if (result.success == false) {
                facilityApp.site.showError('Error occurred while saving driver information. Please reload the page and try again!');
            }
            else {
                //Hide popscreen and reload main grid here
                $('.js-modal-close').click();
                this.mainDriverMaintenace.validateAndSubmit();
            }
            $.blockUI;
        };
        driverMaintenanceForm.prototype.errorCallbackToSaveDriverDetails = function (result) {
            facilityApp.site.showError('Error occurred while saving driver information. Please reload the page and try again!');
        };
        //#endregion Add Driver Fourth Step end GetDriverFacilities
        driverMaintenanceForm.prototype.showEditPopup = function (driverID, name, mainForm) {
            this.mainDriverMaintenace = mainForm;
            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popup').fadeIn();
            var driverHead = "";
            if (driverID > 0) {
                driverHead = "Edit " + name + " Information";
            }
            else {
                driverHead = "Add New Driver";
            }
            $('#popupAddEditDriverHead').text(driverHead);
            this.addEditDriver(driverID);
        };
        driverMaintenanceForm.prototype.initPopup = function () {
            var _this = this;
            $(".js-modal-close, .modal-overlay").click(function (e) {
                e.preventDefault();
                $(".modal-box, .modal-overlay").fadeOut(500, function () {
                    $(".modal-overlay").remove();
                });
                //console.log('close');
                _this.cleanAddEditPopup();
            });
            $(window).resize(function () {
                $(".modal-box").css({
                    top: 50,
                    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                });
            });
            $(window).resize();
        };
        driverMaintenanceForm.prototype.cleanAddEditPopup = function () {
            $('#wzDrUserInformation').empty();
            $('#wzDrSecurity').empty();
            $('#wzDrFacilityAssignment').empty();
        };
        return driverMaintenanceForm;
    }());
    driverMaintenanceFormModule.driverMaintenanceForm = driverMaintenanceForm;
})(driverMaintenanceFormModule || (driverMaintenanceFormModule = {}));
$(document).ready(function () {
    var oDriverForm = new driverMaintenanceFormModule.driverMaintenanceForm();
    oDriverForm.initPopup();
    //oDriverForm.initAddDriverWizard();
});
//# sourceMappingURL=driverMaintenanceForm.js.map