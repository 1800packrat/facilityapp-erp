/// <reference path="Libraries\jquery.d.ts" />;
var facilityAppForgotPassword;
(function (facilityAppForgotPassword) {
    var URLHelper = /** @class */ (function () {
        function URLHelper() {
        }
        URLHelper.ForgotPassword = '';
        URLHelper.redirectLogin = '/Home/Logout';
        return URLHelper;
    }());
    facilityAppForgotPassword.URLHelper = URLHelper;
    var forgotpassword = /** @class */ (function () {
        function forgotpassword() {
            var _this = this;
            this.btnSendEmail = document.getElementById('btnSendEmail');
            this.btnSendEmail.onclick = function (e) { _this.submit(); };
            this.btnCancel = document.getElementById('btnCancel');
            this.btnCancel.onclick = function (e) { window.location.href = URLHelper.redirectLogin; };
        }
        forgotpassword.prototype.validate = function () {
            //validations here
            if ($('#txtUserName').val() == '') {
                $('#lblError').html("Please enter user name.");
                $('#txtUserName').focus();
                return false;
            }
            $('#lblError').html('');
            return true;
        };
        forgotpassword.prototype.submit = function () {
            if (this.validate()) {
                var request = {
                    UserName: $('#txtUserName').val()
                };
                this.sendRequest(request);
            }
        };
        forgotpassword.prototype.sendRequest = function (postData) {
            var ajaxSettings = {
                url: facilityAppForgotPassword.URLHelper.ForgotPassword,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: this.successCallbackLogin,
                error: this.errorCallbackLogin
            };
            $.ajax(ajaxSettings);
        };
        forgotpassword.prototype.successCallbackLogin = function (result) {
            if (result.success == true) {
                if (result.message === "Auto") {
                    var conf = confirm('New password has been sent to your email! Please click ok to redirect to Login page.');
                    if (conf == true)
                        window.location.href = result.data;
                }
                else {
                    var conf = confirm('Your request has been sent to Help Desk and will be processed shortly. Please click OK to redirect to login page.');
                    if (conf == true)
                        window.location.href = result.data;
                }
            }
            else {
                //write your logic to show errors
                alert(result.message);
                //$('#lblError').html(result.message);
            }
        };
        forgotpassword.prototype.errorCallbackLogin = function (result) {
            alert(result.message);
            //$('#lblError').html(result.message);
        };
        return forgotpassword;
    }());
    facilityAppForgotPassword.forgotpassword = forgotpassword;
})(facilityAppForgotPassword || (facilityAppForgotPassword = {}));
$(document).ready(function () {
    var oForgotPassword = new facilityAppForgotPassword.forgotpassword();
});
//# sourceMappingURL=forgotPassword.js.map