/// <reference path="Libraries\jquery.d.ts" />;
var facilityAppChangePassword;
(function (facilityAppChangePassword) {
    var URLHelper = /** @class */ (function () {
        function URLHelper() {
        }
        URLHelper.changePassword = '';
        URLHelper.redirectLogin = '/Home/Logout';
        return URLHelper;
    }());
    facilityAppChangePassword.URLHelper = URLHelper;
    var changePassword = /** @class */ (function () {
        function changePassword() {
            var _this = this;
            this.oldPassword = '';
            this.newPassword = '';
            this.retypeNewPassword = '';
            this.btnLogin = document.getElementById('btnSignIn');
            this.btnLogin.onclick = function (e) { _this.submit(); };
            this.btnCancel = document.getElementById('btnCancel');
            this.btnCancel.onclick = function (e) { window.location.href = URLHelper.redirectLogin; };
        }
        ;
        changePassword.prototype.validatePassword = function () {
            //validations here
            if ($('#txtOldPassword').val() == '') {
                $('#lblError').html("Please enter current password.");
                $('#txtOldPassword').focus();
                return false;
            }
            if ($('#txtNewPassword').val() == '') {
                $('#lblError').html("Please enter new password.");
                $('#txtNewPassword').focus();
                return false;
            }
            if ($('#txtRetypeNewPassword').val() == '') {
                $('#lblError').html("Please enter confirm password.");
                $('#txtRetypeNewPassword').focus();
                return false;
            }
            //if ($('#txtNewPassword').val() != '') {
            //    var password = $('#txtNewPassword').val();
            //    if (!(password.match(/[A-Z]/) && password.match(/\d+/) && password.match(/.[!,@@,#,$,%,\^,&,*,?,_,~]/) && (password.length > 6))) {
            //        $('#lblError').html("This is not a strong password \n A strong password is at least 7 characters long and contains uppercase and lowercase letters, numerals, and symbols.");
            //        $('#txtNewPassword').focus();
            //        $('#txtNewPassword').val('');
            //        $('#txtRetypeNewPassword').val('');
            //        return false;
            //    }
            //}
            //if ($('#txtNewPassword').val() !== $('#txtRetypeNewPassword').val()) {
            //    $('#lblError').html("New password and confirm password should match.");
            //    $('#txtNewPassword').val('');
            //    $('#txtRetypeNewPassword').val('');
            //    return false;
            //}
            //if ($('#txtOldPassword').val() === $('#txtNewPassword').val()) {
            //    $('#lblError').html("Current password and new password should not be same, please try different password.");
            //    $('#txtNewPassword').val('');
            //    $('#txtRetypeNewPassword').val('');
            //    return false;
            //}
            $('#lblError').html('');
            return true;
        };
        changePassword.prototype.submit = function () {
            if (this.validatePassword()) {
                var request = {
                    OldPassword: $('#txtOldPassword').val(),
                    NewPassword: $('#txtNewPassword').val(),
                    RetypeNewPassword: $('#txtRetypeNewPassword').val()
                };
                this.sendRequest(request);
            }
        };
        changePassword.prototype.sendRequest = function (postData) {
            var ajaxSettings = {
                url: facilityAppChangePassword.URLHelper.changePassword,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: this.successCallbackLogin,
                error: this.errorCallbackLogin
            };
            $.ajax(ajaxSettings);
        };
        changePassword.prototype.successCallbackLogin = function (result) {
            if (result.success == true) {
                var conf = confirm('Password changed successfully. Click OK to redirect to the Login page');
                if (conf == true)
                    window.location.href = result.data;
            }
            else {
                //write your logic to show errors
                $('#lblError').html(result.message);
                $('#txtOldPassword').val('');
                $('#txtNewPassword').val('');
                $('#txtRetypeNewPassword').val('');
            }
        };
        changePassword.prototype.errorCallbackLogin = function (result) {
            $('#lblError').html(result.message);
            //Show what is the error you got from valid login page
        };
        return changePassword;
    }());
    facilityAppChangePassword.changePassword = changePassword;
})(facilityAppChangePassword || (facilityAppChangePassword = {}));
$(document).ready(function () {
    var oChangePassword = new facilityAppChangePassword.changePassword();
});
//# sourceMappingURL=changePassword.js.map