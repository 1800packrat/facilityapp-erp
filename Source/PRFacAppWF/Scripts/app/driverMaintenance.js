/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="driverMaintenanceForm.ts" />
/// <reference path="driverSchedule.ts" />
/// <reference path="site.ts" />
var driverMaintenanceModule;
(function (driverMaintenanceModule) {
    var URLHelper = /** @class */ (function () {
        function URLHelper() {
        }
        URLHelper.getDriverScheduleList = '';
        return URLHelper;
    }());
    driverMaintenanceModule.URLHelper = URLHelper;
    var driverMaintenance = /** @class */ (function () {
        function driverMaintenance() {
            var _this = this;
            this.driverMaintenanceFormObj = new driverMaintenanceFormModule.driverMaintenanceForm();
            this.btnSearch = document.getElementById('btnSearch');
            this.btnReset = document.getElementById('btnReset');
            this.btnAddNewUser = document.getElementById('btnAddNewUser');
            this.btnPrint = document.getElementById('btnPrint');
            this.imgStartDateIcon = document.getElementById('StartDateIcon');
            var txtStartDate = $('#StartDate');
            this.btnSearch.onclick = function (e) { e.preventDefault(); _this.validateAndSubmit(); };
            this.btnReset.onclick = function (e) { e.preventDefault(); _this.resetForm(); };
            this.btnAddNewUser.onclick = function (e) {
                e.preventDefault();
                var driverMaintenanceFormObjLocal = new driverMaintenanceFormModule.driverMaintenanceForm();
                driverMaintenanceFormObjLocal.showEditPopup(0, "", _this);
            };
            this.btnPrint.onclick = function (e) {
                e.preventDefault();
                _this.handlePrintFuncationality();
            };
            //this.assignEditMethodToBtn();
            this.imgStartDateIcon.onclick = function (e) { e.preventDefault(); txtStartDate.focus(); };
            txtStartDate.datepicker({});
            //window.onresize = this.adjustGridHeight;
        }
        driverMaintenance.prototype.handlePrintFuncationality = function () {
            var data = $('.cust-grid-jqgrid').clone(); // $('.tblDriverScheduleHrs').html();
            data.find('#dvDriverScheduleList').height('100%');
            data.find('#dvDriverScheduleList').css('overflow', 'visible');
            data.find('tbody').css('overflow', 'visible');
            data.find('th').each(function () { $(this).css('top', '0px'); });
            data.find('a').each(function () {
                $(this).removeAttr("href");
            });
            data.find('#imgPrevious').remove();
            data.find('#imgNext').remove();
            var mywindow = window.open('', 'Packrat Drivers', 'height=400,width=600');
            mywindow.document.write('<html><head><title>Packrat Drivers</title>');
            mywindow.document.write('<link href="/Content/css/footable.core.css" rel="stylesheet" media="print"/><link href="/Content/css/footable.standalone.css" rel="stylesheet" media="print" /> <style type="text/css" >  @page { size: landscape; } @media print { table thead tr th.center,  table tbody tr td.center { text-align: center !important; } table th, table td { border:1px solid #ccc; padding: 0.005em; } } </style>');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data.html());
            mywindow.document.write('</body></html>');
            mywindow.print();
            mywindow.close();
        };
        driverMaintenance.prototype.resetForm = function () {
            var reqDate = (new Date()).toDateMMDDYYYY();
            var request = {
                StartDate: reqDate
            };
            if (request.StartDate != undefined) {
                this.sendRequest(request);
                $('#StartDate').val(reqDate);
            }
        };
        driverMaintenance.prototype.onDriverScheduleGridLoad = function () {
            var _this = this;
            this.imgNextBtn = document.getElementById('imgNext');
            this.imgNextBtn.onclick = function (e) { e.preventDefault(); _this.nextAndSubmit(); };
            this.imgPreviousBtn = document.getElementById('imgPrevious');
            this.imgPreviousBtn.onclick = function (e) { e.preventDefault(); _this.previousAndSubmit(); };
            //this.adjustGridHeight();
        };
        //adjustGridHeight(): void {
        //var wheight = (window.innerHeight - 410);
        //$('#dvDriverScheduleList').css("height", wheight);
        //}
        driverMaintenance.prototype.validateAndSubmit = function () {
            var request = {
                StartDate: $('#StartDate').val()
            };
            if (request.StartDate != undefined)
                this.sendRequest(request);
            else
                facilityApp.site.showError("Please select date and Submit.");
        };
        driverMaintenance.prototype.nextAndSubmit = function () {
            var StartDate = new Date($('#StartDate').val());
            StartDate.setDate(StartDate.getDate() + 7);
            var reqDate = (StartDate.getMonth() + 1) + "/" + StartDate.getDate() + "/" + StartDate.getFullYear();
            var request = {
                StartDate: reqDate
            };
            if (request.StartDate != undefined) {
                this.sendRequest(request);
                $('#StartDate').val(reqDate);
                this.assignEditMethodToBtn();
            }
        };
        driverMaintenance.prototype.previousAndSubmit = function () {
            var StartDate = new Date($('#StartDate').val());
            StartDate.setDate(StartDate.getDate() - 7);
            var reqDate = (StartDate.getMonth() + 1) + "/" + StartDate.getDate() + "/" + StartDate.getFullYear();
            var request = {
                StartDate: reqDate
            };
            if (request.StartDate != undefined) {
                this.sendRequest(request);
                $('#StartDate').val(reqDate);
                this.assignEditMethodToBtn();
            }
        };
        driverMaintenance.prototype.sendRequest = function (postData) {
            var ajaxSettings = {
                url: driverMaintenanceModule.URLHelper.getDriverScheduleList,
                type: 'GET',
                data: postData,
                traditional: true,
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallback.bind(this))
                .fail(this.errorCallback)
                .done(this.onDriverScheduleGridLoad.bind(this));
        };
        driverMaintenance.prototype.successCallback = function (result) {
            if (result == "error") {
                facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
            }
            else {
                $('#dvDriverScheduleList').html(result);
                this.assignEditMethodToBtn();
                //fixed header
                $(".new-fixed-header").tableHeadFixer();
            }
            $.blockUI;
        };
        driverMaintenance.prototype.errorCallback = function (result) {
            facilityApp.site.showError('Error occurred while fetching driver information. Please reload the page and try again!');
        };
        driverMaintenance.prototype.assignEditMethodToBtn = function () {
            var _this = this;
            $('.editDriverDetails').click(function (evt) {
                evt.preventDefault();
                var currentTarget = $(evt.currentTarget);
                var driverId = currentTarget.attr('data-driverId');
                var drivername = currentTarget.attr('data-driverfullname');
                //let driverMaintenanceFormObjLocal = new driverMaintenanceFormModule.driverMaintenanceForm();
                //driverMaintenanceFormObjLocal.showEditPopup(driverId, drivername, this, driverScheduleModule.editDriverOptions.driverScheduleHours);
                var thStartDate = $('#StartDate').val();
                var driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
                driverMaintenanceFormObjLocal.showDriverSchedulePopup(driverId, drivername, thStartDate, driverScheduleModule.editDriverOptions.driverInformation, _this);
            });
            $('.editDriverLicenseDetails').click(function (evt) {
                evt.preventDefault();
                var currentTarget = $(evt.currentTarget);
                var driverId = currentTarget.attr('data-driverId');
                var drivername = currentTarget.attr('data-driverfullname');
                //let driverMaintenanceFormObjLocal = new driverMaintenanceFormModule.driverMaintenanceForm();
                //driverMaintenanceFormObjLocal.showEditPopup(driverId, drivername, this, driverScheduleModule.editDriverOptions.driverScheduleHours);
                var thStartDate = $('#StartDate').val();
                var driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
                driverMaintenanceFormObjLocal.showDriverSchedulePopup(driverId, drivername, thStartDate, driverScheduleModule.editDriverOptions.driverInformation, _this);
            });
            $('.editDriverScheduleHours').click(function (evt) {
                evt.preventDefault();
                var currentTarget = $(evt.currentTarget);
                var driverId = currentTarget.attr('data-driverId');
                var drivername = currentTarget.attr('data-driverfullname');
                //let thisTd = $(evt.currentTarget).closest('td');
                //var thStartDate = thisTd.closest('table').find('th').eq(thisTd.index()).text();
                var thStartDate = currentTarget.attr('data-date');
                var datastatus = currentTarget.attr('data-status');
                //console.log('111111111111111111 : ' + thStartDate);
                var editDriverOptions = (datastatus == "RoverOut" ? driverScheduleModule.editDriverOptions.rover : driverScheduleModule.editDriverOptions.driverScheduleHours);
                var driverMaintenanceFormObjLocal = new driverScheduleModule.driverSchedule();
                driverMaintenanceFormObjLocal.showDriverSchedulePopup(driverId, drivername, thStartDate, editDriverOptions, _this);
            });
            $('.driver-schedule-truckstatistics').click(function (evt) {
                evt.preventDefault();
                var currentTarget = $(evt.currentTarget).find('img');
                var tmp = currentTarget.attr('src');
                currentTarget.attr('src', currentTarget.attr('data-togglesrc'));
                currentTarget.attr('data-togglesrc', tmp);
                $('.truck-totalhours').toggleClass('hide');
            });
        };
        return driverMaintenance;
    }());
    driverMaintenanceModule.driverMaintenance = driverMaintenance;
})(driverMaintenanceModule || (driverMaintenanceModule = {}));
$(document).ready(function () {
    var oDM = new driverMaintenanceModule.driverMaintenance();
    oDM.validateAndSubmit();
});
//# sourceMappingURL=driverMaintenance.js.map