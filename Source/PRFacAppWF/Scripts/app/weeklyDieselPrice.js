/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqgrid.d.ts" />
/// <reference path="libraries\jquery.validation.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="libraries\jquery.tmpl.d.ts" />
/// <reference path="libraries\lib.d.ts" />
/// <reference path="site.ts" />
// SFERP-TODO-CTRMV-- Remove class --TG-563
var weeklyDieselPriceModule;
(function (weeklyDieselPriceModule) {
    var URLHelper = /** @class */ (function () {
        function URLHelper() {
        }
        URLHelper.searchDieselPricesUrl = '';
        URLHelper.saveDieselPricesURL = '';
        URLHelper.getDieselPriceURL = '';
        return URLHelper;
    }());
    weeklyDieselPriceModule.URLHelper = URLHelper;
    var getAccessLevel = /** @class */ (function () {
        function getAccessLevel() {
        }
        getAccessLevel.isStageStatusFullAccess = '';
        return getAccessLevel;
    }());
    weeklyDieselPriceModule.getAccessLevel = getAccessLevel;
    var weeklyDieselPrice = /** @class */ (function () {
        function weeklyDieselPrice() {
            var _this = this;
            this.gridId = "list";
            this.gridListId = "listPager";
            this.dsGrid = null;
            this.dsGridList = null;
            this.$searchdata = { page: 0, total: 0, records: 0, rows: 0 };
            this.dsGrid = $("#" + this.gridId);
            this.WeeklyDieselPriceFilterCriteria = $('#WeeklyDieselPriceFilterCriteria');
            this.btnSearch = document.getElementById('btnSearch');
            this.btnSearch.onclick = function (e) { e.preventDefault(); _this.validateAndSubmit(); };
            this.btnReset = document.getElementById('btnReset');
            this.btnReset.onclick = function (e) { e.preventDefault(); _this.resetFilterForm(); };
            this.btnAddNewDieselPrice = document.getElementById('btnAddNewDieselPrice');
            this.btnAddNewDieselPrice.onclick = function (e) { e.preventDefault(); _this.onAddDieselPrice(); };
            this.showHideButton = document.getElementById('showHideButton');
            this.showHideButton.onclick = function (e) {
                e.preventDefault();
                $(e.target).toggleClass("ui-icon-circle-triangle-n ui-icon-circle-triangle-s");
                $('.form-list-watouches').slideToggle();
            };
            this.hShowHide = document.getElementById('hShowHide');
            this.hShowHide.onclick = function (e) {
                e.preventDefault();
                $(e.target).toggleClass("ui-icon-circle-triangle-n ui-icon-circle-triangle-s");
                $('.form-list-watouches').slideToggle();
            };
            this.dvEditWeeklyDieselPrice = $('#dvEditWeeklyDieselPrice');
            this.dvEditWeeklyDieselPriceTmpl = $('#dvEditWeeklyDieselPriceTmpl');
            this.bindFilterDatepicker();
            this.modelSaveWeeklyDieselPrice = $('#modelSaveWeeklyDieselPrice');
            this.modelSaveWeeklyDieselPrice.unbind('click').click(function (e) {
                e.preventDefault();
                _this.onUpdateWeeklyDieselPrice(e, _this);
            });
            this.LocalTransSubsidyDefaultValue = $('#LocalTransSubsidyDefaultValue').val();
        }
        weeklyDieselPrice.prototype.onUpdateWeeklyDieselPrice = function (e, obj) {
            var dieselPriceModel = this.getDieselPriceFromForm();
            if (this.isValidDieselForm(dieselPriceModel)) {
                var postData = { DieselPrice: dieselPriceModel };
                this.saveDieselPrice(postData);
            }
        };
        weeklyDieselPrice.prototype.getDieselPriceFromForm = function () {
            //console.log($('.dtBeginDate', this.dvEditWeeklyDieselPrice));
            var dmodel = {
                ID: $('#ID', this.dvEditWeeklyDieselPrice).val(),
                BeginDate: $('.dtBeginDate', this.dvEditWeeklyDieselPrice).val(),
                EndDate: $('.dtEndDate', this.dvEditWeeklyDieselPrice).val(),
                DieselPrice: $('#DieselPrice', this.dvEditWeeklyDieselPrice).val(),
                FuelAdjustmentRate: $('#FuelAdjustmentRate', this.dvEditWeeklyDieselPrice).val(),
                LDMMultiplier: $('#LDMMultiplier', this.dvEditWeeklyDieselPrice).val(),
                LocalMultiplier: $('#LocalMultiplier', this.dvEditWeeklyDieselPrice).val(),
                ActualTransCost: $('#ActualTransCost', this.dvEditWeeklyDieselPrice).val(),
                BaseTransCost: $('#BaseTransCost', this.dvEditWeeklyDieselPrice).val(),
                LocalTransSubsidy: $('#LocalTransSubsidy', this.dvEditWeeklyDieselPrice).val(),
                RepositioningFee: $('#RepositioningFee', this.dvEditWeeklyDieselPrice).val()
            };
            return dmodel;
        };
        weeklyDieselPrice.prototype.isValidDieselForm = function (model) {
            var validMessage = '';
            //If it is new entry validate dates range
            if (!(model.BeginDate.length > 0)) {
                validMessage = "Please select begin date";
            }
            if (!(model.EndDate.length > 0)) {
                validMessage += "<br /> Please select end date";
            }
            if (new Date(model.BeginDate) > new Date(model.EndDate)) {
                validMessage += "<br /> End date should be grater than Begin date";
            }
            if (!model.DieselPrice) {
                validMessage += "<br /> Please enter valid diesel price";
            }
            if (!model.FuelAdjustmentRate) {
                validMessage += "<br /> Please enter valid fuel adjustment rate";
            }
            if (!model.LocalMultiplier) {
                validMessage += "<br /> Please enter valid local multiplier";
            }
            if (!model.LDMMultiplier) {
                validMessage += "<br /> Please enter valid LDM multiplier";
            }
            if (!model.ActualTransCost) {
                validMessage += "<br /> Please enter valid actual transcost";
            }
            if (!model.BaseTransCost) {
                validMessage += "<br /> Please enter valid base transcost";
            }
            if (!model.LocalTransSubsidy) {
                validMessage += "<br /> Please enter valid local transcost";
            }
            if (!model.RepositioningFee) {
                validMessage += "<br /> Please enter valid repositioning fee";
            }
            if (validMessage.length > 0) {
                facilityApp.site.showError(validMessage);
                return false;
            }
            return true;
        };
        weeklyDieselPrice.prototype.saveDieselPrice = function (postData) {
            var ajaxSettings = {
                url: weeklyDieselPriceModule.URLHelper.saveDieselPricesURL,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbacksaveDieselPrice.bind(this))
                .fail(this.errorCallbacksaveDieselPrice);
        };
        weeklyDieselPrice.prototype.successCallbacksaveDieselPrice = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                $('.js-modal-close').click();
                $('#btnSearch').trigger('click');
            }
            $.blockUI;
        };
        weeklyDieselPrice.prototype.errorCallbacksaveDieselPrice = function (result) {
            facilityApp.site.showError('Error occurred while saving diesel price. Please reload the page and try again!');
        };
        weeklyDieselPrice.prototype.bindFilterDatepicker = function () {
            var txtBeginDate = $('.dtBeginDate', this.WeeklyDieselPriceFilterCriteria);
            var txtBeginDateIconIcon = $('#BeginDateIcon', this.WeeklyDieselPriceFilterCriteria);
            txtBeginDateIconIcon.unbind("click").click(function (e) {
                e.preventDefault();
                txtBeginDate.focus();
            });
            txtBeginDate.datepicker();
            var txtEndDate = $('.dtEndDate', this.WeeklyDieselPriceFilterCriteria);
            var txtEndDateIconIcon = $('#EndDateIcon', this.WeeklyDieselPriceFilterCriteria);
            txtEndDateIconIcon.unbind("click").click(function (e) {
                e.preventDefault();
                txtEndDate.focus();
            });
            txtEndDate.datepicker();
        };
        weeklyDieselPrice.prototype.resetFilterForm = function () {
            $('#WeeklyDieselPriceFilterCriteria')[0].reset();
            $('#btnSearch').click();
        };
        weeklyDieselPrice.prototype.validateAndSubmit = function () {
            var searchText = {
                BeginDate: $('#BeginDate').val(),
                EndDate: $('#EndDate').val(),
            };
            ///Before we fire a query to server, look for data in inmemory for this given date range. if it exists then filter on the local data. This saves SL calls
            this.sendRequestToDieselPrice(searchText);
        };
        weeklyDieselPrice.prototype.sendRequestToDieselPrice = function (postData) {
            var ajaxSettings = {
                url: weeklyDieselPriceModule.URLHelper.searchDieselPricesUrl,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbacksendRequestToDieselPrice.bind(this))
                .fail(this.errorCallbacksendRequestToDieselPrice);
        };
        weeklyDieselPrice.prototype.successCallbacksendRequestToDieselPrice = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                ///Before load new data, we have to unbind and bind new controlles
                this.unloadAndBindGrid();
                this.$searchdata = {
                    page: result.data.page,
                    total: result.data.total,
                    records: result.data.records,
                    rows: result.data.rows
                };
                ///once we get data, then store it in local dataset and then call below method to apply local filters. and then populate grid.
                this.bindGridControl(this.$searchdata);
            }
            $.blockUI;
        };
        weeklyDieselPrice.prototype.errorCallbacksendRequestToDieselPrice = function (result) {
            facilityApp.site.showError('Error occurred while fetching diesel price. Please reload the page and try again!');
        };
        weeklyDieselPrice.prototype.unloadAndBindGrid = function () {
            $.jgrid.gridUnload(this.gridId);
            //Initiate grid controlles and load the data
            this.dsGrid = $("#" + this.gridId);
            this.dsGridList = $("#" + this.gridListId);
        };
        weeklyDieselPrice.prototype.showAddEditForm = function (dieselPriceModel) {
            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popupWeeklyDieselPrice').fadeIn();
            var editFormTmpl = this.dvEditWeeklyDieselPriceTmpl.tmpl(dieselPriceModel);
            this.dvEditWeeklyDieselPrice.html(editFormTmpl);
            facilityApp.site.bindAcceptOnlyDecimalEvent();
            //Edit mode we should not allow user to edit dates
            if (dieselPriceModel.ID <= 0) {
                $('.dprice-edit-popuphead').html('Add');
                var txtBeginDate_1 = $('.dtBeginDate', this.dvEditWeeklyDieselPrice);
                txtBeginDate_1.removeAttr('disabled');
                var txtBeginDateIconIcon = $('#BeginDateIcon', this.dvEditWeeklyDieselPrice);
                txtBeginDateIconIcon.unbind("click").click(function (e) {
                    e.preventDefault();
                    txtBeginDate_1.focus();
                });
                txtBeginDate_1.datepicker({
                    minDate: new Date(),
                    onSelect: function (dateText, inst) {
                        txtEndDate_1.datepicker("option", "minDate", txtBeginDate_1.datepicker("getDate"));
                    }.bind(this)
                });
                var txtEndDate_1 = $('.dtEndDate', this.dvEditWeeklyDieselPrice);
                txtEndDate_1.removeAttr('disabled');
                var txtEndDateIconIcon = $('#EndDateIcon', this.dvEditWeeklyDieselPrice);
                txtEndDateIconIcon.unbind("click").click(function (e) {
                    e.preventDefault();
                    txtEndDate_1.focus();
                });
                txtEndDate_1.datepicker({
                    minDate: new Date(),
                    onSelect: function (dateText, inst) {
                        txtBeginDate_1.datepicker("option", "maxDate", txtEndDate_1.datepicker("getDate"));
                    }.bind(this)
                });
            }
            else {
                $('.dtBeginDate', this.dvEditWeeklyDieselPrice).attr('disabled', 'disabled');
                $('.dtEndDate', this.dvEditWeeklyDieselPrice).attr('disabled', 'disabled');
                $('.dprice-edit-popuphead').html('Edit');
            }
        };
        weeklyDieselPrice.prototype.onAddDieselPrice = function () {
            var addForm = {
                ID: 0,
                BeginDate: '',
                EndDate: '',
                DieselPrice: 0,
                FuelAdjustmentRate: 0,
                LDMMultiplier: 0,
                LocalMultiplier: 0,
                ActualTransCost: 0,
                BaseTransCost: 0,
                LocalTransSubsidy: this.LocalTransSubsidyDefaultValue,
                RepositioningFee: 0
            };
            this.showAddEditForm(addForm);
        };
        weeklyDieselPrice.prototype.sendRequestToGetDieselPrice = function (id) {
            var ajaxSettings = {
                url: weeklyDieselPriceModule.URLHelper.getDieselPriceURL,
                type: 'GET',
                data: { Id: id },
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbacksendRequestToGetDieselPrice.bind(this))
                .fail(this.errorCallbacksendRequestToGetDieselPrice);
        };
        weeklyDieselPrice.prototype.successCallbacksendRequestToGetDieselPrice = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                ///Before load new data, we have to unbind and bind new controlles
                this.unloadAndBindGrid();
                this.$searchdata = {
                    page: result.data.page,
                    total: result.data.total,
                    records: result.data.records,
                    rows: result.data.rows
                };
                ///once we get data, then store it in local dataset and then call below method to apply local filters. and then populate grid.
                this.bindGridControl(this.$searchdata);
            }
            $.blockUI;
        };
        weeklyDieselPrice.prototype.errorCallbacksendRequestToGetDieselPrice = function (result) {
            facilityApp.site.showError('Error occurred while fetching diesel price. Please reload the page and try again!');
        };
        weeklyDieselPrice.prototype.init = function () {
            this.validateAndSubmit();
            this.initPopup();
        };
        weeklyDieselPrice.prototype.initPopup = function () {
            $(".js-modal-close, .modal-overlay").click(function (e) {
                e.preventDefault();
                $(".modal-box, .modal-overlay").fadeOut(500, function () {
                    $(".modal-overlay").remove();
                });
            });
            $(window).resize(function () {
                $(".modal-box").css({
                    top: 5,
                    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                });
            });
            $(window).resize();
        };
        weeklyDieselPrice.prototype.formatEditButton = function (cellvalue, options, rowObject) {
            return "<input type='button' class='editWeeklyDieselPrice' value='Edit'"
                + "data-id='" + rowObject.ID + "'"
                + "data-begindate='" + rowObject.BeginDateDisp + "'"
                + "data-enddate='" + rowObject.EndDateDisp + "'"
                + "data-dieselprice='" + rowObject.DieselPrice + "'"
                + "data-fueladjustmentrate='" + rowObject.FuelAdjustmentRate + "'"
                + "data-ldmmultiplier='" + rowObject.LDMMultiplier + "'"
                + "data-localmultiplier='" + rowObject.LocalMultiplier + "'"
                + "data-actualtranscost='" + rowObject.ActualTransCost + "'"
                + "data-basetranscost='" + rowObject.BaseTransCost + "'"
                + "data-localtranssubsidy='" + rowObject.LocalTransSubsidy + "'"
                + "data-repositioningfee='" + rowObject.RepositioningFee + "'"
                + "/>";
        };
        weeklyDieselPrice.prototype.bindGridControl = function (data) {
            var columnNames = ['ID', 'Begin Date', 'Begin Date', 'End Date', 'End Date', 'Price', 'Fuel Adjustment Rate', 'LDM Multiplier', 'Local Multiplier', 'Actual TransCost', 'Base TransCost', 'Repositioning Fee', 'Local Trans Subsidy', 'Action'];
            var columnModel = [
                { name: 'ID', index: 'ID', editable: false, hidedlg: true, key: true, hidden: true, search: false },
                { name: 'BeginDate', index: 'BeginDate', editable: false, search: false, align: 'center', sortable: true, formatter: 'date' },
                { name: 'BeginDateDisp', index: 'BeginDateDisp', editable: false, search: false, align: 'center', sortable: true, hidden: true },
                { name: 'EndDate', index: 'EndDate', editable: false, search: false, align: 'center', sortable: true, formatter: 'date' },
                { name: 'EndDateDisp', index: 'EndDateDisp', editable: false, search: false, align: 'center', sortable: true, hidden: true },
                { name: 'DieselPrice', index: 'DieselPrice', search: false, align: 'center', sortable: true },
                { name: 'FuelAdjustmentRate', index: 'FuelAdjustmentRate', editable: false, hidedlg: true, align: 'center', hidden: false, search: false },
                { name: 'LDMMultiplier', index: 'LDMMultiplier', editable: false, hidedlg: true, align: 'center', hidden: false, search: false },
                { name: 'LocalMultiplier', index: 'LocalMultiplier', editable: false, hidedlg: true, align: 'center', hidden: false, search: false },
                { name: 'ActualTransCost', index: 'ActualTransCost', editable: false, hidedlg: true, align: 'center', hidden: false, search: false },
                { name: 'BaseTransCost', index: 'BaseTransCost', editable: false, hidedlg: true, align: 'center', hidden: false, search: false },
                { name: 'RepositioningFee', index: 'RepositioningFee', editable: false, hidedlg: true, align: 'center', hidden: false, search: false },
                { name: 'LocalTransSubsidy', index: 'LocalTransSubsidy', editable: false, hidedlg: true, align: 'center', hidden: false, search: false },
                { name: 'Action', index: 'Action', width: 40, editable: false, hidedlg: true, align: 'center', sortable: false, hidden: false, search: false, formatter: this.formatEditButton.bind(this) }
            ];
            var jqGridOptions = {
                datatype: "local",
                data: data.rows,
                loadonce: true,
                pager: 'listPager',
                colNames: columnNames,
                colModel: columnModel,
                rowNum: 20,
                rowList: [10, 20, 30, 60, 365]
                //, sortname: 'ActionID'
                //, sortorder: "asc"
                ,
                viewrecords: true,
                caption: 'Weekly Diesel Price',
                autoWidth: false,
                gridview: true,
                id: "ID",
                sortname: "ID",
                sortorder: "desc",
                height: "100%",
                width: null
                //, multiselect: true
                //, multiboxonly: true
                //  , toolbar: [true, "top"]
                ,
                emptyrecords: 'no records found',
                gridComplete: this.gridCompleteLocal.bind(this),
                loadComplete: this.gridLoadComplete.bind(this),
                loadError: this.gridLoadError.bind(this)
                //, ondblClickRow: this.gridDblClickRow.bind(this)
            };
            this.dsGrid.jqGrid(jqGridOptions).navGrid(this.dsGridList, {
                edit: false, add: false, del: false, search: false, refresh: false
            }, null, null, null);
        };
        weeklyDieselPrice.prototype.gridCompleteLocal = function () {
        };
        weeklyDieselPrice.prototype.gridLoadComplete = function () {
            var _this = this;
            if (this.dsGrid.getGridParam('records') === 0) {
                $('#' + this.gridId + ' tbody').html("<div style='text-align:center; padding: 30px'>No records found</div>");
            }
            else {
                $('.editWeeklyDieselPrice').unbind('click').click(function (evt) {
                    evt.preventDefault();
                    var currentElement = $(evt.currentTarget);
                    var editFormModel = {
                        ID: currentElement.data('id'),
                        BeginDate: currentElement.data('begindate'),
                        EndDate: currentElement.data('enddate'),
                        DieselPrice: currentElement.data('dieselprice'),
                        FuelAdjustmentRate: currentElement.data('fueladjustmentrate'),
                        LDMMultiplier: currentElement.data('ldmmultiplier'),
                        LocalMultiplier: currentElement.data('localmultiplier'),
                        ActualTransCost: currentElement.data('actualtranscost'),
                        BaseTransCost: currentElement.data('basetranscost'),
                        LocalTransSubsidy: currentElement.data('localtranssubsidy'),
                        RepositioningFee: currentElement.data('repositioningfee')
                    };
                    _this.showAddEditForm(editFormModel);
                });
            }
        };
        weeklyDieselPrice.prototype.gridLoadError = function (xhr, st, err) {
            if (xhr.status == "200")
                return false;
            var error = eval('(' + xhr.responseText + ')');
            $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                width: '600',
                buttons: {
                    Ok: function () {
                        //$(this).dialog("close");
                    }
                }
            });
        };
        weeklyDieselPrice.prototype.reloadDSGrid = function () {
            this.dsGrid.jqGrid("clearGridData", true);
            this.dsGrid.jqGrid('setGridParam', {
                mtype: 'GET',
                url: weeklyDieselPriceModule.URLHelper.searchDieselPricesUrl,
                datatype: 'json',
                postData: {
                    StatusName: $('#txtSearchStatus').val()
                }
            }).trigger("reloadGrid");
        };
        return weeklyDieselPrice;
    }());
    weeklyDieselPriceModule.weeklyDieselPrice = weeklyDieselPrice;
})(weeklyDieselPriceModule || (weeklyDieselPriceModule = {}));
$(document).ready(function () {
    var oweeklyDieselPrice = new weeklyDieselPriceModule.weeklyDieselPrice();
    oweeklyDieselPrice.init();
});
//# sourceMappingURL=weeklyDieselPrice.js.map