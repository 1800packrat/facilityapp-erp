/// <reference path="Libraries\jquery.d.ts" />;
/// <reference path="libraries\jquery.blockui.d.ts" />
/// <reference path="libraries\jqgrid.d.ts" />
/// <reference path="libraries\jquery.validation.d.ts" />
/// <reference path="libraries\jqueryui.d.ts" />
/// <reference path="libraries\jquery.tmpl.d.ts" />
/// <reference path="site.ts" />
var waTouchContainerStatusModule;
(function (waTouchContainerStatusModule) {
    var URLHelper = /** @class */ (function () {
        function URLHelper() {
        }
        URLHelper.searchContainerStatusList = '';
        URLHelper.setStatusActiveAndInactive = '';
        URLHelper.addEditContainerStatus = '';
        return URLHelper;
    }());
    waTouchContainerStatusModule.URLHelper = URLHelper;
    var getAccessLevel = /** @class */ (function () {
        function getAccessLevel() {
        }
        getAccessLevel.isStageStatusFullAccess = '';
        return getAccessLevel;
    }());
    waTouchContainerStatusModule.getAccessLevel = getAccessLevel;
    var waTouchContainerStatus = /** @class */ (function () {
        function waTouchContainerStatus() {
            var _this = this;
            //private isContainerStatusFullAccess = window.isStageStatusFullAccess;
            this.gridId = "list";
            this.gridListId = "listPager";
            this.dsGrid = null;
            this.dsGridList = null;
            this.$searchdata = { key: "", data: {} };
            this.dsGrid = $("#" + this.gridId);
            this.btnSearch = document.getElementById('btnSearch');
            this.btnSearch.onclick = function (e) { e.preventDefault(); _this.validateAndSubmit(false); };
            this.btnAddNewStatus = document.getElementById('btnAddNewStatus');
            this.btnAddNewStatus.onclick = function (e) { e.preventDefault(); _this.addNewContainerStatus(); };
            this.btnSaveContainerStatus = document.getElementById('modelSaveContainerStatusInfo');
            this.btnSaveContainerStatus.onclick = function (e) { e.preventDefault(); _this.saveContainerStatus(); };
            this.btnReset = document.getElementById('btnReset');
            this.btnReset.onclick = function (e) { $('#txtSearchStatus').val(''); _this.validateAndSubmit(true); $('#showHideButton').trigger('click'); };
        }
        waTouchContainerStatus.prototype.validateAndSubmit = function (isReset) {
            var searchText = {
                StatusName: $('#txtSearchStatus').val()
            };
            var existingDataKey = this.$searchdata.key;
            var newSearchKey = this.getSearchKey();
            ///Before we fire a query to server, look for data in inmemory for this given date range. if it exists then filter on the local data. This saves SL calls
            if (this.$searchdata.data != null && !isReset) {
                this.filterDataSetAndLoadGrid();
            }
            else {
                this.sendRequestToContainerStatusDetails(searchText);
            }
        };
        waTouchContainerStatus.prototype.sendRequestToContainerStatusDetails = function (postData) {
            var ajaxSettings = {
                url: waTouchContainerStatusModule.URLHelper.searchContainerStatusList,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbacksendRequestToContainerStatusDetails.bind(this))
                .fail(this.errorCallbacksendRequestToContainerStatusDetails);
        };
        waTouchContainerStatus.prototype.successCallbacksendRequestToContainerStatusDetails = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                this.$searchdata.data = result.Data;
                this.$searchdata.key = this.getSearchKey();
                ///Before load new data, we have to unbind and bind new controlles
                this.unloadAndBindGrid();
                ///once we get data, then store it in local dataset and then call below method to apply local filters. and then populate grid.
                this.filterDataSetAndLoadGrid();
            }
            $.blockUI;
        };
        waTouchContainerStatus.prototype.errorCallbacksendRequestToContainerStatusDetails = function (result) {
            facilityApp.site.showError('Error occurred while fetching stating areas. Please reload the page and try again!');
        };
        waTouchContainerStatus.prototype.unloadAndBindGrid = function () {
            $.jgrid.gridUnload(this.gridId);
            //Initiate grid controlles and load the data
            this.dsGrid = $("#" + this.gridId);
            this.dsGridList = $("#" + this.gridListId);
        };
        waTouchContainerStatus.prototype.filterDataSetAndLoadGrid = function () {
            ///Before load new data, we have to unbind and bind new controlles
            this.unloadAndBindGrid();
            var txtSearchStatus = $.trim($('#txtSearchStatus').val());
            var filterDataFromLocalDS = $.grep(this.$searchdata.data.rows, function (o, i) {
                return (txtSearchStatus.length == 0 || o.ActionName.toLowerCase().indexOf(txtSearchStatus.toLowerCase()) >= 0);
            });
            var response = { page: this.$searchdata.data.page, total: this.$searchdata.data.total, records: this.$searchdata.data.records, rows: filterDataFromLocalDS };
            this.bindGridControl(response);
        };
        waTouchContainerStatus.prototype.getSearchKey = function () {
            return $('#txtSearchStatus').val();
        };
        waTouchContainerStatus.prototype.addNewContainerStatus = function () {
            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popupEditContainerStatus').fadeIn();
            $('#containerStatusHeader').html("Add Information");
            this.clearAllControlsInAddEditForm();
        };
        waTouchContainerStatus.prototype.validateData = function () {
            var editDvPopup = $('#dvEditContainerStatus');
            if ($('#DispStatusName', editDvPopup).val() == '') {
                facilityApp.site.showAlert('Please enter staging area name.');
                $('DispStatusName', editDvPopup).focus();
                return false;
            }
            return true;
        };
        waTouchContainerStatus.prototype.saveContainerStatus = function () {
            if (this.validateData()) {
                var editDvPopup = $('#dvEditContainerStatus');
                var postData = {
                    ActionID: $('#hfPKId', editDvPopup).val(),
                    Status: $('#Status', editDvPopup).val(),
                    ActionCategoryID: $('#hfActionCategoryID', editDvPopup).val(),
                    ActionName: $('#DispStatusName', editDvPopup).val(),
                    Description: $('#txtStatusDesc', editDvPopup).val(),
                    IsGlobalStatus: $('#hfIsGlobalStatus', editDvPopup).val()
                };
                var ajaxSettings = {
                    url: waTouchContainerStatusModule.URLHelper.addEditContainerStatus,
                    type: 'POST',
                    data: JSON.stringify(postData),
                    traditional: true,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8'
                };
                $.ajax(ajaxSettings)
                    .then(this.successCallbackaddEditContainerStatus.bind(this))
                    .fail(this.errorCallbackaddEditContainerStatus);
            }
        };
        waTouchContainerStatus.prototype.successCallbackaddEditContainerStatus = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                //After save data, we need to reload grid. To reload grid, we have to clear local data variable. so that system will get new data from server.
                this.$searchdata.key = "";
                this.validateAndSubmit(true);
                //After save data, we need to close popup
                $('.js-modal-close').click();
            }
        };
        waTouchContainerStatus.prototype.errorCallbackaddEditContainerStatus = function () {
            facilityApp.site.showError("Error occurred while saving staging area, please reload and try again!");
        };
        waTouchContainerStatus.prototype.init = function () {
            this.validateAndSubmit(true);
            this.initPopup();
        };
        waTouchContainerStatus.prototype.initPopup = function () {
            $(".js-modal-close, .modal-overlay").click(function (e) {
                e.preventDefault();
                $(".modal-box, .modal-overlay").fadeOut(500, function () {
                    $(".modal-overlay").remove();
                });
            });
            $(window).resize(function () {
                $(".modal-box").css({
                    top: 5,
                    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                });
            });
            $(window).resize();
        };
        waTouchContainerStatus.prototype.SetActiveInactive = function (PkId, Status, isGlobalStatus, Parent) {
            facilityApp.site.showConfrimWithOkCallback("Are you sure you want to make it " + Status + " ?", this.callBackActiveToggleConfirm.bind(this, PkId, Status, isGlobalStatus));
        };
        waTouchContainerStatus.prototype.callBackActiveToggleConfirm = function (PkId, status, isGlobalStatus) {
            var postData = {
                PKID: PkId,
                Status: status,
                IsGlobalStatus: isGlobalStatus
            };
            var ajaxSettings = {
                url: waTouchContainerStatusModule.URLHelper.setStatusActiveAndInactive,
                type: 'POST',
                data: JSON.stringify(postData),
                traditional: true,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            };
            $.ajax(ajaxSettings)
                .then(this.successCallbackToggleActiveStatus.bind(this))
                .fail(this.errorCallbackToggleActiveStatus);
        };
        waTouchContainerStatus.prototype.successCallbackToggleActiveStatus = function (result) {
            if (result.success == false) {
                facilityApp.site.showError(result.message);
            }
            else {
                this.$searchdata.key = "";
                this.validateAndSubmit(true);
            }
        };
        waTouchContainerStatus.prototype.errorCallbackToggleActiveStatus = function () {
            facilityApp.site.showError("Error occurred while changing status, please reload and try again!");
        };
        waTouchContainerStatus.prototype.showEditPopup = function (RowId, Status, StatusCatergoryId, Description, DispActionName, IsGlobalStatus) {
            var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            $('#popupEditContainerStatus').fadeIn();
            $('#txtStatusDesc', $('#dvEditContainerStatus')).val(Description);
            $('#Status', $('#dvEditContainerStatus')).val(Status);
            //$('#StatusCategory', $('#dvEditContainerStatus')).val(StatusCatergoryId);
            $('#DispStatusName', $('#dvEditContainerStatus')).val(DispActionName);
            $('#hfPKId', $('#dvEditContainerStatus')).val(RowId);
            $('#hfIsGlobalStatus', $('#dvEditContainerStatus')).val(IsGlobalStatus);
            $('#hfActionCategoryID', $('#dvEditContainerStatus')).val(StatusCatergoryId);
            $('#containerStatusHeader').html('Edit <b>' + DispActionName + '</b> Information');
        };
        waTouchContainerStatus.prototype.clearAllControlsInAddEditForm = function () {
            $('#txtStatusDesc', $('#dvEditContainerStatus')).val("");
            //$('#Status', $('#dvEditContainerStatus')).val(-1);
            $('#hfActionCategoryID', $('#dvEditContainerStatus')).val(1);
            $('#DispStatusName', $('#dvEditContainerStatus')).val("");
            $('#hfPKId', $('#dvEditContainerStatus')).val(0);
        };
        waTouchContainerStatus.prototype.formatEditButton = function (cellvalue, options, rowObject) {
            var retString = "<input type='button' class='editContainerStatus' value='Edit' data-Id='"
                + rowObject.ActionID + "' data-status='" + rowObject.Status + "' data-ActionCategoryID='" + rowObject.ActionCategoryID
                + "' data-Description='" + rowObject.Description + "' data-dispactionname='" + rowObject.ActionName
                + "' data-isglobalstatus='" + rowObject.IsGlobalStatus + "'/>";
            if (waTouchContainerStatusModule.getAccessLevel.isStageStatusFullAccess == "False") {
                if (rowObject.Status == null || rowObject.Status == "N") {
                    retString = retString + "<input disabled class='btnActive' value='Active' data-isglobalstatus='" + rowObject.IsGlobalStatus + "' data-value='" + rowObject.ActionID + "' type='button' style='margin: 2px;' />";
                }
                else if (rowObject.Status == "A") {
                    retString = retString + "<input disabled class='btnActive'  value= 'InActive' data- isglobalstatus='" + rowObject.IsGlobalStatus + "' data- value='" + rowObject.ActionID + "' type= 'button'  style= 'margin: 2px;' />";
                }
            }
            else {
                if (rowObject.Status == null || rowObject.Status == "N") {
                    retString = retString + "<input class='btnActive' value='Active' data-isglobalstatus='" + rowObject.IsGlobalStatus + "' data-value='" + rowObject.ActionID + "' type='button' style='margin: 2px;' />";
                }
                else if (rowObject.Status == "A") {
                    retString = retString + "<input class='btnActive' value='InActive' data-isglobalstatus='" + rowObject.IsGlobalStatus + "' data-value='" + rowObject.ActionID + "' type='button'  style='margin: 2px;' />";
                }
            }
            return retString;
        };
        waTouchContainerStatus.prototype.formatStatusLevel = function (cellvalue, options, rowObject) {
            var sts = "Facility Specific";
            if (rowObject.IsGlobalStatus == 1) {
                sts = "Global";
            }
            return sts;
        };
        waTouchContainerStatus.prototype.formatStatus = function (cellvalue, options, rowObject) {
            var sts = "InActive";
            if (rowObject.Status == 'A') {
                sts = "Active";
            }
            return sts;
        };
        waTouchContainerStatus.prototype.bindGridControl = function (data) {
            var columnNames = ['RowID', 'Stage Area', 'Description', 'Category', 'Status Level', 'Status', '', 'IsGlobalStatus'];
            var columnModel = [
                { name: 'ActionID', index: 'ActionID', editable: false, hidedlg: true, hidden: true, search: false },
                { name: 'ActionName', index: 'ActionName', editable: false, search: false, align: 'center', sortable: true },
                { name: 'Description', index: 'Description', editable: false, search: false, align: 'center', sortable: true },
                { name: 'ActionCategoryName', index: 'ActionCategoryName', search: false, align: 'center', sortable: true },
                { name: 'StatusLevel', index: 'StatusLevel', editable: false, hidedlg: true, align: 'center', hidden: false, search: false, formatter: this.formatStatusLevel.bind(this) },
                { name: 'Status', index: 'Status', editable: false, hidedlg: true, align: 'center', hidden: false, search: false, formatter: this.formatStatus.bind(this) },
                { name: '', index: '', hidden: false, search: false, formatter: this.formatEditButton.bind(this), align: 'center', sortable: false },
                { name: 'IsGlobalStatus', index: 'IsGlobalStatus', editable: false, hidedlg: true, hidden: true, search: false }
            ];
            var jqGridOptions = {
                datatype: "local",
                data: data.rows,
                loadonce: true,
                pager: 'listPager',
                colNames: columnNames,
                colModel: columnModel,
                rowNum: 20,
                rowList: [10, 20, 30, 60, 365]
                //, sortname: 'ActionID'
                //, sortorder: "asc"
                ,
                viewrecords: true,
                caption: 'Stage Areas',
                autoWidth: false,
                gridview: true,
                id: "RowID",
                height: "100%",
                width: null
                //, multiselect: true
                //, multiboxonly: true
                //  , toolbar: [true, "top"]
                ,
                emptyrecords: 'no records found',
                gridComplete: this.gridCompleteLocal.bind(this),
                loadComplete: this.gridLoadComplete.bind(this),
                loadError: this.gridLoadError.bind(this)
                //, ondblClickRow: this.gridDblClickRow.bind(this)
            };
            this.dsGrid.jqGrid(jqGridOptions).navGrid(this.dsGridList, {
                edit: false, add: false, del: false, search: false, refresh: false
            }, null, null, null);
            //Removing filter toolbar
            // this.dsGrid.jqGrid('filterToolbar');
        };
        waTouchContainerStatus.prototype.gridCompleteLocal = function () {
        };
        waTouchContainerStatus.prototype.gridLoadComplete = function () {
            var _this = this;
            if (this.dsGrid.getGridParam('records') === 0) {
                $('#' + this.gridId + ' tbody').html("<div style='text-align:center; padding: 30px'>No records found</div>");
            }
            else {
                $('.editContainerStatus').unbind('click').click(function (evt) {
                    evt.preventDefault();
                    var currentElement = $(evt.currentTarget);
                    var Id = currentElement.data('id');
                    var Status = currentElement.data('status');
                    var StatusCatergoryID = currentElement.data('actioncategoryid');
                    var Description = currentElement.data('description');
                    var DispActionName = currentElement.data('dispactionname');
                    var IsGlobalStatus = currentElement.data('isglobalstatus');
                    _this.showEditPopup(Id, Status, StatusCatergoryID, Description, DispActionName, IsGlobalStatus);
                });
                $('.btnActive').unbind('click').click(function (evt) {
                    evt.preventDefault();
                    var currentElement = $(evt.currentTarget);
                    var pkId = currentElement.data('value');
                    var isGlobalStatus = currentElement.data('isglobalstatus');
                    var Status = currentElement.val();
                    _this.SetActiveInactive(pkId, Status, isGlobalStatus, _this);
                });
            }
        };
        waTouchContainerStatus.prototype.gridLoadError = function (xhr, st, err) {
            if (xhr.status == "200")
                return false;
            var error = eval('(' + xhr.responseText + ')');
            $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                width: '600',
                buttons: {
                    Ok: function () {
                        //$(this).dialog("close");
                    }
                }
            });
        };
        waTouchContainerStatus.prototype.reloadDSGrid = function () {
            this.dsGrid.jqGrid("clearGridData", true);
            this.dsGrid.jqGrid('setGridParam', {
                mtype: 'GET',
                url: waTouchContainerStatusModule.URLHelper.searchContainerStatusList,
                datatype: 'json',
                postData: {
                    StatusName: $('#txtSearchStatus').val()
                }
            }).trigger("reloadGrid");
        };
        return waTouchContainerStatus;
    }());
    waTouchContainerStatusModule.waTouchContainerStatus = waTouchContainerStatus;
})(waTouchContainerStatusModule || (waTouchContainerStatusModule = {}));
$(document).ready(function () {
    var owaTouchContainerStatus = new waTouchContainerStatusModule.waTouchContainerStatus();
    owaTouchContainerStatus.init();
});
//# sourceMappingURL=waTouchContainerStatus.js.map