﻿var hdnCAPApplicationParameterId = -1;

function initControls() {
    $("#StartDateIcon").click(function () {
        $("#StartDate").focus();
    });
    $('#StartDate').datepicker({
    });

    $("#EditStartDateIcon").click(function () {
        $("#EditStartDate").focus();
    });
    $('#EditStartDate').datepicker({
    });

    $("form").on("submit", function (event) {
        event.preventDefault();

        loadASData();
    });

    $('#modalSavehours').click(function () {
        saveASData();
    });
}

function loadASData() {
    $.ajax({
        url: URLHelper.SearchApplicationSettingsUrl,
        data: {
            Date: $('#StartDate').val(),
            SortCol: $('#hidSortCol').val(),
            SortDir: $('#hidSortDir').val(),
        },
        success: function (retVal) {
            if (retVal == "error") {
                alert('Error occurred while loading information. Please reload the page and try again!');
            } else {
                $('#dvApplicationSettings').html(retVal);
            }
        },
        error: function (error) {
            alert('Error occurred while loading information. Please reload the page and try again!');
        }
    });
}

function initPopup() {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $('a[data-modal-id]').click(function (e) {
        e.preventDefault();

        $("body").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7);
        $('#popup').fadeIn();
    });

    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });
    $(window).resize(function () {
        $(".modal-box").css({
            top: 10,
            left: ($(window).width() - $(".modal-box").outerWidth()) / 2
        });
    });
    $(window).resize();
}

var ApplicationSettingsPopupSetting = {
    actions: {
        adjustCenter: function () {
        },
        adjustCenterWithDelay: function () {
            setTimeout('ApplicationSettingsPopupSetting.actions.adjustCenter()', 700);
        },
        adjustTransferDimentions: function () {
            ApplicationSettingsPopupSetting.actions.adjustCenterWithDelay();
        },
        adjustNoserviceDimentions: function () {
        },
        adjustHourChangeDimentions: function () {
            ApplicationSettingsPopupSetting.actions.adjustCenterWithDelay();

            var gridId = 'list';
            var grid = $('#gbox_' + gridId).parent();
            var gridParentWidth = grid.width();
            $('#' + gridId).jqGrid('setGridWidth', gridParentWidth);

            var gridParentHeight = grid.height();
            if (gridParentHeight <= 800) {

                var height = ($(window).height() - $(".modal-box").outerHeight()) - 100;

                $('#truck-ophours-reason-from').css('max-height', height + 'px'); //set max height
                $('#truck-ophours-reason-from').css('overflow-y', 'scroll'); //set max height
                $('#truck-ophours-reason-from').css('overflow-x', 'hidden'); //set max height
            }
        }
    }
}

function showApplicationSettingsPopup(ApplicationParameter) {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popup').fadeIn();
    $("#ddlApplicationSettings").val(ApplicationParameter);
    $('#ddlApplicationSettings').trigger('change');
    ApplicationSettingsPopupSetting.actions.adjustHourChangeDimentions();
}

function loadApplicationSettingChangeGrid(ApplicationParameter) {
    $("#list").clearGridData();

    $("#list").jqGrid('setGridParam', {
        mtype: 'GET', editurl: URLHelper.SaveTempHourChange, onclickSubmit: batchSumbit, url: URLHelper.GetApplicationSettingsListUrl, datatype: 'json'
                                , postData: {
                                    SettingName: ApplicationParameter
                                }
    }).trigger("reloadGrid");
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function initApplicationSettingsChangeGrid() {

    var checkStartEndDatevalidation = function (value, colname) {
        var startTime = $("input#Startdate").val();
        var endTime = $("input#Enddate").val();
        var applicationValue = $("input#ApplicationValue").val();
        var endDataNullChecked = $("#enddate_chkbox").is(':checked');
        var vsetting = $('#ddlApplicationSettings').val();
        if (vsetting != '') {
            if (vsetting == 'Facility_AM_End_Time' || vsetting == 'FacilityAMTime' || vsetting == 'FacilityPMTime') {
                var rege = /^(1[0-2]|0?[1-9]):([0-5]?[0-9])( ?[AP]M)?$/;
                //var rege = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
                if (!rege.test(applicationValue)) { return [false, "ApplicationValue: Valid time(Format like: 08:00 AM/PM).", ""]; }
            }
            else if (vsetting == 'SiteLinkMileageAdjustment') {
                if (!isNumeric(applicationValue)) { return [false, "ApplicationValue: Enter valid value.", ""]; }
                var deci2 = parseFloat(applicationValue).toFixed(2);
                $("input#ApplicationValue").val(deci2);
            }
            else {
                if (!isNumeric(applicationValue)) { return [false, "ApplicationValue: Enter valid value.", ""]; }
            }
        }
        if (endDataNullChecked == false) {
            if (endTime == '') {
                return [false, "End Date: Field is required.", ""];
            } else if (startTime != '' && endTime != '') {
                if (new Date(startTime) > new Date(endTime)) {
                    return [false, "Start date should be less than End date.", ""];
                }
            }
        }

        return [true, "", ""];
    };

    var updateDialog = {
        url: URLHelper.SaveTempHourChange
                , closeAfterEdit: true
                , reloadAfterSubmit: true
                , closeAfterAdd: true
                , closeAfterEdit: true
                , onclickSubmit: function (params) {
                    var ajaxData = {};
                    var list = $("#list");
                    var selectedRow = list.getGridParam("selrow");

                    rowData = list.getRowData(selectedRow);

                    ajaxData = { dates: jQuery("#list").jqGrid('getGridParam', 'selarrrow'), CalDate: rowData.CalDate };

                    return ajaxData;
                }
                , afterComplete: function (o) {
                    json = jQuery.parseJSON(o.responseText);
                    if (json.success == true)
                        alert('Truck operating details have been saved successfully.');
                    loadDataWithGridColumnHeaderAndData();
                }
                , modal: true
                , width: "400"
    };

    $("#list").jqGrid({
        colNames: ['RowID', 'ApplicationValue', 'Start Date', 'End Date', 'ValueUnit', 'ApplicationParameter', ''],
        colModel: [  //editable: false, hidedlg: true, hidden: true ,
              { name: 'RowID', index: 'RowID', key: true, editable: false, hidedlg: true, search: false, hidden: true },
              { name: 'ApplicationValue', index: 'ApplicationValue', width: 200, editable: true, editrules: { required: true }, hidedlg: true, search: false, align: 'center' },
              {
                  name: 'Startdate', index: 'Startdate', width: 170, editable: true, align: 'center', editrules: { custom: true, custom_func: checkStartEndDatevalidation, required: true }, hidedlg: true, search: false,
                  editoptions: {
                      size: 20,
                      dataInit: function (el) {
                          $(el).datepicker({ dateFormat: 'm/d/yy', minDate: new Date() });
                      },
                      defaultValue: function () {
                          var currentTime = new Date();
                          var month = parseInt(currentTime.getMonth() + 1);
                          var day = currentTime.getDate();
                          var year = currentTime.getFullYear();
                          return month + "/" + day + "/" + year;
                      },
                      readonly: true
                  }
              },
                {
                    name: 'Enddate', index: 'Enddate', width: 170, editable: true, align: 'center', editrules: { custom: true, custom_func: checkStartEndDatevalidation }, hidedlg: true, search: false,
                    editoptions: {
                        size: 20,
                        dataInit: function (el) {
                            $(el).datepicker({ dateFormat: 'm/d/yy', minDate: new Date() });
                        },
                        defaultValue: function () {
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            var day = currentTime.getDate();
                            var year = currentTime.getFullYear();
                            return month + "/" + day + "/" + year;
                        },
                        readonly: true
                    }
                },
                { name: 'ValueUnit', index: 'ValueUnit', width: 170, search: false, align: 'center' },
                { name: 'ApplicationParameter', index: 'ApplicationParameter', hidden: true, search: false },
                { name: 'Action', index: 'Action', hidden: false, search: false, formatter: formatActionButton('RowID') }
        ]
            , pager: $('#listPager')
            , width: 700
            , rowNum: 10
            , rowList: [10, 20, 30, 60, 365]
            , sortname: 'Startdate'
            , sortorder: "desc"
            , viewrecords: true
            , autoencode: true
            , caption: 'Application Setting'
            , autowidth: false
            , gridview: true
            , id: "RowID"
            , height: "100%"
            , datatype: 'local'
            , loadError: function (xhr, st, err) {
                if (xhr.status == "200") return false;
                var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                    modal: false,
                    title: 'Error',
                    maxwidth: '600',
                    width: '600',
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
            , gridComplete: function () {
                var ids = $("#list").jqGrid('getDataIDs');
                var CurDate = new Date();

                for (var i = 0; i < ids.length; i++) {
                    var rowId = ids[i],
                        endDT = $("#list").jqGrid('getCell', rowId, 'Enddate'),
                        activeBtn = "<input type='button' style='background-color: #008287; border-radius: 4px; color: white;' value='Edit' onclick='showEditPopup(" + rowId + ")' />";

                    if ((isFullAccess == "False") || (endDT != '' && CurDate > new Date(endDT))) {
                        activeBtn = "<input type='button' style='background-color: ##8289; border-radius: 4px; color: white;' value='Edit' disabled onclick='showEditPopup(" + rowId + ")' />";
                    }

                    /*if (isFullAccess == "False") {
                        activeBtn = "<input type='button' style='background-color: ##8289; border-radius: 4px; color: white;' value='Edit' disabled onclick='showEditPopup(" + rowId + ")' />";
                    }

                    if (endDT != '' && CurDate > new Date(endDT)) { // Inactive
                        activeBtn = "<input type='button' style='background-color: ##8289; border-radius: 4px; color: white;' value='Edit' disabled onclick='showEditPopup(" + rowId + ")' />";
                    }*/

                    $("#list").jqGrid('setRowData', rowId, { Action: activeBtn });
                };

                $(".ui-jqgrid-sortable").each(function () {
                    this.style.color = "#0073ea";
                });
            }
    }).navGrid('#listPager',
        {
            edit: false, add: false, del: false, refresh: false, search: false
        },
        updateDialog,
        null,
        null);

    jQuery("#modalUpdateselectedhours").click(function () {
        showEditPopup();
    });

    //jQuery("#UpdateCorporateDefaults").click(function () {
    //    showDefaultPopup();
    //});
}

function formatActionButton(cellvalue, options, rowObject) {
    return "<input type='button' value='Edit' onclick='showEditPopup(); return false;' style='margin: 2px;' />";
}

function showEditPopup(rowId) {
    if ($('#ddlApplicationSettings').val() != -1) {
        var rows = [];//jQuery("#list").jqGrid('getGridParam', 'selarrrow');
        rows.push(rowId);
        $('#Startdate').val('');
        $('#Enddate').val('');
        $('#ApplicationValue').val('');
        $('#enddate_chkbox').attr('checked', false);
        jQuery("#list").jqGrid('editGridRow', rows, {
            height: 240,
            closeAfterEdit: true,
            closeAfterAdd: true,
            reloadAfterSubmit: true,
            beforeShowForm: function (formid) {
                var chkboxexists = $('#Enddate').parent().find('#enddate_chkbox');
                if (chkboxexists.length == 0) {
                    var chkbox = "<input type='checkbox' style='margin-left: 5px;' id='enddate_chkbox' >";
                    $('#Enddate').parent().append(chkbox);

                    $('#enddate_chkbox').on('change', function () { // on change of state
                        if (this.checked) // if changed state is "CHECKED"
                        {
                            $('#Enddate').attr("disabled", "disabled");
                            $('#Enddate').val('');
                        } else {
                            $('#Enddate').removeAttr("disabled");
                        }
                    })
                }
            },
            afterShowForm: function (formid) {
                $(".EditButton").css('text-align', 'left');
                $(".navButton").css('display', 'none');
                $("#editmodlist").css('height', '240px');
                var Cdt = new Date();
                var StrDt = $('#Startdate').val();
                if (Cdt > new Date(StrDt)) {
                    var MM = Cdt.getMonth() + 1;
                    var dd = Cdt.getDate();
                    var yy = Cdt.getFullYear();
                    $('#Startdate').val([MM, dd, yy].join('/'));
                }
            }

            , onclickSubmit: batchSumbit
            , url: URLHelper.SaveApplicationSettingsChange
            , afterComplete: function (o) {
                var json = jQuery.parseJSON(o.responseText);
                if (json.success == true)
                    loadASData();
            }
        });
    } else alert("Please Select Application Settings.");
}

var batchSumbit = function (params) {
    var ajaxData = {};
    var data;
    //var rowids = jQuery("#list").jqGrid('getGridParam', 'selarrrow');

    //if (rowids != null) {
    return ajaxData = { hidSettingname: $('#ddlApplicationSettings').val(), hidNullEndDate: $("#enddate_chkbox").is(':checked') };
    //}
}


function SortColumn(ColumnName) {
    $.ajaxSetup({ cache: false });
    if (ColumnName == "")
        ColumnName = 'ApplicationParameter';
    $("#hidSortCol").val(ColumnName);
    var sortDir = $("#hidSortDir").val();
    $('img[id^="Image"]').css('display', 'none');
    if (sortDir.toLowerCase() == "asc") {
        $("#hidSortDir").val("DESC");
        $('#Image' + ColumnName).attr('src', "/Images/arrow-down.png");
        $('#Image' + ColumnName).css('display', 'inline-block');
    }
    else {
        $("#hidSortDir").val("ASC");
        $('#Image' + ColumnName).attr('src', "/Images/arrow-up.png");
        $('#Image' + ColumnName).css('display', 'inline-block');
    }
    loadASData();
}

$(document).ready(function () {
    $.ajaxSetup({ cache: false });

    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    //loadASData()

    initApplicationSettingsChangeGrid();

    initControls();

    initPopup();

    SortColumn('');

});

Date.prototype.ToDate = function () {
    return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
}

Date.prototype.ToTime = function () {
    var hr = parseInt(this.getHours());
    var ampm = hr > 11 ? 'PM' : 'AM';
    var hr = hr > 12 ? (hr - 12) : hr;
    var mm = parseInt(this.getMinutes()) > 9 ? this.getMinutes() : '0' + this.getMinutes();
    return hr + ":" + mm + " " + ampm;
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}