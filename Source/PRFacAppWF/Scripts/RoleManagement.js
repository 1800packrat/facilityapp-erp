﻿$(document).ready(function () {

    initRolesGrid();

    bindAjaxStartEvent();

    $('#filterJqGrid').click(function (e) {
        e.preventDefault();
        filterGrid();
    });   
});

function bindAjaxStartEvent() {
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
}

function initRolesGrid() {
    $("#tblLstRoles").jqGrid({
        url: URLHelper.getUserList,
        mtype: 'GET',
        datatype: 'json',
        contentType: "application/json; charset-utf-8",
        colNames: ['RoleId', 'Role Name', 'Description', 'Status'],
        colModel: [
                    { name: 'RoleId', index: 'RoleId', key: true, editable: false, hidedlg: true, hidden: true },
                    { name: 'GroupName', index: 'GroupName', width: 100, editable: true, align: 'left' },
                    { name: 'Description', index: 'Description', width: 100, editable: true, align: 'left', sortable: true },
                    { name: 'Status', index: 'Status', width: 50, hidden: false, search: false, sortable: false, align: 'center', formatter: formatActionButton }],
        pager: $('#ptblLstRoles'),
        width: 800,
        rowNum: 20,
        rowList: [10, 20, 30, 60, 365],
        sortname: 'GroupName',
        sortorder: "asc",
        viewrecords: true,
        //caption: 'Users List',
        autowidth: true,
        gridview: true,
        id: "RoleId",
        height: "100%",
        multiselect: false,
        multiboxonly: true,
        loadonce: false, //when search is implementing from server side
        // datatype: 'local',
        loadError: function (xhr, st, err) {
            if (xhr.status == "200") return false;
            var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                maxwidth: '600',
                width: '600',
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        },
        gridComplete: function () {
            if (isFullAccess == "False") {
                $('#tblLstRoles').find('#btnActive').each(function () { $(this).attr('disabled', 'disabled'); });
            }
        },
        ondblClickRow: function (rowid, iRow, iCol, e) {
        }
    }).navGrid('#ptblLstRoles', { edit: false, add: false, del: false, search: false, refresh: false }, null, null, null);
}

function filterGrid() {
    $("#tblLstRoles").jqGrid('clearGridData');

    $("#tblLstRoles").jqGrid('setGridParam', { page: 1, postData: { roleName: $('#txtRoleName').val() } }).trigger("reloadGrid");
}

function formatActionButton(cellvalue, options, rowObject) {
    return "<input id='btnActive' type='button' value='" + cellvalue + "' onclick='SetActiveInactive(" + rowObject[0] + ",\"" + rowObject[1] + "\", this.value); return false;' style='margin: 2px;' /> <input type='button' id='editRole' value='Edit' onclick='editPage(true, " + rowObject[0] + "); return false;'/>";
}

function SetActiveInactive(RoleId, RoleName, Status) {
    if (RoleName == isDriverDefaultRoleGroupName) {
        alert("The Driver Role can not be disabled as drivers would be unable to access Driver app. Please contact IT to make arrangements for your request.");
    }
    else {
        var result = confirm("Are you sure you want to make it " + Status + " ?");
        if (result == true) {
            $.ajax({
                url: URLHelper.SetRoleActiveInactiveUrl,
                data: {
                    RoleId: RoleId,
                    status: Status
                },
                success: function (retVal) {
                    if (retVal == "error") {
                        alert('Error occurred while loading information. Please reload the page and try again!');
                    }
                    else {
                        $("#tblLstRoles").trigger("reloadGrid");
                    }
                },
                error: function (error) {
                    alert('Error occurred while loading information. Please reload the page and try again!');
                }
            });
        }
    }
}

function editPage(isEdit, RoleId) {
    window.location.href = "CreateUpdateRole/?RoleId=" + RoleId;
}

function initPopup() {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popup').fadeIn();

    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });
    $(window).resize(function () {
        $(".modal-box").css({
            top: 50,
            left: ($(window).width() - $(".modal-box").outerWidth()) / 2
        });
    });
    $(window).resize();
}

function resetform() {
    $('#txtRoleName').val('');
    $("#tblLstRoles").jqGrid('clearGridData');

    $("#tblLstRoles").jqGrid('setGridParam', { page: 1, postData: { roleName: '' } }).trigger("reloadGrid");
}