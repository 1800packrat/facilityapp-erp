﻿
function initControls() {
    $("#StartDateIcon").click(function () {
        $("#StartDate").focus();
    });
    $('#StartDate').datepicker({
    });

    $("#StartDateIconJQ").click(function () {
        $("#StartDateJQ").focus();
    });
    $('#StartDateJQ').datepicker({
    });

    $("#EditStartDateIcon").click(function () {
        $("#EditStartDate").focus();
    });
    $('#EditStartDate').datepicker({
    });

    $("form").on("submit", function (event) {
        event.preventDefault();
        loadTouchTimeListData();
    });

    $('#modalSavehours').click(function () {
        saveASData();
    });
}

function loadTouchTimeListData() {
    $.ajax({
        url: URLHelper.SearchTouchTimeUrl,
        data: {
            Date: $('#StartDate').val(),
            SortCol: $('#hidSortCol').val(),
            SortDir: $('#hidSortDir').val(),
        },
        success: function (retVal) {
            if (retVal == "error") {
                alert('Error occurred while loading information. Please reload the page and try again!');
            } else {
                //$('#divTouchtime').html(retVal);
                //Remove previousloaded values
                $('#hdCorpRow').remove();

                //Append new result to UI
                $('#divTouchtime').html(retVal);

                //Find corp head 
                var corpHead = $('#hdCorpRow', $('#divTouchtime'));
                $('#tblHead').append(corpHead);
                $('#hdCorpRow', $('#divTouchtime')).remove();

                //Bind table
                $("#fixTable").tableHeadFixer();
            }
        },
        error: function (error) {
            alert('Error occurred while loading information. Please reload the page and try again!');
        }
    });
}


function initPopup() {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $('a[data-modal-id]').click(function (e) {
        e.preventDefault();

        $("body").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7);
        $('#popup').fadeIn();
    });

    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });
    $(window).resize(function () {
        $(".modal-box").css({
            top: 10,
            left: ($(window).width() - $(".modal-box").outerWidth()) / 2
        });
    });
    $(window).resize();
}

var touchTimePopupSetting = {
    actions: {
        adjustCenter: function () {
            /* $(".modal-box").css({
                 //top: top,
                 //left: left
                 top: (($(window).height() / 2) - ($(".modal-box").outerHeight() / 2)) + $(window).scrollTop(),
                 left: (($(window).width() / 2) - ($(".modal-box").outerWidth() / 2))
             });*/
        },
        adjustCenterWithDelay: function () {
            setTimeout('touchTimePopupSetting.actions.adjustCenter()', 700);
        },
        adjustTransferDimentions: function () {
            touchTimePopupSetting.actions.adjustCenterWithDelay();
        },
        adjustNoserviceDimentions: function () {
        },
        adjustHourChangeDimentions: function () {
            touchTimePopupSetting.actions.adjustCenterWithDelay();

            var gridId = 'list';
            var grid = $('#gbox_' + gridId).parent();
            var gridParentWidth = grid.width();
            $('#' + gridId).jqGrid('setGridWidth', gridParentWidth);

            var gridParentHeight = grid.height();
            if (gridParentHeight <= 800) {

                var height = ($(window).height() - $(".modal-box").outerHeight()) - 100;

                $('#truck-ophours-reason-from').css('max-height', height + 'px'); //set max height
                $('#truck-ophours-reason-from').css('overflow-y', 'scroll'); //set max height
                $('#truck-ophours-reason-from').css('overflow-x', 'hidden'); //set max height
            }
        }
    }
}

function showTouchTimePopup(facilityId) {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popup').fadeIn();
    $("#corporateDiv").css('display', 'none');
    $('#dropdownDiv').removeAttr("style");
    $("#ddlFacilityDetails option[value='0']").remove();
    $("#ddlFacilityDetails").val(facilityId);
    $('#ddlFacilityDetails').trigger('change');
    touchTimePopupSetting.actions.adjustHourChangeDimentions();
}

function showCorporatePopup(facilityId) {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popup').fadeIn();
    $("#ddlFacilityDetails").append($('<option></option>').val(facilityId).html(''));
    $("#ddlFacilityDetails").val(facilityId);
    $("#dropdownDiv").css('display', 'none');
    $('#corporateDiv').removeAttr("style");
    loadTouchTimeChangeGrid(facilityId);
    SLMileagePopupSetting.actions.adjustHourChangeDimentions();
}

function loadTouchTimeChangeGrid(facilityId) {

    $("#list").clearGridData();
    if (facilityId == 0)
        $("#UpdateCorporateDefaults").css('display', 'none');
    else
        $("#UpdateCorporateDefaults").css('display', 'block');

    $("#list").jqGrid('setGridParam', {
        mtype: 'GET', editurl: URLHelper.SaveTempHourChange, onclickSubmit: batchSumbit, url: URLHelper.GetFacilityTouchTimeListUrl, datatype: 'json'
                                , postData: {
                                    facilityId: facilityId
                                }
    }).trigger("reloadGrid");
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n) && n % 1 === 0;
}

function initTouchTimeChangeGrid() {

    var checkStartEndDatevalidation = function (value, colname) {
        var startTime = $("input#Startdate").val();
        var endTime = $("input#Enddate").val();
        //alert($("#enddate_chkbox").is(':checked'));
        var endDataNullChecked = $("#enddate_chkbox").is(':checked');
        var vValue = $('input#DE').val() + $('input#DF').val() + $('input#RE').val() + $('input#RF').val() + $('input#IBO').val() + $('input#OBO').val() + $('input#WA').val() + $('input#CC').val();
        if (vValue != '') {
            if (!isNumeric(vValue)) { return [false, "Enter valid data.", ""]; }
        }
        if (endDataNullChecked == false) {
            if (endTime == '') {
                return [false, "End Date: Field is required.", ""];
            } else if (startTime != '' && endTime != '') {
                if (new Date(startTime) > new Date(endTime)) {
                    return [false, "Start date should be less than End date.", ""];
                }
            }
        }

        return [true, "", ""];
    };

    var updateDialog = {
        url: URLHelper.SaveTempHourChange
                , closeAfterEdit: true
                , reloadAfterSubmit: true
                , closeAfterAdd: true
                , closeAfterEdit: true
                , onclickSubmit: function (params) {
                    var ajaxData = {};
                    var list = $("#list");
                    var selectedRow = list.getGridParam("selrow");

                    rowData = list.getRowData(selectedRow);

                    ajaxData = { dates: jQuery("#list").jqGrid('getGridParam', 'selarrrow'), CalDate: rowData.CalDate };

                    return ajaxData;
                }
                , afterComplete: function (o) {
                    json = jQuery.parseJSON(o.responseText);
                    if (json.success == true)
                        alert('Truck operating details have been saved successfully.');
                    loadDataWithGridColumnHeaderAndData();
                }
                , modal: true
                , width: "240"
    };

    $("#list").jqGrid({
        colNames: ['RowID', 'DE', 'DF', 'RE', 'RF', 'IBO', 'OBO', 'WA', 'CC', 'Start Date', 'End Date', 'State', 'Facility Id', ''],
        colModel: [  //editable: false, hidedlg: true, hidden: true ,
                { name: 'RowID', index: 'RowID', key: true, editable: false, hidedlg: true, search: false, hidden: true },
                { name: 'DE', index: 'DE', width: 100, editable: true, editrules: { required: true }, hidedlg: true, align: 'center', search: false },//formoptions: { rowpos: 1, colpos: 1 },
                { name: 'DF', index: 'DF', width: 100, editable: true, editrules: { required: true }, hidedlg: true, align: 'center', search: false },//formoptions: { rowpos: 1, colpos: 2 },
                { name: 'RE', index: 'RE', width: 100, editable: true, editrules: { required: true }, hidedlg: true, align: 'center', search: false },//formoptions: { rowpos: 2, colpos: 1 },
                { name: 'RF', index: 'RF', width: 100, editable: true, editrules: { required: true }, hidedlg: true, align: 'center', search: false },//formoptions: { rowpos: 2, colpos: 2 },
                { name: 'IBO', index: 'IBO', width: 100, editable: true, editrules: { required: true }, hidedlg: true, align: 'center', search: false },//formoptions: { rowpos: 3, colpos: 1 }, 
                { name: 'OBO', index: 'OBO', width: 100, editable: true, editrules: { required: true }, hidedlg: true, align: 'center', search: false },//formoptions: { rowpos: 3, colpos: 2 },
                { name: 'WA', index: 'WA', width: 100, editable: true, editrules: { required: true }, hidedlg: true, align: 'center', search: false },//formoptions: { rowpos: 4, colpos: 1 }, 
                { name: 'CC', index: 'CC', width: 100, editable: true, editrules: { required: true }, hidedlg: true, align: 'center', search: false },//formoptions: { rowpos: 4, colpos: 2 }, 
                {
                    name: 'Startdate', index: 'Startdate', width: 140, editable: true, align: 'center', editrules: { custom: true, custom_func: checkStartEndDatevalidation, required: true }, hidedlg: true, search: false, //formoptions: { rowpos: 5, colpos: 1 }, 
                    editoptions: {
                        size: 20,
                        dataInit: function (el) {
                            $(el).datepicker({ dateFormat: 'm/d/yy', minDate: new Date() });
                        },
                        defaultValue: function () {
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            var day = currentTime.getDate();
                            var year = currentTime.getFullYear();
                            return month + "/" + day + "/" + year;
                        },
                        readonly: true
                    }
                },
                {
                    name: 'Enddate', index: 'Enddate', width: 140, editable: true, align: 'center', editrules: { custom: true, custom_func: checkStartEndDatevalidation }, hidedlg: true, search: false, //formoptions: { rowpos: 5, colpos: 2 },
                    editoptions: {
                        size: 20,
                        dataInit: function (el) {
                            $(el).datepicker({ dateFormat: 'm/d/yy', minDate: new Date() });
                        },
                        defaultValue: function () {
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            var day = currentTime.getDate();
                            var year = currentTime.getFullYear();
                            return month + "/" + day + "/" + year;
                        },
                        readonly: true
                    }
                },
                { name: 'State', index: 'State', hidden: true, search: false },
                { name: 'FacilityId', index: 'FacilityId', hidden: true, search: false },
                { name: 'Action', index: 'Action', hidden: false, search: false, formatter: formatActionButton('RowID') }
        ]
            , pager: $('#listPager')
            , width: 910
            , rowNum: 10
            , rowList: [10, 20, 30, 60, 365]
            , sortname: 'Startdate'
            , sortorder: "desc"
            , viewrecords: true
            , autoencode: true
            , caption: 'Touch Time'
            , autowidth: false
            , gridview: true
            , id: "RowID"
            , height: "100%"
        //, multiselect: true
        //, toolbar: [true, "top"]
            , datatype: 'local'
            , loadError: function (xhr, st, err) {
                if (xhr.status == "200") return false;
                var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                    modal: false,
                    title: 'Error',
                    maxwidth: '600',
                    width: '600',
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
            , gridComplete: function () {
                var ids = $("#list").jqGrid('getDataIDs');
                var CurDate = new Date().withoutTime();
                for (var i = 0; i < ids.length; i++) {
                    var rowId = ids[i],
                     endDT = $("#list").jqGrid('getCell', rowId, 'Enddate'),
                    activeBtn = "<input type='button' class='row-editbutton' style='background-color: #008287; border-radius: 4px; color: white;' value='Edit' onclick='showEditPopup(" + rowId + ")' />";

                     if (endDT != '' && CurDate > new Date(endDT).withoutTime()) { // Inactive
                         activeBtn = "<input type='button' style='background-color: ##8289; border-radius: 4px; color: white;' value='Edit' disabled onclick='showEditPopup(" + rowId + ")' />";
                     }
                    $("#list").jqGrid('setRowData', rowId, { Action: activeBtn });
                };
                $(".ui-jqgrid-sortable").each(function () {
                    this.style.color = "#0073ea";
                });
            },
            loadComplete: function (dt) {
                if (dt.isfullaccess == false) {
                    $('#list').find('.row-editbutton').each(function () { $(this).prop('disabled', 'disabled'); });
                    $('#modalUpdateselectedhours').addClass('not-active');
                    $("#UpdateCorporateDefaults").addClass('not-active');
                } else {
                    //$('#list').find('.row-editbutton').removeAttr("disabled");
                    $('#list').find('.row-editbutton').each(function () { $(this).removeAttr('disabled'); });
                    $('#modalUpdateselectedhours').removeClass('not-active');
                    $("#UpdateCorporateDefaults").removeClass('not-active');
                }
            }
    }).navGrid('#listPager',
            {
                edit: false, add: false, del: false, refresh: false, search: false
            },
            updateDialog,
            null,
            null);

    jQuery("#modalUpdateselectedhours").click(function () {
        showEditPopup();
    });
    jQuery("#UpdateCorporateDefaults").click(function () {
        showDefaultPopup();
    });
}

function formatActionButton(cellvalue, options, rowObject) {
    return "<input type='button' value='Edit' onclick='showEditPopup(); return false;' style='margin: 2px;' />";
}

function showDefaultPopup() {
    if ($('#ddlFacilityDetails').val() != -1) {
        jQuery("#list").jqGrid('editGridRow', "new", {
            height: 240
            , closeAfterEdit: true
            , closeAfterAdd: true
            , reloadAfterSubmit: true
            , afterShowForm: function (formid) {
                $(".EditButton").css('text-align', 'left');
                $(".navButton").css('display', 'none');
                $("#editmodlist").css('height', '150px');
            }
            , onclickSubmit: DefaultSubmit
            , url: URLHelper.SetDefaultValueURL
            , afterComplete: function (o) {
                var json = jQuery.parseJSON(o.responseText);
                if (json.success == true)
                    loadTouchTimeListData();
            }
        });
        $('#DE').val('0');
        $('#DF').val('0');
        $('#RE').val('0');
        $('#RF').val('0');
        $('#CC').val('0');
        $('#WA').val('0');
        $('#IBO').val('0');
        $('#OBO').val('0');
        $('#Enddate').val('9999-01-01');
        $('#tr_DE').css('display', 'none');
        $('#tr_DF').css('display', 'none');
        $('#tr_RE').css('display', 'none');
        $('#tr_RF').css('display', 'none');
        $('#tr_CC').css('display', 'none');
        $('#tr_WA').css('display', 'none');
        $('#tr_IBO').css('display', 'none');
        $('#tr_OBO').css('display', 'none');
        $('#tr_Enddate').css('display', 'none');
        $("#edithdlist span").text("Set to Corporate Default");
    } else alert("Please select a Facility");
}

function showEditPopup(rowId) {
    $('#DE').val('');
    $('#DF').val('');
    $('#RE').val('');
    $('#RF').val('');
    $('#CC').val('');
    $('#WA').val('');
    $('#IBO').val('');
    $('#OBO').val('');
    if ($('#ddlFacilityDetails').val() != -1) {
        var rows = [];//jQuery("#list").jqGrid('getGridParam', 'selarrrow');        
        rows.push(rowId);
        $('#tr_DE').removeAttr("style");
        $('#tr_DF').removeAttr("style");
        $('#tr_RE').removeAttr("style");
        $('#tr_RF').removeAttr("style");
        $('#tr_CC').removeAttr("style");
        $('#tr_WA').removeAttr("style");
        $('#tr_IBO').removeAttr("style");
        $('#tr_OBO').removeAttr("style");
        $('#tr_Enddate').removeAttr("style");
        $('#Startdate').val('');
        $('#Enddate').val('');
        $('#enddate_chkbox').attr('checked', false);
        jQuery("#list").jqGrid('editGridRow', rows, {
            height: 280,
            width: 240,
            top: 70,
            left: 280,
            closeAfterEdit: true,
            closeAfterAdd: true,
            reloadAfterSubmit: true,
            beforeShowForm: function (formid) {
                var chkboxexists = $('#Enddate').parent().find('#enddate_chkbox');
                if (chkboxexists.length == 0) {
                    var chkbox = "<input type='checkbox' title='Select for open ended' style='margin-left: 5px;' id='enddate_chkbox' >";
                    $('#Enddate').parent().append(chkbox);

                    $('#enddate_chkbox').on('change', function () { // on change of state
                        if (this.checked) // if changed state is "CHECKED"
                        {
                            $('#Enddate').attr("disabled", "disabled");
                            $('#Enddate').val('');
                        } else {
                            $('#Enddate').removeAttr("disabled");
                        }
                    })
                }
            },
            afterShowForm: function (formid) {
                $(".EditButton").css('text-align', 'left');
                $(".navButton").css('display', 'none');
                $("#editmodlist").css('height', '530px');
                $("#editmodlist").css('width', '280px');
                var Cdt = new Date();
                var StrDt = $('#Startdate').val();
                if (Cdt > new Date(StrDt)) {
                    var MM = Cdt.getMonth() + 1;
                    var dd = Cdt.getDate();
                    var yy = Cdt.getFullYear();
                    $('#Startdate').val([MM, dd, yy].join('/'));
                }
            }

            , onclickSubmit: batchSumbit
            , url: URLHelper.SaveTouchTime
            , afterComplete: function (o) {
                var json = jQuery.parseJSON(o.responseText);
                if (json.success == true)
                    loadTouchTimeListData();
            }
        });
    } else alert("Please select a Facility");
}

var batchSumbit = function (params) {
    var ajaxData = {};
    var data;
    //var rowids = jQuery("#list").jqGrid('getGridParam', 'selarrrow');

    //if (rowids != null) {
    return ajaxData = { hidfacilityid: $('#ddlFacilityDetails').val(), hidNullEndDate: $("#enddate_chkbox").is(':checked') };
    //}
}

var DefaultSubmit = function (params) {
    var ajaxData = {};
    var data;
    return ajaxData = { hidfacilityid: $('#ddlFacilityDetails').val() };
}

function SortColumn(ColumnName) {
    $.ajaxSetup({ cache: false });
    if (ColumnName == "")
        ColumnName = 'Facility';
    $("#hidSortCol").val(ColumnName);
    var sortDir = $("#hidSortDir").val();
    $('img[id^="Image"]').css('display', 'none');
    if (sortDir.toLowerCase() == "asc") {
        $("#hidSortDir").val("DESC");
        $('#Image' + ColumnName).attr('src', "/Images/arrow-down.png");
        $('#Image' + ColumnName).css('display', 'inline-block');
    }
    else {
        $("#hidSortDir").val("ASC");
        $('#Image' + ColumnName).attr('src', "/Images/arrow-up.png");
        $('#Image' + ColumnName).css('display', 'inline-block');
    }
    loadTouchTimeListData();
}

$(document).ready(function () {
    $.ajaxSetup({ cache: false });

    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    //loadTouchTimeListData();

    initControls();

    initTouchTimeChangeGrid();

    initPopup();

    SortColumn('');

    $("#fixTable").tableHeadFixer();
});

Date.prototype.ToDate = function () {
    return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
}

Date.prototype.ToTime = function () {
    var hr = parseInt(this.getHours());
    var ampm = hr > 11 ? 'PM' : 'AM';
    var hr = hr > 12 ? (hr - 12) : hr;
    var mm = parseInt(this.getMinutes()) > 9 ? this.getMinutes() : '0' + this.getMinutes();
    return hr + ":" + mm + " " + ampm;
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

Date.prototype.withoutTime = function () {
    var d = new Date(this);
    d.setHours(0, 0, 0, 0, 0);
    return d
}