﻿//list scripts
// SFERP-TODO-CTRMV -- Remove this page completely --TG-562

var err = {
    TodateGrater: 'To Date cannot be before From Date',
    RequireTodate: 'Must enter From and To Dates'
};

$("form").on("submit", function (event) {
    event.preventDefault();
    if (ValidateDate($('#FromDate').val(), $('#ToDate').val()) == false)
        return false;

    SearchUnitMaintenance($(this).serialize());
});

function ValidateDate(fromDate, toDate) {
    if (fromDate.length > 0 && toDate.length > 0 && new Date(fromDate) > new Date(toDate)) {
        $('#errToDate').html(err.TodateGrater);
        $('#errToDate').show();

        return false;
    } else if ((fromDate.length == 0 && toDate.length > 0) || (fromDate.length > 0 && toDate.length == 0)) {
        $('#errToDate').html(err.RequireTodate);
        $('#errToDate').show();
        return false;
    } else
        $('#errToDate').hide();
    return true;
}
function getFormData() {
    var ob = {
        FromDate: $('#FromDate').val(),
        ToDate: $('#ToDate').val(),
        UnitNumber: $('#UnitNumber').val(),
        UnitSize: $('#UnitSize').val(),
        Location: $('#Location').val(),
        UnitSize: $('#UnitSize').val(),
        TotalLoss: $('#TotalLoss').val(),
        Rentable: $('#Rentable').val(),
        PartType: $('#PartType').val(),
        OnlyMaintenanceRec: $('#OnlyMaintenanceRec').is(':checked'),
        PageNo: 1,
        RecordCntperPage: 1000000
    };
    return ob;
}

function SearchUnitMaintenance(formdata) {

    $('#progress').show();

    jQuery('#tblUnitMaintenaceList').trigger("reloadGrid");
}

function expandCollapseGroups(expandAll) {
    var $grid = $("#tblUnitMaintenaceList");
    var idPrefix = $grid[0].id + "ghead_0_", trspans;
    var groups = $grid[0].p.groupingView.groups;
    if ($grid[0].p.grouping) {
        for (var index = 0; index < groups.length; index++) {
            if (expandAll) {
                trspans = $("#" + idPrefix + index + " span.tree-wrap-" + $grid[0].p.direction + "." + $grid[0].p.groupingView.plusicon);
            } else {
                trspans = $("#" + idPrefix + index + " span.tree-wrap-" + $grid[0].p.direction + "." + $grid[0].p.groupingView.minusicon);
            }
            if (trspans.length > 0) {
                $grid.jqGrid('groupingToggle', idPrefix + index);
            }
        }
    }
}

function ExportUnitMaintenanceToExcel() {
    var ob =
        "FromDate=" + $('#FromDate').val() + "&ToDate=" + $('#ToDate').val() + "&UnitNumber=" + $('#UnitNumber').val() + "&UnitSize=" + $('#UnitSize').val() + "&Location=" + $('#Location').val() + "&UnitSize=" + $('#UnitSize').val() + "&TotalLoss=" + $('#TotalLoss').val() + "&Rentable=" + $('#Rentable').val() + "&PartType=" + $('#PartType').val() + "&OnlyMaintenanceRec=" + $('#OnlyMaintenanceRec').is(':checked');

    window.open("/UnitMaintenance/ExportUnitMaintenance?" + ob, "_blank");
}

function formatTLR(cellvalue, options, rowObject) {
    return cellvalue == true ? "Y" : "N";
}

$(document).ready(function () {
    $("#FromDateIcon").click(function () {
        $("#FromDate").focus();
    });
    $("#ToDateIcon").click(function () {
        $("#ToDate").focus();
    });
    $('#FromDate').datepicker({
        maxDate: 0,
        onSelect: function (date) {
            return ValidateDate(date, $('#ToDate').val());
        }
    });
    $('#ToDate').datepicker({
        maxDate: 0,
        onSelect: function (date) {
            return ValidateDate($('#FromDate').val(), date);
        }
    });
    $('#progress').show();

    $('#UnitNumber').autocomplete(
    {
        source: function (request, response) {
            //UnitNumberOnBlurEventFire = true;
            $('#progress').show();
            $.ajax({
                url: "/UnitMaintenance/UnitsSearch",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.UnitName, id: item.UMUnitId, unitSize: item.UnitSize, value: item.UnitName, location: item.Location, facilityName: item.FacilityName };
                    }))
                    $('#progress').hide();
                },
                error: function () {
                    $('#progress').hide();
                }
            });
        },
        select: function (event, ui) {
        }
    });

    jQuery('#tblUnitMaintenaceList').jqGrid({
        url: URLHelper.searchUnitMaintenanceList,
        datatype: 'json',
        mtype: 'POST', //
        colNames: ['UnitMaintenanceDetailId', 'Unit Number', 'Entry Date', 'Size', 'Total Loss', 'Rentable', 'Location', 'PartType', '#Parts', 'Comments', 'Entered By', 'Facility Name'],
        colModel: [
                    { name: 'UnitMaintenanceDetailId', index: 'UnitMaintenanceDetailId', hidden: true, key: true },
                    { name: 'UnitName', index: 'UnitName', width: 85, sortable: false },
                    { name: 'EntryDateDisp', index: 'EntryDateDisp', align: "center", width: 80, sortable: false },
                    { name: 'UnitSize', index: 'UnitSize', align: "center", width: 60, sorttype: "float", sortable: false },
                    { name: 'TotalLoss', index: 'TotalLoss', align: "center", width: 80, sortable: false, formatter: formatTLR },
                    { name: 'Rentable', index: 'Rentable', align: "center", width: 70, sortable: false, formatter: formatTLR },
                    { name: 'Location', index: 'Location', sortable: false },
                    { name: 'PartType', index: 'PartType', sortable: false, width: 90 },
                    { name: 'NumberOfParts', index: 'NumberOfParts', sortable: false, align: "right", width: 60, },
                    { name: 'Comments', index: 'Comments', sortable: false },
                    { name: 'EnterdBy', index: 'EnterdBy', sortable: false },
                    { name: 'FacilityName', index: 'FacilityName', sortable: false }
        ],
        //define how pages are displayed and paged
        pager: "ptblUnitMaintenaceList",
        page: 1, // In case this is a select column rebind
        sortname: "EntryDateDisp", // Initially sorted on
        viewrecords: true,
        sortorder: "desc",
        //onSortCol: function (index, columnIndex, sortOrder) {
        //},
        grouping: true,
        //width: jQuery("#SQLInstantResults").width(),
        autowidth: true,
        hidegrid: false,
        forceFit: true, /* fit all columns with in the specified grid area */
        height: 'auto',
        scrollOffset: 0, /* remove scrollbar */
        beforeRequest: function () {
            jQuery('#tblUnitMaintenaceList').setGridParam({ postData: { filter: JSON.stringify(getFormData()) } });
        },
        loadComplete: function () {
            $('#progress').hide();
        },
        jsonReader: {
            root: 'rows',
            id: 'UnitMaintenanceDetailId',
            repeatitems: false
        },
        loadError: function () {
            $('#progress').hide();
            alert('Error occurred while loading data, Please reload page and try again!');
        },
        //onSelectRow: function(){},
        //onSelectAll: function(){},
        altRows: true,
        altclass: "GridAltClass",
        //toppager: true,
        rowNum: 25,
        rowList: [25, 50, 75, 100],
        groupingView: {
            groupField: ['UnitName'], groupColumnShow: [true], groupText: ['<b>{0}</b>'], groupCollapse: false, groupOrder: ['desc']
        },
        caption: "Unit Results <div class='cust-action'><a href='#' onclick='return false;' id='expandCollaseAll'></a>&nbsp;|&nbsp;<a href='#' onclick='ExportUnitMaintenanceToExcel(); return false;'>Export to Excel&nbsp;<img src='../Images/go-arrow-icon.png' width='10' height='10' alt=''/></a></div>"
    }).jqGrid('navGrid', '#ptblUnitMaintenaceList', { cloneToTop: true, edit: false, add: false, del: false, search: false, refresh: false });

    //var subGridOptions = $('#tblUnitMaintenaceList').jqGrid("getGridParam", "subGridOptions"),
    var plusIcon = 'ui-icon-circlesmall-plus-cust',
        minusIcon = 'ui-icon-circlesmall-minus-cust',
        expandAllTitle = "Expand All",
        collapseAllTitle = "Collapse All";
    $('#expandCollaseAll')
        .html('<a style="cursor: pointer;"><span class="' + minusIcon + '" title="">' + collapseAllTitle + '</span></a>')
        .click(function () {
            var $spanIcon = $(this).find(">a>span"),
                $body = $('#tblUnitMaintenaceList');

            if ($spanIcon.hasClass(plusIcon)) {
                $spanIcon.removeClass(plusIcon)
                    .addClass(minusIcon)
                    .attr("title", collapseAllTitle)
                .html(collapseAllTitle);
                $body.find('.ui-icon-circlesmall-plus').click()
            } else {
                $spanIcon.removeClass(minusIcon)
                    .addClass(plusIcon)
                    .attr("title", expandAllTitle)
                .html(expandAllTitle);
                $body.find('.ui-icon-circlesmall-minus').click()
            }
        });
});
