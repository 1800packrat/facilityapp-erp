﻿$(document).ready(function () {

    bindAjaxStartEvent();

    $('#btnSaveRoleDetailsBottom, #btnSaveRoleDetailsTop').click(function (e) {
        saveRoleDetails();
    });

    $('#ddlFilterArea').change(function () {
        filterPermissions(this);

        handleSelectAllRadioBtn();
    });

    $("input[name*='module']:radio", $('.footable > tbody > tr').not(':first')).change(function () {
        handleSelectAllRadioBtn();
    });

    handleSelectAllRadioBtn();
});

function filterPermissions(ddlFilterArea) {
    var selectedValue = $(ddlFilterArea).val();
    var applicationId = $('option:selected', ddlFilterArea).attr('data-applicationid');

    var rows = $('.footable > tbody > tr').not(':first');

    if (selectedValue == "-1") {
        rows.show();
    }
    else {
        rows.filter(function () {
            return $(this).attr('data-areaid') == selectedValue || $(this).attr('data-applicationId') == applicationId
        }).show();
        rows.not(function () {
            return $(this).attr('data-areaid') == selectedValue || $(this).attr('data-applicationId') == applicationId
        }).hide();
    }
}

function togglePermissions(spanToggle) {
    $(spanToggle).text($(spanToggle).text() == '+' ? '-' : '+');

    var selectedValue = $(spanToggle).parent().parent().attr('data-areaid');

    var rows = $('.footable > tbody > tr');

    rows.filter(function () {
        return $(this).attr('data-areaid') == selectedValue
    }).not(':first').toggle();
}

function bindAjaxStartEvent() {
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
}

function rtnSelectedValues() {
    var selectedModules = [];

    $('.footable > tbody').each(function () {
        var thisDiv = $(this);
        var checked = $(this).find('input:radio:checked');
        if (checked.length > 0) {
            $(checked).each(function () {
                var hf = $(this).parent().parent().find('#hfModuleId');
                var hfAreaModuleId = $(this).parent().parent().find('#hfAreaModuleId');
                var modAccessRelation = {}
                if (hf.length > 0) {
                    modAccessRelation = {
                        ModuleID: $(hf).val(),
                        AccessLevelID: $(this).val()
                    };
                    selectedModules.push(modAccessRelation);
                }
            })
        }
    });
    return selectedModules;
}

function saveRoleDetails() {
    var GroupId = $('#groupId').val();
    var GroupName = $('#GroupName').val();
    var Description = $('#Description').val();

    var seltedModules = rtnSelectedValues();

    if (GroupName.length == 0) {
        alert('Role Name should not be empty.');
        $('#GroupName').focus();
        return;
    }

    if (seltedModules.length == 0) {
        //alert('Please select atleast one permission.');
        var result = confirm("No Access to all permissions by default.");
        if (result == false) {
            return;
        }
    }

    var postData = {
        RoleId: GroupId,
        RoleName: GroupName,
        RoleDescription: Description,
        selectedModules: seltedModules
    }

    $.ajax({
        url: URLHelper.addEditRole,
        type: 'POST',
        data: JSON.stringify(postData),
        traditional: true,
        datatype: "json",
        contentType: 'application/json; charset=utf-8',
        beforeSend: function () {
        },
        success: function (result) {
            if (result.msg == "RoleExists")// if not zero then there is data to display
            {
                alert('Role name already exists, please choose another.')
            }
            else if (result.msg == "error") {
                alert('Error occurred while loading information. Please reload the page and try again!');
            }
            else if (result.msg == "updated") {
                //alert('Role updated successfully.');               
                location.href = '/RoleManagement/Index'; //uncommented this line if we want to redirect back to role list page
            }
            else {
                //alert('Role created successfully.');
                location.href = '/RoleManagement/Index'; //uncommented this line if we want to redirect back to role list page
            }
        },
        error: function (error) {
            alert('Error occurred while loading information. Please reload the page and try again!');
        },
        complete: function () {
        }
    });
}

function SetActiveInactive(RoleId, RoleName, Status) {
    if (RoleName == isDriverDefaultRoleGroupName) {
        alert("The Driver Role can not be disabled as drivers would be unable to access Driver app. Please contact IT to make arrangements for your request.");
    }
    else {
        var result = confirm("Are you sure you want to make it " + Status + " ?");
        if (result == true) {
            $.ajax({
                url: URLHelper.SetRoleActiveInactiveUrl,
                data: {
                    RoleId: RoleId,
                    status: Status
                },
                success: function (retVal) {
                    if (retVal == "error") {
                        alert('Error occurred while loading information. Please reload the page and try again!');
                    }
                    else {
                        $("#tblLstRoles").trigger("reloadGrid");
                    }
                },
                error: function (error) {
                    alert('Error occurred while loading information. Please reload the page and try again!');
                }
            });
        }
    }
}

function initPopup() {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popup').fadeIn();

    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });
    $(window).resize(function () {
        $(".modal-box").css({
            top: 50,
            left: ($(window).width() - $(".modal-box").outerWidth()) / 2
        });
    });
    $(window).resize();
}

//deprecated
//function checkChildRadios(AreaRadio) {
//    var areaID = $(AreaRadio).parent().parent().find('#hfAreaId').val();
//    var accessLevelID = $(AreaRadio).val();

//    var mainparent = $(AreaRadio).parent().parent().parent();

//    $(mainparent).find("#hfChildAreaId").each(function () {
//        if ($(this).val() == areaID) {
//            $(this).parent().parent().find("input[type=radio]").each(function () {
//                if ($(this).val() == accessLevelID) {
//                    this.checked = "checked";
//                }
//            });
//        }
//    });
//}

function checkChildRadios(AreaRadio) {
    var accessLevelID = $(AreaRadio).val();

    var rows = $('.footable > tbody > tr').not(':first').filter(function () {
        if ($('#GroupName').val() == isDriverDefaultRoleGroupName) {
            return $(this).data('areaid') != "5";
        }
        else {
            return $(this).data('areaid');
        }

    });

    ////unchecking all checkboxes before checking them based on filter
    //$(rows).filter(function () {
    //    return $(this).css('display') !== 'none';
    //});

    var selectedValue = $("#ddlFilterArea").val();
    if (selectedValue == "-1") {
        $(rows).find("input[type=radio]").each(function () {
            if ($(this).val() == accessLevelID) {
                this.checked = "checked";
            }
        });
    }
    else {
        $(rows).filter(function () {
            return $(this).data('areaid') == selectedValue;
        }).find("input[type=radio]").each(function () {
            if ($(this).val() == accessLevelID) {
                this.checked = "checked";
            }
        });
    }
}

function handleSelectAllRadioBtn() {
    var totalModuleRow = $('.footable > tbody > tr').not(':first').filter(':visible');
    var rows = $('.footable > tbody > tr').not(':first').find(':radio:checked:visible');
    var singleValSelected = true;

    if (rows.length == totalModuleRow.length) {
        var prevSelectedAccPer = -1;
        rows.each(function () {
            var thisObjAccesslevel = $(this).val();
            if (prevSelectedAccPer != -1 && prevSelectedAccPer != thisObjAccesslevel) {
                console.log(prevSelectedAccPer + '!= -1 && ' + prevSelectedAccPer + '!= ' + thisObjAccesslevel);
                singleValSelected = false;
            }
            prevSelectedAccPer = thisObjAccesslevel;
        });
    } else {
        singleValSelected = false;
    }

    if (singleValSelected == true && prevSelectedAccPer > 0) {
        if (prevSelectedAccPer == 3)
            $('#RdReadAndWrite').prop("checked", true);
        else if (prevSelectedAccPer == 2)
            $('#RdRead').prop("checked", true);
        else if (prevSelectedAccPer == 1)
            $('#RdNoAccess').prop("checked", true);
    }
    else {
        $('#RdReadAndWrite').prop("checked", false);
        $('#RdRead').prop("checked", false);
        $('#RdNoAccess').prop("checked", false);
    }
}