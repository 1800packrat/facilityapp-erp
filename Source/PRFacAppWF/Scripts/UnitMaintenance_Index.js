﻿// SFERP-TODO-CTRMV -- Remove this page completely --TG-563
    
var CountPartTypes = 0;
var UnitNumberOnBlurEventFire = true;

var err = {
    Unitdoesnotexist: 'Unit does not exist',
    RequriedField: 'This is required field',
    CommentsMaxLength: 'Comments should not be more than 250 characters'
};

$(document).ready(function () {
    var oDate = new Date();
    $('#errUnitNumber').hide();

    $('#UnitNumber').on('blur', function (obj, val) {
        var val = $(this).val().trim();

        setTimeout(function () { GetUnitDetails(val); }, 500);
    });

    $('.non-negative').on('keyup', function (obj, val) {
        var val = parseInt($(this).val().trim());
        //console.log(val);
        if ($(this).val().trim() != val || isNaN(val) || val <= 0)
            $(this).val('');
    });

    $('#Comments').on('blur', function (obj, val) {

        if ($(this).val().trim().length <= 0 && $('input:radio[name=IsTotalLoss]:checked').val() == "true") {
            $('#errComments').html(err.RequriedField).show();
        } else if ($(this).val().trim().length > 250) {
            $('#errComments').html(err.CommentsMaxLength).show();
        } else
            $('#errComments').html('').hide();
    });

    $('#UnitNumber').autocomplete(
    {
        source: function (request, response) {
            UnitNumberOnBlurEventFire = true;

            $.ajax({
                url: URLHelper.searchUnits,
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.UnitName, id: item.UMUnitId, unitSize: item.UnitSize, value: item.UnitName, location: item.Location, facilityName: item.FacilityName };
                    }))
                }
            });
        },
        select: function (event, ui) {
            $('#errUnitNumber').hide();
            UnitNumberOnBlurEventFire = false;
            SetSizeLocation(ui.item.id, ui.item.unitSize, ui.item.location, ui.item.facilityName);
        }
    });

    $("#TotalLoss input[name='IsTotalLoss']").click(function () {
        if ($('input:radio[name=IsTotalLoss]:checked').val() == "true") {
            $('#errPartDetails').hide();

            $('#rdoRNo').prop('checked', true);
            $('#dvRentable input[type=radio]').attr("disabled", true);

            $('#PartTypeNoOfParts input[type=text]').attr("disabled", true);
            $('#PartTypeNoOfParts select').attr("disabled", true);
        } else {
            $('#errComments').html('').hide();

            $('#dvRentable input[type=radio]').removeAttr("disabled");

            $('#PartTypeNoOfParts input[type=text]').removeAttr("disabled");
            $('#PartTypeNoOfParts select').removeAttr("disabled");
        }
    });
});

function GetUnitDetails(val) {
    if (val.length == 0) {
        $('#errUnitNumber').html(err.RequriedField).show();
    }

    if (val.length > 0 && UnitNumberOnBlurEventFire == true) {
        $.ajax({
            url: URLHelper.searchUnits,
            type: "POST",
            dataType: "json",
            data: { term: val },
            success: function (data) {
                if (data.length == 1) {
                    //console.log(data[0].UnitSize + " , " + data[0].Location);
                    $('#errUnitNumber').hide();
                    SetSizeLocation(data[0].UMUnitId, data[0].UnitSize, data[0].Location, data[0].FacilityName);
                } else {
                    $('#errUnitNumber').html(err.Unitdoesnotexist).show();
                    SetSizeLocation(-1, "", "", "");
                }
            }
        });
    }
}

function SetUnitDetails(UMUnitId, size, location) {
    $('#txtLocation').val(location);
    $('#UMUnitId').val(UMUnitId);

    if (size == '8 Ft' || size == '12 Ft' || size == '16 Ft')
        $('#Size').val(size);
    else
        $('#Size').val("");
}

function SetSizeLocation(UMUnitId, size, location, facilityName) {
    var currentfacilityName = $('#CurrentFacilityName').val();
    if (currentfacilityName != facilityName && UMUnitId > 0) {
        $('#SelectedFacilityName').html(facilityName);
        showConfirmation();

        $('#confirmation-pop-UMUnitId').val(UMUnitId);
        $('#confirmation-pop-size').val(size);
        $('#confirmation-pop-location').val(location);
    } else
        SetUnitDetails(UMUnitId, size, location);
}

function onConfirmYes() {
    SetUnitDetails($('#confirmation-pop-UMUnitId').val(), $('#confirmation-pop-size').val(), $('#confirmation-pop-location').val());
    closeConfirmation();
}
function onConfirmNo() {
    closeConfirmation();

    SetUnitDetails(-1, "-1", "");
    setTimeout(function () { $('#UnitNumber').val(''); $('#errUnitNumber').html(err.RequriedField).show(); }, 500);
}
function closeConfirmation() {
    $('#confirmation-pop-UMUnitId').val('');
    $('#confirmation-pop-size').val('');
    $('#confirmation-pop-location').val('');

    $('#confirmation-pop').hide();
}
function showConfirmation() {
    $('#confirmation-pop-UMUnitId').val('');
    $('#confirmation-pop-size').val('');
    $('#confirmation-pop-location').val('');

    $('#confirmation-pop').show();
}

function GetHtmlForNewPartDetails() {
    //var len = $('.extraUnitPartInfo').length;
    var $html = $('.addmore-section').clone();

    //$html.find('[name=ContSize]')[0].id = "ContSize" + len;
    $html.find('[name=PartDetailsUnitPartsId]')[0].id = "UMPartDetails_" + CountPartTypes + "__UnitPartsId";
    $html.find('[name=PartDetailsUnitPartsId]')[0].name = "UMPartDetails[" + CountPartTypes + "].UnitPartsId";
    $html.find('[name=PartDetailsNumberOfParts]')[0].id = "UMPartDetails_" + CountPartTypes + "__NumberOfParts";
    $html.find('[name=PartDetailsNumberOfParts]')[0].name = "UMPartDetails[" + CountPartTypes + "].NumberOfParts";

    $html.find('[name=IsDeleted]')[0].id = "UMPartDetails_" + CountPartTypes + "__IsDeleted";
    $html.find('[name=IsDeleted]')[0].name = "UMPartDetails[" + CountPartTypes + "].IsDeleted";

    //$html.find('[name=Add]')[0].id = "Add" + len;
    $html.find('[name=Remove]')[0].id = "Remove" + CountPartTypes;
    $html.find('[name=Remove]')[0].name = "Remove" + CountPartTypes;
    return $html.html();
}

function RemoveRow(e) {
    if ($('#IsTotalLoss').is(':checked') == false) {

        var number = e.id.replace("Remove", "");
        $('#UMPartDetails_' + number + '__IsDeleted').val("true");
        $('#extraUnitPartInfo_' + number).css({ display: 'none' });
    }
}

function AddNewPartDetails(e) {
    if ($('#IsTotalLoss').is(':checked') == false) {
        CountPartTypes++;

        $('<div/>', {
            'class': 'extraUnitPartInfo clear',
            'id': 'extraUnitPartInfo_' + CountPartTypes,
            html: GetHtmlForNewPartDetails()
        }).hide().appendTo('#container').slideDown('slow');
    }

    return false;
}

function CheckDuplicatePartType(obj) {
    var selVal = $(obj).val();
    var selId = obj.id;

    $.each($('#PartTypeNoOfParts select'), function (obj1, o) {
        if (o.id != "PartDetailsUnitPartsId" && o.id != selId) {
            if (selVal == $(o).val()) {
                alert('Cannot enter duplicate part types.  Must remove the duplicate or change the part type to save the entry.');
                $(obj).val("");
            }
        }
    });
}

function onResetFrom() {
    $('#dvRentable input[type=radio]').removeAttr("disabled");

    $('#PartTypeNoOfParts input[type=text]').removeAttr("disabled");
    $('#PartTypeNoOfParts select').removeAttr("disabled");
}

function ValidateForm() {

    var rtVal = true;
    var errMsg = "";

    if ($('#UnitNumber').val().trim().length <= 0) {
        rtVal = false;
        $('#errUnitNumber').html(err.RequriedField).show();
    }
    else if ($('#UMUnitId').val() <= 0) {
        rtVal = false;
        $('#errUnitNumber').html(err.Unitdoesnotexist).show();
    }

    if (($('input:radio[name=IsTotalLoss]:checked').val() == "true") && $('#Comments').val().trim().length <= 0) {
        rtVal = false;
        $('#errComments').html(err.RequriedField).show();
    }

    if ($('#Comments').val().trim().length > 250) {
        rtVal = false;
        $('#errComments').html(err.CommentsMaxLength).show();
    }

    var partFlag = ValidatePartDetial();

    if (rtVal == true && partFlag == false)
        rtVal = partFlag;

    if (rtVal == true)
        $('#progress').show();

    return rtVal;
}

function ValidatePartDetial() {
    var rtVal = true;
    var msg = ""
    var msg1 = ""

    if ($('#IsTotalLoss').is(':checked') == false) {

        for (ii = 0; ii <= CountPartTypes; ii++) {

            if ($("#UMPartDetails_" + ii + "__IsDeleted").val() == "false") {

                if ($("#UMPartDetails_" + ii + "__UnitPartsId").val() <= 0) {
                    rtVal = false;
                    msg = "Part Type is required";
                }

                if (isNaN(parseInt($("#UMPartDetails_" + ii + "__NumberOfParts").val())) == true) {
                    rtVal = false;
                    msg1 = "Number of Parts are required";
                }
            }

            if (rtVal == false)
                break;
        }

        if (msg != '' || msg1 != '') {
            $('#errPartDetailsT').html(msg);
            $('#errPartDetailsN').html(msg);
            $('#errPartDetails').show();
        }
        else
            $('#errPartDetails').hide();
    } else
        $('#errPartDetails').hide();

    return rtVal;
}

