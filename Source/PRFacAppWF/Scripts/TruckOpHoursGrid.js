﻿var popupElement;
var editSelectedDate;

function GetSelectedDate() {
    return editSelectedDate ? editSelectedDate : (new Date()).ToDate();
}

var truckOpHoursPopupSetting = {
    actions: {
        adjustCenter: function () {
            /* $(".modal-box").css({
                 //top: top,
                 //left: left
                 top: (($(window).height() / 2) - ($(".modal-box").outerHeight() / 2)) + $(window).scrollTop(),
                 left: (($(window).width() / 2) - ($(".modal-box").outerWidth() / 2))
             });*/
        },
        adjustCenterWithDelay: function () {
            setTimeout('truckOpHoursPopupSetting.actions.adjustCenter()', 700);
        },
        adjustTransferDimentions: function () {
            truckOpHoursPopupSetting.actions.adjustCenterWithDelay();
        },
        adjustNoserviceDimentions: function () {
        },
        adjustHourChangeDimentions: function () {
            truckOpHoursPopupSetting.actions.adjustCenterWithDelay();

            var gridId = 'list';
            var grid = $('#gbox_' + gridId).parent();
            var gridParentWidth = grid.width();
            $('#' + gridId).jqGrid('setGridWidth', gridParentWidth);

            var gridParentHeight = grid.height();
            if (gridParentHeight <= 800) {

                var height = ($(window).height() - $(".modal-box").outerHeight()) - 100;

                $('#truck-ophours-reason-from').css('max-height', height + 'px'); //set max height
                $('#truck-ophours-reason-from').css('overflow-y', 'scroll'); //set max height
                $('#truck-ophours-reason-from').css('overflow-x', 'hidden'); //set max height
            }
        }
    },
    initPopup: function () {
        var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
        $('a[data-modal-id]').click(function (e) {
            e.preventDefault();
            //$("body").append(appendthis);
            //$(".modal-overlay").fadeTo(500, 0.7);
            ////$(".js-modalbox").fadeIn(500);
            //var modalBox = $(this).attr('data-modal-id');
            //$('#' + modalBox).fadeIn($(this).data());

            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            //$(".js-modalbox").fadeIn(500);
            //var modalBox = $(this).attr('data-modal-id');
            $('#popup').fadeIn();

            //loadGrid();
        });

        $(".js-modal-close, .modal-overlay").click(function () {
            $(".modal-box, .modal-overlay").fadeOut(500, function () {
                $(".modal-overlay").remove();
            });

            //When you are closing popup, need to refresh data to get updated information specially for Temp hour changes
            //loadDataWithGridColumnHeaderAndData();
        });
        $(window).resize(function () {
            $(".modal-box").css({
                top: 50,
                left: ($(window).width() - $(".modal-box").outerWidth()) / 2
            });
        });
        $(window).resize();
    }
};

function showTruckOpPopup(truckId, truckName, truckOpHrId, opReasonId, selectedDate) {
    //If user click on any date hour change, then we have to store that value into global variable. other wise, it should be TruckOpHourStartDate 
    if (selectedDate && editSelectedDate.length > 0)
        editSelectedDate = selectedDate;
    else
        //editSelectedDate = $('#TruckOpHourStartDate').val();
        editSelectedDate = (new Date()).ToDate();


    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popup').fadeIn();

    $('#popupTruckOpHourHead').text(truckName);

    //Add truck id and truckoperation hourid for edit purpose
    $('#hidtruckId').val(truckId);

    //If we are in edit, then call database and get details and set it to form
    //If the opReasonId is 6 means - this truck is available for this facility, so user is trying to update some operational hours
    if (truckOpHrId > 0 && opReasonId != 6) {
        $('#hidCAPTruckOperatingHourId').val(truckOpHrId);
        getPopupWindowDateByTruckOpHrId(truckOpHrId)
    }
    else if (opReasonId == 3) {
        $('#hidCAPTruckOperatingHourId').val(-1);
        $("#ddlTruckOperations").val(3);
        showhideTruckOpControls()
    }
    else {
        $('#hidCAPTruckOperatingHourId').val(-1);
        $("#ddlTruckOperations").val(-1);
        showhideTruckOpControls()
    }
}

function getPopupWindowDateByTruckOpHrId(truckOperatingHourId) {

    $.ajax({
        //functionality yet to do for dispalying the existing details
        url: URLHelper.getTruckOpDetails,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            CAPTruckOperatingHourId: truckOperatingHourId
        }),
        success: function (retVal) {
            if (retVal.CAPTruckOperatingHourId != 0) {
                getTruckOpHoursData(retVal);
            }
            else {
                alert('Error occurred while loading data...! Please reload and try again!');
            }
        },
        error: function (error) {
            alert('Error occurred while displaying data. Please reload the page and try again!');
        }
    });
}

function loadTruckOpHrChangeGrid() {

    $("#list").clearGridData();

    $("#list").jqGrid('setGridParam', {
        mtype: 'GET', editurl: URLHelper.SaveTempHourChange, onclickSubmit: batchSumbit, url: URLHelper.GetTempHourChange, datatype: 'json'
                                , postData: {
                                    dates: jQuery("#list").jqGrid('getGridParam', 'selarrrow'),
                                    //StartDate: function () { return $('#TruckOpHourStartDate').val(); },
                                    StartDate: function () { return GetSelectedDate(); },
                                    truckId: function () { return $('#hidtruckId').length == 0 ? -1 : $('#hidtruckId').val(); }
                                }
    }).trigger("reloadGrid");
}

function GetWorkingHours(formid, isMultipleSelected) {
    //When user select multiple check box from UI and trying to update all at once, by that time we are showing default values as 07AM to 05PM.
    if (isMultipleSelected) {
        $("select#StoreOpen", formid).val('O')
        $("input#StartTime", formid).val('07:00 AM');
        $("input#EndTime", formid).val('05:00 PM');
    }

    var startTime = $("input#StartTime", formid).val();
    var endTime = $("input#EndTime", formid).val();
    var storeOpen = $("select#StoreOpen", formid).val();
    var workingHr = '0 hr 0 min';
    
    if (storeOpen == "O" && startTime != '' && endTime != '') {
        var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
        var endTimeDt = new Date("1/1/1970" + ' ' + endTime);

        if (startTimeDt < endTimeDt) {
            var differenceDt = new Date(endTimeDt - startTimeDt)

            var hours = differenceDt.getUTCHours();
            var minutes = differenceDt.getUTCMinutes();
            workingHr = hours + ' hr ' + minutes + ' min';
        }
    } 

    $("span#TruckOperatingTimeMinutesDisp", formid).html(workingHr);
}

function initTempHrChangeGrid() {

    var checkStartEndTime = function (value, colname) {
        var startTime = $("input#StartTime").val();
        var endTime = $("input#EndTime").val();

        if (startTime != '' && endTime != '') {
            var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
            var endTimeDt = new Date("1/1/1970" + ' ' + endTime);

            if (startTimeDt > endTimeDt) {
                return [false, "Start time should be less than End time.", ""];
            }
        }

        return [true, "", ""];
    };

    var updateDialog = {
        url: URLHelper.SaveTempHourChange
                , closeAfterEdit: true
                , reloadAfterSubmit: true
                , closeAfterAdd: true
                , closeAfterEdit: true
                , afterShowForm: function (formid) {
                    //$("select#StoreOpen", formid).val("O")
                    GetWorkingHours(formid);

                    //$("input#EndTime", formid).blur(function (o) {
                    //    GetWorkingHours(formid);
                    //});
                    //$("input#StartTime", formid).blur(function (o) {
                    //    GetWorkingHours(formid);
                    //});
                    $("select#StoreOpen", formid).change(function (o) {
                        $("input#StartTime", formid).val('12:00 AM');
                        $("input#EndTime", formid).val('12:00 AM');
                        GetWorkingHours(formid);
                    });
                }
                , onclickSubmit: function (params) {
                    var ajaxData = {};
                    var list = $("#list");
                    var selectedRow = list.getGridParam("selrow");

                    rowData = list.getRowData(selectedRow);

                    ajaxData = { dates: jQuery("#list").jqGrid('getGridParam', 'selarrrow'), CalDate: rowData.CalDate };

                    return ajaxData;
                }
                , afterComplete: function (o) {
                    json = jQuery.parseJSON(o.responseText);
                    if (json.success == true)
                        //alert('Truck operating details have been saved successfully.');
                        loadDataWithGridColumnHeaderAndData();
                }
                , modal: true
                , width: "400"
    };

    $("#list").jqGrid({
        colNames: ['RowID', 'CapacityDateIndex', 'CapacityDate', 'TruckId', 'Truck Status', 'Start Time', 'End Time', 'Operating Hours', 'Status', 'WeekDay']
                    , colModel: [
                            { name: 'RowID', index: 'RowID', editable: false, hidedlg: true, hidden: true },
                            { name: 'CapacityDateIndex', index: 'CapacityDateIndex', key: true, editable: true, editrules: { edithidden: false }, hidedlg: true, hidden: true, search: false },
                            { name: 'CapacityDateDisp', index: 'CapacityDateDisp', width: 80, editable: false, hidedlg: true, hidden: false, search: false, align: 'center' },
                            { name: 'TruckId', index: 'TruckId',  editable: true, editrules: { edithidden: false }, hidedlg: true, hidden: true },
                            { name: 'StoreOpen', index: 'StoreOpen', width: 80, editable: true, edittype: 'select', formatter: 'select', editrules: { required: true }, formoptions: { elmsuffix: ' *' }, editoptions: { value: { O: 'Open', C: 'Closed' } }, search: false, align: 'center' },
                            {
                                name: 'StartTime', index: 'StartTime', width: 60, editable: true, formatter: 'text', edittype: 'text', editrules: { custom: true, custom_func: checkStartEndTime, time: true, required: true },
                                editoptions: {
                                    dataInit: function (element) {
                                        $(element).timepicker({
                                            minTime: '12:00 AM', // 11:45:00 AM,
                                            maxTime: '11:30 PM',
                                            timeFormat: 'h:i A',
                                            step: 15
                                        }).on('changeTime', function () {
                                            ///Here editmodlist is a id of the popup
                                            GetWorkingHours($('#editmodlist'));
                                        });
                                    }
                                },
                                formoptions: { elmsuffix: ' *' }, search: false, align: 'center'
                            },
                            {
                                name: 'EndTime', index: 'EndTime', width: 60, editable: true, edittype: 'text', editrules: { custom: true, custom_func: checkStartEndTime, time: true, required: true },
                                editoptions: {
                                    dataInit: function (element) {
                                        $(element).timepicker({
                                            minTime: '12:00 AM', // 11:45:00 AM,
                                            maxTime: '11:30 PM',
                                            timeFormat: 'h:i A',
                                            step: 15
                                        }).on('changeTime', function () {
                                            ///Here editmodlist is a id of the popup
                                            GetWorkingHours($('#editmodlist'));
                                        });
                                    }
                                },
                                formoptions: { elmsuffix: ' *' }, search: false, align: 'center'
                            },
                            { name: 'TruckOperatingTimeMinutesDisp', index: 'TruckOperatingTimeMinutesDisp', width: 80, align:'center', editable: true, edittype: 'custom', editoptions: { custom_element: myelem, custom_value: myvalue }, search: false},
                            { name: 'Status', index: 'Status', width: 80, editable: false, hidedlg: true, hidden: false, search: false, align: 'center' },
                            { name: 'WeekDay', index: 'WeekDay', width: 100, editable: false, hidden: false, stype: 'select', searchoptions: { value: "0:All Days;8:Weekends;15:WeekDays;1:Sunday;2:Monday;3:Tuesday;4:Wednesday;5:Thursday;6:Friday;7:Saturday;9:Holidays", clearSearch: false }, align: 'center' }

                    ]
                , pager: 'listPager'
                , width: 920
                , rowNum: 20
                , rowList: [10, 20, 30, 60, 365]
                , sortname: 'CapacityDateIndex'
                , sortorder: "asc"
                , caption: 'Truck Temp Hour Change'
                , viewrecords: true
                , autowidth: false
                , gridview: true
                , multiselect: true
                , multiboxonly: true
                , id: "CapacityDateIndex"
                , height: "100%"                
                //, toolbar: [true, "top"]
                , datatype: 'local'
                , loadError: function (xhr, st, err) {
                    if (xhr.status == "200") return false;
                    var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                        modal: false,
                        title: 'Error',
                        maxwidth: '600',
                        width: '600',
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
                , gridComplete: function () {
                    var ids = $("#list").getDataIDs();
                    for (var i = 0; i < ids.length; i++) {
                        var cell = $("#list").getCell(ids[i], "WeekDay");
                        if (cell == "Sunday" || cell == "Saturday") {
                            $(this).jqGrid('setRowData', ids[i], false, { background: '#e0effa' });
                            // $("#" + ids)[i].style.backgroundColor = "yellow"
                        }
                    }

                    //disable check boxes which are less than todays date
                    var jqgList = $("input:checkbox[id^=jqg_list_]");
                    $.each(jqgList, function (i, o) {
                        var dt = new Date(o.id.replace('jqg_list_', ''));
                        var currentdate = new Date();
                        currentdate.setHours(0, 0, 0, 0);
                        //console.log(currentdate);
                        if (currentdate > dt)
                            $(o).remove();
                    });

                    truckOpHoursPopupSetting.actions.adjustCenter();
                }
                , ondblClickRow: function (rowid, iRow, iCol, e) {
                    if (isFullAccess == "True") {
                        var dt = new Date(rowid);
                        var currentdate = new Date();
                        if (currentdate < dt)
                            $("#list").editGridRow(rowid, updateDialog);
                    }
                }
    }).navGrid('#listPager',
                   {
                       edit: false, add: false, del: false, search: false, refresh: false
                   },
                   updateDialog,
                   null,
                   null);

    jQuery("#modalUpdateselectedhours").click(function () {
        var rows = jQuery("#list").jqGrid('getGridParam', 'selarrrow');

        if (rows.length > 0) {
            jQuery("#list").jqGrid('editGridRow', rows, {
                height: 300,
                width: 450,
                closeAfterEdit: true,
                reloadAfterSubmit: true,
                afterShowForm: function (formid) {
                    //console.log(formid);
                    //$("select#StoreOpen", formid).val("O")
                    GetWorkingHours(formid, (rows.length > 1));

                    //$("input#EndTime", formid).blur(function (o) {
                    //    console.log('end time on blur');
                    //    console.log(formid);
                    //    GetWorkingHours(formid);
                    //});
                    //$("input#StartTime", formid).blur(function (o) {
                    //    console.log('starttime time on blur');
                    //    console.log(formid);
                    //    GetWorkingHours(formid);
                    //});
                    $("select#StoreOpen", formid).change(function (o) {
                        $("input#StartTime", formid).val('12:00 AM');
                        $("input#EndTime", formid).val('12:00 AM');
                        GetWorkingHours(formid);
                    });
                }
                , onclickSubmit: batchSumbit
                , url: URLHelper.SaveTempHourChange
                , afterComplete: function (o) {
                    var json = jQuery.parseJSON(o.responseText);
                    if (json.success == true)
                        //alert('Truck operating details have been saved successfully.');
                        loadDataWithGridColumnHeaderAndData();
                }
            });
        } else alert("Please Select a Row");
    });

    jQuery("#modalUpdateFacilityhours").click(function () {
        var rows = jQuery("#list").jqGrid('getGridParam', 'selarrrow');
        var TruckId = jQuery("#list").getRowData(rows[0]).TruckId;

        if (rows.length > 0) {
            var dataToSend = {
                collection: rows,
                TruckId: TruckId
            };

            $.ajax({
                type: "POST",
                url: URLHelper.RevertToFacilityOperatingHours,
                dataType: "json",
                data: JSON.stringify(dataToSend),
                contentType: "application/json; charset=utf-8",
                success: function (response, textStatus, jqXHR) {
                    loadDataWithGridColumnHeaderAndData();
                    $("#list").trigger("reloadGrid");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                  
                }
            });
        } else alert("Please Select a Row");
    });

    jQuery("#list").jqGrid('filterToolbar');
}

var batchSumbit = function (params) {
    var ajaxData = {};
    var data;
    var rowids = jQuery("#list").jqGrid('getGridParam', 'selarrrow');

    if (rowids != null) {
        return ajaxData = { dates: rowids, TruckId: $('#hidtruckId').val() };
    }
}

function myelem(value, options) {
    var el = document.createElement("span");
    el.type = "span";
    el.className = 'oprationHoursDuration'
    el.innerHTML = value;
    return el;
}

function myvalue(elem, operation, value) {
    if (operation === 'get') {
        return $(elem).val();
    } else if (operation === 'set') {
        $('span', elem).val(value);
    }
}

function hideAllControls() {
    //$('#truck-ophours-reason-from').css('max-height', height + 'px'); //set max height

    $('#truck-ophours-reason-from').css('overflow-y', 'hidden'); //set max height
    $('#truck-ophours-reason-from').css('overflow-x', 'hidden'); //set max height

    $('#modalUpdateselectedhours').hide();
    $('#modalUpdateFacilityhours').hide();
    $('#backToService').hide();

    $('#noneReasonMsg').hide();
    $('#truckNoService').hide();
    $('#truckTransfers').hide();
    $('#tempHourChange').hide();
    $('#tempHourChange').hide();
}

function getTruckOpHoursData(retVal) {
    if (retVal.TruckOHReasonId != 0) {
        hideAllControls();

        $('#hidtruckId').val(retVal.TruckId);
        $('#hidCAPTruckOperatingHourId').val(retVal.CAPTruckOperatingHourId);

        //saving previous TruckOpReasonid for validating the record during saving
        $("#ddlTruckOperations").val(retVal.TruckOHReasonId);

        switch (retVal.TruckOHReasonId) {
            case 1:
                editTruckOpHr.truckNoService(retVal);
                break;
            case 3:
                editTruckOpHr.tempHourChange(retVal);
                break;
            case 6:
                editTruckOpHr.permanentTransfer(retVal);
                break;
        }
    }
    else {
        $("#ddlTruckOperations").val(-1);

        showhideTruckOpControls();
    }
}

function showhideTruckOpControls() {
    hideAllControls();

    switch (parseInt($("#ddlTruckOperations").val())) {
        case 1://NoService
            resetTruckOpHrForm.truckNoService();
            truckOpHoursPopupSetting.actions.adjustTransferDimentions();
            break;
        case 3: //HourChangeDimentions
            resetTruckOpHrForm.tempHourChange();
            truckOpHoursPopupSetting.actions.adjustHourChangeDimentions();
            break;
        case 6://PermanentTransfer
            resetTruckOpHrForm.permanentTransfer();
            truckOpHoursPopupSetting.actions.adjustTransferDimentions();
            break;
        default:
            $('#noneReasonMsg').show();
            resetTruckOpHrForm.defaultReset();
            truckOpHoursPopupSetting.actions.adjustTransferDimentions();
            break;
    }
}

var resetTruckOpHrForm = {
    truckNoService: function () {
        $('#truckNoService').show();
        $('#modalSavehours').show();

        $('#MaintenOfflineDate').val(GetSelectedDate());
        $('#MaintenOfflineDate').removeAttr('disabled');
        $('#MaintenOfflineDateIcon').removeAttr('disabled');
        $('#MaintenOfflineDate').datepicker("option", {
            minDate: new Date(),
            maxDate: null
        });

        $('#MaintenOnlineDate').val('');
        $('#MaintenOnlineDate').removeAttr('disabled');
        $('#MaintenOnlineDateIcon').removeAttr('disabled');
        $('#MaintenOnlineDate').datepicker("option", {
            //minDate: new Date(),
            minDate: new Date(GetSelectedDate()).addDays(1),
            maxDate: null
        });

        $('#dvTransferDetails').text("");
    },
    tempHourChange: function () {
        $('#tempHourChange').show();

        $('#modalUpdateselectedhours').show();
        $('#modalUpdateFacilityhours').show();
        $('#modalSavehours').hide();

        loadTruckOpHrChangeGrid();
    },
    permanentTransfer: function () {
        $('#truckTransfers').show();
        $('#modalSavehours').show();

        $('#ddlTransferPermFacilites').val(-1);
        $('#ddlTransferPermFacilites').removeAttr('disabled');

        $('#ddlTransferPermReason').val(-1);
        $('#ddlTransferPermReason').removeAttr('disabled');

        $('#transPermLeaveFacility').val(GetSelectedDate());
        $('#transPermLeaveFacility').removeAttr('disabled');
        $('#transPermLeaveFacilityIcon').removeAttr('disabled');
        $('#transPermLeaveFacility').datepicker("option", {
            minDate: new Date(),
            maxDate: null
        });

        $('#transPermArrFaciliity').val('');
        $('#transPermArrFaciliity').removeAttr('disabled');
        $('#transPermArrFaciliityIcon').removeAttr('disabled');
        $('#transPermArrFaciliity').datepicker("option", {
            //minDate: new Date(),
            minDate: new Date(GetSelectedDate()),
            maxDate: null
        });

        $('#lblTransferPermArriveAt').text('Facility');

        $('#dvTransferPermDetails').text("");
    },
    defaultReset: function () {
        $('#ddlTruckOperations').removeAttr('disabled');
        $('#modalSavehours').hide();
    }
};

var editTruckOpHr = {
    truckNoService: function (retVal) {
        $('#truckNoService').show();

        $('#ddlTruckOperations').attr('disabled', 'disabled');

        var startDate = new Date(parseInt(retVal.Startdate.replace("/Date(", "").replace(")/", ""), 10));
        var endDate = new Date(parseInt(retVal.Enddate.replace("/Date(", "").replace(")/", ""), 10)).addDays(1);

        //console.log(startDate.ToDate() + ' ' + startDate + ' ' +  new Date() + '; Result: ' + (startDate < new Date()))

        if (startDate < new Date(new Date().ToDate())) {
            $('#MaintenOfflineDate').attr('disabled', 'disabled');
            $('#MaintenOfflineDateIcon').attr('disabled', 'disabled');
        }
        else {
            $('#MaintenOfflineDate').datepicker('option', 'maxDate', endDate);
        }

        if (endDate < new Date(new Date().ToDate())) {
            $('#MaintenOnlineDate').attr('disabled', 'disabled');
            $('#MaintenOnlineDateIcon').attr('disabled', 'disabled');
            $('#modalSavehours').hide();
        } else {
            $('#backToService').show(); //This button only show when the EndDate is not greatr than todays date.
            $('#modalSavehours').show();
            $('#MaintenOnlineDate').datepicker('option', 'minDate', startDate.addDays(1));
        }

        $('#MaintenOfflineDate').val(startDate.ToDate());
        $('#MaintenOnlineDate').val(endDate.ToDate());

        var onlinedate = endDate;
        onlinedate.setDate(onlinedate.getDate());

        $('#dvTransferDetails').html("* This truck will be available on <strong>" + onlinedate.ToDate() + "</strong>");
    },
    tempHourChange: function () {
        resetTruckOpHrForm.tempHourChange();
        truckOpHoursPopupSetting.actions.adjustHourChangeDimentions();
    },
    permanentTransfer: function () {
        resetTruckOpHrForm.permanentTransfer();
        truckOpHoursPopupSetting.actions.adjustHourChangeDimentions();
    }
}

$(document).ready(function () {
    popupElement = $("#dlgAddEditTruckOpHours");

    truckOpHoursPopupSetting.initPopup();

    initTempHrChangeGrid();

    //By default store date into global variable and use it when editing Truck operating hours
    //editSelectedDate = $('#TruckOpHourStartDate').val();
    editSelectedDate = (new Date()).ToDate();
});