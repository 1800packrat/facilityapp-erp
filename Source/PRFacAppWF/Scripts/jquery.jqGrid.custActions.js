﻿/*global jQuery */
(function (a) {
    'use strict';
    /*jslint unparam: true */
    a.extend(a.fn.fmatter, {

        custActions: function (c, b, z) {
            var d = {
                keys: !1,
                editbutton: !0,
                delbutton: !0,
                editformbutton: !1
            },
                e = b.rowId,
                f = "";
            a.fmatter.isUndefined(b.colModel.formatoptions) || (d = a.extend(d, b.colModel.formatoptions));
            if (void 0 === e || a.fmatter.isEmpty(e)) return "";

            if (a.isFunction(d.delbutton)) {
                d.delbutton = d.delbutton.call(this, c, b.rowId, z, b);
            }

            if (a.isFunction(d.editbutton)) {
                d.editbutton = d.editbutton.call(this, c, b.rowId, z, b);
            }


            d.editformbutton ? f += "<div title='" + a.jgrid.nav.edittitle + "' style='float:left;cursor:pointer;' class='ui-pg-div ui-inline-edit' onclick=jQuery.fn.fmatter.rowactions.call(this,'formedit'); onmouseover=jQuery(this).addClass('ui-state-hover'); onmouseout=jQuery(this).removeClass('ui-state-hover'); ><span class='ui-icon ui-icon-pencil'></span></div>" :
            d.editbutton && (f += "<div title='" + a.jgrid.nav.edittitle + "' style='float:left;cursor:pointer;' class='ui-pg-div ui-inline-edit' onclick=jQuery.fn.fmatter.rowactions.call(this,'edit'); onmouseover=jQuery(this).addClass('ui-state-hover'); onmouseout=jQuery(this).removeClass('ui-state-hover') ><span class='ui-icon ui-icon-pencil'></span></div>");
            d.delbutton && (f += "<div title='" + a.jgrid.nav.deltitle + "' style='float:left;margin-left:5px;' class='ui-pg-div ui-inline-del' onclick=jQuery.fn.fmatter.rowactions.call(this,'del'); onmouseover=jQuery(this).addClass('ui-state-hover'); onmouseout=jQuery(this).removeClass('ui-state-hover'); ><span class='ui-icon ui-icon-trash'></span></div>");
            f += "<div title='" + a.jgrid.edit.bSubmit + "' style='float:left;display:none' class='ui-pg-div ui-inline-save' onclick=jQuery.fn.fmatter.rowactions.call(this,'save'); onmouseover=jQuery(this).addClass('ui-state-hover'); onmouseout=jQuery(this).removeClass('ui-state-hover'); ><span class='ui-icon ui-icon-disk'></span></div>";
            f += "<div title='" + a.jgrid.edit.bCancel + "' style='float:left;display:none;margin-left:5px;' class='ui-pg-div ui-inline-cancel' onclick=jQuery.fn.fmatter.rowactions.call(this,'cancel'); onmouseover=jQuery(this).addClass('ui-state-hover'); onmouseout=jQuery(this).removeClass('ui-state-hover'); ><span class='ui-icon ui-icon-cancel'></span></div>";
            return "<div style='margin-left:8px;'>" + f + "</div>"
        }
    });

}(jQuery));