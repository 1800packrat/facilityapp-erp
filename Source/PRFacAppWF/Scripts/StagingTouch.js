﻿var TIInput = {};

$(document).ready(function () {
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    $("form").on("submit", function (event) {
        event.preventDefault();

        SearchTruckStatus();
    });

    $("#StagingTouchDateIcon").click(function () {
        $("#StagingTouchDate").focus();
    });
    $('#StagingTouchDate').datepicker({
        //maxDate: 0,
        onSelect: function (date) {}
    });

    initTIUnitNoPopup();

    initGrid();
});

function initTIUnitNoPopup() {
    $("#SubmitEnterUnitNumber").click(function () {
        if ($.trim($('#UnitNo').val()).length <= 0 || $.trim($('#ReUnitNo').val()).length <= 0) {
            alert('Please enter UnitNo');
            return false;
        }
        //Validate to check ReUnit and Unit numbers are same or not
        if ($.trim($('#UnitNo').val()) != $.trim($('#ReUnitNo').val())) {
            alert("Please enter Re Enter Unit No same as Unit No.");
            return false;
        }

        if(TIInput == undefined && TIInput.TripId <= 0){
            alert("Some thing went wrong, please reload the page and try again!");
            return false;
        }else{
            TransferInTouch($('#UnitNo').val());
        }

        return false;
    });

    $("#CancelEnterUnitNumber").click(function () {
        $("#dlgEnterUnitNumber").hide();
        return false;
    });
}

function ShowUnitNoPopup(UnitId, DestQORId, OrgQORId, BillingQORId, TripId, StarsId, OrigLocationCode, DestLocationCode, ContainerType) {
    TIInput = {
        UnitName: "",
        UnitId: UnitId,
        DestQORId: DestQORId,
        OrgQORId: OrgQORId,
        BillingQORId: BillingQORId,
        TripId: TripId,
        StarsId: StarsId,
        OrigLocationCode: OrigLocationCode,
        DestLocationCode: DestLocationCode,
        ContainerType: ContainerType
    };

    $('#dlgEnterUnitNumber').show();
}

function getFormData() {
    var ob = {
        StagingTouchDate: $('#StagingTouchDate').val()
    };
    return ob;
}

function getPostData() {
    var postData = jQuery("#tblStagingTouchesList").jqGrid('getGridParam', 'postData');

    var postDt = {
        filter: getFormData(),
        page: postData.page,
        rows: postData.rows,
        sidx: postData.sidx,
        sord: postData.sord
    }
    return postDt;
}

function SearchTruckStatus() {
    //$('#progress').show();
    jQuery('#tblStagingTouchesList').trigger("reloadGrid");
}

function TransferOutTouch(unitId, qORId, dummyBillingQORId, billingQORId, tripId, starsId) {
    $.ajax({
        url: URLHelper.TransferOutTouchUrl,
        data: {
            QORId: qORId,
            dummyBillingQORId: dummyBillingQORId,
            billingQORId: billingQORId,
            TripId: tripId,
            StarsId: starsId
        },
        beforeSend: function () {
            //$.blockUI({ message: '<h3>Just a moment...</h3>' })
        },
        success: function (retVal) {
            if (retVal.success == false) {
                alert('Error occurred transfer-out. Please reload the page and try again! \n Error: ' + retVal.errMsg);
            } else {
                alert('This order successfully Transferred out.');

                SearchTruckStatus();
            }
        },
        error: function (error) {
            //document.getElementById("divProgress").style.display = 'none;';
            alert('Error occurred Transfer-out. Please reload the page and try again!');
        },
        complete: function () {
            //$.unblockUI();
        }
    });
}

function TransferInTouch(unitName) {
    TIInput.UnitName = unitName;
    
    $.ajax({
        url: URLHelper.TransferInTouchUrl,
        data: TIInput,
        beforeSend: function () {
            $.blockUI({ message: '<h3>Just a moment...</h3>' })
        },
        success: function (retVal) {
            if (retVal.success == false) {
                alert(retVal.msg);
            } 
            else {
                $("#dlgEnterUnitNumber").hide();
                alert('This order successfully Transferred In.');

                SearchTruckStatus();
            }
        },
        error: function (error) {
            //document.getElementById("divProgress").style.display = 'none;';
            alert('Error occurred while transfer-in. Please reload the page and try again! \n Error: ' + retVal.errMsg);
        },
        complete: function () {//$.unblockUI(); 
        }
    });
}

function stagedAction(cellvalue, options, rowObject) {
    var stagedActionTag = "";

    if (rowObject.Status == "Completed") {
        if (rowObject.TouchType == "LDMTransferIn") {
            stagedActionTag = "TRANSFERRED-IN";
        }
        else {
            stagedActionTag = "TRANSFERRED-OUT";
        }
    }
    else if (rowObject.Status == "Open") {
        if (rowObject.TouchType == "LDMTransferIn") {
            var onclickparams = rowObject.UnitNumber + ", " +
                                rowObject.Qorid + ", " +
                                rowObject.LDMOrgQORId + ", " +
                                rowObject.LDMBillingQORId + ", " +
                                rowObject.LDMTripId + ", " +
                                rowObject.LDMQuoteID + ", '" +
                                rowObject.LDMOrigLocationCode + "', '" +
                                rowObject.LDMDestLocationCode + "', " +
                                rowObject.LDMContainerType;

            stagedActionTag = '<a href="#" style="color: #8b1010;" onclick="ShowUnitNoPopup(' + onclickparams + '); return false;" >TRANSFER-IN</a>';
        } else {
            var onclickparams = "'" + rowObject.UnitNumber + "', " + rowObject.Qorid + ", " + rowObject.LDMDummyBillingQORId + ", " + rowObject.LDMBillingQORId + ", " + rowObject.LDMTripId + ", " + rowObject.LDMQuoteID;

            stagedActionTag = '<a href="#" style="color: #8b1010;" onclick="TransferOutTouch(' + onclickparams + '); return false;" >TRANSFER-OUT</a>';
        }
    }

    return stagedActionTag;
}

function emptyIfZero(cellvalue, options, rowObject) {
    return parseInt(cellvalue) > 0 ? cellvalue : "";
}

function StagedTouchAction(qorId, seqNumber, touchType) {
    alert('TODO');
}

function completedAction(cellvalue, options, rowObject) {
    return '';
}

function noShowAction(cellvalue, options, rowObject) {
    return '';
}

function initGrid() {
    jQuery('#tblStagingTouchesList').jqGrid({
        url: URLHelper.SearchTouchList,
        datatype: 'json',
        mtype: 'POST', //
        colNames: ['BillingQORId', 'LDMOrgQORId', 'LDMDestQORId', 'DummyBillingQORId', 'Touch Date & Time', 'Customer Name', 'StarsId', 'QORId', 'Trip #', 'Unit #', 'Touch Type', 'Arrival/Departure Time', 'Staged', 'Completed', 'No Show'],
        colModel: [
            { name: 'LDMBillingQORId', index: 'LDMBillingQORId', hidden: true },
            { name: 'LDMOrgQORId', index: 'LDMOrgQORId', width: 70, sortable: true },
            { name: 'LDMDestQORId', index: 'LDMDestQORId', width: 70, sortable: true },
            { name: 'LDMDummyBillingQORId', index: 'LDMDummyBillingQORId', hidden: true },
            { name: 'ScheduledDateStr', index: 'ScheduledDateStr', width: 90, sortable: true },
            { name: 'CustomerName', index: 'CustomerName', align: "center", width: 60, sortable: true },
            { name: 'LDMQuoteID', index: 'LDMQuoteID', align: "center", width: 60, sortable: true, formatter: emptyIfZero },
            { name: 'Qorid', index: 'Qorid', width: 70, sortable: true },
            { name: 'LDMTripNumber', index: 'LDMTripNumber', width: 90, sortable: true, formatter: emptyIfZero },
            { name: 'UnitNumber', index: 'UnitNumber', width: 120, sortable: true },
            { name: 'TouchType', index: 'TouchType', sortable: true },
            { name: 'LDMArrivalDepartureTime', index: 'LDMArrivalDepartureTime', sortable: true },
            { name: 'Staged', index: 'Staged', sortable: true, formatter: stagedAction },
            { name: 'Completed', index: 'Completed', sortable: true, formatter: completedAction },
            { name: 'No Show', index: 'NoShow', sortable: true, formatter: noShowAction }
        ],
        //define how pages are displayed and paged
        pager: "ptblStagingTouchesList",
        page: 1, // In case this is a select column rebind
        sortname: "Facility", // Initially sorted on
        viewrecords: true,
        sortorder: "asc",
        //onSortCol: function (index, columnIndex, sortOrder) {
        //},
        //grouping: true,
        //width: jQuery("#SQLInstantResults").width(),
        autowidth: true,
        hidegrid: false,
        forceFit: true, /* fit all columns with in the specified grid area */
        height: 'auto',
        scrollOffset: 0, /* remove scrollbar */
        beforeRequest: function () {
            jQuery('#tblStagingTouchesList').setGridParam({ postData: { filter: JSON.stringify(getFormData()) } });
        },
        loadComplete: function () {
            $('#progress').hide();
        },
        jsonReader: {
            root: 'rows',
            id: 'Facility',
            repeatitems: false
        },
        loadError: function () {
            $('#progress').hide();
            alert('Error occurred while loading data, Please reload page and try again!');
        },
        //onSelectRow: function(){},
        //onSelectAll: function(){},
        altRows: true,
        altclass: "GridAltClass",
        //toppager: true,
        rowNum: 25,
        rowList: [25, 50, 75, 100]
        //,
        //caption: "Fleet Status Results <div class='cust-action'><a href='#' onclick='ExportTruckStatusToExcel(); return false;'>Export to Excel&nbsp;<img src='../Images/go-arrow-icon.png' width='10' height='10' alt=''/></a></div>"
    }).jqGrid('navGrid', '#ptblStagingTouchesList', { cloneToTop: true, edit: false, add: false, del: false, search: false, refresh: false });
}