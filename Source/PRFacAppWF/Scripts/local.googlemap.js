﻿var directionsService = new google.maps.DirectionsService();
var distanceArray = [];
var mapRouteArray = [];
var map;
var gmarkers = [];
var gmarkersForStops = [];
var selectedMarkers = [];
var currentMarker;
var markerCluster;
var oms;
var line = [];
var lineByOrderNum = {};

var flightPath;
var homelatlng;
var facilitieslatlng = [];
var marker;
var directionsDisplay;

var infowindow = new google.maps.InfoWindow({ size: new google.maps.Size(380, 170) });

function initialize() {
    homelatlng = new google.maps.LatLng(latitude, longitude);
    var rendererOptions = {
        // draggable: true,
        suppressMarkers: true,
        map: map
    }
    directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

    var mapOptions = {
        zoom: 10,
        center: homelatlng,
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
            //style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_CENTER
        }
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    directionsDisplay.setMap(map);

    google.maps.event.addListener(directionsDisplay, 'directions_changed', function () {
        computeTotalDistance(directionsDisplay.getDirections());
        //alert('direction chage event');
    });

    google.maps.event.addDomListener(window, "resize", function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
        //oms.addMarker(marker);
    });

    mapResize();

    Tour_Startup(null);
}

google.maps.event.addDomListener(window, 'load', initialize);

function mapResize() {
    if (map) {
        var center = map.getCenter();
        $('#map-canvas').css({ 'height': (windowHeight - 30) + 'px' });
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    }
}

function PlotMarkers() {
    var facilitiesRaw = JSON.parse(latlang.replace(/&quot;/g, '"'));
    $.each(facilitiesRaw, function (i, o) {
        facilitieslatlng.push(new google.maps.LatLng(o.Latitude, o.Longitude));
    })

    $.ajax({
        url: URLHelper.plotMarkersUrl,
        dataType: 'json',
        data: { date: document.getElementById("txtDate").value },
        success: function (data) {
            oms = new OverlappingMarkerSpiderfier(map, {
                markersWontMove: true, markersWontHide: true, keepSpiderfied: true,
                circleSpiralSwitchover: 0, circleFootSeparation: 100, spiralFootSeparation: 60, spiralLengthFactor: 20
            });
            
            for (var i = 0; i < data.length; i++) {

                var latlng = new google.maps.LatLng(data[i].Latitude, data[i].Longitude);

                var marker = new google.maps.Marker({
                    position: latlng,
                    icon: data[i].ImageDefault,
                    map: map,
                    draggable: false,
                    driverId: data[i].driverId,
                    touchId: data[i].touchId,
                    orderNumber: data[i].orderNumber,
                    touchType: data[i].TouchType,
                    date: $("#txtDate").val(),
                    selectedIcon: data[i].ImageSelected,
                    defaultIcon: data[i].ImageDefault,
                    startTime: data[i].StartTime,
                    //facCode: data[i].FacCode,
                    siteName: data[i].SiteName,
                    globalSiteNum: data[i].GlobalSiteNum,
                    html: setInfoWindowContent(data[i])
                });
                oms.addMarker(marker);

                var infowindow = new google.maps.InfoWindow();

                google.maps.event.addListener(marker, 'mouseover', (function (marker, content, infowindow) {
                    return function () {
                        infowindow.setContent(this.html);
                        infowindow.open(map, marker);
                    };
                })(marker, this.html, infowindow));

                google.maps.event.addListener(marker, 'mouseout', (function (marker, content, infowindow) {
                    return function () {
                        infowindow.close();
                    };
                })(marker, this.html, infowindow));

                if (data[i].TouchType == "CC") {
                    var lineSymbol = {
                        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                    };
                    var symbolThree = {
                        path: 'M -2,-2 2,2 M 2,-2 -2,2',
                        strokeColor: '#292',
                        strokeWeight: 4
                    };
                    var lineCoordinates = [new google.maps.LatLng(data[i].LatitudeCCOrg, data[i].LongitudeCCOrg), new google.maps.LatLng(data[i].Latitude, data[i].Longitude)];

                    var flightPath = new google.maps.Polyline({
                        path: lineCoordinates,
                        icons: [{
                            icon: lineSymbol,
                            offset: '100%'
                        }],
                        map: map
                    });
                    line.push(flightPath);
                    lineByOrderNum[marker.orderNumber] = flightPath;

                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(data[i].LatitudeCCOrg, data[i].LongitudeCCOrg),
                        icon: data[i].ImageDefault,
                        map: map,
                        draggable: false,
                        driverId: data[i].driverId,
                        touchId: data[i].touchId,
                        orderNumber: data[i].orderNumber,
                        touchType: data[i].TouchType,
                        date: $("#txtDate").val(),
                        selectedIcon: data[i].ImageSelected,
                        defaultIcon: data[i].ImageDefault,
                        startTime: data[i].StartTime,
                        //facCode: data[i].FacCode,
                        globalSiteNum: data[i].GlobalSiteNum,
                        siteName: data[i].SiteName,
                        html: data[i].ToolTip
                    });
                    oms.addMarker(marker);
                }
            }
            var marker = new google.maps.Marker({
                position: homelatlng,
                icon: "/images/LocalLogistics/home.png",
                map: map
            });

            $(facilitieslatlng).each(function () {
                new google.maps.Marker({
                    position: this,
                    icon: "/images/LocalLogistics/home.png",
                    map: map
                });
            });

            var infowindow = new google.maps.InfoWindow();
            gmarkers = oms.getMarkers();
            
            google.maps.event.addListener(map, 'click', function () {
                infowindow.open(null, null);
            });

            oms.addListener('click', function (marker, event) {
                var isInArray = $.inArray(marker, selectedMarkers);
                if (isInArray == -1) {
                    selectedMarkers.push(marker);
                    marker.setIcon(marker.selectedIcon);

                    //If the user clicked touch is CC then we have to look for other end CC marker and need to select that
                    if (marker.touchType == "CC") {
                        $.each(gmarkers, function (i, o) {
                            if (marker != o && o.orderNumber == marker.orderNumber) {
                                selectedMarkers.push(o);
                                o.setIcon(o.selectedIcon);
                                return false;
                            }
                        });
                    }
                } else if (isInArray != -1) {
                    selectedMarkers.splice($.inArray(marker, selectedMarkers), 1);
                    marker.setIcon(marker.defaultIcon);

                    //If the user clicked touch is CC then we have to look for other end CC marker and need to select that
                    if (marker.touchType == "CC") {
                        $.each(gmarkers, function (i, o) {
                            if (marker != o && o.orderNumber == marker.orderNumber) {
                                selectedMarkers.splice($.inArray(marker, o), 1);
                                o.setIcon(o.defaultIcon);
                                return false;
                            }
                        });
                    }
                }

                currentMarker = marker;
                infowindow.close();
            });

        },
        error: function (error) {

            return;
        }
    });

    function setInfoWindowContent(data) {
        //If route is not locked then dont show Scheduled Time
        if (data.IsLoadLocked == true || data.IsLoadLocked == 'true' || data.IsLoadLocked == 'True')
            $("#htmlMapinfowindow").find('.scheduled-time').removeClass('hidden');
        else
            $("#htmlMapinfowindow").find('.scheduled-time').addClass('hidden');

        if (data.ProvidedETASettings != null && data.ProvidedETASettings.CanDisplayETAStatus == true) {
            $("#htmlMapinfowindow").find('.hdProvETA').removeClass('hidden');
            $("#htmlMapinfowindow").find('.ETAClass').removeClass('hidden');

            $("#htmlMapinfowindow").find('.hdProvETA').css('background-color', data.ProvidedETASettings.BackGroundColor);
            $("#htmlMapinfowindow").find('.hdProvETA').css('color', data.ProvidedETASettings.TextColor);
        }
        else {
            $("#htmlMapinfowindow").find('.hdProvETA').addClass('hidden');
            $("#htmlMapinfowindow").find('.ETAClass').addClass('hidden');
        }


        var htmll = $("#htmlMapinfowindow").html();

        return htmll.replace('{CustomerName}', data.CustomerName)
                    .replace('{TouchNumberDisp}', data.TouchNumberDisp)
                    .replace('{FromAddress}', data.FromAddress)
                    .replace('{ToAddress}', data.ToAddress)
                    .replace('{SiteLinkMileage}', data.SiteLinkMileage)
                    .replace('{TouchInstructions}', data.TouchInstructions)
                    .replace('{WeightStnReqDisp}', data.WeightStnReqDisp)
                    .replace('{DoorPOS}', data.DoorPOS)
                    .replace('{orderNumber}', String(data.orderNumber).split("-", 2)[1])
                    .replace('{DriverNm}', data.driverName)
                    .replace('{StartTime}', data.StartTime)
                    .replace('{ScheduledTime}', data.StartTimeHHMM)
                    .replace('{ProvidedETA}', data.ProvidedETASettings.ProvidedETARange);
    }
}

function showThisDriverMarkers(driverId) {
    for (var i = 0; i < window.gmarkers.length; i++) {
        if (window.gmarkers[i].driverId == driverId) {
            window.gmarkers[i].setVisible(true);
        } else {
            window.gmarkers[i].setVisible(false);
        }
    }
}

function validateTouchAddress() {
    //checking if the address is available for the touches and continuing if manager wants to continue else returning 
    var consolidatedWarningMessage = '';
    $.map(selectedMarkers, function (obj) {
        var toAdd = $(obj.html).find('#toAdd').text().split(',');

        if (!toAdd[0] && (typeof obj.html != 'undefined')) {
            consolidatedWarningMessage += $(obj.html).find('#tchType').text().slice(0, -7) + ' ' + $(obj.html).find('#CustNm').text() + ', ';
        }
    });

    if (consolidatedWarningMessage.length > 0) {
        consolidatedWarningMessage = consolidatedWarningMessage.slice(0, -2) + ' does not have a street address. Please update the address in Salesforce and Reload.';
        var conf = confirm(consolidatedWarningMessage);
        if (!conf) {
            return false;
        }
        else {
            return true;
        }
    }
}

//only for this page. REdo this when you got time.
function warningAlert(message) {
    var alertPop = $('#alert-popwar');
    var returnValue = true;

    $('#LLSAlertBody', alertPop).html(message);
    alertPop.show();

    returnValue = $('#WarOk').click(function () {
        alertPop.hide();
    });
    returnValue = $('#WarCancel').click(function () {
        alertPop.hide();
        return false;
    });
    returnValue = $('#imgClose').click(function () {
        alertPop.hide();
        return false;
    });

    return returnValue;
}

function getSelectedMarkersData() {
    var driverId = $('#dwAvailableDrivers').val();
    var loadId = $('option:selected', $('#dwAvailableDrivers')).attr('data-loadid');
    var globalsitenum = $('option:selected', $('#dwAvailableDrivers')).attr('data-globalsitenumber');
    var truckId = $('#dwAvailableTrucks').val();

    var touchIdsAndOrderNumbers = $.map(selectedMarkers, function (obj) {
        return obj.touchId + "|" + obj.orderNumber;
    });

    touchIdsAndOrderNumbers = $.unique(touchIdsAndOrderNumbers);

    var dt = {
        date: $("#txtDate").val(),
        driverId: driverId,
        loadId: loadId,
        truckId: truckId,
        driverSiteNumber: globalsitenum,
        touchIdsAndOrderNumbers: touchIdsAndOrderNumbers
    };

    return dt;
}

function hideAllMarkers() {
    $.each(gmarkers, function (indx, mrkr) {
        mrkr.setVisible(false);
    });
}

function showHideMarkers() {
    var time = $('.chkTouchType:checkbox:checked', $('#MapTimeFilter')).map(function () {
        return this.value;
    }).get();

    var driverIds = $('.chkMapDrivers:checkbox:checked', $('#dvpAvailableDriverDetails')).map(function () {
        return parseInt($(this).attr('data-driverid'));
    }).get();

    var facilities = $('.chkFacilityFilter:checkbox:checked', $('#MapFacilityFilter')).map(function () {
        return this.value;
    }).get();

    $.each(gmarkers, function (indx, mrkr) {
        var thismarkerline = lineByOrderNum[mrkr.orderNumber];

        if ($.inArray(mrkr.startTime, time) >= 0 && $.inArray(mrkr.driverId, driverIds) >= 0 && $.inArray(mrkr.globalSiteNum, facilities) >= 0) {
            mrkr.setVisible(true);
            if (thismarkerline && thismarkerline != undefined) {
                thismarkerline.setVisible(true);
            }
        }
        else {
            mrkr.setVisible(false);
            if (thismarkerline && thismarkerline != undefined) {
                thismarkerline.setVisible(false);
            }
        }
    });
}

// Custom Infot window
function attachInstructionText(markerpostion, text, markerImg, title) {

    var marker = new google.maps.Marker({
        position: markerpostion,
        icon: markerImg,
        // customInfo: num.toString(),
        title: title,
        draggable: true,
        map: map,
        zIndex: Math.round(markerpostion.lat() * -100000) << 5
    });
    gmarkersForStops.push(marker);

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(text);
        infowindow.open(map, marker);

    });
}

function markerClick(i) {
    google.maps.event.trigger(gmarkersForStops[i], "click");
}

function drawMap(originLocation, destLocation, waypts, callbackFuncation, allWaypts) {

    clearMapRouteRelatedVariables();

    window.tour.calcRoute(directionsService, directionsDisplay, allWaypts, callbackFuncation);
}

function Tour_Startup(stopsCollection) {
    if (!window.tour) {
        window.tour = {
            updateStops: function (newStops) {
                stopsColletion = newStops;
            },
            calcRoute: function (directionsService, directionsDisplay, stops, callbackFuncation) {
                var batches = [];
                var itemsPerBatch = 10;
                var itemsCounter = 0;
                var wayptsExist = stops.length > 0;

                while (wayptsExist) {
                    var subBatch = [];
                    var subItemsCounter = 0;
                    for (var j = itemsCounter; j < stops.length; j++) {
                        subItemsCounter++;
                        subBatch.push(stops[j]);

                        if (subItemsCounter == itemsPerBatch)
                            break;
                    }

                    itemsCounter += subItemsCounter;
                    batches.push(subBatch);
                    wayptsExist = itemsCounter < stops.length;

                    itemsCounter--;
                }

                //condition to handle if there is a single stop in the last batch
                if (stops.length % itemsPerBatch == 1) {
                    var batchlast = batches[batches.length - 1];
                    batchlast.shift();
                    batches[batches.length - 2] = batches[batches.length - 2].concat(batchlast);
                    batches.splice(-1, 1);//removing last element of the batches array as it is added to last before element.
                }

                var combinedResults;
                var unsortedResults = [{}];
                var directionsResultsReturned = 0;

                for (var k = 0; k < batches.length; k++) {
                    var lastIndex = batches[k].length - 1;
                    var start = batches[k][0].location;
                    var end = batches[k][lastIndex].location;

                    var wayPts = [];
                    wayPts = batches[k];
                    wayPts.splice(0, 1);
                    wayPts.splice(wayPts.length - 1, 1);

                    var request = {
                        origin: start,
                        destination: end,
                        waypoints: wayPts,
                        travelMode: window.google.maps.TravelMode.DRIVING
                    };
                    (function (kk) {
                        directionsService.route(request, function (result, status) {

                            if (status == window.google.maps.DirectionsStatus.OK) {
                                var unsortedResult = { order: kk, result: result };
                                unsortedResults.push(unsortedResult);
                                directionsResultsReturned++;

                                if (directionsResultsReturned == batches.length) {
                                    unsortedResults.sort(function (a, b) {
                                        return parseFloat(a.order) - parseFloat(b.order);
                                    });

                                    var Count = 0;
                                    for (var key in unsortedResults) {
                                        if (unsortedResults[key].result != null) {
                                            if (unsortedResults.hasOwnProperty(key)) {

                                                if (Count == 0)
                                                    combinedResults = unsortedResults[key].result;
                                                else {
                                                    combinedResults.routes[0].legs =
                                                        combinedResults.routes[0].legs.concat(unsortedResults[key].result.routes[0].legs);
                                                    combinedResults.routes[0].overview_path =
                                                        combinedResults.routes[0].overview_path.concat(unsortedResults[key].result.routes[0].OverView_Path);
                                                    combinedResults.routes[0].bounds =
                                                        combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getNorthEast());
                                                    combinedResults.routes[0].bounds =
                                                        combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getSouthWest());
                                                }
                                                Count++;
                                            }
                                        }
                                    }

                                    directionsDisplay.setDirections(combinedResults);

                                    computeTotalDistance(directionsDisplay.getDirections());

                                    if (callbackFuncation != undefined) {
                                        callbackFuncation();
                                    }
                                }

                            }
                            else if (status == google.maps.DirectionsStatus.MAX_WAYPOINTS_EXCEEDED) {
                                ShowAlert('You have selected max number of touches. Please try to add less touches and try again.')
                            } else {
                                ShowAlert('Error occurred while calculating route, Please reload the page and try again.')
                            }
                        });
                    })(k);

                }
            }
        }
    }
}

function computeTotalDistance(result) {
    var total = 0;

    var myroute = result.routes[0];
    if (myroute != undefined && myroute.legs != undefined && myroute.legs.length > 0) {
        for (var i = 0; i < myroute.legs.length; i++) {
            distanceArray.push(myroute.legs[i].duration);
            mapRouteArray.push(myroute.legs[i]);
        }
    }
}

function cleareMapRoute() {
    //directionsDisplay.setMap(null);
    directionsDisplay.setDirections({ routes: [] });
}

function clearMapRouteRelatedVariables() {
    cleareMapRoute();
    clearStopsMapVariables();
    clearMapLinesForCC();
}

function clearMapLinesForCC() {
    for (i = 0; i < line.length; i++) {
        line[i].setVisible(false);
        line[i].setMap(null); //or line[i].setVisible(false);
        line[i] = null;
    }
    line = [];
    lineByOrderNum = {};
}

function clearMapRelatedVariables() {
    clearMapRouteRelatedVariables();

    selectedMarkers = [];

    for (var i = 0; i < gmarkers.length; i++) {
        gmarkers[i].setVisible(false);
        gmarkers[i].setMap(null);
        gmarkers[i] = null;
    }
    gmarkers = [];
}

function clearStopsMapVariables() {
    for (var i = 0; i < gmarkersForStops.length; i++) {
        gmarkersForStops[i].setVisible(false);
        gmarkersForStops[i].setMap(null);
        gmarkersForStops[i] = null;
    }
    gmarkersForStops = [];
}