﻿$(document).ready(function () {

    initUsersGrid();

    $('#filterJqGrid').click(function (e) {
        e.preventDefault();
        filterGrid();
    });

    bindAjaxStartEvent();
});

function initUsersGrid() {
    $("#tblLstUsers").jqGrid({
        url: URLHelper.getUserList,
        mtype: 'GET',
        datatype: 'json',
        contentType: "application/json; charset-utf-8",
        colNames: ['ID', 'User Name', 'First Name', 'Last Name', 'Email', 'RoleId', 'Role Name', '', 'UniqueID'],
        colModel: [
                    { name: 'PK_UserId', index: 'PK_UserId', key: true, editable: false, hidedlg: true, hidden: true },
                    { name: 'Username', index: 'Username', editable: false, align: 'center', width: 120 },
                    { name: 'FirstName', index: 'FirstName', editable: true, align: 'center', width: 120 },
                    { name: 'LastName', index: 'LastName', editable: true, align: 'center', width: 120 },
                    { name: 'EmailAddress', index: 'EmailAddress', editable: false, align: 'center', width: 120 },
                    { name: 'FK_GroupID', index: 'FK_GroupID', editable: false, hidden: true },
                    { name: 'GroupName', index: 'GroupName', sortable: false, width: 120, editable: false, align: 'center', formatter: goToRoleEdit },
                    { name: 'Status', index: 'Status', hidden: false, search: false, sortable: false, align: 'center', formatter: formatActionButton },
                    { name: 'UniqueID', index: 'UniqueID', hidden: true, editable: false }],
        pager: $('#ptblLstUsers'),
        width: 920,
        rowNum: 20,
        rowList: [10, 20, 30, 60, 365],
        sortname: 'Username',
        sortorder: "asc",
        viewrecords: true,
        //caption: 'Users List',
        autowidth: true,
        gridview: true,
        id: "PK_UserId",
        height: "100%",
        multiselect: false,
        multiboxonly: true,
        loadonce: false, //when search is implementing from server side
        // datatype: 'local',
        loadError: function (xhr, st, err) {
            if (xhr.status == "200") return false;
            var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                maxwidth: '600',
                width: '600',
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        },
        gridComplete: function () {
            if (isFullAccess == "False") {
                $('#tblLstUsers').find('#btnActive').each(function () { $(this).attr('disabled', 'disabled'); });
            }
        },
        ondblClickRow: function (rowid, iRow, iCol, e) {
        }
    }).navGrid('#ptblLstUsers',
    {
        edit: false, add: false, del: false, search: false, refresh: false
    },
    null,
    null,
    null);
}

function goToRoleEdit(cellvalue, options, rowObject) {
    return "<a style='color: #2e92cf !important;text-decoration: underline;' href='/RoleManagement/CreateUpdateRole/?RoleId=" + rowObject[5] + "' >" + cellvalue + "</a>";
}

function filterGrid() {
    $("#tblLstUsers").clearGridData();

    $("#tblLstUsers").jqGrid('setGridParam',
        {
            page: 1,
            postData: {
                userName: $('#txtUserName').val(),
                RoleNo: $('#ddlRoles').val() == '-1' ? '0' : $('#ddlRoles').val(),
                isActiveOnly: $('#chkOnlyActive').prop('checked')
            }
        }).trigger("reloadGrid");
}

function resetform() {
    $('#txtUserName').val('');
    $('#ddlRoles').val('-1');
    $('#chkOnlyActive').attr('checked', true);
    $("#tblLstUsers").jqGrid('clearGridData');

    $("#tblLstUsers").jqGrid('setGridParam', {
        postData: {
            userName: '',
            RoleNo: 0,
            isActiveOnly: true
        }
    }).trigger("reloadGrid");
}

function formatActionButton(cellvalue, options, rowObject) {
    return "<input id='btnActive' type='button' value='" + cellvalue + "' onclick='SetActiveInactive(" + rowObject[0] + "," + rowObject[8] + ", this.value); return false;' style='margin: 2px;' /> <input type='button' id='editUser' value='Edit' onclick='editPage(true, " + rowObject[0] + "); return false;'/>";
}

function SetActiveInactive(UserID, UniqueID, Status) {
    var result = confirm("Are you sure you want to make it " + Status + " ?");
    if (result == true) {
        $.ajax({
            url: URLHelper.SetUserActiveInactiveUrl,
            data: {
                UserId: UserID,
                UniqueId:UniqueID,
                status: Status
            },
            success: function (retVal) {
                if (retVal.success == false) {
                    alert(retVal.message);
                }
                else {
                    $("#tblLstUsers").trigger("reloadGrid");
                }
            },
            error: function (error) {
                alert('Error occurred while loading information. Please reload the page and try again!');
            }
        });
    }
}

function editPage(isEdit, UserID) {
    if (isEdit && UserID > 0)
        window.location.href = "EditUser?UserID=" + UserID;
    else
        window.location.href = "AddUser";
}

function bindAjaxStartEvent() {
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
}

function handleSelectAllRadioBtn() {
    var totalModuleRow = $('.footable > tbody > tr').not(':first').filter(':visible');
    var rows = $('.footable > tbody > tr').not(':first').find(':radio:checked:visible');
    var singleValSelected = true;

    if (rows.length == totalModuleRow.length) {
        var prevSelectedAccPer = -1;
        rows.each(function () {
            var thisObjAccesslevel = $(this).val();
            if (prevSelectedAccPer != -1 && prevSelectedAccPer != thisObjAccesslevel) {
                //console.log(prevSelectedAccPer + '!= -1 && ' + prevSelectedAccPer + '!= ' + thisObjAccesslevel);
                singleValSelected = false;
            }
            prevSelectedAccPer = thisObjAccesslevel;
        });
    } else {
        singleValSelected = false;
    }


    if (singleValSelected == true && prevSelectedAccPer > 0) {
        if (prevSelectedAccPer == 4)
            $('#RdSecondaryFacility').prop("checked", true);
        else if (prevSelectedAccPer == 3)
            $('#RdReadAndWrite').prop("checked", true);
        else if (prevSelectedAccPer == 2)
            $('#RdRead').prop("checked", true);
        else if (prevSelectedAccPer == 1)
            $('#RdNoAccess').prop("checked", true);
    }
    else {
        $('#RdSecondaryFacility').prop("checked", false);
        $('#RdReadAndWrite').prop("checked", false);
        $('#RdRead').prop("checked", false);
        $('#RdNoAccess').prop("checked", false);
    }
}