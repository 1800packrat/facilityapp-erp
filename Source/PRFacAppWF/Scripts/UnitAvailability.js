﻿
$(document).ready(function () {

    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    $("#UnitAvailabilityStartDateIcon").click(function () {
        $("#UnitAvailabilityStartDate").focus();
    });
    $('#UnitAvailabilityStartDate').datepicker({
        onSelect: function (dateText, inst) {
            //$("#UnitAvailabilityStartDate").datepicker("getDate");
        }
    });

    $("#UnitFromDateIcon").click(function () {
        if ($(this).attr('disabled') != 'disabled')
            $("#UnitFromDate").focus();
    });
    $('#UnitFromDate').datepicker({
        onSelect: function (dateText, inst) {
            $("#UnitThroughDate").datepicker("option", "minDate",
            $("#UnitFromDate").datepicker("getDate"));
        }
    });

    $("#UnitThroughDateIcon").click(function () {
        if ($(this).attr('disabled') != 'disabled')
            $("#UnitThroughDate").focus();
    });
    $('#UnitThroughDate').datepicker({
        onSelect: function (dateText, inst) {
            $("#UnitFromDate").datepicker("option", "maxDate",
            $("#UnitThroughDate").datepicker("getDate"));
        }
    });

    $('#btnAddEdit').click(function () {
        resetAllControls();

        $('#UnitFromDate').datepicker("option", {
            minDate: new Date(),
            maxDate: null
        });
        $('#UnitThroughDate').datepicker("option", {
            minDate: new Date(),
            maxDate: null
        });

        $('#dlgAddUnitAvailability input').attr('value', '');
        $('#hdUnitAvailabilityInfoId').val(0);
        $('#ddlFacilites').removeAttr('disabled');
        $('#ddlUnitSize').removeAttr('disabled');
        $('#dlgAddUnitAvailability').css('display', 'block');
    });

    $("#CancelUnitAvailability").click(function () {
        $("#dlgAddUnitAvailability").css('display', 'none');
        return false;
    });

    $("#imgClose").click(function () {
        $("#dlgAddUnitAvailability").css('display', 'none');
        return false;
    });

    $('#SubmitUnitAvailability').click(function () {
        if (validateInput()) {
            saveUnitAvailability();
        }
    });

    $('#btnSearch').click(function () {
        initGridOnClick();
    });

    $('#imgNext').click(function () {
        loadNextDataWithGridColumnHeaderAndData();
    });

    $('#imgPrevious').click(function () {
        loadPreviousDataWithGridColumnHeaderAndData();
    });

    initGridOnClick();

    $('#ddlFacilites').change(function (e) {
        enableDisableSubmit(this.value);//enabling and disabling submit button based on facility access levels.
    });
});

function enableDisableSubmit(facilityID) {
    $.ajax({
        url: URLHelper.getFacilityAccessLevel,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({ FacilityID: facilityID }),
        success: function (isFullAccess) {
            if (isFullAccess == false) {
                $('#SubmitUnitAvailability').prop('disabled', 'disabled');
            } else {
                $('#SubmitUnitAvailability').removeAttr('disabled');
            }
        },
        error: function (error) {
            
        }
    });

}

function resetAllControls() {
    $('#ddlFacilites').val(-1);
    $('#ddlUnitSize').val(-1);
    $('#txtReportedBy').val('');
    $('#UnitFromDate').removeAttr('disabled');
    $('#UnitThroughDate').removeAttr('disabled');
    $('#UnitFromDateIcon').removeAttr('disabled');
    $('#UnitThroughDateIcon').removeAttr('disabled');
}

function validateInput() {
    if ($('#ddlFacilites :selected').val() == '-1') {
        alert('Please select facility.');
        return false;
    }
    if ($("#UnitFromDate").val() == '') {
        alert("Unit from date cannot be empty.");
        $("#UnitFromDate").focus();
        return false;
    }
    if ($("#UnitThroughDate").val() == '') {
        alert("Unit through date cannot be empty.");
        $("#UnitThroughDate").focus();
        return false;
    }

    if (new Date($("#UnitFromDate").val()) > new Date($("#UnitThroughDate").val())) {
        alert("From date cannot be less than through date.");
        return false;
    }

    if ($('#ddlUnitSize :selected').val() == '-1') {
        alert('Please select unitsize.');
        return false;
    }

    return true;
}

function saveUnitAvailability() {

    var getData = {
        UnitAvailabilityInfoId: $('#hdUnitAvailabilityInfoId').val(),
        FacilityName: $('#ddlFacilites option:selected').text(),
        StoreNo: $('#ddlFacilites option:selected').val(),
        UnitSize: $('#ddlUnitSize').val(),
        ReportedBy: $('#txtReportedBy').val(),
        FromDate: $('#UnitFromDate').val(),
        ThroughDate: $('#UnitThroughDate').val()
    };
    $.ajax({
        url: URLHelper.saveUnitAvailability,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({ uaInfo: getData }),
        success: function (retVal) {
            if (retVal.success == false) {
                alert(retVal.msg);
            } else {
                initGridOnClick();
                $('#dlgAddUnitAvailability').css('display', 'none');
            }
        },
        error: function (error) {
            alert('Error occurred while saving data. Please reload the page and try again!');
        }
    });
}

function addlink(cellValue, options, rowObject, action) {
    var id = rowObject[options.colModel.name + 'id'];
    var value = cellValue == null ? '' : cellValue;
    return "<a onclick='showPopup(" + id + ")' class='truckOHEdit' title='Click here to edit.' >" + value + "</a>";
}

function showPopup(id) {
    resetAllControls();
   
    $('#hdUnitAvailabilityInfoId').val(id);

    $('#dlgAddUnitAvailability').css('display', 'block');
    if (id != null) {
        populatePopupWindow(id);
    }
}

function populatePopupWindow(id) {
    $.ajax({
        url: URLHelper.getUnitAvailableInfo,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            UnitAvailabilityInfoId: id
        }),
        success: function (retVal) {
            if (retVal.UnitAvailabilityInfoId != 0) {
                $('#ddlFacilites').val(retVal.StoreNo);
                $('#ddlFacilites').attr('disabled', 'disabled');
                $('#ddlFacilites').trigger('change');

                var startDate = new Date(parseInt(retVal.FromDate.replace("/Date(", "").replace(")/", ""), 10));
                var endDate = new Date(parseInt(retVal.ThroughDate.replace("/Date(", "").replace(")/", ""), 10));

                $('#UnitFromDate').val(startDate.ToDate());
                $('#UnitThroughDate').val(endDate.ToDate());

                if (startDate <= new Date()) {
                    $('#UnitFromDate').attr('disabled', 'disabled');
                    $('#UnitFromDateIcon').attr('disabled', 'disabled');
                }
                if (endDate <= new Date()) {
                    $('#UnitThroughDate').attr('disabled', 'disabled');
                    $('#UnitThroughDateIcon').attr('disabled', 'disabled');
                }

                if (startDate > new Date() && endDate > new Date())
                {
                    $('#UnitThroughDate').datepicker('option', 'minDate', startDate);
                }
                else if (startDate < new Date() && endDate > new Date()) {
                    $('#UnitThroughDate').datepicker('option', 'minDate', new Date());
                }

                $('#UnitFromDate').datepicker('option', 'maxDate', endDate);
                

                $('#ddlUnitSize').val(retVal.UnitSize);
                $('#ddlUnitSize').attr('disabled', 'disabled');
                $('#txtReportedBy').val(retVal.ReportedBy);
            }
        },
        error: function (error) {
            alert('Error occurred while displaying data. Please reload the page and try again!');
        }
    });
}

function initGridOnClick() {
    var unitTypes = $('input[type=checkbox]:checked').map(function (i, e) {
        return $(e).val();
    }).get();

    var dateEnd = $('#UnitAvailabilityStartDate').val()
    $.ajax({
        url: URLHelper.searchUnitAvaiability,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            date: dateEnd,
            unitType: unitTypes
        }),
        success: function (data) {
            if (data.success == false) {
                alert('Error occurred while fetching trucks information. Please reload the page and try again!');
            } else {
                loadPartialGridData(data);
            }
        },
        error: function (error) {
            alert('Error occurred while fetching trucks information. Please reload the page and try again!');
        }
    });

}


function loadNextDataWithGridColumnHeaderAndData() {
    var unitTypes = $('input[type=checkbox]:checked').map(function (i, e) {
        return $(e).val();
    }).get();

    var onlyDate = $('#UnitAvailabilityStartDate').val();
    var dateEnd = new Date(onlyDate).addDays(7);

    $("#UnitAvailabilityStartDate").val(dateEnd.ToDate());

    $.ajax({
        url: URLHelper.searchUnitAvaiability,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            date: dateEnd,
            unitType: unitTypes
        }),
        success: function (data) {
            if (data.success == false) {
                alert('Error occurred while fetching trucks information. Please reload the page and try again!');
            } else {
                loadPartialGridData(data);
            }
        },
        error: function (error) {
            alert('Error occurred while fetching trucks information. Please reload the page and try again!');
        }
    });
}

function loadPreviousDataWithGridColumnHeaderAndData() {
    var unitTypes = $('input[type=checkbox]:checked').map(function (i, e) {
        return $(e).val();
    }).get();

    var onlyDate = $('#UnitAvailabilityStartDate').val();
    var dateEnd = new Date(onlyDate).addDays(-7);

    $("#UnitAvailabilityStartDate").val(dateEnd.ToDate());

    $.ajax({
        url: URLHelper.searchUnitAvaiability,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            date: dateEnd,
            unitType: unitTypes
        }),
        success: function (data) {
            if (data.success == false) {
                alert('Error occurred while fetching trucks information. Please reload the page and try again!');
            } else {
                loadPartialGridData(data);
            }
        },
        error: function (error) {
            alert('Error occurred while fetching trucks information. Please reload the page and try again!');
        }
    });
}

function loadPartialGridData(html) {
    $('#dvunitAvailability').html(html);
}

Date.prototype.ToDate = function () {
    return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
}
