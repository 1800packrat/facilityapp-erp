﻿var AjaxGlobalHandler = {
    Initiate: function (options) {
        $.ajaxSetup({ cache: false });
        // Ajax events fire in following order
        $(document).ajaxStart(function () {
            //$.blockUI({
            //    //message: options.AjaxWait.AjaxWaitMessage,
            //    //css: options.AjaxWait.AjaxWaitMessageCss
            //});
        }).ajaxSend(function (e, xhr, opts) {
        }).ajaxError(function (e, xhr, opts) {
            if (options.SessionOut.StatusCode == xhr.status) {
                document.location.href = options.SessionOut.RedirectUrl;
                return;
            }

            //$.colorbox({ html: options.AjaxErrorMessage });
        }).ajaxSuccess(function (e, xhr, opts) {
        }).ajaxComplete(function (e, xhr, opts) {
        }).ajaxStop(function () {
            //$.unblockUI();
        });
    }
};

var sessionoutRedirect = '/Home/Logout';
$(document).ready(function () {
    var options = {
        SessionOut: {
            StatusCode: 401,
            RedirectUrl: sessionoutRedirect
        }
    };

    AjaxGlobalHandler.Initiate(options);
});