﻿function GetAllStopsElementsByOrder() {
    var excudingBusyStops = [];
    var allStops = $('ol.stop-optimization li').map(function (i, o) {
        if (o.id.indexOf('hookup') == -1 && o.id.indexOf('unhook') == -1 && o.id.indexOf('grouped') == -1) {
            if (o.id.indexOf('BusyTime') == -1) {
                excudingBusyStops.push(o);
            }
            return o;
        }
    });

    var AllStops = {
        allStops: allStops,
        excudingBusyStops: excudingBusyStops
    }

    return AllStops;
}

function initOptimizeStops(loadData) {
    ShowOptStops();

    document.getElementById('divDataTableForStops').innerHTML = '';
    document.getElementById('divDataTableForStops').innerHTML = loadData;
    $(".opt-drivername").text($("#dwAvailableDrivers option:selected").text());

    bindStopOptimizationSortEvents();

    bindStopOptimizationContextMenuEvents();

    bindBusyTimeShowDeleteEvents();

    clearMapRouteRelatedVariables();

    distanceArray = [];
    mapRouteArray = [];

    selectSelectedDriverInFilter();

    handleLockLoadEventsOnLoad(document.getElementById('leftControlPanel_OptStops'));

    var isLocked = $('#btnLock', $('#leftControlPanel_OptStops')).attr('data-islocked');
    //If the route is locked then we need to send back to left nav menu
    if (isLocked == true || isLocked == "true" || isLocked == 'True') {
        setTimeout(function () { drawRouteForStops(AdjustScheduleTimeWithOutUpdateTimings); }, 300);
    }

    //BindEditDriverDialog();
    setHeight();
}

function bindStopOptimizationSortEvents() {
    $(".group-items-sortable").sortable({
        containment: "parent",
        items: "> li.sortchild:not(Completed)",
        tolerance: "pointer",
        connectWith: '.group-items-sortable'
    });
}

function bindStopOptimizationContextMenuEvents() {
    $.contextMenu({
        selector: '.busytime-handler',
        callback: function (key, options) {
            addBreakStop(this, key, options);
        },
        items: {
            "break60above": { name: "60 Minutes Break Time Above", icon: "lunchbreak", comment: "60 Minutes Break Time", value: 60 },
            "break60below": { name: "60 Minutes Break Time Below", icon: "lunchbreak", comment: "60 Minutes Break Time", value: 60 },
            "break30above": { name: "30 Minutes Break Time Above", icon: "lunchbreak", comment: "30 Minutes Break Time", value: 30 },
            "break30below": { name: "30 Minutes Break Time Below", icon: "lunchbreak", comment: "30 Minutes Break Time", value: 30 },
            "break20above": { name: "20 Minutes Break Time Above", icon: "lunchbreak", comment: "20 Minutes Break Time", value: 20 },
            "break20below": { name: "20 Minutes Break Time Below", icon: "lunchbreak", comment: "20 Minutes Break Time", value: 20 },
            "break10above": { name: "10 Minutes Break Time Above", icon: "lunchbreak", comment: "10 Minutes Break Time", value: 10 },
            "break10below": { name: "10 Minutes Break Time Below", icon: "lunchbreak", comment: "10 Minutes Break Time", value: 10 }
            //"sep1": "---------",
            //"teabreakabove": { name: "Tea Break Above", icon: "teabreak", value: 20 },
            //"teabreakbelow": { name: "Tea Break Below", icon: "teabreak", value: 20 }
        }
    });
}

function handleLockLoadEventsOnLoad(stops) {
    var isLock = $('#leftControlPanel_OptStops').attr('data-islocked');

    handleLockLoadEvents(stops, isLock);
}

function handleLockLoadEvents(stops, isLock) {

    if (isLock == 'true' || isLock == true) {
        $('.busytime-handler').contextMenu(false);

        $('.busytimerowtoggle').off('mouseenter mouseleave');

        $('.group-items-sortable', stops).sortable("disable");
        $('.group-items-sortable', stops).css("cursor", "default");

        //Hide Back btn and Save btns        
        $('#btnOptimizeStops').hide();
    } else {
        $('.busytime-handler').contextMenu(true);

        $('.busytimerowtoggle').hover(HandlerInBusyTimeStop, HandlerOutBusyTimeStop);

        $('.group-items-sortable', stops).sortable("option", "disabled", false);
        $('.group-items-sortable', stops).css("cursor", "move");

        //Show Back btn and Save btns
        $('#btnOptimizeStops').show();
    }
}

function CalculateHowManyroutesPerTouch() {
    var allStops = GetAllStopsElementsByOrder().excudingBusyStops;

    var route = [];
    var routsCnt = 0;

    $.each(allStops, function (i, o) {
        routsCnt = 1;
        if (i > 0) {
            var lastElemtn = allStops[i - 1];

            var latlong = getLatLongValuesForStops(this, lastElemtn);

            var lstAddId = latlong.prevStopDestAddId;
            var thisAddId = latlong.thisStopOrgAddressId;

            if (lstAddId != thisAddId) {
                routsCnt++;
            }
        }

        var routeCalStop = {
            index: i,
            coutRoute: routsCnt,
            stopid: $(this).attr('data-stopid')
        };

        route.push(routeCalStop);
    });

    return route;
}

function getLatLongValuesForStops(stopObj, prevStopObj) {
    var orgLat;
    var orgLong;
    var destLat;
    var destLong;
    var lastAddId;
    var thisStopOrgAddressId;
    var thisStopDestAddressId;

    var isOriginWarehouse = $(stopObj).find(".origin-warehouse-address");

    if (isOriginWarehouse.length > 0) {
        orgLat = $('option:selected', isOriginWarehouse).attr('data-orglat');
        orgLong = $('option:selected', isOriginWarehouse).attr('data-orglong');
        thisStopOrgAddressId = $('option:selected', isOriginWarehouse).attr('value');
    } else {
        orgLat = $(stopObj).find(".orgLat").val();
        orgLong = $(stopObj).find(".orgLong").val();
        thisStopOrgAddressId = $(stopObj).find(".orgAddressID").val();
    }

    var isDestinationWarehouse = $(stopObj).find(".destination-warehouse-address");

    if (isDestinationWarehouse.length > 0) {
        destLat = $('option:selected', isDestinationWarehouse).attr('data-destlat');
        destLong = $('option:selected', isDestinationWarehouse).attr('data-destlong');
        thisStopDestAddressId = $('option:selected', isDestinationWarehouse).attr('value');
    } else {
        destLat = $(stopObj).find(".destLat").val();
        destLong = $(stopObj).find(".destLong").val();
        thisStopDestAddressId = $(stopObj).find(".destAddressID").val();
    }

    var prevStopDestAddId;
    if (prevStopObj) {
        var isPreviousStopDestWarehouse = $(prevStopObj).find(".destination-warehouse-address");

        if (isPreviousStopDestWarehouse.length > 0) {
            prevStopDestAddId = $('option:selected', isPreviousStopDestWarehouse).attr('value');
        } else {
            prevStopDestAddId = $(prevStopObj).find(".destAddressID").val();
        }
    }

    return { orgLat: orgLat, orgLong: orgLong, destLat: destLat, destLong: destLong, thisStopOrgAddressId: thisStopOrgAddressId, thisStopDestAddressId: thisStopDestAddressId, prevStopDestAddId: prevStopDestAddId };
}

function drawRouteForStops(callBackAdjustScheduleTime) {
    var waypts = [];
    var allWaypts = [];
    var originLocation;
    var destLocation;

    var allStops = GetAllStopsElementsByOrder().excudingBusyStops;
    var busyTime = 0;

    $.each(allStops, function (i, o) {
        if (i == 0) {
            //originLocation = new google.maps.LatLng($(this).find(".orgLat").val(), $(this).find(".orgLong").val());
            var latlong = getLatLongValuesForStops(this);

            originLocation = new google.maps.LatLng(latlong.orgLat, latlong.orgLong);

            waypts.push({
                //location: new google.maps.LatLng($(this).find(".destLat").val(), $(this).find(".destLong").val()),
                location: new google.maps.LatLng(latlong.destLat, latlong.destLong),
                stopover: true
            });

            allWaypts.push({
                //location: new google.maps.LatLng($(this).find(".destLat").val(), $(this).find(".destLong").val()),
                location: new google.maps.LatLng(latlong.orgLat, latlong.orgLong),
                stopover: true
            });
            allWaypts.push({
                //location: new google.maps.LatLng($(this).find(".destLat").val(), $(this).find(".destLong").val()),
                location: new google.maps.LatLng(latlong.destLat, latlong.destLong),
                stopover: true
            });

        } else if (i == allStops.length - 1) {
            var lastElemtn = allStops[i - 1];

            var latlong = getLatLongValuesForStops(this, lastElemtn);

            var lstAddId = latlong.prevStopDestAddId;
            var thisAddId = latlong.thisStopOrgAddressId;

            var orgLat = latlong.orgLat;
            var orgLng = latlong.orgLong;
            //var stopId = $(this).attr('data-stopid');

            if (lstAddId != thisAddId) {
                waypts.push({
                    location: new google.maps.LatLng(orgLat, orgLng),
                    stopover: true
                });

                allWaypts.push({
                    location: new google.maps.LatLng(orgLat, orgLng),
                    stopover: true
                });
            }
            destLocation = new google.maps.LatLng(latlong.destLat, latlong.destLong);
            allWaypts.push({
                location: new google.maps.LatLng(latlong.destLat, latlong.destLong),
                stopover: true
            });            
        } else {
            var lastElemtn = allStops[i - 1];

            var latlong = getLatLongValuesForStops(this, lastElemtn);

            var lstAddId = latlong.prevStopDestAddId;
            var thisAddId = latlong.thisStopOrgAddressId;

            var orgLat = latlong.orgLat;
            var orgLng = latlong.orgLong;

            if (lstAddId != thisAddId) {
                var orgLatLong = { location: new google.maps.LatLng(orgLat, orgLng), stopover: true };
                waypts.push(orgLatLong);
                allWaypts.push(orgLatLong);
            }

            var destLat = latlong.destLat;
            var destLng = latlong.destLong;

            var destLatLong = { location: new google.maps.LatLng(destLat, destLng), stopover: true };
            waypts.push(destLatLong);
            allWaypts.push(destLatLong);
        }
    });

    drawMap(originLocation, destLocation, waypts, callBackAdjustScheduleTime, allWaypts);
}

function stopsDrawRoute() {
    //If the route is already locked then we have to refresh the stop info and then generate route
    var isLocked = $('#btnLock', $('#leftControlPanel_OptStops')).attr('data-islocked');//$('#btnLock').attr('data-islocked');

    //If the route is locked then we need to send back to left nav menu
    if (isLocked == true || isLocked == "true" || isLocked == 'True') {
        initOptimizationLoad();
        return false;
    }

    drawRouteForStops(AdjustScheduleTime);
}

function getFormattedDate(date) {
    var month = date.getMonth() + 1;
    month = month.length == 1 ? '0' + month : month;
    var dt = date.getDate();
    dt = dt.length == 1 ? '0' + dt : dt;
    var dateString = date.formatHHMMSS(); 

    return dateString;
}

function AdjustScheduleTimeWithOutUpdateTimings() {    
    var stopTimes = validateAndGetStopTiming();

    //Hide all markers bcz once you generate map then it automatically add its markers. 
    hideAllMarkers();
}

function AdjustScheduleTime() {    
    var stopTimes = validateAndGetStopTiming();

    //Hide all markers bcz once you generate map then it automatically add its markers. 
    hideAllMarkers();

    $.each(stopTimes.data, function (i, o) {

        var stopStatus = $(this).find('.StopStatus').val();
        if (stopStatus != undefined && (stopStatus.toLowerCase() == 'completed' || stopStatus.toLowerCase() == 'skipped')) {

        }
        else {
            var startDt = new Date(o.stopStartDateTime);
            var endDt = new Date(o.stopEndDateTime);

            var scheduleDtTime = getFormattedDate(startDt) + ' - ' + getFormattedDate(endDt);
            var liObj = $('#SortStopId_' + o.stopID);

            liObj.find('.stop-scheduled-time').html(scheduleDtTime);
            liObj.find('.stop-estimated-time').html(scheduleDtTime);
        }
    });
}

function SaveOptimizedStops() {
    if (mapRouteArray == undefined || mapRouteArray.length <= 0) {
        ShowAlert('Please generate route map and then optimize it.');
        return false;
    }
    clearMapRouteRelatedVariables();
   
    var validation = GetStopTimings();

    var postData = {
        isOptimized: true,
        stopAndTime: validation.data,
        loadId: $('option:selected', $('#dwAvailableDrivers')).attr('data-loadid')
    }

    $.ajax({
        url: URLHelper.SaveStopOptimizationUrl,
        //data: { stopSeqId: stopIds, isOptimized: $('option:selected', $('#dwAvailableDrivers')).attr('data-isoptimized') },
        type: 'POST',
        data: JSON.stringify(postData),
        traditional: true,
        datatype: "json",
        contentType: 'application/json; charset=utf-8',
        success: function (rtVal) {
            if (rtVal == "Failure") {
                ShowAlert('Error occurred while saving stops. Please reload the page and try again!');
            }
            else {
                ShowSuccessAlert('Successfully created load for this route.');
               
                ShowLeftNavMainMenu();

                afterOptimizationLockToMapMain(postData.loadId, $('option:selected', $('#dwAvailableDrivers')).attr('data-driverid'))
            }
        },
        error: function (error) {
        }
    });
}

function GetStopTimings() {
    var driverStartTime = $('#driverStartTimeStopLevel').val();
    driverStartTime = driverStartTime == undefined || driverStartTime.length == 0 ? "8:30:00" : driverStartTime;

    var loadDate = $('#txtDate').val();
    var totalTodayTimeObj = new Date(loadDate + " " + driverStartTime);//Needs to change this line

    //alert(totalTodayTimeObj.formatMMDDYYYYHHMMSS());

    var allStops = GetAllStopsElementsByOrder().allStops;
    var allStopsWithNoofRouteCalc = CalculateHowManyroutesPerTouch();

    var stopAndTimes = [];
    var arrMsgs = [];
    var index = -1;
    var busyTime = 0;
    var weightTicketTouchIds = new Array();
    var indexForInfoWindow = -1;
    var markerimage = "";
    var sequence = 0;
    var orgAddressId;
    var destAddressId;
    var distance;

    gmarkersForStops = new Array();

    //changes for LL-38 enhancement, reassigning totalTodayTimeObj to system time
    var chkCurrentDate = new Date();
    //if ((totalTodayTimeObj.toDateString() === chkCurrentDate.toDateString()) && (totalTodayTimeObj < chkCurrentDate)) {
    //    totalTodayTimeObj = chkCurrentDate;
    //}

    allStops.each(function (i, o) {
        orgAddressId = '';
        destAddressId = '';
        distance = 0;

        if (o.id.indexOf('BusyTime') > -1) {
            busyTime = parseInt($(this).attr('data-breaktime'));
            //var startDatetime = totalTodayTimeObj.toUTCString();//toLocaleFormat();
            var startDatetime = totalTodayTimeObj.formatMMDDYYYYHHMMSS();//.toLocaleString();

            totalTodayTimeObj.setMinutes(totalTodayTimeObj.getMinutes() + parseInt(busyTime));
            //var endDatetime = totalTodayTimeObj.toUTCString();//.toLocaleFormat();
            var endDatetime = totalTodayTimeObj.formatMMDDYYYYHHMMSS();//.toLocaleString();

            var stopAndTime = {
                orgAddressId: orgAddressId,
                destAddressId: destAddressId,
                sequence: sequence,
                stopID: $(this).attr('data-stopid'),//$(this.id).attr('data-touchid');
                stopStartDateTime: startDatetime,
                stopEndDateTime: endDatetime,
                actualDistance: distance
            };
            stopAndTimes.push(stopAndTime);
            sequence++;

            return;
        }

        //index++;
        var time = 0;
        var touchTimeZone = $(this).find('.touchTimeZone').val();
        var stopType = $(this).find('.stopType').val();
        var TouchStopTypeAcronym = $(this).find('.TouchStopTypeAcronym').val();
        var QORID = $(this).find('.QORID').val();
        var TouchNumberDisp = $(this).find('.TouchNumberDisp').val();
        var thisStopId = $(this).attr('data-stopid');
        var weightstnTime = 0;
        var startDatetime;
        var endDatetime;
        var arrivtime;
        var leavtime;

        var stopStatus = $(this).find('.StopStatus').val().toLowerCase();
        if (stopStatus == 'completed' || stopStatus == 'skipped') {
            var stopScheduleStartTime = $(this).find('.stopScheduledStartTime').val();
            var stopScheduleEndTime = $(this).find('.stopScheduledEndTime').val();

            //var stopEstimatedStartTime = $(this).find('.stopEstimatedStartTime').val();
            //var stopEstimatedEndTime = $(this).find('.stopEstimatedEndTime').val();

            var stopActualStartTime = $(this).find('.stopActualStartTime').val();
            var stopActualEndTime = $(this).find('.stopActualEndTime').val();

            totalTodayTimeObj = new Date(stopScheduleStartTime);

            startDatetime = (new Date(stopScheduleStartTime)).formatMMDDYYYYHHMMSS();//.toLocaleString();
            arrivtime = (new Date(stopScheduleStartTime)).formatHHMMSS(); //toLocaleTimeString();

            //var endDatetime = totalTodayTimeObj.toUTCString();//.toLocaleFormat();
            endDatetime = (new Date(stopScheduleEndTime)).formatMMDDYYYYHHMMSS();//.toLocaleString();
            leavtime = (new Date(stopScheduleEndTime)).formatHHMMSS(); //toLocaleTimeString();

            //var noOfCntObj;
            var elementFount;
            var exist = $.grep(allStopsWithNoofRouteCalc, function (obj) {
                return obj.stopid == thisStopId;
            });
            if (exist.length == 1) {
                //noOfCntObj = exist[0];
                for (var ii = 0; ii < exist[0].coutRoute; ii++) {
                    index++;
                    //travellingTime += Math.round(mapRouteArray[index].duration.value / 60);
                }
            }

            if (stopActualEndTime && stopActualEndTime.length > 0) {
                totalTodayTimeObj = new Date(stopActualEndTime);
            }

            //if stop
            if ((totalTodayTimeObj.toDateString() === chkCurrentDate.toDateString()) && (totalTodayTimeObj < chkCurrentDate)) {
                totalTodayTimeObj = chkCurrentDate;
            }
            //console.log(totalTodayTimeObj);
        }
        else {
            var thisStopTouchId = $(this).attr('data-touchid');
            if ($(this).attr('data-weightstnreq') == "True") {
                if ($.inArray(thisStopTouchId, weightTicketTouchIds) == -1) {
                    weightstnTime = 20;
                    weightTicketTouchIds.push(thisStopTouchId);
                }
            }

            var handlingTime = parseInt($('#handlingTime' + stopType).val());
            handlingTime = (handlingTime == undefined || isNaN(handlingTime) || handlingTime == '' ? 0 : handlingTime);
            //var travellingTime = Math.round(mapRouteArray[index].duration.value / 60);
            var travellingTime = 0;   //actiontimeDropoff = $('#handlingTime' + touchType + 'DF').val();

            //var noOfCntObj;
            var elementFount;
            var exist = $.grep(allStopsWithNoofRouteCalc, function (obj) {
                return obj.stopid == thisStopId;
            });

            if (exist.length == 1) {
                //noOfCntObj = exist[0];
                for (var ii = 0; ii < exist[0].coutRoute; ii++) {

                    index++;
                    travellingTime += Math.round(mapRouteArray[index].duration.value / 60);
                    distance += parseInt(mapRouteArray[index].distance.value);
                }
            }

            totalTodayTimeObj.setMinutes(totalTodayTimeObj.getMinutes() + parseInt(handlingTime));
            //var startDatetime = totalTodayTimeObj.toUTCString();//.toLocaleFormat();
            startDatetime = totalTodayTimeObj.formatMMDDYYYYHHMMSS();//.toLocaleString();
            arrivtime = totalTodayTimeObj.formatHHMMSS(); //toLocaleTimeString();

            totalTodayTimeObj.setMinutes(totalTodayTimeObj.getMinutes() + parseInt(travellingTime) + parseInt(weightstnTime));
            //var endDatetime = totalTodayTimeObj.toUTCString();//.toLocaleFormat();
            endDatetime = totalTodayTimeObj.formatMMDDYYYYHHMMSS();//.toLocaleString();
            leavtime = totalTodayTimeObj.formatHHMMSS(); //toLocaleTimeString();

            var latlong = getLatLongValuesForStops(this);
            orgAddressId = latlong.thisStopOrgAddressId;
            destAddressId = latlong.thisStopDestAddressId;
        }

        var stopAndTime = {
            orgAddressId: orgAddressId,
            destAddressId: destAddressId,
            sequence: sequence,
            isOpenStop: !(stopStatus == 'completed' || stopStatus == 'skipped'),
            stopID: $(this).attr('data-stopid'),//$(this.id).attr('data-touchid');
            stopStartDateTime: startDatetime,
            stopEndDateTime: endDatetime,
            actualDistance: (distance / 1609.344).toFixed(1) //This will convert to from meters to miles
        };
        stopAndTimes.push(stopAndTime);

        sequence++;
    });

    var rtValue = {};

    if (arrMsgs.length > 0) {
        rtValue.success = false;
        rtValue.errormsg = arrMsgs.join('.\n');
        rtValue.data = stopAndTimes;
    } else {
        rtValue.success = true;
        rtValue.errormsg = '';
        rtValue.data = stopAndTimes;
    }

    return rtValue;
}

function validateAndGetStopTiming() {
    var driverStartTime = $('#driverStartTimeStopLevel').val();
    driverStartTime = driverStartTime == undefined || driverStartTime.length == 0 ? "8:30:00" : driverStartTime;

    var loadDate = $('#txtDate').val();
    var totalTodayTimeObj = new Date(loadDate + " " + driverStartTime);//Needs to change this line

    //alert(totalTodayTimeObj.formatMMDDYYYYHHMMSS());

    var allStops = GetAllStopsElementsByOrder().allStops;
    var allStopsWithNoofRouteCalc = CalculateHowManyroutesPerTouch();

    var stopAndTimes = [];
    var arrMsgs = [];
    var index = -1;
    var busyTime = 0;
    var weightTicketTouchIds = new Array();
    var marNum = 64;
    var marMaxNum = 90;//default value should be 90
    var colorNum = 0;
    var colors = { red: 0, green: 1, purple: 2, grey: 3, orange: 4, yellow: 5, brown: 6, black: 7, white: 8 };
    var indexForInfoWindow = -1;
    var markerimage = "";
    var sequence = 0;
    var orgAddressId;
    var destAddressId;

    gmarkersForStops = new Array();

    //changes for LL-38 enhancement, reassigning totalTodayTimeObj to system time
    var chkCurrentDate = new Date();
    //if ((totalTodayTimeObj.toDateString() === chkCurrentDate.toDateString()) && (totalTodayTimeObj < chkCurrentDate)) {
    //    totalTodayTimeObj = chkCurrentDate;
    //}

    // console.log(totalTodayTimeObj < chkCurrentDate);

    allStops.each(function (i, o) {
        orgAddressId = '';
        destAddressId = '';

        if (o.id.indexOf('BusyTime') > -1) {
            busyTime = parseInt($(this).attr('data-breaktime'));
            //var startDatetime = totalTodayTimeObj.toUTCString();//toLocaleFormat();
            var startDatetime = totalTodayTimeObj.formatMMDDYYYYHHMMSS();//.toLocaleString();

            totalTodayTimeObj.setMinutes(totalTodayTimeObj.getMinutes() + parseInt(busyTime));
            //var endDatetime = totalTodayTimeObj.toUTCString();//.toLocaleFormat();
            var endDatetime = totalTodayTimeObj.formatMMDDYYYYHHMMSS();//.toLocaleString();

            var stopAndTime = {
                orgAddressId: orgAddressId,
                destAddressId: destAddressId,
                sequence: sequence,
                stopID: $(this).attr('data-stopid'),//$(this.id).attr('data-touchid');
                stopStartDateTime: startDatetime,
                stopEndDateTime: endDatetime
            };
            stopAndTimes.push(stopAndTime);
            sequence++;

            return;
        }

        //index++;
        var time = 0;
        var touchTimeZone = $(this).find('.touchTimeZone').val();
        var stopType = $(this).find('.stopType').val();
        var TouchStopTypeAcronym = $(this).find('.TouchStopTypeAcronym').val();
        var QORID = $(this).find('.QORID').val();
        var TouchNumberDisp = $(this).find('.TouchNumberDisp').val();
        var thisStopId = $(this).attr('data-stopid');
        var weightstnTime = 0;
        var startDatetime;
        var endDatetime;
        var arrivtime;
        var leavtime;

        var stopStatus = $(this).find('.StopStatus').val().toLowerCase();
        if (stopStatus == 'completed' || stopStatus == 'skipped') {
            var stopScheduleStartTime = $(this).find('.stopScheduledStartTime').val();
            var stopScheduleEndTime = $(this).find('.stopScheduledEndTime').val();

            //var stopEstimatedStartTime = $(this).find('.stopEstimatedStartTime').val();
            //var stopEstimatedEndTime = $(this).find('.stopEstimatedEndTime').val();

            var stopActualStartTime = $(this).find('.stopActualStartTime').val();
            var stopActualEndTime = $(this).find('.stopActualEndTime').val();

            totalTodayTimeObj = new Date(stopScheduleStartTime);

            startDatetime = (new Date(stopScheduleStartTime)).formatMMDDYYYYHHMMSS();//.toLocaleString();
            arrivtime = (new Date(stopScheduleStartTime)).formatHHMMSS(); //toLocaleTimeString();

            //var endDatetime = totalTodayTimeObj.toUTCString();//.toLocaleFormat();
            endDatetime = (new Date(stopScheduleEndTime)).formatMMDDYYYYHHMMSS();//.toLocaleString();
            leavtime = (new Date(stopScheduleEndTime)).formatHHMMSS(); //toLocaleTimeString();

            //var noOfCntObj;
            var elementFount;
            var exist = $.grep(allStopsWithNoofRouteCalc, function (obj) {
                return obj.stopid == thisStopId;
            });
            if (exist.length == 1) {
                //noOfCntObj = exist[0];
                for (var ii = 0; ii < exist[0].coutRoute; ii++) {
                    index++;
                    //travellingTime += Math.round(mapRouteArray[index].duration.value / 60);
                }
            }

            if (stopActualEndTime && stopActualEndTime.length > 0) {
                totalTodayTimeObj = new Date(stopActualEndTime);
            }

            //if stopactualendtime is less than current system then we need to take system time else we are taking stopactualendtime.
            if ((totalTodayTimeObj.toDateString() === chkCurrentDate.toDateString()) && (totalTodayTimeObj < chkCurrentDate)) {
                totalTodayTimeObj = chkCurrentDate;
            }
            //console.log(totalTodayTimeObj);
        }
        else {
            var thisStopTouchId = $(this).attr('data-touchid');
            if ($(this).attr('data-weightstnreq') == "True") {
                if ($.inArray(thisStopTouchId, weightTicketTouchIds) == -1) {
                    weightstnTime = 20;
                    weightTicketTouchIds.push(thisStopTouchId);
                }
            }

            var handlingTime = parseInt($('#handlingTime' + stopType).val());
            handlingTime = (handlingTime == undefined || isNaN(handlingTime) || handlingTime == '' ? 0 : handlingTime);
            //var travellingTime = Math.round(mapRouteArray[index].duration.value / 60);
            var travellingTime = 0;   //actiontimeDropoff = $('#handlingTime' + touchType + 'DF').val();

            //var noOfCntObj;
            var elementFount;
            var exist = $.grep(allStopsWithNoofRouteCalc, function (obj) {
                return obj.stopid == thisStopId;
            });

            if (exist.length == 1) {
                //noOfCntObj = exist[0];               
                for (var ii = 0; ii < exist[0].coutRoute; ii++) {
                    index++;
                    travellingTime += Math.round(mapRouteArray[index].duration.value / 60);
                }
            }

            totalTodayTimeObj.setMinutes(totalTodayTimeObj.getMinutes() + parseInt(handlingTime));
            //var startDatetime = totalTodayTimeObj.toUTCString();//.toLocaleFormat();
            startDatetime = totalTodayTimeObj.formatMMDDYYYYHHMMSS();//.toLocaleString();
            arrivtime = totalTodayTimeObj.formatHHMMSS(); //toLocaleTimeString();

            totalTodayTimeObj.setMinutes(totalTodayTimeObj.getMinutes() + parseInt(travellingTime) + parseInt(weightstnTime));
            //var endDatetime = totalTodayTimeObj.toUTCString();//.toLocaleFormat();
            endDatetime = totalTodayTimeObj.formatMMDDYYYYHHMMSS();//.toLocaleString();
            leavtime = totalTodayTimeObj.formatHHMMSS(); //toLocaleTimeString();

            var latlong = getLatLongValuesForStops(this);
            orgAddressId = latlong.thisStopOrgAddressId;
            destAddressId = latlong.thisStopDestAddressId;
        }

        var stopAndTime = {
            orgAddressId: orgAddressId,
            destAddressId: destAddressId,
            sequence: sequence,
            isOpenStop: !(stopStatus == 'completed' || stopStatus == 'skipped'),
            stopID: $(this).attr('data-stopid'),//$(this.id).attr('data-touchid');
            stopStartDateTime: startDatetime,
            stopEndDateTime: endDatetime
        };
        stopAndTimes.push(stopAndTime);
        sequence++;

        var lastElement = allStops[i - 1];
        var lastElementDestAddId = $(lastElement).find('.destAddressID').val();
        var thisElementOrdAddId = $(this).find('.orgAddressID').val();

        if (lastElementDestAddId == thisElementOrdAddId) {
            var spanEle = $("<img onclick='markerClick(" + indexForInfoWindow + ")' class='stops-map-markericon' src='" + markerimage + "'></img>");
            $(this).find('.org-markericon-text').html(spanEle);
        }
        else {
            var orgAddressType = $(this).find('.orgAddressType').val();

            if (orgAddressType == 'Warehouse') {
                markerimage = 'http://maps.google.com/mapfiles/kml/pal2/icon10.png';
            }
            else {
                marNum += 1;
                if (marNum > marMaxNum) {
                    colorNum += 1;
                    marNum = 65;
                }

                markerimage = retMarkerImg(colorNum, marNum);
            }

            attachInstructionText(mapRouteArray[index].start_location,
                '<div style="height:180px;">Driver at: ' + mapRouteArray[index].start_address +
                '<br>Touch: ' + TouchNumberDisp +
                '<br>QORID: ' + QORID +
                '<br>Stop Type: ' + stopType +
                '<br>Distance to next point: ' + (mapRouteArray[index].distance.value / 1609.34).toFixed(2) + ' miles' +
                '<br>Travel Time to next point: ' + mapRouteArray[index].duration.text +
                '<br>Arrival Time: ' + arrivtime +
                '<br>Leave Time: ' + leavtime +
                '</div>',
                markerimage, TouchStopTypeAcronym);

            indexForInfoWindow++;
            var spanEle = $("<img onclick='markerClick(" + indexForInfoWindow + ")' class='stops-map-markericon' src='" + markerimage + "'></img>");
            $(this).find('.org-markericon-text').html(spanEle);
        }


        var destAddressType = $(this).find('.destAddressType').val();
        if (destAddressType == 'Warehouse') {
            markerimage = 'http://maps.google.com/mapfiles/kml/pal2/icon10.png';
        }
        else {
            marNum += 1;
            if (marNum > marMaxNum) {
                colorNum += 1;
                marNum = 65;
            }

            markerimage = retMarkerImg(colorNum, marNum);
        }
        attachInstructionText(mapRouteArray[index].end_location,
            '<div style="height:180px;">Driver at: ' + mapRouteArray[index].end_address +
            '<br>Touch: ' + TouchNumberDisp +
            '<br>QORID: ' + QORID +
            '<br>Stop Type: ' + stopType +
            '<br>Distance to next point: ' + (mapRouteArray[index].distance.value / 1609.34).toFixed(2) + ' miles' +
            '<br>Travel Time to next point: ' + mapRouteArray[index].duration.text +
            '<br>Arrival Time: ' + arrivtime +
            '<br>Leave Time: ' + leavtime +
            '</div>',
            markerimage, TouchStopTypeAcronym);

        indexForInfoWindow++;
        var spanEle = $("<img onclick='markerClick(" + indexForInfoWindow + ")' class='stops-map-markericon' src='" + markerimage + "'></img>");
        $(this).find('.dest-markericon-text').html(spanEle);

        if (touchTimeZone == "AM" && totalTodayTimeObj.getHours() > 12) {
            arrMsgs.push($(this).find('.main-head').html() + ': is an AM touch scheduled outside of the AM window');
        } else if (touchTimeZone == "PM" && totalTodayTimeObj.getHours() <= 11) {
            arrMsgs.push($(this).find('.main-head').html() + ': is an PM touch scheduled outside of the PM window');
        }
    });
    // clearMapRouteRelatedVariables();
    var rtValue = {};

    if (arrMsgs.length > 0) {
        rtValue.success = false;
        rtValue.errormsg = arrMsgs.join('.\n');
        rtValue.data = stopAndTimes;
    } else {
        rtValue.success = true;
        rtValue.errormsg = '';
        rtValue.data = stopAndTimes;
    }

    return rtValue;
}

function retMarkerImg(colorNumber, charNum) {
    var markerimage;

    switch (colorNumber) {
        case 0: //red
            markerimage = 'https://www.google.com/mapfiles/marker' + String.fromCharCode(charNum) + '.png';
            break;
        case 1: //green
            markerimage = 'https://www.google.com/mapfiles/marker_green' + String.fromCharCode(charNum) + '.png';
            break;
        case 2: //purple
            markerimage = 'https://www.google.com/mapfiles/marker_purple' + String.fromCharCode(charNum) + '.png';
            break;

        case 3: //grey
            markerimage = 'https://www.google.com/mapfiles/marker_grey' + String.fromCharCode(charNum) + '.png';
            break;
        case 4: //orange
            markerimage = 'https://www.google.com/mapfiles/marker_orange' + String.fromCharCode(charNum) + '.png';
            break;
        case 5: //yellow
            markerimage = 'https://www.google.com/mapfiles/marker_yellow' + String.fromCharCode(charNum) + '.png';
            break;
        case 6: //brown
            markerimage = 'https://www.google.com/mapfiles/marker_brown' + String.fromCharCode(charNum) + '.png';
            break;
        case 7: //black
            markerimage = 'https://www.google.com/mapfiles/marker_black' + String.fromCharCode(charNum) + '.png';
            break;
        default: //white
            markerimage = 'https://www.google.com/mapfiles/marker_white' + String.fromCharCode(charNum) + '.png';
    }

    return markerimage;
}


function addBreakStop(currentObj, key, options) {
    //var breakTime = options.items.lunchbreakabove.value;
    var breakTime = options.items[key].value;
    var commentMsg = options.items[key].comment;
    var currentLoadInfo = getCurrentLoadInfo();

    var postData = {
        date: currentLoadInfo.loadDate,
        loadId: currentLoadInfo.loadId,
        breakTimeInMinutes: breakTime,
        comment: commentMsg
    };

    $.ajax({
        url: URLHelper.AddBreakTouchStopUrl,
        type: 'POST',
        data: JSON.stringify(postData),
        traditional: true,
        datatype: "json",
        contentType: 'application/json; charset=utf-8',
        //contentType: 'application/json; charset=utf-8',
        beforeSend: function () {
        },
        success: function (rtVal) {
            if (rtVal == "Failure") {
                ShowAlert('Error occurred while saving data. Please reload the page and try again!');
            }
            else {
                var li = $('<li></li>');
                li.text(commentMsg);
                li.addClass('busytime');
                li.addClass('busytimerowtoggle');

                li.attr('id', 'BusyTime_' + rtVal.TouchStopId);
                li.attr('data-stopid', rtVal.TouchStopId);
                li.attr('data-touchid', rtVal.TouchId);
                li.attr('data-breaktime', breakTime);

                var img = $('<img/>');
                img.addClass('busytimerowdelete');
                img.addClass('busytimerow_' + rtVal.TouchStopId);
                img.prop('src', '/Images/close.png');
                img.css({ 'float': 'right', 'display': 'none' });
                li.append(img);
                //<img class="busytimerowdelete busytimerow_@stop.TouchStopId" alt="X" src="/Images/close.png" style="float: right; display: none;">

                //ShowAlert(key);
                //ShowAlert(key.indexOf('above'));

                if (key.indexOf('above') >= 0)
                    $(currentObj).before(li);
                else
                    $(currentObj).after(li);

                bindBusyTimeShowDeleteEvents(li);
            }
        },
        error: function (error) {
        },
        complete: function () {
        }
    });
    //switch (key) {
    //    case "lunchbreakabove":
    //        currentObj

    //        break;
    //    case "lunchbreakbelow":
    //        break;
    //    case "teabreakabove":
    //        break;
    //    case "teabreakabove":
    //        break;
    //}
}

function bindBusyTimeShowDeleteEvents(li) {
    $('.busytimerowtoggle').hover(HandlerInBusyTimeStop, HandlerOutBusyTimeStop);

    if (li) {
        $('.busytimerowdelete', li).click(HandlerDeleteBusyTimeTouchAndStop);
    } else {
        $('.busytimerowdelete').click(HandlerDeleteBusyTimeTouchAndStop);
    }
}

function HandlerInBusyTimeStop(o) {
    var guid = $(this).attr('data-stopid');
    $('.busytimerow_' + guid).show();
}

function HandlerOutBusyTimeStop(o) {
    var guid = $(this).attr('data-stopid');
    $('.busytimerow_' + guid).hide();
}

function HandlerDeleteBusyTimeTouchAndStop(o) {
    if (confirm('Are you sure do you want to remove stop?')) {
        var stopid = $(this).closest("li").attr('data-stopid');

        var postData = {
            stopId: stopid
        }

        $.ajax({
            url: URLHelper.RemoveBreakTouchAndStopUrl,
            type: 'POST',
            data: JSON.stringify(postData),
            traditional: true,
            datatype: "json",
            contentType: 'application/json; charset=utf-8',
            beforeSend: function () {
            },
            success: function (rtVal) {
                if (rtVal == "Failure") {
                    ShowAlert('Error occurred while removing stops. Please reload the page and try again!');
                }
                else {
                    $('#BusyTime_' + stopid).remove();
                    // $('#BusyTime_' + stopid).remove();
                }
            },
            error: function (error) {
            },
            complete: function () {
            }
        });
    }
}

function onChangeStopDestinationAddress(obj, isGrouped) {
    var nextLI;

    if (isGrouped == true) {
        var groupOL = $(obj).closest("ol");
        nextLI = groupOL.closest("li").next();
    } else {
        nextLI = $(obj).closest("li").next();
    }

    var orgDropdownInNextLI = nextLI.find('.origin-warehouse-address');

    if (orgDropdownInNextLI != undefined && orgDropdownInNextLI.length > 0) {
        orgDropdownInNextLI.val($(obj).val());
    }
}