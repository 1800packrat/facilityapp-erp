﻿function initControls() {
    $("#StartDateIcon").click(function () {
        $("#StartDate").focus();
    });
    $('#StartDate').datepicker({
    });

    $("#StartDateIconJQ").click(function () {
        $("#StartDateJQ").focus();
    });
    $('#StartDateJQ').datepicker({
    });

    $("#EditStartDateIcon").click(function () {
        $("#EditStartDate").focus();
    });
    $('#EditStartDate').datepicker({
    });

    $("form").on("submit", function (event) {
        event.preventDefault();
        loadAverageMPHListData();
    });

    $('#modalSavehours').click(function () {
        saveASData();
    });
}

function loadAverageMPHListData() {
    $.ajax({
        url: URLHelper.SearchAvgMPHUrl,
        data: {
            Date: $('#StartDate').val(),
            SortCol: $('#hidSortCol').val(),
            SortDir: $('#hidSortDir').val(),
        },
        success: function (retVal) {
            if (retVal == "error") {
                alert('Error occurred while loading information. Please reload the page and try again!');
            } else {
                //$('#divAverageMPH').html(retVal);
                //Remove previousloaded values
                $('#hdCorpRow').remove();

                //Append new result to UI
                $('#divAverageMPH').html(retVal);

                //Find corp head 
                var corpHead = $('#hdCorpRow', $('#divAverageMPH'));
                $('#tblHead').append(corpHead);
                $('#hdCorpRow', $('#divAverageMPH')).remove();

                //Bind table
                $("#fixTable").tableHeadFixer();
            }
        },
        error: function (error) {
            alert('Error occurred while loading information. Please reload the page and try again!');
        }
    });
}


function initPopup() {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $('a[data-modal-id]').click(function (e) {
        e.preventDefault();

        $("body").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7);
        $('#popup').fadeIn();
    });

    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });
    $(window).resize(function () {
        $(".modal-box").css({
            top: 10,
            left: ($(window).width() - $(".modal-box").outerWidth()) / 2
        });
    });
    $(window).resize();
}

var averageMPHPopupSetting = {
    actions: {
        adjustCenter: function () {
            /* $(".modal-box").css({
                 //top: top,
                 //left: left
                 top: (($(window).height() / 2) - ($(".modal-box").outerHeight() / 2)) + $(window).scrollTop(),
                 left: (($(window).width() / 2) - ($(".modal-box").outerWidth() / 2))
             });*/
        },
        adjustCenterWithDelay: function () {
            setTimeout('averageMPHPopupSetting.actions.adjustCenter()', 700);
        },
        adjustTransferDimentions: function () {
            averageMPHPopupSetting.actions.adjustCenterWithDelay();
        },
        adjustNoserviceDimentions: function () {
        },
        adjustHourChangeDimentions: function () {
            averageMPHPopupSetting.actions.adjustCenterWithDelay();

            var gridId = 'list';
            var grid = $('#gbox_' + gridId).parent();
            var gridParentWidth = grid.width();
            $('#' + gridId).jqGrid('setGridWidth', gridParentWidth);

            var gridParentHeight = grid.height();
            if (gridParentHeight <= 800) {

                var height = ($(window).height() - $(".modal-box").outerHeight()) - 100;

                $('#truck-ophours-reason-from').css('max-height', height + 'px'); //set max height
                $('#truck-ophours-reason-from').css('overflow-y', 'scroll'); //set max height
                $('#truck-ophours-reason-from').css('overflow-x', 'hidden'); //set max height
            }
        }
    }
}

function showAveragaeMPHPopup(facilityId) {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popup').fadeIn();
    $("#corporateDiv").css('display', 'none');
    $('#dropdownDiv').removeAttr("style");
    $("#ddlFacilityDetails option[value='0']").remove();
    $("#ddlFacilityDetails").val(facilityId);
    $('#ddlFacilityDetails').trigger('change');
    averageMPHPopupSetting.actions.adjustHourChangeDimentions();
}

function showCorporatePopup(facilityId) {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popup').fadeIn();
    $("#ddlFacilityDetails").append($('<option></option>').val(facilityId).html(''));
    $("#ddlFacilityDetails").val(facilityId);
    $("#dropdownDiv").css('display', 'none');
    $('#corporateDiv').removeAttr("style");
    loadAvgMPHChangeGrid(facilityId);
    averageMPHPopupSetting.actions.adjustHourChangeDimentions();
}

function loadAvgMPHChangeGrid(facilityId) {

    $("#list").clearGridData();
    if (facilityId == 0)
        $("#UpdateCorporateDefaults").css('display', 'none');
    else
        $("#UpdateCorporateDefaults").css('display', 'block');

    $("#list").jqGrid('setGridParam', {
        mtype: 'GET', editurl: URLHelper.SaveTempHourChange, onclickSubmit: batchSumbit, url: URLHelper.GetFacilityAvgMPHHistoryUrl, datatype: 'json'
                                , postData: { facilityId: facilityId }
    }).trigger("reloadGrid");
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function initAvgMPHChangeGrid() {

    var checkStartEndDatevalidation = function (value, colname) {
        var startTime = $("input#Startdate").val();
        var endTime = $("input#Enddate").val();
        //alert($("#enddate_chkbox").is(':checked'));
        var endDataNullChecked = $("#enddate_chkbox").is(':checked');
        var vValue = $('input#AvgMPH').val();
        if (vValue != '') {
            if (!isNumeric(vValue)) { return [false, "AvgMPH: Enter valid value.", ""]; }
        }
        if (endDataNullChecked == false) {
            if (endTime == '') {
                return [false, "End Date: Field is required.", ""];
            } else if (startTime != '' && endTime != '') {
                if (new Date(startTime) > new Date(endTime)) {
                    return [false, "Start date should be less than End date.", ""];
                }
            }
        }

        return [true, "", ""];
    };

    var updateDialog = {
        url: URLHelper.SaveTempHourChange
                , closeAfterEdit: true
                , reloadAfterSubmit: true
                , closeAfterAdd: true
                , closeAfterEdit: true
                , onclickSubmit: function (params) {
                    var ajaxData = {};
                    var list = $("#list");
                    var selectedRow = list.getGridParam("selrow");

                    rowData = list.getRowData(selectedRow);

                    ajaxData = { dates: jQuery("#list").jqGrid('getGridParam', 'selarrrow'), CalDate: rowData.CalDate };

                    return ajaxData;
                }
                , afterComplete: function (o) {
                    json = jQuery.parseJSON(o.responseText);
                    if (json.success == true)
                        alert('Truck operating details have been saved successfully.');
                    loadDataWithGridColumnHeaderAndData();
                }
                , modal: true
                , width: "400"
    };

    $("#list").jqGrid({
        colNames: ['RowID', 'MPH', 'Start Date', 'End Date', 'Hours of Operation', 'Mileage', 'State', 'Facility Id', ''],
        colModel: [
              { name: 'RowID', index: 'RowID', key: true, editable: false, hidedlg: true, search: false, hidden: true },
              { name: 'AvgMPH', index: 'AvgMPH', width: 180, editable: true, editrules: { required: true }, hidedlg: true, search: false, align: 'center' },
              {
                  name: 'Startdate', index: 'Startdate', width: 180, editable: true, align: 'center', editrules: { custom: true, custom_func: checkStartEndDatevalidation, required: true }, hidedlg: true, search: false,
                  editoptions: {
                      size: 20,
                      dataInit: function (el) {
                          $(el).datepicker({ dateFormat: 'm/d/yy', minDate: new Date() });
                      },
                      defaultValue: function () {
                          var currentTime = new Date();
                          var month = parseInt(currentTime.getMonth() + 1);
                          //month = month <= 9 ? "0" + month : month;
                          var day = currentTime.getDate();
                          //day = day <= 9 ? "0" + day : day;
                          var year = currentTime.getFullYear();
                          return month + "/" + day + "/" + year;
                      },
                      readonly: true
                  }
              },
                {
                    name: 'Enddate', index: 'Enddate', width: 180, editable: true, align: 'center', editrules: { custom: true, custom_func: checkStartEndDatevalidation }, hidedlg: true, search: false,
                    editoptions: {
                        size: 20,
                        dataInit: function (el) {
                            $(el).datepicker({ dateFormat: 'm/d/yy', minDate: new Date() });
                        },
                        defaultValue: function () {
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            //month = month <= 9 ? "0" + month : month;
                            var day = currentTime.getDate();
                            //day = day <= 9 ? "0" + day : day;
                            var year = currentTime.getFullYear();
                            return month + "/" + day + "/" + year;
                        },
                        readonly: true
                    }
                },
                { name: 'Hours of Operation', index: 'Operatinghours', width: 180, search: false, align: 'center' },
                { name: 'Mileage', index: 'Mileage', search: false, align: 'center' },
                { name: 'State', index: 'State', hidden: true, search: false },
                { name: 'FacilityId', index: 'FacilityId', hidden: true, search: false },
                { name: 'Action', index: 'Action', hidden: false, search: false, formatter: formatActionButton('RowID') }
        ]
            , pager: $('#listPager')
            , width: 910
            , rowNum: 10
            , rowList: [10, 20, 30, 60, 365]
            , sortname: 'Startdate'
            , sortorder: "desc"
            , viewrecords: true
            , autoencode: true
            , caption: 'Average MPH'
            , autowidth: true
            , gridview: true
            , id: "RowID"
            , height: "100%"
            , datatype: 'local'
            , loadError: function (xhr, st, err) {
                if (xhr.status == "200") return false;
                var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                    modal: false,
                    title: 'Error',
                    maxwidth: '600',
                    width: '600',
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
            , gridComplete: function () {

                var ids = $("#list").jqGrid('getDataIDs');
                var CurDate = new Date().withoutTime();
                for (var i = 0; i < ids.length; i++) {
                    var rowId = ids[i],
                        endDT = $("#list").jqGrid('getCell', rowId, 'Enddate'),
                        activeBtn = "<input type='button' class='row-editbutton' style='background-color: #008287; border-radius: 4px; color: white;' value='Edit' onclick='showEditPopup(" + rowId + ")' />";

                    if (endDT != '' && CurDate > new Date(endDT).withoutTime()) { // Inactive
                        activeBtn = "<input type='button' style='background-color: ##8289; border-radius: 4px; color: white;' value='Edit' disabled onclick='showEditPopup(" + rowId + ")' />";
                    }
                    $("#list").jqGrid('setRowData', rowId, { Action: activeBtn });
                };
                $(".ui-jqgrid-sortable").each(function () {
                    this.style.color = "#0073ea";
                });
            },
        loadComplete: function (dt) {
            if (dt.isfullaccess == false) {
                $('#list').find('.row-editbutton').each(function () { $(this).prop('disabled', 'disabled'); });
                $('#modalUpdateselectedhours').addClass('not-active');
                $("#UpdateCorporateDefaults").addClass('not-active');
            } else {
                //$('#list').find('.row-editbutton').removeAttr("disabled");
                $('#list').find('.row-editbutton').each(function () { $(this).removeAttr('disabled'); });
                $('#modalUpdateselectedhours').removeClass('not-active');
                $("#UpdateCorporateDefaults").removeClass('not-active');
            }
        }
    }).navGrid('#listPager',
            {
                edit: false, add: false, del: false, refresh: false, search: false
            },
            updateDialog,
            null,
            null);


    jQuery("#modalUpdateselectedhours").click(function () {
        showEditPopup();
    });
    jQuery("#UpdateCorporateDefaults").click(function () {
        showDefaultPopup();
    });
}

function formatActionButton(cellvalue, options, rowObject) {
    return "<input type='button' value='Edit' onclick='showEditPopup(); return false;' style='margin: 2px;' />";
}

function showDefaultPopup() {
    if ($('#ddlFacilityDetails').val() != -1) {
        jQuery("#list").jqGrid('editGridRow', "new", {
            height: 240
            , closeAfterEdit: true
            , closeAfterAdd: true
            , reloadAfterSubmit: true
            , afterShowForm: function (formid) {
                $(".EditButton").css('text-align', 'left');
                $(".navButton").css('display', 'none');
                $("#editmodlist").css('height', '150px');
            }
            , onclickSubmit: DefaultSubmit
            , url: URLHelper.SetDefaultValueURL
            , afterComplete: function (o) {
                var json = jQuery.parseJSON(o.responseText);
                if (json.success == true)
                    loadAverageMPHListData();
            }
        });
        $('#AvgMPH').val('0');
        $('#tr_AvgMPH').css('display', 'none');
        $('#Enddate').val('9999-01-01');
        $('#tr_Enddate').css('display', 'none');
        $("#edithdlist span").text("Set to Corporate Default");
    } else alert("Please select a Facility");
}

function showEditPopup(rowId) {
    if ($('#ddlFacilityDetails').val() != -1) {
        var rows = [];//jQuery("#list").jqGrid('getGridParam', 'selarrrow');
        rows.push(rowId);
        $('#tr_Startdate').css('display', 'inline-block');
        $('#tr_Enddate').css('display', 'inline-block');
        $('#tr_AvgMPH').css('display', 'inline-block');
        $('#tr_Startdate').removeAttr("style");
        $('#tr_Enddate').removeAttr("style");
        $('#tr_AvgMPH').removeAttr("style"); // .removeProp("display");
        $('#Startdate').val('');
        $('#Enddate').val('');
        $('#AvgMPH').val('');
        $('#enddate_chkbox').attr('checked', false);
        jQuery("#list").jqGrid('editGridRow', rows, {
            height: 240,
            closeAfterEdit: true,
            closeAfterAdd: true,
            reloadAfterSubmit: true,
            beforeShowForm: function (formid) {
                var chkboxexists = $('#Enddate').parent().find('#enddate_chkbox');
                if (chkboxexists.length == 0) {
                    var chkbox = "<input type='checkbox' title='Select for open ended' style='margin-left: 5px;' id='enddate_chkbox' >";
                    $('#Enddate').parent().append(chkbox);

                    $('#enddate_chkbox').on('change', function () { // on change of state
                        if (this.checked) // if changed state is "CHECKED"
                        {
                            $('#Enddate').attr("disabled", "disabled");
                            $('#Enddate').val('');
                        } else {
                            $('#Enddate').removeAttr("disabled");
                        }
                    })
                }
            },
            afterShowForm: function (formid) {
                $("#AvgMPH", formid).focus();
                $(".EditButton").css('text-align', 'left');
                $(".navButton").css('display', 'none');
                $("#editmodlist").css('height', '240px');
                var Cdt = new Date();
                var StrDt = $('#Startdate').val();
                if (Cdt > new Date(StrDt)) {
                    var MM = Cdt.getMonth() + 1;
                    var dd = Cdt.getDate();
                    var yy = Cdt.getFullYear();
                    $('#Startdate').val([MM, dd, yy].join('/'));
                }
            }
            , onclickSubmit: batchSumbit
            , url: URLHelper.SaveAMPHourChange
            , afterComplete: function (o) {
                var json = jQuery.parseJSON(o.responseText);
                if (json.success == true)
                    loadAverageMPHListData();
                $("#AvgMPH", formid).focus();
            }
        });
    } else alert("Please select a Facility");
}

var batchSumbit = function (params) {
    var ajaxData = {};
    var data;
    //var rowids = jQuery("#list").jqGrid('getGridParam', 'selarrrow');

    //if (rowids != null) {
    return ajaxData = { hidfacilityid: $('#ddlFacilityDetails').val(), hidNullEndDate: $("#enddate_chkbox").is(':checked') };
    //}
}

var DefaultSubmit = function (params) {
    var ajaxData = {};
    var data;
    return ajaxData = { hidfacilityid: $('#ddlFacilityDetails').val() };
}

function SortColumn(ColumnName) {
    $.ajaxSetup({ cache: false });
    if (ColumnName == "")
        ColumnName = 'Facility';
    $("#hidSortCol").val(ColumnName);
    var sortDir = $("#hidSortDir").val();
    $('img[id^="Image"]').css('display', 'none');
    if (sortDir.toLowerCase() == "asc") {
        $("#hidSortDir").val("DESC");
        $('#Image' + ColumnName).attr('src', "/Images/arrow-down.png");
        $('#Image' + ColumnName).css('display', 'inline-block');
    }
    else {
        $("#hidSortDir").val("ASC");
        $('#Image' + ColumnName).attr('src', "/Images/arrow-up.png");
        $('#Image' + ColumnName).css('display', 'inline-block');
    }
    loadAverageMPHListData();
}


$(document).ready(function () {
    $.ajaxSetup({ cache: false });

    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    //loadAverageMPHListData();

    initControls();

    initAvgMPHChangeGrid();

    initPopup();

    SortColumn('');

    $("#fixTable").tableHeadFixer();
});

Date.prototype.ToDate = function () {
    return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
}

Date.prototype.ToTime = function () {
    var hr = parseInt(this.getHours());
    var ampm = hr > 11 ? 'PM' : 'AM';
    var hr = hr > 12 ? (hr - 12) : hr;
    var mm = parseInt(this.getMinutes()) > 9 ? this.getMinutes() : '0' + this.getMinutes();
    return hr + ":" + mm + " " + ampm;
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

Date.prototype.withoutTime = function () {
    var d = new Date(this);
    d.setHours(0, 0, 0, 0, 0);
    return d
}