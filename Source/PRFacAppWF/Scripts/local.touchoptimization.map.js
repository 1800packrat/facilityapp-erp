﻿function GetAllTouchesElementsByOrder() {
    var allStops = $('ol.touch-optimization li').map(function (i, o) {
        if (o.id.indexOf('hookup') == -1 && o.id.indexOf('unhook') == -1 && o.id.indexOf('grouped') == -1) {
            return o;
        }
    });

    return allStops;
}

function initOptimizeTouches(Touches) {
    optimizeLoadDisplayTouches(Touches);

    $(".opt-drivername").text($("#dwAvailableDrivers option:selected").text());
    showThisDriverMarkers($("#dwAvailableDrivers option:selected").val());

    bindTrailerHookandUnHookEvents();

    selectSelectedDriverInFilter();

    handleLockLoadTouchesEventsOnLoad(document.getElementById('leftControlPanel'));

    setHeight();
}

function optimizeLoadDisplayTouches(loadData) {
    showOptimizedTouches();

    document.getElementById('divDataTable').innerHTML = '';
    document.getElementById('divDataTable').innerHTML = loadData;

    $('#driverStartTime').timepicker({
        timeFormat: 'h:i A',
        minTime: '12:00 AM', // 11:45:00 AM,
        maxTime: '11:30 PM',
        // startTime: new Date(0, 0, 0, 8, 30, 0), // 3:00:00 PM - noon
        interval: 15 // 15 minutes
    });

    //As per Eric, we need to show "Start At" time based on below condition.
    //If Driver Schedule Start time less than current time then we should display current time by default
    let isLoadLocked = $('#driverStartTime').attr('data-islocked');
    if (isLoadLocked == "false") {
        let drStartTime = $('#driverStartTime').attr('data-driverschedulestarttime');
        var loadDate = $('#txtDate').val();
        var dtDrStartTime = new Date(loadDate + " " + drStartTime);
        let timeToDisplay = dtDrStartTime;
        if (dtDrStartTime < (new Date())) {
            timeToDisplay = new Date();
        }

        $('#driverStartTime').val(timeToDisplay.formatHHMMSS());
    }

    //OptimizeTouches('open');

    bindTouchOptimizationSortEvents();

    // Sort the children
    //$(".group-items-sortable").sortable({
    //    containment: "parent",
    //    items: "> li",
    //    tolerance: "pointer",
    //    connectWith: '.group-items-sortable'
    //});

    $("#btnGroupTchs").click(function () {
        //GroupTouchs();
        GroupTouchWithCheck();
    });
}

function handleLockLoadTouchesEventsOnLoad(touches) {
    var isLock = $('#leftControlPanel').attr('data-islocked');

    handleLockLoadTouchesEvents(touches, isLock);
}

function handleLockLoadTouchesEvents(touches, isLock) {

    if (isLock == 'true' || isLock == true) {
        $('.touch-optimization-sortable', touches).sortable("disable");
        $('.touch-optimization-sortable', touches).css("cursor", "default");
    } else {
        $('.touch-optimization-sortable', touches).sortable("option", "disabled", false);
        $('.touch-optimization-sortable', touches).css("cursor", "move");
    }
}

function AddTrailerOptimizeTouches(driverId) {
    var id = 'tbldriver-' + driverId;
    var trailerId = $('#Trailer-' + driverId).val();
    var trailerName = $('#Trailer-' + driverId + ' option:selected').text();

    if (trailerId <= 0) {
        ShowAlert('Please select trailer and then add...!');
    } else {
        var now = new Date();
        var guid = now.getHours() + '' + now.getMinutes() + '' + now.getSeconds();

        $('ol.touch-optimization > li:first').before('<li id="unhook_' + guid + '" data-trailerid="' + trailerId + '" data-guid="' + guid + '" class="trailerrowtoggle sortparent"><span style="font-weight:bold;">Un-Hook - Trailer (' + trailerName + ') <img style="float:right; display:none;" src="/Images/close.png" alt="X" class="trailerrowdelete trailerrow_' + guid + '"/></span></li>');
        $('ol.touch-optimization > li:first').before('<li id="hookup_' + guid + '" data-trailerid="' + trailerId + '" data-guid="' + guid + '" class="trailerrowtoggle sortparent"><span style="font-weight:bold;">Hookup - Trailer (' + trailerName + ')<img style="float:right; display:none;" src="/Images/close.png" alt="X" class="trailerrowdelete trailerrow_' + guid + '"/></span></li>');

        bindTouchOptimizationSortEvents();

        bindTrailerHookandUnHookEvents();
        //addTrailer();
    }
}

function bindTouchOptimizationSortEvents() {
    $("ol.touch-optimization-sortable").sortable({
        connectWith: "ol",
        items: "> li.sortparent",
        start: function (event, ui) {
            var parent = ui.item.parent();
            parent.addClass('modified_ul');
        },
        stop: function (event, ui) {
            var parent = ui.item.parent();
            parent.addClass('modified_ul');
        },
        update: function (event, ui) {
            //     $('#update_channel_order').removeAttr('disabled').removeClass('button_big_disabled');
        }
    });
}

function GroupTouchWithCheck() {
    var touchsLi = []; //SortTouchId_@
    $("input:checked", $('.touch-optimization')).each(function () {
        var thisLi = $(this).closest("li");

        touchsLi.push(thisLi);
    });

    //touchType
    //unitSize

    var sizeWarningFound = false;
    var sizeErrorFound = false;
    var sizeMsg = "";
    for (var index = 0; index < touchsLi.length; index++) {
        var current = touchsLi[index];
        var next = touchsLi[index + 1];

        var currentTouchType = $(current).find('.touchType').val();
        var nextTouchType = $(next).find('.touchType').val();

        if (currentTouchType == "RE" && nextTouchType == "DE") {
            var currentContSize = parseInt($(current).find('.unitSize').val());
            var nextContSize = parseInt($(next).find('.unitSize').val());

            if (currentContSize > nextContSize) {
                sizeWarningFound = true;
                var currentTouchTypeAcronym = $(current).find('.OrderNumberDisp').val() + " (" + currentContSize + ")";
                var nextTouchTypeAcronym = $(next).find('.OrderNumberDisp').val() + " (" + nextContSize + ")";

                sizeMsg = "Grouped container sizes do not match: " + currentTouchTypeAcronym + " to " + nextTouchTypeAcronym;
            }
            else if (currentContSize < nextContSize) {
                sizeErrorFound = true;
                var currentTouchTypeAcronym = $(current).find('.OrderNumberDisp').val() + " (" + currentContSize + ")";
                var nextTouchTypeAcronym = $(next).find('.OrderNumberDisp').val() + " (" + nextContSize + ")";

                sizeMsg = "Cannot group RE DE due to mismatch in container size: " + currentTouchTypeAcronym + " to " + nextTouchTypeAcronym;
            }
        }
    }

    if (sizeErrorFound == true) {
        ShowAlert(sizeMsg);
    } else if (sizeWarningFound == true) {
        ShowWarningAlertWithCustAction(sizeMsg, GroupTouchs);
    } else {
        GroupTouchs();
    }
}

function GroupTouchs() {

    var touchIds = []; //SortTouchId_@
    $("input:checked", $('.touch-optimization')).each(function () {
        var thisLi = $(this).closest("li");

        touchIds.push(thisLi.attr('data-touchid'));
    });

    if (touchIds.length < 2) {
        ShowAlert('Please select atleast two touches and then group it.');
        return false;
    }

    //var allTouchIds = []; //SortTouchId_@
    //$("input:checked", $('.touch-optimization')).each(function () {
    //    touchIds.push($(this).closest("li").attr('data-touchid'));
    //});


    var allTouchIdsOrder = [];
    $.each(GetAllTouchesElementsByOrder(), function (i, o) {
        allTouchIdsOrder.push($(this).attr('data-touchid'));
    });

    //if (validateTouchs() == false) {
    //    //ShowAlert('Trailers are in-valid position, Please re-arrage them in proper position.');

    //    return false;
    //}

    $.ajax({
        url: URLHelper.GroupingTouchesUrl,
        data: { TouchIds: touchIds, AllTouchIdsOrder: allTouchIdsOrder },
        traditional: true,
        beforeSend: function () {
        },
        success: function (data) {
            if (data == "True") {
                //Once grouping is done successfully then Reload touches
                LoadTouchForOptimization();
            } else if (data == 'Cannot group RE DE due to mismatch in container size') {
                ShowAlert("Cannot group RE DE due to mismatch in container size.");
            } else {
                ShowAlert("Touches cannot be combined..!");
            }
        },
        error: function (error) {
            ShowAlert(error);
        },
        complete: function () {
            //$.unblockUI();
        }
    });
}

function UnGroupThisTouchGroup(loadid, groupguid) {
    $.ajax({
        url: URLHelper.UnGroupThisTouchGroupUrl,
        data: { loadid: loadid, guid: groupguid },
        traditional: true,
        beforeSend: function () {
        },
        success: function (rtVal) {
            if (rtVal == "Failure") {
                ShowAlert('Error occurred while saving stops. Please reload the page and try again!');
            }
            else {
                //ShowAlert('Successfully un-grouped touches.');
                LoadTouchForOptimization(true);
            }
        },
        error: function (error) {
        },
        complete: function () {
            //$.unblockUI();
        }
    });
}

//Save touches order and trailer order also
function SaveSortAndOptimizeStops() {

    drawTouchesRouteAndCallBackSave(validateTouchTiming);

    //setTimeout(function () { SaveSortTouchesAndContinue(false); }, 800);
}

function SaveSortTouchesAndContinue(validateTouchsWithTimeTimeings) {//(onlySave, reloadTouches) {
    //This will validate touch orders, Open touch should not fall before Completed or Skipped or InProgress touches
    var touchorder = validateTouchOrders();
    if (touchorder == false)
        return false;

    ///   Moved below method to validateTouchTiming
    //    //validate all the touches are valid or not if they are valid then only save it
    //    validateTouchsWithTimeTimeings = validateTouchs();
    
    if (validateTouchsWithTimeTimeings.success == false || validateTouchsWithTimeTimeings.success == undefined)
        return false;

    //var allObj = $($(".touchtable tbody")).sortable('toArray');
    var allObj = $($("ol.touch-optimization li")).map(function (i, o) {
        return o.id;
    });
    var trailerHookup = false;
    var touchTrailerMapIds = new Array();
    var touchIds = new Array();
    var trailarRow = false;
    var trailerid = 0;

    $.each(allObj, function (i, o) {
        trailarRow = false;

        if (o.indexOf('hookup') != -1) {
            trailerHookup = true;
            trailarRow = true;
            //trailerid = parseInt(o.replace('hookup_', ''));
            trailerid = parseInt($('#' + o).attr('data-trailerid'));
        } else if (o.indexOf('unhook') != -1) {
            trailerHookup = false;
            trailarRow = true;
            trailerid = 0;
        }

        //If the row is grouped row then we have to skip this row
        if (o.indexOf('grouped') == -1) {
            if (trailarRow == false) {
                var touchid = parseInt(o.replace('SortTouchId_', ''));

                if (trailerHookup == true) {
                    touchTrailerMapIds.push(touchid + ',' + trailerid);
                }

                touchIds.push(touchid);
            }
        }
    });

    var postData = {
        touchSeqId: touchIds,
        touchTrailerMapIds: touchTrailerMapIds,
        loadId: $('option:selected', $('#dwAvailableDrivers')).attr('data-loadid'),
        driverStartTime: $('#driverStartTime').val(),
        touchAndTime: validateTouchsWithTimeTimeings.data,
        isIgnoreWar: false
    }

    saveTouchSortOrder(postData);

    function saveTouchSortOrder(data) {
        $.ajax({
            url: URLHelper.SaveTouchSortOrderUrl,
            type: 'POST',
            data: JSON.stringify(data),
            traditional: true,
            datatype: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            beforeSend: function () {
            },
            success: function (rtVal) {
                if (rtVal == "Failure") {
                    ShowAlert('Error occurred while saving load. Please reload the page and try again!');
                } else if (rtVal.indexOf('This truck is already scheduled') != -1) {
                    warningAlert(rtVal);
                }
                else {
                    initOptimizeStops(rtVal);
                }
            },
            //error: function (error) {
            //    debugger;
            //    alert(error);
            //},
            complete: function () {
            }
        });
    }

    //only for this page.
    function warningAlert(message) {
        var alertPop = $('#alert-popwar');

        $('#LLSAlertBody', alertPop).html(message);
        alertPop.show();

        $('#WarOk').click(function () {
            alertPop.hide();
            var postDataIgnore = {
                touchSeqId: touchIds,
                touchTrailerMapIds: touchTrailerMapIds,
                loadId: $('option:selected', $('#dwAvailableDrivers')).attr('data-loadid'),
                driverStartTime: $('#driverStartTime').val(),
                touchAndTime: validateTouchsWithTimeTimeings.data,
                isIgnoreWar: true
            }
            saveTouchSortOrder(postDataIgnore);
        });
        $('#WarCancel').click(function () {
            alertPop.hide();
            //return false;
        });
        $('#imgClose').click(function () {
            alertPop.hide();
            // return false;
        });

    }
}



function validateTouchOrders() {
    var touchStatuses = [];
    var errmsg = "";
    var success = true;
    var allTouchs = GetAllTouchesElementsByOrder();

    allTouchs.each(function (i, o) {
        var touchStatus = $(this).find('.TouchStatus').val();
        var custname = $(this).find('.customer-name-ordnum').find('.main-head').html();

        if (touchStatuses.length > 0 && (touchStatus.toLocaleLowerCase() == 'completed' || touchStatus.toLocaleLowerCase() == 'skipped' || touchStatus.toLocaleLowerCase() == 'inprogress')) {
            $.each(touchStatuses, function (i, o) {
                if (o == 'Open') {
                    errmsg = 'This route includes completed or skipped touches for (' + custname + ').  Reorder the route with completed/skipped touches first then all incomplete touches.';
                    //errmsg = custname + ', Dont include Completed or Skipped touches after Open touches';
                    success = false;
                    return;
                }
            });
        }

        if (success == false) {
            ShowAlert(errmsg);
            return;
        } else
            touchStatuses.push(touchStatus);
    });

    return success;
}

function drawTouchesRouteAndCallBackSave(saveSortTouchesCallBack) {

    var waypts = [];
    var allWaypts = [];

    //var allTouchs = $('ol.touch-optimization li').map(function (i, o) {
    //    if (o.id.indexOf('hookup') == -1 && o.id.indexOf('unhook') == -1 && o.id.indexOf('grouped') == -1) {
    //        return o;
    //    }
    //});
    var allTouchs = GetAllTouchesElementsByOrder();
    var originLocation;
    var destLocation;

    allTouchs.each(function (i, o) {
        if (i == 0) {
            originLocation = new google.maps.LatLng($(this).find(".orgLat").val(), $(this).find(".orgLong").val());
            waypts.push({
                location: new google.maps.LatLng($(this).find(".destLat").val(), $(this).find(".destLong").val()),
                stopover: true
            });

            allWaypts.push({
                location: new google.maps.LatLng($(this).find(".orgLat").val(), $(this).find(".orgLong").val()),
                stopover: true
            });
            allWaypts.push({
                location: new google.maps.LatLng($(this).find(".destLat").val(), $(this).find(".destLong").val()),
                stopover: true
            });

            //If user tries to create route for only one touch then we need to add destinationLocation here it self
            if (allTouchs.length == 1) {
                destLocation = originLocation;//new google.maps.LatLng($(this).find(".orgLat").val(), $(this).find(".orgLong").val());
            }
        } else if (i == allTouchs.length - 1) {
            waypts.push({
                location: new google.maps.LatLng($(this).find(".orgLat").val(), $(this).find(".orgLong").val()),
                stopover: true
            });
            allWaypts.push({
                location: new google.maps.LatLng($(this).find(".orgLat").val(), $(this).find(".orgLong").val()),
                stopover: true
            });
            destLocation = new google.maps.LatLng($(this).find(".destLat").val(), $(this).find(".destLong").val());
            allWaypts.push({
                location: new google.maps.LatLng($(this).find(".destLat").val(), $(this).find(".destLong").val()),
                stopover: true
            });
        } else {

            var orgLat = $(this).find(".orgLat").val();
            var orgLng = $(this).find(".orgLong").val();
            var orgLatLong = { location: new google.maps.LatLng(orgLat, orgLng), stopover: true };
            waypts.push(orgLatLong);
            allWaypts.push(orgLatLong);
            //var prvOrgLat = waypts[waypts.length - 1].location.lat();
            //var prvOrgLng = waypts[waypts.length - 1].location.lng();

            //if (orgLatLong.location.lat() != prvOrgLat || orgLatLong.location.lng() != prvOrgLng) {
            //    waypts.push(orgLatLong);
            //}

            var destLat = $(this).find(".destLat").val();
            var destLng = $(this).find(".destLong").val();
            var destLatLong = { location: new google.maps.LatLng(destLat, destLng), stopover: true };
            waypts.push(destLatLong);
            allWaypts.push(destLatLong);

            //var prvDestLat = waypts[waypts.length - 1].location.lat();
            //var prvDestLng = waypts[waypts.length - 1].location.lng();

            //if (destLatLong.location.lat() != prvDestLat || destLatLong.location.lng() != prvDestLng) {
            //    waypts.push(destLatLong);
            //}
        }
    });

    drawMap(originLocation, destLocation, waypts, saveSortTouchesCallBack, allWaypts);
}

/*********************************************** Touch Validation Starts *******************************************************/
function validateTouchs(touchTimings) {

    var trailerValidation = validateAddTrialerTouches();
    //var touchGroupValidation = validateTouchGropTouches();
    //var touchTimings = validateTouchTiming();
    
    ///touchTimings error messages are displayed in the 
    var finalValidation = (trailerValidation.success == true && touchTimings.success == true);

    var rtValue = {};
    if (finalValidation == true) {
        rtValue.success = true;
        rtValue.data = touchTimings.data;
    }
    else {
        var msg = trailerValidation.errormsg;
       
        if (msg != undefined && msg != "") {
            ShowWarningAlert(msg);
        }

        rtValue.success = false;
        rtValue.data = touchTimings.data;
    }

    SaveSortTouchesAndContinue(rtValue);
    //return rtValue;
}


function validateAddTrialerTouches() {
    var rtFlag = true;
    var trailerArray = new Array();
    var trailerArrayForValid = new Array();
    //Here need to validate hook up leg should be first step and un-hook should be next leg
    var msg = "";
    var allObj = $($("ol.touch-optimization li")).map(function (i, o) {
        if (o.id.indexOf('hookup') != -1) {
            trailerid = parseInt($('#' + o.id).attr('data-trailerid'));
            var traileobj = { trailerId: trailerid, index: i, stoptype: 'hookup' };

            trailerArray.push(traileobj);
            trailerArrayForValid.push(traileobj);
        } else if (o.id.indexOf('unhook') != -1) {
            trailerid = parseInt($('#' + o.id).attr('data-trailerid'));
            var traileobj = { trailerId: trailerid, index: i, stoptype: 'unhook' };

            trailerArray.push(traileobj);

            var hookupobj = trailerArrayForValid[trailerArrayForValid.length - 1];
            if (hookupobj != undefined && hookupobj.stoptype == 'hookup' && hookupobj.trailerId == trailerid) {
                trailerArrayForValid.splice(trailerArrayForValid.length - 1, 1);
            } else {
                trailerArrayForValid.push(traileobj);
            }
        }

        return o;
    });

    var rtValue = {};

    if (trailerArrayForValid.length != 0) {
        rtValue.success = false;
        rtValue.errormsg = 'Trailer hook and un-hook touches are not valid. Please re-arrange and try again.';
    } else {
        rtValue.success = true;
        rtValue.errormsg = '';
    }


    return rtValue;
}

function validateTouchGropTouches(toucheIds) {
    var rtFlag = true;

    //Here need to validate hook up leg should be first step and un-hook should be next leg



    if (toucheIds.length <= 0)
        rtFlag = false;

    return rtFlag;
}

function validateTouchTiming() {
    var driverStartTime = $('#driverStartTime').val();

    driverStartTime = driverStartTime.length == 0 ? "8:30:00" : driverStartTime;

    var loadDate = $('#txtDate').val();

    var totalTodayTimeObj = new Date(loadDate + " " + driverStartTime);//Needs to change this line
    var touchAndTimes = [];

    //var allTouchs = $('ol.touch-optimization li').map(function (i, o) {
    //    if (o.id.indexOf('hookup') == -1 && o.id.indexOf('unhook') == -1 && o.id.indexOf('grouped') == -1) {
    //        return o;
    //    }
    //});

    //changes for LL-38 enhancement, reassigning totalTodayTimeObj to system time
    var chkCurrentDate = new Date();
    //if ((totalTodayTimeObj.toDateString() === chkCurrentDate.toDateString()) && (totalTodayTimeObj < chkCurrentDate)) {
    //    totalTodayTimeObj = chkCurrentDate;
    //}

    var allTouchs = GetAllTouchesElementsByOrder();

    var arrMsgs = [];
    var index = -1;
    //let firstOpenTouchFound = false;
    //console.log(allTouchs);
    //console.log(mapRouteArray);
    //console.log(mapRouteArray.length);
    allTouchs.each(function (i, o) {
        index++;
        let touchStatus = $(this).find('.TouchStatus').val().toLowerCase();
        if (touchStatus == "completed" || touchStatus == "skipped") {
            
            let touchScheduleStartTime = new Date($(this).find('.touchScheduledStartTime').val());
            let touchActualEndTime = new Date($(this).find('.touchActualEndTime').val());

            ///If we have any touches completed in a route, then we should consider driverStartTime as his first start touch start time
            if (i == 0) {
                driverStartTime = touchScheduleStartTime.format24HHMMSS();
            } else {
                index++;
            }
            //console.log('if index: ' + index + '; Touch type: ' + $(this).find('.touchType').val() + '; firstOpenTouchFound: ' + firstOpenTouchFound + '; touchScheduleStartTime.formatMMDDYYYYHHMMSS(): ' + touchActualEndTime.formatMMDDYYYYHHMMSS());
            totalTodayTimeObj = touchActualEndTime;

            var touchAndTime = {
                touchID: $(this).attr('data-touchid'),//$(this.id).attr('data-touchid');
                //touchDateTime: totalTodayTimeObj.toUTCString() //.toLocaleFormat()
                touchDateTime: touchActualEndTime.formatMMDDYYYYHHMMSS()//toLocaleString()
            };
            touchAndTimes.push(touchAndTime);

            //IF we have any completed touches in a route, then for next touch we should consider either last touch completed time or current time which ever is grater
            if ((totalTodayTimeObj.toDateString() === chkCurrentDate.toDateString()) && (totalTodayTimeObj < chkCurrentDate)) {
                totalTodayTimeObj = chkCurrentDate;
            }
        }
        else {            
            //if (firstOpenTouchFound == false) {
                ///If user is trying to schedule route for today and then we should consider driver start time as should be future date but not past date (IF driver start time falls in past)
                //03-09-2016 - If the current load not yet started then driver start at time selected as less than current time then we need to consider driver start at time as a base time. dont take current time as a base time
                //if ((totalTodayTimeObj.toDateString() === chkCurrentDate.toDateString()) && (totalTodayTimeObj < chkCurrentDate)) {
                //    totalTodayTimeObj = chkCurrentDate;
                //}
               // firstOpenTouchFound = true;
            //}
            //console.log('else index: ' + index + '; firstOpenTouchFound: ' + firstOpenTouchFound + 'totalTodayTimeObj.formatMMDDYYYYHHMMSS(): ' + totalTodayTimeObj.formatMMDDYYYYHHMMSS());
            let touchTimeZone = $(this).find('.touchTimeZone').val();
            let touchType = $(this).find('.touchType').val();

            var actiontimePickup = $('#handlingTime' + touchType + 'PU').val(),
                travellingTime = 0,
                weightstnTime = 0,
                actiontimeDropoff = $('#handlingTime' + touchType + 'DF').val();

            if (i == 0) {
                travellingTime = Math.round(mapRouteArray[index].duration.value / 60);
                driverStartTime = totalTodayTimeObj.format24HHMMSS();
                //console.log(driverStartTime);
            } else {
                travellingTime = Math.round(mapRouteArray[index].duration.value / 60);
                index++;
                travellingTime += Math.round(mapRouteArray[index].duration.value / 60);
            }

            if ($(this).attr('data-weightstnreq') == "True") {
                weightstnTime = 20;
            }

            var totalTime = parseInt(actiontimePickup) + parseInt(travellingTime) + parseInt(weightstnTime) + parseInt(actiontimeDropoff);
            totalTodayTimeObj.setMinutes(totalTodayTimeObj.getMinutes() + totalTime);

            //console.log(totalTodayTimeObj);

            var touchAndTime = {
                touchID: $(this).attr('data-touchid'),//$(this.id).attr('data-touchid');
                //touchDateTime: totalTodayTimeObj.toUTCString() //.toLocaleFormat()
                touchDateTime: totalTodayTimeObj.formatMMDDYYYYHHMMSS()//toLocaleString()
            };
            touchAndTimes.push(touchAndTime);

            if (touchTimeZone == "AM" && totalTodayTimeObj.getHours() > 12) {
                arrMsgs.push($(this).find('.main-head').html() + ': is an AM touch scheduled outside of the AM window');
            } else if (touchTimeZone == "PM" && totalTodayTimeObj.getHours() <= 11) {
                arrMsgs.push($(this).find('.main-head').html() + ': is an PM touch scheduled outside of the PM window');
            }
        }
    });

    var startDayTimeObj = new Date(loadDate + " " + driverStartTime);
    var diff_time = ((totalTodayTimeObj - startDayTimeObj) / (60 * 60 * 1000));
    //console.log(totalTodayTimeObj);
    //console.log(startDayTimeObj);
    //console.log(diff_time);
    if (diff_time > 8) {
        arrMsgs.push('This route exceeds 8 hours')
    }

    var DriverWorkHours_3GTMS = parseInt($('#DriverWorkHours_3GTMS').val());

    //console.log(diff_time + ' > ' + DriverWorkHours_3GTMS)

    if (diff_time > DriverWorkHours_3GTMS) {
        arrMsgs.push('This route goes outside of the driver’s scheduled hours for the day');
    }

    var rtValue = {};
    //following condition was added by Guru Pratap in order to fix LL-27 issue.

    if (arrMsgs != undefined && arrMsgs.length > 0) {
        CustomConfirmBox(arrMsgs.join('. <br />'), function (Confirmed) {
            if (Confirmed) {
                //confirmed means continue the process with warning so returning success
                rtValue.success = true;
                rtValue.errormsg = '';
                rtValue.data = touchAndTimes;

                return validateTouchs(rtValue) //callback function inorder to save the results when user select continue
            }
            else {
                rtValue.success = false;
                //rtValue.errormsg = arrMsgs.join('. <br />');
                rtValue.errormsg = '';
                rtValue.data = touchAndTimes;

                return validateTouchs(rtValue); //callback function inorder to save the results when user select continue
            }
        });

        return;
    }
    else {
        rtValue.success = true;
        rtValue.errormsg = '';
        rtValue.data = touchAndTimes;

        return validateTouchs(rtValue)
    }

    //return rtValue;


    //if ($('#overrideconstraint').is(':checked') == false && arrMsgs.length > 0) {
    //    rtValue.success = false;
    //    rtValue.errormsg = arrMsgs.join('.<br/>');
    //    rtValue.data = touchAndTimes;
    //} else {
    //    rtValue.success = true;
    //    rtValue.errormsg = '';
    //    rtValue.data = touchAndTimes;
    //}

    //var _MS_PER_Hour = 1000 * 60 * 60;
    //var d = Math.floor((totalTodayTimeObj - startDayTimeObj) / _MS_PER_Hour);

    //alert(d);
    //if (totalTodayTimeObj - star)


}
/*********************************************** Touch Validation End *******************************************************/

function ShowSLUpdates(obj) {
    var slupdates = $(obj).parent().find('.slupdates-oldandnew');

    //var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    var isFF = navigator.userAgent.search("Firefox");
    if (isFF < 0) {
        window.onmousemove = function (e) {
            var x = e.clientX,
                y = e.clientY;
            $(slupdates).css({ top: (y - 100) + 'px', left: (x - 30) + 'px', position: 'absolute' });
        };
    }
    slupdates.show();    
}

function HideSLUpdates(obj) {
    var slupdates = $(obj).parent().find('.slupdates-oldandnew');
    slupdates.hide();
}

function RemoveCancleTouch(touchId, obj) {

    $.ajax({
        url: URLHelper.removeOrderfromLoadUrl,
        data: {
            touchId: touchId
        },
        beforeSend: function () {
            //$.blockUI({ message: '<h3>Just a moment...</h3>' })
        },
        success: function (retVal) {
            if (retVal == "Success") {
                // Here we call Driver Touches & UnAssigned Touches( if method return true )
                //$("#btnRealod").click();
                var actionFrom = $(obj).closest('table').hasClass('footable'); //If this is true means action from Grid view

                if (actionFrom == true) {
                    $("#btnRealod").click();
                } else {
                    $('SortTouchId_' + touchId).remove();
                }
            }
            else {
                ShowAlert(retVal);
            }
        },
        error: function (error) {
            //document.getElementById("divProgress").style.display = 'none;';
            ShowAlert('Error occurred while processing your request. Please reload the page and try again!');
        },
        complete: function () {
            $.unblockUI();
        }
    });
}

function RemoveCancleStop(touchId, loadId) {

    $.ajax({
        url: URLHelper.removeCancleTouchfromLoadUrl,
        data: {
            touchId: touchId,
            loadId: loadId
        },
        beforeSend: function () {
            //$.blockUI({ message: '<h3>Just a moment...</h3>' })
        },
        success: function (retVal) {
            if (retVal == "Success") {
                // Here we call Driver Touches & UnAssigned Touches( if method return true )
                //$("#btnRealod").click();
                LoadTouchForOptimization();
            }
            else {
                ShowAlert(retVal);
            }
        },
        error: function (error) {
            //document.getElementById("divProgress").style.display = 'none;';
            ShowAlert('Error occurred while processing your request. Please reload the page and try again!');
        },
        complete: function () {
            $.unblockUI();
        }
    });
}