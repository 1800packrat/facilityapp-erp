﻿$(document).ready(function () {
    var manager = Sys.WebForms.PageRequestManager.getInstance();
    manager.add_endRequest(EndRequestHandler);
});

function EndRequestHandler(sender, args) {
    if (args.get_error() != undefined) {
        var errorMessage = args.get_error().message;
        args.set_errorHandled(true);
        if (errorMessage.startsWith("Sys.WebForms.PageRequestManagerTimeoutException:")) {
            alert("The touch is scheduled successfully,please reload the page or view touches screen for the updated information.");
            self.close();
        } else {
            alert(errorMessage);
        }

    }
}
window.onunload = function () {

    window.opener.document.getElementById("ctl00_cphCenter_btnSecretBind").click();

}