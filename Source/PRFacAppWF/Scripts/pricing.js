﻿function CalculateTotal(i) {


    var discount = document.getElementById('ctl00_cphCenter_gvtransitems_ctl' + i + '_txtEditDiscounts').value;
    var total = document.getElementById('ctl00_cphCenter_gvtransitems_ctl' + i + '_txtEditTotal').value;
    var subtotal = document.getElementById('ctl00_cphCenter_gvtransitems_ctl' + i + '_hdSubtotal').value;
    if (isNaN(discount) || discount < 0) {
        alert('please enter a positive value');
    } else {
        total = subtotal - discount;
        document.getElementById('ctl00_cphCenter_gvtransitems_ctl' + i + '_txtEditTotal').value = parseFloat(total).toFixed(2);
    }
}
function CalculateDiscount(i) {
    discount = document.getElementById('ctl00_cphCenter_gvtransitems_ctl' + i + '_txtEditDiscounts').value;
    total = document.getElementById('ctl00_cphCenter_gvtransitems_ctl' + i + '_txtEditTotal').value;
    subtotal = document.getElementById('ctl00_cphCenter_gvtransitems_ctl' + i + '_hdSubtotal').value;
    if (isNaN(total) || total < 0) {
        alert('please enter a positive value');
    } else {
        discount = subtotal - total;
        document.getElementById('ctl00_cphCenter_gvtransitems_ctl' + i + '_txtEditDiscounts').value = parseFloat(discount).toFixed(2);
    }
}