﻿//we are not using this file.

$(document).ready(function () {

    initDriversGrid();
    initPopup();

    initPopupSecheduleHours();
    saveDriverDetails();

    initHrChangeGrid();

    bindAjaxStartEvent();

    $('#filterJqGrid').click(function (e) {
        e.preventDefault();
        filterGrid();
    });

    $('#ddlFacilites').change(function () {
        filterGrid();
    });
});

function resetform() {
    $('#txtDriverName').val('');
    $('#ddlFacilites').val('-1');
    $('#chkOnlyActive').attr('checked', true);
    $("#tblLstDrivers").jqGrid('clearGridData');

    $("#tblLstDrivers").jqGrid('setGridParam', {
        postData: {
            driverName: '',
            storeNo: 0,
            isActiveOnly: true
        }
    }).trigger("reloadGrid");
}

function initDriversGrid() {
    $("#tblLstDrivers").jqGrid({
        url: URLHelper.getUserList,
        mtype: 'GET',
        datatype: 'json',
        postData: {
            driverName: $('#txtDriverName').val(),
            storeNo: $('#ddlFacilites').val() == '-1' ? '0' : $('#ddlFacilites').val(),
            isActiveOnly: $('#chkOnlyActive').prop('checked')
        },
        contentType: "application/json; charset-utf-8",
        colNames: ['ID', 'Driver Name', 'Status', 'User Name', 'Facility', '', 'Active', 'FacilityID'],
        colModel: [{ name: 'DriverId', index: 'ID', key: true, editable: false, hidedlg: true, hidden: true },
                    { name: 'FirstName', index: 'FirstName', width: 70, editable: true, align: 'left' },
                    { name: 'Status', index: 'Status', width: 50, editable: true, align: 'center', sortable: false },
                    { name: 'UserName', index: 'User Name', width: 50, editable: true, align: 'center', sortable: false },
                    { name: 'Facility', index: 'Facility', width: 60, editable: true, align: 'center', sortable: false },
                    { name: 'Edit', search: false, index: 'DriverId', sortable: false, formatter: formatActionButton, align: 'center' },
                    { name: 'Active', index: 'Active', hidden: true },
                    { name: 'FacilityID', index: 'FacilityID', key: true, editable: false, hidedlg: true, hidden: true }],
        pager: $('#ptblLstDrivers'),
        width: 920,
        rowNum: 20,
        rowList: [10, 20, 30, 60, 365],
        sortname: 'FirstName',
        sortorder: "asc",
        viewrecords: true,
        //caption: 'Users List',
        autowidth: true,
        gridview: true,
        id: "ID",
        height: "100%",
        multiselect: false,
        multiboxonly: true,
        loadonce: false, //when search is implementing from server side
        // datatype: 'local',
        beforeRequest: function () {
        },
        loadError: function (xhr, st, err) {
            if (xhr.status == "200") return false;
            var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                modal: false,
                title: 'Error',
                maxwidth: '600',
                width: '600',
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        },
        gridComplete: function () {
            if (isFullAccess == "False") {
                $('#tblLstDrivers').find('#btnActive').each(function () { $(this).attr('disabled', 'disabled'); });
            }
        },
        loadComplete: function () { },
        ondblClickRow: function (rowid, iRow, iCol, e) {
        }
    }).navGrid('#ptblLstDrivers',
    {
        edit: false, add: false, del: false, search: false, refresh: false
    }, null, null, null);


}

function filterGrid() {
    $("#tblLstDrivers").clearGridData();

    $("#tblLstDrivers").jqGrid('setGridParam', {
        postData: {
            driverName: $('#txtDriverName').val(),
            storeNo: $('#ddlFacilites').val() == '-1' ? '0' : $('#ddlFacilites').val(),
            isActiveOnly: $('#chkOnlyActive').prop('checked')
        }
    }).trigger("reloadGrid");
}

function saveDriverDetails() {
    $('#saveDriverDetails').click(function () {
        var driverId = $('#driverId').val();
        var fullName = $('#FullName').val();
        var firstName = $('#FirstName').val();
        var lastName = $('#LastName').val();
        var password = $('#Password').val();
        var StoreNumber = $('#ddlDriverFacilites').val();

        if (fullName == '') {
            alert('User Name should not be empty.');
            $('#FullName').focus();
            return;
        }
        if (firstName == '') {
            alert('First Name should not be empty.');
            $('#FirstName').focus();
            return;
        }
        if (lastName == '') {
            alert('Last Name should not be empty.');
            $('#LastName').focus();
            return;
        }
        if (StoreNumber == '-1') {
            alert('Please select facility.');
            $('#ddlDriverFacilites').focus();
            return;
        }
        if (password == '') {
            alert('Password should not be empty.');
            $('#Password').focus();
            return;
        }

        $.ajax({
            url: URLHelper.addEditDriver,
            traditional: true,
            data: { DriverId: driverId, DriverName: fullName, FirstName: firstName, LastName: lastName, FacilityId: StoreNumber, Password: password },
            beforeSend: function () {
            },
            success: function (result) {
                if (result.msg == "error") {
                    alert('Error occurred while loading information. Please reload the page and try again!');
                }
                else if (result.msg == "updated") {
                    alert('Driver updated successfully.');
                    $("#popup").hide();
                    removeMaskAfterPopup();
                    $("#tblLstDrivers").trigger("reloadGrid");
                }
                else {
                    alert('Driver created successfully.');
                    $("#popup").hide();
                    removeMaskAfterPopup();
                    $("#tblLstDrivers").trigger("reloadGrid");
                }
            },
            error: function (error) {
                alert('Error occurred while loading information. Please reload the page and try again!');
            },
            complete: function () {
            }
        });

    });
}


function displaydriverDetails(drvId) {
    $.ajax({
        url: URLHelper.displayDriverDetails,
        data: { DriverID: drvId },
        beforeSend: function () {
        },
        success: function (result) {

            if (result != "0")// if not zero then there is data to display
            {
                //$('#ddlFacilites').val(result.StoreNumber);
                $("#popup").show();
                addMasktoPopup1();
                document.getElementById('displayDriverDetails').innerHTML = '';
                document.getElementById('displayDriverDetails').innerHTML = result;
            } else {
                //$("#popup").hide();
            }
        },
        error: function (error) {
        },
        complete: function () {
        }
    });
}

function addMasktoPopup1() {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popup').fadeIn();

    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box2, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });
}

function addMasktoPopup() {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popupScheduleHour').fadeIn();

    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box1, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });
}

function removeMaskAfterPopup() {
    $(".modal-box2, .modal-overlay").fadeOut(500, function () {
        $(".modal-overlay").remove();
    });
}

function initPopupSecheduleHours() {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $('a[data-modal-id]').click(function (e) {
        e.preventDefault();

        $("body").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7);
        $('#popupScheduleHour').fadeIn();
    });

    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box1, .modal-overlay ").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });
    $(window).resize(function () {
        $(".modal-box1").css({
            top: 75,
            left: ($(window).width() - $(".modal-box1").outerWidth()) / 2
        });

    });
    $(window).resize();
}

function initPopup() {
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    $('a[data-modal-id]').click(function (e) {
        e.preventDefault();

        $("body").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7);
        $('#popup').fadeIn();
    });

    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box2, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });
    $(window).resize(function () {
        $(".modal-box2").css({
            top: 75,
            left: ($(window).width() - $(".modal-box2").outerWidth()) / 2
        });

    });
    $(window).resize();
}

function formatActionButton(cellvalue, options, rowObject) {
    return "<input id='btnActive' type='button' value='" + rowObject[5] + "' onclick='SetActiveInactive(" + rowObject[0] + ", this.value); return false;' style='margin: 2px;' /> <input type='button' value='Edit' onclick='displaydriverDetails(" + rowObject[0] + "); return false;'/> <input type='button' value='Edit Schedule Hours' onclick='DisplayScheduleHours(" + rowObject[0] + ", " + rowObject[6] + "); return false;'/>";
}

function SetActiveInactive(driverId, Status) {

    var result = confirm("Are you sure you want to make it " + Status + " ?");
    if (result == true) {
        $.ajax({
            url: URLHelper.SetDriverActiveInactiveUrl,
            data: {
                DriverId: driverId,
                status: Status
            },
            success: function (retVal) {
                if (retVal == "error") {
                    alert('Error occurred while loading information. Please reload the page and try again!');
                } else {
                    $("#tblLstDrivers").trigger("reloadGrid");
                }
            },
            error: function (error) {
                alert('Error occurred while loading information. Please reload the page and try again!');
            }
        });
    }
}

function bindAjaxStartEvent() {
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
}

function DisplayScheduleHours(driverId, FacilityID) {
    $('#popupScheduleHour').show();
    addMasktoPopup();
    $('#hdSelectDriverId').val(driverId);
    $('#hdSelectedFacilityId').val(FacilityID);

    //initHrChangeGrid();
    addAndGetDriverOperatingHours();
}


function myelem(value, options) {
    var el = document.createElement("span");
    el.type = "span";
    el.className = 'oprationHoursDuration'
    el.innerHTML = value;
    return el;
}

function myvalue(elem, operation, value) {
    if (operation === 'get') {
        return $(elem).val();
    } else if (operation === 'set') {
        $('span', elem).val(value);
    }
}

function initHrChangeGrid() {
    var checkStartEndTime = function (value, colname) {
        var startTime = $("input#ScheduleStartTime").val();
        var endTime = $("input#ScheduleEndTime").val();

        if (startTime != '' && endTime != '') {
            var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
            var endTimeDt = new Date("1/1/1970" + ' ' + endTime);

            if (startTimeDt > endTimeDt) {
                return [false, "Start time should be less than End time.", ""];
            }
        }

        return [true, "", ""];
    };

    var updateDialog = {
        url: URLHelper.saveDriverHourChangeUrl
                , closeAfterEdit: true
                , reloadAfterSubmit: true
                , closeAfterAdd: true
                , closeAfterEdit: true
                , afterShowForm: function (formid) {
                    //$("select#DayWork", formid).val("W")
                    GetWorkingHours(formid);

                    $("input#ScheduleEndTime", formid).blur(function (o) {
                        setTimeout(function () { GetWorkingHours(formid); }, 500);
                    });
                    $("input#ScheduleStartTime", formid).blur(function (o) {
                        setTimeout(function () { GetWorkingHours(formid); }, 500);
                    });
                    $("select#DayWork", formid).change(function (o) {
                        $("input#ScheduleStartTime", formid).val('12:00 AM');
                        $("input#ScheduleEndTime", formid).val('12:00 AM');
                        GetWorkingHours(formid);
                    });
                }
                , onclickSubmit: function (params) {
                    var ajaxData = {};
                    var list = $("#list");
                    var selectedRow = list.getGridParam("selrow");

                    rowData = list.getRowData(selectedRow);

                    ajaxData = { rowIds: jQuery("#list").jqGrid('getGridParam', 'selarrrow'), ScheduleDate: rowData.ScheduleDate };

                    return ajaxData;
                }
                , afterComplete: function (o) {
                }
                , modal: true
                , width: "100%"
    };

    $("#list").jqGrid({
        colNames: ['RowID', 'Date', 'DayWorking', 'Start Time', 'End Time', 'Operating Minutes', 'WeekDay']
                    , colModel: [
                            { name: 'ID', index: 'RowID', key: true, editable: false, hidedlg: true, hidden: true },
                            //{ name: 'CalDate', index: 'CalDateIndex', editable: true, editrules: { edithidden: false }, hidedlg: true, hidden: true, search: false },
                            { name: 'ScheduleDate', index: 'ScheduleDate', width: 80, editable: false, hidedlg: true, hidden: false, search: false, align: 'center' },
                            //{ name: 'StoreOpen', index: 'StoreOpen', width: 40, editable: true, edittype: 'select', formatter: 'select', editrules: { required: true }, formoptions: { elmsuffix: ' *' }, editoptions: { value: { O: 'Open', C: 'Closed' } }, width: "40", search: false, align: 'center' },
                            { name: 'DayWork', index: 'DayWork', width: 40, editable: true, edittype: 'select', formatter: 'select', editrules: { required: true }, formoptions: { elmsuffix: ' *' }, editoptions: { value: { W: 'Working', N: 'Non-Working' } }, width: "40", search: false, align: 'center' },
                            {
                                name: 'ScheduleStartTime', index: 'ScheduleStartTime', width: 100, editable: true, formatter: 'text', edittype: 'text', editrules: { custom: true, custom_func: checkStartEndTime, required: true },
                                editoptions: {
                                    dataInit: function (element) {
                                        $(element).timepicker({
                                            timeFormat: 'h:mm p',
                                            minTime: '12:00 AM', // 11:45:00 AM,
                                            maxTime: '11:30 PM',
                                            interval: 15
                                        });
                                    }
                                },
                                formoptions: { elmsuffix: ' *' }, search: false, align: 'center'
                            },
                            {
                                name: 'ScheduleEndTime', index: 'ScheduleEndTime', width: 100, editable: true, edittype: 'text', editrules: { custom: true, custom_func: checkStartEndTime, required: true },
                                editoptions: {
                                    dataInit: function (element) {
                                        $(element).timepicker({
                                            timeFormat: 'h:mm p',
                                            minTime: '12:00 AM', // 11:45:00 AM,
                                            maxTime: '11:30 PM',
                                            interval: 15
                                        });
                                    }
                                },
                                formoptions: { elmsuffix: ' *' }, search: false, align: 'center'
                            },
                            { name: 'OperatingTimeMinutes', index: 'OperatingTimeMinutes', width: 100, editable: true, edittype: 'custom', editoptions: { custom_element: myelem, custom_value: myvalue }, search: false, align: 'center' },
                            { name: 'WeekDay', sortable: false, index: 'WeekDay', editable: false, hidden: false, stype: 'select', editoptions: { value: "0:All Days;8:Weekends;15:WeekDays;1:Sunday;2:Monday;3:Tuesday;4:Wednesday;5:Thursday;6:Friday;7:Saturday" }, align: 'center' }
                    ]
                , pager: $('#listPager')
                , autowidth: true
        //, shrinkToFit: false
                , rowNum: 20
                , rowList: [10, 20, 30, 60, 365]
                , sortname: 'ScheduleDate'
                , sortorder: "asc"
                , viewrecords: true
                , caption: 'Edit Schedule Hours'
        //, autowidth: false
                , gridview: true
                , id: "RowID"
                , height: "100%"
                , width: "100%"
                , multiselect: true
                , multiboxonly: true
                , toolbar: [true, "top"]
                , datatype: 'local'
                , loadError: function (xhr, st, err) {
                    if (xhr.status == "200") return false;
                    var error = eval('(' + xhr.responseText + ')'); $("#errormsg").html(error.Message).dialog({
                        modal: false,
                        title: 'Error',
                        maxwidth: '600',
                        width: '600',
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
                , gridComplete: function () {
                    var ids = $("#list").getDataIDs();
                    for (var i = 0; i < ids.length; i++) {
                        var cell = $("#list").getCell(ids[i], "WeekDay");
                        if (cell == "Sunday" || cell == "Saturday") {
                            $(this).jqGrid('setRowData', ids[i], false, { background: '#e0effa' });
                            // $("#" + ids)[i].style.backgroundColor = "yellow"
                        }

                        var dtVal = $("#list").getCell(ids[i], "ScheduleDate");
                        var dt = new Date(dtVal);
                        var currentdate = new Date();
                        currentdate.setHours(0, 0, 0, 0);
                        //console.log(currentdate);
                        if (currentdate > dt) {
                            var rowID = $("#list").getCell(ids[i], "RowID");
                            var o = $("input:checkbox[id=jqg_list_" + rowID + "]");
                            $(o).remove();
                        }
                    }
                }
                , emptyrecords: 'no records found'
                , loadComplete: function () {
                    var ts = this;
                    if (ts.p.reccount === 0) {
                        $(this).hide();
                        $('#modalUpdateselectedhours').hide();
                    } else {
                        $(this).show();
                        $('#modalUpdateselectedhours').show();
                    }
                }
                , ondblClickRow: function (rowid, iRow, iCol, e) {
                    if (isFullAccess == "True") {
                        var grid = $('#list');
                        var ScheduleDate = grid.jqGrid('getCell', rowid, 'ScheduleDate');
                        var dt = new Date(ScheduleDate);
                        var currentdate = new Date();
                        if (currentdate < dt)
                            $("#list").editGridRow(rowid, updateDialog);
                    }
                }
    }).navGrid('#listPager',
                   {
                       edit: false, add: false, del: false, search: false, refresh: false
                   },
                   updateDialog,
                   null,
                   null);

    jQuery("#modalUpdateselectedhours").click(function () {
        var rows = jQuery("#list").jqGrid('getGridParam', 'selarrrow');

        if (rows.length > 0) {
            jQuery("#list").jqGrid('editGridRow', rows, {
                height: 240,
                closeAfterEdit: true,
                reloadAfterSubmit: true,
                afterShowForm: function (formid) {
                    // $("select#DayWork", formid).val("W")
                    GetWorkingHours(formid, (rows.length > 1));

                    $("input#ScheduleEndTime", formid).blur(function (o) {
                        setTimeout(function () { GetWorkingHours(formid); }, 500);
                    });
                    $("input#ScheduleStartTime", formid).blur(function (o) {
                        setTimeout(function () { GetWorkingHours(formid); }, 500);
                    });
                    $("select#DayWork", formid).change(function (o) {
                        $("input#ScheduleStartTime", formid).val('12:00 AM');
                        $("input#ScheduleEndTime", formid).val('12:00 AM');
                        GetWorkingHours(formid);
                    });
                }
                , onclickSubmit: batchSumbit
                , url: URLHelper.saveDriverHourChangeUrl
                , afterComplete: function (o) { }
            });
        } else alert("Please Select a Row");
    });

    jQuery("#list").jqGrid('filterToolbar');

    $("#list").setGridWidth(Math.round($('#popupScheduleHour').width() - 50, true));
}

function addAndGetDriverOperatingHours() {
    $("#list").clearGridData();

    $("#list").jqGrid('setGridParam', {
        mtype: 'GET', editurl: URLHelper.saveDriverHourChangeUrl, onclickSubmit: batchSumbit, url: URLHelper.addAndGetDriverOperatingHourUrl, datatype: 'json'
                                , postData: {
                                    rowIds: jQuery("#list").jqGrid('getGridParam', 'selarrrow'),
                                    DriverId: function () { return $('#hdSelectDriverId').val(); },
                                    FacilityID: function () { return $('#hdSelectedFacilityId').val(); },
                                    WeekDay: '0'
                                }
    }).trigger("reloadGrid");

    $("#gs_WeekDay").val('0');
}

var batchSumbit = function (params) {
    var ajaxData = {};
    var data;
    var rowids = jQuery("#list").jqGrid('getGridParam', 'selarrrow');

    if (rowids != null) {
        return ajaxData = { rowIds: rowids };
    }
}

function GetWorkingHours(formid, isMultipleSelected) {
    //When user select multiple check box from UI and trying to update all at once, by that time we are showing default values as 07AM to 05PM.
    if (isMultipleSelected) {
        //$("select#StoreOpen", formid).val('O')
        $("input#ScheduleStartTime", formid).val('07:00 AM');
        $("input#ScheduleEndTime", formid).val('05:00 PM');
    }

    var startTime = $("input#ScheduleStartTime", formid).val();
    var endTime = $("input#ScheduleEndTime", formid).val();
    var storeOpen = $("select#DayWork", formid).val();
    var workingHr = '00';

    //console.log(startTime);
    //console.log(endTime);

    if (storeOpen == "W" && startTime != '' && endTime != '') {
        var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
        var endTimeDt = new Date("1/1/1970" + ' ' + endTime);

        if (startTimeDt < endTimeDt) {
            var differenceDt = new Date(endTimeDt - startTimeDt)

            //var hours = differenceDt.getUTCHours();
            //var minutes = differenceDt.getUTCMinutes();

            var convertedMinutes = Math.round(differenceDt.getTime() / 60000);

            // workingHr = hours + ' hr ' + minutes + ' min';
            workingHr = convertedMinutes;
        }
    }

    $("span#OperatingTimeMinutes", formid).html(workingHr);
}

Date.prototype.ToDate = function () {
    return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
}

Date.prototype.ToTime = function () {
    var hr = parseInt(this.getHours());
    var ampm = hr > 11 ? 'PM' : 'AM';
    var hr = hr > 12 ? (hr - 12) : hr;
    var mm = parseInt(this.getMinutes()) > 9 ? this.getMinutes() : '0' + this.getMinutes();
    return hr + ":" + mm + " " + ampm;
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

Date.prototype.addYears = function (years) {
    var dat = new Date(this.valueOf());
    dat.setYear(dat.getFullYear() + years);
    return dat;
}