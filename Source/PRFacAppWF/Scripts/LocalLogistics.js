﻿var driverModellist = [];
var unassignedlist = [];
var alldrivers = [];
var start_pos;
var latest_pos;
var rowIndex = 0;
var orderNum = 0;
var touchId = 0;
var confirmationPopup = $('#confirmation-pop');
var windowHeight = $(window).height();

$(document).ready(function () {
    $("div#panel").slideDown("slow"); $("#toggle a").toggle();
    $.ajaxSetup({ cache: false });
    $('#txtDate').datepicker();
    $("#mapView").hide();
    $("#open").click(function () {
        $("div#panel").slideDown("slow");
    });

    // Collapse Panel
    $("#close").click(function () {
        $("div#panel").slideUp("slow");
    });

    // Switch buttons from "Log In | Register" to "Close Panel" on click
    $("#toggle a").click(function () {
        $("#toggle a").toggle();
    });

    $(".chkTouchType").prop("checked", true);

    //When page loads...
    $(".tab_content").hide(); //Hide all content
    $("ul.tabs li:first").addClass("active").show(); //Activate first tab
    $(".tab_content:first").show(); //Show first tab content

    //On Click Event
    $("ul.tabs li").click(function () {
        $("ul.tabs li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".tab_content").hide(); //Hide all tab content

        var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
        $(activeTab).fadeIn(); //Fade in the active ID content
        return false;
    });

    $("#btnFlip").click(function () {
        $("#mapView").toggle("fast");
        $("#gridView").toggle("fast");

        GetTransportationTouchesToSessionAndCall('btnFlip');

        //If panel is visible them only toggle to slideup, other wise dont slide up again
        if ($('#panel').is(':visible')) {
            $("div#panel").slideUp("slow");
            $("#toggle a").toggle();
        }
    });

    $("#btnRealod").click(function () {
        if (document.getElementById("txtDate").value == "") {
            ShowAlert("Please select the date");
            return;
        }

        GetTransportationTouchesToSessionAndCall('btnRealod');
    });

    $('#btnUpdateMappoints').click(function (obj) {
        if ($(this).hasClass('disabled') == true) {
            if ($(this).attr('data-DayWork') == "O")
                ShowAlert('This driver has no schedule for the day.');
            else
                ShowAlert('This driver load is locked, Please unlock and try again!');
        } else {
            saveDriverTouches();
        }
        return false;
    });

    bindAjaxStartEvent();

    setHeight();

    //resizing the height of the left nav to the window height when window is resized
    $(window).resize(function () {
        setTimeout(function () { mapResize(); }, 2000);

        setHeight();

        setGridPageHeight();

        setAddressUpdaterHeight();
    });

    ///This function binds edit driver schedule hours
    BindEditDriverDialog();
});

function setAddressUpdaterHeight() {
    $("#modalAddressesEditor").css({
        top: 20,
        left: 50,
    });
}

function setHeight() {
    $(".opt-stop-table-content").css('height', ($(window).innerHeight() - 250));
    $(".opt-table-content").css('height', ($(window).innerHeight() - 250));
}

function setGridPageHeight() {
    $("#unassignedtouches").css('height', ($(window).innerHeight() - 151));
    $("#assignedtouches").css('height', ($(window).innerHeight() - 151));
}

function opendialog() {
    $(".popup").center();
    $("#modalAddressUpdater").css('display', 'block').css('height', ($(window).innerHeight() - 151)).css('width', ($(window).innerWidth() - 50));;
    $('html').addClass('overlay');
    $("#tblAddressList").footable();
    $("#modalAddressUpdater").addClass('visible');
}

function clearPopupUpdateAddressList() {
    $('.popup.visible').addClass('transitioning').removeClass('visible');
    $('html').removeClass('overlay');

    setTimeout(function () {
        $('.popup').removeClass('transitioning');
    }, 200);
    $("#modalAddressUpdater").css('display', 'none');
}

function bindAjaxStartEvent() {
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
}

function GetMapRelatedInfo() {
    clearMapLinesForCC();
    clearMapRelatedVariables();
    ShowLeftNavMainMenu();
    PlotMarkers();
    getDrivers();
    getTrucks();
    AddressUpdaterMapView();

    setTimeout(function () { mapResize(); }, 2000);
}

function GetGridRelatedInfo() {
    LoadUnassignedTouches();
    LoadLoadInformation();
}

function GetTransportationTouchesToSessionAndCall_SuccessCallBack(actionFrom, response) {

    if (response.success == true) {

        if (actionFrom == 'btnRealod') {
            if ($("#btnFlip").val() == "Show Map") {
                GetGridRelatedInfo();
            }
            else {
                GetMapRelatedInfo();
            }
            $("div#panel").slideUp("slow"); $("#toggle a").toggle();
        } else {
            if ($("#btnFlip").attr("value") == "Show Map") {
                $("#btnFlip").val("Show Grid");

                GetMapRelatedInfo();
            } else {
                $("#btnFlip").val("Show Map");

                GetGridRelatedInfo();
            }
        }
    } else {
        ShowAlert("Error occurred while fetching transportation touches from Salesforce. Error: " + response.message);
    }
}

function GetTransportationTouchesToSessionAndCall(actionFrom) {
    $.ajax({
        url: URLHelper.getTransporationTouchesIntoSession
        , data: { date: document.getElementById("txtDate").value }
        , success: GetTransportationTouchesToSessionAndCall_SuccessCallBack.bind(this, actionFrom)
        , error: function (error) {
            ShowAlert("Error occurred while fetching transportation touches from Salesforce");
        }
    });
}

function SaveUpdateAddressList() {

    var addressUpdaterList = [];

    $("table#tblAddressList > tbody > tr").each(function () {
        var addressupdater = {
            OrderId: $(this).attr('data-touchid'),
            ID: $(this).attr('id'),//$(this.id).attr('data-touchid');
            AddressType: $(this).attr('data-addrtype'),
            Lat: $(this).find('.latitude').val(), //.toLocaleFormat()
            Long: $(this).find('.longitude').val() //.toLocaleFormat()
        };
        addressUpdaterList.push(addressupdater);
    });

    var postData = {
        addressList: addressUpdaterList
    };

    $.ajax({
        url: URLHelper.UpdateMissingAddressListUrl,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(postData),
        beforeSend: function () { clearPopupUpdateAddressList(); },
        success: function (result) {
            if (result == "success") {
                clearPopupUpdateAddressList();
                LoadUnassignedTouchesFromSession();
                LoadLoadInformation();
            }
            else {
                ShowAlert("Error in saving address! Please try again.");
            }
        }
    });
}

function CheckDuplicateTrucks(obj) {
    var selVal = $(obj).val();
    var selId = obj.id;

    var isLock = $(obj).closest("article").attr('data-islocked');
    //Check if this load is locked or not 
    if (isLock == 'true' || isLock == true) {
        ShowAlert('Load is locked, You cannot change truck.');
        $(obj).val($('#hdn' + selId).val());
        return false;
    }

    //This needs to be extended
    if (selVal == "" || selVal.length == 0) {
        return;
    }

    var isDuplicate = false;
    if (selVal.length > 0) {
        $('select.load-truck').each(function (obj1, o) {
            if (o.id != selId) {
                if (selVal == $(o).val()) {
                    if (confirm('Are sure do you want to assign this truck to this load?. This truck is already assigned to another load.'))
                        UpdateTruckToThisLoad(parseInt($(obj).attr('data-loadid')), selVal);

                    $(obj1).css('color', 'red');
                    isDuplicate = true;
                    return;
                }
            }
        });
    }

    if (isDuplicate == false && parseInt($(obj).attr('data-loadid')) > 0) {
        if (confirm('Are sure do you want to assign this truck to this load?'))
            UpdateTruckToThisLoad(parseInt($(obj).attr('data-loadid')), selVal);
        else
            $(obj).val($('#hdn' + selId).val());
    }
}

function UpdateTruckToThisLoad(loadId, truckId) {
    $.ajax({
        url: URLHelper.UpdateLoadTruckUrl,
        data: { LoadId: loadId, TruckId: truckId },
        success: function (pvUnassignedTouhces) {
            ShowSuccessAlert('successfully updated selected truck to this load.');
        },
        error: function (error) {
        }
    });
}

function LoadUnassignedTouchesFromSession() {
    $.ajax({
        url: URLHelper.LoadUnassignedTouchesFromSession,
        data: { date: document.getElementById("txtDate").value },
        success: function (pvUnassignedTouhces) {
            document.getElementById('divUnAssinedTouches').innerHTML = '';
            document.getElementById('divUnAssinedTouches').innerHTML = pvUnassignedTouhces;

            //console.log($('.footable'));
            $('.footable').footable();
            bindDraggables($('#divUnAssinedTouches'));
        },
        error: function (error) {
        }
    });
}

function LoadUnassignedTouches() {
    $.ajax({
        url: URLHelper.UnSheduleTouchesUrl,
        data: { date: document.getElementById("txtDate").value },
        success: function (pvUnassignedTouhces) {
            console.log(pvUnassignedTouhces);
            document.getElementById('divUnAssinedTouches').innerHTML = '';
            document.getElementById('divUnAssinedTouches').innerHTML = pvUnassignedTouhces;

            //console.log($('.footable'));
            $('.footable').footable();
            bindDraggables($('#divUnAssinedTouches'));

            //This will set height to unassigned/assigned grid section
            setGridPageHeight();
        },
        error: function (error) {
            alert("exception");
            console.log(pvUnassignedTouhces);
        }
    });
}

function LoadLoadInformation() {
    $.ajax({
        url: URLHelper.RealodUrl,
        data: { date: document.getElementById("txtDate").value },
        success: function (pvDriversTouhces) {
            document.getElementById('main').innerHTML = '';
            document.getElementById('main').innerHTML = pvDriversTouhces;
            $('.footable').footable();
            $.each($('.hdnLoadAndDriverID', $('#main')), function (i, o) {
                var did = parseInt($(o).attr('data-driverid'));

                if (did > 0) {
                    $('#tooltip_' + did).toolbar({
                        content: '#user-options_' + did,
                        position: 'top',
                        hover: true
                    });
                }
            });
            bindDraggables($('#main'));
            disableDragAndDropForLockLoads();
            AddressesEditor();//added to edit addresses

            //This will set height to unassigned/assigned grid section
            setGridPageHeight();
        },
        error: function (error) {
        }
    });
}

function PrintLoad(loadId) {
    var url = URLHelper.PrintLoadUrl + "?loadId=" + loadId;
    var mywindow = window.open(url, '', 'height=400,width=600');

    mywindow.focus();
}

function UnlockLoad(loadId, obj, actionFrom, driverId) {
    loadId = parseInt(loadId);

    if (loadId <= 0) {
        ShowAlert('With out creating load you cannot lock/unlock load..!');
        return false;
    }

    var isLock = $(obj).attr('data-islocked');

    if (isLock == 'true' || isLock == true) {

        $.ajax({
            url: URLHelper.LoadLockUrl,
            data: {
                loadId: loadId,
                isLock: false
            },
            success: function (retVal) {
                if (retVal == "Failure") {
                    ShowAlert('Error occurred while locking load. Please reload the page and try again!');
                }
                else {
                    var imgLock = '/Images/unlock-icon.png';
                    $(obj).attr('src', imgLock);
                    $(obj).prop('title', 'Un-Lock');
                    $(obj).attr('data-islocked', 'false');

                    if (actionFrom == 'mainload') {
                        $('#mainLoad-' + driverId).attr('data-islocked', false);

                        //Make sure aEditDriverScheduleHours element islocked attribute updated, other wise user may get ETA warning while updating his schedules
                        $('#aEditDriverScheduleHours', $('#mainLoad-' + driverId)).attr('data-islocked', 'false');

                        callBackLockLoadMainGrid(obj, isLock);
                        disableDragAndDropForLockLoads();
                    } else if (actionFrom == 'lockstops') {
                        callBackLockStops(obj, isLock);
                        callBackLockMapMainSection(loadId, obj, isLock);
                    } else if (actionFrom == 'mapmainsection') {
                        callBackLockMapMainSection(loadId, obj, isLock);
                    }
                }
            },
            error: function (error) {
                ShowAlert('Error occurred while locking load. Please reload the page and try again!');
            }
        });
    }
}

function callBackLockStops(obj, isLock) {

    var stops = $('#leftControlPanel_OptStops');
    isLock = (isLock == 'true' || isLock == true ? false : true);

    handleLockLoadEvents(stops, isLock);
}

function callBackLockLoadMainGrid(obj, isLock) {
    if (isLock == 'true' || isLock == true) {
        $(obj).closest("article").find('.selector-draggable').draggable("enable");
        $(obj).closest("article").find('.selector-draggable').droppable("enable");
    } else {
        $(obj).closest("article").find('.selector-draggable').draggable("disable");
        $(obj).closest("article").find('.selector-draggable').droppable("disable");
    }
}

function disableDragAndDropForLockLoads() {
    var allLoads = $('.main-driverload');

    $.each(allLoads, function (i, o) {
        var islocked = $(o).attr('data-islocked');

        if (islocked == 'true') {
            $(o).find('.selector-draggable').draggable("disable");
            $(o).find('.selector-draggable').droppable("disable");
            $(o).find('#droppableFooter').hide();
        } else {
            $(o).find('#droppableFooter').show();
        }
    });
}

/********************************* drag and drop functionality STARTS *******************************/
function bindDraggables(outer) {
    outer.find(".selector-draggable").not('.Completed').draggable({
        cursor: "crosshair",
        cursorAt: { left: 50, top: 5, bottom: 5 },
        helper: "clone",
        opacity: 0.85,
        revert: "invalid",
        revertDuration: 100,
        //scope: "tasks",
        //snap: true,
        //zIndex: 100,

        start: function (event, ui) {
            //sourceTableID = $(event.currentTarget).closest("table").attr("id");
        },
        drag: function (event, ui) {
        },
        stop: function (event, ui) {
        }
    });
    outer.find(".selector-droppable").droppable({
        drop: function (event, ui) {
            var sourceTableID = ui.draggable.closest("table").attr("id");
            var destinationTableID = $(this).closest("table").attr("id");
            var destinationTableClass = $(this).closest("table");
            var facilityArticle = $(this).closest("article");

            if (sourceTableID == destinationTableID) {
                return false;
            }

            // var newtablenamearray = sourceTableID.split("-");
            var newTableNamearray = destinationTableID.split("-");
            var touchId = ui.draggable.attr('touchId');
            var selectedDate = $("#txtDate").val();

            if (destinationTableClass.hasClass("tblunassignedtouches")) {
                MoveTouchToUnassignedList($("#" + sourceTableID).attr("loadId"), touchId, selectedDate, sourceTableID, ui.draggable, destinationTableID);
            }
            else {
                var islocked = $("#" + destinationTableID).parent("div").parent("article").attr('data-islocked');

                if (islocked == 'true') {
                    return false;
                }

                var isDriverInActive = $("#" + destinationTableID).parent("div").parent("article").attr('data-DayWork');

                if (isDriverInActive != "S") {
                    return false;
                }

                //var trucknum = $("#" + newtblname).parent("div").find('select').val();
                var trucknum = $("#" + destinationTableID).parent("div").parent("article").find('select').val();

                //With out Truck selection, dont process user request, Stop here and alert user to select Truck 
                if (trucknum == "" || trucknum == undefined) {
                    ShowAlert("Please select a truck before adding a touch");
                    return false;
                }

                //Showing Warning message if there is no street address to touch.
                var destAddress = ui.draggable.find(".destAddress").text().split(',');

                var NoLatLang = ui.draggable.data("nolatlang");

                var custName = $.trim(ui.draggable.find(".footable-first-column").text());
                var tchAndOrderNum = $.trim(ui.draggable.find(".TouchNumberDisp").text()).slice(0, -7);
                var latLngTouchID = $.trim(ui.draggable.find(".TouchOrderNumber").val());

                if (!$.trim(destAddress[0]) && (typeof ui.draggable.find(".destAddress") != 'undefined')) {
                    var conf = confirm(tchAndOrderNum + ' ' + custName + " does not have a street address. Please update the address in Salesforce and Reload.");
                    if (!conf) {
                        return false;
                    }
                }
                else if (NoLatLang == "True" && NoLatLang != 'undefined') {
                    $('#ConfirmLatLang').find('#latLngOrderNum').val(latLngTouchID);//assigned value to hidden field in latlng popup, inorder to trigger click event of anchor tag to show customer details.
                    CustomConfirmBoxLatLang(function (confirmed, latLngTouchID) {
                        //1 -- Navigate to touch Page, 2 -- add touch to driver, 3 -- cancel adding
                        if (confirmed == 1) {
                        }
                        else if (confirmed == 2) {
                            dropTouchToDriver(selectedDate, touchId, newTableNamearray, facilityArticle, trucknum, ui, sourceTableID);
                        }
                        else if (confirmed == 3) {
                            return false;
                        }
                    })
                    return false;
                }
                else {
                    dropTouchToDriver(selectedDate, touchId, newTableNamearray, facilityArticle, trucknum, ui, sourceTableID);
                }
            }
        }
    });
}

function dropTouchToDriver(selectedDate, touchId, newTableNamearray, facilityArticle, trucknum, ui, sourceTableID) {
    //var orderNum = ui.draggable.find("td:nth-child(2)").text();
    var orderNum = ui.draggable.find(".TouchOrderNumber").val();
    var oldtbrowindex = -1; //TODO
    //var loadId = $("#" + destinationTableID).attr("loadId");
    var driverId = newTableNamearray[1];
    var loadId = GetLoadId(driverId);
    var homeSiteNumber = ui.draggable.closest(".market-facility").attr("storeNumber");
    var driverSiteNumber = facilityArticle.attr("data-globalsite-num");

    //if (newtablenamearray.length > 1) {
    var data = {
        date: selectedDate,
        loadId: loadId,//$("#" + newtblname).parents("article").find(".btnLock").attr('id'),
        touchId: touchId,
        driverId: driverId,
        homeSiteNumber: homeSiteNumber,
        driverSiteNumber: driverSiteNumber,
        orderNumber: orderNum,
        truckID: trucknum
    }

    AddTouchToLoad(data, sourceTableID, ui.draggable);
}

function GetLoadId(driverId) {
    return $('#hdnLoadID-' + driverId).val();
}

function MoveTouchToUnassignedList(loadId, touchId, date, oldtbName, oldtbrow, unassignedFacilityId) {

    $.ajax({
        url: URLHelper.removeOrderfromLoadUrl,
        data: {
            touchId: touchId, date: date
        },
        success: function (retVal) {
            if (retVal.success) {
                var trRemovedObj;
                var unassignedtablenamearray = unassignedFacilityId.split("-");

                var tableId = 'tblunassignedtouchesFacility-' + $(retVal.data).attr('data-globalsitenum');

                $('#' + tableId).removeClass('footable-loaded');
                $('#' + tableId + ' tbody').append(retVal.data);

                $('.footable').footable();

                ////Removing collapsable panel if there are displaying -- LL-37
                if ($(oldtbrow).hasClass('footable-detail-show')) {
                    if ($(oldtbrow).next('tr').hasClass('footable-row-detail')) {
                        if ($(oldtbrow).next('tr').find('td:first').hasClass('footable-row-detail-cell')) {
                            $(oldtbrow).next('tr').remove();
                        }
                    }
                }
                oldtbrow.remove();

                //Here system updates touch count in old table/load 
                updateUnassignedTouchCount(tableId);

                var oldtablenamearray = oldtbName.split("-");
                //Here system updates touch count in new table/load
                updateLoadTouchesCount(oldtablenamearray[1], true);

                bindDraggables($('#' + tableId));
            } else {
                ShowAlert(retVal.message);
            }
        },
        error: function (error) {
            //document.getElementById("divProgress").style.display = 'none;';
            ShowAlert('Error occurred while processing your request. Please reload the page and try again!');
        }
    });
}

function AddTouchToLoad(data, oldtbName, oldtbrow) {
    $.ajax({
        url: URLHelper.addTouchToLoadUrl,
        data: {
            date: data.date,
            loadId: data.loadId,//$("#" + newtblname).parents("article").find(".btnLock").attr('id'),
            touchId: data.touchId,
            driverId: data.driverId,
            //siteNumber: data.siteNumber,
            homeSiteNumber: data.homeSiteNumber,
            driverSiteNumber: data.driverSiteNumber,
            orderNumber: data.orderNumber,
            truckID: data.truckID

        },
        success: function (retVal) {

            if (retVal.indexOf("This touch cannot remove from this load") > -1) {
                ShowAlert(retVal);
            } else if (retVal == "Failure") {
                ShowAlert('Error occurred while adding touch to load. Please reload the page and try again!');
                //$("#btnRealod").click();
            } else {

                //This needs to be removed and below code need to correct it, to restrict number of calls?
                //$("#btnRealod").click();
                //return;

                var tableId = 'tbldriver-' + data.driverId;

                $('#' + tableId).removeClass('footable-loaded');
                $('#' + tableId + ' tbody').append(retVal);

                if (data.loadId <= 0) {
                    setLoadIDRemovehdnElement(data.driverId);
                }

                $('.footable').footable();

                //if (oldtbName == 'tblunassignedtouches') {
                if ($("#" + oldtbName).hasClass('tblunassignedtouches')) {
                    //$($('#' + oldtbName + ' tr')[oldtbrowindex]).remove();
                    ////Removing collapsable panel if there are displaying -- LL-37
                    if ($(oldtbrow).hasClass('footable-detail-show')) {
                        if ($(oldtbrow).next('tr').hasClass('footable-row-detail')) {
                            if ($(oldtbrow).next('tr').find('td:first').hasClass('footable-row-detail-cell')) {
                                $(oldtbrow).next('tr').remove();
                            }
                        }
                    }
                    oldtbrow.remove();

                    //Here system updates touch count in unassigned touch list 
                    updateUnassignedTouchCount(oldtbName);

                } else {
                    $('#' + oldtbName + ' tr').each(function (o, p) {
                        if ($(p).attr('touchid') == data.touchId) {
                            if ($(p).hasClass('footable-detail-show')) {
                                if ($(p).next('tr').hasClass('footable-row-detail')) {
                                    if ($(p).next('tr').find('td:first').hasClass('footable-row-detail-cell')) {
                                        $(p).next('tr').remove();
                                    }
                                }
                            }
                            $(p).remove();
                        }
                    });

                    var oldtablenamearray = oldtbName.split("-");

                    //Here system updates touch count in old table/load 
                    updateLoadTouchesCount(oldtablenamearray[1], true);
                }
                //Here system updates touch count in new table/load
                updateLoadTouchesCount(data.driverId, true);

                bindDraggables($('#' + tableId));
            }
        },
        error: function (error) {
            //document.getElementById("divProgress").style.display = 'none;';
            ShowAlert('Error occurred while adding touch to load. Please reload the page and try again!');
            return;
        }
    });
}

function setLoadIDRemovehdnElement(driverId) {
    var tableId = '#tbldriver-' + driverId;
    var loadId = $(tableId).find('#hdnLoadID').val();

    $('#hdnLoadID-' + driverId).val(loadId);

    //After we assign loadid to actual load then we need to remove this element from the DOM
    $(tableId).find('#hdnLoadID').remove();
}

function updateLoadTouchesCount(driverId, isOldTable) {
    var tableId = '#tbldriver-' + driverId;

    var totalTouches = $('.selector-draggable', tableId).length;
    //This case for: if we remove all touches from a load then we are removing that load from back end so we need to reload the page
    //Sorry: if you find better option you can replace this reload page functionality
    if (isOldTable == true && totalTouches == 0) {
        //$("#btnRealod").click();

        $('#hdnLoadID-' + driverId).val(0);
        $('#LoadTruck-' + driverId).val('');
    }

    $('#cntTouchesFor-' + driverId).html(totalTouches);
}

function updateUnassignedTouchCount(tableId) {
    var totalTouches = $('.selector-draggable', '#' + tableId).length;
    var unassignedTableId = tableId.split("-");

    $('#cntUnassignedTouches-' + unassignedTableId[1]).html(totalTouches);
}

//this should be called while savings driver schedule hours from pop-up
function updateDriverScheduleHours() {
    $("#ScheduleStartTime", $('#editForm')).removeAttr("disabled");

    $.ajax({
        url: URLHelper.saveDriverScheduleHours,
        //data: returnDriverScheduleChangeDetailsData,
        type: 'POST',
        datatype: 'json',
        data: $('#editForm').serialize(),
        success: function (retVal) {
            if (retVal.success) {
                ShowSuccessAlert('Successfully, updated driver schedule hours.');
                //refresh grid
                //$("#btnRealod").click();
                //Just refreshing Anchor tag data attributes.
                var driverId = $('#editForm').find('#driverID').val();
                var StartTime = $('#editForm').find('#ScheduleStartTime').val();
                var EndTime = $('#editForm').find('#ScheduleEndTime').val();

                var scheduleStartTime = StartTime.split(' ')[0] + ":00";
                //Assigning values back in stopoptimization screen
                $('#driverStartTimeStopLevel').val(scheduleStartTime);
                $('.stopsDriverStartTime').val(scheduleStartTime);

                var currentAnchorHtml = $('#aEditDriverScheduleHours[data-driverid="' + driverId + '"]');

                if ($(currentAnchorHtml).attr('data-isDriverStarted') != "True") {
                    var loadDate = $('#txtDate').val();
                    var dtDrStartTime = new Date(loadDate + " " + StartTime);
                    let timeToDisplay = dtDrStartTime;
                    if (dtDrStartTime < (new Date())) {
                        timeToDisplay = new Date();
                    }

                    $('#driverStartTime').val(timeToDisplay.formatHHMMSS());
                }

                $(currentAnchorHtml).attr('data-startTime', StartTime);
                $(currentAnchorHtml).attr('data-endTime', EndTime);

                $(currentAnchorHtml).find('span').html(StartTime + ' - ' + EndTime);

                $("#dialogContent").dialog('close');
            }
        },
        error: function (error) {
            alert('Error occurred while updating driver schedule hours, please reload page.');
        },
    });
}

function returnDriverScheduleChangeDetailsData() {
    return collection = {
        driverID: $("#driverID").val(),
        dates: $("#id_g").val(),
        ScheduleStartTime: $("#ScheduleStartTime").val(),
        ScheduleEndTime: $("#ScheduleEndTime").val(),
        DayWork: $("#DayWork").val()
    };
}

function deleteLoad(loadId, driverId) {
    if (loadId <= 0) {
        ShowAlert('Load is not yet created.');
        return false;
    }

    var wrgMsg = "";
    var isLoadLocked = $('#mainLoad-' + driverId).attr('data-islocked');

    if (isLoadLocked == true || isLoadLocked == 'true') {
        wrgMsg = 'You are deleting a locked route. Are you sure you want to delete the route?';
    } else {
        wrgMsg = 'Are you sure do you want to delete this load?';
    }

    if (confirm(wrgMsg)) {
        $.ajax({
            url: URLHelper.DeleteLoadUrl,
            data: {
                loadId: loadId
            },
            success: function (retVal) {
                if (retVal == "Failure") {
                    ShowAlert('Error occurred while loading drivers. Please reload the page and try again!');
                }
                else {
                    //Once user deletes load then reload it to keep everything in place
                    $("#btnRealod").click();
                }
            },
            error: function (error) {
                //document.getElementById("divProgress").style.display = 'none;';
                ShowAlert('Error occurred while processing your request. Please reload the page and try again!');
            }
        });
    }
}

function BindEditDriverDialog() {
    $("#dialogContent").dialog({
        autoOpen: false,
        modal: true,
        height: 250,
        width: 370,
        resizable: false,
        draggable: false,
        buttons: [{
            text: "OK",
            id: "btnDigOK",//by this id set now you can do all operation base on this id
            click: function () {
                $("#DayWork").prop("disabled", false);
                if (checkStartEndTime($('#editForm'))) {
                    updateDriverScheduleHours();
                }
            }
        },
        {
            text: "Continue",
            id: "btnDigContinue",//by this id set now you can do all operation base on this id
            click: function () {
                $("#DayWork").prop("disabled", false);
                updateDriverScheduleHours();
            }
        }, {
            text: "Cancel",
            id: "btnDigCancel",//by this id set now you can do all operation base on this id
            click: function () {
                $(this).dialog("close");
            }
        }],
        open: function () {
            $('#btnDigOK').focus();
        }
    });

    $("#ScheduleStartTime").timepicker({
        timeFormat: 'h:i A',
        minTime: '12:00 AM', // 11:45:00 AM,
        maxTime: '11:30 PM',
        interval: 15,
        change: function (time) {
            getWorkingHours($('#editForm'));
        }
    });
    $("#ScheduleEndTime").timepicker({
        timeFormat: 'h:i A',
        minTime: '12:00 AM', // 11:45:00 AM,
        maxTime: '11:30 PM',
        interval: 15,
        change: function (time) {
            getWorkingHours($('#editForm'));
        }
    });
}

function checkStartEndTime(formid) {
    var startTime = $("input#ScheduleStartTime", formid).val();
    var endTime = $("input#ScheduleEndTime", formid).val();
    var errorBlock = $("#FormError", formid);

    var driverId = $('#editForm').find('#driverID').val();
    //var currentAnchorHtml = $('#aEditDriverScheduleHours[data-driverid="' + driverId + '"]');

    if (startTime != '' && endTime != '') {
        var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
        var endTimeDt = new Date("1/1/1970" + ' ' + endTime);

        if (startTimeDt >= endTimeDt) {
            $(errorBlock).css("display", "table-row").find(".ui-state-error").html("Error: Start time should be less than end time.");
            return false;
        }

        //if ($(currentAnchorHtml).attr('data-isDriverStarted') == "True" || $(currentAnchorHtml).attr('data-islocked') == "true") {
        if ($("input#isLockedForEditDriverSchedule", formid).val() == "true") {
            var loadEndtime = new Date("1/1/1970" + ' ' + $("input#loadEndTimeForEditDriverSchedule", formid).val());
            var loadStarttime = new Date("1/1/1970" + ' ' + $("input#loadStartTimeForEditDriverSchedule", formid).val());

            if ((endTimeDt < loadEndtime) || (startTime != loadStarttime)) {
                CustomConfirmBox("The driver start time isn’t the same as the route start time or the route ends after the driver end time.<br /><br />Press the continue button if you want to make the changes or cancel to not make the changes.  If you decide to accept the changes, please optimize the route to provide more accurate ETAs.", function (confirmed) {
                    if (confirmed)
                        updateDriverScheduleHours();
                    else {
                    }
                })
                return false;
            }
        }
    }
    $(errorBlock).css("display", "none").find(".ui-state-error").html("");
    return true;
};

function getWorkingHours(formid) {
    var errorBlock = $("#FormError", formid);
    let startTime = $("input#ScheduleStartTime", formid).val();
    let endTime = $("input#ScheduleEndTime", formid).val();
    let DayWork = $("select#DayWork", formid).val();
    let workingHr = '0 hr 0 min';
    if ((DayWork != 'O' || DayWork != 'C') && startTime != '' && endTime != '') {
        var startTimeDt = new Date("1/1/1970" + ' ' + startTime);
        let endTimeDt = new Date("1/1/1970" + ' ' + endTime);

        if (startTimeDt < endTimeDt) {
            let differenceDt = new Date(endTimeDt - startTimeDt);

            let hours = differenceDt.getUTCHours();
            let minutes = differenceDt.getUTCMinutes();
            workingHr = hours + ' hr ' + minutes + ' min';
        }
    }
    // $('#btnDigContinue').hide();
    // $('#btnDigOK').show();
    $(errorBlock).css("display", "none").find(".ui-state-error").html("");
    $("span#DriverWorkingHours", formid).html(workingHr);
}

function showPopup(object) {
    $("#ScheduleStartTime").val($(object).attr('data-startTime'));
    $("#ScheduleEndTime").val($(object).attr('data-endTime'));

    $('select[name^="DayWork"] option:selected').attr("selected", null);
    $('select[name^="DayWork"] option[value="S"]').attr("selected", "selected");

    getWorkingHours($('#editForm'));

    $("#id_g").val($(object).attr('data-scheduledate'));
    $("#driverID").val($(object).attr('data-driverid'));
    $("#dates").val($(object).attr('data-scheduledate'));


    $("#loadEndTimeForEditDriverSchedule").val($(object).attr('data-loadEndTime'));
    $("#loadStartTimeForEditDriverSchedule").val($(object).attr('data-loadStartTime'));
    $("#isLockedForEditDriverSchedule").val($(object).attr('data-islocked'));

    $(".ui-dialog-titlebar-close").hide();

    $("#dialogContent").dialog("open");
    $("#DayWork").prop("disabled", true);
    //$(".ui-dialog :button:contains('OK')").focus();

    $('#btnDigContinue').hide();
    $('#btnDigOK').show();

    $("#FormError", $('#editForm')).css("display", "none").find(".ui-state-error").html("");

    return false;
}

function ShowAlert(msg) {
    ShowAlertMsg(msg, 'Error');
}

function ShowWarningAlert(msg) {
    ShowAlertMsg(msg, 'Warning');
}

//this field added to handle warning message in touch grouping 
//Change from error to warning - allow users to override the warning if they are grouping a larger unit to a smaller unit
var ShowWarningAlertWithCustActionMethod = undefined;
function ShowWarningAlertWithCustAction(msg, callBack) {
    ShowAlertMsg(msg, 'Warning');
    ShowWarningAlertWithCustActionMethod = callBack;
}

function ShowSuccessAlert(msg) {
    ShowAlertMsg(msg, 'Message');
}

function ShowAlertMsg(msg, title) {
    var alertPop = $('#alert-pop');
    alertPop.find(".alert-pop-title").html(title);

    $('#LLSAlertBody', alertPop).html(msg);
    alertPop.show();
}

function onAlertOk() {
    var alertPop = $('#alert-pop');
    alertPop.hide();

    if (ShowWarningAlertWithCustActionMethod)
        ShowWarningAlertWithCustActionMethod();

    ShowWarningAlertWithCustActionMethod = undefined;
}

//written by Guru Pratap on 11/Nov/2016 for showing custom confirm box without changing previous design
var ShowConfirmCustomCallBack = undefined;
function CustomConfirmBox(msg, callBack) {
    ShowConfirmMsg(msg);
    ShowConfirmCustomCallBack = callBack;
}

function ShowConfirmMsg(msg) {
    var confirmPop = $('#Confirm-New');

    $('#LLSAlertBody', confirmPop).html(msg);
    confirmPop.show();
}

function ConfirmContinue() {
    var confirmPop = $('#Confirm-New');
    confirmPop.hide();
    ShowConfirmCustomCallBack(true);
}

function ConfirmOk() {
    var confirmPop = $('#Confirm-New');
    confirmPop.hide();
    ShowConfirmCustomCallBack(false);
}

/*MARKETS START*/

function toggleFacilityLoad(obj) {
    $(obj).parent().parent().find('article').toggle();
}

function toggleFacilityUnassignedTouches(obj) {
    $(obj).closest('.market-facility').find('table').toggle();
}

/*MARKETS END*/

Date.prototype.formatMMDDYYYYHHMMSS = function () {
    var hr = parseInt(this.getHours());
    var ampm = hr > 11 ? 'PM' : 'AM';
    var hr = hr > 12 ? (hr - 12) : hr;
    var mm = parseInt(this.getMinutes()) > 9 ? this.getMinutes() : '0' + this.getMinutes();
    return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear() + " " + hr + ":" + mm + " " + ampm;
}

Date.prototype.formatHHMMSS = function () {
    var hr = parseInt(this.getHours());
    var ampm = hr > 11 ? 'PM' : 'AM';
    var hr = hr > 12 ? (hr - 12) : hr;
    var mm = parseInt(this.getMinutes()) > 9 ? this.getMinutes() : '0' + this.getMinutes();
    return hr + ":" + mm + " " + ampm;
}

Date.prototype.format24HHMMSS = function () {
    var hr = parseInt(this.getHours());
    var mm = parseInt(this.getMinutes()) > 9 ? this.getMinutes() : '0' + this.getMinutes();
    return hr + ":" + mm + ":00";
}