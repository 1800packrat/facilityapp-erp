﻿$(document).ready(function () {
    LoadHoverPanel();

    console.log("Load Calendar"); //SFERP-TODO-CTRMV

    // Highlight the selected date
    $('td[data-isdateselected=1] a').each(function () {
        $(this).css('font-weight', 'bold').css('font-size', '18px');
    });
});

function LoadHoverPanel() {
    // Formatting calender anchors
    $('#calSchedule_upCal').find('a').css('text-decoration', 'none');

    $('td').each(function () {
        var attr = $(this).attr('data-regularmiles');
        var td = $(this);
        if (typeof attr !== typeof undefined && attr !== false) {


            // Remove title for the anchor inside td
            $(td).find('a').removeAttr('title');
            $('table[id$=calScheduleAnyTime]').removeAttr('title');
            $('table[id$=calScheduleAM]').removeAttr('title');
            $('table[id$=calSchedulePM]').removeAttr('title');


            $(td).on("mouseenter", function () {
                var facilitymiles = 0;
                var bookedmiles = 0;
                var regularmiles = $(td).attr('data-regularmiles');
                var salesregularmiles = $(td).attr('data-salesregularmiles');
                var serviceregularmiles = $(td).attr('data-serviceregularmiles');
                var reservedmiles = 0;
                if ($(td).attr("data-reservedmiles"))
                    reservedmiles = $(td).attr('data-reservedmiles');

                if ($(td).attr("data-facilitymiles"))
                    facilitymiles = $(td).attr('data-facilitymiles');
                if ($(td).attr("data-bookedmiles"))
                    bookedmiles = $(td).attr('data-bookedmiles');

                var tripmiles = $(td).attr('data-tripmiles');
                var roundtripmiles = $(td).attr('data-roundtripmiles');
                var touchmiles = $(td).attr('data-touchmiles');
                var date = $(td).attr('data-date');

                var data = [
                            {
                                date: date,
                                regular: parseFloat(regularmiles).toFixed(0),
                                salesregular: parseFloat(salesregularmiles).toFixed(0),
                                serviceregular: parseFloat(serviceregularmiles).toFixed(0),
                                reserved: parseFloat(reservedmiles).toFixed(0),
                                facility: parseFloat(facilitymiles).toFixed(0),
                                booked: parseFloat(bookedmiles).toFixed(0),
                                trip: parseFloat(tripmiles).toFixed(0),
                                touch: parseFloat(touchmiles).toFixed(0),
                                roundtrip: parseFloat(roundtripmiles).toFixed(0)
                            }
                ];

                var templateContent = $("#hoverTemplate").html();
                var template = kendo.template(templateContent);
                var result = kendo.render(template, data);
                $(td).append(jQuery(result));

                // Adjustments for AM/PM
                if ($(td).closest('table').attr('id').indexOf('calSchedule_calSchedulePM') != -1) {

                    // CHANGE THE TITLE TO SHOW FACILITY PM MILES
                    var span_facility_miles = $(this).find('span[id=span-facility-miles]').first();
                    var html = $(span_facility_miles).html().replace('Facility Miles', 'Facility PM Miles');
                    $(span_facility_miles).html(html);

                    // CHANGE THE TITLE TO SHOW BOOKED PM MILES
                    var span_booked_miles = $(this).find('span[id=span-booked-miles]').first();
                    var html = $(span_booked_miles).html().replace('Booked Miles', 'Booked PM Miles');
                    $(span_booked_miles).html(html);

                    // REMOVE RESERVE MILES, SERVICE REGULAR MILES, SALES REGULAR MILES

                    $(td).find('#span-reserved-miles').remove();
                    $(td).find('#span-service-regular-miles').remove();
                    $(td).find('#span-sales-regular-miles').remove();
                }

                if ($(td).closest('table').attr('id').indexOf('calSchedule_calScheduleAM') != -1) {
                    // CHANGE THE TITLE TO SHOW FACILITY PM MILES

                    var span_facility_miles = $(this).find('span[id=span-facility-miles]').first();
                    var html = $(span_facility_miles).html().replace('Facility Miles', 'Facility AM Miles');
                    $(span_facility_miles).html(html);

                    // CHANGE THE TITLE TO SHOW BOOKED AM MILES
                    var span_booked_miles = $(this).find('span[id=span-booked-miles]').first();
                    var html = $(span_booked_miles).html().replace('Booked Miles', 'Booked AM Miles');
                    $(span_booked_miles).html(html);

                    // REMOVE RESERVE MILES, SERVICE REGULAR MILES, SALES REGULAR MILES

                    $(td).find('#span-reserved-miles').remove();
                    $(td).find('#span-service-regular-miles').remove();
                    $(td).find('#span-sales-regular-miles').remove();
                }
                if ($(td).closest('table').attr('id').indexOf('calSchedule_calScheduleAnyTime') != -1) {
                    // REMOVE REGULAR MILES 
                    $(td).find('#span-regular-miles').remove();
                }

                // Set hover popup with the window and visible

                var win = $('#hoverpanel');
                if (win.length > 0) {

                    var screenwidth = $('body').width();
                    var screenheight = $('#divAll').height();
                    var width = win.width();
                    var height = win.height();
                    var pos = win.position();
                    var rightpos = width + pos.left;
                    if (rightpos > screenwidth) {
                        var moveleft = rightpos - screenwidth + 10;
                        win.css('margin-left', '-' + moveleft + 'px');
                    } else {
                        win.css('margin-left', '0px');
                    }

                    //// Adjust top position 

                    //var totalheight = height + pos.top - 250;
                    //if (totalheight > screenheight) {
                    //    var movetop = totalheight - (2 * height) - 120;
                    //    win.css('margin-top', movetop + 'px');
                    //} else {
                    //    win.css('margin-top', '0px');
                    //}

                    // If it is not a supervisor remove booked miles & facility miles from hover popup & adjust height

                    if (!$(td).attr("data-facilitymiles")) {
                        $('span[id=span-facility-miles]').remove();
                    }
                    if (!$(td).attr("data-bookedmiles")) {
                        $('span[id=span-booked-miles]').remove();
                    }
                    if (!$(td).attr("data-reservedmiles")) {
                        $('span[id=span-reserved-miles]').remove();
                    }
                }


            }).on("mouseleave", function () {
                $("#hoverpanel").remove();
            });

        }
    });
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function funGetNoDateSelect(scheduledate) {
    var today = new Date();
    if (new Date(scheduledate) < new Date(today.toDateString())) {
        alert('Cannot select past date');
        return false;
    }
    alert('Please select proper date');
    return false;
}

function DENotification(scheduledate, deliverdate, ScheduleType, istxtInput) {

    var dDate = new Date(deliverdate);
    if (dDate.getFullYear() == "1901") {
        document.getElementById('<%=scheduledate.ClientID%>').value = scheduledate;
        alert(document.getElementById('<%=scheduledate.ClientID%>').value + '   ' + ScheduleType);
        document.getElementById('<%=hdscheduletype.ClientID%>').value = ScheduleType;
        document.getElementById('btnsave').disabled = !istxtInput;
        document.getElementById('<%=hdIsUserEntered.ClientID%>').value = istxtInput;
    }
    else {
        var sDate = new Date(scheduledate);
        var startDate = dDate.add(-4, "days", false);
        var endDate = dDate.add(4, "days", false);
        if ((sDate >= startDate) && (endDate >= sDate)) {
            document.getElementById('<%=scheduledate.ClientID%>').value = scheduledate;
            alert(document.getElementById('<%=scheduledate.ClientID%>').value + '   ' + ScheduleType);
            document.getElementById('<%=hdscheduletype.ClientID%>').value = ScheduleType;
            document.getElementById('btnsave').disabled = !istxtInput;
            document.getElementById('<%=hdIsUserEntered.ClientID%>').value = istxtInput;

        } else {
            var verbiage = "The touch cannot be scheduled for this selected date because the preferred delivery is " + dDate.toDateString() + " Please Requote to change the delivery date";
            document.getElementById('btnsave').disabled = true;
            alert(verbiage);
        }
    }
    return false;
}

function SaveScheduleDate(istxtInput, scheduledate, ScheduleType, deliverydate, touchType, role) {
    if (!isDate(scheduledate) && istxtInput) {
        alert('Invalid date format!Please enter in mm/dd/yyyy format.');
        document.getElementById('btnsave').disabled = true;
    }
    else if (CheckRange(scheduledate, role)) {
        if (scheduledate == "0") {
            document.getElementById('btnsave').disabled = true;
            alert("TOUCH BEING EDITED IS NOT AN INITIAL DELIVERY; NON-DELIVERY TOUCH CANNOT BE SCHEDULED ON RED DATE.");

        }

        if (touchType == "DE-Deliver Empty") {
            DENotification(scheduledate, deliverydate, ScheduleType, deliverydate, istxtInput);
            document.getElementById('<%=hdIsUserEntered.ClientID%>').value = istxtInput;

        } else {
            document.getElementById('<%=scheduledate.ClientID%>').value = scheduledate;
            alert(document.getElementById('<%=scheduledate.ClientID%>').value + '   ' + ScheduleType);
            document.getElementById('<%=hdscheduletype.ClientID%>').value = ScheduleType;
            document.getElementById('btnsave').disabled = false;
            document.getElementById('<%=hdIsUserEntered.ClientID%>').value = istxtInput;

        }
    }
    return false;
}