﻿var CountPartTypes = 0;
var UnitNumberOnBlurEventFire = true;
var err = {
    RequriedField: 'This is required field',
    ToDateShouldBeGreate: 'To Date cannot be before From Date',
    CommentsMaxLength: 'Comments should not be more than 250 characters'
};

$(document).ready(function () {

    $("#NextPreventiveMaintDateIcon").click(function () {
        $("#NextPreventiveMaintDate").focus();
    });
    $('#NextPreventiveMaintDate').datepicker({
        minDate: 0
    });

    $("#TransferValidToIcon").click(function () {
        $("#TransferValidTo").focus();
    });
    $('#TransferValidTo').datepicker({
        minDate: 0,
        onSelect: function (date) {
            //return ValidateDate(date, $('#TransferValidTo').val());
            return hideErrMsg($('#TransferValidTo'), 'errTransferValidTo');
        }
    });

    $("#TransferValidFromIcon").click(function () {
        $("#TransferValidFrom").focus();
    });
    $('#TransferValidFrom').datepicker({
        minDate: 0,
        onSelect: function (date) {
            //return ValidateDate(date, $('#TransferValidFrom').val());
            return hideErrMsg($('#TransferValidFrom'), 'errTransferValidFrom');
        }
    });

    $('.IsFleetTransfer').change(function (obj) {

        if (this.value == "true") {
            $('#liTransferFleetForm').show();
        } else {
            $('#liTransferFleetForm').hide();
        }
    });

    $('.IsTransferTemparory').change(function (obj) {

        if (this.value == "true") {
            $('.transferFleetDate').show();
        } else {
            $('.transferFleetDate').hide();
        }
    });
});

function hideErrMsg(obj, errMsg) {
    if (obj.val().length > 0) {
        $('#' + errMsg).hide();
    }
}

function ChangeTruckNumber(obj) {
    var selVal = $(obj).val();
    if (selVal.length > 0)
        $('#errTruckNumber').html('').hide();
    else
        $('#errTruckNumber').html(err.RequriedField).show();
}
function ChangeTruckStatus(obj) {
    var selVal = $(obj).val();
    if (selVal.length > 0)
        $('#errTruckStatus').html('').hide();
    else
        $('#errTruckStatus').html(err.RequriedField).show();
}

function ChangeFacility(obj) {
    var selVal = $(obj).val();
    if (selVal.length > 0)
        $('#errFacilityStoreNo').html('').hide();
    else
        $('#errFacilityStoreNo').html(err.RequriedField).show();
}

function onConfirmYes() {
    closeConfirmation();
    $('#progress').show();
    //alert(this);
    $('form#TruckStatusForm').submit();
}
function onConfirmNo() {
    closeConfirmation();
    $('#progress').show();
    //resetForm();
    window.location.href = '@Url.Action("Index", "TruckStatus")';
}
function closeConfirmation() {
    $('#confirmation-pop').hide();
}
//function resetForm() {
//    $('#TruckNumber').val(0);
//    $('#TruckStatus').val('');
//    //$('#NextPreventiveMaintDate').val(0);
//    $('#Comments').val('');
//    $('#TruckDetailsId').val(0);
//}

function ValidateForm() {

    var rtVal = true;
    var errMsg = "";

    if ($('#TruckNumber').val() == '' || $('#TruckNumber').val().trim().length <= 0) {
        rtVal = false;
        $('#errTruckNumber').html(err.RequriedField).show();
    }

    if ($('#TruckStatus').val() == '' || $('#TruckStatus').val().trim().length <= 0) {
        rtVal = false;
        $('#errTruckStatus').html(err.RequriedField).show();
    }

    var truckDetailsId = $('#TruckStatus').val();

    if ((truckDetailsId == 2 || truckDetailsId == 3) && $('#Comments').val().trim().length <= 0) {
        rtVal = false;
        $('#errComments').html(err.RequriedField).show();
    }

    if ($('#Comments').val().trim().length > 250) {
        rtVal = false;
        $('#errComments').html(err.CommentsMaxLength).show();
    }


    var isTransfer = $('input:radio[name=IsFleetTransfer]:checked').val();// $('#IsFleetTransfer').is(':checked');

    if (isTransfer == "true") {
        var TransferStoreNo = $('#TransferStoreNo').val();

        if (TransferStoreNo <= 0) {
            rtVal = false;
            $('#errFacilityStoreNo').html(err.RequriedField).show();
        }

        var isTempTransfer = $('input:radio[name=IsTransferTemparory]:checked').val();// $('#IsTransferTemparory').is(':checked');
        if (isTempTransfer == "true") {

            //Date Validation
            var fromDate = $('#TransferValidFrom').val();
            var toDate = $('#TransferValidTo').val();

            if (fromDate.length <= 0) {
                rtVal = false;
                $('#errTransferValidFrom').html(err.RequriedField).show();
            }
            else if (toDate.length <= 0) {
                rtVal = false;
                $('#errTransferValidTo').html(err.RequriedField).show();
            }
            else if (Date.parse(fromDate) > Date.parse(toDate)) {
                rtVal = false;
                $('#errTransferValidTo').html(err.ToDateShouldBeGreate).show();
            }
        }
    }

    if (rtVal == true)
        $('#progress').show();

    return rtVal;
}