﻿var mapEditAddress,
    homeMarker,
    infoWindow,
    isAddressUpdatedAtleastOnce = false;

//var mapAddressEntered;
var geocoder; //To use later

function InitializeAddressEditMaps() {
    geocoder = new google.maps.Geocoder();

    homelatlng = new google.maps.LatLng(latitude, longitude);

    var mapOptions = {
        zoom: 14,
        center: homelatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
            //style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_CENTER
        }
    };

    mapEditAddress = new google.maps.Map(document.getElementById('mapEditAddress'), mapOptions);
    infoWindow = new google.maps.InfoWindow();

    mapEditAddress.addListener('click', function (e) {
        placeMarker(e.latLng);

        geocoder.geocode({ latLng: homeMarker.getPosition() }, function (responses) {
            if (responses && responses.length > 0) {
                infoWindow.close();
                infoWindow.setContent("<div class='place'>" + responses[0].formatted_address + "<br /> </div>");
                infoWindow.open(mapEditAddress, homeMarker);

                $(mapSelectedLocationAddress).html(responses[0].formatted_address);
                $('#SearchMapInput').val(responses[0].formatted_address);
            } else {
                alert('Error: Google Maps could not determine the address of this location.');
            }
        });

    });

    initAutoComplete();

    $("#btnInvalidAddrssToggleInstructions").on("click", function (e) {
        e.preventDefault();

        $('#divTouchInstructions').toggle();
    });

    $("#btnInvalidAddrssToggleDetails").on("click", function (e) {
        e.preventDefault();

        $('#mapAddressIssueContent').toggle();
    });

    google.maps.event.addListener(homeMarker, 'dragend', function (event) {
        geocodePosition(homeMarker.getPosition());
    });
}

function geocodePosition(pos) {
    geocoder.geocode({
        latLng: pos
    }, showMarkerToolTip);
}

function showMarkerToolTip(responses, status) {
    console.log(responses);
    if (status == google.maps.GeocoderStatus.OK) {
        infoWindow.close();
        infoWindow.setContent("<div class='place'>" + responses[0].formatted_address + "<br /> </div>");
        infoWindow.open(mapEditAddress, homeMarker);

        $(mapSelectedLocationAddress).html(responses[0].formatted_address);
        $('#SearchMapInput').val(responses[0].formatted_address);
    } else {
        alert('Cannot determine address at this location.');
    }
}

function initAutoComplete() {
    var input = document.getElementById('SearchMapInput');

    mapEditAddress.controls[google.maps.ControlPosition.TOP_CENTER].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo('bounds', mapEditAddress);

    //var infowindow = new google.maps.InfoWindow();
    //var infowindowContent = document.getElementById('infowindow-content');
    //infoWindow.setContent(infowindowContent);

    homeMarker = new google.maps.Marker({
        map: mapEditAddress,
        draggable: true,
        position: homelatlng
    });

    autocomplete.addListener('place_changed', function () {
        infoWindow.close();
        homeMarker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            window.alert("Unable to map address. Please continue with the instructions as the top of the page.");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            mapEditAddress.fitBounds(place.geometry.viewport);
        } else {
            mapEditAddress.setCenter(place.geometry.location);
            mapEditAddress.setZoom(17);  // Why 17? Because it looks good.
        }
        homeMarker.setPosition(place.geometry.location);
        homeMarker.setVisible(true);

        //var address = '';
        //if (place.address_components) {
        //    address = [
        //      (place.address_components[0] && place.address_components[0].short_name || ''),
        //      (place.address_components[1] && place.address_components[1].short_name || ''),
        //      (place.address_components[2] && place.address_components[2].short_name || '')
        //    ].join(' ');
        //}

        //infowindowContent.children['place-icon'].src = place.icon;
        //infowindowContent.children['place-name'].textContent = place.name;
        //infowindowContent.children['place-address'].textContent = address;
        infoWindow.setContent("<div class='place'>" + place.formatted_address + "<br /> </div>");
        $(mapSelectedLocationAddress).html(place.formatted_address);
        infoWindow.open(mapEditAddress, homeMarker);
    });
}

google.maps.event.addDomListener(window, 'load', InitializeAddressEditMaps);

////Call this wherever needed to actually handle the display
function showAddressonMap() {
    var mapAddress1 = $('#LatLngAddress1').val().trim();
    var mapCity = $('#LatLngCity').val().trim().split(' ').join('+');
    var mapZipCode = $('#LatLngZip').val().trim();

    mapAddressEntered = mapCity + '+' + mapZipCode;

    geocoder.geocode({ 'address': mapAddressEntered }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            //Got result, center the map and put it out there
            placeMarker(results[0].geometry.location);

            //console.log(results[0]);
            infoWindow.close();
            //infoWindow.setContent("<div class='place' style='min-width: 300px'>" + results[0].formatted_address + "<br /> </div>");
            //infoWindow.open(mapEditAddress, homeMarker);

            $(mapSelectedLocationAddress).html(results[0].formatted_address);
            $('#SearchMapInput').val(mapAddress1 + ' ' + results[0].formatted_address);
        } else {
            $(mapSelectedLocationAddress).html("Geocode was not successful for the following reason: " + status);
        }
    });
}

function placeMarker(position) {
    if (homeMarker) {
        homeMarker.setPosition(position);
    } else {
        homeMarker = new google.maps.Marker({
            position: position,
            draggable: true,
            map: mapEditAddress
        });
    }
    mapEditAddress.setCenter(position);
}

function openAddressesEditordialog() {
    $(".popup").center();
    $(".popup").css('left', "50px").css('top', "20px");
    $("#modalAddressesEditor").css('display', 'block');
    $('html').addClass('overlay');
    //$("#tblAddressList").footable();
    $("#modalAddressesEditor").addClass('visible');

    isAddressUpdatedAtleastOnce = false;
}

function AddressUpdaterMapView() {
    //This call needs to remove... I am just adding for demo
    AddressesEditor();

    $.ajax({
        url: URLHelper.GetMissingAddressListMapViewUrl,
        data: { date: document.getElementById("txtDate").value },
        success: function (result) {
            if (result != "0")// if not zero then there is data to display
            {
                $("#mapAddressIssue").show();
                document.getElementById('mapAddressIssueContent').innerHTML = '';
                document.getElementById('mapAddressIssueContent').innerHTML = result;

                $("#mapAddressIssueContent").find('table').tableHeadFixer();
            } else {
                $("#mapAddressIssue").hide();
            }
        },
        error: function (error) {
        },
        complete: function () {
        }
    });
}

function AddressesEditor() {
    $.ajax({
        url: URLHelper.GetMissingAddressListForEditorUrl,
        data: { date: document.getElementById("txtDate").value },
        success: function (result) {
            if (result.success == true)
                successCallBackAddressesEditor(result.data)
            else {
                $("#dvAddressIssue").hide();
            }

            if (result != "0")// if not zero then there is data to display
            {
                $("#dvAddressIssue").show();

                document.getElementById('modalAddressesEditorBody').innerHTML = '';
                document.getElementById('modalAddressesEditorBody').innerHTML = result;

                initPopupTabs();
            } else {
                $("#dvAddressIssue").hide();
                //$("#modalAddressesEditor").hide();
            }
        },
        error: function (error) {
        },
        complete: function () {
        }
    });
}

function bindCustomerInfo(callingDiv) {
    //$('.nav').removeClass('anchorBlack');
    //$(callingDiv).find('.nav').removeClass('anchorBlack');

    $('div.invalid-address-foredit').removeClass('active');
    $(callingDiv).addClass('active');

    $('#lblAddressValidate').html('');

    $('#divCustomerInfo').find('#lblCustomerName').html($(callingDiv).attr('data-CustomerName'));
    $('#divCustomerInfo').find('#lblCustomerPhone').html($(callingDiv).attr('data-CustomerPhone'));
    //$('#divCustomerInfo').find('#lblCustomerAltPhone').html($(callingDiv).attr('data-AltPhoneNumber'));

    $('#divEditAddressInfo').find('#LatLngAddress1').val($(callingDiv).attr('data-AddressLine1'));
    $('#divEditAddressInfo').find('#LatLngAddress2').val($(callingDiv).attr('data-AddressLine2'));
    $('#divEditAddressInfo').find('#LatLngCity').val($(callingDiv).attr('data-City'));
    $('#divEditAddressInfo').find("#ddlStates").val($(callingDiv).attr('data-State'));
    $('#divEditAddressInfo').find('#LatLngZip').val($(callingDiv).attr('data-Zip'));

    //$('#divEditAddressInfo').find('#LatLngInstructions').val('');
    $('#divEditAddressInfo').find('#LatLngInstructions').val($(callingDiv).find('#hdTouchInstructions').val());
    $('#checkGoogleMap').find('#mapInstructions').val($(callingDiv).find('#hdTouchInstructions').val());


    //$('#divAddressHiddenParams').find('#mapAddress1').val($(callingAnchor).parent().find('#hdAddressLine1').val());
    //$('#divAddressHiddenParams').find('#mapZipcode').val($(callingAnchor).parent().find('#hdZip').val());

    $('#divAddressHiddenParams').find('#hdAddressType').val($(callingDiv).attr('data-addrtype'));
    $('#divAddressHiddenParams').find('#hdAddressId').val($(callingDiv).attr('id'));
    $('#divAddressHiddenParams').find('#hdTouchID').val($(callingDiv).attr('data-touchid'));

    $('#divAddressHiddenParams').find('#hdQoridLatLngMap').val($(callingDiv).attr('data-QoridLatLng'));
    $('#divAddressHiddenParams').find('#hdSequenceNumLatLngMap').val($(callingDiv).attr('data-SequenceNumLatLng'));
    $('#divAddressHiddenParams').find('#hdTouchTypeLatLngMap').val($(callingDiv).attr('data-TouchTypeLatLng'));

    setTimeout(function () { showAddressonMap(); }, 700);    
}

function validateAddress() {
    let thisContext = $('#divEditAddressInfo');
    var address = {
        AddressLine1: $('#LatLngAddress1', thisContext).val(),
        AddressLine2: $('#LatLngAddress2', thisContext).val(),
        City: $('#LatLngCity', thisContext).val(),
        State: $('#ddlStates', thisContext).val(),
        Zip: $('#LatLngZip', thisContext).val()
    };

    var postData = {
        addressToValidate: address
    };

    $.ajax({
        url: URLHelper.validAddressUrl,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(postData),
        success: function (result) {
            if (result.success) {
                //$('#lblAddressValidate').html(result.message).css('color', 'green');
                $('#lblAddressValidate').html("A valid address was found. Please click Save to update the record.").css('color', 'green');

                //After address validation, try to update suggested address to the UI
                $('#LatLngAddress1', thisContext).val(result.data.AddressLine1),
                $('#LatLngAddress2', thisContext).val(result.data.AddressLine2),
                $('#LatLngCity', thisContext).val(result.data.City),
                $('#ddlStates', thisContext).val(result.data.State),
                $('#LatLngZip', thisContext).val(result.data.Zip)
            }
            else {
                //$('#lblAddressValidate').html(result.message).css('color', 'red');
                $('#lblAddressValidate').html("Unable to map this address, please follow the instructions at the top of the page.").css('color', 'red');
            }
        }
    });
}

function saveFullAddressForLatLng() {
    var startAddress,
        toAddress,
        orderId = $('#divAddressHiddenParams').find('#hdTouchID').val(),
        touchType,
        sequenceNo,
        isOrigin,
        instructions;

    var AddressType = $('#divAddressHiddenParams').find('#hdAddressType').val();

    if (AddressType == 'dest') {
        isOrigin = false;
        toAddress = {
            ID: $('#divAddressHiddenParams').find('#hdAddressId').val(),
            AddressLine1: $('#divEditAddressInfo').find('#LatLngAddress1').val(),
            AddressLine2: $('#divEditAddressInfo').find('#LatLngAddress2').val(),
            City: $('#divEditAddressInfo').find('#LatLngCity').val(),
            State: $('#divEditAddressInfo').find('#ddlStates option:selected').val(),
            Zip: $('#divEditAddressInfo').find('#LatLngZip').val(),
        };
    }
    else {
        isOrigin = true;
        startAddress = {
            ID: $('#divAddressHiddenParams').find('#hdAddressId').val(),
            AddressLine1: $('#divEditAddressInfo').find('#LatLngAddress1').val(),
            AddressLine2: $('#divEditAddressInfo').find('#LatLngAddress2').val(),
            City: $('#divEditAddressInfo').find('#LatLngCity').val(),
            State: $('#divEditAddressInfo').find('#ddlStates option:selected').val(),
            Zip: $('#divEditAddressInfo').find('#LatLngZip').val(),
        };
    }

    var postData = {
        QORID: $('#divAddressHiddenParams').find('#hdQoridLatLngMap').val(),
        startAddress: startAddress,
        toAddress: toAddress,
        touchType: $('#divAddressHiddenParams').find('#hdTouchTypeLatLngMap').val(),
        sequenceNum: $('#divAddressHiddenParams').find('#hdSequenceNumLatLngMap').val(),
        isOrigin: isOrigin,
        Instructions: $('#divEditAddressInfo').find('#LatLngInstructions').val()
    };

    $.ajax({
        url: URLHelper.SaveFullAddressForLatLngURL,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(postData),
        beforeSend: function () { },
        success: function (result) {            
            if (result.success) {
                isAddressUpdatedAtleastOnce = true;
                //Removing background color and data attribute once address saved successfully 

                //Change color to the existing list in the UnAssigned list
                $(".tblunassignedtouches tbody tr").filter(function () {
                    return $(this).find('.TouchOrderNumber').val().trim().split('-')[1] == orderId;
                }).removeData("NoLatLang").children('td,th').css('background-color', '');

                $('#divCustomerAddresses').find("[data-touchid='" + orderId + "']").remove();

                if ($('#divCustomerAddresses').children().length == 0) {
                    clearPopupAddressEditor();
                }

                $('#divCustomerAddresses').find('#showCustomerDetails:first').click();
                //Write logic to clear the value in UI.
            }
            else {
                ShowAlert("Error in saving address! Please try again.");
            }
        }
    });
}

function saveLatLangFromMap() {
    var addressupdater, orderId = $('#divAddressHiddenParams').find('#hdTouchID').val();

    var addressupdater = {
        OrderId: $('#divAddressHiddenParams').find('#hdTouchID').val(),
        ID: $('#divAddressHiddenParams').find('#hdAddressId').val(),
        AddressType: $('#divAddressHiddenParams').find('#hdAddressType').val(),
        Lat: homeMarker.getPosition().lat(),
        Long: homeMarker.getPosition().lng()
    };

    var postData = {
        address: addressupdater,
        Instructions: $('#mapInstructions').val(),
        QORID: $('#divAddressHiddenParams').find('#hdQoridLatLngMap').val(),
        touchType: $('#divAddressHiddenParams').find('#hdTouchTypeLatLngMap').val(),
        sequenceNo: $('#divAddressHiddenParams').find('#hdSequenceNumLatLngMap').val(),
    };

    $.ajax({
        url: URLHelper.UpdateMissingAddressFromMapUrl,
        datatype: "json",
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(postData),
        success: function (result) {            
            if (result.success) {
                isAddressUpdatedAtleastOnce = true;
                //Write logic to clear the value in UI.
                //Change color to the existing list in the UnAssigned list
                $(".tblunassignedtouches tbody tr").filter(function () {
                    return $(this).find('.TouchOrderNumber').val().trim().split('-')[1] == orderId;
                }).removeData("NoLatLang").children('td,th').css('background-color', '');


                $('#divCustomerAddresses').find("[data-touchid='" + orderId + "']").remove();

                if ($('#divCustomerAddresses').children().length == 0) {
                    clearPopupAddressEditor();
                }

                $('#divCustomerAddresses').find('#showCustomerDetails:first').click();               
            }
            else {
                ShowAlert("Error in saving address! Please try again.");
            }
        }
    });
}

function initPopupTabs() {
    $('#editAddressInfo').tabs({
        active: 0,
        activate: function (event, ui) {
            var tabSelectedIndex = $("#editAddressInfo").tabs('option', 'active');
            
            if (tabSelectedIndex == 1) {
                setTimeout(function () { resizingMap(); }, 500);
                $('#divTouchInstructions').html($('#googlemapInstructions').html())
            }
            else {
                $('#divTouchInstructions').html($('#checkAddressInstructions').html())
            }
        }
    });
}

function clearPopupAddressEditor() {
    $('.popup.visible').addClass('transitioning').removeClass('visible');
    $('html').removeClass('overlay');

    setTimeout(function () { $('.popup').removeClass('transitioning'); }, 200);
    $("#modalAddressesEditor").css('display', 'none');

    if (isAddressUpdatedAtleastOnce == true) {
        $("#btnRealod").click();
    }
}

function closeConfirmLatLng() {
    $('.popup.visible').addClass('transitioning').removeClass('visible');
    $('html').removeClass('overlay');

    setTimeout(function () { $('.popup').removeClass('transitioning'); }, 200);
    $("#ConfirmLatLang").css('display', 'none');
}

//Written by pratap to handle latlang zero issue
var ShowConfirmCustomCallBackLatLang = undefined;
function CustomConfirmBoxLatLang(callBack) {
    ShowConfirmMsgLatLang();
    ShowConfirmCustomCallBackLatLang = callBack;
}

function ShowConfirmMsgLatLang() {
    var confirmPop = $('#ConfirmLatLang');
    confirmPop.show();
}

function OpenEvalLatLongAddressPopup() {
    openAddressesEditordialog();

    $('#divCustomerAddresses').find('#showCustomerDetails:first').click();

    initPopupTabs();

    $('#lblAddressValidate').html('');
    var confirmPop = $('#ConfirmLatLang');
    confirmPop.hide();
}

function resizingMap() {
    if (typeof mapEditAddress == "undefined") return;
    var currCenter = mapEditAddress.getCenter();
    google.maps.event.trigger(mapEditAddress, "resize");
    mapEditAddress.setCenter(currCenter);
}

function showEditInvalidAddFromMapView(obj) {
    openAddressesEditordialog();

    let orderId = $(obj).closest('tr').attr('data-touchid');

    //var latLngTouchId = $('#ConfirmLatLang').find('#latLngOrderNum').val();
    $('#divCustomerAddresses').find("[data-touchid='" + orderId + "']").find('#showCustomerDetails').click();

    initPopupTabs();

    $('#lblAddressValidate').html('');
}

function RedirectTOTouchPage() {
    openAddressesEditordialog();
    var latLngTouchId = $('#ConfirmLatLang').find('#latLngOrderNum').val();
    $('#divCustomerAddresses').find("[data-touchid='" + latLngTouchId + "']").find('#showCustomerDetails').click();

    initPopupTabs();

    $('#lblAddressValidate').html('');
    var confirmPop = $('#ConfirmLatLang');
    confirmPop.hide();
    //ShowConfirmCustomCallBackLatLang(1);
}

function AddTouch() {
    var confirmPop = $('#ConfirmLatLang');
    confirmPop.hide();
    ShowConfirmCustomCallBackLatLang(2);
}

function CancelAdding() {
    var confirmPop = $('#ConfirmLatLang');
    confirmPop.hide();
    ShowConfirmCustomCallBackLatLang(3);
}