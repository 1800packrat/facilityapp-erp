﻿//SFERP-TODO-CTRMV -- Remove this page completely TG-581

using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using PRFacAppWF.CodeHelpers;
using EncryptQueryString;

//using EncryptQueryString;
using PR.BusinessLogic;
using PR.Entities;
using System.Configuration;
using System.Data;
using System.Collections;

namespace PRFacAppWF
{
    public partial class calpopup : BasePage
    {
        private ScheduleCalendar sceduleCalendar;
        private SMDBusinessLogic smd;
        private DateTime scheduleDate;
        private int time;
        private LocalComponent localComponent;
        private bool shouldSendEmail = true;
        protected const String ViewBookedMiles = "View Booked Miles";
        protected const String ViewFacilityMiles = "View Facility Miles";
        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    smd = new SMDBusinessLogic(Session["UserId"] != null ? Convert.ToString(Session["UserId"]) : "");
            //    sp.AsyncPostBackTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["AjaxTimeout"]);
            //    btnsave.Enabled = false;

            //    //  LoadScheduleCalendar();
            //    if (!IsPostBack)
            //    {
            //        ViewState["UpdatePosItem"] = false;
            //    }


            //}
            //catch (Exception ex)
            //{
            //    Helper.LogError(ex);
            //    DisplayMessage(ex.Message);
            //}
        }

        /// <summary>
        /// Saves the selected time value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsave_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    string ans = Session["tempAdminUser"].ToString();
            //    DataTable dtscheduleTable = FetchScheduledDataTable();//Fetch specific dataTable AM/Pm/Anytime for the selected date
            //    ValidateAndScheduleTouches(dtscheduleTable);
            //}
            //catch (Exception ex)
            //{
            //    Helper.LogError(ex);
            //    DisplayMessage(ex.Message);
            //}
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Session["eFee"] = true;
            //    mpedialog.Hide();
            //    pnldialog.CssClass = "HidePanel";
            //    //ScheduleTouch();
            //    CheckForPremiumDeliveryFees();
            //    //ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript", Helper.BuildTooltips(), true);
            //}
            //catch (Exception ex)
            //{
            //    Helper.LogError(ex);
            //    DisplayMessage(ex.Message);
            //}
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Session["eFee"] = false;
            //    mpedialog.Hide();
            //    pnldialog.CssClass = "HidePanel";
            //    CheckForPremiumDeliveryFees();
            //    //ScheduleTouch();
            //    //ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript", Helper.BuildTooltips(), true);
            //}
            //catch (Exception ex)
            //{
            //    Helper.LogError(ex);
            //    DisplayMessage(ex.Message);
            //}
        }

        protected void btnApprYes_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    localComponent = new LocalComponent();
            //    mpeNdeTouch.Hide();
            //    pnlNdeTouch.CssClass = "HidePanel";

            //    // ScheduleTouch();
            //    //CheckFuelSurcharge();
            //    CheckForPremiumDeliveryFees();
            //    SendCallBlastMail();
            //    ActivityLogForNonAvailableDates();
            //    ClearTempAuthSessions();
            //    //ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript", Helper.BuildTooltips(), true);
            //}
            //catch (Exception ex)
            //{
            //    Helper.LogError(ex);
            //    DisplayMessage(ex.Message);
            //}
        }

        protected void btnApprNo_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    mpeNdeTouch.Hide();
            //    pnlNdeTouch.CssClass = "HidePanel";
            //    ClearTempAuthSessions();
            //}
            //catch (Exception ex)
            //{
            //    Helper.LogError(ex);
            //    DisplayMessage(ex.Message);
            //}
        }
        protected void BtnMsg_Click(object sender, EventArgs e)
        {
            //mpeError.Hide();
            //ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript", "window.close();", true);

        }

        protected void btnPremiumYes_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    shouldSendEmail = Convert.ToBoolean(((Button)sender).CommandArgument);
            //    mpePremiumFee.Hide();
            //    pnlPremiumFee.CssClass = "HidePanel";
            //    ViewState["UpdatePosItem"] = true;
            //    ScheduleTouch();
            //}
            //catch (Exception ex)
            //{

            //    DisplayMessage(ex.Message);
            //}
        }

        protected void btnPremiumNo_Click(object sender, EventArgs e)
        {
            //mpePremiumFee.Hide();
            //pnlPremiumFee.CssClass = "HidePanel";
            //ClearTempAuthSessions();
            //ViewState["UpdatePosItem"] = false;
            //ScheduleTouch();
        }

        protected void btnFsYes_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    PR.UtilityLibrary.PREnums.TouchType touchType = PR.UtilityLibrary.PREnums.TouchType.DeliverEmpty;
            //    switch (Session["touchType"].ToString())
            //    {
            //        case "DE-Deliver Empty":
            //            touchType = PR.UtilityLibrary.PREnums.TouchType.DeliverEmpty;
            //            break;
            //        case "DF-Deliver Full":
            //            touchType = PR.UtilityLibrary.PREnums.TouchType.DeliverFull;
            //            break;
            //        case "RF-Return Full":
            //            touchType = PR.UtilityLibrary.PREnums.TouchType.ReturnFull;
            //            break;
            //        case "RE-Return Empty":
            //            touchType = PR.UtilityLibrary.PREnums.TouchType.ReturnEmpty;
            //            break;
            //        case "CC-Curb To Curb":
            //            touchType = PR.UtilityLibrary.PREnums.TouchType.CurbToCurb;
            //            break;
            //    }


            //    smd.AddFuelSurcharge(touchType, Convert.ToDecimal(btnFsYes.CommandArgument), Convert.ToInt32(Session["unitId"]), Helper.GetSLAPIObject());
            //    Helper.SaveQoute(Session);
            //    SendMail();
            //}
            //catch (Exception ex)
            //{
            //    DisplayMessage(ex.Message);
            //}
        }

        protected void btnFsNo_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    string activity = string.Format("Fuel adjustment of amount ${0} is denied for the touch type {1}", btnFsYes.CommandArgument, Request.QueryString["type"].ToString());
            //    (new LocalComponent()).InsertActivityLog(Convert.ToInt32(Helper.GetSession("qorid")), "Fuel Adjustment denied", activity, Session["UserId"].ToString());
            //    Helper.SaveQoute(Session);
            //    SendMail();
            //}
            //catch (Exception ex)
            //{
            //    DisplayMessage(ex.Message);
            //}
        }

        #region private methods

        //private decimal CheckFuelSurcharge()
        //{
        //    int numOfFuelAdj = Convert.ToInt32(ConfigurationManager.AppSettings["numOfFuelAdjDays"]);
        //    LocalComponent lc = new LocalComponent();
        //    decimal distance = Convert.ToDecimal(Request.QueryString["dis"]);
        //    decimal surcharge = lc.GetFuelSurchargeValue(Helper.GetSession("locationCode").ToString(), Convert.ToInt32(Helper.GetSession("qorid")), distance);
        //    return surcharge;
        //}

        //private DataTable FetchScheduledDataTable()
        //{
        //    QTTimeValues objQTtimevalues = smd.GetQTTimeInfo(Helper.GetSLAPIObject());
        //    DataTable dtscheduleTable = null;
        //    switch (calSchedule.scheduleType)
        //    {
        //        case "AnyTime":
        //            time = objQTtimevalues.QTTimeIDAnyTime;
        //            dtscheduleTable = ((ScheduleCalendar)Session["dsAvailCalendar"]).AnyTime;
        //            break;
        //        case "AM":
        //            time = objQTtimevalues.QTTimeIDAM;
        //            dtscheduleTable = ((ScheduleCalendar)Session["dsAvailCalendar"]).AM;
        //            break;
        //        case "PM":
        //            time = objQTtimevalues.QTTimeIDPM;
        //            dtscheduleTable = ((ScheduleCalendar)Session["dsAvailCalendar"]).PM;
        //            break;
        //        default:
        //            break;
        //    }
        //    return dtscheduleTable;
        //}

        //private void LoadScheduleCalendar()
        //{
        //    PRCapacity tbl = new PRCapacity(Session["UserId"] != null ? Convert.ToString(Session["UserId"]) : "");
        //    DateTime date = Request.QueryString["date"] == null ? Convert.ToDateTime(DateTime.Today.ToShortDateString()) : Convert.ToDateTime(Request.QueryString["date"]);
        //    if (Request.QueryString != null)
        //    {
        //        if (Session["dsAvailCalendar"] == null)
        //        { 
        //            sceduleCalendar = tbl.GetCapacity(Helper.Corpcode,
        //                                                      Helper.GetSession("locationCode").ToString(),
        //                                                      Helper.username, Helper.password, date,
        //                                                      Convert.ToInt32(ConfigurationManager.AppSettings["numberOfDays"]),
        //                                                      Convert.ToDecimal(Request.QueryString["dis"]), null, null);
        //            Session["dsAvailCalendar"] = sceduleCalendar;
        //        }
        //        else
        //        {
        //            sceduleCalendar = (ScheduleCalendar)Session["dsAvailCalendar"];
        //        }
        //        calSchedule.AvailableDates = sceduleCalendar;
        //        calSchedule.DateColumnName = "Date";
        //        calSchedule.DefaultSelectedDate = DateTime.Now;
        //    }
        //}

        ////private void ScheduleTouches(DataTable dtscheduleTable)
        ////{
        ////    smd.ScheduleTouch(Convert.ToDateTime(Session["scheduleDate"]), Convert.ToInt32(Session["time"]), Convert.ToInt32(Session["time"]), Convert.ToInt32(Request.QueryString["eid"]),
        ////                             Helper.GetSLAPIObject(), Convert.ToBoolean(Session["eFee"]));
        ////    var isAvailable = from rows in dtscheduleTable.AsEnumerable() where rows.Field<DateTime>("Date") == Convert.ToDateTime(Session["scheduleDate"]) select rows.Field<Boolean>("IsAvailable");
        ////    bool isGreen = false;
        ////    foreach (bool isavail in isAvailable)
        ////    {
        ////        isGreen = isavail;
        ////    }
        ////    if (Request.QueryString["type"].ToString() != "DE-Deliver Empty" && isGreen == false)
        ////    {
        ////        LocalComponent localComponent = new LocalComponent();
        ////        string activity = string.Format("{0} touch is scheduled for an unavailable date {1}", Request.QueryString["type"].ToString(), Session["scheduleDate"].ToString());
        ////        localComponent.InsertActivityLog(Convert.ToInt32(Helper.GetSession("qorid")), "Schedule touch on unavailable date", activity, Session["UserId"].ToString());
        ////    }
        ////    Helper.SaveQoute(Session);
        ////    Session["RentalType"] = "Order";
        ////    SendMail();
        ////}

        //private void ValidateAndScheduleTouches(DataTable dtscheduleTable)
        //{
        //    Session["time"] = time;
        //    Session["scheduleDate"] = calSchedule.GetScheduleDate();
        //    Session["eFee"] = false;
        //    Session["isAvailable"] = false;
        //    string StoreStatus = string.Empty;
        //    var rowCollection = (from rows in dtscheduleTable.AsEnumerable()
        //                         where rows.Field<DateTime>("Date") == calSchedule.GetScheduleDate()
        //                         select new
        //                         { 
        //                             date = rows.Field<DateTime>("Date"), 
        //                             BookedMiles = rows.Field<decimal>("BookedMiles"), 
        //                             StoreStatus = rows.Field<String>("StoreStatus"),
        //                             TotalMiles = rows.Field<decimal>("FacilityMiles"),
        //                             TripMiles = rows.Field<decimal>("TripMiles"),
        //                             RegularMiles = rows.Field<decimal>("RegularMiles"),
        //                             ReservedMiles = rows.Field<decimal>("ReservedMiles")
        //                         });//filter the row to fetch the selected date
        //    foreach (var r in rowCollection)
        //    { 
        //        if (CheckIfRoleIsAllowed(string.Empty, ViewFacilityMiles))
        //        {
        //            trTotalMiles.Visible = true;
        //            lblTotalMiles.Text = Math.Round(r.TotalMiles, 2).ToString();
        //        }
        //        else
        //            trTotalMiles.Visible = false;

        //        if (CheckIfRoleIsAllowed(string.Empty, ViewBookedMiles))
        //        {
        //            lblBookedMiles.Text = Math.Round(r.BookedMiles, 2).ToString();
        //            trBookedMiles.Visible = true;
        //        }
        //        else
        //            trBookedMiles.Visible = false;


        //        lblRegularMiles.Text = Math.Round(r.RegularMiles, 2).ToString();
        //        lblReservedMiles.Text = Math.Round(r.ReservedMiles, 2).ToString();
        //        lblTripMiles.Text = Math.Round(r.TripMiles, 2).ToString(); 
        //        StoreStatus = r.StoreStatus;
        //    }

        //    if (StoreStatus == "C" && !(Session["RoleId"].ToString() == "Administrator" || Session["RoleId"].ToString() == "Supervisor" || Session["RoleId"].ToString() == "SuperAdmin" || Session["tempAdminUser"].ToString() == "True"))
        //    {
        //        throw new Exception("Touch cannot be scheduled on a closed date");
        //    } 
        //    else if (StoreStatus == "C" && Session["RoleId"].ToString() == "Administrator")
        //    { 
        //        CheckForPremiumDeliveryFees();
        //        ActivityLogForClosedDates();
        //        ClearTempAuthSessions();
        //    } 
        //    else
        //    { 
        //        CheckForPremiumDeliveryFees();
        //        LogActivityForUnavailableDate();
        //        ClearTempAuthSessions();
        //    }
        //}

        //private void ScheduleTouch()
        //{
        //    decimal totalMiles = Convert.ToDecimal(lblTotalMiles.Text);
        //    decimal bookedMiles = Convert.ToDecimal(lblBookedMiles.Text);
        //    decimal tripMiles = Convert.ToDecimal(lblTripMiles.Text);
        //    decimal regularMiles = Convert.ToDecimal(lblRegularMiles.Text);
        //    decimal reservedMiles = Convert.ToDecimal(lblReservedMiles.Text);

        //    SMDBusinessLogic tblogic = new SMDBusinessLogic(Session["UserId"] != null ? Convert.ToString(Session["UserId"]) : "");

        //    tblogic.ScheduleTouch(Convert.ToDateTime(Session["scheduleDate"]), Convert.ToInt32(Session["time"]), Convert.ToInt32(Session["time"]), Convert.ToInt32(Request.QueryString["eid"]),
        //        0,
        //        totalMiles,
        //        0,
        //        bookedMiles + tripMiles,
        //        txtApproverName.Text,
        //        Helper.GetSLAPIObject(),
        //        Convert.ToBoolean(Session["eFee"]),
        //        tripMiles,
        //        reservedMiles,
        //        regularMiles,
        //        0,
        //        GetTouchTypeIdByTouchName());
        //    UpdatePOSItemIfAvailable();


        //    UnHoldTouch();
        //    var custSession = (CustomerSession)Helper.GetSession("customer");
        //    bool custEmailExsist = !string.IsNullOrEmpty(custSession.email);
        //    if (custEmailExsist)
        //    {
        //        UpdateOnlineContractLink();
        //    }

        //    if (!Session["touchType"].ToString().Contains("LDM"))
        //    {
        //        bool calculateFuelSurcharge = false;

        //        PR.UtilityLibrary.PREnums.TouchType touchType = PR.UtilityLibrary.PREnums.TouchType.DeliverEmpty;
        //        switch (Session["touchType"].ToString())
        //        {
        //            case "DE-Deliver Empty":
        //                touchType = PR.UtilityLibrary.PREnums.TouchType.DeliverEmpty;
        //                break;
        //            case "DF-Deliver Full":
        //                touchType = PR.UtilityLibrary.PREnums.TouchType.DeliverFull;
        //                break;
        //            case "RF-Return Full":
        //                touchType = PR.UtilityLibrary.PREnums.TouchType.ReturnFull;
        //                break;
        //            case "RE-Return Empty":
        //                touchType = PR.UtilityLibrary.PREnums.TouchType.ReturnEmpty;
        //                break;
        //            case "CC-Curb To Curb":
        //                touchType = PR.UtilityLibrary.PREnums.TouchType.CurbToCurb;
        //                break;
        //        }

        //        //Do not calculate Fuel Surcharge for DE touch
        //        if (touchType != PR.UtilityLibrary.PREnums.TouchType.DeliverEmpty)
        //        {
        //            calculateFuelSurcharge = !smd.IsFuelAdjusmentAlreadyAdded(touchType, Convert.ToInt32(Helper.GetSession("unitId")), Helper.GetSLAPIObject());
        //        }

        //        if (calculateFuelSurcharge)
        //        {
        //            decimal fuelSurcharge = CheckFuelSurcharge();
        //            if (fuelSurcharge > 0)
        //            {
        //                mpeFuelSurchage.Show();
        //                pnlFuelSurcharge.CssClass = "UnHidePanel";
        //                lblFuelSurcharge.Text = string.Format("A fuel adjustment of ${0:0.00} has been added to the touch.", fuelSurcharge);
        //                btnFsYes.CommandArgument = fuelSurcharge.ToString();

        //            }
        //            else
        //            {

        //                Helper.SaveQoute(Session);
        //                SendMail();
        //            }
        //        }
        //        else
        //        {
        //            Helper.SaveQoute(Session);
        //            SendMail();
        //        }
        //    }
        //    else
        //    {
        //        Helper.SaveQoute(Session);
        //    }
        //} 

        //private void UpdatePOSItemIfAvailable()
        //{
        //    if (Convert.ToBoolean(ViewState["UpdatePosItem"]) == true)
        //    {
        //        smd.AddUpdatePOSItem(Convert.ToInt32(Helper.GetSession("unitId")), Convert.ToInt32(ViewState["chargeId"]), 1, Convert.ToDecimal(ViewState["Price"]), Helper.GetSLAPIObject());
        //    }
        //}

        //private void CheckForPremiumDeliveryFees()
        //{
        //    POSItems posItems = smd.GetPOSItmes(Helper.GetSLAPIObject());
        //    DateTime scheduledDate = Convert.ToDateTime(Session["scheduleDate"]);

        //    if (scheduledDate.DayOfWeek == DayOfWeek.Sunday)
        //    {
        //        DataRow[] rows = posItems.Tables[0].Select("ItemDescription='Premium Delivery - Sunday'");
        //        if (rows != null && rows.Length >= 1 && Convert.ToDecimal(rows[0]["Price"]) > 0)
        //        {
        //            SavePosValues(Convert.ToDecimal(rows[0]["Price"]), rows[0]["ChargeDescriptionId"].ToString());
        //            mpePremiumFee.Show();
        //            pnlPremiumFee.CssClass = "UnHidePanel";
        //        }
        //        else
        //        {
        //            ScheduleTouch();
        //        }
        //    }
        //    else if (scheduledDate.Date == DateTime.Today)
        //    {
        //        DataRow[] rows = posItems.Tables[0].Select("ItemDescription='Premium Delivery - Same Day'");
        //        if (rows != null && rows.Length >= 1 && Convert.ToDecimal(rows[0]["Price"]) > 0)
        //        {
        //            SavePosValues(Convert.ToDecimal(rows[0]["Price"]), rows[0]["ChargeDescriptionId"].ToString());
        //            mpePremiumFee.Show();
        //            pnlPremiumFee.CssClass = "UnHidePanel";
        //        }
        //        else
        //        {
        //            ScheduleTouch();
        //        }
        //    }
        //    else if (scheduledDate.Date == DateTime.Today.AddDays(1))
        //    {
        //        DataRow[] rows = posItems.Tables[0].Select("ItemDescription='Premium Delivery - Next Day'");
        //        if (rows != null && rows.Length >= 1 && Convert.ToDecimal(rows[0]["Price"]) > 0)
        //        {
        //            SavePosValues(Convert.ToDecimal(rows[0]["Price"]), rows[0]["ChargeDescriptionId"].ToString());
        //            mpePremiumFee.Show();
        //            pnlPremiumFee.CssClass = "UnHidePanel";
        //        }
        //        else
        //        {
        //            ScheduleTouch();
        //        }
        //    }
        //    else
        //    {
        //        ScheduleTouch();
        //    }
        //}

        //private void SavePosValues(decimal price, string chargeDescriptionId)
        //{
        //    ViewState["Price"] = price;
        //    ViewState["chargeId"] = chargeDescriptionId;
        //    lblPremiumFeeMsg.Text = "Scheduling this touch for the selected date will add an extra " + price.ToString("c") + " premium delivery fee to the QOR. do you want to apply this charge?";
        //}

        //private void LogActivityForUnavailableDate()
        //{
        //    localComponent = new LocalComponent();
        //    if (Request.QueryString["type"].ToString() != "DE-Deliver Empty" && Convert.ToBoolean(Session["isAvailable"]) == false)
        //    {
        //        string activity = string.Format("{0} Touch is scheduled for an unavailable date {1}", Request.QueryString["type"].ToString(), Session["scheduleDate"].ToString());
        //        localComponent.InsertActivityLog(Convert.ToInt32(Helper.GetSession("qorid")), "Schedule touch on unavailable date", activity, Session["UserId"].ToString());
        //    }
        //}

        //private void ActivityLogForNonAvailableDates()
        //{
        //    string activity = string.Empty;
        //    if (Session["tempAdminUser"].ToString() == "True")
        //    {
        //        activity = string.Format("{0} Touch is scheduled for an unavailable date {1}. Facility approval by: {2}, Call Center authorization by: {3}", Request.QueryString["type"].ToString(), Convert.ToDateTime(Session["scheduleDate"]).ToShortDateString(), txtApproverName.Text, Session["tempAuthorizer"].ToString());
        //    }
        //    else
        //    {
        //        activity = string.Format("{0} Touch is scheduled for an unavailable date {1}. Facility Approval by: {2}", Request.QueryString["type"].ToString(), Convert.ToDateTime(Session["scheduleDate"]).ToShortDateString(), txtApproverName.Text);
        //    }
        //    localComponent.InsertActivityLog(Convert.ToInt32(Helper.GetSession("qorid")), "Schedule touch on unavailable date", activity, Helper.GetSession("userId").ToString(),
        //        Convert.ToDecimal(lblTotalMiles.Text.Trim()),
        //        Convert.ToDecimal(lblBookedMiles.Text.Trim()), Convert.ToDateTime(Session["scheduleDate"]), Session["locationCode"].ToString(), txtApproverName.Text.Trim(), Convert.ToDecimal(lblTripMiles.Text.Trim()), Convert.ToDecimal(lblReservedMiles.Text.Trim()), Convert.ToDecimal(lblRegularMiles.Text.Trim()), 0, GetTouchTypeIdByTouchName());
        //}

        //private void ActivityLogForClosedDates()
        //{
        //    localComponent = new LocalComponent();
        //    string activity = string.Format("{0} Touch is scheduled for a closed date {1}", Request.QueryString["type"].ToString(), Session["scheduleDate"].ToString());

        //    localComponent.InsertActivityLog(Convert.ToInt32(Helper.GetSession("qorid")), "Schedule touch on closed date", activity, Helper.GetSession("userId").ToString(),
        //        Convert.ToDecimal(lblTotalMiles.Text.Trim()),
        //        Convert.ToDecimal(lblBookedMiles.Text.Trim()), Convert.ToDateTime(Session["scheduleDate"]), Session["locationCode"].ToString(), txtApproverName.Text.Trim()
        //        , Convert.ToDecimal(lblTripMiles.Text.Trim()), Convert.ToDecimal(lblReservedMiles.Text.Trim()), Convert.ToDecimal(lblRegularMiles.Text.Trim()), 0, GetTouchTypeIdByTouchName());
        //}

        //private void SendMail()
        //{
        //    var custSession = (CustomerSession)Helper.GetSession("customer");
        //    bool custEmailExsist = !string.IsNullOrEmpty(custSession.email);

        //    string savedurl = string.Empty;
        //    PricingInfo pricingInfo = smd.GetPricingInfoData(Helper.GetSLAPIObject());
        //    LocalComponent lc = new LocalComponent();

        //    if (Request.QueryString["type"] == "DE-Deliver Empty" && Convert.ToString(Session["RentalType"]) == "Order")
        //    {
        //        lc.UpdatePRQuoteOrderLog(Convert.ToInt32(Helper.GetSession("qorid")),
        //                                                    Session["UserId"].ToString(), string.Empty,
        //                                                    (pricingInfo.DueAtDelivery != 0M
        //                                                         ? Convert.ToDecimal(pricingInfo.DueAtDelivery)
        //                                                         : decimal.MinValue),
        //                                                    (pricingInfo.FutureTransportationCharge != 0M
        //                                                         ? Convert.ToDecimal(
        //                                                             pricingInfo.FutureTransportationCharge)
        //                                                         : decimal.MinValue),
        //                                                    (pricingInfo.RecurringMonthlyCharge != 0M
        //                                                         ? Convert.ToDecimal(
        //                                                             pricingInfo.RecurringMonthlyCharge)
        //                                                         : decimal.MinValue));
        //        Session["RentalType"] = "Order";
        //    }

        //    if (custEmailExsist && shouldSendEmail)
        //    {
        //        Touches touches = smd.GetAppliedTouches(Helper.GetSLAPIObject());
        //        var touchesData = (from t in touches.Touch.AsEnumerable() where t.Field<string>("Service") == "DE-Deliver Empty" select new { date = t.Field<DateTime>("DeliveryDate") });
        //        DateTime rowDate = DateTime.MinValue;
        //        foreach (var row in touchesData)
        //        {
        //            rowDate = row.date;
        //        }
        //        Hashtable ht = GenerateEmailDataTemplate(pricingInfo, lc);

        //        if (rowDate == DateTime.MinValue && Request.QueryString["type"] != "DE-Deliver Empty")
        //            Session["SendOrderMail"] = false;
        //        else
        //            if (rowDate != DateTime.MinValue && Request.QueryString["type"] == "DE-Deliver Empty")
        //        {
        //            try
        //            {
        //                //UpdateOnlineContractLink(ht);
        //                //Helper.SaveQoute(Session);
        //                MailManager.SendOrderMail(ht, custSession.email);
        //            }
        //            catch (Exception ex)
        //            {
        //                DisplayMessage("Email could not be sent, " + ex.Message);
        //                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript",
        //                //                                    "alert('Email could not be sent, " + ex.Message +
        //                //                                    "');window.close();", true);
        //            }
        //        }
        //        else
        //                if (rowDate != DateTime.MinValue && Request.QueryString["type"] != "DE-Deliver Empty")
        //        {
        //            try
        //            {
        //                MailManager.SendTransUpdateEmail(ht, custSession.email);
        //            }
        //            catch (Exception ex)
        //            {
        //                DisplayMessage("Email could not be sent, " + ex.Message);
        //                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript",
        //                //                                    "alert('Email could not be sent, " + ex.Message +
        //                //                                    "');window.close();", true);
        //            }
        //        }
        //        DisplayMessage("Scheduled date is successfully updated");
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript",
        //        //                                                 "alert('Scheduled date is successfully updated');window.close();", true);
        //    }
        //    else if (!custEmailExsist)
        //    {
        //        DisplayMessage("Scheduled date is successfully updated but an email is not sent. Please update customer email address in the billing information screen.");
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript",
        //        //                                    "alert('Scheduled date is successfully updated but an email is not sent. Please update customer email address in the billing information screen.');window.close();",
        //        //                                    true);
        //    }
        //    else
        //    {
        //        DisplayMessage("Scheduled date is successfully updated");
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript",
        //        //                                    "alert('Scheduled date is successfully updated');window.close();",
        //        //                                    true);
        //    }
        //}

        //private void UpdateOnlineContractLink(Hashtable ht)
        //{
        //    EncryptedQueryString.CurrentPath = Server.MapPath("/");
        //    EncryptedQueryString QueryString = new EncryptedQueryString();
        //    QueryString.Add("qorid", Session["qorid"].ToString());
        //    string savedUrl = ConfigurationManager.AppSettings["OnlineContractUrl"] + QueryString.ToString();
        //    smd.UpdateOCURL(savedUrl, Helper.GetSLAPIObject());
        //    string emailUrl = ConfigurationManager.AppSettings["OnlineContractUrlForMail"] + QueryString.ToString();
        //    ht.Add("ContractUrl", emailUrl);
        //}

        //private void UpdateOnlineContractLink()
        //{
        //    string response = smd.GetOCURL(Helper.GetSLAPIObject());
        //    if (string.IsNullOrEmpty(response) && Session["RentalType"].ToString() == "Order")
        //    {
        //        EncryptedQueryString.CurrentPath = Server.MapPath("/");
        //        EncryptedQueryString QueryString = new EncryptedQueryString();
        //        QueryString.Add("qorid", Session["qorid"].ToString());
        //        string savedUrl = ConfigurationManager.AppSettings["OnlineContractUrl"] + QueryString.ToString();
        //        smd.UpdateOCURL(savedUrl, Helper.GetSLAPIObject());
        //    }
        //    //string emailUrl = ConfigurationManager.AppSettings["OnlineContractUrlForMail"] + QueryString.ToString();
        //    // ht.Add("ContractUrl", emailUrl);
        //}

        //private void SendCallBlastMail()
        //{
        //    if (Session["LoggedInUserEmail"] != null && Session["SendEmailtoCallBlast"] != null && Convert.ToBoolean(Session["SendEmailtoCallBlast"]) == true && Session["CallBlastToEmail"] != null)
        //    {
        //        string messageBody = string.Format("{0} has been scheduled for {1} for the QORID  {2}<br/> Approver:-{3}", Request.QueryString["type"].ToString(), calSchedule.GetScheduleDate().ToShortDateString(), Session["qorid"].ToString(), txtApproverName.Text);
        //        string subject = string.Format("{0} has been scheduled for {1} for the QORID {2}", Request.QueryString["type"].ToString(), calSchedule.GetScheduleDate().ToString(), Session["qorid"].ToString()); ;
        //        MailManager.SendCallBlastEmail(Session["LoggedInUserEmail"].ToString(), Session["CallBlastToEmail"].ToString(), subject, messageBody);
        //    }
        //}

        //private Hashtable GenerateEmailDataTemplate(PricingInfo pricingInfo, LocalComponent lc)
        //{
        //    var ht = new Hashtable();
        //    scheduleDate = (DateTime)Session["scheduleDate"];
        //    var custSession = (CustomerSession)Helper.GetSession("customer");

        //    decimal recuringDiscountMthly = pricingInfo.RecurringMonthlyChargeDiscountedPeriod > 0 ? pricingInfo.RecurringMonthlyChargeDiscountedPeriod : 0;
        //    ht.Add("RecMthlyDiscount", recuringDiscountMthly);
        //    ht.Add("QORID", Helper.GetSession("rentalId"));
        //    ht.Add("QORID_Global", Helper.GetSession("qorid"));
        //    ht.Add("FirstName", custSession.firstName.Replace("&", "&amp;"));
        //    ht.Add("LastName", custSession.lastName.Replace("&", "&amp;"));
        //    ht.Add("DeliveryAddress1", custSession.addr1.Replace("&", "&amp;"));
        //    ht.Add("BillingCity", custSession.city.Replace("&", "&amp;"));
        //    ht.Add("BillingState", custSession.state.Replace("&", "&amp;"));
        //    ht.Add("BillingZip", custSession.zip);
        //    ht.Add("CustomerPhoneNumber", custSession.phone);
        //    ht.Add("DeliveryDate", scheduleDate.ToShortDateString());
        //    ht.Add("CustomerEmail", custSession.email);
        //    ht.Add("CompanyName", custSession.companyName.Replace("&", "&amp;"));
        //    //  ht.Add("BillingFacility",);
        //    ht.Add("MobilePhone", custSession.mobilePhone);
        //    //  ht.Add("ScheDate", scheduledDate);
        //    ht.Add("EmailAddress", custSession.email);
        //    ht.Add("DueAtDelivery", Helper.GetCurrencyFormat(pricingInfo.DueAtDelivery));
        //    ht.Add("FutureTransportationCharge",
        //           Helper.GetCurrencyFormat(pricingInfo.FutureTransportationCharge));
        //    ht.Add("RecurringPrice", Helper.GetCurrencyFormat(pricingInfo.RecurringMonthlyCharge));
        //    ht.Add("Facility1ID", Helper.GetSession("locationCode").ToString());
        //    ht.Add("CUSTNMBR", Helper.GetSession("TennantId").ToString());
        //    ht.Add("StorageSku", Helper.GetSession("unitId").ToString());
        //    ht.Add("GUID", Helper.GetSession("GUID").ToString());
        //    ht.Add("BillingCycle", Session["BillingCycle"].ToString());
        //    ht.Add("FuelSurchage", lc.IsFuelsurChargeApplicable(Helper.GetSession("locationCode").ToString()));
        //    ht.Add("ExpirationDate", smd.GetQuoteExpirationDate(Convert.ToInt32(Helper.GetSession("qorid")), Convert.ToInt32(ConfigurationManager.AppSettings["QuoteExpirationDate"])).ToShortDateString());
        //    ht.Add("Type", Request.QueryString["type"].ToString());
        //    return ht;
        //}

        //private void UnHoldTouch()
        //{
        //    if (Request.QueryString["status"] == "Unhold")
        //    {
        //        smd.UpdateTouchStatus(SLNet.Common.eQTStatus.Open, Convert.ToInt32(Request.QueryString["eid"]),
        //                                   Helper.GetSLAPIObject());
        //        Helper.BuildTooltips();
        //    }
        //}

        //private void ClearTempAuthSessions()
        //{
        //    Session["tempAdminUser"] = "False";
        //    Session["tempAuthorizer"] = string.Empty;
        //}


        //private void DisplayMessage(string message)
        //{
        //    lblglobalMsg.Text = message;
        //    pnlMsg.CssClass = "UnHidePanel";
        //    mpeError.Show();
        //}

        //int GetTouchTypeIdByTouchName()
        //{
        //    string touchType = Session["touchType"].ToString();
        //    PRCapacity oTransiteBusinessLogic = new PRCapacity();
        //    var touchTypes = oTransiteBusinessLogic.GetAllCapTouchTypes().Tables[0].Rows.OfType<DataRow>().Select(c => new
        //    {
        //        CAPTouchTypeId = Convert.ToInt32(c["CAPTouchTypeId"]),
        //        Description = Convert.ToString(c["Description"]),
        //        TouchTypeCode = Convert.ToString(c["TouchTypeCode"])
        //    }).ToList();


        //    if (!string.IsNullOrEmpty(touchType))
        //    {
        //        return touchTypes.FirstOrDefault(c => touchType.ToLower().Contains(string.Format("{0}-", c.TouchTypeCode.ToLower()))).CAPTouchTypeId;
        //    }
        //    else
        //    {
        //        return touchTypes.FirstOrDefault(c => "DE-".ToLower().Contains(string.Format("{0}-", c.TouchTypeCode.ToLower()))).CAPTouchTypeId;
        //    }
        //}
        //bool CheckIfRoleIsAllowed(string color, string action)
        //{
        //    DataSet dsColorActionRole;
        //    if (Session["ColorActionRole"] != null)
        //        dsColorActionRole = Session["ColorActionRole"] as DataSet;
        //    else
        //    {
        //        int roleLevel = Convert.ToInt16(ConfigurationManager.AppSettings["CalendarAccessRoleLevel"]);
        //        dsColorActionRole = (new PRCapacity()).GetCapacityColorActionsByRole(roleLevel);
        //        Session["ColorActionRole"] = dsColorActionRole;
        //    }
        //    return dsColorActionRole.Tables[0].Rows.OfType<DataRow>().Any(c => c["Color"].ToString().ToLower().Trim() == color.ToLower().Trim()
        //        && c["Action"].ToString().ToLower().Trim() == action.ToLower().Trim());
        //}

        #endregion private methods
    }
}
