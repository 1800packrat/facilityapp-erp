﻿<%@ Page Title="Unauthorize Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Unauthorized.aspx.cs" Inherits="PRFacAppWF.Unauthorized" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCenter" runat="server">
    <div class="unauthorized-page">
        <div class="head" style="padding-top: 50px; text-align: center;">
            <h1>
                You are not authorized to see this page. <br />Please contact Admin.
            </h1>
        </div>
        <div style="padding-top: 50px; text-align: center; font-size: 30px;">
            Click <a href="DashBoard.aspx" style="font-size: 30px">Here</a> to go back to dashboard
        </div>
    </div>
</asp:Content>