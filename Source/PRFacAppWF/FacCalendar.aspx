﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FacCalendar.aspx.cs" Inherits="PRFacAppWF.FacCalendar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%= Scripts.Render("~/bundles/FacCalendar") %>
    <script type="text/x-kendo-template" id="hoverTemplate">        
            <div id='hoverpanel' class='pop-cal-hoverpanel'>
                <span style='font-weight:bold'>Date : #: date # </span>
                <span id='span-facility-miles' >Facility Miles : #: facility # </span>
                <span id='span-booked-miles' >Booked Miles : #: booked # </span>
                <span id='span-sales-regular-miles'>Sales Available Miles : #: salesregular # </span>      
                <span id='span-service-regular-miles'>Service Available Miles : #: serviceregular # </span>      
                <span id='span-regular-miles'>Available Miles : #: regular # </span>      
                <span id='span-reserved-miles'>Reserved Miles : #: reserved # </span>
                <span>Total Miles : #: trip # </span>
                <span style='padding-left:7px'><b>&bull;</b> Round Trip Miles : #: roundtrip # </span>
                <span style='padding-left:7px'><b>&bull;</b> Touch Miles : #: touch # </span>                
            </div>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCenter" runat="server">

    <div class="form-panel">
        <h2>
            <span class="back-arrow"><a href="Dashboard.aspx">
                <img src="Images/back.png" width="48" height="48" style="float: left; margin-right: 10px;" /></a></span>
            <img src="Images/Driver.png" width="48" height="48" style="float: left; margin-right: 10px;" />
            <div class="h-title">
                <span style="float: left; font-size: 9pt; text-transform: uppercase;">Calendar</span><br />
                <span style="float: left;"></span>
            </div>
            <div class="clearfix">
            </div>

        </h2>
        <div class="clearfix">
        </div>
        <input type="hidden" id="scheduledate" runat="server" />
        <input type="hidden" id="hdscheduletype" runat="server" />
        <input type="hidden" id="hdIsUserEntered" runat="server" />
        <input type="hidden" id="hdIsAvaialable" runat="server" />
        <input type="hidden" id="hdAllowSupLogin" runat="server" value="false" />
        <%--<input type="hidden" id="ColDate" runat="server" />--%>
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <ul class="form-list2" style="text-align: center; height: 500px">
            <li>
                <asp:Label runat="server" ID="lblSchedule" Font-Bold="true" Visible="false" Font-Names="Arial"
                    Font-Size="10pt"></asp:Label>
                <div class="input-control text lft-all">
                    <asp:DropDownList runat="server" ID="ddlYear">
                    </asp:DropDownList>
                </div>
                <div class="input-control text lft-all">
                    <asp:DropDownList runat="server" ID="ddlMonth">
                    </asp:DropDownList>
                </div>
                <asp:Button runat="server" ID="btnView" Text="Show" OnClick="btnView_Click" />
            </li>
            <li>
                <div class="clear">
                </div>

                <div class="clear">
                </div>
                <div style="width: 100%;">
                    <div style="float: left; margin: 0 2% 0 2%; width: 30%;">
                        <h5 style="text-align: center;">anytime</h5>
                        <asp:Calendar ID="calScheduleAnyTime" EnableViewState="true"
                            ShowNextPrevMonth="false" Font-Size="Smaller"
                            ShowTitle="false" runat="server" NextPrevFormat="FullMonth"
                            OnDayRender="calScheduleAnyTime_DayRender1"
                            OnSelectionChanged="calScheduleAnyTime_SelectionChanged"></asp:Calendar>
                    </div>
                    <div style="float: left; margin: 0 2% 0 0; width: 30%;">
                        <h5 style="text-align: center;">am</h5>
                        <asp:Calendar ID="calScheduleAM" ShowNextPrevMonth="false" ShowTitle="false" runat="server" Font-Size="Smaller"
                            NextPrevFormat="FullMonth" OnDayRender="calScheduleAnyTime_DayRender1" OnSelectionChanged="calScheduleAnyTime_SelectionChanged"></asp:Calendar>
                    </div>
                    <div style="float: left; margin: 0 2% 0 0; width: 30%;">
                        <h5 style="text-align: center;">pm</h5>
                        <asp:Calendar ID="calSchedulePM" ShowNextPrevMonth="false" ShowTitle="false" runat="server" Font-Size="Smaller"
                            NextPrevFormat="FullMonth" OnDayRender="calScheduleAnyTime_DayRender1" OnSelectionChanged="calScheduleAnyTime_SelectionChanged"></asp:Calendar>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</asp:Content>
