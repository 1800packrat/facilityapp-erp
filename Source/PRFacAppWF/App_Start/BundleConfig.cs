﻿namespace PRFacAppWF.App_Start
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css/styles").Include(
                "~/Content/css/site.css",
                "~/Content/css/modern.css",
                //"~/Content/css/jquery.ui.all.css",
                "~/Content/css/modern-responsive.css",
                //"~/Content/css/jquery-ui.css",
                //"~/Content/css/ui.jqgrid.css"
                "~/Content/css/jquery.ui.theme.css"
                //,
                //"~/Content/css/print-grid.css"
                ));


            bundles.Add(new StyleBundle("~/Content/cssprint/styles").Include(
                "~/Content/css/modern.css",
                "~/Content/css/site.css"
                ));

            bundles.Add(new StyleBundle("~/Content/css/jqueryui").Include(
                 "~/Content/css/jquery/ui.jqgrid.css",
                "~/Content/css/jquery/ui.jqgrid-bootstrap-ui.css",
                // "~/Content/css/jquery/jquery-ui.theme.min.css",
                "~/Content/css/jquery/ui.jqgrid-bootstrap.css",
                "~/Content/css/jquery/ui.multiselect.css",
                "~/Content/css/jquery/jquery.steps.css",
                 "~/Content/css/jquery/jquery-ui.min.css",
                "~/Content/css/jquery/jquery.timepicker.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/Libraries/jquery-3.1.1.min.js",
                "~/Scripts/Libraries/jqGrid/i18n/grid.locale-en.js",
                "~/Scripts/Libraries/jqGrid/jquery.jqGrid.min.js",
                "~/Scripts/Libraries/jquery.blockUI.js",
                "~/Scripts/Libraries/jquery.steps.js",
                "~/Scripts/Libraries/jQuery.tmpl.js",
                "~/Scripts/Libraries/jquery-ui.min.js",
                "~/Scripts/Libraries/jquery.timepicker.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/script").Include(
                "~/Scripts/AjaxGlobalHandler.js",
                "~/Scripts/tableHeadFixer.js",
                //"~/Scripts/jquery.blockUI.js",
                "~/Scripts/app/Site.js"
                ));

            //Below bundle uses only for TouchList page
            bundles.Add(new ScriptBundle("~/bundles/watouchlist").Include(
                //"~/Scripts/app/waFilterForm.js",
                "~/Scripts/app/waTouchGrid.js",
                "~/Scripts/Libraries/jspdf.min.js",
                "~/Scripts/Libraries/html2canvas.js"
                ));

            //Below bundle uses only for staging areas page
            bundles.Add(new ScriptBundle("~/bundles/stagingareas").Include(
                //"~/Scripts/app/waFilterForm.js",
                "~/Scripts/app/waTouchContainerStatus.js"
                ));

            //Below bundle uses only for TruckStatus/TruckOpHours page
            bundles.Add(new ScriptBundle("~/bundles/truckophours").Include(
                "~/Scripts/TruckOpHours.js",
                "~/Scripts/TruckOpHoursGrid.js"
                ));

            //Below bundle uses only for facility operating hours page
            bundles.Add(new ScriptBundle("~/bundles/facilityophours").Include(
                "~/Scripts/FacilityOperatingHour.js"
                ));

            //Below bundle uses only for facility operating hours page
            bundles.Add(new ScriptBundle("~/bundles/drivermanagement").Include(
                "~/Scripts/App/DriverMaintenance.js",
                "~/Scripts/App/DriverMaintenanceForm.js",
                "~/Scripts/App/DriverSchedule.js"
                ));


            // SFERP-TODO-CTRMV -- remove this bundle. TG-562
            //Below bundle uses only for unit maintenance page
            bundles.Add(new ScriptBundle("~/bundles/unitmaintenanceindex").Include(
                "~/Scripts/UnitMaintenance_Index.js"
                ));

            // SFERP-TODO-CTRMV -- remove this bundle. TG-562
            //Below bundle uses only for unit maintenance page
            bundles.Add(new ScriptBundle("~/bundles/unitmaintenancefilter").Include(
                "~/Scripts/UnitMaintenance_Filter.js"
                ));

            // SFERP-TODO-CTRMV -- remove this bundle. TG-576
            //Below bundle uses only for unit maintenance page
            bundles.Add(new ScriptBundle("~/bundles/unitmaintenanceReport").Include(
                "~/Scripts/UnitMaintenanceReport.js",
                "~/Scripts/grid.locale-en.js"
                ));

            // SFERP-TODO-CTRMV -- remove this bundle. TG-563
            //Below bundle uses only for unit maintenance page
            bundles.Add(new ScriptBundle("~/bundles/weeklyDieselPrice").Include(
                "~/Scripts/App/WeeklyDieselPrice.js"
                ));

            //Below bundle uses only for Change password page
            bundles.Add(new ScriptBundle("~/bundles/changePassword").Include(
                "~/Scripts/App/changePassword.js"
                ));

            //Below bundle uses only for Forgot password page
            bundles.Add(new ScriptBundle("~/bundles/forgotPassword").Include(
                "~/Scripts/App/forgotPassword.js"
                ));

            //Below bundle uses only for login page
            bundles.Add(new ScriptBundle("~/bundles/login").Include(
                "~/Scripts/App/login.js",
                "~/Scripts/App/site.js"
                ));

            // SFERP-TODO-CTRMV -- remove this bundle. TG-577
            //Below bundle uses only for Fleet Status Report page
            bundles.Add(new ScriptBundle("~/bundles/FleetStatusReport").Include(
                "~/Scripts/FleetStatusReport.js"
                ));

            // SFERP-TODO-CTRMV -- remove this bundle. TG-578 
            //Below bundle uses only for Dispatch Dashboard Report page
            bundles.Add(new ScriptBundle("~/bundles/DispatchDashboard").Include(
                "~/Scripts/DispatchDashboard.js"
                ));

            //Below bundle uses only for Unit unavailability page in corporate controls
            bundles.Add(new ScriptBundle("~/bundles/UnitAvailability").Include(
                "~/Scripts/UnitAvailability.js"
                ));

            //Below bundle uses only for application settings page in corporate controls
            bundles.Add(new ScriptBundle("~/bundles/ApplicationSettings").Include(
                "~/Scripts/ApplicationSettings.js"
                ));

            //Below bundle uses only for application settings page in corporate controls
            bundles.Add(new ScriptBundle("~/bundles/AverageMPH").Include(
                "~/Scripts/AverageMPH.js"
                ));

            //Below bundle uses only for Truck Transfer Reason page in corporate controls
            bundles.Add(new ScriptBundle("~/bundles/TransferReason").Include(
                "~/Scripts/TransferReason.js"
                ));

            //Below bundle uses only for Touch Time page in corporate controls
            bundles.Add(new ScriptBundle("~/bundles/TouchTime").Include(
                "~/Scripts/TouchTime.js"
                ));

            //Below bundle uses only for Site Link page in corporate controls
            bundles.Add(new ScriptBundle("~/bundles/SLMileage").Include(
                "~/Scripts/SLMileage.js"
                ));

            //Below bundle uses only for Truck down Time page in corporate controls
            bundles.Add(new ScriptBundle("~/bundles/TruckDownTime").Include(
                "~/Scripts/TruckDownTime.js"
                ));

            //Below bundle uses only for User management page in admin controls
            bundles.Add(new ScriptBundle("~/bundles/UserAssignment").Include(
                "~/Scripts/UserAssignment.js"
                ));

            //Below bundle uses only for User management page in admin controls
            bundles.Add(new ScriptBundle("~/bundles/UserAssignmentAddUpdate").Include(
                "~/Scripts/UserAssignment_AddUpdate.js"
                ));

            //Below bundle uses only for Role management page in admin controls
            bundles.Add(new ScriptBundle("~/bundles/RoleManagement").Include(
                "~/Scripts/RoleManagement.js"
                ));

            //Below bundle uses only for Role management page in admin controls
            bundles.Add(new ScriptBundle("~/bundles/RoleManagementAddUpdate").Include(
                "~/Scripts/RoleManagement_Addupdate.js"
                ));

            //Below bundle uses only for Local Logistics, User management and Role management pages in admin controls
            bundles.Add(new ScriptBundle("~/bundles/footable").Include(
               "~/Scripts/FooTable/footable.js",
               "~/Scripts/FooTable/footable.sort.js",
               "~/Scripts/FooTable/footable.paginate.js"
               ));

            // Below bundle uses only for Local Logistics, User management and Role management pages in admin controls 
            bundles.Add(new StyleBundle("~/Content/css/footable").Include(
               "~/Content/css/footable.core.css",
               "~/Content/css/footable.standalone.css"
               ));

            // below bundle uses in sitemaster
            bundles.Add(new ScriptBundle("~/bundles/SiteMaster").Include(
                "~/Scripts/Libraries/jquery-3.1.1.min.js",
                "~/Scripts/MetroUI/accordion.js",
                "~/Scripts/MetroUI/buttonset.js",
                "~/Scripts/MetroUI/dropdown.js",
                "~/Scripts/MetroUI/input-control.js",
                "~/Scripts/MetroUI/pagecontrol.js",
                "~/Scripts/MetroUI/tile-drag.js",
                "~/Scripts/MetroUI/tile-slider.js",
                "~/Scripts/MetroUI/carousel.js",
                "~/Scripts/jquery.blockUI.js"
                ));

            //below code is for Site.master
            bundles.Add(new StyleBundle("~/Content/css/SiteMaster").Include(
                "~/Content/css/modern-responsive.css",
                "~/Content/css/modern.css",
                "~/Content/css/site.css"
                ));

            // below bundle uses in FacCalendar pagge
            bundles.Add(new ScriptBundle("~/bundles/FacCalendar").Include(
                "~/Scripts/kendo.all.min.js",
                "~/Scripts/FacCalendar.js"
                ));

            // below bundle uses in InnerMaster
            bundles.Add(new ScriptBundle("~/bundles/InnerMaster").Include(
                "~/Scripts/Libraries/jquery-3.1.1.min.js",
                "~/Scripts/MetroUI/accordion.js",
                "~/Scripts/MetroUI/dropdown.js",
                "~/Scripts/MetroUI/input-control.js",
                "~/Scripts/MetroUI/pagecontrol.js",
                "~/Scripts/MetroUI/tile-drag.js",
                "~/Scripts/jquery.blockUI.js"
                ));

            // SFERP-TODO-CTRMV -- remove this bundle. TG-570
            // below bundle uses in InnerMaster
            bundles.Add(new ScriptBundle("~/bundles/AdvanceSearch").Include(
                "~/Scripts/AdvanceSearch.js"
                ));

            // below bundle uses in InnerMaster
            bundles.Add(new ScriptBundle("~/bundles/Touches").Include(
                "~/Scripts/Touches.js"
                ));

            // SFERP-TODO-CTRMV -- remove this bundle. TG-584
            // below bundle is for pricing.js
            bundles.Add(new ScriptBundle("~/bundles/Pricing").Include(
                "~/Scripts/pricing.js"
                ));

            // SFERP-TODO-CTRMV -- remove this bundle. TG-568
            // below bundle is for quickquote.js
            bundles.Add(new ScriptBundle("~/bundles/QuickQuote").Include(
                "~/Scripts/jquery.ddslick.min.js",
                "~/Scripts/quickquote.js"
                ));

            // below bundle is for Local logistics layout page
            bundles.Add(new ScriptBundle("~/bundles/LocalDispatch").Include(
                "~/Scripts/LocalLogistics.js",
                "~/Scripts/local.nav-left.js",
                "~/Scripts/local.stopoptimization.map.js",
                "~/Scripts/local.touchoptimization.map.js",
                "~/Scripts/local.googlemap.js",
                "~/Scripts/local.EditAddresses.js"
                ));

            // below bundle is for Local logistics layout page
            bundles.Add(new ScriptBundle("~/bundles/jqueryPluginsLD").Include(
                "~/Scripts/date.js",
                "~/Scripts/jquery.contextMenu.js",
                "~/Scripts/LightDialog.js",
                "~/Scripts/oms.min.js",
                "~/Scripts/markerclusterer.js",
                "~/Scripts/jquery.toolbar.js",
                "~/Scripts/MediaLoot/hideshow.js",
                "~/Scripts/MediaLoot/jquery.equalHeight.js"
                ));

            // below bundle is for Local logistics layout page
            bundles.Add(new StyleBundle("~/Content/css/LocalDispatch").Include(
                 "~/Content/css/jquery.toolbars.css",
                "~/Content/css/bootstrap.icons.css",
                "~/Content/css/slide.css",
                "~/Content/css/MediaLoot/layout.css"
                ));


            // below code is for calpopup.aspx inline scripts
            bundles.Add(new ScriptBundle("~/bundles/CalPopup").Include(
                "~/Scripts/Libraries/jquery-3.1.1.min.js",
                "~/Scripts/calpopup.js"
                ));

            // below code is for calpopup.aspx referred css
            bundles.Add(new StyleBundle("~/Content/css/CalPopup").Include(
                "~/Content/css/jquery.metro.css",
                "~/Content/css/site.css",
                "~/Content/css/modern.css"
                ));

            // SFERP-TODO-CTRMV -- remove this bundle. TG-583
            // below code is for billinginfo.aspx inline scripts
            bundles.Add(new ScriptBundle("~/bundles/BillingInfo").Include(
                "~/Scripts/billinginfo.js"
                ));

            //Below bundle uses only for Truck Status Index page in admin controls
            bundles.Add(new ScriptBundle("~/bundles/TruckStatusIndex").Include(
                "~/Scripts/TruckStatusIndex.js"
                ));

            //Below bundle uses only for Truck Status List page in admin controls
            bundles.Add(new ScriptBundle("~/bundles/TruckStatusList").Include(
                "~/Scripts/TruckStatusList.js"
                ));
        }
    }
}