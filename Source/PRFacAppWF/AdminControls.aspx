﻿<%@ Page Title="Admin Controls" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="AdminControls.aspx.cs" Inherits="PRFacAppWF.AdminControls" %>

<asp:Content ID="AdminControlsBody" ContentPlaceHolderID="cphCenter" runat="server">
    <div class="margin-left-1prct">
    <h1>Admin Controls</h1>
    <asp:Panel ID="pnlDashBoard" runat="server" Style="width: 100%">
        <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.UserAssignment, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
           { %>
        <div class="tile  bg-color-red">
            <a href="UserAssignment/Index" id="UserAssignment">
                <div class="tile-content">
                    <h3 style="margin-bottom: 15px;">User Management</h3>
                </div>
            </a>
        </div>
        <%} %>
        <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.RoleManagement, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
           { %>
        <div class="tile  bg-color-green">
            <a href="RoleManagement/Index" id="RoleManagement">
                <div class="tile-content">
                    <h3 style="margin-bottom: 15px;">Role Management</h3>
                </div>
            </a>
        </div>
        <%} %>
        <div class="tile  bg-color-orange">
            <a href="DashBoard.aspx">
                <div class="tile-content">
                    <h3 style="margin-bottom: 15px;">Return to DashBoard</h3>
                </div>
                <div class="brand bg-color-orange">
                    <div class="badge">
                        <i class="icon-arrow-left-3 fg-color-white" style="font-size: 29pt; margin-top: 5px;"></i>
                    </div>
                </div>
            </a>
        </div>
    </asp:Panel>
        </div>
</asp:Content>
