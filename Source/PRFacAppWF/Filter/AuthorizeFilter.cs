﻿using PRFacAppWF.models;
using PRFacAppWF.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRFacAppWF.Filter
{
    public class AuthorizeFilter
    {
        public AuthorizeFilter()
        {

        }

        public UserInfo ValidateLogin(string userName,  string password)
        {
            IPRSService oPRSService = new PRSService();

            return oPRSService.ValidateLogin(userName, password);
        }

        //public User GetUserInfo(int UserId)
        //{
        //    return null;
        //}

        public bool IsInRole(string roleId, string pageName)
        {
            return false;
        }
    }
}