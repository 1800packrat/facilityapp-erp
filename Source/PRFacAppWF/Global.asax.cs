﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using PR.BusinessLogic;
using PRFacAppWF.CodeHelpers;
using System.Web.Routing;
using System.Web.Mvc;
using SimpleInjector;
using PR.LocalLogisticsSolution.Interfaces;
using PR.LocalLogisticsSolution.Model;
using PR.LocalLogisticsSolution.Service.Implementation;
using System.Web.Compilation;
using System.Reflection;
using SimpleInjector.Advanced;
using System.ComponentModel.Composition;
using SimpleInjector.Integration.Web.Mvc;
using PRFacAppWF.App_Start;
using System.Web.Optimization;

//[assembly: PreApplicationStartMethod(
//    typeof(PRFacAppWF.PageInitializerModule),
//    "Initialize")]


namespace PRFacAppWF
{
    //public sealed class PageInitializerModule : IHttpModule
    //{
    //    public static void Initialize()
    //    {
    //        DynamicModuleUtility.RegisterModule(typeof(PageInitializerModule));
    //    }

    //    void IHttpModule.Init(HttpApplication context)
    //    {
    //        context.PreRequestHandlerExecute += (sender, e) =>
    //        {
    //            if (context.Context.CurrentHandler != null)
    //            {
    //                Global.InitializeHandler(context.Context.CurrentHandler);
    //            }
    //        };
    //    }

    //    void IHttpModule.Dispose() { }
    //}

    public class Global : HttpApplication
    {
        private static Container container;

        public static void InitializeHandler(IHttpHandler handler)
        {
            container.GetRegistration(handler.GetType(), true).Registration
                .InitializeInstance(handler);
        }



        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.IgnoreRoute("{resource}.aspx/{*pathinfo}");
            routes.MapRoute(
                name: "Default" // Route name
                , url: "{controller}/{action}/{id}"//); // URL with parameters
                //"Default.aspx"
                , defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }
        protected void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = true;

            if (Convert.ToString(ConfigurationManager.AppSettings["CacheOnDemand"]) == "0")
            {
                var objGlobal = new Global_asax();
         
            }

            ConfigureDependencies();
          
        }

        private void ConfigureDependencies()
        {
            var container = new Container(); 
            container.Options.PropertySelectionBehavior =
                new ImportAttributePropertySelectionBehavior();
            container.Register<IUserService, UserService>();
            container.Register<ITouchService, TouchService>();
            container.Register<IStagingTouchService, StagingTouchService>(); 
            container.Register<IDBRepository>(() => new JaguarRepository(container.GetInstance<IRouteRepository>())); 
            container.Register<ISitelinkRepository, SiteLinkrepository>();
            container.Register<IRouteRepository, RouteRepository>();
            container.Register<IStarsDbRepository, StarsRepository>(); 

            // 3. Store the container for use by Page classes.
            Global.container = container;
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }


        private static void RegisterWebPages(Container container)
        {
            var pageTypes =
                from assembly in BuildManager.GetReferencedAssemblies().Cast<Assembly>()
                where !assembly.IsDynamic
                where !assembly.GlobalAssemblyCache
                from type in assembly.GetExportedTypes()
                where type.IsSubclassOf(typeof(Page))
                where !type.IsAbstract && !type.IsGenericType
                select type;

            pageTypes.ToList().ForEach(container.Register);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (Convert.ToString(ConfigurationManager.AppSettings["CacheOnDemand"]) == "0")
            {
                var objGlobal = new Global_asax();
        
            }
        }



        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_EndRequest()
        {
            var context = new HttpContextWrapper(this.Context);

            // If we're an ajax request and forms authentication caused a 302, 
            // then we actually need to do a 401
            if (FormsAuthentication.IsEnabled && context.Response.StatusCode == 302 && context.Request.IsAjaxRequest())
            {
                context.Response.Clear();
                context.Response.StatusCode = 401;
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Helper.LogError(exc);
            HttpContext.Current.Response.Redirect("~/errorpage.aspx", false);
        }

        protected void Session_End(object sender, EventArgs e)
        {
            try
            { 
                Session.Abandon();
            }
            catch
            {
            }
            finally
            {
                //  Response.Redirect("~/Catslogin.aspx",false);
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {
           
        }
 

        public class ImportAttributePropertySelectionBehavior : IPropertySelectionBehavior
        {
            public bool SelectProperty(Type serviceType, PropertyInfo propertyInfo)
            {
                // Makes use of the System.ComponentModel.Composition assembly
                return typeof(Page).IsAssignableFrom(serviceType) &&
                propertyInfo.GetCustomAttributes(typeof(ImportAttribute), true).Any();
            }
        }


        public class InternalConstructorResolutionBehavior : IConstructorResolutionBehavior
        {
            private IConstructorResolutionBehavior original;

            public InternalConstructorResolutionBehavior(Container container)
            {
                this.original = container.Options.ConstructorResolutionBehavior;
            }

            public ConstructorInfo GetConstructor(Type serviceType, Type implementationType)
            {
                if (!implementationType.GetConstructors().Any())
                {
                    var internalCtors = implementationType.GetConstructors(
                        BindingFlags.Instance | BindingFlags.NonPublic)
                        .Where(c => !c.IsPrivate)
                        .ToArray();

                    if (internalCtors.Length == 1) return internalCtors.First();
                }

                return this.original.GetConstructor(serviceType, implementationType);
            }
        }
    }
}