﻿
<%--SFERP-TODO-CTRMV -- Remove this page completely. TG-581--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="calpopup.aspx.cs" Inherits="PRFacAppWF.calpopup" %>

<%@ Register Src="~/usercontrols/calendar.ascx" TagPrefix="ucl" TagName="Calendar" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Schedule Calendar</title>
   <%-- <link href="Content/css/jquery.metro.css?ts="+ <%= PRFacAppWF.CodeHelpers.Helper.VersionNumber %> rel="stylesheet" type="text/css" />
    <link href="Content/css/site.css?ts="+ <%= PRFacAppWF.CodeHelpers.Helper.VersionNumber %> rel="stylesheet" type="text/css" />
    <link href="Content/css/modern.css?ts="+ <%= PRFacAppWF.CodeHelpers.Helper.VersionNumber %> rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.7.1.js" type="text/javascript"></script>--%>
   <%= Scripts.Render("~/bundles/CalPopup") %>
    <%= Styles.Render("~/Content/css/CalPopup") %>
    <style type="text/css">
        .HidePanel
        {
            display: none;
        }
        .UnHidePanel
        {
            display: block;
            position:absolute; 
            width:300px; 
        }
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=60);
            opacity: 0.60;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sp">
    </asp:ScriptManager>
    <div>
        <asp:Panel runat="server" Style="display: block; text-align: center; background-color: White;
            font-size: 90%;" ID="pnScheduledCalendar">
            <asp:UpdatePanel runat="server" ID="up">
                <ContentTemplate>
                    <ucl:Calendar runat="server" ID="calSchedule" />
                     <%--Legend--%>
                        <div style="float: left; width: 100%">
                            <div class="cal-legend-outer">
                                <span>Legend :- </span>
                                <div class="cal-legend">
                                    <div class="cal-legend-row">
                                        <div class="cal-legend-col-1">Facility Closed</div>
                                        <div class="cal-legend-col-2 cellblue">&nbsp;</div>
                                    </div>
                                    <div class="cal-legend-row">
                                        <div class="cal-legend-col-1">Available miles</div>
                                        <div class="cal-legend-col-2 cellgreen">&nbsp;</div>
                                    </div>
                                    <div class="cal-legend-row">
                                        <div class="cal-legend-col-1">Reserve miles</div>
                                        <div class="cal-legend-col-2 cellred">&nbsp;</div>
                                    </div>
                                    <div class="cal-legend-row">
                                        <div class="cal-legend-col-1" style="width: 90px !important">Overbooked</div>
                                        <div class="cal-legend-col-2 cellblack">&nbsp;</div>
                                    </div>
                                    <div class="cal-legend-row">
                                        <div class="cal-legend-col-1">Available miles (No Inventory)</div>
                                        <div class="cal-legend-col-2 cellorange">&nbsp;</div>
                                    </div>
                                    <div class="cal-legend-row">
                                        <div class="cal-legend-col-1">Reserve miles (No Inventory)</div>
                                        <div class="cal-legend-col-2 cellpurple">&nbsp;</div>
                                    </div>
                                    <div class="cal-legend-row">
                                        <div class="cal-legend-col-1">Block Dates</div>
                                        <div class="cal-legend-col-2 cellwhite">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <asp:Button runat="server" ID="btnsave" Text="Save" OnClick="btnsave_Click" />
                    <ajaxToolkit:ModalPopupExtender ID="mpedialog" TargetControlID="hdfd" PopupControlID="pnldialog"
                        DropShadow="true" runat="server">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:HiddenField runat="server" ID="hdfd" />
                    <asp:Panel runat="server" ID="pnldialog" BackColor="White" BorderWidth="1px" BorderColor="Gray"
                        CssClass="HidePanel">
                        <div class="message-dialog bg-color-oliveDark" style="width: 300px; color: #fff;">
                            <div class="txtHead02">
                                Expedite Fee
                            </div>
                            <div class="tableHolderPopUp" style="width: 280px">
                                <table class="grid" width="280px">
                                    <tr>
                                        <td>
                                            <b>Expedite fees may apply.<br></br>Do you want to add the expedite fee?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button runat="server" Text="Yes" ID="btnYes" OnClick="btnYes_Click" />
                                            <asp:Button runat="server" Text="No" ID="btnNo" OnClick="btnNo_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="mpeNdeTouch" TargetControlID="hdfd2" PopupControlID="pnlNdeTouch"
                        DropShadow="true" runat="server">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:HiddenField runat="server" ID="hdfd2" />
                    <asp:Panel runat="server" ID="pnlNdeTouch" BackColor="White" BorderWidth="1px" BorderColor="Gray"
                        CssClass="HidePanel">
                        <div class="message-dialog bg-color-oliveDark" style="width: 300px; color: #fff;">
                            <h3>
                                Customer Details</h3>
                            <table class="grid" style="text-align: left;" width="280px">
                                <tr>
                                    <td style="width: 180px;">
                                        <b>Facility approver's name
                                            <br>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtApproverName" Width="100px" runat="server" ValidationGroup="redValid"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvapprname" runat="server" ControlToValidate="txtApproverName"
                                            ValidationGroup="redValid" Text="name required" ErrorMessage="name required"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                                          <%-- <tr>
                                        <td style="width: 180px;">
                                            Touch Limit
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblTouchLimit" Text="0"></asp:Label>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td style="width: 180px;">
                                            Mileage Limit
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblMileageLimit" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 180px;">
                                            Touches Scheduled
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblTouchesScheduled" Text="0" Font-Bold="true"></asp:Label>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td style="width: 180px;">
                                            Mileages Booked
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblMileagesBooked" Text="0" Font-Bold="true"></asp:Label>
                                        </td>
                                    </tr>--%>

                                    <tr id="trTotalMiles" runat="server">
                                        <td style="width: 180px;">
                                            Total Miles :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblTotalMiles" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trBookedMiles" runat="server">
                                        <td style="width: 180px;">
                                            Booked Miles :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblBookedMiles" Text="0"></asp:Label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 180px;">
                                            Trip Miles :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblTripMiles" Text="0"></asp:Label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 180px;">
                                            Available Miles:
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblRegularMiles" Text="0"></asp:Label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 180px;">
                                            Reserved Miles :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblReservedMiles" Text="0"></asp:Label>
                                        </td>
                                    </tr>

                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button runat="server" Text="OK" ID="btnApprYes" ValidationGroup="redValid" OnClick="btnApprYes_Click" />
                                        <asp:Button runat="server" Text="Cancel" ID="btnApprNo" OnClick="btnApprNo_Click" />
                                    </td>
                                </tr>
                            </table>
                            <div class="clear">
                            </div>
                        </div>
                    </asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="mpePremiumFee" TargetControlID="hfPremium" PopupControlID="pnlPremiumFee"
                        DropShadow="true" runat="server">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:HiddenField runat="server" ID="hfPremium" />
                    <asp:Panel runat="server" ID="pnlPremiumFee" CssClass="HidePanel">
                        <div class="message-dialog bg-color-oliveDark" style="width: 300px; color: #fff;">
                            <div class="txtHead02">
                                Premium Delivery Fee
                            </div>
                            <div class="tableHolderPopUp" style="width: 280px">
                                <table class="grid" width="280px">
                                    <tr>
                                        <td>
                                            <b>
                                                <asp:Label runat="server" ID="lblPremiumFeeMsg"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                             <asp:Button runat="server" Text="Yes,Send Mail" ID="btnPremiumYes" OnClick="btnPremiumYes_Click" CommandArgument="true" />
                                            <asp:Button runat="server" Text="Yes,No Mail" ID="btnPremYesNoMail" OnClick="btnPremiumYes_Click" CommandArgument="false" />
                                             <asp:Button runat="server" Text="No" ID="btnPremiumNo" OnClick="btnPremiumNo_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="mpeFuelSurchage" TargetControlID="hfFulSurcarge"
                        PopupControlID="pnlFuelSurcharge" DropShadow="true" runat="server">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:HiddenField runat="server" ID="hfFulSurcarge" />
                    <asp:Panel runat="server" ID="pnlFuelSurcharge" CssClass="HidePanel">
                        <div class="message-dialog bg-color-oliveDark" style="width: 300px; color: #fff;">
                            <div class="txtHead02">
                                Fuel Adjustment
                            </div>
                            <table class="grid" width="280px">
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label runat="server" ID="lblFuelSurcharge"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button runat="server" Text="Yes" ID="btnFsYes" OnClick="btnFsYes_Click" />
                                        <asp:Button runat="server" Text="No" ID="btnFsNo" OnClick="btnFsNo_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField runat="server" ID="hderror" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeError" BackgroundCssClass="modalBackground"
                        TargetControlID="hderror" PopupControlID="pnlMsg">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel runat="server" ID="pnlMsg" CssClass="HidePanel">
                        <div class="message-dialog bg-color-oliveDark" style="color: #fff;">
                            <div>
                                Message
                            </div>
                            <div class="button-panel">
                                <asp:Label runat="server" ID="lblglobalMsg"></asp:Label><br />
                                <asp:Button runat="server" ID="BtnMsg" Text="Ok" OnClick="BtnMsg_Click" OnClientClick="return messageOk();" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:UpdateProgress ID="uprog" runat="server" DynamicLayout="true">
            <ProgressTemplate>
                <div class="progress">
                    <img src="Images/preloader-w8-line-black.gif" id="progress2" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    </form>
</body>
</html>
