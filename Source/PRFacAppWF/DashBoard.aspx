﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="PRFacAppWF.DashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCenter" runat="server">
    <div class="margin-left-1prct">
        <h1>DashBoard</h1>
        <asp:Panel ID="pnlDashBoard" runat="server" Style="width: 100%">


            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.FacilityOperatingHour, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile  bg-color-red">
                <a onclick="$.blockUI();" href="FacilityOperatingHour" id="FacilityOperatingHour">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Facility Operational Hours</h3>
                    </div>
                </a>
            </div>
            <%} %>

            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.TruckStatus, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile  bg-color-green">
                <a onclick="$.blockUI();" href="TruckStatus/TruckOpHours" id="TruckOpHours">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Truck Operational Hours</h3>
                    </div>
                </a>
            </div>
            <%} %>
            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.DriverMaintenance, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile  bg-color-orangeDark"> 
                <a onclick="$.blockUI();" href="DriverMaintenance/Index">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Driver Management</h3>
                    </div>
                </a>
            </div>
            <%}%>
            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.FacCalendar, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile  bg-color-blue">
                <!-- SFERP-TODO-CTUPD -- Need to uncomment below line - TBD.-->
                <a onclick="$.blockUI();" href="FacCalendar.aspx"> 
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">View Calendar</h3>
                    </div>
                </a>
            </div>
            <%} %>


            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.TouchList, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile  bg-color-orange">
                <a onclick="$.blockUI();" href="TouchList/Index" id="hrTouches">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Touch Dashboard</h3>
                    </div>
                </a>
            </div>
            <%} %>
            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.WarehouseTouchContainerStatus, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile  bg-color-green">
                <a onclick="$.blockUI();" href="WarehouseTouchContainerStatus/Index" id="ContainerStatus">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Stage Areas</h3>
                    </div>
                </a>
            </div>
            <%} %>

            <div class="tile  bg-color-red">
                <a onclick="$.blockUI();" href="LocalLogistics/Index" id="A1">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Local Dispatch</h3>
                    </div>
                </a>
            </div>

            <div class="tile  bg-color-orange" id="dvChgFac" runat="server" style="display: none">
                <asp:LinkButton runat="server" ID="lbtnChgFac" OnClick="lbtnChgFac_Click"> 
                <div class="tile-content">
                    <h3 style="margin-bottom: 15px;">Change Facility</h3>
                </div>
                <div class="brand bg-color-orange">
                    <div class="badge">
                        <i class="icon-arrow-left-3 fg-color-white" style="font-size: 29pt; margin-top: 5px;"></i>
                    </div>
                </div>
                </asp:LinkButton>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hidPRFacility" runat="server" />
        <asp:Panel ID="pnlFacility" runat="server" Width="100%">
            <h2>
                <span class="back-arrow bg-color-blue" style="margin-bottom: 10px; color: #fff;">
                    <a href="DashBoard.aspx">
                        <img src="Images/back.png" width="48" height="48" style="float: left; margin-right: 10px;" />
                    </a>Choose your facility
                </span>
            </h2>

            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.CorporateControls, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <h2 class="corporate-reports-head">
                <span class="back-arrow bg-color-orange" style="margin-bottom: 10px;">
                    <a href="CorporateControls.aspx" style="color: #fff;">
                        <img src="Images/corporate-reports-icon.png" width="48" height="48" style="float: left; margin-right: 10px;" />
                        Corporate Controls&nbsp;&nbsp;
                    </a>
                </span>
            </h2>
            <% } %>
            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.AdminControls, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <h2 class="corporate-reports-head">
                <span class="back-arrow bg-color-red" style="margin-bottom: 10px;">
                    <a href="AdminControls.aspx" style="color: #fff;">
                        <img src="Images/corporate-reports-icon.png" width="48" height="48" style="float: left; margin-right: 10px;" />
                        Admin Controls&nbsp;&nbsp;
                    </a>
                </span>
            </h2>
            <% } %>
            <div class="clearfix"></div>
            <ul class="listview fluid">
                <asp:Repeater runat="server" ID="rptFac" OnItemCommand="rptFac_ItemCommand">
                    <ItemTemplate>
                        <li class="bg-color-yellow">
                            <asp:LinkButton runat="server" ID="ibtn" ToolTip='<%#Eval("SiteName") %>' CommandArgument='<%#Eval("LocationCode") %>' CommandName="ed">             
                           <div class="tile-content" style="height: 60px;"> <h3 style="margin-bottom: 15px;"><%#Eval("SiteName") %>
                           </h3>
                            </div>
                            </asp:LinkButton>
                        </li>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <li class="bg-color-orangeDark">
                            <asp:LinkButton runat="server" ID="ibtn" ToolTip='<%#Eval("SiteName") %>' CommandArgument='<%#Eval("LocationCode") %>' CommandName="ed"> 
                           <div class="tile-content" style="height: 60px;"> 
                               <h3 style="margin-bottom: 15px;"><%#Eval("SiteName") %>
                               </h3>
                           </div>
                            </asp:LinkButton>
                        </li>
                    </AlternatingItemTemplate>
                </asp:Repeater>
            </ul>
        </asp:Panel>
    </div>
</asp:Content>
