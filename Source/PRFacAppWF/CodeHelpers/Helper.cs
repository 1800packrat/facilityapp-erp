﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Linq;
using PR.BusinessLogic;
using PR.Entities;
using PR.LocalLogisticsSolution.Model;

namespace PRFacAppWF.CodeHelpers
{
    public class Helper
    {
        public static string EnDeKey { get { return ConfigurationManager.AppSettings["EnDeKey"]; } }

        public static string DriverDefaultRoleGroupName { get { return ConfigurationManager.AppSettings["DriverDefaultRoleGroupName"]; } }
          
        /// <summary>
        /// Logs the error
        /// </summary>
        /// <param name="ex"></param>
        public static void LogError(Exception ex)
        {
            if (ex != null && ex.GetType().Name != "SqlException")
            {
                try
                {
                    string userid = HttpContext.Current.Session == null ? string.Empty : HttpContext.Current.Session["userId"].ToString();
                    int qorid = HttpContext.Current.Session == null ? 0 : Convert.ToInt32(HttpContext.Current.Session["qorid"]);

                    var objLocalComp = new LocalComponent();
                    objLocalComp.InsertPRErrorLog(DateTime.Now, userid, ex.GetType().ToString(), ex.Message, ex.StackTrace, qorid, HttpContext.Current.Request.Url.AbsoluteUri, string.Empty);
                }
                catch
                {
                    //HttpContext.Current.Response.Redirect("~/errorpage.aspx",true);
                }
            }
        }

        /// <summary>
        /// Converts a string to currency format
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string GetCurrencyFormat(object obj)
        {
            return Convert.ToDecimal(obj).ToString("c");
        }

        /// <summary>
        /// Clear all session objects
        /// </summary>
        /// <param name="session"></param>
        public static void ClearSessionObjects(HttpSessionState session)
        {
            session["qorid"] = null;
            session["slapi"] = null;
            session["TennantId"] = null;
            session["rentalId"] = null;
            session["unitId"] = null;
            session["RentalType"] = null;
            session["unitName"] = null;
            session["customer"] = null;
            session["SiteId"] = null;
            session["promotionsObject"] = null;
            session["touches"] = null;
            session["sdrecitems"] = null;
            session["rentalId"] = null;
            session["dspositems"] = null;
            session["tennantBalance"] = null;
            session["UsdRate"] = null;
            session["CurrentUnitId"] = null;
            session["UnitIDs"] = null;
            session["HeaderTouchInfo"] = null;
            session["AppliedTouches"] = null;
            session["MoveType"] = null;
            session["SendEmailtoCallBlast"] = null;
            session["CallBlastToEmail"] = null;
            session["touchDistance"] = null;
            session["dsAvailCalendar"] = null;
            session["IsQuoteExpired"] = null;
        }

        /// <summary>
        /// Checks whether the current session object is null or not & throws null reference exception on null
        /// </summary>
        /// <param name="sessionName"></param>
        /// <returns></returns>
        public static object GetSession(string sessionName)
        {
            if (HttpContext.Current.Session[sessionName] != null)
            {
                return HttpContext.Current.Session[sessionName];
            }
            else
            {
                if (HttpContext.Current.Session == null)
                {
                    Logout();
                }
                throw new Exception();
            }
        }

        /// <summary>
        /// Logs out of the application
        /// </summary>
        public static void Logout()
        { 
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Response.Redirect("~/CATSLogin.aspx", false);
        } 

        public static void HandleException(Exception ex)
        {
            try
            {
                var objSMD =
                    new SMDBusinessLogic(HttpContext.Current.Session["UserId"] != null
                                             ? Convert.ToString(HttpContext.Current.Session["UserId"])
                                             : "");
                var h = (Hashtable)HttpContext.Current.Session["HashValue"];
                int Qorid = -1;
                if (h != null)
                {
                    if (h.ContainsKey("QORID_Global"))
                    {
                        Qorid = Convert.ToInt32(h["QORID_Global"]);
                    }
                }
                var objLocalComp = new LocalComponent();
                objLocalComp.InsertPRErrorLog(DateTime.Now, Convert.ToString(HttpContext.Current.Session["UserId"]),
                                              ex.GetType().ToString(), ex.Message, ex.StackTrace, Qorid);

                #region Log error to file

                string strLogFilePath = ConfigurationManager.AppSettings["LogFilePath"];
                // Added by guru
                using (
                    StreamWriter objWriter =
                        File.AppendText(strLogFilePath + "AppLog_" + DateTime.Now.ToString("MM-dd-yyyy") + ".txt"))
                {
                    Exception objException = ex; 
                    objWriter.WriteLine(objException.Message
                                        + "||" + objException.StackTrace
                                        + "||" + objException.InnerException
                                        + "||" + DateTime.Now);
                }
                ;

                #endregion Log error to file
            }
            catch
            {
            }
        }       

        public static SortedDictionary<string, string> GetUSStates()
        {
            var hlUsList = new SortedDictionary<string, string>();

            hlUsList.Add("AL", "Alabama");

            hlUsList.Add("AK", "Alaska");

            hlUsList.Add("AZ", "Arizona");

            hlUsList.Add("AR", "Arkansas");

            hlUsList.Add("CA", "California");

            hlUsList.Add("CO", "Colorado");

            hlUsList.Add("CT", "Connecticut");

            hlUsList.Add("DE", "Delaware");

            hlUsList.Add("DC", "District of Columbia");

            hlUsList.Add("FL", "Florida");

            hlUsList.Add("GA", "Georgia");

            hlUsList.Add("HI", "Hawaii");

            hlUsList.Add("ID", "Idaho");

            hlUsList.Add("IL", "Illinois");

            hlUsList.Add("IN", "Indiana");

            hlUsList.Add("IA", "Iowa");

            hlUsList.Add("KS", "Kansas");

            hlUsList.Add("KY", "Kentucky");

            hlUsList.Add("LA", "Louisiana");

            hlUsList.Add("ME", "Maine");

            hlUsList.Add("MD", "Maryland");

            hlUsList.Add("MA", "Massachusetts");

            hlUsList.Add("MI", "Michigan");

            hlUsList.Add("MN", "Minnesota");

            hlUsList.Add("MS", "Mississippi");

            hlUsList.Add("MO", "Missouri ");

            hlUsList.Add("MT", "Montana");

            hlUsList.Add("NE", "Nebraska");

            hlUsList.Add("NV", "Nevada");

            hlUsList.Add("NH", "New Hampshire");

            hlUsList.Add("NJ", "New Jersey");

            hlUsList.Add("NM", "New Mexico");

            hlUsList.Add("NY", "New York");

            hlUsList.Add("NC", "North Carolina");

            hlUsList.Add("ND", "North Dakota");

            hlUsList.Add("OH", "Ohio");

            hlUsList.Add("OK", "Oklahoma");

            hlUsList.Add("OR", "Oregon");

            hlUsList.Add("PA", "Pennsylvania");

            hlUsList.Add("RI", "Rhode Island");

            hlUsList.Add("SC", "South Carolina");

            hlUsList.Add("SD", "South Dakota");

            hlUsList.Add("TN", "Tennessee");

            hlUsList.Add("TX", "Texas");

            hlUsList.Add("UT", "Utah");

            hlUsList.Add("VT", "Vermont");

            hlUsList.Add("VA", "Virginia");

            hlUsList.Add("WA", "Washington");

            hlUsList.Add("WV", "West Virginia");

            hlUsList.Add("WI", "Wisconsin");

            hlUsList.Add("WY", "Wyoming");

            return hlUsList;
        }

    public static string GetConfigSettings(string Key)
        {
            return ConfigurationManager.AppSettings[Key];
        }

        //public static Int32 GetUnitID(string UnitType, string LocationCode)
        //{
        //    var objSMD =
        //        new SMDBusinessLogic(HttpContext.Current.Session["UserId"] != null
        //                                 ? Convert.ToString(HttpContext.Current.Session["UserId"])
        //                                 : "");
          
        //    if (UnitType == "R12")
        //    {
        //        return (new LocalComponent()).GetUnitIdByUnitName(LocationCode, GetConfigSettings("12FootUName"));
        //    }
        //    else if (UnitType == "R16")
        //    {
        //        return (new LocalComponent()).GetUnitIdByUnitName(LocationCode, GetConfigSettings("16FootUName"));
        //    }
        //    else
        //    {
        //        return -1;
        //    }
        //}

       
        //public static void DisposeWebSLAPIObject()
        //{
        //    try
        //    {
        //        if (HttpContext.Current != null)
        //        {
        //            if (HttpContext.Current.Session["objSLAPI"] != null)
        //            { 
        //                HttpContext.Current.Session["objSLAPI"] = null;
        //            }
        //        }
        //        ManangeSLAPIObjects.RemoveSLAPIFromDics();
        //    }
        //    catch (Exception ex)
        //    {
        //        Helper.LogError(ex);
        //    }
        //}


        public static string ToAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }

        public static SqlParameter SqlParameterObj(string paramName, object val)
        {
            return new SqlParameter { ParameterName = paramName, Value = val ?? DBNull.Value };
        }

        public static SiteInfo CurrentSiteInfo
        {
            get
            {  
                if (CurrentUser != null && HttpContext.Current.Session["FaclocationCode"] != null)
                { 
                    string FaclocationCode = Convert.ToString(HttpContext.Current.Session["FaclocationCode"]);

                    SiteInfo currentSiteInfo = CurrentUser.Facilities.Where(x => x.LocationCode == FaclocationCode).First();
                    currentSiteInfo.SiteAddress.AddressType = PR.UtilityLibrary.PREnums.AddressType.Customer;

                    return currentSiteInfo;
                }
                else
                    return null;
            }
        }

        /// <summary>
        /// This property returns logged in user details. This session must be assigned after login
        /// </summary>
        public static LogisticsUser CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session[typeof(LogisticsUser).Name] != null)
                    return (LogisticsUser)HttpContext.Current.Session[typeof(LogisticsUser).Name];

                return null;
            }
        }

        public static string UserLoginWelcomeInfo
        {
            get
            {
                if (HttpContext.Current != null && CurrentUser != null)
                {
                    string facilityname = Convert.ToString(HttpContext.Current.Session["FacilityName"]);
                    return "Welcome " + CurrentUser.FullName;// + " - " + CurrentUser.GroupName + (string.IsNullOrEmpty(facilityname) ? string.Empty : " (" + facilityname + ")");
                }
                else
                    return string.Empty;
            }
        }

        public static string UserLoginWelcomeInfoWithFacility
        {
            get
            {
                if (HttpContext.Current != null && CurrentUser != null)
                {
                    string facilityname = Convert.ToString(HttpContext.Current.Session["FacilityName"]);
                    return "Welcome " + CurrentUser.FullName + " - " + CurrentUser.GroupName + (string.IsNullOrEmpty(facilityname) ? string.Empty : " (" + facilityname + ")");
                }
                else
                    return string.Empty;
            }
        }

        public static string UserLoginWelcomeInfoTooltip
        {
            get
            {
                if (HttpContext.Current != null && CurrentUser != null)
                {
                    string facilityname = Convert.ToString(HttpContext.Current.Session["FacilityName"]);
                    return CurrentUser.GroupName + (string.IsNullOrEmpty(facilityname) ? string.Empty : " (" + facilityname + ")");
                }
                else
                    return string.Empty;
            }
        }

        public static IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        public static string DecryptString(string text)
        {
            return string.IsNullOrEmpty(text) ? string.Empty : PR.UtilityLibrary.CommonUtility.Decrypt(text.Trim(), Helper.EnDeKey);            
        }

        public static string VersionNumber
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["Version"].ToString();
            }
        }

        /// <summary>
        /// Get Facility email Id
        /// </summary>
        /// <param name="strLocationCode"></param>
        /// <returns></returns>
        public static string GetFacilityOMEmailId(string strLocationCode)
        {
            DataSet oDataSet = new DataSet();
            LocalComponent oLocalComponent = new LocalComponent();
            oDataSet = oLocalComponent.GetCallBlastData(strLocationCode);
            return oDataSet.Tables[0].Rows[0]["Email1"].ToString().Trim();
        }

    }
}