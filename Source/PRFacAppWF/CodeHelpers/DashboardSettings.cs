﻿using System.Web;

namespace PRFacAppWF.CodeHelpers
{
    public class DashboardSettings
    {

        public string ChangeFacIconVisibility { get; set; }
        public bool MultipleFacalitiesAvailable { get; set; }
        public bool FacilitySelected { get; set; }

        public void CommitToSession()
        {
            HttpContext.Current.Session["DasbbrdSettings"] = this;
        }

        public static DashboardSettings RetrieveFromSession()
        {
            if (HttpContext.Current.Session["DasbbrdSettings"] != null)
                return HttpContext.Current.Session["DasbbrdSettings"] as DashboardSettings;
            else
                return new DashboardSettings();
        }
         
    }
}