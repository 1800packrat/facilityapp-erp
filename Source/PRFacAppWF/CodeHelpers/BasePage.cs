﻿using PR.LocalLogisticsSolution.Model;
using PRFacAppWF.Filters;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Http;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace   PRFacAppWF.CodeHelpers
{
    [Authorize]
    public class BasePage : Page
    {
        protected override void OnInit(EventArgs e)
        {
            if (HttpContext.Current.Session != null)
            {
                if (HttpContext.Current.Session.Count == 0)
                {
                    HttpContext.Current.Response.Redirect("/Login/Index?id=1",true);
                }
                else
                {                    
                    string pageName = Path.GetFileNameWithoutExtension(HttpContext.Current.Request.Url.AbsolutePath);

                    Entities.Enums.ModuleName moduleName = (Entities.Enums.ModuleName)Enum.Parse(typeof(Entities.Enums.ModuleName), pageName, true);

                    bool isAccessable = PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(moduleName, PRFacAppWF.Entities.Enums.AccessLevel.Any, true);

                    if (isAccessable == false)
                    {
                        HttpContext.Current.Response.Redirect("~/Unauthorized.aspx", true);
                    }
                }
            }
            base.OnInit(e);
        }

        protected void BindDataToGrid(DataSet ds, GridView gv, string EmptyText)
        {
            if (ds.Tables[0].Rows.Count == 0)
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gv.DataSource = ds;
                gv.DataBind();
                int columnCount = gv.Rows[0].Cells.Count;
                gv.Rows[0].Cells.Clear();
                gv.Rows[0].Cells.Add(new TableCell());
                gv.Rows[0].Cells[0].ColumnSpan = columnCount;
                gv.Rows[0].Height = 60;
                gv.Rows[0].Cells[0].Text = "<p style='adding-top:50px;padding-bottom;50px'>" + EmptyText + "</p>";
                gv.Rows[1].Cells.Clear();
                gv.Rows[1].Cells.Add(new TableCell());
                gv.Rows[1].Cells[0].ColumnSpan = columnCount;
                gv.Rows[1].Cells[0].BackColor = Color.FromArgb(123, 167, 204);
                gv.Rows[1].Height = 25;
            }
            else
            {
                gv.DataSource = ds;
                gv.DataBind();
            }
        }

        /// <summary>
        /// This property returns logged in user details. This session must be assigned after login
        /// </summary>
        public LogisticsUser CurrentUser
        {
            get
            {
                return Helper.CurrentUser;
                //if (HttpContext.Current.Session[typeof(LogisticsUser).Name] != null)
                //    return (LogisticsUser)HttpContext.Current.Session[typeof(LogisticsUser).Name];

                //return null;
            }
        }
    }
}
