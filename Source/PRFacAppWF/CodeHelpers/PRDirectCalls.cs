﻿using System; 
using PR.BusinessLogic;
using PR.Entities;


namespace PRFacAppWF.CodeHelpers
{
    public class PRDirectCalls
    {
       
        //public static void DisposeWebSLAPIObject()
        //{
        //    try
        //    {
        //        if (HttpContext.Current != null)
        //        {
        //            if (HttpContext.Current.Session["objSLAPI"] != null)
        //            { 
        //                HttpContext.Current.Session["objSLAPI"] = null;
        //            }
        //        }
        //        ManangeSLAPIObjects.RemoveSLAPIFromDics();
        //    }
        //    catch (Exception ex)
        //    {
        //        Helper.LogError(ex);
        //    }
        //}

       
        public static UserInfo Login(string FName, string LName, string Password)
        {
            UserInfo objUserInfo = null;
            try
            {
                objUserInfo = (new LocalComponent()).LogIn(FName, LName, Password);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                throw ex;
            }
            return objUserInfo;
        }
 
    }
}