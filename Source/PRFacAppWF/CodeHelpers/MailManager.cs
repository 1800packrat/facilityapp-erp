﻿using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Xml;
using PR.BusinessLogic;
using System.Threading;

namespace PRFacAppWF.CodeHelpers
{
    public class MailManager
    {
        public static void SendOrderMail(Hashtable h, string email)
        {
            var localComponent = new LocalComponent();
            Hashtable formData = h;
            IDictionaryEnumerator myEnumeratorNew = formData.GetEnumerator();
            var doc = new XmlDocument();
            var docTemp = new XmlDocument();
            var srm = new StreamReader((HttpContext.Current.Server.MapPath("SimpleSample.xml")));
            string XmlInnerText = srm.ReadToEnd();
            docTemp.LoadXml(XmlInnerText);
            srm.Close();
            foreach (XmlNode node in docTemp.GetElementsByTagName("HashtableContent").Item(0).ChildNodes)
            {
                if (h.ContainsKey(node.Name))
                {
                    XmlInnerText = XmlInnerText.Replace("~" + node.Name.Trim() + "~",
                                                        h.ContainsKey(node.Name.Trim())
                                                            ? h[node.Name.Trim()].ToString()
                                                            : "");
                }
                else
                {
                    XmlInnerText = XmlInnerText.Replace("~" + node.Name + "~", "");
                }
            }
            doc.LoadXml(XmlInnerText);
            var random = new Random();
            int rand = random.Next(1000, 9999);
            doc.Save(HttpContext.Current.Server.MapPath("/XMLFiles/" + rand + ".xml"));
 
            string SMTPServer = ConfigurationManager.AppSettings["SMTPServer"];
            var objRequest =
                (HttpWebRequest)
                WebRequest.Create(ConfigurationManager.AppSettings["AppURL"] + "emails/OrderEmail.aspx?id=" + rand);

            objRequest.Method = WebRequestMethods.Http.Get;
            objRequest.Proxy = null;
            objRequest.UseDefaultCredentials = true;
            var objResponse = (HttpWebResponse)objRequest.GetResponse();

            var sr = new StreamReader(objResponse.GetResponseStream());
            String emailbody = sr.ReadToEnd();
      
            sr.Close();
            var message = new MailMessage(ConfigurationManager.AppSettings["mailFrom"], email,
                                          "Your order from 1-800PACKRAT.",
                                          emailbody);
             
            message.IsBodyHtml = true;
            ThreadPool.QueueUserWorkItem(new WaitCallback(SendAsyncEmail), message);
        
            string Comments = "***Automatic HTML Order Summary E-Mail sent to " + email + " on " + DateTime.Now;
            localComponent.InsertActivityLog(HttpContext.Current.Session["UserId"].ToString(), DateTime.Now,
                                             Convert.ToInt32(Helper.GetSession("SiteId")),
                                             Convert.ToInt32(Helper.GetSession("qorid")),
                                             Convert.ToInt32(Helper.GetSession("TennantId")), Comments);
           
        }

        public static void SendTransUpdateEmail(Hashtable h, string email)
        {
            try
            {
                var localComponent = new LocalComponent();
                Hashtable formData = h;
                IDictionaryEnumerator myEnumeratorNew = formData.GetEnumerator();
                var doc = new XmlDocument();
                var docTemp = new XmlDocument();
                var srm = new StreamReader((HttpContext.Current.Server.MapPath("SimpleSample.xml")));
                string XmlInnerText = srm.ReadToEnd();
                docTemp.LoadXml(XmlInnerText);
                srm.Close();
                foreach (XmlNode node in docTemp.GetElementsByTagName("HashtableContent").Item(0).ChildNodes)
                {
                    if (h.ContainsKey(node.Name))
                    {
                        XmlInnerText = XmlInnerText.Replace("~" + node.Name.Trim() + "~",
                                                            h.ContainsKey(node.Name.Trim())
                                                                ? h[node.Name.Trim()].ToString()
                                                                : "");
                    }
                    else
                    {
                        XmlInnerText = XmlInnerText.Replace("~" + node.Name + "~", "");
                    }
                }
                doc.LoadXml(XmlInnerText);
                var random = new Random();
                int rand = random.Next(1000, 9999);
                doc.Save(HttpContext.Current.Server.MapPath("/XMLFiles/" + rand + ".xml"));
      
                string SMTPServer = ConfigurationManager.AppSettings["SMTPServer"];
                var objRequest =
                    (HttpWebRequest)
                    WebRequest.Create(ConfigurationManager.AppSettings["AppURL"] + "emails/transportationEmail.aspx?id=" + rand);

                objRequest.Method = WebRequestMethods.Http.Get;
                objRequest.Proxy = null;

                var objResponse = (HttpWebResponse)objRequest.GetResponse

                    ();
                var sr = new StreamReader(objResponse.GetResponseStream());
                String emailbody = sr.ReadToEnd();
             
                sr.Close();
                var message = new MailMessage(ConfigurationManager.AppSettings["mailFrom"], email,
                                              "Confirmation of Your Appointment with 1-800-PACK-RAT",
                                              emailbody); 
                message.IsBodyHtml = true;
                ThreadPool.QueueUserWorkItem(new WaitCallback(SendAsyncEmail), message);
     
                string Comments = "***Automatic HTML Transportation Summary E-Mail sent to " + email + " on " +
                                  DateTime.Now;
                localComponent.InsertActivityLog(HttpContext.Current.Session["UserId"].ToString(), DateTime.Now,
                                                 Convert.ToInt32(Helper.GetSession("SiteId")),
                                                 Convert.ToInt32(Helper.GetSession("qorid")),
                                                 Convert.ToInt32(Helper.GetSession("TennantId")), Comments);
           
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void SendQouteEmail(Hashtable h, string email)
        {
            try
            {
                var localComponent = new LocalComponent();
                Hashtable formData = h;
                IDictionaryEnumerator myEnumeratorNew = formData.GetEnumerator();
                var doc = new XmlDocument();
                var docTemp = new XmlDocument();
                var srm = new StreamReader((HttpContext.Current.Server.MapPath("SimpleSample.xml")));
                string XmlInnerText = srm.ReadToEnd();
                docTemp.LoadXml(XmlInnerText);
                srm.Close();
                foreach (XmlNode node in docTemp.GetElementsByTagName("HashtableContent").Item(0).ChildNodes)
                {
                    if (h.ContainsKey(node.Name))
                    {
                        XmlInnerText = XmlInnerText.Replace("~" + node.Name.Trim() + "~",
                                                            h.ContainsKey(node.Name.Trim())
                                                                ? h[node.Name.Trim()].ToString()
                                                                : "");
                    }
                    else
                    {
                        XmlInnerText = XmlInnerText.Replace("~" + node.Name + "~", "");
                    }
                }
                doc.LoadXml(XmlInnerText);
                var random = new Random();
                int rand = random.Next(1000, 9999);
                doc.Save(HttpContext.Current.Server.MapPath("/XMLFiles/" + rand + ".xml"));
                // Ends Here
                string SMTPServer = ConfigurationManager.AppSettings["SMTPServer"];
                var objRequest =
                    (HttpWebRequest)
                    WebRequest.Create(ConfigurationManager.AppSettings["AppURL"] + "emails/QuoteEmail.aspx?id=" +
                                      rand);

                objRequest.Method = WebRequestMethods.Http.Get;
                objRequest.Proxy = null;

                var objResponse = (HttpWebResponse)objRequest.GetResponse();
                var sr = new StreamReader(objResponse.GetResponseStream());
                String emailbody = sr.ReadToEnd();
                //    emailbody = emailbody.Replace("&amp;", "&");
                sr.Close();
                var message = new MailMessage(ConfigurationManager.AppSettings["mailFrom"], email,
                                              "Your quote from 1-800PACKRAT. Call 1-800-722-5728 to schedule delivery.",
                                              emailbody);
                //   var emailClient = new SmtpClient(SMTPServer);
                message.IsBodyHtml = true;
                ThreadPool.QueueUserWorkItem(new WaitCallback(SendAsyncEmail), message);
                // emailClient.Send(message);
                string Comments = "***Automatic HTML Quote Summary E-Mail sent to " + email + " on " + DateTime.Now;
                localComponent.InsertActivityLog(HttpContext.Current.Session["UserId"].ToString(), DateTime.Now,
                                                 Convert.ToInt32(Helper.GetSession("SiteId")),
                                                 Convert.ToInt32(Helper.GetSession("qorid")),
                                                 Convert.ToInt32(Helper.GetSession("TennantId")), Comments);
                //PRDirectCalls.DisposeWebSLAPIObject();
                //HttpContext.Current.Session["objSLAPI"] = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void SendCallBlastEmail(string fromEmail, string toEmail, string subject, string message)
        {
            try
            {
                LocalComponent lc = new LocalComponent();

                var mailMessage = new MailMessage(fromEmail, toEmail, subject, message);
                MailAddress mAddress = new MailAddress(ConfigurationManager.AppSettings["SupervisorMail"].ToString());
                mailMessage.CC.Add(mAddress);

                mailMessage.IsBodyHtml = true;
                ThreadPool.QueueUserWorkItem(new WaitCallback(SendAsyncEmail), mailMessage);

                // emailClient.Send(mailMessage);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private static void SendAsyncEmail(object mailMsg)
        {
            string SMTPServer = ConfigurationManager.AppSettings["SMTPServer"];
            var mailMessage = (MailMessage)mailMsg;
            var emailClient = new SmtpClient(SMTPServer);
            emailClient.Send(mailMessage);
        }

        public static void SendCallBlastEmail(string fromEmail, string toEmail, string cc, string bcc, string subject, string message)
        {
            try
            {
                LocalComponent lc = new LocalComponent();

                var mailMessage = new MailMessage(fromEmail, toEmail, subject, message);
                MailAddress mAddress = new MailAddress(ConfigurationManager.AppSettings["SupervisorMail"].ToString());
                mailMessage.CC.Add(cc);
                mailMessage.Bcc.Add(bcc);
                mailMessage.IsBodyHtml = true;
                ThreadPool.QueueUserWorkItem(new WaitCallback(SendAsyncEmail), mailMessage);
                string activityMessage = string.Format("A call blast email sent to {0}. MailBody: {1}", toEmail, message);
                lc.InsertActivityLog(Convert.ToInt32(HttpContext.Current.Session["qorid"].ToString()), "CallBlast mail message", activityMessage, HttpContext.Current.Session["UserId"].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string LoadEmailTemplate(string templateFileName)
        {
            string templatesPath = System.Web.HttpContext.Current.Server.MapPath("~/emails");
            string filePath = System.IO.Path.Combine(templatesPath, templateFileName);
            using (System.IO.StreamReader fileStream = System.IO.File.OpenText(filePath))
            {
                return fileStream.ReadToEnd();
            }
        }

        public static void SendEmail(string messageBody, string emailAddress, string subject)
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(ConfigurationManager.AppSettings["mailFrom"], emailAddress);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = messageBody;
            ThreadPool.QueueUserWorkItem(new WaitCallback(SendAsyncEmail), message);
            //string SMTPServer = ConfigurationManager.AppSettings["SMTPServer"];
            //System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            //client.Host = SMTPServer;
            //client.Send(message);
        }

        /// <summary>
        /// Function To send mail 
        /// </summary>
        /// <param name="mailFrom"></param>
        /// <param name="mailTo"></param>
        /// <param name="mailSubject"></param>
        /// <param name="mailBody"></param>
        /// <param name="mailAttachment"></param>
        /// <param name="mailCc"></param>
        /// <param name="mailBcc"></param>
        /// <param name="PortIP"></param>
        public static void SendEmail(string mailFrom, string mailTo, string mailSubject, string mailBody, string mailAttachment = "", string mailCc = "", string mailBcc = "")
        {
            try
            {
                //---- For Multiple To mail Id --------
                if (!string.IsNullOrEmpty(mailTo) || mailTo != string.Empty)
                {
                    //To get the To Mail Id
                    //------------------------------------
                    System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
                    SmtpClient smtp = new SmtpClient();
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Priority = MailPriority.High;
                    MailAddress from = new MailAddress(mailFrom);
                    mailMessage.From = from;
                    foreach (var address in mailTo.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        mailMessage.To.Add(address);
                    }
                    mailMessage.Body = Convert.ToString(mailBody);
                    mailMessage.Subject = mailSubject;
                    smtp.Host = "localhost";
                    smtp.UseDefaultCredentials = true;

                    // for Uploaded File Attachment,get the file path
                    //--------------------------------------------------
                    if (!string.IsNullOrEmpty(mailAttachment) & mailAttachment != string.Empty)
                    {
                        if (File.Exists(mailAttachment) == true)
                        {
                            mailMessage.Attachments.Add(new System.Net.Mail.Attachment(mailAttachment));
                        }
                    }
                    //--------------------------------------------------
                    //code for cc
                    if ((!string.IsNullOrEmpty(mailCc) & mailCc != string.Empty))
                    {
                        string[] strCCarry = mailCc.Split(';');
                        for (int intCounter = 0; intCounter <= strCCarry.Length - 1; intCounter++)
                        {
                            mailMessage.CC.Add(new MailAddress(strCCarry[intCounter].ToString()));
                        }
                    }
                    //Code for bcc
                    if ((!string.IsNullOrEmpty(mailBcc) & mailBcc != string.Empty))
                    {
                        string[] strBCCarry = mailBcc.Split(';');
                        for (int intbcccounter = 0; intbcccounter <= strBCCarry.Length - 1; intbcccounter++)
                        {
                            mailMessage.Bcc.Add(new MailAddress(strBCCarry[intbcccounter].ToString()));
                        }
                    }
                    ThreadPool.QueueUserWorkItem(new WaitCallback(SendAsyncEmail), mailMessage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }
}
