﻿namespace PRFacAppWF.CodeHelpers
{
    using Entities;
    using Entities.Enums;
    using Entities.Touches;
    using PR.BusinessLogic;
    using PR.UtilityLibrary;
    using Repository.Services;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using PR.Entities;
    using PR.Mailer.SendGrid;
    using System.Collections;
    using System.Threading;
    using PR.Mailer.Model;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using System.IO;
    using PR.Entities.EsbEntities;

    public class WarehouseTouches
    {
        private readonly IPRSService prsService;
        private GlobalSettings gs;

        public WarehouseTouches()
        {
            if (this.prsService == null)
                this.prsService = new PRSService();

            if (gs == null)
                gs = new GlobalSettings();
        }

        // TG-713 , ticket created for ESB endpoint
        public List<WATouchInfo> GetWATouchs(TouchListFilterCriteria FilterCriteria)
        {
            #region SFERP-TODO-ESBMTD
            /* TG-548 - Create Stub method to run and test the app for the below section. 
             * 1. Get Last 30 days WareHouse Touches including todays Date.
             * 2. Get All tocuhes based on Start and End Date selection.
             * Get data from ESB method call for future and make the build success. 
             * FinalTouchList - Replace/update this list from the ESB method call result
             * Compare data from ESB and Stub Method, if all good - Remove the Stub Method. */

            // TG-713 , ticket created for ESB endpoint
            //Read JSON data from file and replace with ESB method call.
            List<WATouchInfo> waTouchListFromSL = GetWareHouseTouches(FilterCriteria);

            int StoreNumber = Convert.ToInt32(Helper.CurrentSiteInfo.GlobalSiteNum);

            ////Get Touch List from Stub Method call.
            //string jsonData2 = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetWATouchs-{FilterCriteria.LocationCode}.txt"));
            //waTouchListFromSL = (List<WATouchInfo>)JsonConvert.DeserializeObject(jsonData2, (typeof(List<WATouchInfo>)));

            //Get touch information from RoutOptimization DB
            List<WATouchInfo> dbwaTouchListFromSL = prsService.GetWATouchInformation(FilterCriteria.StartDate, FilterCriteria.EndDate, StoreNumber, waTouchListFromSL);

            ///Here need to merge Saleforce touches with Local touches and update status based on business needs
            List<WATouchInfo> FinalTouchList = MergeLoadTouchesWithSLTouches(waTouchListFromSL, dbwaTouchListFromSL);

            #endregion

            //Set type 1 alert for warehouse touches
            string[] WareHouseTouchTypes = new string[] { "WA", "IBO", "OBO", "TO", "TI" };
            FinalTouchList.Where(t => t.DispTouchStatus != "Completed" && WareHouseTouchTypes.Contains(t.TouchType) && t.ScheduledDate.Value.Date < DateTime.Now.Date)
                .ToList()
                .ForEach(t => t.AlertLevel = 1);

            //Set type 2 alerts for date changes / unschedule / reschedule touches
            FinalTouchList.Where(t => t.DispTouchStatus != "Completed" && (t.IsConflictWithScheduleDate || t.IsTouchMissed))
                .ToList()
                .ForEach(t => { t.AlertLevel = 2; });

            return FinalTouchList == null ? new List<WATouchInfo>() : FinalTouchList;
        }

        //SFERP-TODO-CTUPD - Replace staic data with actual contents.
        private List<WATouchInfo> GetWareHouseTouches(TouchListFilterCriteria filterCriteria)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();

            WarehouseTouchRequest requestObject = new WarehouseTouchRequest
            {
                locationCode = filterCriteria.LocationCode,
                waStartDate = DateTime.Now.AddDays(gs.FAWTShowPriorNotCompletedTouchesDays).ToString("MM/dd/yyyy"),
                waEndDate = DateTime.Now.ToString("MM/dd/yyyy"),
                trStartDate = filterCriteria.StartDate.ToString("MM/dd/yyyy"),
                endDate = filterCriteria.EndDate.ToString("MM/dd/yyyy")
            };

            List<WATouchInfo> finalTouchList = new List<WATouchInfo>();

            PR.Entities.EsbEntities.TouchDashboard.RootObject touchList = smd.GetWareHouseTouches(requestObject);

            if (touchList != null && touchList.SF_WarehouseTouch != null)
            {
                foreach (var sfTouch in touchList.SF_WarehouseTouch)
                {
                    var objTouch = new WATouchInfo()
                    {
                        QORId = string.IsNullOrWhiteSpace(sfTouch.QORId) ? 0 : Convert.ToInt32(sfTouch.QORId),
                        UnitName = CommonUtility.GetUnitNameOrNumber(sfTouch.UnitName),
                        SequenceNum = string.IsNullOrWhiteSpace(sfTouch.SequenceNo) ? 0 : Convert.ToInt32(sfTouch.SequenceNo),
                        StarsId = string.IsNullOrWhiteSpace(sfTouch.OrderNo) ? 0 : Convert.ToInt32(sfTouch.OrderNo),
                        StarsUnitId = string.IsNullOrWhiteSpace(sfTouch.UnitId) ? 0 : Convert.ToInt32(sfTouch.UnitId),
                        IsLDMOrderTouch = string.IsNullOrWhiteSpace(sfTouch.IsLDMOrderTouch) ? false : Convert.ToBoolean(sfTouch.IsLDMOrderTouch),
                        TouchTypeFull = string.IsNullOrWhiteSpace(sfTouch.TouchTypeFull) ? "" : sfTouch.TouchTypeFull,
                        TenantID = string.IsNullOrWhiteSpace(sfTouch.CustomerId) ? 0 : Convert.ToInt32(sfTouch.CustomerId),
                        Phonenumber = string.IsNullOrWhiteSpace(sfTouch.PhoneNo) ? "" : sfTouch.PhoneNo,

                        SLTouchStatus = sfTouch.TouchStatus == WATouchStatus.Open.ToString() ? WATouchStatus.Scheduled.ToString() : sfTouch.TouchStatus,

                        FirstName = string.IsNullOrWhiteSpace(sfTouch.Customer_FirstName) ? "" : sfTouch.Customer_FirstName,
                        LastName = string.IsNullOrWhiteSpace(sfTouch.Customer_LastName) ? "" : sfTouch.Customer_LastName,
                        dcMileage = string.IsNullOrWhiteSpace(sfTouch.TouchMiles) ? Convert.ToDecimal(0) : Convert.ToDecimal(sfTouch.TouchMiles),
                        Comments = string.IsNullOrWhiteSpace(sfTouch.Comments) ? "" : sfTouch.Comments,

                        CustAddress1 = string.IsNullOrWhiteSpace(sfTouch.Customer_Address1) ? "" : sfTouch.Customer_Address1,
                        CustAddress2 = string.IsNullOrWhiteSpace(sfTouch.Customer_Address2) ? "" : sfTouch.Customer_Address2,
                        CustCity = string.IsNullOrWhiteSpace(sfTouch.Customer_City) ? "" : sfTouch.Customer_City,
                        CustRegion = string.IsNullOrWhiteSpace(sfTouch.Customer_Region) ? "" : sfTouch.Customer_Region,
                        CustZip = string.IsNullOrWhiteSpace(sfTouch.Customer_Zip) ? "" : sfTouch.Customer_Zip,

                        FromFirstName = string.IsNullOrWhiteSpace(sfTouch.Origin_FirstName) ? "" : sfTouch.Origin_FirstName,
                        FromLastName = string.IsNullOrWhiteSpace(sfTouch.Origin_LastName) ? "" : sfTouch.Origin_LastName,
                        FromPhone = string.IsNullOrWhiteSpace(sfTouch.Origin_PhoneNo) ? "" : sfTouch.Origin_PhoneNo,
                        FromAddr1 = string.IsNullOrWhiteSpace(sfTouch.Origin_Address1) ? "" : sfTouch.Origin_Address1,
                        FromAddr2 = string.IsNullOrWhiteSpace(sfTouch.Origin_Address2) ? "" : sfTouch.Origin_Address2,
                        FromCity = string.IsNullOrWhiteSpace(sfTouch.Origin_City) ? "" : sfTouch.Origin_City,
                        FromState = string.IsNullOrWhiteSpace(sfTouch.Origin_State) ? "" : sfTouch.Origin_State,
                        FromZip = string.IsNullOrWhiteSpace(sfTouch.Origin_Zip) ? "" : sfTouch.Origin_Zip,

                        ToFirstName = string.IsNullOrWhiteSpace(sfTouch.Destination_FirstName) ? "" : sfTouch.Destination_FirstName,
                        ToLastName = string.IsNullOrWhiteSpace(sfTouch.Destination_LastName) ? "" : sfTouch.Destination_LastName,
                        ToPhone = string.IsNullOrWhiteSpace(sfTouch.Destination_PhoneNo) ? "" : sfTouch.Destination_PhoneNo,
                        ToAddr1 = string.IsNullOrWhiteSpace(sfTouch.Destination_Address1) ? "" : sfTouch.Destination_Address1,
                        ToAddr2 = string.IsNullOrWhiteSpace(sfTouch.Destination_Address2) ? "" : sfTouch.Destination_Address2,
                        ToCity = string.IsNullOrWhiteSpace(sfTouch.Destination_City) ? "" : sfTouch.Destination_City,
                        ToState = string.IsNullOrWhiteSpace(sfTouch.Destination_State) ? "" : sfTouch.Destination_State,
                        ToZip = string.IsNullOrWhiteSpace(sfTouch.Destination_Zip) ? "" : sfTouch.Destination_Zip,

                        ScheduledDate = sfTouch.ScheduledDate == DateTime.MinValue ? DateTime.Now : Convert.ToDateTime(sfTouch.ScheduledDate),
                        SLScheduledDate = sfTouch.ScheduledDate == DateTime.MinValue ? DateTime.Now : Convert.ToDateTime(sfTouch.ScheduledDate), // same as ScheduledDate
                        TouchTime = string.IsNullOrWhiteSpace(sfTouch.TouchTime) ? "Anytime" : sfTouch.TouchTime,
                    };

                    finalTouchList.Add(objTouch);
                }
            }
            else
            {
                throw new Exception("EsbMethod.GetWareHouseTouches - No touch available.");
            }

            return finalTouchList;
        }

        private List<WATouchInfo> MergeLoadTouchesWithSLTouches(List<WATouchInfo> slTouches, List<WATouchInfo> dbTouches)
        {
            LocalComponent local = new LocalComponent();
            List<int> QORId = slTouches.Select(s => s.QORId).ToList();

            string uniqueIdsToGetETASettings = string.Join(",", slTouches.Select(s => string.Format("{0}_{1}_{2}", s.QORId, s.TouchType, s.SequenceNum)).ToList());

            var etaSettings = local.GetETASettingList(uniqueIdsToGetETASettings);

            foreach (var slTouch in slTouches)
            {
                WATouchInfo dbTouch = dbTouches.Where(t => t.QORId == slTouch.QORId && t.SequenceNum == slTouch.SequenceNum && t.TouchType == slTouch.TouchType).FirstOrDefault();

                if (dbTouch != null)
                {
                    if (slTouch.SLTouchStatus == WATouchStatus.OnHold.ToString())
                    {
                        slTouch.TouchStatus = WATouchStatus.OnHold;
                        slTouch.TouchStatusID = (int)WATouchStatus.OnHold;
                    }
                    else
                    {
                        slTouch.TouchStatusID = dbTouch.TouchStatusID;
                        slTouch.TouchStatus = dbTouch.TouchStatus;
                    }

                    if (slTouch.UnitName == Helper.GetConfigSettings("16FootUName") || slTouch.UnitName == Helper.GetConfigSettings("12FootUName")
                       || slTouch.UnitName == Helper.GetConfigSettings("8FootUName"))
                    {
                        slTouch.UnitName = dbTouch.UnitName;
                    }

                    slTouch.Comments = dbTouch.Comments;

                    slTouch.ActionID = dbTouch.ActionID;
                    slTouch.ActionName = dbTouch.ActionName;
                    slTouch.StageActionID = dbTouch.StageActionID;
                    slTouch.StageActionName = dbTouch.StageActionName;
                    slTouch.TouchActionCategory = dbTouch.TouchActionCategory;

                    //We will not have schedule date in database in following case - the touch is unscheduled and then come back
                    if (dbTouch.ScheduledDate.HasValue)
                        slTouch.ScheduledDate = dbTouch.ScheduledDate;

                    //Once you copy local data to sltouch then delete it
                    dbTouches.Remove(dbTouch);
                }
                else
                {
                    FAWTTouchActions defaultAction = new FAWTTouchActions();
                    slTouch.ActionID = defaultAction.ActionID;
                    slTouch.ActionName = defaultAction.ActionName;
                    slTouch.TouchActionCategory = WTTouchActionCategory.None;

                    if (slTouch.SLTouchStatus == WATouchStatus.Scheduled.ToString())
                    {
                        slTouch.TouchStatus = WATouchStatus.Scheduled;
                        slTouch.TouchStatusID = (int)WATouchStatus.Scheduled;
                    }
                    else if (slTouch.SLTouchStatus == WATouchStatus.OnHold.ToString())
                    {
                        slTouch.TouchStatus = WATouchStatus.OnHold;
                        slTouch.TouchStatusID = (int)WATouchStatus.OnHold;
                    }
                    else if (slTouch.SLTouchStatus == WATouchStatus.Completed.ToString())
                    {
                        slTouch.TouchStatus = WATouchStatus.Completed;
                        slTouch.TouchStatusID = (int)WATouchStatus.Completed;
                    }
                }

                slTouch.ETASettings = etaSettings.Where(t => t.TouchKey == string.Format("{0}_{1}_{2}", slTouch.QORId, slTouch.TouchType, slTouch.SequenceNum)).FirstOrDefault();
            }

            ////SEFRP-TODO-RMVNICD 
            // Remove after full testing required, by Sohan
            //if (dbTouches.Count > 0)
            //{
            //    slTouches.AddRange(dbTouches);

            //    List<int> lstQorIDs = dbTouches.Select(t => t.QORId).ToList();

            //    List<WATouchInfo> missingSLTouches = GetWATouchsFromSL(dbTouches);

            //    foreach (var dbTouch in dbTouches)
            //    {
            //        WATouchInfo slTouch = missingSLTouches.Where(t => t.QORId == dbTouch.QORId && t.SequenceNum == dbTouch.SequenceNum && t.TouchType == dbTouch.TouchType).FirstOrDefault();

            //        if (slTouch != null && slTouch.SLScheduledDate.HasValue)
            //        {
            //            dbTouch.SLScheduledDate = slTouch.SLScheduledDate;
            //            dbTouch.SLTouchStatus = slTouch.SLTouchStatus;
            //            //if ((dbTouch.TouchType == "IBO" || dbTouch.TouchType == "OBO") && slTouch.SLTouchStatus == WATouchStatus.OnHold.ToString())
            //            if (slTouch.SLTouchStatus == WATouchStatus.OnHold.ToString())
            //            {
            //                dbTouch.TouchStatus = WATouchStatus.OnHold;
            //                dbTouch.TouchStatusID = (int)WATouchStatus.OnHold;
            //            }
            //        }
            //        else
            //        {
            //            //dbTouch.TouchStatus = WATouchStatus.OnHold;
            //            //dbTouch.TouchStatusID = (int)WATouchStatus.OnHold;
            //            //dbTouch.SLScheduledDate = slTouch.SLScheduledDate;
            //            dbTouch.SLTouchStatus = WATouchStatus.UnScheduled.ToString();
            //            dbTouch.TouchStatus = WATouchStatus.UnScheduled;
            //            dbTouch.TouchStatusID = (int)WATouchStatus.UnScheduled;

            //            dbTouch.SLScheduledDate = null;
            //            dbTouch.IsTouchMissed = true;
            //        }
            //    }
            //}

            return slTouches;
        }

        public List<FAWTTouchStatus> GetWHTStatusList()
        {
            List<FAWTTouchStatus> lstTouchStatus = new List<FAWTTouchStatus>();

            lstTouchStatus = prsService.GetWHTStatusList();

            FAWTTouchStatus faWTStatusCategory = new FAWTTouchStatus()
            {
                TouchStatusID = 0,
                StatusName = "Not Completed"
            };

            lstTouchStatus.Add(faWTStatusCategory);

            return lstTouchStatus.OrderBy(x => x.StatusName).ToList();
        }

        public void HandleUpdateWarehouseTouchInfo(WATouchInfo TouchInfo, int UserId)
        {
            TouchInfo.StoreNumber = Convert.ToInt32(Helper.CurrentSiteInfo.GlobalSiteNum);

            if (TouchInfo.IsChangeAction)
            {
                WATouchTypesAndActionCategory touchSettings = prsService.GetTouchActionSetting(TouchInfo.TouchType, TouchInfo.TouchActionCategory);
                LocalComponent lc = new LocalComponent();
                TouchInfo.CanUpdateTouchStatusID = touchSettings.CanUpdateLocalStatus;

                if (touchSettings.CanUpdateLocalStatus && touchSettings.UpdateLocalStatusID.HasValue)
                {
                    TouchInfo.TouchStatusID = touchSettings.UpdateLocalStatusID.Value;
                    TouchInfo.TouchStatus = (WATouchStatus)Enum.Parse(typeof(WATouchStatus), touchSettings.UpdateLocalStatusName);
                }

                switch (TouchInfo.TouchActionCategory)
                {
                    case WTTouchActionCategory.Stage:
                        //As of now, we dont need to do any SL action for this status change
                        //If user is updating touch action which is of type stage, then we have to keep stage action. Stage action name is used to identify stage location name
                        TouchInfo.StageActionID = TouchInfo.ActionID;
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " is staged at " + TouchInfo.ActionName, Helper.CurrentUser.UserName, string.Empty, 0);
                        break;
                    case WTTouchActionCategory.NoShowDispatchAlerted:
                        HandleTouchNoShowDispatchAlertedActions(TouchInfo);
                        break;
                    case WTTouchActionCategory.NoShowReturnToStack:
                        HandleTouchNoShowReturnToStackActions(TouchInfo, touchSettings);
                        break;
                    case WTTouchActionCategory.NoShowLeaveStaged:
                        HandleTouchNoShowLeaveStagedActions(TouchInfo, touchSettings);
                        break;
                    case WTTouchActionCategory.Completed:
                        HandleTouchCompleteActions(TouchInfo, touchSettings);
                        break;
                    case WTTouchActionCategory.ReturnToStack:
                        //As of now, we dont need to do any SL action for this status change
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " is " + TouchInfo.ActionName, Helper.CurrentUser.UserName, string.Empty, 0);

                        TouchInfo.DontChangeStatusForTheFirstUpdateButChangeInSecondUpdate = true;
                        TouchInfo.UpdateCustomActionID = (int)WTTouchActionCategory.None;
                        break;
                    case WTTouchActionCategory.LeaveStaged:
                        //As of now, we dont need to do any SL action for this status change
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " is " + TouchInfo.ActionName, Helper.CurrentUser.UserName, string.Empty, 0);
                        break;
                    default:
                        throw new Exception("This action change functionality not yet implemented...! Please contact Admin/IT department.");
                }

                if (touchSettings.CanSendNotification)
                {
                    //Email communication as per settings
                    LoadReqDataAndSendEmailThroughSendGrid(TouchInfo, touchSettings.EmailTemplateIDs, UserId);
                }
            }

            prsService.SaveWTTouchInformation(TouchInfo, UserId);

        }

        private void HandleTouchNoShowDispatchAlertedActions(WATouchInfo TouchInfo)
        {
            LocalComponent lc = new LocalComponent();

            switch (TouchInfo.TouchType)
            {
                case "TI":
                    lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "LDM In Touch is no-show dispatch alerted in FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                    break;
                case "TO":
                    lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "LDM Out Touch is no-show dispatch alerted in FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                    break;
                default:
                    throw new Exception("Invalid action for this touch type, Please contact admin for more details.");
            }
        }

        private void HandleTouchNoShowReturnToStackActions(WATouchInfo TouchInfo, WATouchTypesAndActionCategory settings)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic(Helper.CurrentUser.UserName);
            LocalComponent lc = new LocalComponent();

            switch (TouchInfo.TouchType)
            {
                case "WA":
                    if (settings.CanUpdateStatusInSL)
                    {
                        smd.CompleteIBOWHOBOExtraTouches(TouchInfo.QORId, TouchInfo.TouchTypeFull, TouchInfo.SequenceNum.Value, Helper.CurrentUser.UserName, TouchInfo.UnitName);
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "WA Touch is no-show return to stack, it has been completed from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                        TouchInfo.SLTouchStatus = WATouchStatus.Completed.ToString();
                        TouchInfo.CompletedDateTime = DateTime.Now;
                    }
                    else
                    {
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "WA Touch is no-show return to stack, no status updates from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                    }
                    break;
                case "IBO":
                    if (settings.CanUpdateStatusInSL)
                    {
                        PREnums.TouchTypeShort touchtype = (PREnums.TouchTypeShort)Enum.Parse(typeof(PREnums.TouchTypeShort), TouchInfo.TouchType);

                        smd.HoldTouch(TouchInfo.QORId, touchtype, TouchInfo.SequenceNum.Value, Helper.CurrentUser.UserName, TouchInfo.UnitName);
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "IBO Touch is no-show return to stack, it has been put on-hold from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                        TouchInfo.SLTouchStatus = WATouchStatus.OnHold.ToString();
                    }
                    else
                    {
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "IBO Touch is no-show return to stack, no status updates from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                    }
                    TouchInfo.DontChangeStatusForTheFirstUpdateButChangeInSecondUpdate = true;
                    TouchInfo.UpdateCustomActionID = (int)WTTouchActionCategory.None;
                    break;
                case "OBO":
                    if (settings.CanUpdateStatusInSL)
                    {
                        //To unschedule a touch, we should pass 1/1/1753 as date
                        DateTime dt = DateTime.Parse("1/1/1753");
                        PREnums.TouchTypeShort touchtype = (PREnums.TouchTypeShort)Enum.Parse(typeof(PREnums.TouchTypeShort), TouchInfo.TouchType);

                        smd.ScheduleTouch(TouchInfo.QORId, touchtype, TouchInfo.SequenceNum.Value, dt, TouchInfo.TouchTime, Helper.CurrentUser.UserName);
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "OBO Touch is no-show return to stack, it has been unscheduled from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                        TouchInfo.SLTouchStatus = WATouchStatus.Open.ToString();
                    }
                    else
                    {
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "OBO Touch is no-show return to stack, no status updates from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                    }
                    TouchInfo.DontChangeStatusForTheFirstUpdateButChangeInSecondUpdate = true;
                    TouchInfo.UpdateCustomActionID = (int)WTTouchActionCategory.None;
                    break;
                default:
                    throw new Exception("Invalid action for this touch type, Please contact admin for more details.");
            }
        }

        private void HandleTouchNoShowLeaveStagedActions(WATouchInfo TouchInfo, WATouchTypesAndActionCategory settings)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic(Helper.CurrentUser.UserName);
            LocalComponent lc = new LocalComponent();

            switch (TouchInfo.TouchType)
            {
                case "IBO":
                    if (settings.CanUpdateStatusInSL)
                    {
                        PREnums.TouchTypeShort touchtype = (PREnums.TouchTypeShort)Enum.Parse(typeof(PREnums.TouchTypeShort), TouchInfo.TouchType);

                        smd.HoldTouch(TouchInfo.QORId, touchtype, TouchInfo.SequenceNum.Value, Helper.CurrentUser.UserName, TouchInfo.UnitName);
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "IBO Touch is no-show leave staged, it has been put on-hold from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                        TouchInfo.SLTouchStatus = WATouchStatus.OnHold.ToString();
                    }
                    else
                    {
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "IBO Touch status has been update to no-show leave staged from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                    }
                    TouchInfo.DontChangeStatusForTheFirstUpdateButChangeInSecondUpdate = true;
                    TouchInfo.UpdateCustomActionID = (int)WTTouchActionCategory.None;
                    break;
                case "OBO":
                    if (settings.CanUpdateStatusInSL)
                    {
                        //To unschedule a touch, we should pass 1/1/1753 as date
                        DateTime dt = DateTime.Parse("1/1/1753");
                        PREnums.TouchTypeShort touchtype = (PREnums.TouchTypeShort)Enum.Parse(typeof(PREnums.TouchTypeShort), TouchInfo.TouchType);

                        smd.ScheduleTouch(TouchInfo.QORId, touchtype, TouchInfo.SequenceNum.Value, dt, TouchInfo.TouchTime, Helper.CurrentUser.UserName);
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "OBO Touch is no-show leave staged, it has been unscheduled from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                        TouchInfo.SLTouchStatus = WATouchStatus.Open.ToString();
                    }
                    else
                    {
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", "OBO Touch status has been update to no-show leave staged from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                    }
                    TouchInfo.DontChangeStatusForTheFirstUpdateButChangeInSecondUpdate = true;
                    TouchInfo.UpdateCustomActionID = (int)WTTouchActionCategory.None;
                    break;
                default:
                    throw new Exception("Invalid action for this touch type, Please contact admin for more details.");
            }

        }


        private void HandleTouchCompleteActions(WATouchInfo TouchInfo, WATouchTypesAndActionCategory settings)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic(Helper.CurrentUser.UserName);
            LocalComponent lc = new LocalComponent();

            switch (TouchInfo.TouchType)
            {
                case "WA":
                    if (settings.CanUpdateStatusInSL)
                    {
                        //Complete touch in SL
                        smd.CompleteIBOWHOBOExtraTouches(TouchInfo.QORId, TouchInfo.TouchTypeFull, TouchInfo.SequenceNum.Value, Helper.CurrentUser.UserName, TouchInfo.UnitName);
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " Touch has been completed from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                        TouchInfo.SLTouchStatus = WATouchStatus.Completed.ToString();
                    }
                    else
                    {
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " Touch status has been updated to completed from FacilityApp but not completed in SiteLink", Helper.CurrentUser.UserName, string.Empty, 0);
                    }
                    TouchInfo.CompletedDateTime = DateTime.Now;

                    break;
                case "IBO":
                    if (settings.CanUpdateStatusInSL)
                    {
                        //Complete touch in SL
                        smd.CompleteIBOWHOBOExtraTouches(TouchInfo.QORId, TouchInfo.TouchTypeFull, TouchInfo.SequenceNum.Value, Helper.CurrentUser.UserName, TouchInfo.UnitName);
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " Touch has been completed from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                        TouchInfo.SLTouchStatus = WATouchStatus.Completed.ToString();
                    }
                    else
                    {
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " Touch status has been updated to completed from FacilityApp but not completed in SiteLink", Helper.CurrentUser.UserName, string.Empty, 0);
                    }
                    TouchInfo.CompletedDateTime = DateTime.Now;

                    break;
                case "OBO":
                    if (settings.CanUpdateStatusInSL)
                    {
                        //Complete touch in SL
                        smd.CompleteIBOWHOBOExtraTouches(TouchInfo.QORId, TouchInfo.TouchTypeFull, TouchInfo.SequenceNum.Value, Helper.CurrentUser.UserName, TouchInfo.UnitName);
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " Touch has been completed from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                        TouchInfo.SLTouchStatus = WATouchStatus.Completed.ToString();
                    }
                    else
                    {
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " Touch status has been updated to completed from FacilityApp but not completed in SiteLink", Helper.CurrentUser.UserName, string.Empty, 0);
                    }
                    TouchInfo.CompletedDateTime = DateTime.Now;

                    break;
                case "TI":
                    if (settings.CanUpdateStatusInSL)
                    {
                        //Before move in TI touch, add new unit to destination location
                        //string unitTransferMsg = smd.TransferUnitFromOrigToDestForLDM(Helper.Corpcode, "OriginLocation", "DestLocationCode", Helper.username, Helper.password, TouchInfo.UnitName, "000016", false);

                        string Guid = (new Guid()).ToString();
                        string ActualUnitName = TouchInfo.UnitName;
                        //Complete touch in SL
                        smd.CompletTIAndMoveInTITouch(TouchInfo.QORId, TouchInfo.UnitName, Helper.CurrentUser.UserName, TouchInfo.StarsUnitId.Value, TouchInfo.StarsId.Value);
                        // smd.AddUnitCompletTIAndMoveInTITouch(TouchInfo.QORId, TouchInfo.StarsId.Value.ToString(), TouchInfo.StarsUnitId.Value, Helper.CurrentUser.UserName, out ActualUnitName);

                        //once you movein TI touch, then get the actual unit number and assign to TouchInfo object
                        TouchInfo.UnitName = smd.GetAddedUnit(TouchInfo.QORId); //ActualUnitName

                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " Touch has been completed from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                        TouchInfo.SLTouchStatus = WATouchStatus.Completed.ToString();
                    }
                    else
                    {
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " Touch status has been updated to completed from FacilityApp but not completed in SiteLink", Helper.CurrentUser.UserName, string.Empty, 0);
                    }

                    TouchInfo.CompletedDateTime = DateTime.Now;

                    break;
                case "TO":
                    if (settings.CanUpdateStatusInSL)
                    {
                        //Complete touch in SL
                        smd.CompletTOAndMoveOutTOTouch(TouchInfo.QORId, Helper.CurrentUser.UserName, TouchInfo.UnitName);
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " Touch has been completed from FacilityApp", Helper.CurrentUser.UserName, string.Empty, 0);
                        TouchInfo.SLTouchStatus = WATouchStatus.Completed.ToString();
                    }
                    else
                    {
                        lc.InsertActivityLog((TouchInfo.StarsId.HasValue ? TouchInfo.StarsId.Value : 0), TouchInfo.QORId, "Touch status updated", TouchInfo.TouchTypeFull + " Touch status has been updated to completed from FacilityApp but not completed in SiteLink", Helper.CurrentUser.UserName, string.Empty, 0);
                    }

                    TouchInfo.CompletedDateTime = DateTime.Now;
                    break;
                default:
                    throw new Exception("You cannot complete this touch from this screen.");
            }

        }

        public string GetPRGQuoteOrderLogData(int qorid)
        {
            LocalComponent oLocalComponent = new LocalComponent();
            DataSet oDataSet = oLocalComponent.GetPRGQuoteOrderLogData(qorid);
            string purchaseOrder = string.Empty;

            if (oDataSet.Tables.Count > 0 && oDataSet.Tables[0].Rows.Count > 0)
                purchaseOrder = oDataSet.Tables[0].Rows[0]["PurchaseOrder"].ToString();

            return (String.IsNullOrWhiteSpace(purchaseOrder) ? "N/A" : purchaseOrder);
        }

        public bool IsValidUnitName(string unitName)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic(Helper.CurrentUser.UserName);
            return smd.ValidateUnitByUnitName(Helper.CurrentSiteInfo.LocationCode, unitName) > 0;
        }

        private string GetTripNumber(int qorid)
        {
            //LocalComponent oLocalComponent = new LocalComponent();
            //DataSet oDataSet = oLocalComponent.GetTripDetails(T26ContainerId);

            SMDBusinessLogic smd = new SMDBusinessLogic(Helper.CurrentUser.UserName);

            string tripNumber = "";

            try
            {
                tripNumber = smd.GetTripNumber(qorid);
            }
            catch (Exception ex)
            {
                tripNumber = "";
            }

            return (String.IsNullOrWhiteSpace(tripNumber) ? "N/A" : tripNumber);
        }

        //SPERP-TODO-CTRMV , Remove after done with ESB
        //private string GetTripNumber(int T26ContainerId)
        //{
        //    LocalComponent oLocalComponent = new LocalComponent();
        //    DataSet oDataSet = oLocalComponent.GetTripDetails(T26ContainerId);
        //    string tripNumber = string.Empty;

        //    if (oDataSet.Tables.Count > 0 && oDataSet.Tables[0].Rows.Count > 0)
        //        tripNumber = oDataSet.Tables[0].Rows[0]["TripNumber"].ToString();

        //    return (String.IsNullOrWhiteSpace(tripNumber) ? "N/A" : tripNumber);
        //}

        private void LoadDriverDetails(WATouchInfo touchInfo)
        {
            LocalComponent oLocalComponent = new LocalComponent();
            DataSet oDataSet = oLocalComponent.GetLoadDriverDetails(touchInfo.QORId, touchInfo.TouchType, touchInfo.SequenceNum.Value);

            if (oDataSet.Tables.Count > 0 && oDataSet.Tables[0].Rows.Count > 0)
            {
                touchInfo.DriverID = oDataSet.Tables[0].Rows[0]["DriverID"].ToString();
                touchInfo.DriverName = oDataSet.Tables[0].Rows[0]["DriverName"].ToString();
            }
        }

        private void GetUnitDescription(WATouchInfo touchInfo)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic(Helper.CurrentUser.UserName);
            touchInfo.UnitDescription = smd.GetUnitTypeNameByUnitName(Helper.CurrentSiteInfo.LocationCode, touchInfo.UnitName);
        }

        private void LoadReqDataAndSendEmailThroughSendGrid(WATouchInfo touchInfo, int[] emailTemplateIDs, int userID)
        {
            touchInfo.PurchaseOrder = GetPRGQuoteOrderLogData(touchInfo.QORId);

            ////Get Trip Id for LDM orders
            //if (touchInfo.StarsUnitId.HasValue && touchInfo.StarsUnitId.Value > 0)
            //touchInfo.TripNumber = GetTripNumber(touchInfo.QORId, touchInfo.StarsUnitId.Value);

            if (touchInfo.IsLdmOrder)
                touchInfo.TripNumber = GetTripNumber(touchInfo.QORId);

            //Load driver details for this touch
            LoadDriverDetails(touchInfo);

            //Get UnitTypeName by unit name from SL
            GetUnitDescription(touchInfo);

            SendEmailThroughSendGrid(touchInfo, emailTemplateIDs, userID);
        }

        private void SendEmailThroughSendGrid(WATouchInfo touchInfo, int[] emailTemplateIDs, int userID)
        {
            foreach (int emailTemplateID in emailTemplateIDs)
            {
                SendEmailToSingleTemplate(touchInfo, emailTemplateID, userID);
            }
        }

        private void SendEmailToSingleTemplate(WATouchInfo touchInfo, int emailTemplateID, int userID)
        {
            FAEmailHistoryLog oFAEmailLog = new FAEmailHistoryLog();
            try
            {
                var localComponent = new LocalComponent();
                string strEmail = string.Empty;

                //getting template details based on template id.
                var emailTemplateDetails = prsService.GetEmailTemplateDetails(emailTemplateID);

                MailBaseRequest emailRequest = new MailBaseRequest();
                emailRequest.TemplateId = emailTemplateDetails.TemplateID;


                emailRequest.apiKey = gs.SendGridAPIKey;
                emailRequest.From = new EmailAddressRequest(emailTemplateDetails.FromEmail);

                emailRequest.QORID = touchInfo.QORId;
                emailRequest.LoginId = Helper.CurrentUser.UserName;

                foreach (string str in emailTemplateDetails.ToEmails.Split(';'))
                {
                    string toEmailId = str;

                    if (toEmailId.Equals("{{FacilityOM}}", System.StringComparison.OrdinalIgnoreCase))
                    {
                        toEmailId = Helper.GetFacilityOMEmailId(Helper.CurrentSiteInfo.LocationCode);
                    }

                    emailRequest.TOs.Add(new EmailAddressRequest(toEmailId));
                }

                if (emailTemplateDetails.CCEmail != null)
                {
                    foreach (string str in emailTemplateDetails.CCEmail.Split(';'))
                    {
                        string ccEmailId = str;

                        if (ccEmailId.Equals("{{FacilityOM}}", System.StringComparison.OrdinalIgnoreCase))
                        {
                            ccEmailId = Helper.GetFacilityOMEmailId(Helper.CurrentSiteInfo.LocationCode);
                        }

                        emailRequest.CCs.Add(new EmailAddressRequest(ccEmailId));
                    }
                }

                if (emailTemplateDetails.BCCEmail != null)
                {
                    foreach (string str in emailTemplateDetails.BCCEmail.Split(';'))
                    {
                        string bccEmailId = str;

                        if (bccEmailId.Equals("{{FacilityOM}}", System.StringComparison.OrdinalIgnoreCase))
                        {
                            bccEmailId = Helper.GetFacilityOMEmailId(Helper.CurrentSiteInfo.LocationCode);
                        }

                        emailRequest.BCCs.Add(new EmailAddressRequest(bccEmailId));
                    }
                }

                emailRequest.Tags = LoadEmailTags(touchInfo);

                //Creating email log object
                oFAEmailLog.ApplicationName = "Facility App";
                oFAEmailLog.IPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                oFAEmailLog.UserID = userID;
                oFAEmailLog.Subject = "WareHouse Touch Complete - Facility App";
                oFAEmailLog.ToEmailIDs = emailTemplateDetails.ToEmails;
                oFAEmailLog.FromEmailID = emailTemplateDetails.FromEmail;
                oFAEmailLog.MethodName = "SendTouchCompletionEmail";
                oFAEmailLog.EmailTypeID = (int)EmailTypes.Scheduled;//need to change based on status
                oFAEmailLog.QORID = touchInfo.QORId;
                oFAEmailLog.TouchType = touchInfo.TouchType;
                oFAEmailLog.SLSeqNo = touchInfo.SequenceNum;
                oFAEmailLog.TemplateID = emailTemplateID;
                //send email
                //get email it from callblast method
                oFAEmailLog.StartTime = DateTime.Now;

                emailRequest.sgCat = new SendGridCategories(emailTemplateDetails.Application, SendGridFunctionType.TouchDashboard, emailTemplateDetails.TemplateID, emailTemplateDetails.ToEmails);

                ThreadPool.QueueUserWorkItem(new WaitCallback(SendSGAsyncEmail), emailRequest);
                oFAEmailLog.EndTime = DateTime.Now;
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }
            finally
            {
                prsService.InsertEmailLog(oFAEmailLog);
            }
        }

        private static void SendSGAsyncEmail(object mailMsg)
        {
            MailBaseRequest ti = (MailBaseRequest)mailMsg;
            Task<MailResponse> obj = SendGridManager.Execute(ti);
        }

        private static Hashtable LoadEmailTags(WATouchInfo touchInfo)
        {
            LocalComponent lc = new LocalComponent();
            var ht = new Hashtable();

            ht.Add(EmailTagsConstant.FirstName, touchInfo.FirstName);
            ht.Add(EmailTagsConstant.LastName, touchInfo.LastName);
            ht.Add(EmailTagsConstant.QORID, touchInfo.QORId);

            if (touchInfo.TouchType == "TI")
            {
                ht.Add(EmailTagsConstant.TouchType, "LDM In");
            }
            else if (touchInfo.TouchType == "TO")
            {
                ht.Add(EmailTagsConstant.TouchType, "LDM Out");
            }
            else
            {
                ht.Add(EmailTagsConstant.TouchType, touchInfo.TouchType);
            }

            ht.Add(EmailTagsConstant.CompletedDateTime, touchInfo.CompletedDateTime.HasValue ? touchInfo.CompletedDateTime.Value.ToString("MM/dd/yyyy hh:mm tt") : "");
            ht.Add(EmailTagsConstant.DeliveryDate, touchInfo.ScheduledDate.HasValue ? touchInfo.ScheduledDate.Value.ToString("MM/dd/yyyy") : "");
            ht.Add(EmailTagsConstant.Comments, touchInfo.Comments);

            ht.Add(EmailTagsConstant.CustomerName, touchInfo.FirstName + " " + touchInfo.LastName);
            ht.Add(EmailTagsConstant.TouchStatus, touchInfo.TouchStatus);

            ht.Add(EmailTagsConstant.CustomerPhoneNumber, touchInfo.Phonenumber == null ? string.Empty : touchInfo.Phonenumber);
            ht.Add(EmailTagsConstant.DriverID, touchInfo.DriverID == null ? string.Empty : touchInfo.DriverID);
            ht.Add(EmailTagsConstant.DriverName, touchInfo.DriverName == null ? string.Empty : touchInfo.DriverName);
            ht.Add(EmailTagsConstant.PurchaseOrder, touchInfo.PurchaseOrder == null ? string.Empty : touchInfo.PurchaseOrder);
            ht.Add(EmailTagsConstant.TripNumber, touchInfo.TripNumber);

            ht.Add(EmailTagsConstant.UnitNumber, touchInfo.UnitName);
            ht.Add(EmailTagsConstant.DeliveredUnitNumber, touchInfo.UnitName);
            ht.Add(EmailTagsConstant.UnitDescription, touchInfo.UnitDescription);

            ht.Add(EmailTagsConstant.FacilityName, Helper.CurrentSiteInfo.SiteName);//Get it from currentsiteinfo session
            ht.Add(EmailTagsConstant.LocationCode, Helper.CurrentSiteInfo.LocationCode);//Get it from currentsiteinfo session
            ht.Add(EmailTagsConstant.TouchCompleteConfirm, "Touch Complete Confirmation");

            ht.Add(EmailTagsConstant.Address1, touchInfo.CustAddress1 == null ? string.Empty : touchInfo.CustAddress1);
            ht.Add(EmailTagsConstant.Address2, touchInfo.CustAddress2 == null ? string.Empty : touchInfo.CustAddress2);
            ht.Add(EmailTagsConstant.City, touchInfo.CustCity == null ? string.Empty : touchInfo.CustCity);
            ht.Add(EmailTagsConstant.CustRegion, touchInfo.CustRegion == null ? string.Empty : touchInfo.CustRegion);
            ht.Add(EmailTagsConstant.Zip, touchInfo.CustZip == null ? string.Empty : touchInfo.CustZip);

            ht.Add(EmailTagsConstant.FromFirstName, touchInfo.FromFirstName == null ? string.Empty : touchInfo.FromFirstName);
            ht.Add(EmailTagsConstant.FromLastName, touchInfo.FromLastName == null ? string.Empty : touchInfo.FromLastName);
            ht.Add(EmailTagsConstant.FromPhone, touchInfo.FromPhone == null ? string.Empty : touchInfo.FromPhone);
            ht.Add(EmailTagsConstant.FromAddr1, touchInfo.FromAddr1 == null ? string.Empty : touchInfo.FromAddr1);
            ht.Add(EmailTagsConstant.FromAddr2, touchInfo.FromAddr2 == null ? string.Empty : touchInfo.FromAddr2);
            ht.Add(EmailTagsConstant.FromCity, touchInfo.FromCity == null ? string.Empty : touchInfo.FromCity);
            ht.Add(EmailTagsConstant.FromState, touchInfo.FromState == null ? string.Empty : touchInfo.FromState);
            ht.Add(EmailTagsConstant.FromZip, touchInfo.FromZip == null ? string.Empty : touchInfo.FromZip);

            ht.Add(EmailTagsConstant.ToFirstName, touchInfo.ToFirstName == null ? string.Empty : touchInfo.ToFirstName);
            ht.Add(EmailTagsConstant.ToLastName, touchInfo.ToLastName == null ? string.Empty : touchInfo.ToLastName);
            ht.Add(EmailTagsConstant.ToPhone, touchInfo.ToPhone == null ? string.Empty : touchInfo.ToPhone);
            ht.Add(EmailTagsConstant.ToAddr1, touchInfo.ToAddr1 == null ? string.Empty : touchInfo.ToAddr1);
            ht.Add(EmailTagsConstant.ToAddr2, touchInfo.ToAddr2 == null ? string.Empty : touchInfo.ToAddr2);
            ht.Add(EmailTagsConstant.ToCity, touchInfo.ToCity == null ? string.Empty : touchInfo.ToCity);
            ht.Add(EmailTagsConstant.ToState, touchInfo.ToState == null ? string.Empty : touchInfo.ToState);
            ht.Add(EmailTagsConstant.ToZip, touchInfo.ToZip == null ? string.Empty : touchInfo.ToZip);

            return ht;
        }

        public List<WATouchInfoHistory> GetTouchHistory(int QORID, string TouchType, int SeqNumber)
        {
            return this.prsService.GetTouchHistory(QORID, TouchType, SeqNumber);
        }
        #region ContainerStatus


        public List<FAWTTouchActions> GetActionsByTouchType(string TouchTypeID, int StatusID, DateTime TouchScheduleDate, int StoreNumber)
        {
            return prsService.GetActionsByTouchType(TouchTypeID, StatusID, TouchScheduleDate, StoreNumber);
        }

        public List<FAWTTouchActions> SearchContainerStatus(int StoreNumber, string StatusName)
        {
            return prsService.SearchContainerStatus(StoreNumber, StatusName);
        }

        public void ToggleActiveAndInactiveContainerStatus(int PKID, string Status, int UserId, int IsGlobalStatus, int StoreNumber)
        {
            prsService.ToggleActiveAndInactiveContainerStatus(PKID, Status, UserId, IsGlobalStatus, StoreNumber);
        }

        public void AddContainerStatus(FAWTTouchActions oContainerStatus, int UserId)
        {
            prsService.AddContainerStatus(oContainerStatus, UserId);
        }
        #endregion
    }
}
