﻿using System.Web;

namespace PRFacAppWF.CodeHelpers
{
    public class IVRItems
    {
        public string ZipFrom { get; set; }
        public string ZipTo { get; set; }
        public int ProgramId { get; set; }
        public int RowId { get; set; }

        public static IVRItems GetIVRItems()
        {
            if (HttpContext.Current.Session["IVRItems"] != null)
            {
                return (IVRItems)HttpContext.Current.Session["IVRItems"];
            }
            else
            {
                IVRItems ivrItems = new IVRItems();
                HttpContext.Current.Session["IVRItems"] = ivrItems;
                return ivrItems;
            }
        }

       
        public static void ReleaseFromSession()
        {
            HttpContext.Current.Session["IVRItems"] = null;
        }


    }
}
