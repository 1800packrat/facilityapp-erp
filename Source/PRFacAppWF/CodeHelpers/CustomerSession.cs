﻿using System;

namespace   PRFacAppWF.CodeHelpers
{
    public class CustomerSession
    {
        public string addr1,
                      addr2;

        public string altPhone;

        public string businessPhone;

        public string city;
        public string companyName;
        public string corpCode;
        public DateTime dateOfBirth;

        public string driverLicenseNumber;

        public string email;

        public string ext;

        public string facilityid;
        public string fax;
        public string firstName;
        public string gateCode;

        public bool isCommercialTenant, isCompanyTenant;
        public string lastName;
        public string licenseState;
        public string locationCode;
        public string middlename;
        public string mobilePhone;
        public string pager;
        public string password;
        public string phone;
        public string prefix;
        public string socialSecurityNumber;
        public string state;
        public string taxexempt;
        public string tenantId;
        public string tenantNotes;
        public string type;
        public string unit_status;
        public string userName;
        public string zip;
    }
}