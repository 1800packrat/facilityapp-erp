﻿namespace PRFacAppWF.CodeHelpers
{
    using PRFacAppWF.Repository.Services;
    using System;

    public class GlobalSettings
    {
        private readonly IPRSService prsService;

        public GlobalSettings()
        {
            if (prsService == null)
                prsService = new PRSService();
        }

        public DateTime FAWTSearchStartDate
        {
            get
            {
                DateTime dt = DateTime.Now;
                var noOfDaysToAdd = prsService.GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey.FAWTStartDate, Helper.CurrentSiteInfo.GlobalSiteNum);

                if (noOfDaysToAdd != null && !string.IsNullOrEmpty(noOfDaysToAdd.SettingsValue))
                {
                    int noOfDays = 0;
                    int.TryParse(noOfDaysToAdd.SettingsValue, out noOfDays);

                   dt = dt.AddDays(noOfDays);
                }

                return dt;
            }
        }

        public DateTime FAWTSearchEndDate
        {
            get
            {
                DateTime dt = DateTime.Now;
                var noOfDaysToAdd = prsService.GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey.FAWTEndDate, Helper.CurrentSiteInfo.GlobalSiteNum);

                if (noOfDaysToAdd != null && !string.IsNullOrEmpty(noOfDaysToAdd.SettingsValue))
                {
                    int noOfDays = 0;
                    int.TryParse(noOfDaysToAdd.SettingsValue, out noOfDays);

                    dt = dt.AddDays(noOfDays);
                }

                return dt;
            }
        }

        public string FAWTPRDispatch
        {
            get
            {
                var dispatchEmail = prsService.GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey.FAWTPRDispatch, Helper.CurrentSiteInfo.GlobalSiteNum);

                if (dispatchEmail != null)
                {
                    return dispatchEmail.SettingsValue;
                }

                return string.Empty;
            }
        }

        public string FAWTCustomerLoyalty
        {
            get
            {
                var clEmail = prsService.GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey.FAWTCustomerLoyalty, Helper.CurrentSiteInfo.GlobalSiteNum);

                if (clEmail != null)
                {
                    return clEmail.SettingsValue;
                }

                return string.Empty;
            }
        }

        public string FAWTCentralAdmin
        {
            get
            {
                var caEmail = prsService.GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey.FAWTCentralAdmin, Helper.CurrentSiteInfo.GlobalSiteNum);

                if (caEmail != null)
                {
                    return caEmail.SettingsValue;
                }

                return string.Empty;
            }
        }

        public int FAWTShowPriorNotCompletedTouchesDays
        {
            get
            {
                var caEmail = prsService.GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey.FAWTShowPriorNotCompletedTouchesDays, Helper.CurrentSiteInfo.GlobalSiteNum);

                if (caEmail != null)
                {
                    return Convert.ToInt32(caEmail.SettingsValue);
                }

                return 0;
            }
        }

        public string SendGridAPIKey
        {
            get {
                var sendGridKey = prsService.GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey.SendGridAPIKey, Helper.CurrentSiteInfo.GlobalSiteNum);

                if (sendGridKey != null)
                {
                    return sendGridKey.SettingsValue;
                }

                return string.Empty;
            }
        }

        public DateTime DieselPriceFilterBeginDate
        {
            get
            {
                DateTime dt = DateTime.Now;
                var noOfMonthsToAdd = prsService.GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey.DieselPriceFilterBeginDateInMonths);

                if (noOfMonthsToAdd != null && !string.IsNullOrEmpty(noOfMonthsToAdd.SettingsValue))
                {
                    int noOfMonths = 0;
                    int.TryParse(noOfMonthsToAdd.SettingsValue, out noOfMonths);

                    dt = dt.AddMonths(noOfMonths);
                }

                return dt;
            }
        }

        public DateTime DieselPriceFilterEndDate
        {
            get
            {
                DateTime dt = DateTime.Now;
                var noOfMonthsToAdd = prsService.GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey.DieselPriceFilterEndDateInMonths);

                if (noOfMonthsToAdd != null && !string.IsNullOrEmpty(noOfMonthsToAdd.SettingsValue))
                {
                    int noOfMonths = 0;
                    int.TryParse(noOfMonthsToAdd.SettingsValue, out noOfMonths);

                    dt = dt.AddDays(noOfMonths);
                }

                return dt;
            }
        }

        public decimal DieselPriceLocalTransSubsidyDefaultValue
        {
            get
            {
                var caEmail = prsService.GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey.DieselPriceLocalTransSubsidyDefaultValue);

                if (caEmail != null)
                {
                    return Convert.ToDecimal(caEmail.SettingsValue);
                }

                return (decimal)0;
            }
        }

        public bool IsGetLiveData
        {
            get
            {
                var isLiveData = prsService.GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey.IsGetLiveData);

                if (isLiveData != null)
                {
                    return Convert.ToBoolean(isLiveData.SettingsValue);
                }

                return false;
            }
        }
    }
}