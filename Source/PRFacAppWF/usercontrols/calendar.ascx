﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="calendar.ascx.cs" Inherits="PRFacAppWF.usercontrols.calendar" %>
<asp:UpdatePanel runat="server" ID="upCal" UpdateMode="Conditional">
    <ContentTemplate>
        <script src="../Scripts/DP_DateExtensions.js" type="text/javascript"></script>
        <script src="../Scripts/kendo.all.min.js"></script>
        <script language="javascript" type="text/javascript">
            $(document).ready(function () {
                LoadHoverPanel();

                // Highlight the selected date
                $('td[data-isdateselected=1] a').each(function () {
                    $(this).css('font-weight', 'bold').css('font-size', '18px');
                });
            });

            function LoadHoverPanel() {
                // Formatting calender anchors
                $('#calSchedule_upCal').find('a').css('text-decoration', 'none');

                $('td').each(function () {
                    var attr = $(this).attr('data-regularmiles');
                    var td = $(this);
                    if (typeof attr !== typeof undefined && attr !== false) {


                        // Remove title for the anchor inside td
                        $(td).find('a').removeAttr('title');
                        $('table[id$=calScheduleAnyTime]').removeAttr('title');
                        $('table[id$=calScheduleAM]').removeAttr('title');
                        $('table[id$=calSchedulePM]').removeAttr('title');


                        $(td).on("mouseenter", function () {
                            var facilitymiles = 0;
                            var bookedmiles = 0;
                            var regularmiles = $(td).attr('data-regularmiles');
                            var salesregularmiles = $(td).attr('data-salesregularmiles');
                            var serviceregularmiles = $(td).attr('data-serviceregularmiles');
                            var reservedmiles = 0;
                            if ($(td).attr("data-reservedmiles"))
                                reservedmiles = $(td).attr('data-reservedmiles');

                            if ($(td).attr("data-facilitymiles"))
                                facilitymiles = $(td).attr('data-facilitymiles');
                            if ($(td).attr("data-bookedmiles"))
                                bookedmiles = $(td).attr('data-bookedmiles');

                            var tripmiles = $(td).attr('data-tripmiles');
                            var roundtripmiles = $(td).attr('data-roundtripmiles');
                            var touchmiles = $(td).attr('data-touchmiles');
                            var date = $(td).attr('data-date');

                            var data = [
                                        {
                                            date: date,
                                            regular: parseFloat(regularmiles).toFixed(0),
                                            salesregular: parseFloat(salesregularmiles).toFixed(0),
                                            serviceregular: parseFloat(serviceregularmiles).toFixed(0),
                                            reserved: parseFloat(reservedmiles).toFixed(0),
                                            facility: parseFloat(facilitymiles).toFixed(0),
                                            booked: parseFloat(bookedmiles).toFixed(0),
                                            trip: parseFloat(tripmiles).toFixed(0),
                                            touch: parseFloat(touchmiles).toFixed(0),
                                            roundtrip: parseFloat(roundtripmiles).toFixed(0)
                                        }
                            ];

                            var templateContent = $("#hoverTemplate").html();
                            var template = kendo.template(templateContent);
                            var result = kendo.render(template, data);
                            $(td).append(jQuery(result));

                            // Adjustments for AM/PM
                            if ($(td).closest('table').attr('id').indexOf('calSchedule_calSchedulePM') != -1) {

                                // CHANGE THE TITLE TO SHOW FACILITY PM MILES
                                var span_facility_miles = $(this).find('span[id=span-facility-miles]').first();
                                var html = $(span_facility_miles).html().replace('Facility Miles', 'Facility PM Miles');
                                $(span_facility_miles).html(html);

                                // CHANGE THE TITLE TO SHOW BOOKED PM MILES
                                var span_booked_miles = $(this).find('span[id=span-booked-miles]').first();
                                var html = $(span_booked_miles).html().replace('Booked Miles', 'Booked PM Miles');
                                $(span_booked_miles).html(html);

                                // REMOVE RESERVE MILES, SERVICE REGULAR MILES, SALES REGULAR MILES

                                $(td).find('#span-reserved-miles').remove();
                                $(td).find('#span-service-regular-miles').remove();
                                $(td).find('#span-sales-regular-miles').remove();
                            }

                            if ($(td).closest('table').attr('id').indexOf('calSchedule_calScheduleAM') != -1) {
                                // CHANGE THE TITLE TO SHOW FACILITY PM MILES

                                var span_facility_miles = $(this).find('span[id=span-facility-miles]').first();
                                var html = $(span_facility_miles).html().replace('Facility Miles', 'Facility AM Miles');
                                $(span_facility_miles).html(html);

                                // CHANGE THE TITLE TO SHOW BOOKED AM MILES
                                var span_booked_miles = $(this).find('span[id=span-booked-miles]').first();
                                var html = $(span_booked_miles).html().replace('Booked Miles', 'Booked AM Miles');
                                $(span_booked_miles).html(html);

                                // REMOVE RESERVE MILES, SERVICE REGULAR MILES, SALES REGULAR MILES

                                $(td).find('#span-reserved-miles').remove();
                                $(td).find('#span-service-regular-miles').remove();
                                $(td).find('#span-sales-regular-miles').remove();
                            }
                            if ($(td).closest('table').attr('id').indexOf('calSchedule_calScheduleAnyTime') != -1) {
                                // REMOVE REGULAR MILES 
                                $(td).find('#span-regular-miles').remove();
                            }

                            // Set hover popup with the window and visible

                            var win = $('#hoverpanel');
                            if (win.length > 0) {

                                var screenwidth = $('body').width();
                                var screenheight = $('#divAll').height();
                                var width = win.width();
                                var height = win.height();
                                var pos = win.position();
                                var rightpos = width + pos.left;
                                if (rightpos > screenwidth) {
                                    var moveleft = rightpos - screenwidth + 10;
                                    win.css('margin-left', '-' + moveleft + 'px');
                                } else {
                                    win.css('margin-left', '0px');
                                }

                                //// Adjust top position 

                                //var totalheight = height + pos.top - 250;
                                //if (totalheight > screenheight) {
                                //    var movetop = totalheight - (2 * height) - 120;
                                //    win.css('margin-top', movetop + 'px');
                                //} else {
                                //    win.css('margin-top', '0px');
                                //}

                                // If it is not a supervisor remove booked miles & facility miles from hover popup & adjust height

                                if (!$(td).attr("data-facilitymiles")) {
                                    $('span[id=span-facility-miles]').remove();
                                }
                                if (!$(td).attr("data-bookedmiles")) {
                                    $('span[id=span-booked-miles]').remove();
                                }
                                if (!$(td).attr("data-reservedmiles")) {
                                    $('span[id=span-reserved-miles]').remove();
                                }
                            }


                        }).on("mouseleave", function () {
                            $("#hoverpanel").remove();
                        });

                    }
                });



            }

            function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }
            function ibtn_Clear_Call() {
                document.getElementById("<%=lblSchedule.ClientID%>").innerHTML = "";
            }
            function DENotification(scheduledate, deliverdate, ScheduleType, istxtInput) {

                var dDate = new Date(deliverdate);
                if (dDate.getFullYear() == "1901") {
                    document.getElementById('<%=scheduledate.ClientID%>').value = scheduledate;
                    alert(document.getElementById('<%=scheduledate.ClientID%>').value + '   ' + ScheduleType);
                    document.getElementById('<%=hdscheduletype.ClientID%>').value = ScheduleType;
                    document.getElementById('btnsave').disabled = !istxtInput;
                    document.getElementById('<%=hdIsUserEntered.ClientID%>').value = istxtInput;
                }
                else {
                    var sDate = new Date(scheduledate);
                 
                        document.getElementById('<%=scheduledate.ClientID%>').value = scheduledate;
                        alert(document.getElementById('<%=scheduledate.ClientID%>').value + '   ' + ScheduleType);
                        document.getElementById('<%=hdscheduletype.ClientID%>').value = ScheduleType;
                        document.getElementById('btnsave').disabled = !istxtInput;
                        document.getElementById('<%=hdIsUserEntered.ClientID%>').value = istxtInput;

                    
                }
                return false;
            }
            function SaveScheduleDate(istxtInput, scheduledate, ScheduleType, deliverydate, touchType, role) {
                if (!isDate(scheduledate) && istxtInput) {
                    alert('Invalid date format!Please enter in mm/dd/yyyy format.');
                    document.getElementById('btnsave').disabled = true;
                }
                else if (CheckRange(scheduledate, role)) {
                    if (scheduledate == "0") {
                        document.getElementById('btnsave').disabled = true;
                        alert("TOUCH BEING EDITED IS NOT AN INITIAL DELIVERY; NON-DELIVERY TOUCH CANNOT BE SCHEDULED ON RED DATE.");

                    }

                    if (touchType == "DE-Deliver Empty") {
                        DENotification(scheduledate, deliverydate, ScheduleType, deliverydate, istxtInput);
                        document.getElementById('<%=hdIsUserEntered.ClientID%>').value = istxtInput;

                    } else {
                        document.getElementById('<%=scheduledate.ClientID%>').value = scheduledate;
                        alert(document.getElementById('<%=scheduledate.ClientID%>').value + '   ' + ScheduleType);
                        document.getElementById('<%=hdscheduletype.ClientID%>').value = ScheduleType;
                        document.getElementById('btnsave').disabled = false;
                        document.getElementById('<%=hdIsUserEntered.ClientID%>').value = istxtInput;

                    }
                }
                return false;
            }
            function CheckRange(scheduledate, role) {
                var dte = new Date();

                if (new Date(scheduledate) < new Date(dte.toDateString())) {

                    var result = confirm('The date  ' + scheduledate + ' Is a past date. Do you want to save this? ');
                    if (result) {
                        return true;
                    } else {
                        document.getElementById('btnsave').disabled = true;
                        return false;

                    }

                }
                else {
                    return true;
                }
            }
            function isDate(txtDate) {
                var objDate,  // date object initialized from the txtDate string
 mSeconds, // txtDate in milliseconds
 day,      // day
 month,    // month
 year;     // year
                // date length should be 10 characters (no more no less)
                if (txtDate.length !== 10) {
                    return false;
                }
                // third and sixth character should be '/'
                if (txtDate.substring(2, 3) !== '/' || txtDate.substring(5, 6) !== '/') {
                    return false;
                }
                // extract month, day and year from the txtDate (expected format is mm/dd/yyyy)
                // subtraction will cast variables to integer implicitly (needed
                // for !== comparing)
                month = txtDate.substring(0, 2) - 1; // because months in JS start from 0
                day = txtDate.substring(3, 5) - 0;
                year = txtDate.substring(6, 10) - 0;
                // test year range
                if (year < 1000 || year > 3000) {
                    return false;
                }
                // convert txtDate to milliseconds
                mSeconds = (new Date(year, month, day)).getTime();
                // initialize Date() object from calculated milliseconds
                objDate = new Date();
                objDate.setTime(mSeconds);
                // compare input date and parts from Date() object
                // if difference exists then date isn't valid
                if (objDate.getFullYear() !== year ||
 objDate.getMonth() !== month ||
 objDate.getDate() !== day) {
                    return false;
                }

                // otherwise return true
                return true;
            }
            function funGetNoDateSelect(scheduledate) {
                var today = new Date();
                if (new Date(scheduledate) < new Date(today.toDateString())) {
                    alert('Cannot select past date');
                    return false;
                }
                alert('You are not authorized for this selection');
                return false;
            }
        </script>
        <script type="text/x-kendo-template" id="hoverTemplate">        
            <div id='hoverpanel' class='pop-cal-hoverpanel'>
                <span style='font-weight:bold'>Date : #: date # </span>
                <span id='span-facility-miles' >Facility Miles : #: facility # </span>
                <span id='span-booked-miles' >Booked Miles : #: booked # </span>
                <span id='span-sales-regular-miles'>Sales Available Miles : #: salesregular # </span>      
                <span id='span-service-regular-miles'>Service Available Miles : #: serviceregular # </span>      
                <span id='span-regular-miles'>Available Miles : #: regular # </span>      
                <span id='span-reserved-miles'>Reserved Miles : #: reserved # </span>
                <span>Total Miles : #: trip # </span>
                <span style='padding-left:7px'><b>&bull;</b> Round Trip Miles : #: roundtrip # </span>
                <span style='padding-left:7px'><b>&bull;</b> Touch Miles : #: touch # </span>                
            </div>
        </script>
        <input type="hidden" id="scheduledate" runat="server" />
        <input type="hidden" id="hdscheduletype" runat="server" />
        <input type="hidden" id="hdIsUserEntered" runat="server" />
        <input type="hidden" id="hdIsAvaialable" runat="server" />
        <input type="hidden" id="hdAllowSupLogin" runat="server" value="false" />
        <%--<input type="hidden" id="ColDate" runat="server" />--%>
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <ul class="form-list2" style="text-align: center">
            <li>
                <asp:Label runat="server" ID="lblSchedule" Font-Bold="true" Visible="false" Font-Names="Arial"
                    Font-Size="10pt"></asp:Label>
              <div class="input-control text lft-all">
                    <asp:DropDownList runat="server" ID="ddlYear">
                    </asp:DropDownList>
                    </div>
                      <div class="input-control text lft-all">
                    <asp:DropDownList runat="server" ID="ddlMonth">
                    </asp:DropDownList>
                </div>
                <asp:Button runat="server" ID="btnView" Text="Show" OnClick="btnView_Click" />
            </li>
            <div class="clear">
        </div>
        </ul>
        <div class="clear">
        </div>
        <div style="width: 100%;">
            <div style="float: left; margin: 0 2% 0 2%; width: 30%;">
                <h5 style="text-align: center;">
                    anytime</h5>
                <asp:Calendar ID="calScheduleAnyTime" EnableViewState="true" ShowNextPrevMonth="false" Font-Size="Smaller"
                    ShowTitle="false" runat="server" NextPrevFormat="FullMonth" OnDayRender="calScheduleAnyTime_DayRender1">
                </asp:Calendar>
            </div>
            <div style="float: left; margin: 0 2% 0 0; width: 30%;">
                <h5 style="text-align: center;">
                    am "8 A.M.-12 P.M."</h5>
                <asp:Calendar ID="calScheduleAM" ShowNextPrevMonth="false" ShowTitle="false" runat="server" Font-Size="Smaller"
                    NextPrevFormat="FullMonth" OnDayRender="calScheduleAnyTime_DayRender1"></asp:Calendar>
            </div>
            <div style="float: left; margin: 0 2% 0 0; width: 30%;">
                <h5 style="text-align: center;">
                    pm</h5>
                <asp:Calendar ID="calSchedulePM" ShowNextPrevMonth="false" ShowTitle="false" runat="server" Font-Size="Smaller"
                    NextPrevFormat="FullMonth" OnDayRender="calScheduleAnyTime_DayRender1"></asp:Calendar>
            </div>
        </div>
        <br />
        <asp:HiddenField runat="server" ID="hdnCallB" />
        <ajaxToolkit:ModalPopupExtender runat="server" ID="mpelogin" BackgroundCssClass="modalBackground"
            TargetControlID="hdnCallB" PopupControlID="pnlLogin">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel runat="server" ID="pnlLogin" BackColor="White" CssClass="HidePanel">
            <div class="message-dialog bg-color-oliveDark" style="color: #fff;">
                <div style="float: right">
                    <asp:Button ID="btnCloseLoginPopup" runat="server" Text="X" Width="30" OnClick="btnCloseLoginPopup_Click" />
                </div>
                <div class="txtHead02">
                    Supervisor/Admin Login
                </div>
                <div class="tableHolderPopUp" style="width: 280px; text-align: center;">
                    <table width="100%" border="0" cellpadding="5" cellspacing="0">
                        <tr>
                            <td>
                                First Name :
                            </td>
                            <td align="left">
                                <asp:TextBox runat="server" ID="txtFirstName" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="rfFname" ValidationGroup="login" ErrorMessage="*"
                                    ControlToValidate="txtFirstName"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Last Name :
                            </td>
                            <td align="left">
                                <asp:TextBox runat="server" ID="txtLastName" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ErrorMessage="*"
                                    ValidationGroup="login" ControlToValidate="txtLastName"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Password :
                            </td>
                            <td align="left">
                                <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" Text="skonanki"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ErrorMessage="*"
                                    ValidationGroup="login" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td align="left">
                                <asp:ImageButton runat="server" ID="btnLogin" text="Login" ValidationGroup="login"
                                    ImageUrl="~/images/btn_login.jpg" AlternateText="Login" OnClick="btnLogin_Click" />
                                <input type="hidden" id="Hidden1" runat="server" value="false" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Label runat="server" ForeColor="Red" Font-Bold="true" ID="lblErrDisp"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
