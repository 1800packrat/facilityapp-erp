﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PR.BusinessLogic;
using PR.Entities;
using PRFacAppWF.CodeHelpers;

namespace PRFacAppWF.usercontrols
{
    public partial class calendar : System.Web.UI.UserControl
    {
        private DataSet _AvailableDates;
        private string _DateColumnName;
        private DateTime _DefaultSelectedDate = DateTime.Now;
        private DateTime deliveryDate;
        private SMDBusinessLogic smdObject;
        protected const String BookAction = "Allow Booking";
        protected const String ViewBookedMiles = "View Booked Miles";
        protected const String ViewFacilityMiles = "View Facility Miles";
        protected const String ViewReservedMiles = "View Reserved Miles";

        public DateTime DefaultSelectedDate
        {
            get { return _DefaultSelectedDate; }
            set { _DefaultSelectedDate = value; }
        }

        public DataSet AvailableDates
        {
            get { return _AvailableDates; }
            set { _AvailableDates = value; }
        }

        public string DateColumnName
        {
            get { return _DateColumnName; }
            set { _DateColumnName = value; }
        }

        public string scheduleType
        {
            get { return hdscheduletype.Value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                smdObject = new SMDBusinessLogic(Session["UserId"] != null ? Convert.ToString(Session["UserId"]) : "");
                deliveryDate = smdObject.GetPreferredDeliveryDate(Convert.ToInt32(Session["qorid"]));
                //  txtSchDate.Attributes.Add("onblur", "return SaveScheduleDate(true,this.value,'AnyTime','" + deliveryDate + "','" + Session["touchType"].ToString() + "','" + Session["RoleId"].ToString() + "');");
                calScheduleAM.SelectedDate = deliveryDate;
                calSchedulePM.SelectedDate = deliveryDate;
                calScheduleAnyTime.SelectedDate = deliveryDate;


                if (!IsPostBack)
                {
                    LoadScheduleCalendar(Convert.ToDateTime(Request.QueryString["date"]));
                    calScheduleAnyTime.VisibleDate = _DefaultSelectedDate;
                    calScheduleAM.VisibleDate = _DefaultSelectedDate;
                    calSchedulePM.VisibleDate = _DefaultSelectedDate;
                    //  lblMonth.Text = GetMonthName(calScheduleAnyTime.VisibleDate.Month) + " " +
                    //calScheduleAnyTime.VisibleDate.Year;
                    BindMonths(); BindDDLYears();
                }
            }
            //catch (NullReferenceException nex)
            //{
            //    Helper.LogError(nex); ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertScript", "alert('An error has occurred in this application.Please login again');", true);

            //}
            catch (Exception ex)
            {
                // Helper.LogError(ex);
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript", "alert('" + ex.Message + "');", true);
            }
        }

        public void BindCalender()
        {
            calScheduleAnyTime.DataBind();
        }

        //protected void ibtnPrev_Click(object sender, ImageClickEventArgs e)
        //{
        //    if (calScheduleAnyTime.VisibleDate.Month == 1)
        //    {
        //        calScheduleAnyTime.VisibleDate = new DateTime(calScheduleAnyTime.VisibleDate.Year - 1, 12, 1);
        //        calScheduleAM.VisibleDate = new DateTime(calScheduleAM.VisibleDate.Year - 1, 12, 1);
        //        calSchedulePM.VisibleDate = new DateTime(calSchedulePM.VisibleDate.Year - 1, 12, 1);
        //    }
        //    else
        //    {
        //        calScheduleAnyTime.VisibleDate = new DateTime(calScheduleAnyTime.VisibleDate.Year,
        //                                                      calScheduleAnyTime.VisibleDate.Month - 1, 1);
        //        calScheduleAM.VisibleDate = new DateTime(calScheduleAM.VisibleDate.Year,
        //                                                 calScheduleAM.VisibleDate.Month - 1, 1);
        //        calSchedulePM.VisibleDate = new DateTime(calSchedulePM.VisibleDate.Year,
        //                                                 calSchedulePM.VisibleDate.Month - 1, 1);
        //    }
        //    lblMonth.Text = GetMonthName(calScheduleAnyTime.VisibleDate.Month) + " " +
        //                    calScheduleAnyTime.VisibleDate.Year;
        //}

        protected string GetMonthName(int Month)
        {
            string MonthName = "";
            switch (Month)
            {
                case 1:
                    MonthName = "JANUARY";
                    break;
                case 2:
                    MonthName = "FEBRUARY";
                    break;
                case 3:
                    MonthName = "MARCH";
                    break;
                case 4:
                    MonthName = "APRIL";
                    break;
                case 5:
                    MonthName = "MAY";
                    break;
                case 6:
                    MonthName = "JUNE";
                    break;
                case 7:
                    MonthName = "JULY";
                    break;
                case 8:
                    MonthName = "AUGUST";
                    break;
                case 9:
                    MonthName = "SEPTEMBER";
                    break;
                case 10:
                    MonthName = "OCTOBER";
                    break;
                case 11:
                    MonthName = "NOVEMBER";
                    break;
                case 12:
                    MonthName = "DECEMBER";
                    break;
            }

            return MonthName;
        }

        //protected void ibtnNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    if (calScheduleAnyTime.VisibleDate.Month == 12)
        //    {
        //        calScheduleAnyTime.VisibleDate = new DateTime(calScheduleAnyTime.VisibleDate.Year + 1, 1,
        //                                                      1);
        //        calScheduleAM.VisibleDate = new DateTime(calScheduleAM.VisibleDate.Year + 1, 1,
        //                                                 1);
        //        calSchedulePM.VisibleDate = new DateTime(calSchedulePM.VisibleDate.Year + 1, 1,
        //                                                1);
        //    }
        //    else
        //    {
        //        calScheduleAnyTime.VisibleDate = new DateTime(calScheduleAnyTime.VisibleDate.Year,
        //                                                      calScheduleAnyTime.VisibleDate.Month + 1,
        //                                                      1);
        //        calScheduleAM.VisibleDate = new DateTime(calScheduleAM.VisibleDate.Year,
        //                                                 calScheduleAM.VisibleDate.Month + 1,
        //                                                 1);
        //        calSchedulePM.VisibleDate = new DateTime(calSchedulePM.VisibleDate.Year,
        //                                                 calSchedulePM.VisibleDate.Month + 1,
        //                                                 1);
        //    }

        //  //  lblMonth.Text = GetMonthName(calScheduleAnyTime.VisibleDate.Month) + " " +
        //                    calScheduleAnyTime.VisibleDate.Year;
        //}

        //string _AvailabilityLevelColumnName;
        //public string AvailabilityLevelColumnName
        //{
        //    get
        //    {
        //        return _AvailabilityLevelColumnName;
        //    }
        //    set
        //    {
        //        _AvailabilityLevelColumnName = value;
        //    }
        //}
        public DateTime GetScheduleDate()
        {
            return Convert.ToDateTime(scheduledate.Value);
        }

        public bool IsDateUserInput()
        {
            return Convert.ToBoolean(hdIsUserEntered.Value);
        }

        protected void calScheduleAnyTime_DayRender1(object sender, DayRenderEventArgs e)
        {
            //bool flag = false;
            //string IsRed = "";
            var calVar = (Calendar)sender;
            string ScheduleType = "";
            if (calVar.ID == "calScheduleAnyTime")
            {
                ScheduleType = "AnyTime";
            }
            else if (calVar.ID == "calScheduleAM")
            {
                ScheduleType = "AM";
            }
            else if (calVar.ID == "calSchedulePM")
            {
                ScheduleType = "PM";
            }

            if (_AvailableDates != null)
            {
                if ((_AvailableDates.Tables[ScheduleType].Rows.Count > 0))
                {
                    var dr = _AvailableDates.Tables[ScheduleType].Rows.OfType<DataRow>().FirstOrDefault(d => Convert.ToDateTime(d[_DateColumnName].ToString()).ToShortDateString() == e.Day.Date.ToShortDateString());

                    if (dr != null)
                    {
                        if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Green))
                        {
                            e.Cell.CssClass = "cellgreen";
                            if (!CheckIfRoleIsAllowed(Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Green), BookAction))
                                e.Cell.Attributes.Add("onClick", " return funGetNoDateSelect('" + e.Day.Date.ToShortDateString() + "');");
                            else
                            {
                                if (Session["touchType"].ToString() == "DE-Deliver Empty")
                                {
                                    e.Cell.Attributes.Add("onClick",
                                                         "return DENotification('" + e.Day.Date + "','" +
                                                          deliveryDate + "','" +
                                                          ScheduleType + "','false');");
                                }
                                else
                                {
                                    e.Cell.Attributes.Add("onClick",
                                                          "return SaveScheduleDate(false,'" +
                                                          string.Format("{0:ddd, MMM d, yyyy}", e.Day.Date) + "','" +
                                                          ScheduleType + "','" +
                                                          deliveryDate + "','" + Session["touchType"].ToString() + "');");
                                }
                            }
                        }
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Red))
                        {
                            e.Cell.CssClass = "cellred";
                            if (!CheckIfRoleIsAllowed(Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Red), BookAction))
                                e.Cell.Attributes.Add("onClick", " return funGetNoDateSelect('" + e.Day.Date.ToShortDateString() + "');");
                            else
                            {
                                string resval = (((Convert.ToDateTime(e.Day.Date.ToString()) - DateTime.Now).TotalDays > 1) ? "true" : "false");
                                e.Cell.Attributes.Add("onClick",
                                                          "return SaveScheduleDate(false,'" +
                                                          string.Format("{0:ddd, MMM d, yyyy}", e.Day.Date) + "','" +
                                                          ScheduleType + "','" +
                                                          deliveryDate + "','" + Session["touchType"].ToString() + "');");
                            }
                        }
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Blue))
                        {
                            e.Cell.CssClass = "cellblue";
                            if (!CheckIfRoleIsAllowed(Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Blue), BookAction))
                                e.Cell.Attributes.Add("onClick", " return funGetNoDateSelect('" + e.Day.Date.ToShortDateString() + "');");
                            else
                            {
                                string resval = (((Convert.ToDateTime(e.Day.Date.ToString()) - DateTime.Now).TotalDays > 1) ? "true" : "false");

                                e.Cell.Attributes.Add("onClick",
                                                          "return SaveScheduleDate(false,'" +
                                                          string.Format("{0:ddd, MMM d, yyyy}", e.Day.Date) + "','" +
                                                          ScheduleType + "','" +
                                                          deliveryDate + "','" + Session["touchType"].ToString() + "');");
                            }
                        }
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Black))
                        {
                            e.Cell.CssClass = "cellblack";
                            e.Cell.ForeColor = Color.White;
                            if (!CheckIfRoleIsAllowed(Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Black), BookAction))
                                e.Cell.Attributes.Add("onClick", " return funGetNoDateSelect('" + e.Day.Date.ToShortDateString() + "');");
                            else
                            {
                                string resval = (((Convert.ToDateTime(e.Day.Date.ToString()) - DateTime.Now).TotalDays > 1) ? "true" : "false");

                                e.Cell.Attributes.Add("onClick",
                                                          "return SaveScheduleDate(false,'" +
                                                          string.Format("{0:ddd, MMM d, yyyy}", e.Day.Date) + "','" +
                                                          ScheduleType + "','" +
                                                          deliveryDate + "','" + Session["touchType"].ToString() + "');");
                            }
                        }
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Orange))
                        {
                            e.Cell.CssClass = "cellorange";
                            if (!CheckIfRoleIsAllowed(Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Orange), BookAction))
                                e.Cell.Attributes.Add("onClick", " return funGetNoDateSelect('" + e.Day.Date.ToShortDateString() + "');");
                            else
                            {
                                string resval = (((Convert.ToDateTime(e.Day.Date.ToString()) - DateTime.Now).TotalDays > 1) ? "true" : "false");

                                e.Cell.Attributes.Add("onClick",
                                                          "return SaveScheduleDate(false,'" +
                                                          string.Format("{0:ddd, MMM d, yyyy}", e.Day.Date) + "','" +
                                                          ScheduleType + "','" +
                                                          deliveryDate + "','" + Session["touchType"].ToString() + "');");
                            }
                        }
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Purple))
                        {
                            e.Cell.CssClass = "cellpurple";
                            if (!CheckIfRoleIsAllowed(Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Purple), BookAction))
                                e.Cell.Attributes.Add("onClick", " return funGetNoDateSelect('" + e.Day.Date.ToShortDateString() + "');");
                            else
                            {
                                string resval = (((Convert.ToDateTime(e.Day.Date.ToString()) - DateTime.Now).TotalDays > 1) ? "true" : "false");

                                e.Cell.Attributes.Add("onClick",
                                                          "return SaveScheduleDate(false,'" +
                                                          string.Format("{0:ddd, MMM d, yyyy}", e.Day.Date) + "','" +
                                                          ScheduleType + "','" +
                                                          deliveryDate + "','" + Session["touchType"].ToString() + "');");
                            }

                        }
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.White))
                        {
                            e.Cell.CssClass = "cellwhite";
                            e.Cell.ForeColor = Color.Black;
                            if (!CheckIfRoleIsAllowed(Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.White), BookAction))
                                e.Cell.Attributes.Add("onClick", " return funGetNoDateSelect('" + e.Day.Date.ToShortDateString() + "');");
                            else
                            {
                                string resval = (((Convert.ToDateTime(e.Day.Date.ToString()) - DateTime.Now).TotalDays > 1) ? "true" : "false");

                                e.Cell.Attributes.Add("onClick",
                                                          "return SaveScheduleDate(false,'" +
                                                          string.Format("{0:ddd, MMM d, yyyy}", e.Day.Date) + "','" +
                                                          ScheduleType + "','" +
                                                          deliveryDate + "','" + Session["touchType"].ToString() + "');");
                            }
                        }

                        #region Bind hover panel
                        if (Request.QueryString["selecteddate"] != "undefined" && !string.IsNullOrEmpty(Request.QueryString["selecteddate"]) && Convert.ToDateTime(Request.QueryString["selecteddate"]) == Convert.ToDateTime(dr[_DateColumnName]))
                            if (Request.QueryString["selectedtime"] == ScheduleType)
                                e.Cell.Attributes.Add("data-isdateselected", "1");

                        decimal roundTripMiles = Convert.ToDecimal(Request.QueryString["dis"]);
                        decimal tripMiles = Convert.ToDecimal(dr["TripMiles"]);
                        decimal touchMiles = tripMiles - roundTripMiles;
                        e.Cell.Attributes.Add("data-date", Convert.ToDateTime(dr[_DateColumnName]).ToString("d MMM, ddd"));
                        e.Cell.Attributes.Add("data-regularmiles", Convert.ToString(dr["RegularMiles"]));



                        e.Cell.Attributes.Add("data-salesregularmiles", Convert.ToString(dr["SalesRegularMiles"]));
                        e.Cell.Attributes.Add("data-serviceregularmiles", Convert.ToString(dr["ServiceRegularMiles"]));
                        e.Cell.Attributes.Add("data-tripmiles", tripMiles.ToString());
                        e.Cell.Attributes.Add("data-touchmiles", touchMiles.ToString());
                        e.Cell.Attributes.Add("data-roundtripmiles", roundTripMiles.ToString());
                        if (CheckIfRoleIsAllowed(string.Empty, ViewFacilityMiles))
                            e.Cell.Attributes.Add("data-facilitymiles", Convert.ToString(dr["FacilityMiles"]));
                        if (CheckIfRoleIsAllowed(string.Empty, ViewBookedMiles))
                            e.Cell.Attributes.Add("data-bookedmiles", Convert.ToString(dr["BookedMiles"]));
                        if (CheckIfRoleIsAllowed(string.Empty, ViewReservedMiles))
                            if (ScheduleType.ToUpper() == "ANYTIME")
                                e.Cell.Attributes.Add("data-reservedmiles", Convert.ToString(dr["ReservedMiles"]));
                        #endregion
                    }
                    else
                        e.Cell.Attributes.Add("onClick", " return funGetNoDateSelect('" + e.Day.Date.ToShortDateString() + "');");
                }
            }
        }

        private string BuildToolTip(DataRow dr)
        {
            return "TouchesScheduled: " + dr["TouchesScheduled"].ToString() + Environment.NewLine +
                               " MileagesBooked: " + Convert.ToDecimal(dr["MileagesBooked"]).ToString("0.00") + Environment.NewLine +
                               " TouchLimit: " + dr["TouchLimit"].ToString() + Environment.NewLine +
                               " MileageLimit: " + dr["MileageLimit"].ToString() + Environment.NewLine;
        }

        private ScheduleCalendar sceduleCalendar;
        private void LoadScheduleCalendar(DateTime? startDate)
        {
            DateTime date;
            double noDays = 0, daysinMonth = 0;
            if (Request.QueryString["type"] == "DE-Deliver Empty" && startDate.Value.Month == DateTime.Now.Month)
            {
                date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime firstDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                daysinMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
                DateTime lastDay = firstDay.AddDays(daysinMonth);
                noDays = (lastDay - date).TotalDays;
            }
            else
            {
                date = startDate.Value;
                DateTime firstDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                noDays = DateTime.DaysInMonth(startDate.Value.Year, startDate.Value.Month);

            }

            smdObject = new SMDBusinessLogic();
            if (Request.QueryString != null)
            {
                PRCapacity prc = new PRCapacity(Session["UserId"] != null ? Convert.ToString(Session["UserId"]) : "");

                //Added by Sohan, pull data from ESB endpoint now.
                sceduleCalendar = prc.GetCapacity(Helper.GetSession("locationCode").ToString(), date, Convert.ToInt32(noDays),
                        Convert.ToDecimal(Request.QueryString["dis"]), GetTouchTypeIdByTouchName(Request.QueryString["type"]),
                        (!string.IsNullOrEmpty(Request.QueryString["ContainerType"]) ? Convert.ToInt32(Request.QueryString["ContainerType"]) as int? : null));

                ////commented by Sohan 
                //sceduleCalendar = prc.GetCapacity(Helper.Corpcode,
                //                                Helper.GetSession("locationCode").ToString(),
                //                                Helper.username, Helper.password, date,
                //                                Convert.ToInt32(noDays),
                //                                Convert.ToDecimal(Request.QueryString["dis"]),
                //                                GetTouchTypeIdByTouchName(Request.QueryString["type"]),
                //                                (!string.IsNullOrEmpty(Request.QueryString["ContainerType"]) ? Convert.ToInt32(Request.QueryString["ContainerType"]) as int? : null));

                Session["dsAvailCalendar"] = sceduleCalendar;

                AvailableDates = sceduleCalendar;
                DateColumnName = "Date";
                DefaultSelectedDate = startDate.Value;
            }
        }

        protected void ibtnRefresh_Click(object sender, ImageClickEventArgs e)
        {
            HttpContext.Current.Items.Add("Reload", "1");
        }

        private bool CheckDeliveryDateExpirationRange(DateTime date)
        {
            if ((date > deliveryDate.AddDays(-4)) && (date < deliveryDate.AddDays(4)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void btnLogin_Click(object sender, ImageClickEventArgs e)
        {
            UserInfo objUserInfo = PRDirectCalls.Login(txtFirstName.Text.Trim(), txtLastName.Text.Trim(),
                                                         txtPassword.Text.Trim());
            if (objUserInfo.FirstName != "" || objUserInfo.LastName != "" || objUserInfo.Role != "")
            {
                if (objUserInfo.Role == "Administrator" || objUserInfo.Role == "Supervisor")
                {
                    // Session["tempAdminUser"] = true;
                    Session["tempAuthorizer"] = objUserInfo.FirstName + " " + objUserInfo.LastName;
                    hdAllowSupLogin.Value = "true";
                    mpelogin.Hide();
                    pnlLogin.CssClass = "HidePanel";
                    Response.Redirect(Request.Url.ToString() + "&tempAuth=True");
                }
                else
                {
                    Session["tempAdminUser"] = false;
                    Session["tempAuthorizer"] = string.Empty;
                    hdAllowSupLogin.Value = "false";

                    lblErrDisp.Text = "The user is not a supervisor or admin user.";
                    mpelogin.Show();
                    pnlLogin.CssClass = "UnhidePanel";
                }
            }
            else
            {
                lblErrDisp.Text = "Please enter valid credentials.";
                mpelogin.Show();
                pnlLogin.CssClass = "UnhidePanel";
            }
        }

        protected void hlSupLogin_Click(object sender, EventArgs e)
        {
            mpelogin.Show();
            pnlLogin.CssClass = "UnhidePanel";
        }

        protected void btnCloseLoginPopup_Click(object sender, EventArgs e)
        {
            mpelogin.Hide();
            pnlLogin.CssClass = "HidePanel";
        }



        protected void btnView_Click(object sender, EventArgs e)
        {
            var newDateTime = new DateTime(Convert.ToInt32(ddlYear.SelectedValue),
                                                              Convert.ToInt32(ddlMonth.SelectedValue),
                                                              1);
            LoadScheduleCalendar(newDateTime);
            calScheduleAnyTime.VisibleDate = newDateTime;
            calScheduleAM.VisibleDate = new DateTime(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth.SelectedValue), 1);
            calSchedulePM.VisibleDate = new DateTime(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth.SelectedValue), 1);

            ScriptManager.RegisterStartupScript(this, GetType(), "HoverScript", "LoadHoverPanel();", true);
        }

        private void BindMonths()
        {
            DateTime month = Convert.ToDateTime("1/1/2000");
            for (int i = 0; i < 12; i++)
            {

                DateTime NextMont = month.AddMonths(i);
                ListItem list = new ListItem();
                list.Text = NextMont.ToString("MMMM");
                list.Value = NextMont.Month.ToString();
                ddlMonth.Items.Add(list);
            }

            ddlMonth.Items.FindByValue(calScheduleAnyTime.VisibleDate.Month.ToString()).Selected = true;

        }

        public void BindDDLYears()
        {
            ListItem item = null;
            int currentYear;// = DateTime.Now.Year-3;
            for (int i = 0; i <= 15; i++)
            {
                currentYear = DateTime.Now.AddYears((i - 3)).Year;
                item = new ListItem(currentYear.ToString(), currentYear.ToString());

                ddlYear.Items.Add(item);
            }

            ddlYear.SelectedValue = calScheduleAnyTime.VisibleDate.Year.ToString();
        }
        int GetTouchTypeIdByTouchName(string touchType)
        {

            PRCapacity oPRCapacity = new PRCapacity();
            var touchTypes = oPRCapacity.GetAllCapTouchTypes().Tables[0].Rows.OfType<DataRow>().Select(c => new
            {
                CAPTouchTypeId = Convert.ToInt32(c["CAPTouchTypeId"]),
                Description = Convert.ToString(c["Description"]),
                TouchTypeCode = Convert.ToString(c["TouchTypeCode"])
            }).ToList();


            if (!string.IsNullOrEmpty(touchType))
            {
                return touchTypes.FirstOrDefault(c => touchType.ToLower().Contains(string.Format("{0}-", c.TouchTypeCode.ToLower()))).CAPTouchTypeId;
            }
            else
            {
                return touchTypes.FirstOrDefault(c => "DE-".ToLower().Contains(string.Format("{0}-", c.TouchTypeCode.ToLower()))).CAPTouchTypeId;
            }
        }


        string GetColorCodeById(int codeId)
        {
            var color = string.Empty;
            if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Black) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Black.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Blue) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Blue.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Green) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Green.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Orange) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Orange.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Purple) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Purple.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Red) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Red.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.White) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.White.ToString();
            return color;
        }
        bool CheckIfRoleIsAllowed(string color, string action)
        {
            DataSet dsColorActionRole;
            if (Session["ColorActionRole"] != null)
                dsColorActionRole = Session["ColorActionRole"] as DataSet;
            else
            {
                int roleLevel = Convert.ToInt16(ConfigurationManager.AppSettings["CalendarAccessRoleLevel"]);
                dsColorActionRole = (new PRCapacity()).GetCapacityColorActionsByRole(roleLevel);
                Session["ColorActionRole"] = dsColorActionRole;
            }
            return dsColorActionRole.Tables[0].Rows.OfType<DataRow>().Any(c => c["Color"].ToString().ToLower().Trim() == color.ToLower().Trim()
                && c["Action"].ToString().ToLower().Trim() == action.ToLower().Trim());
        }
    }
}