﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteEmail.aspx.cs" Inherits="PRFacAppWF.QuoteEmail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Your 1-800-PACK-RAT Quote</title>
    <style type="text/css">
        body, td, th
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            color: #0165a2;
        }
        body
        {
            background-color: #FFF;
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }
    </style>
</head>
<body>
    <br />
    <%--<br />
    <p align="center">
        <font size="-2">If you are having trouble viewing this email with images, <a id="hlExtLink"
            runat="server"><strong>click here</strong></a></font>.</p>--%>
    <table cellspacing="0" cellpadding="0" align="center" border="0" width="600">
        <tr>
            <td>
                <!--<img alt="Your 1-800-PACK-RAT Quote" height="186" width="600" src="images/QuoteEmailheader.jpg">-->
                <asp:Image runat="server" ID="headerImage" Height="186" Width="600" AlternateText="Your 1-800-PACK-RAT Quote" />
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="600">
                    <tr>
                        <td bgcolor="#378ac0" rowspan="2" width="13">
                            &nbsp;
                        </td>
                        <td bgcolor="#dce6f2" width="550">
                            <table cellspacing="0" cellpadding="0" align="center" border="0" width="530">
                                <%--    <tr>
                                    <td>
                                        <p>
                                            Thank you for considering <strong><font color="013453">1-800-PACK-RAT</font></strong>
                                            for your storage and moving needs! We take pride in our customer service and want
                                            to help you with all your questions and needs. Call us at <strong><font color="013453">
                                                1-800-722-5728</font></strong> for more details, discounts and deals!<br />
                                            <br />
                                        </p>
                                        <p>
                                            <!--<img height="6" width="529" src="images/dottedLine.jpg"></p>-->
                                            <asp:Image ID="dottedlineImage" runat="server" Height="6" Width="529" />
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" width="530">
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="331">
                                                    <asp:Label ID="lblCustomer" runat="server" Font-Bold="true" Font-Size="Medium"></asp:Label><br />
                                                    <%=address1 %><br />
                                                    <%=city %>,
                                                    <%=state %>
                                                    &nbsp;
                                                    <%=zip %>
                                                </td>
                                                <%--<td width="199" align="right">
                                                    <font size="5" color="013453"><strong>
                                                    <%=dateOfQuote %></strong></font><br />
                                                    <%=phone %><br />
                                                    <%=email %>
                                                </td>--%>
                                                <td width="199" align="right">
                                                    <b>Quote Expires:
                                                        <%=quoteExpDate%>&nbsp; </b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            <br />
                                            Thank you for considering <strong>1-800-PACK-RAT</strong> for your storage and moving
                                            needs! We take pride in our customer service and want to help you with all your
                                            questions and needs. Call us at <strong>1-800-722-5728</strong> for more details,
                                            discounts and deals!<br />
                                        </p>
                                    </td>
                                </tr>
                                <br />
                                <tr>
                                    <td>
                                        <asp:Image ID="dottedlineImage" runat="server" Height="6" Width="529" />
                                    </td>
                                </tr>
                                <br />
                                <tr>
                                </tr>
                                <tr>
                                    <td>
                                        <p align="center">
                                            <font size="3+">Please call 1-800-PACK-RAT soon to schedule your delivery today!
                                                <br />
                                                Your Quote Number is <b>
                                                    <%=quoteNumber %></b>. </font>
                                            <br />
                                        </p>
                                        <p align="center">
                                            <strong><font size="+2"><%=billingCycle %> Rent: <font color="013453">
                                                <%=monthlyRent %></font></strong>
                                            <br />
                                            For one
                                            <%=unitsize %>
                                            Container
                                        </p>
                                        <br />
                                        <p align="center">
                                            <strong><font size="+2">Total Due at Delivery: <font color="013453">
                                                <%=dueAtDelivery %></font></font></strong>
                                            <br />
                                            1st <%=rentDuration%>'s rent, delivery charge, and any additional items purchased
                                        </p>
                                        <br />
                                        <p align="center">
                                            <strong><font size="+2">Estimated Future Transportation: <font color="013453">
                                                <%=futurePrice %></font></font></strong>
                                            <br />
                                            Based on Zip Codes provided
                                        </p>
                                        <br />
                                        <br />
                                        <div align="center">
                                            <asp:HyperLink runat="server" ID="hlHelp" NavigateUrl="http://www.hireahelper.com/a/1800packrat/welcome" />
                                        </div>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td bgcolor="#378ac0" rowspan="2" width="12">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#dce6f2">
                            <table cellspacing="0" cellpadding="0" align="center" border="0" width="530">
                                <tr>
                                    <td>
                                        <p>
                                            <font size="-2">This quote includes all transportation fees, monthly rent, taxes, and
                                                discounts based on the given information. Your price is only subject to change if
                                                the given information changes or if you need to expedite your transportation. If
                                                you would like additional clarification, or want to discuss the details of your
                                                move, please call us at <strong>1-800-722-5728</strong>. We want to make your experience
                                                with <strong>1-800-PACK-RAT</strong> as simple as possible.</font></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#378ac0" height="13">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
</body>
</html>