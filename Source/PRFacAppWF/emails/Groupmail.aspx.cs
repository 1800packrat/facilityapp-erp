﻿using System;
using System.Configuration;

namespace PRFacAppWF.emails
{
    public partial class Groupmail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            headerImage.ImageUrl = ConfigurationManager.AppSettings["AppURL"] + "images/header_order.jpg";
            dottedlineImage.ImageUrl = ConfigurationManager.AppSettings["AppURL"] + "images/dottedLine.jpg";
            hlHelp.ImageUrl = ConfigurationManager.AppSettings["AppURL"] + "images/smallbanner.png";
        }
    }
}