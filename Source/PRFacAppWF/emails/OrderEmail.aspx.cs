﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Xml;

namespace PRFacAppWF
{
    public partial class OrderEmail : System.Web.UI.Page
    {
        public string customerName, address1, address2, city, state, zip, phone, email = string.Empty;

        public string quoteDate, quoteNumber, quoteExpDate, deliveryDate, dateOfQuote = string.Empty;

        public string noOfUnits, unitsize, noOfMonths, location = string.Empty;

        public string dueAtDelivery, monthlyRent, futurePrice = string.Empty;

        public string billingCycle, rentDuration = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            headerImage.ImageUrl = ConfigurationManager.AppSettings["AppURL"] + "images/header_order.jpg";
            dottedlineImage.ImageUrl = ConfigurationManager.AppSettings["AppURL"] + "images/dottedLine.jpg";
            hlHelp.ImageUrl = ConfigurationManager.AppSettings["AppURL"] + "images/smallbanner.png";

            //lblCustomer.Text = "Joydeep Misra";
            //address1 = "3111 Heritage Trade Drive";
            //city = "Wake Forest";
            //state = "NC";
            //zip = "27587";
            //quoteNumber = "1490453";
            //monthlyRent = "$147.77";
            //unitsize = "16 Foot";
            //dueAtDelivery = "$147.77";
            //futurePrice = "$168.99";
            BindDataToControls();
        }

        private void BindDataToControls()
        {
            string ran = Request.QueryString["id"];
            try
            {
                var doc = new XmlDocument();
                doc.Load(ConfigurationManager.AppSettings["AppURL"] + "XMLFiles/" + ran + ".xml");
                email = GetKeyValue(doc, "EmailAddress");

                lblCustomer.Text = GetKeyValue(doc, "FirstName") + " " + GetKeyValue(doc, "LastName");

                address1 = GetKeyValue(doc, "DeliveryAddress1");
                city = GetKeyValue(doc, "BillingCity");
                state = GetKeyValue(doc, "BillingState");
                zip = GetKeyValue(doc, "BillingZip");
                phone = GetKeyValue(doc, "CustomerPhoneNumber");
                dateOfQuote = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year;
                quoteNumber = GetKeyValue(doc, "QORID_Global");
                email = GetKeyValue(doc, "CustomerEmail");
                // deliveryDate = GetKeyValue(doc, "SchedDate");
                deliveryDate = GetKeyValue(doc, "DeliveryDate");
                quoteExpDate = GetKeyValue(doc, "ExpirationDate");
                futurePrice = GetKeyValue(doc, "FutureTransportationCharge");
                monthlyRent = GetKeyValue(doc, "RecurringPrice");
                dueAtDelivery = GetKeyValue(doc, "DueAtDelivery");
                location = GetKeyValue(doc, "DeliveryAddress1");
                unitsize = GetKeyValue(doc, "UnitSize");
                //SetUnitInformation(doc);

                billingCycle = GetKeyValue(doc, "BillingCycle");

                if (billingCycle == "Monthly")
                    rentDuration = "month";
                else
                    rentDuration = "week";
                hlHelp.NavigateUrl = "http://1800packrat.com/moving/hireahelper?p1=" +Convert.ToDateTime(deliveryDate).AddDays(1).ToShortDateString() + "&p2=" + zip + "&p3=" + quoteNumber;
            }
            catch (Exception ex)
            {
                var File = new FileInfo(HttpContext.Current.Server.MapPath("~/XMLFiles/" + ran + ".xml"));
                File.Delete();
            }
        }

        protected string GetKeyValue(XmlDocument doc, string Key)
        {
            string Value = "";
            try
            {
                if (Key != "")
                {
                    XmlNodeList xmlnodelst = doc.GetElementsByTagName(Key);
                    if (xmlnodelst != null)
                    {
                        if (xmlnodelst.Item(0).ChildNodes[0] != null)
                        {
                            if (xmlnodelst.Item(0).ChildNodes[0].Value != null)
                                Value = xmlnodelst.Item(0).ChildNodes[0].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Value = "";
            }
            return Value;
        }
    }
}