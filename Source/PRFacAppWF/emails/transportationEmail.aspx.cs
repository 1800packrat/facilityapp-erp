﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Xml;
using PRFacAppWF.CodeHelpers;
using PR.BusinessLogic;
using PR.Entities;
using SLAPI;

namespace PRFacAppWF
{
    public partial class transportationEmail : System.Web.UI.Page
    {
        public string customerName, address1, address2, city, state, zip, phone, email = string.Empty;

        public string quoteDate, quoteNumber, quoteExpDate, deliveryDate, dateOfQuote = string.Empty;

        public string touchType = string.Empty;

        public string price;

        protected void Page_Load(object sender, EventArgs e)
        {
            headerImage.ImageUrl = ConfigurationManager.AppSettings["AppURL"] + "images/transheader.jpg";
            dottedlineImage.ImageUrl = ConfigurationManager.AppSettings["AppURL"] + "images/dottedLine.jpg";
            hlHelp.ImageUrl = ConfigurationManager.AppSettings["AppURL"] + "images/smallbanner.png";
            BindDataToControls();
        }

        private void BindDataToControls()
        {
            string ran = Request.QueryString["id"];
            try
            {
                var doc = new XmlDocument();
                doc.Load(ConfigurationManager.AppSettings["AppURL"] + "XMLFiles/" + ran + ".xml");
                email = GetKeyValue(doc, "EmailAddress");

                lblCustomer.Text = GetKeyValue(doc, "FirstName") + " " + GetKeyValue(doc, "LastName");

                address1 = GetKeyValue(doc, "DeliveryAddress1");
                city = GetKeyValue(doc, "BillingCity");
                state = GetKeyValue(doc, "BillingState");
                zip = GetKeyValue(doc, "BillingZip");
                phone = GetKeyValue(doc, "CustomerPhoneNumber");
                dateOfQuote = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year;
                quoteNumber = GetKeyValue(doc, "QORID_Global");
                email = GetKeyValue(doc, "CustomerEmail");
                // deliveryDate = GetKeyValue(doc, "SchedDate");
                deliveryDate = GetKeyValue(doc, "DeliveryDate");
                quoteExpDate = GetKeyValue(doc, "ExpirationDate");
                touchType = GetKeyValue(doc, "Type");
                setMessagesBasedOnTouch(touchType);
                price = GetTouchPricing(doc).ToString("C");
                //SetUnitInformation(doc);
                SetHelpIcon();
            }
            catch (Exception ex)
            {
                var File = new FileInfo(HttpContext.Current.Server.MapPath("~/XMLFiles/" + ran + ".xml"));
                File.Delete();
            }
        }

        private void SetHelpIcon()
        {
            if (touchType != "RE-Return Empty")
            {
                hlHelp.Visible = true;
            }
            else
            {
                hlHelp.Visible = false;
            }
        }

        private void setMessagesBasedOnTouch(string touchType)
        {
            switch (touchType)
            {
                case "DE-Deliver Empty":
                    lblChargeforDeliverymsg.Text = "This charge is for the initial delivery of your container to your house.";
                    lblSchedulemsg.Text = "Your Delivery is scheduled for " + deliveryDate;
                    break;
                case "RF-Return Full":
                    lblChargeforDeliverymsg.Text = "This charge is for picking up your full container and bringing it to our warehouse.";
                    lblSchedulemsg.Text = "Your Pickup Full is scheduled for " + deliveryDate;
                    break;
                case "RE-Return Empty":
                    lblChargeforDeliverymsg.Text = "This charge is for the final pickup of your empty container from your house.";
                    lblSchedulemsg.Text = "“Your Pickup is scheduled for " + deliveryDate;
                    break;
                case "CC-Curb To Curb":
                    lblChargeforDeliverymsg.Text = "This charge is for the move of your container.";
                    lblSchedulemsg.Text = "Your Move is scheduled for " + deliveryDate;
                    break;
                case "DF-Deliver Full":
                    lblChargeforDeliverymsg.Text = "This charge is for the re-delivery of your full container from our warehouse.";
                    lblSchedulemsg.Text = "Your Deliver Full is scheduled for " + deliveryDate;
                    break;
                default:
                    break;
            }
        }

        protected decimal GetTouchPricing(XmlDocument doc)
        {
            SLAPIMobile objSLAPI =
               ((new SMDBusinessLogic(HttpContext.Current.Session["UserId"] != null
                                          ? Convert.ToString(HttpContext.Current.Session["UserId"])
                                          : ""))).EditQuote(Helper.Corpcode, GetKeyValue(doc, "Facility1ID"), Helper.username, Helper.password, Convert.ToInt32(GetKeyValue(doc, "QORID")), Convert.ToInt32(GetKeyValue(doc, "CUSTNMBR")), (HttpContext.Current.Session["GUID"] != null)
                                                   ? HttpContext.Current.Session["GUID"].ToString()
                                                   : Guid.NewGuid().ToString());

            SMDBusinessLogic smd = new SMDBusinessLogic();
            Touches touch = smd.GetAppliedTouches(objSLAPI);
            decimal amount = 0;
            var row = from r in touch.Tables[0].AsEnumerable() where r.Field<string>("Service") == touchType select r;
           
            foreach (var item in row)
            {
                var subTotal = item.IsNull("SubTotal")?0: item.Field<decimal>("SubTotal");
                var Tax = item.IsNull("Tax")? 0:item.Field<decimal>("Tax");
                amount = subTotal + Tax;
            }

            return amount;
        }

        protected string GetKeyValue(XmlDocument doc, string Key)
        {
            string Value = "";
            try
            {
                if (Key != "")
                {
                    XmlNodeList xmlnodelst = doc.GetElementsByTagName(Key);
                    if (xmlnodelst != null)
                    {
                        if (xmlnodelst.Item(0).ChildNodes[0] != null)
                        {
                            if (xmlnodelst.Item(0).ChildNodes[0].Value != null)
                                Value = xmlnodelst.Item(0).ChildNodes[0].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Value = "";
            }
            return Value;
        }
    }
}
