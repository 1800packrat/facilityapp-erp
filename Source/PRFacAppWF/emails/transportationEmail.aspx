﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="transportationEmail.aspx.cs"
    Inherits="PRFacAppWF.transportationEmail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Your 1-800-PACK-RAT Tranportation Update</title>
    <style type="text/css">
      
         body, td, th
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            color: #0165a2;
        }
        body
        {
            background-color: #FFF;
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }
        </style>
</head>
<body>
  
   
    <table cellspacing="0" cellpadding="0" align="center" border="0" width="600">
        <tr>
            <td>
                <!--<img alt="Your 1-800-PACK-RAT Quote" height="186" width="600" src="images/QuoteEmailheader.jpg">-->
                <asp:Image runat="server" ID="headerImage" ImageUrl="~/images/transheader.jpg" Height="183"
                    Width="600" AlternateText="Your 1-800-PACK-RAT Update" />
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="600" >
                    <tr>
                        <td bgcolor="#378ac0" rowspan="2" width="13">
                            &nbsp;
                        </td>
                        <td bgcolor="#dce6f2" width="550">
                            <table cellspacing="0" cellpadding="0" align="center" border="0" width="530">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" width="530">
                                            <%--<tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td width="331">
                                                    <asp:Label ID="lblCustomer" runat="server" Font-Bold="true" Font-Size="Medium"></asp:Label><br />
                                                    <%=address1 %>
                                                    <br />
                                                    <%=city %>,
                                                    <%=state %>
                                                    &nbsp;
                                                    <%=zip %>
                                                </td>
                                                <td width="199" align="right">
                                                    Order Number<b>:
                                                        <%=quoteNumber%>&nbsp; </b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <p>
                                            <br />
                                            Thank you for scheduling your appointment with
                                            <br />
                                            1800-PACK-RAT.<br />
                                        </p>
                                    </td>
                                </tr>
                             
                                <tr>
                                    <td>
                                        <asp:Image ID="dottedlineImage" runat="server" Height="6" Width="529" />
                                    </td>
                                </tr>
                                <br />
                                <tr>
                                    <td style="font-size: 23px;">
                                        <p align="center">
                                            <asp:Label runat="server" ID="lblSchedulemsg" Font-Bold="true"></asp:Label>
                                            <br />
                                        </p>
                                        <p align="center">
                                            <b>Transportation charges <span style="color: #013351;">
                                                <%= price %></span></b>
                                        </p>
                                        <p style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 18px;
                                            font-weight: normal;">
                                            <asp:Label runat="server" ID="lblChargeforDeliverymsg"></asp:Label>
                                            Our driver will contact you prior to your appointment to confirm. If you have any
                                            questions, please call <strong>1-800-722-5728</strong>.
                                            <br />
                                            <br />
                                        </p>
                                        <p>
                                            <div align="center">
                                                <asp:HyperLink runat="server" ID="hlHelp" NavigateUrl="http://www.hireahelper.com/a/1800packrat/welcome" /></div>
                                        </p>
                                        <p style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 13px;
                                            font-weight: normal; line-height: 20px;">
                                            For more information checkout our <a href="http://1800packrat.com/About/FAQ">Frequently
                                                Asked Questions</a>
                                        </p>
                                                <br />
                                                <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td bgcolor="#378ac0" rowspan="2" width="12">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#dce6f2">
                            <%--<table cellspacing="0" cellpadding="0" align="center" border="0" width="530">
                                <tr>
                                    <td>
                                        <p>
                                            <font style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 13px;
                                                font-weight: normal; line-height: 20px;">This quote includes all transportation
                                                fees, monthly rent, taxes, and discounts based on the given information. Your price
                                                is only subject to change if the given information changes or if you need to expedite
                                                your transportation. If you would like additional clarification, or want to discuss
                                                the details of your move, please call us at <strong>1-800-722-5278</strong>. We
                                                want to make your experience with <strong>1-800-PACK-RAT</strong> as simple as possible.</font></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>--%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#378ac0" height="13">
                &nbsp;
            </td>
        </tr>
    </table>
 
</body>
</html>
