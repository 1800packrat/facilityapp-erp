﻿<%--SFERP-TODO-CTRMV-- Remove this complete page --TG-568--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="matchingcustomers.aspx.cs" Inherits="PRFacAppWF.matchingcustomers" %>

<!DOCTYPE html>

<html xmlns="https://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
        function Storeinhf(element, slocationcode, hasActiveLedger,firstname,lastname,address,phone,zip,cmp,email) {
            document.getElementById('hdTennantId').value = element;
            document.getElementById("hdLocationCode").value = slocationcode;
            document.getElementById("hdHasAciveLedger").value = hasActiveLedger;
            document.getElementById("hdFirstName").value = firstname;
            document.getElementById("hdLastName").value = lastname;
            document.getElementById("hdPhone").value = phone;
            document.getElementById("hdAddress").value = address;
            document.getElementById("hdEmail").value = email;
            document.getElementById("hdZip").value = zip;
            document.getElementById("hdcmp").value = cmp;
            document.getElementById("btnOk").disabled = false;
        }

        
    </script>
    <%= Styles.Render("~/Content/css/styles") %>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content2">
            <div class="txtHead02">
                <%=Page.Title %>
            </div>
            <div class="tableHolder2">

                <asp:ScriptManager runat="server" ID="sp">
                </asp:ScriptManager>
                <asp:UpdatePanel runat="server" ID="up">
                    <ContentTemplate>
                        <div style="width: 100%; height: 500px; overflow: scroll;" align="center">
                            <table class="grid">
                                <tr>
                                    <td>
                                        <asp:GridView runat="server" ID="gvMatchingCustomers" AllowPaging="true" CssClass="bordered" PagerStyle-CssClass="pgr"
                                            PageSize="10" OnPageIndexChanging="gvMatchingCustomers_PageIndexChanging" AutoGenerateColumns="false">

                                            <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <ItemTemplate>
                                                        <input id="radtiogvcust" name="rbcust" type="radio" onclick="Storeinhf('<%#Eval("TenantID")%>    ','<%#Eval("sLocationCode")%>    ','<%#Eval("bHasActiveLedger")%>    ','<%#Eval("sFName")%>    ','<%#Eval("sLName")%>    ','<%#Eval("sAddr1")%>    ','<%#Eval("sPhone")%>    ','<%#Eval("sPostalCode")%>    ','<%#Eval("sCompany")%>    ','<%#Eval("sEmail")%>    ')" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tenant ID">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTenantid" Text='<%#Eval("TenantID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="First Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblsfname" Text='<%#Eval("sFName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblsLname" Text='<%#Eval("sLName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Company">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblsLname" Text='<%#Eval("sCompany")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Address">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblsaddress" Text='<%#Eval("sAddr1")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="City">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblcity" Text='<%#Eval("sCity")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Region">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblregion" Text='<%#Eval("sRegion")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Zip">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblzip" Text='<%#Eval("sPostalCode")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Phone">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblphone" Text='<%#Eval("sPhone")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="HasActiveLedger" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblHasActiveledger" Text='<%#Eval("bHasActiveLedger")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                        <asp:Label runat="server" ID="lblmsg" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <asp:Button runat="server" ID="btnOk" Text="Ok" OnClick="btnOk_Click" Enabled="false" />
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:HiddenField runat="server" ID="hdLocationCode" />
                <asp:HiddenField runat="server" ID="hdHasAciveLedger" />
                <asp:HiddenField runat="server" ID="hdTennantId" />
                <asp:HiddenField runat="server" ID="hdFirstName" />
                <asp:HiddenField runat="server" ID="hdLastName" />
                <asp:HiddenField runat="server" ID="hdAddress" />
                <asp:HiddenField runat="server" ID="hdPhone" />
                <asp:HiddenField runat="server" ID="hdEmail" />
                <asp:HiddenField runat="server" ID="hdZip" />
                <asp:HiddenField runat="server" ID="hdcmp" />


            </div>
        </div>
    </form>
</body>
</html>
