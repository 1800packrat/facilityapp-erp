﻿namespace PRFacAppWF.Security
{
    using Repository.models;
    using System.Collections.Generic;
    using Repository.Services;
    using System.Linq;
    using CodeHelpers;
    using Entities.Enums;
    public class SecuredEntityDelegate
    {
        private IUserSecurity prsService;

        protected List<FAModule> _modules;
        //protected IDictionary<string, IDictionary<string, string>> _mLookup;
        //protected List<FAModule> _mLookup;

        protected static SecuredEntityDelegate theInstance;

        protected SecuredEntityDelegate()
        {
        }

        protected static SecuredEntityDelegate GetInstance()
        {
            if (theInstance == null)
            {
                theInstance = new SecuredEntityDelegate();
                theInstance.init();
            }

            return theInstance;
        }

        //public static bool CanUserExecute(string policyTarget, Entities.Enums.AccessLevel accessType)
        //{
        //    var faModuleAccess = GetInstance()._modules.Where(m => m.ModuleName.Equals(policyTarget.ToString(), System.StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

        //    return Filters.AuthorizationFilter.GetRoleGroupPermissions.Any(g => g.ModuleId == faModuleAccess.PK_ModuleID && g.AccessLevel == accessType);
        //}
        
        public static bool CanUserExecute(ModuleName policyTarget, AccessLevel accessType, string storeNumber)
        {
            return CanUserExecute(policyTarget, accessType, storeNumber, false);
        }

        public static bool CanUserExecute(ModuleName policyTarget, AccessLevel accessType, bool byPassFacilityAccessLevelCheck)
        {
            return CanUserExecute(policyTarget, accessType, null, byPassFacilityAccessLevelCheck);
        }

        public static bool CanUserExecute(ModuleName policyTarget, AccessLevel accessType, string storeNumber = null, bool byPassFacilityAccessLevelCheck = false)
        {
            var faModuleAccess = GetInstance()._modules.Where(m => m.ModuleName.Equals(policyTarget.ToString(), System.StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

            PR.Entities.SiteInfo site = string.IsNullOrEmpty(storeNumber) ? Helper.CurrentSiteInfo : Helper.CurrentUser.Facilities.Where(f => f.GlobalSiteNum == storeNumber).FirstOrDefault();

            return (Filters.AuthorizationFilter.GetRoleGroupPermissions.Any(g => g.ModuleId == faModuleAccess.PK_ModuleID
                && ((accessType == AccessLevel.Any && (g.AccessLevel == AccessLevel.ReadAndWrite || g.AccessLevel == AccessLevel.ReadOnly))
                    || g.AccessLevel == accessType)))
             && (
                byPassFacilityAccessLevelCheck
                || (site != null && ((accessType == AccessLevel.Any && (site.AccessLevel == (int)AccessLevel.ReadAndWrite || site.AccessLevel == (int)AccessLevel.ReadOnly))
                                    || (site.AccessLevel == (int)accessType))));
        }

        protected void init()
        {
            this.prsService = new Repository.Services.Implementations.UserSecurityService();
            this._modules = prsService.ListFAModule();
        }

        public static void Reload()
        {
            GetInstance().init();
        }
    }
}