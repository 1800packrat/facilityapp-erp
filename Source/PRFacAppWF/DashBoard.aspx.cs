﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using PR.BusinessLogic;
using PRFacAppWF.CodeHelpers;
using PR.Entities;

namespace PRFacAppWF
{
    public partial class DashBoard : BasePage
    {
        private Hashtable h = new Hashtable();
        public List<SiteInfo> objLstSiteInfo = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Helper.DisposeWebSLAPIObject();
                Session["RoleId"] = 1;
                Session["SendOrderMail"] = false;
                Session["ShowCalButton"] = true;
                var GUID = new Guid();
                GUID = Guid.NewGuid();
                Session["GUID"] = GUID;
                Session["MarketId"] = null;
                AddInHashTable("ResponseType", "1");
                Session["UserFName"] = "";
                Session["UserLName"] = "";
                Session["Password"] = "";
              
            }
            SetDashBoardSettings();

        }

        private void SetDashBoardSettings()
        {
            DashboardSettings settings = DashboardSettings.RetrieveFromSession();
            dvChgFac.Style.Add(HtmlTextWriterStyle.Display, settings.ChangeFacIconVisibility);
            if (settings.MultipleFacalitiesAvailable && settings.FacilitySelected == false)
            {
                //Multiple facility manager
                RenderFacilityPanel();


            }
            else
            {
                //single facility manager

                pnlFacility.Visible = false;
                pnlDashBoard.Visible = true;

            }
        }

        private void RenderFacilityPanel()
        {
            pnlFacility.Visible = true;
            pnlDashBoard.Visible = false;
            objLstSiteInfo = CurrentUser.Facilities;// (List<SiteInfo>)Session["SiteInfo"];
            rptFac.DataSource = objLstSiteInfo.OrderBy(x => x.SiteName);
            rptFac.DataBind();
        }

        public void AddInHashTable(string strKey, string strValue)
        {
            if (Session["HashValue"] != null)
            {
                h = (Hashtable)Session["HashValue"];
            }
            try
            {
                if (h != null)
                {
                    if (!h.ContainsKey(strKey))
                    {
                        h.Add(strKey, strValue);
                    }
                    else
                    {
                        h.Remove(strKey);
                        h.Add(strKey, strValue);
                    }
                }
            }
            catch (Exception ex)
            {
                #region ErrorCatch

                Helper.HandleException(ex);               

                #endregion ErrorCatch
            }
            Session["HashValue"] = h;
        } 
 
        protected void lbtnChgFac_Click(object sender, EventArgs e)
        {
            Session.Remove("FaclocationCode");
            Session.Remove("FacilityName");
            Session.Remove("ContactName");
            Session.Remove("WareHouseCode");
            DashboardSettings settings = DashboardSettings.RetrieveFromSession();
            settings.FacilitySelected = false;

            settings.CommitToSession();

            RenderFacilityPanel();
        }

        protected void rptFac_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ed")
            {
                LocalComponent objLocalComponent = new LocalComponent();
                try
                {
                    // (List<SiteInfo>)Session["SiteInfo"];
                    objLstSiteInfo = CurrentUser.Facilities;
                    Session["FaclocationCode"] = e.CommandArgument;
                    Session["FacilityName"] = ((System.Web.UI.WebControls.LinkButton)(e.CommandSource)).ToolTip;
                    Session["ContactName"] = objLstSiteInfo.Where(x => x.LocationCode == Session["FaclocationCode"].ToString()).Select(y => y.ContactName).FirstOrDefault();
                    DashboardSettings settings = DashboardSettings.RetrieveFromSession();

                    //Added for the below lines of Code.
                    Session["WareHouseCode"] = objLstSiteInfo.Where(x => x.LocationCode == Session["FaclocationCode"].ToString()).Select(y => y.GlobalSiteNum).FirstOrDefault();

                    //DataSet dsFacility = objLocalComponent.GetRDFacilityData(Convert.ToString(Session["FaclocationCode"]));
                    //if (dsFacility.Tables.Count > 0 && dsFacility.Tables[0].Rows.Count > 0)
                    //{
                    //    Session["WareHouseCode"] = dsFacility.Tables[0].Rows[0]["StoreNo"].ToString();

                    //    //SFERP-TODO, commented for not in use, 
                    //    //settings.IsJaguarFacility = Convert.ToBoolean(dsFacility.Tables[0].Rows[0]["IsJaguarLoad"]);
                    //}

                    objLocalComponent = null;
                    objLstSiteInfo = null;
                    //  dsFacility.Dispose();
                    pnlDashBoard.Visible = true;
                    pnlFacility.Visible = false;

                    settings.FacilitySelected = true;
                    settings.CommitToSession();
                }
                catch (Exception ex)
                {
                    Helper.LogError(ex);
                }
            }
        }
    }
}