﻿namespace PRFacAppWF.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using PRFacAppWF.CodeHelpers;
    using PRFacAppWF.models;
    using PRFacAppWF.Repository.Services;

    public class searchModel
    {
        public DateTime date { set; get; }
        public string[] unitType { set; get; }
    }

    [HandleJsonExceptionAttribute]
    public class UnitUnavailabilityController : BaseController
    {
        #region Capacity Unit Availability

        private readonly IPRSService prsService;
        public UnitUnavailabilityController()
        {
            if (this.prsService == null)
                this.prsService = new PRSService(); 
        }

        public ActionResult Index()
        {
            ViewBag.Facilities = this.prsService.GetFacilityListByUserId(CurrentUser.ID);
            return View();
        }

        public PartialViewResult SearchUnitAvailability(searchModel srchM)
        {

            lstUnitUnAvailability unitUnavailable = new lstUnitUnAvailability();
            try
            {
                tblunitAvailability objHead = new tblunitAvailability();

                objHead.Head = "";
                objHead.Day1 = srchM.date.ToShortDateString();
                objHead.Day2 = srchM.date.AddDays(1).ToShortDateString();
                objHead.Day3 = srchM.date.AddDays(2).ToShortDateString();
                objHead.Day4 = srchM.date.AddDays(3).ToShortDateString();
                objHead.Day5 = srchM.date.AddDays(4).ToShortDateString();
                objHead.Day6 = srchM.date.AddDays(5).ToShortDateString();
                objHead.Day7 = srchM.date.AddDays(6).ToShortDateString();
                unitUnavailable.Head = objHead;

                var lstUnitAvailability = this.prsService.GetUnitUnavailabilityInfo(srchM.date, srchM.date.AddDays(6), srchM.unitType, CurrentUser.ID);
                List<tblunitAvailability> lstUnitAvail = ParseUnitAvailabilityToJSON(srchM.date, lstUnitAvailability);

                unitUnavailable.unitDetails = lstUnitAvail;
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }
            return PartialView("Partial/_searchUnitAvailability", unitUnavailable);
        }

        private List<tblunitAvailability> ParseUnitAvailabilityToJSON(DateTime startDate, List<UnitAvailabilityInfo> lstUnitAvailability)
        {
            List<tblunitAvailability> UnitAvaiGridData = new List<tblunitAvailability>();

            var Day1List = lstUnitAvailability.Where(u => u.FromDate <= startDate && startDate <= u.ThroughDate).Distinct();
            var Day2List = lstUnitAvailability.Where(u => u.FromDate <= startDate.AddDays(1) && startDate.AddDays(1) <= u.ThroughDate).Distinct();
            var Day3List = lstUnitAvailability.Where(u => u.FromDate <= startDate.AddDays(2) && startDate.AddDays(2) <= u.ThroughDate).Distinct();
            var Day4List = lstUnitAvailability.Where(u => u.FromDate <= startDate.AddDays(3) && startDate.AddDays(3) <= u.ThroughDate).Distinct();
            var Day5List = lstUnitAvailability.Where(u => u.FromDate <= startDate.AddDays(4) && startDate.AddDays(4) <= u.ThroughDate).Distinct();
            var Day6List = lstUnitAvailability.Where(u => u.FromDate <= startDate.AddDays(5) && startDate.AddDays(5) <= u.ThroughDate).Distinct();
            var Day7List = lstUnitAvailability.Where(u => u.FromDate <= startDate.AddDays(6) && startDate.AddDays(6) <= u.ThroughDate).Distinct();


            int[] anArray = { Day1List.Count(), Day2List.Count(), Day3List.Count(), Day4List.Count(), Day5List.Count(), Day6List.Count(), Day7List.Count() };
            int maxCountValue = anArray.Max();

            for (int i = 0; i <= maxCountValue; i++)
            {
                tblunitAvailability row = new tblunitAvailability();
                if (Day1List.Count() > i)
                {
                    if (Day1List.ElementAt(i) != null)
                    {
                        row.Day1 = Day1List.ElementAt(i).FacilityName + " - " + Day1List.ElementAt(i).UnitSize;
                        row.Day1Id = Day1List.ElementAt(i).UnitAvailabilityInfoId.ToString();
                    }
                }
                else
                {
                    row.Day1 = null;
                    row.Day1Id = "0";
                }
                if (Day2List.Count() > i)
                {
                    if (Day2List.ElementAt(i) != null)
                    {
                        row.Day2 = Day2List.ElementAt(i).FacilityName + " - " + Day2List.ElementAt(i).UnitSize;
                        row.Day2Id = Day2List.ElementAt(i).UnitAvailabilityInfoId.ToString();
                    }
                }
                else
                {
                    row.Day2 = null;
                    row.Day2Id = "0";
                }
                if (Day3List.Count() > i)
                {
                    if (Day3List.ElementAt(i) != null)
                    {
                        row.Day3 = Day3List.ElementAt(i).FacilityName + " - " + Day3List.ElementAt(i).UnitSize;
                        row.Day3Id = Day3List.ElementAt(i).UnitAvailabilityInfoId.ToString();
                    }
                }
                else
                {
                    row.Day3 = null;
                    row.Day3Id = "0";
                }
                if (Day4List.Count() > i)
                {
                    if (Day4List.ElementAt(i) != null)
                    {
                        row.Day4 = Day4List.ElementAt(i).FacilityName + " - " + Day4List.ElementAt(i).UnitSize;
                        row.Day4Id = Day4List.ElementAt(i).UnitAvailabilityInfoId.ToString();
                    }
                }
                else
                {
                    row.Day4 = null;
                    row.Day4Id = "0";
                }
                if (Day5List.Count() > i)
                {
                    if (Day5List.ElementAt(i) != null)
                    {
                        row.Day5 = Day5List.ElementAt(i).FacilityName + " - " + Day5List.ElementAt(i).UnitSize;
                        row.Day5Id = Day5List.ElementAt(i).UnitAvailabilityInfoId.ToString();
                    }
                }
                else
                {
                    row.Day5 = null;
                    row.Day5Id = "0";
                }
                if (Day6List.Count() > i)
                {
                    if (Day6List.ElementAt(i) != null)
                    {
                        row.Day6 = Day6List.ElementAt(i).FacilityName + " - " + Day6List.ElementAt(i).UnitSize;
                        row.Day6Id = Day6List.ElementAt(i).UnitAvailabilityInfoId.ToString();
                    }
                }
                else
                {
                    row.Day6 = null;
                    row.Day6Id = "0";
                }
                if (Day7List.Count() > i)
                {
                    if (Day7List.ElementAt(i) != null)
                    {
                        row.Day7 = Day7List.ElementAt(i).FacilityName + " - " + Day7List.ElementAt(i).UnitSize;
                        row.Day7Id = Day7List.ElementAt(i).UnitAvailabilityInfoId.ToString();
                    }
                }
                else
                {
                    row.Day7 = null;
                    row.Day7Id = "0";
                }
                UnitAvaiGridData.Add(row);
            }

            return UnitAvaiGridData;
        }

        public JsonResult GetUnitUnAvailabilityDetails(int UnitAvailabilityInfoId)
        {
            var UnitUnavailability = prsService.GetUnitUnavailabilityInfo(UnitAvailabilityInfoId);

            return Json(UnitUnavailability, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFacilityAccessLevel(int FacilityID)
        {
            var IsFullAccess = Security.SecuredEntityDelegate.CanUserExecute(Entities.Enums.ModuleName.UnitUnavailability, Entities.Enums.AccessLevel.ReadAndWrite, Convert.ToString(FacilityID));
            return Json(IsFullAccess, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult SaveUnitAvailability(UnitAvailabilityInfo uaInfo)
        {
            string errMsg = string.Empty;
            bool success = true;

            try
            {
                uaInfo.CreatedBy = Convert.ToInt32(Session["ID"]);

                this.prsService.SaveUnitAvaialability(uaInfo);

            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                errMsg = ex.Message;
                success = false;
            }

            var retJSON = new
            {
                success = success,
                msg = errMsg
            };

            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }



        #endregion
    }
}