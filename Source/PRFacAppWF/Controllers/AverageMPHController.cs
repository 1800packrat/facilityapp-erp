﻿namespace PRFacAppWF.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using PRFacAppWF.CodeHelpers;
    using PRFacAppWF.models;
    using PRFacAppWF.Repository.Services;

    [HandleJsonExceptionAttribute]
    [Authorize]
    public class AverageMPHController : BaseController
    {
        private readonly IPRSService prsService;
        public AverageMPHController()
        {
            if (this.prsService == null)
                this.prsService = new PRSService();
        }

        public ActionResult Index()
        {
            ViewBag.FacilityList = this.prsService.GetFacilityListByUserId(CurrentUser.ID);
            return View();
        }

        public PartialViewResult SearchAvgMPH(DateTime Date, string SortCol = null, string SortDir = null)
        {
            List<AverageMPH> lstAS = new List<AverageMPH>();

            try
            {
                SortCol = SortCol == "" ? "FacilityName" : SortCol;
                SortDir = SortDir == "" ? "ASC" : SortDir;
                lstAS = this.prsService.GetAverageMPHList(Date, CurrentUser.ID, SortCol, SortDir);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return PartialView("partial/_listAvgMPHSearchResults", lstAS);
        }

        public ActionResult GetFacilityAvgMPHHistory(string sidx, string sord, int page, int rows, int FacilityId)
        {
            try
            {
                var varAvgMPHResults = this.prsService.SearchFacilityAverageMPHList(FacilityId);
                int totalCount = varAvgMPHResults.Count();
                var sortedResults = FilterAvgMPHGrid(varAvgMPHResults, sidx, sord, page, rows);

                var pageSize = 10;
                var totalRecords = totalCount;
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

                var IsFullAccess = false;

                if (FacilityId > 0)
                    IsFullAccess = Security.SecuredEntityDelegate.CanUserExecute(Entities.Enums.ModuleName.AverageMPH, Entities.Enums.AccessLevel.ReadAndWrite, Convert.ToString(FacilityId));
                else
                    IsFullAccess = Security.SecuredEntityDelegate.CanUserExecute(Entities.Enums.ModuleName.AverageMPH, Entities.Enums.AccessLevel.ReadAndWrite, true);


                var jsonData = new
                {
                    total = totalPages,
                    page = page,
                    isfullaccess = IsFullAccess,
                    facilityId = FacilityId,
                    records = totalRecords,
                    rows = (from lst in sortedResults
                            select new
                            {
                                i = lst.CAPAvgMPHId,
                                cell = new string[]
                                {
                                    lst.CAPAvgMPHId.ToString(),
                                    lst.AvgMPH.ToString(),
                                    lst.Startdate.HasValue ? lst.Startdate.Value.ToShortDateString():"",
                                    lst.Enddate.HasValue ? lst.Enddate.Value.ToShortDateString():"",
                                    lst.Operatinghours.ToString(),
                                    lst.Mileage.ToString(),
                                    lst.State.ToString(),
                                    lst.FacilityId.ToString()
                                }
                            }).ToArray()
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);

                var jsonData = new
                {
                    total = 0,
                    page = 0,
                    records = 0,
                    rows = new string[0]
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        private IEnumerable<AverageMPH> FilterAvgMPHGrid(IEnumerable<AverageMPH> data, string sidx, string sord, int page, int rows)
        {

            int skiprows = (page - 1) * rows;
            ////Since in the UI we are displaying CapacityDate column as CapacityDateDisp.
            sidx = sidx == "" ? "Startdate" : sidx;
            if (sord == "desc")
                data = data.OrderByDescending(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);
            else
                data = data.OrderBy(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);

            return data;
        }

        public ActionResult UpdateAverageMPHRecords(int FacilityId)
        {
            ViewBag.FacilityId = FacilityId;
            return View();
        }

        [HttpPost]
        public JsonResult UpdateAverageMPHRecords(FormCollection formCollection)
        {
            DateTime? endDate = null;
            decimal mphval = 0.00M;
            bool isNullEnddate = false;
            string result = string.Empty;
            string errMsg = string.Empty;
            bool success = true;

            try
            {
                int facilityid = Convert.ToInt32(formCollection["hidfacilityid"]);
                DateTime startDate = Convert.ToDateTime(formCollection["Startdate"]);
                mphval = Convert.ToDecimal(formCollection["AvgMPH"]);
                isNullEnddate = Convert.ToBoolean(formCollection["hidNullEndDate"]);
                if (!isNullEnddate)
                    endDate = Convert.ToDateTime(formCollection["Enddate"]);

                string lstAS = this.prsService.UpdateFacilityAvgMPHValues(CurrentUser.ID, facilityid, startDate, endDate, mphval, isNullEnddate);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };
            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetAverageMPHtoCorporateDefault(FormCollection formCollection)
        {
            DateTime startDate = DateTime.Now;
            int facilityid = 0;
            string result = string.Empty;
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                startDate = Convert.ToDateTime(formCollection["Startdate"]);
                facilityid = Convert.ToInt32(formCollection["hidfacilityid"]);
                string lstAS = this.prsService.SetAverageMPHtoCorporateDefault(CurrentUser.ID, facilityid, startDate);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };
            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }
    }
}