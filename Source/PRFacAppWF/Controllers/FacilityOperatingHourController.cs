﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PRFacAppWF.CodeHelpers;
using PRFacAppWF.models;
using PRFacAppWF.Repository.Services;
using System.Data;

namespace PRFacAppWF.Controllers
{
    public class FacilityOperatingHourController : BaseController
    {
        private readonly IPRSService prsService;

        public FacilityOperatingHourController()
        {
            if (this.prsService == null)
                this.prsService = new PRSService();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddAndGetFacilityOperatingHour(string sidx, string sord, int page, int rows, DateTime startDate, DateTime endDate, bool _search, string weekday)
        {
            try
            {
                int wkday = 0;
                int.TryParse(weekday, out wkday);
                var facilityOpHoursRaw = this.prsService.AddAndGetFacilityOpHour(Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum), startDate, endDate, CurrentUser.ID.ToString(), wkday);
                int totalCount = facilityOpHoursRaw.Count();
                var facilityOpHours = FilterGrid(facilityOpHoursRaw, sidx, sord, page, rows);


                var pageSize = rows;
                var totalRecords = totalCount;
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

                var jsonData = new
                {
                    total = totalPages,
                    page = page,
                    records = totalRecords,
                    rows = facilityOpHours
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);

                var jsonData = new
                {
                    total = 0,
                    page = 0,
                    records = 0,
                    rows = new string[0]
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveFacilityHourChange(FormCollection collection)
        {
            string errMsg = string.Empty;
            bool success = true;

            try
            {
                string rowIds = collection["rowIds[]"];
                string StartTime = DateTime.Parse(collection["StartTime"]).ToString("HH:mm");
                string EndTime = DateTime.Parse(collection["EndTime"]).ToString("HH:mm");
                string StoreOpen = collection["StoreOpen"];

                if (rowIds.Trim().Length > 0)
                    this.prsService.SaveFacilityHourChange(rowIds, StartTime, EndTime, CurrentUser.ID, StoreOpen);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };

            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<FacilityOperatingHours> FilterGrid(IEnumerable<FacilityOperatingHours> data, string sidx, string sord, int page, int rows)
        {
            //Thsi total we are showing in grid and it should be after filter before page filter

            int skiprows = (page - 1) * rows;
            //Since in the UI we are displaying CapacityDate column as CapacityDateDisp.
            sidx = sidx.ToLower().Contains("caldate") ? "CalDate" : sidx;

            if (sord == "desc")
                data = data.OrderByDescending(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);
            else
                data = data.OrderBy(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);

            return data;
        }
    }
}