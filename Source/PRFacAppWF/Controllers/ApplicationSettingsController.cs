﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PRFacAppWF.CodeHelpers;
using PRFacAppWF.models;
using PRFacAppWF.Repository.Services;

namespace PRFacAppWF.Controllers
{
    [HandleJsonExceptionAttribute]
    [Authorize]
    public class ApplicationSettingsController : BaseController
    {
        private readonly IPRSService prsService;

        public ApplicationSettingsController()
        {
            if (this.prsService == null)
                this.prsService = new PRSService();
        }

        /// <summary>
        /// Application settings index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            this.ViewBag.AllApplicationSettings = this.prsService.GetAllApplicationSettings();

            return View();
        }

        public PartialViewResult SearchApplicationSettings(DateTime Date, string SortCol = null, string SortDir = null)
        {
            List<ApplicationSettings> lstAS = new List<ApplicationSettings>();
            try
            {
                SortCol = SortCol == "" ? "FacilityName" : SortCol;
                SortDir = SortDir == "" ? "ASC" : SortDir;
                lstAS = this.prsService.GetApplicationSettingsList(Date, SortCol, SortDir);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return PartialView("partial/_listApplicationSetting", lstAS);
        }

        public ActionResult GetApplicationSettingsList(string sidx, string sord, int page, int rows, string SettingName)
        {
            try
            {
                var varSettingResults = this.prsService.SearchApplicationSettingsList(SettingName);
                int totalCount = varSettingResults.Count();
                var sortedResults = FilterApplicationSettingsGrid(varSettingResults, sidx, sord, page, rows);
                var pageSize = 10;
                var totalRecords = totalCount;
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

                var jsonData = new
                {
                    total = totalPages,
                    page = page,
                    records = totalRecords,
                    rows = (from lst in sortedResults
                            select new
                            {
                                i = lst.CAPApplicationParameterId,
                                cell = new string[]
                                {
                                    lst.CAPApplicationParameterId.ToString(),

                                    ///As per the Eric feedback, display time values in 12 hours format
                                    (lst.ValueUnit.ToString().Equals("time", StringComparison.OrdinalIgnoreCase) ? (DateTime.Parse(DateTime.Now.ToShortDateString() + " " + lst.ApplicationValue)).ToString("h:mm tt") : lst.ApplicationValue),
                                    lst.Startdate.HasValue ? lst.Startdate.Value.ToShortDateString():"",
                                    lst.Enddate.HasValue ? lst.Enddate.Value.ToShortDateString():"",
                                    lst.ValueUnit.ToString(),
                                    lst.ApplicationParameter.ToString()
                                }
                            }).ToArray()
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);

                var jsonData = new
                {
                    total = 0,
                    page = 0,
                    records = 0,
                    rows = new string[0]
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateApplicationSettingsRecords(string SettingName)
        {
            ViewBag.SettingName = SettingName;
            return View();
        }

        [HttpPost]
        public JsonResult UpdateApplicationSettingsRecords(FormCollection formCollection)
        {
            DateTime? endDate = null;
            string ApplicationValue = "";
            bool isNullEnddate = false;
            string result = string.Empty;
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                string Settingname = formCollection["hidSettingname"].ToString();
                DateTime startDate = Convert.ToDateTime(formCollection["Startdate"]);
                ApplicationValue = formCollection["ApplicationValue"].ToString();

                ///Saving time should be 24 hour format bcz this value is beeing used by Capcity system. For now dont want to disturb it.
                if (Settingname.Equals("Facility_AM_End_Time", StringComparison.OrdinalIgnoreCase) || Settingname.Equals("FacilityAMTime", StringComparison.OrdinalIgnoreCase) || Settingname.Equals("FacilityPMTime", StringComparison.OrdinalIgnoreCase))
                {
                    ApplicationValue = (DateTime.Parse(DateTime.Now.ToShortDateString() + " " + ApplicationValue)).ToString("HH:mm");
                }

                isNullEnddate = Convert.ToBoolean(formCollection["hidNullEndDate"]);
                if (!isNullEnddate)
                    endDate = Convert.ToDateTime(formCollection["Enddate"]);
                string lstAS = this.prsService.UpdateApplicationSettingsValues(CurrentUser.ID, Settingname, startDate, endDate, ApplicationValue, isNullEnddate);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };
            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<ApplicationSettings> FilterApplicationSettingsGrid(IEnumerable<ApplicationSettings> data, string sidx, string sord, int page, int rows)
        {

            int skiprows = (page - 1) * rows;
            ////Since in the UI we are displaying CapacityDate column as CapacityDateDisp.
            sidx = sidx == "" ? "Startdate" : sidx;
            if (sord == "desc")
                data = data.OrderByDescending(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);
            else
                data = data.OrderBy(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);

            return data;
        }
    }
}