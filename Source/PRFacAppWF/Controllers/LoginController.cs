﻿using PR.BusinessLogic;
using PR.LocalLogisticsSolution.Interfaces;
using PRFacAppWF.Entities;
using System;
using System.Linq;
using System.Web.Mvc;
using PRFacAppWF.CodeHelpers;
using PR.LocalLogisticsSolution.Model;
using PRFacAppWF.Repository.Services;
using PRFacAppWF.Repository.Services.Implementations;
using System.Web.Security;
using static PR.UtilityLibrary.PREnums;
using System.IO;
using PR.Entities;
using Newtonsoft.Json;

namespace PRFacAppWF.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        public IUserService _userService { get; set; }

        public LoginController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Index()
        {
            //If user already exists then we need not to redirect to login page instead redirect to Dashboard page
            if (CheckSessionAndRedirectDashBoard() == true)
                return null;

            return View();
        }

        /// <summary>
        /// If user trying to hit to login page again then we have to check his session and then try to use same session and redirect to Dashboard page
        /// </summary>
        public bool CheckSessionAndRedirectDashBoard()
        {
            bool sessionExists = false;

            if (Session["UserId"] != null && Session["LoginUserID"] != null && Convert.ToInt32(Session["LoginUserID"]) > 0)
            {
                Response.Redirect("~/DashBoard.aspx", false);
                sessionExists = true;
            }

            return sessionExists;
        }

        public string GetIPAddress
        {
            get
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }

                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
        }

        [HttpPost]
        public JsonResult Login(LoginRequest request)
        {
            var response = new AjaxResponse<string>();
            LocalComponent local = new LocalComponent();

            try
            {
                IUserSecurity oUserSecurity = new UserSecurityService();
                LogisticsUser user = new LogisticsUser();

                FADALoginStatus status = _userService.Login(request.LoginName.Trim(), PR.UtilityLibrary.CommonUtility.Encrypt(request.Password.Trim(), Helper.EnDeKey), GetIPAddress, PR.UtilityLibrary.PREnums.Application.FacilityApp, ref user);
                var passwordSettings = oUserSecurity.GetPasswordConfig();

                FormsAuthentication.SetAuthCookie(request.LoginName, false);
                DashboardSettings settings = new DashboardSettings();

                if (user != null && user.ID > 0 && status == FADALoginStatus.Success)
                {
                    if (user.Facilities.Count == 1)
                    {
                        //Facilitymanager with single facility
                        settings.MultipleFacalitiesAvailable = false;
                        settings.FacilitySelected = true;
                        settings.ChangeFacIconVisibility = "none";
                        Session["FaclocationCode"] = user.Facilities.ElementAt(0).LocationCode;
                        Session["FacilityName"] = user.Facilities.ElementAt(0).SiteName;
                        Session["ContactName"] = user.Facilities.ElementAt(0).ContactName;

                        //SFERP-TODO  --Added for the below lines of Code.
                        Session["WareHouseCode"] = user.Facilities.ElementAt(0).GlobalSiteNum.ToString();
                         
                        //DataSet dsFacility = local.GetRDFacilityData(Convert.ToString(Session["FaclocationCode"]));
                        //if (dsFacility.Tables.Count > 0 && dsFacility.Tables[0].Rows.Count > 0)
                        //{
                        //    Session["WareHouseCode"] = dsFacility.Tables[0].Rows[0]["StoreNo"].ToString();

                        //    //SFERP-TODO, commented for not in use, 
                        //    //settings.IsJaguarFacility = Convert.ToBoolean(dsFacility.Tables[0].Rows[0]["IsJaguarLoad"]);
                        //}

                    }
                    else
                    {
                        //Facilitymanager with single facility
                        settings.MultipleFacalitiesAvailable = true;
                        settings.FacilitySelected = false;
                        settings.ChangeFacIconVisibility = "block";
                    }
                }
                else
                {
                    if (status == FADALoginStatus.InvalidCredentials)
                        throw new Exception("Invalid login credentials!");
                    else if (status == FADALoginStatus.InactiveUser)
                        throw new Exception("Your login is inactive, Please contact Admin!");
                    else if (status == FADALoginStatus.InactiveRole)
                        throw new Exception("Your role group is inactive, Please contact Admin!");
                    else if (status == FADALoginStatus.NoApplicationAccessToYourRole)
                        throw new Exception("Your role group does not have access for this application, Please contact Admin!");
                    else
                        throw new Exception("Error occurred while login, Please contact Admin!");
                }

                //This session should be used throughout application to get current logged in user details
                Session[typeof(LogisticsUser).Name] = user;

                // Store inside cookie for Remember me functionality
                if (request.IsRememberMe == true)
                {
                    Response.Cookies["UName"].Value = request.LoginName;
                    Response.Cookies["PWD"].Value = request.Password;
                    Response.Cookies["UName"].Expires = DateTime.Now.AddMonths(2);
                    Response.Cookies["PWD"].Expires = DateTime.Now.AddMonths(2);
                }
                else
                {
                    Response.Cookies["UName"].Expires = DateTime.Now.AddMonths(-1);
                    Response.Cookies["PWD"].Expires = DateTime.Now.AddMonths(-1);
                }

                settings.CommitToSession();
                Session["UserId"] = request.LoginName;
                Session["LoginUserID"] = user.ID;

                //SFERP-TODO-CTUPD , Added by Sohan
                //Get ESB endpoint settings data from config file.
                Session["EsbEndPoint"] = GetEsbEndPointData();

                response.success = true;

                int DaysLeftToChangepassword = Convert.ToInt32((DateTime.Now - user.PasswordUpdatedDate).TotalDays);

                if (user.IsChangePasswordRequired || (DaysLeftToChangepassword >= passwordSettings.PasswordExpiresInDays))
                {
                    response.data = Url.Action("ChangePassword", "Login");
                }
                else
                    response.data = "../../Dashboard.aspx";
            }
            catch (Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
                Helper.LogError(ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        public JsonResult SaveChangePassword(ChangePasswordRequest request)
        {
            var response = new AjaxResponse<string>();
            try
            {
                IUserSecurity oUserSecurity = new UserSecurityService();
                var passwordSettings = oUserSecurity.GetPasswordConfig();
                var UserDetails = oUserSecurity.GetUserDetailsByUserName(Convert.ToString(Session["UserId"]));
                System.Text.RegularExpressions.Regex rgxPasswordCriteria = new System.Text.RegularExpressions.Regex(passwordSettings.PasswordCriteriaRegex);

                //checking old password is correct or not
                if (!oUserSecurity.CheckOldPassword(Convert.ToInt32(Session["LoginUserID"]), PR.UtilityLibrary.CommonUtility.Encrypt(request.OldPassword.Trim(), Helper.EnDeKey)))
                {
                    response.success = false;
                    response.message = "Current password is incorrect.";
                }
                else if (!rgxPasswordCriteria.IsMatch(request.NewPassword))
                {
                    response.success = false;
                    //response.message = "The password entered needs to be 7 or more characters in length and contain following criteria: Uppercase character, Lowercase Character, Number, Special Character.";
                    response.message = "Password not strong enough. <br/> Password must include: <br/> \u2022 7 characters or more <br/> \u2022 Uppercase Letter <br/> \u2022 Lowercase Letter<br/> \u2022 Number <br/> \u2022 Special Character (!,@,#)</li></ol>";
                }
                else if (!request.NewPassword.Equals(request.RetypeNewPassword, StringComparison.OrdinalIgnoreCase))
                {
                    response.success = false;
                    response.message = "New password and confirm password should match.";
                }
                else if (request.OldPassword.Equals(request.NewPassword, StringComparison.OrdinalIgnoreCase))
                {
                    response.success = false;
                    response.message = "Current password and new password should not be same, please try different password.";
                }
                else if (request.NewPassword.ToLower().Contains(UserDetails.Username.ToLower()) || request.NewPassword.ToLower().Contains(UserDetails.FirstName.ToLower()) || request.NewPassword.ToLower().Contains(UserDetails.LastName.ToLower()))
                {
                    response.success = false;
                    response.message = "New password should not contain UserName, First Name or Last Name, please try different password.";
                }
                else if (request.NewPassword.Trim().Length < passwordSettings.PasswordMinLength)
                {
                    response.success = false;
                    response.message = string.Format("Password length should be atleast {0} characters length.", passwordSettings.PasswordMinLength);
                }
                else if (oUserSecurity.IsPasswordAlreadyUsed(Convert.ToInt32(Session["LoginUserID"]), PR.UtilityLibrary.CommonUtility.Encrypt(request.NewPassword.Trim(), Helper.EnDeKey), Convert.ToInt32(passwordSettings.LastNumberOfPasswords)))
                {
                    response.success = false;
                    response.message = string.Format("Given password was used as one of passwords in last {0} times, please try different password.", passwordSettings.LastNumberOfPasswords);
                }
                else
                {
                    //save new password functionality and log history
                    oUserSecurity.UpdateNewPassword(Convert.ToInt32(Session["LoginUserID"]), PR.UtilityLibrary.CommonUtility.Encrypt(request.NewPassword.Trim(), Helper.EnDeKey), PR.UtilityLibrary.CommonUtility.Encrypt(request.OldPassword.Trim(), Helper.EnDeKey), false);
                    response.success = true;
                    response.message = "Password changed successfully.";
                    response.data = Url.Action("Logout", "Home");
                }
            }
            catch (Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
                Helper.LogError(ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SentPasswordRequest(ForgotPasswordRequest request)
        {
            var response = new AjaxResponse<string>();

            try
            {
                IUserSecurity oUserSecurity = new UserSecurityService();
                var UserDetails = oUserSecurity.GetUserDetailsByUserName(request.UserName);
                var passwordSettings = oUserSecurity.GetPasswordConfig();

                if (UserDetails == null)
                {
                    response.success = false;
                    response.message = "User not found.";
                }
                else if (UserDetails.IsActive == false)
                {
                    response.success = false;
                    response.message = "Your account is inactive, please contact an application administrator for reactivation.";
                }
                else
                {
                    if (passwordSettings.PasswordMethod.Equals("Auto", StringComparison.OrdinalIgnoreCase))
                    {
                        if (UserDetails.EmailAddress.Contains("@1800packrat.com"))
                        {
                            string NewPassword = RandomPassword.Generate(7, 10);
                            oUserSecurity.UpdateNewPassword(UserDetails.PK_UserId, PR.UtilityLibrary.CommonUtility.Encrypt(NewPassword.Trim(), Helper.EnDeKey), UserDetails.Password, true);
                            SendAutoPasswordEmail(UserDetails.EmailAddress, UserDetails.Username, NewPassword);
                            response.success = true;
                            response.message = "Auto";
                            response.data = Url.Action("Logout", "Home");
                        }
                        else
                        {
                            response.success = false;
                            response.message = "You're email address isn't valid. Please contact your application administrator for correction.";
                            response.data = Url.Action("Logout", "Home");
                        }
                    }
                    else
                    {
                        SendEmail(passwordSettings.ForgotEmailUserGroupName, UserDetails.EmailAddress, UserDetails.Username);
                        response.success = true;
                        response.data = Url.Action("Logout", "Home");
                    }
                }
            }
            catch (Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
                Helper.LogError(ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private void SendEmail(string mailTo, string userEmail, string userName)
        {
            string mailFrom = System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString();

            System.Text.StringBuilder mailBody = new System.Text.StringBuilder();
            mailBody.AppendFormat("<h1>Facility App Password Reset – {0}</h1>", userName);
            // mailBody.AppendFormat("Hi, ");
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<p>Login:<b> {0} </b>has forgotten their password and is requesting a reset."
                + " Please update the user’s password in Facility App and send a notification  (to email address {1}) identifying the new password.</p>", userName, userEmail);

            string mailSubject = string.Format("Facility App Password Reset – {0}", userName);

            MailManager.SendEmail(mailFrom, mailTo, mailSubject, mailBody.ToString());
        }

        private void SendAutoPasswordEmail(string userEmail, string userName, string Password)
        {
            string mailFrom = System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString();

            System.Text.StringBuilder mailBody = new System.Text.StringBuilder();
            mailBody.AppendFormat("<h1>Forgotten Password Request – Facility App</h1>");
            // mailBody.AppendFormat("Hi, ");
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<p>Username <b>{0}</b> has requested a password reset. You’re new password is <b>{1}</b> and you will be requested to change your password upon your next login.</p>", userName, Password);


            MailManager.SendEmail(mailFrom, userEmail, "Forgotten Password Request – Facility App", mailBody.ToString());
        }

        private EsbEndPointConfig GetEsbEndPointData()
        {
            return JsonConvert.DeserializeObject<EsbEndPointConfig>(this.LoadJsonConfig()["EsbEndPointSettings"].ToString());
        }
         
        private Newtonsoft.Json.Linq.JToken LoadJsonConfig()
        {
            using (StreamReader jsonConfig = new StreamReader(Server.MapPath(@"~/SiteConfig.json")))
            {
                string json = jsonConfig.ReadToEnd();
                return Newtonsoft.Json.Linq.JObject.Parse(json);
            }
        }

    }
}