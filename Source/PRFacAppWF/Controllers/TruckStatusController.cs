﻿namespace PRFacAppWF.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using PRFacAppWF.CodeHelpers;
    using PRFacAppWF.models;
    using PRFacAppWF.Repository.Services;
    using PR.LocalLogisticsSolution.Interfaces;
    using PR.Entities;
    using System.Data;

    public class TruckStatusController : BaseController
    {
        private readonly IPRSService prsService;
        private IUserService _userService = null;
        private ITouchService _touchService = null;

        public TruckStatusController(IUserService userService, ITouchService touchService)
        {
            if (this.prsService == null)
                this.prsService = new PRSService();

            _userService = userService;
            _touchService = touchService;
        }


        public ActionResult Index()
        {
            TruckStatusDetails truckStatusDetails = new TruckStatusDetails();
            EditSelects();

            return View(truckStatusDetails);
        }

        [HttpPost]
        public ActionResult Index(TruckStatusDetails truckStatusDetails)
        {
            try
            {
                int truckDetailsId = 0;

                //If TruckDetailsId is > 0 means here user wants to update existing Status Entry
                if (truckStatusDetails.TruckDetailsId <= 0)
                    truckDetailsId = this.prsService.CheckTruckStatusEntryExists(truckStatusDetails.TruckNumber);

                if (truckDetailsId > 0)
                {
                    truckStatusDetails.TruckDetailsId = truckDetailsId;

                    EditSelects();

                    return View(truckStatusDetails);
                }
                else
                {
                    truckStatusDetails.Createdby = Convert.ToString(Session["UserId"]);
                    truckStatusDetails.UpdatedBy = Convert.ToString(Session["UserId"]);
                    truckStatusDetails.Comments = string.IsNullOrEmpty(truckStatusDetails.Comments) ? string.Empty : truckStatusDetails.Comments;
                    truckStatusDetails.TransferStoreNo = truckStatusDetails.TransferStoreNo > 0 ? truckStatusDetails.TransferStoreNo : CurrentSiteInfo.SiteId;

                    if (truckStatusDetails.IsFleetTransfer == false)
                    {
                        truckStatusDetails.TransferValidFrom = null;
                        truckStatusDetails.TransferValidTo = null;
                    }
                    else if (truckStatusDetails.IsFleetTransfer == true && truckStatusDetails.IsTransferTemparory == false)
                    {
                        truckStatusDetails.TransferValidFrom = DateTime.Now;
                        truckStatusDetails.TransferValidTo = null;
                    }

                    this.prsService.SaveTruckStatusDetails(truckStatusDetails);

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                ModelState.AddModelError("error.error", ex.Message);

                EditSelects();

                return View(truckStatusDetails);
            }
        }

        public ActionResult List()
        {
            EditSelects();

            return View();
        }

        public ActionResult SearchTruckStatusList()
        {
            SearchFilter<TruckStatusFilterCriteria> tsSearchFilter = new SearchFilter<TruckStatusFilterCriteria>();
            tsSearchFilter.filter = new TruckStatusFilterCriteria();

            tsSearchFilter.rows = Convert.ToInt32(Request.Params["rows"]);
            tsSearchFilter.page = Convert.ToInt32(Request.Params["page"]);
            tsSearchFilter.sidx = Request.Params["sidx"];
            tsSearchFilter.sord = Request.Params["sord"];
            string filter = Request.Params["filter"];

            if (!string.IsNullOrEmpty(filter))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                tsSearchFilter.filter = serializer.Deserialize<TruckStatusFilterCriteria>(filter);
            }
            return GetTruckStatusDetailLst(tsSearchFilter);
        }

        private ActionResult GetTruckStatusDetailLst(SearchFilter<TruckStatusFilterCriteria> tsSearchFilter)
        {
            tsSearchFilter.filter.EnterdBy = Convert.ToString(Session["UserId"]);
            tsSearchFilter.filter.UpdatedBy = Convert.ToString(Session["UserId"]);

            var res = this.prsService.GetTruckStatusDetailList(tsSearchFilter);
            SearchResults<TruckStatusInfo> rtObj = new SearchResults<TruckStatusInfo>();
            rtObj.rows = res.ToList();
            rtObj.page = tsSearchFilter.page;

            int records = (rtObj.rows.Count() > 0 ? rtObj.rows[0].TotalRows : 0);
            float totPages = (records > 0 ? ((float)records / tsSearchFilter.rows) : 1);
            int totpages = (int)Math.Ceiling(totPages);
            rtObj.records = records;
            rtObj.total = totpages;
            rtObj.error = "";

            return this.Json(rtObj);
        }

        public ActionResult ExportTruckStatus(TruckStatusFilterCriteria filter)
        {
            SearchFilter<TruckStatusFilterCriteria> tsSearchFilter = new SearchFilter<TruckStatusFilterCriteria>();
            tsSearchFilter.filter = new TruckStatusFilterCriteria();
            tsSearchFilter.rows = 100000;
            tsSearchFilter.page = 1;
            tsSearchFilter.sidx = Request.Params["sidx"];
            tsSearchFilter.sord = Request.Params["sord"];

            if (filter != null)
            {
                tsSearchFilter.filter = filter;
                tsSearchFilter.sidx = filter.sidx;
                tsSearchFilter.sord = filter.sord;
            }

            var lstTS = this.prsService.GetTruckStatusDetailList(tsSearchFilter);

            DataGrid dg = new DataGrid();

            dg.DataSource = (from ts in lstTS
                             select new
                             {
                                 Facility = string.IsNullOrEmpty(ts.Facility) ? "" : ts.Facility,
                                 EntryDateDisp = string.IsNullOrEmpty(ts.EntryDateDisp) ? "" : ts.EntryDateDisp,
                                 EnterdBy = string.IsNullOrEmpty(ts.EnterdBy) ? "" : ts.EnterdBy,
                                 FleetID = string.IsNullOrEmpty(ts.FleetID) ? "" : ts.FleetID,
                                 Status = string.IsNullOrEmpty(ts.TruckStatus) ? "" : ts.TruckStatus,
                                 Comments = string.IsNullOrEmpty(ts.Comments) ? "" : ts.Comments
                             }).ToList();
            dg.DataBind();

            string attachment = "attachment;filename=FleetStatus_" + DateTime.Now.ToString("MMddyyy") + ".xls";

            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";

            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", attachment);

            dg.GridLines = GridLines.Both;
            dg.HeaderStyle.Font.Bold = true;
            dg.RenderControl(htmltextwrtter);

            Response.Write(strwritter.ToString());
            Response.End();

            return new EmptyResult();
        }

        private void EditSelects()
        {
            this.ViewBag.TruckStatuses = this.prsService.GetTruckStatus();
            this.ViewBag.TruckNumbers = this.prsService.GetTruckDetails(CurrentSiteInfo.GlobalSiteNum, DateTime.Now);
            this.ViewBag.AllFacilities = _userService.GetAllFacilities().Where(t => t.GlobalSiteNum != CurrentSiteInfo.GlobalSiteNum);

            DashboardSettings settings = DashboardSettings.RetrieveFromSession();
        }

        string UserFacilityName
        {
            get { return Convert.ToString(Session["FacilityName"]); }
        }

        #region Capacity Methods

        public ActionResult GetTempHourChange(string sidx, string sord, int page, int rows, int truckId, DateTime startDate, bool _search, string weekday)
        {
            try
            {
                int wkday = 0;
                int.TryParse(weekday, out wkday);
                var truckOpTempHoursRaw = this.prsService.GetTruckTempOpHourDetails(Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum), startDate, rows, truckId, wkday);
                int totalCount = truckOpTempHoursRaw.Count();
                var truckOpTempHours = FilterGrid(truckOpTempHoursRaw, sidx, sord, page, rows);


                var pageSize = rows;
                var totalRecords = totalCount;
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);


                var jsonData = new
                {
                    total = totalPages,
                    page = page,
                    records = totalRecords,
                    rows = truckOpTempHours
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);

                var jsonData = new
                {
                    total = 0,
                    page = 0,
                    records = 0,
                    rows = new string[0]
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveTempHourChange(FormCollection collection)
        {
            string errMsg = string.Empty;
            bool success = true;

            try
            {
                List<TruckTempHourChange> listTempHourChangeEntry = new List<TruckTempHourChange>();
                var lstTruckOpTempHourDates = collection["dates[]"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                TruckTempHourChange tempHourChangeEntry;

                foreach (var date in lstTruckOpTempHourDates)
                {
                    DateTime capcityDate = DateTime.Parse(date);
                    if (DateTime.Now < capcityDate)
                    {
                        tempHourChangeEntry = new TruckTempHourChange();

                        tempHourChangeEntry.StartTime = collection["StartTime"];
                        tempHourChangeEntry.EndTime = collection["EndTime"];
                        tempHourChangeEntry.StoreOpen = collection["StoreOpen"];
                        tempHourChangeEntry.CapacityDate = DateTime.Parse(date);
                        tempHourChangeEntry.TruckId = Convert.ToInt32(collection["TruckId"]);

                        listTempHourChangeEntry.Add(tempHourChangeEntry);
                    }
                }

                if (listTempHourChangeEntry.Count > 0)
                    this.prsService.SaveTempHourChange(listTempHourChangeEntry, Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum), CurrentUser.ID);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };

            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RevertToFacilityOperatingHours(List<string> collection, string TruckId)
        {
            string errMsg = string.Empty;
            bool success = true;

            try
            {
                List<TruckTempHourChange> listTempHourChangeEntry = new List<TruckTempHourChange>();

                TruckTempHourChange tempHourChangeEntry;

                foreach (var date in collection)
                {
                    DateTime capcityDate = DateTime.Parse(date);
                    if (DateTime.Now < capcityDate)
                    {
                        tempHourChangeEntry = new TruckTempHourChange();


                        tempHourChangeEntry.CapacityDate = DateTime.Parse(date);
                        tempHourChangeEntry.TruckId = Convert.ToInt32(TruckId);

                        listTempHourChangeEntry.Add(tempHourChangeEntry);
                    }
                }

                if (listTempHourChangeEntry.Count > 0)
                    this.prsService.RevertToFacilityOperatingHours(listTempHourChangeEntry, Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum), CurrentUser.ID);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };

            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<TruckTempHourChange> FilterGrid(IEnumerable<TruckTempHourChange> data, string sidx, string sord, int page, int rows)
        {
            //if (weekday == null || weekday == "00")
            //{
            //    //data = data; 
            //}
            //else if (weekday == "15")
            //{
            //    data = (from d in data where d.WeekDay != DayOfWeek.Sunday.ToString() && d.WeekDay != DayOfWeek.Saturday.ToString() select d).ToList();
            //}
            //else if (weekday == "07")
            //{
            //    data = (from d in data where d.WeekDay == DayOfWeek.Sunday.ToString() || d.WeekDay == DayOfWeek.Saturday.ToString() select d).ToList();
            //}
            //else
            //{
            //    data = (from d in data where d.WeekDay == Enum.GetName(typeof(DayOfWeek), Convert.ToInt32(weekday)) select d).ToList();
            //}

            //Thsi total we are showing in grid and it should be after filter before page filter

            int skiprows = (page - 1) * rows;
            //Since in the UI we are displaying CapacityDate column as CapacityDateDisp.
            sidx = sidx.ToLower().Contains("capacitydate") ? "CapacityDate" : sidx;

            if (sord == "desc")
                data = data.OrderByDescending(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);
            else
                data = data.OrderBy(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);

            return data;
        }

        public ActionResult TruckOpHours()
        {

            try
            {
                ViewBag.Facilities = this.prsService.GetFacilitiesList().OrderBy(x => x.CompDBAName);

                ViewBag.TruckOHReason = this.prsService.GetTruckOHReason().Where(x => x.Status == 1);

                ViewBag.FacilityNumber = Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum);

                ViewBag.TransferReason = this.prsService.GetTransferReason();
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return View();
        }

        ////SFERP-TODO-CTUPD
        ////Code to be updated based on capacity data from salesforce.
        //private List<DayWiseFacilityCapacity> PraseFacilityCapacity(DateTime startDate, DataTable dsCapacity)
        //{
        //    List<DayWiseFacilityCapacity> lstMarketStatistics = new List<DayWiseFacilityCapacity>();

        //    DayWiseFacilityCapacity dayMAxCapacityMarketStatistics = new DayWiseFacilityCapacity();
        //    dayMAxCapacityMarketStatistics.Head = "Maximum Capacity";

        //    DayWiseFacilityCapacity dayOMSetCapacityMarketStatistics = new DayWiseFacilityCapacity();
        //    dayOMSetCapacityMarketStatistics.Head = "OM Set Capacity";

        //    DayWiseFacilityCapacity dayAvailableMarketStatistics = new DayWiseFacilityCapacity();
        //    dayAvailableMarketStatistics.Head = "Percentage";

        //    DayWiseFacilityCapacity dayBookedMarketStatistics = new DayWiseFacilityCapacity();
        //    dayBookedMarketStatistics.Head = "Scheduled";

        //    DayWiseFacilityCapacity dayOpenMarketStatistics = new DayWiseFacilityCapacity();
        //    dayOpenMarketStatistics.Head = "Available";

        //    DataRow day1Capacity = dsCapacity.Select("TouchTime='AnyTime' AND CAPDate='" + startDate.ToString("MM/dd/yyy") + "'")[0];
        //    dayMAxCapacityMarketStatistics.Day1 = Convert.ToDecimal(day1Capacity["FullFacilityMiles"]);
        //    dayOMSetCapacityMarketStatistics.Day1 = Convert.ToDecimal(day1Capacity["FacilityMiles"]);
        //    dayAvailableMarketStatistics.Day1 = Convert.ToDecimal(day1Capacity["FullFacilityMiles"]) > 0 ? (Convert.ToDecimal(day1Capacity["FacilityMiles"]) / Convert.ToDecimal(day1Capacity["FullFacilityMiles"])) * 100 : 0;
        //    dayBookedMarketStatistics.Day1 = Convert.ToDecimal(day1Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day1Capacity["BookedMiles"]) : 0;
        //    dayOpenMarketStatistics.Day1 = (Convert.ToDecimal(day1Capacity["FacilityMiles"]) - (Convert.ToDecimal(day1Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day1Capacity["BookedMiles"]) : 0));

        //    DataRow day2Capacity = dsCapacity.Select("TouchTime='AnyTime' AND CAPDate='" + startDate.ToString("MM/dd/yyy") + "'")[0];
        //    dayMAxCapacityMarketStatistics.Day2 = Convert.ToDecimal(day2Capacity["FullFacilityMiles"]);
        //    dayOMSetCapacityMarketStatistics.Day2 = Convert.ToDecimal(day2Capacity["FacilityMiles"]) > 0 ? Convert.ToDecimal(day2Capacity["FacilityMiles"]) : 0;
        //    dayAvailableMarketStatistics.Day2 = Convert.ToDecimal(day2Capacity["FullFacilityMiles"]) > 0 ? (Convert.ToDecimal(day2Capacity["FacilityMiles"]) / Convert.ToDecimal(day2Capacity["FullFacilityMiles"])) * 200 : 0;
        //    dayBookedMarketStatistics.Day2 = Convert.ToDecimal(day2Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day2Capacity["BookedMiles"]) : 0;
        //    dayOpenMarketStatistics.Day2 = (Convert.ToDecimal(day2Capacity["FacilityMiles"]) - (Convert.ToDecimal(day2Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day2Capacity["BookedMiles"]) : 0));

        //    DataRow day3Capacity = dsCapacity.Select("TouchTime='AnyTime' AND CAPDate='" + startDate.ToString("MM/dd/yyy") + "'")[0];
        //    dayMAxCapacityMarketStatistics.Day3 = Convert.ToDecimal(day3Capacity["FullFacilityMiles"]);
        //    dayOMSetCapacityMarketStatistics.Day3 = Convert.ToDecimal(day3Capacity["FacilityMiles"]) > 0 ? Convert.ToDecimal(day3Capacity["FacilityMiles"]) : 0;
        //    dayAvailableMarketStatistics.Day3 = Convert.ToDecimal(day3Capacity["FullFacilityMiles"]) > 0 ? (Convert.ToDecimal(day3Capacity["FacilityMiles"]) / Convert.ToDecimal(day3Capacity["FullFacilityMiles"])) * 300 : 0;
        //    dayBookedMarketStatistics.Day3 = Convert.ToDecimal(day3Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day3Capacity["BookedMiles"]) : 0;
        //    dayOpenMarketStatistics.Day3 = (Convert.ToDecimal(day3Capacity["FacilityMiles"]) - (Convert.ToDecimal(day3Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day3Capacity["BookedMiles"]) : 0));

        //    DataRow day4Capacity = dsCapacity.Select("TouchTime='AnyTime' AND CAPDate='" + startDate.ToString("MM/dd/yyy") + "'")[0];
        //    dayMAxCapacityMarketStatistics.Day4 = Convert.ToDecimal(day4Capacity["FullFacilityMiles"]);
        //    dayOMSetCapacityMarketStatistics.Day4 = Convert.ToDecimal(day4Capacity["FacilityMiles"]) > 0 ? Convert.ToDecimal(day4Capacity["FacilityMiles"]) : 0;
        //    dayAvailableMarketStatistics.Day4 = Convert.ToDecimal(day4Capacity["FullFacilityMiles"]) > 0 ? (Convert.ToDecimal(day4Capacity["FacilityMiles"]) / Convert.ToDecimal(day4Capacity["FullFacilityMiles"])) * 400 : 0;
        //    dayBookedMarketStatistics.Day4 = Convert.ToDecimal(day4Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day4Capacity["BookedMiles"]) : 0;
        //    dayOpenMarketStatistics.Day4 = (Convert.ToDecimal(day4Capacity["FacilityMiles"]) - (Convert.ToDecimal(day4Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day4Capacity["BookedMiles"]) : 0));

        //    DataRow day5Capacity = dsCapacity.Select("TouchTime='AnyTime' AND CAPDate='" + startDate.ToString("MM/dd/yyy") + "'")[0];
        //    dayMAxCapacityMarketStatistics.Day5 = Convert.ToDecimal(day5Capacity["FullFacilityMiles"]);
        //    dayOMSetCapacityMarketStatistics.Day5 = Convert.ToDecimal(day5Capacity["FacilityMiles"]) > 0 ? Convert.ToDecimal(day5Capacity["FacilityMiles"]) : 0;
        //    dayAvailableMarketStatistics.Day5 = Convert.ToDecimal(day5Capacity["FullFacilityMiles"]) > 0 ? (Convert.ToDecimal(day5Capacity["FacilityMiles"]) / Convert.ToDecimal(day5Capacity["FullFacilityMiles"])) * 500 : 0;
        //    dayBookedMarketStatistics.Day5 = Convert.ToDecimal(day5Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day5Capacity["BookedMiles"]) : 0;
        //    dayOpenMarketStatistics.Day5 = (Convert.ToDecimal(day5Capacity["FacilityMiles"]) - (Convert.ToDecimal(day5Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day5Capacity["BookedMiles"]) : 0));

        //    DataRow day6Capacity = dsCapacity.Select("TouchTime='AnyTime' AND CAPDate='" + startDate.ToString("MM/dd/yyy") + "'")[0];
        //    dayMAxCapacityMarketStatistics.Day6 = Convert.ToDecimal(day6Capacity["FullFacilityMiles"]);
        //    dayOMSetCapacityMarketStatistics.Day6 = Convert.ToDecimal(day6Capacity["FacilityMiles"]) > 0 ? Convert.ToDecimal(day6Capacity["FacilityMiles"]) : 0;
        //    dayAvailableMarketStatistics.Day6 = Convert.ToDecimal(day6Capacity["FullFacilityMiles"]) > 0 ? (Convert.ToDecimal(day6Capacity["FacilityMiles"]) / Convert.ToDecimal(day6Capacity["FullFacilityMiles"])) * 600 : 0;
        //    dayBookedMarketStatistics.Day6 = Convert.ToDecimal(day6Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day6Capacity["BookedMiles"]) : 0;
        //    dayOpenMarketStatistics.Day6 = (Convert.ToDecimal(day6Capacity["FacilityMiles"]) - (Convert.ToDecimal(day6Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day6Capacity["BookedMiles"]) : 0));

        //    DataRow day7Capacity = dsCapacity.Select("TouchTime='AnyTime' AND CAPDate='" + startDate.ToString("MM/dd/yyy") + "'")[0];
        //    dayMAxCapacityMarketStatistics.Day7 = Convert.ToDecimal(day7Capacity["FullFacilityMiles"]);
        //    dayOMSetCapacityMarketStatistics.Day7 = Convert.ToDecimal(day7Capacity["FacilityMiles"]) > 0 ? Convert.ToDecimal(day7Capacity["FacilityMiles"]) : 0;
        //    dayAvailableMarketStatistics.Day7 = Convert.ToDecimal(day7Capacity["FullFacilityMiles"]) > 0 ? (Convert.ToDecimal(day7Capacity["FacilityMiles"]) / Convert.ToDecimal(day7Capacity["FullFacilityMiles"])) * 700 : 0;
        //    dayBookedMarketStatistics.Day7 = Convert.ToDecimal(day7Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day7Capacity["BookedMiles"]) : 0;
        //    dayOpenMarketStatistics.Day7 = (Convert.ToDecimal(day7Capacity["FacilityMiles"]) - (Convert.ToDecimal(day7Capacity["BookedMiles"]) > 0 ? Convert.ToDecimal(day7Capacity["BookedMiles"]) : 0));

        //    lstMarketStatistics.Add(dayMAxCapacityMarketStatistics);
        //    lstMarketStatistics.Add(dayOMSetCapacityMarketStatistics);
        //    lstMarketStatistics.Add(dayAvailableMarketStatistics);
        //    lstMarketStatistics.Add(dayBookedMarketStatistics);
        //    lstMarketStatistics.Add(dayOpenMarketStatistics);

        //    return lstMarketStatistics;
        //}


        //SFERP-TODO-CTUPD
        //Code to be updated based on capacity data from salesforce.
        private List<DayWiseFacilityCapacity> PraseFacilityCapacity(DateTime startDate, ScheduleCalendar dsCapacity)
        {
            List<DayWiseFacilityCapacity> lstMarketStatistics = new List<DayWiseFacilityCapacity>();

            DayWiseFacilityCapacity dayMAxCapacityMarketStatistics = new DayWiseFacilityCapacity();
            dayMAxCapacityMarketStatistics.Head = "Maximum Capacity";

            DayWiseFacilityCapacity dayOMSetCapacityMarketStatistics = new DayWiseFacilityCapacity();
            dayOMSetCapacityMarketStatistics.Head = "OM Set Capacity";

            DayWiseFacilityCapacity dayAvailableMarketStatistics = new DayWiseFacilityCapacity();
            dayAvailableMarketStatistics.Head = "Percentage";

            DayWiseFacilityCapacity dayBookedMarketStatistics = new DayWiseFacilityCapacity();
            dayBookedMarketStatistics.Head = "Scheduled";

            DayWiseFacilityCapacity dayOpenMarketStatistics = new DayWiseFacilityCapacity();
            dayOpenMarketStatistics.Head = "Available";

            var day1Capacity = dsCapacity.AnyTime.Where(s => s.Date.ToShortDateString() == startDate.ToShortDateString()).First();
            dayMAxCapacityMarketStatistics.Day1 = day1Capacity.FullFacilityMiles;
            dayOMSetCapacityMarketStatistics.Day1 = day1Capacity.FacilityMiles;
            dayAvailableMarketStatistics.Day1 = day1Capacity.FullFacilityMiles > 0 ? (day1Capacity.FacilityMiles / day1Capacity.FullFacilityMiles) * 100 : 0;
            dayBookedMarketStatistics.Day1 = day1Capacity.IsBookedMilesNull() ? 0 : day1Capacity.BookedMiles;
            dayOpenMarketStatistics.Day1 = (day1Capacity.FacilityMiles - (day1Capacity.IsBookedMilesNull() ? 0 : day1Capacity.BookedMiles));

            var day2Capacity = dsCapacity.AnyTime.Where(s => s.Date.ToShortDateString() == startDate.AddDays(1).ToShortDateString()).First();
            dayMAxCapacityMarketStatistics.Day2 = day2Capacity.FullFacilityMiles;
            dayOMSetCapacityMarketStatistics.Day2 = day2Capacity.IsFacilityMilesNull() ? 0 : day2Capacity.FacilityMiles;
            dayAvailableMarketStatistics.Day2 = day2Capacity.FullFacilityMiles > 0 ? (day2Capacity.FacilityMiles / day2Capacity.FullFacilityMiles) * 100 : 0;
            dayBookedMarketStatistics.Day2 = day2Capacity.IsBookedMilesNull() ? 0 : day2Capacity.BookedMiles;
            dayOpenMarketStatistics.Day2 = (day2Capacity.FacilityMiles - (day2Capacity.IsBookedMilesNull() ? 0 : day2Capacity.BookedMiles));

            var day3Capacity = dsCapacity.AnyTime.Where(s => s.Date.ToShortDateString() == startDate.AddDays(2).ToShortDateString()).First();
            dayMAxCapacityMarketStatistics.Day3 = day3Capacity.FullFacilityMiles;
            dayOMSetCapacityMarketStatistics.Day3 = day3Capacity.IsFacilityMilesNull() ? 0 : day3Capacity.FacilityMiles;
            dayAvailableMarketStatistics.Day3 = day3Capacity.FullFacilityMiles > 0 ? (day3Capacity.FacilityMiles / day3Capacity.FullFacilityMiles) * 100 : 0;
            dayBookedMarketStatistics.Day3 = day3Capacity.IsBookedMilesNull() ? 0 : day3Capacity.BookedMiles;
            dayOpenMarketStatistics.Day3 = (day3Capacity.FacilityMiles - (day3Capacity.IsBookedMilesNull() ? 0 : day3Capacity.BookedMiles));

            var day4Capacity = dsCapacity.AnyTime.Where(s => s.Date.ToShortDateString() == startDate.AddDays(3).ToShortDateString()).First();
            dayMAxCapacityMarketStatistics.Day4 = day4Capacity.FullFacilityMiles;
            dayOMSetCapacityMarketStatistics.Day4 = day4Capacity.IsFacilityMilesNull() ? 0 : day4Capacity.FacilityMiles;
            dayAvailableMarketStatistics.Day4 = day4Capacity.FullFacilityMiles > 0 ? (day4Capacity.FacilityMiles / day4Capacity.FullFacilityMiles) * 100 : 0;
            dayBookedMarketStatistics.Day4 = day4Capacity.IsBookedMilesNull() ? 0 : day4Capacity.BookedMiles;
            dayOpenMarketStatistics.Day4 = (day4Capacity.FacilityMiles - (day4Capacity.IsBookedMilesNull() ? 0 : day4Capacity.BookedMiles));

            var day5Capacity = dsCapacity.AnyTime.Where(s => s.Date.ToShortDateString() == startDate.AddDays(4).ToShortDateString()).First();
            dayMAxCapacityMarketStatistics.Day5 = day5Capacity.FullFacilityMiles;
            dayOMSetCapacityMarketStatistics.Day5 = (day5Capacity.IsFacilityMilesNull() ? 0 : day5Capacity.FacilityMiles);
            dayAvailableMarketStatistics.Day5 = day5Capacity.FullFacilityMiles > 0 ? (day5Capacity.FacilityMiles / day5Capacity.FullFacilityMiles) * 100 : 0;
            dayBookedMarketStatistics.Day5 = day5Capacity.IsBookedMilesNull() ? 0 : day5Capacity.BookedMiles;
            dayOpenMarketStatistics.Day5 = (day5Capacity.FacilityMiles - (day5Capacity.IsBookedMilesNull() ? 0 : day5Capacity.BookedMiles));

            var day6Capacity = dsCapacity.AnyTime.Where(s => s.Date.ToShortDateString() == startDate.AddDays(5).ToShortDateString()).First();
            dayMAxCapacityMarketStatistics.Day6 = day6Capacity.FullFacilityMiles;
            dayOMSetCapacityMarketStatistics.Day6 = day6Capacity.IsFacilityMilesNull() ? 0 : day6Capacity.FacilityMiles;
            dayAvailableMarketStatistics.Day6 = day6Capacity.FullFacilityMiles > 0 ? (day6Capacity.FacilityMiles / day6Capacity.FullFacilityMiles) * 100 : 0;
            dayBookedMarketStatistics.Day6 = day6Capacity.IsBookedMilesNull() ? 0 : day6Capacity.BookedMiles;
            dayOpenMarketStatistics.Day6 = (day6Capacity.FacilityMiles - (day6Capacity.IsBookedMilesNull() ? 0 : day6Capacity.BookedMiles));

            var day7Capacity = dsCapacity.AnyTime.Where(s => s.Date.ToShortDateString() == startDate.AddDays(6).ToShortDateString()).First();
            dayMAxCapacityMarketStatistics.Day7 = day7Capacity.FullFacilityMiles;
            dayOMSetCapacityMarketStatistics.Day7 = day7Capacity.IsFacilityMilesNull() ? 0 : day7Capacity.FacilityMiles;
            dayAvailableMarketStatistics.Day7 = day7Capacity.FullFacilityMiles > 0 ? (day7Capacity.FacilityMiles / day7Capacity.FullFacilityMiles) * 100 : 0;
            dayBookedMarketStatistics.Day7 = (day7Capacity.IsBookedMilesNull() ? 0 : day7Capacity.BookedMiles);
            dayOpenMarketStatistics.Day7 = (day7Capacity.FacilityMiles - (day7Capacity.IsBookedMilesNull() ? 0 : day7Capacity.BookedMiles));


            lstMarketStatistics.Add(dayMAxCapacityMarketStatistics);
            lstMarketStatistics.Add(dayOMSetCapacityMarketStatistics);
            lstMarketStatistics.Add(dayAvailableMarketStatistics);
            lstMarketStatistics.Add(dayBookedMarketStatistics);
            lstMarketStatistics.Add(dayOpenMarketStatistics);

            return lstMarketStatistics;
        }

        private void ParseFacilityOpHours(List<FacilityOpHours> lstFacilityOpHours, out FacilityCapacity facilityHrRow)
        {
            facilityHrRow = new FacilityCapacity();
            facilityHrRow.Head = "Facility Hours";

            facilityHrRow.oDay1 = new TruckOHDay(lstFacilityOpHours.ElementAt(0).FacilityOperatingMiuntes);
            facilityHrRow.oDay2 = new TruckOHDay(lstFacilityOpHours.ElementAt(1).FacilityOperatingMiuntes);
            facilityHrRow.oDay3 = new TruckOHDay(lstFacilityOpHours.ElementAt(2).FacilityOperatingMiuntes);
            facilityHrRow.oDay4 = new TruckOHDay(lstFacilityOpHours.ElementAt(3).FacilityOperatingMiuntes);
            facilityHrRow.oDay5 = new TruckOHDay(lstFacilityOpHours.ElementAt(4).FacilityOperatingMiuntes);
            facilityHrRow.oDay6 = new TruckOHDay(lstFacilityOpHours.ElementAt(5).FacilityOperatingMiuntes);
            facilityHrRow.oDay7 = new TruckOHDay(lstFacilityOpHours.ElementAt(6).FacilityOperatingMiuntes);
        }

        private List<FacilityCapacity> ParseTrucksOpHours(DateTime startDate, List<TruckOpHoursInfo> lstTrucks)
        {
            TruckJsonGridData truckJsonGridData = new TruckJsonGridData();
            List<string> trucks = lstTrucks.OrderBy(s => s.FleetID).Select(t => Convert.ToString(t.TruckId)).Distinct().ToList();
            List<FacilityCapacity> lstCap = new List<FacilityCapacity>();

            truckJsonGridData.model = new ColHeader(startDate);

            foreach (var truckid in trucks)
            {
                FacilityCapacity row = new FacilityCapacity();

                row.TruckId = Convert.ToInt32(truckid);
                var c1 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c1.label).FirstOrDefault();
                if (c1 != null)
                {
                    row.Head = c1.FleetID;
                    row.oDay1 = new TruckOHDay(c1.TruckOperatingAnyTimeMinutes, c1.CAPTruckOperatingHourId, c1.CAPTruckOHReasonId, c1.IsPermanentTruckBool, c1.FacilityId, c1.StoreOpen, c1.Acronym);
                }

                var c2 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c2.label).FirstOrDefault();
                if (c2 != null)
                    row.oDay2 = new TruckOHDay(c2.TruckOperatingAnyTimeMinutes, c2.CAPTruckOperatingHourId, c2.CAPTruckOHReasonId, c2.IsPermanentTruckBool, c2.FacilityId, c2.StoreOpen, c2.Acronym);

                var c3 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c3.label).FirstOrDefault();
                if (c3 != null)
                    row.oDay3 = new TruckOHDay(c3.TruckOperatingAnyTimeMinutes, c3.CAPTruckOperatingHourId, c3.CAPTruckOHReasonId, c3.IsPermanentTruckBool, c3.FacilityId, c3.StoreOpen, c3.Acronym);

                var c4 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c4.label).FirstOrDefault();
                if (c4 != null)
                    row.oDay4 = new TruckOHDay(c4.TruckOperatingAnyTimeMinutes, c4.CAPTruckOperatingHourId, c4.CAPTruckOHReasonId, c4.IsPermanentTruckBool, c4.FacilityId, c4.StoreOpen, c4.Acronym);

                var c5 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c5.label).FirstOrDefault();
                if (c5 != null)
                    row.oDay5 = new TruckOHDay(c5.TruckOperatingAnyTimeMinutes, c5.CAPTruckOperatingHourId, c5.CAPTruckOHReasonId, c5.IsPermanentTruckBool, c5.FacilityId, c5.StoreOpen, c5.Acronym);

                var c6 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c6.label).FirstOrDefault();
                if (c6 != null)
                    row.oDay6 = new TruckOHDay(c6.TruckOperatingAnyTimeMinutes, c6.CAPTruckOperatingHourId, c6.CAPTruckOHReasonId, c6.IsPermanentTruckBool, c6.FacilityId, c6.StoreOpen, c6.Acronym);

                var c7 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c7.label).FirstOrDefault();
                if (c7 != null)
                    row.oDay7 = new TruckOHDay(c7.TruckOperatingAnyTimeMinutes, c7.CAPTruckOperatingHourId, c7.CAPTruckOHReasonId, c7.IsPermanentTruckBool, c7.FacilityId, c7.StoreOpen, c7.Acronym);

                lstCap.Add(row);
            }

            return lstCap;
        }

        public PartialViewResult SearchTruckOpHours(DateTime StartDate)
        {
            TruckFacilityCapacity oTruckFacilityCapacity = new TruckFacilityCapacity();

            try
            {
                //getting facilities to bind to dropdowns

                DateTime ToDate = StartDate.AddDays(6);

                FacilityCapacity head = new FacilityCapacity();
                head.Head = "Truck Details";

                head.oDay1 = new TruckOHDay(StartDate);
                head.oDay2 = new TruckOHDay(StartDate.AddDays(1));
                head.oDay3 = new TruckOHDay(StartDate.AddDays(2));
                head.oDay4 = new TruckOHDay(StartDate.AddDays(3));
                head.oDay5 = new TruckOHDay(StartDate.AddDays(4));
                head.oDay6 = new TruckOHDay(StartDate.AddDays(5));
                head.oDay7 = new TruckOHDay(StartDate.AddDays(6));

                oTruckFacilityCapacity.Head = head;

                FacilityCapacity FacilityHours = new FacilityCapacity();

                //var lstFacilityOpHours = this.prsService.GetFacilityOpHours(StartDate, ToDate, CurrentSiteInfo.LocationCode);
                //ParseFacilityOpHours(lstFacilityOpHours.ToList(), out FacilityHours);

                List<FacilityOpHours> lstFacilityOpHours = this.prsService.GetFacilityOpHours(StartDate, ToDate, CurrentSiteInfo.LocationCode);
                ParseFacilityOpHours(lstFacilityOpHours, out FacilityHours);

                var lstTrucks = this.prsService.GetTruckOpHoursDetails(StartDate, ToDate, CurrentSiteInfo.LocationCode);
                oTruckFacilityCapacity.TruckCapacility = ParseTrucksOpHours(StartDate, lstTrucks.ToList());

                oTruckFacilityCapacity.FacilityHours = FacilityHours;

                var capacity = _touchService.GetCapacity(StartDate, ToDate.AddDays(1), CurrentSiteInfo.LocationCode);

                //var capacity = _touchService.GetFacilityCapacity(StartDate, ToDate.AddDays(1), CurrentSiteInfo.LocationCode);

                oTruckFacilityCapacity.MarketStatistics = PraseFacilityCapacity(StartDate, capacity);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return PartialView("partial/_SearchTruckOpHours", oTruckFacilityCapacity);
        }

        public JsonResult AddTruckOpHoursDetails(TrkOpHours truckDetails)
        {
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                truckDetails.CurrentFacilityId = truckDetails.FacilityId = Convert.ToInt32(Session["WareHouseCode"].ToString());
                //write minutiesavailable logic here
                if (truckDetails.TruckOHReasonId == 5 || truckDetails.TruckOHReasonId == 6)
                {
                    truckDetails.MinutesAvailable = 0;
                }
                else if (truckDetails.TruckOHReasonId == 1 || truckDetails.TruckOHReasonId == 4)
                {
                    truckDetails.MinutesAvailable = 0;
                }
                else if (truckDetails.TruckOHReasonId == 2 || truckDetails.TruckOHReasonId == 3)
                {
                    truckDetails.MinutesAvailable = (int)(truckDetails.Enddate.Value.TimeOfDay - truckDetails.Startdate.TimeOfDay).TotalMinutes;
                }

                truckDetails.ModifiedBy = CurrentUser.ID;
                truckDetails.CreatedBy = CurrentUser.ID;


                string result = this.prsService.SaveTruckOpearationalHour(truckDetails);

                if (result == "fail")
                {
                    success = false;
                    errMsg = "Given dates are overlapping with existing records, please check.";
                }
                else if (result == "notavailable")
                {
                    success = false;
                    errMsg = "There’s a future transfer date pending, please contact the 1800PACKRAT Help Desk.";
                }
                else if (result == "success")
                {
                    success = true;
                }
                else if (result == "alreadyassigned")
                {
                    success = false;
                    errMsg = "This truck is already moved to another facility, please check.";
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };

            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTruckOpHoursDetails(int CAPTruckOperatingHourId)
        {
            try
            {
                var truckOpHoursDetails = prsService.GetTruckOpHrsDetails(CAPTruckOperatingHourId);

                return Json(truckOpHoursDetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                var retJson = new
                {
                    CAPTruckOperatingHourId = 0
                };
                return Json(retJson, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult TruckBackToService(int CAPTruckOperatingHourId)
        {
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                if (CAPTruckOperatingHourId > 0)
                {
                    this.prsService.TruckBackToService(CAPTruckOperatingHourId, CurrentUser.ID);
                    success = true;
                }
                else
                {
                    throw new Exception("There is no truck operational id to bring it back to service.");
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };

            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }

    public class TruckJsonGridData
    {
        public ColHeader model { get; set; }
        public List<Rows> rows = new List<Rows>();
    }

    public class ColHeader
    {
        public ColHeader(DateTime startDate)
        {
            c0.label = "TruckId";
            c0fleetid.label = "Truck Operations";
            c1.label = startDate.ToShortDateString();
            c1id.label = "CAPTruckOperatingHourId1";
            c2.label = startDate.AddDays(1).ToShortDateString();
            c2id.label = "CAPTruckOperatingHourId2";
            c3.label = startDate.AddDays(2).ToShortDateString();
            c3id.label = "CAPTruckOperatingHourId3";
            c4.label = startDate.AddDays(3).ToShortDateString();
            c4id.label = "CAPTruckOperatingHourId4";
            c5.label = startDate.AddDays(4).ToShortDateString();
            c5id.label = "CAPTruckOperatingHourId5";
            c6.label = startDate.AddDays(5).ToShortDateString();
            c6id.label = "CAPTruckOperatingHourId6";
            c7.label = startDate.AddDays(6).ToShortDateString();
            c7id.label = "CAPTruckOperatingHourId7";
        }

        public ColHeaderTitle c0 = new ColHeaderTitle();
        public ColHeaderTitle c0fleetid = new ColHeaderTitle();
        public ColHeaderTitle c1 = new ColHeaderTitle();
        public ColHeaderTitle c1id = new ColHeaderTitle();
        public ColHeaderTitle c2 = new ColHeaderTitle();
        public ColHeaderTitle c2id = new ColHeaderTitle();
        public ColHeaderTitle c3 = new ColHeaderTitle();
        public ColHeaderTitle c3id = new ColHeaderTitle();
        public ColHeaderTitle c4 = new ColHeaderTitle();
        public ColHeaderTitle c4id = new ColHeaderTitle();
        public ColHeaderTitle c5 = new ColHeaderTitle();
        public ColHeaderTitle c5id = new ColHeaderTitle();
        public ColHeaderTitle c6 = new ColHeaderTitle();
        public ColHeaderTitle c6id = new ColHeaderTitle();
        public ColHeaderTitle c7 = new ColHeaderTitle();
        public ColHeaderTitle c7id = new ColHeaderTitle();
    }

    public class ColHeaderTitle
    {
        public string label { get; set; }
    }

    public class Rows
    {
        public string id { get; set; }
        public string c0 { get; set; }
        public string c0fleetid { get; set; }
        public string c1 { get; set; }
        public string c1id { get; set; }
        public string c2 { get; set; }
        public string c2id { get; set; }
        public string c3 { get; set; }
        public string c3id { get; set; }
        public string c4 { get; set; }
        public string c4id { get; set; }
        public string c5 { get; set; }
        public string c5id { get; set; }
        public string c6 { get; set; }
        public string c6id { get; set; }
        public string c7 { get; set; }
        public string c7id { get; set; }
    }
}
