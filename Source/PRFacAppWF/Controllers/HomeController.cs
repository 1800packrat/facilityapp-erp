﻿using System.Web.Mvc;
using System.Web.Security;

namespace PRFacAppWF.Controllers
{
    public class HomeController : Controller
    {
        public void Logout()
        {
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();
        
            Response.Redirect("/Login/Index", true);
        }

        public void Index()
        {
            if (Session != null && HttpContext.Session.Count > 0)
                Response.Redirect("~/DashBoard.aspx", true);
            else
                Response.Redirect("/Login/Index", true);
        }
    }
}