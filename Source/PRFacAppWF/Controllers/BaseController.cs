﻿using System;
using System.Web.Mvc;
using PRFacAppWF.CodeHelpers;
using PR.Entities;
using PR.LocalLogisticsSolution.Model;

namespace PRFacAppWF.Controllers
{
    [HandleJsonExceptionAttribute]
    [Authorize]
    public class BaseController : Controller
    {
        public SiteInfo CurrentSiteInfo
        {
            get
            {
                return Helper.CurrentSiteInfo;
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session != null)
            {                
                if (HttpContext.Session.Count == 0)
                {
                    filterContext.Result = new RedirectResult("/login/index?id=1");
                           }
                else
                {
                   
                    //string pageName = Path.GetFileNameWithoutExtension(HttpContext.Current.Request.Url.AbsolutePath);
                    string pageName = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString(); ;

                    Entities.Enums.ModuleName moduleName = (Entities.Enums.ModuleName)Enum.Parse(typeof(Entities.Enums.ModuleName), pageName, true);

                    bool isAccessable = PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(moduleName, PRFacAppWF.Entities.Enums.AccessLevel.Any, true);

                    if (isAccessable == false)
                    {
                        filterContext.Result = new RedirectResult("~/Unauthorized.aspx");
                    }
                }
            }
        }

        /// <summary>
        /// This property returns logged in user details. This session must be assigned after login
        /// </summary>
        public LogisticsUser CurrentUser
        {
            get
            {
                return Helper.CurrentUser;
            }
        }

        /// <summary>
        /// Render a PartialView into a string that contain the Html to display to the browser.
        /// </summary>
        /// <param name="partialViewName">The name of the partial view to render</param>
        /// <param name="model">The model to bind to the partial view</param>
        /// <returns>The html rendered partial view</returns>
        protected virtual string RenderPartialView(string partialViewName, object model)
        {
            if (ControllerContext == null)
                return string.Empty;

            if (string.IsNullOrEmpty(partialViewName))
                throw new ArgumentNullException("partialViewName");

            ModelState.Clear();

            ViewData.Model = model;

            using (var sw = new System.IO.StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, partialViewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}