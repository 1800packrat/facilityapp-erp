﻿namespace PRFacAppWF.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using PR.LocalLogisticsSolution.Interfaces;
    using PRFacAppWF.models;
    using System.Web.Mvc;
    using PR.Entities;
    using PRFacAppWF.Repository.Services;
    using PRFacAppWF.Repository.Services.Implementations;
    using PRFacAppWF.CodeHelpers;
    using PRFacAppWF.Entities;
    using Entities.DriverSchedule;
    using Entities.Enums;
    using Entities.UserManagement;
    using System.Globalization;

    public class DriverMaintenanceController : BaseController
    {
        private readonly IPRSService prsService;
        private IUserService _userService = null;
        private IUserSecurity _userSecurity = null;

        public DriverMaintenanceController(IUserService userSerive)
        {
            if (this.prsService == null)
                this.prsService = new PRSService();

            if (_userSecurity == null)
                _userSecurity = new UserSecurityService();

            _userService = userSerive;
        }

        public ActionResult Index()
        {
            ViewBag.PasswordMinLength = _userSecurity.GetPasswordConfig().PasswordMinLength;

            ViewBag.Facilities = this.prsService.GetFacilitiesList().Where(x => x.SLLocCode != CurrentSiteInfo.LocationCode).OrderBy(x => x.CompDBAName);
            var EditDriverForm = new AddEditDriverViewModel
            {
                WeeklySchedule = new WeeklySchedule
                {
                    WeeklyScheduleFromDate = DateTime.Now,
                    WeeklyScheduleToDate = DateTime.Now.AddYears(1)
                }
            };

            ViewBag.EditDriverForm = EditDriverForm;
            return View();
        }

        public PartialViewResult DriverScheduleList(DateTime StartDate)
        {
            DriversListViewModel driverModel = new DriversListViewModel();

            try
            {
                DateTime ToDate = StartDate.AddDays(6);

                driverModel.DriverScheduleList = this._userSecurity.GetDriversScheduleHrsFromSP(StartDate, ToDate, CurrentSiteInfo.LocationCode);

                ///This method process header and push it to GridHead property
                driverModel.GridHead = new DriverListHead(StartDate);


                ///This method to get truck statistics
                var lstTrucks = this.prsService.GetTruckOpHoursDetails(StartDate, ToDate, CurrentSiteInfo.LocationCode);
                driverModel.TrucksHours = ParseTrucksOpHours(StartDate, lstTrucks.ToList());
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return PartialView("Partial/_driverScheduleList", driverModel);
        }

        private List<FacilityCapacity> ParseTrucksOpHours(DateTime startDate, List<TruckOpHoursInfo> lstTrucks)
        {
            TruckJsonGridData truckJsonGridData = new TruckJsonGridData();
            List<string> trucks = lstTrucks.OrderBy(s => s.FleetID).Select(t => Convert.ToString(t.TruckId)).Distinct().ToList();
            List<FacilityCapacity> lstCap = new List<FacilityCapacity>();

            truckJsonGridData.model = new ColHeader(startDate);

            foreach (var truckid in trucks)
            {
                FacilityCapacity row = new FacilityCapacity();

                row.TruckId = Convert.ToInt32(truckid);
                var c1 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c1.label).FirstOrDefault();
                if (c1 != null)
                {
                    row.Head = c1.FleetID;
                    row.oDay1 = new TruckOHDay(c1.TruckOperatingAnyTimeMinutes, c1.CAPTruckOperatingHourId, c1.CAPTruckOHReasonId, c1.IsPermanentTruckBool, c1.FacilityId, c1.StoreOpen, c1.Acronym);
                }

                var c2 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c2.label).FirstOrDefault();
                if (c2 != null)
                    row.oDay2 = new TruckOHDay(c2.TruckOperatingAnyTimeMinutes, c2.CAPTruckOperatingHourId, c2.CAPTruckOHReasonId, c2.IsPermanentTruckBool, c2.FacilityId, c2.StoreOpen, c2.Acronym);

                var c3 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c3.label).FirstOrDefault();
                if (c3 != null)
                    row.oDay3 = new TruckOHDay(c3.TruckOperatingAnyTimeMinutes, c3.CAPTruckOperatingHourId, c3.CAPTruckOHReasonId, c3.IsPermanentTruckBool, c3.FacilityId, c3.StoreOpen, c3.Acronym);

                var c4 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c4.label).FirstOrDefault();
                if (c4 != null)
                    row.oDay4 = new TruckOHDay(c4.TruckOperatingAnyTimeMinutes, c4.CAPTruckOperatingHourId, c4.CAPTruckOHReasonId, c4.IsPermanentTruckBool, c4.FacilityId, c4.StoreOpen, c4.Acronym);

                var c5 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c5.label).FirstOrDefault();
                if (c5 != null)
                    row.oDay5 = new TruckOHDay(c5.TruckOperatingAnyTimeMinutes, c5.CAPTruckOperatingHourId, c5.CAPTruckOHReasonId, c5.IsPermanentTruckBool, c5.FacilityId, c5.StoreOpen, c5.Acronym);

                var c6 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c6.label).FirstOrDefault();
                if (c6 != null)
                    row.oDay6 = new TruckOHDay(c6.TruckOperatingAnyTimeMinutes, c6.CAPTruckOperatingHourId, c6.CAPTruckOHReasonId, c6.IsPermanentTruckBool, c6.FacilityId, c6.StoreOpen, c6.Acronym);

                var c7 = lstTrucks.Where(t => Convert.ToString(t.TruckId) == truckid && t.CapacityDate.ToShortDateString() == truckJsonGridData.model.c7.label).FirstOrDefault();
                if (c7 != null)
                    row.oDay7 = new TruckOHDay(c7.TruckOperatingAnyTimeMinutes, c7.CAPTruckOperatingHourId, c7.CAPTruckOHReasonId, c7.IsPermanentTruckBool, c7.FacilityId, c7.StoreOpen, c7.Acronym);

                lstCap.Add(row);
            }

            return lstCap;
        }

        public PartialViewResult UserForm(int Id)
        {
            AddEditDriverViewModel driverModel = new AddEditDriverViewModel();

            try
            {
                driverModel.FAUserMaster = this._userSecurity.GetUserInfoById(Id);
                if (driverModel.FAUserMaster.PK_UserId > 0)
                    driverModel.FAUserMaster.Password = PR.UtilityLibrary.CommonUtility.Decrypt(driverModel.FAUserMaster.Password, Helper.EnDeKey);

                driverModel.FAUserMaster.AccessableFacilityList = this._userSecurity.GetDTOFacilityList();

                driverModel.FAUserMaster.StatusList = this._userSecurity.GetUserStatus();
                driverModel.FAUserMaster.LicenseClassList = this._userSecurity.GetLicenseClass();

                driverModel.WeeklySchedule = new WeeklySchedule
                {
                    WeeklyScheduleFromDate = DateTime.Now,
                    WeeklyScheduleToDate = DateTime.Now.AddMonths(6),
                    UserID = driverModel.FAUserMaster.PK_UserId
                };
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            if (Id > 0)
                return PartialView("Partial/_editDriverInfo", driverModel);
            else
                return PartialView("Partial/_addDriver", driverModel);
        }

        public JsonResult AssingDriverToFacility(int driverId, string driverName)
        {
            var response = new AjaxResponse<object>();
            try
            {
                bool faciliyAssigned = _userSecurity.AssingDriverToFacility(driverId, Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum), CurrentUser.ID);

                if (faciliyAssigned == true)
                {
                    response.success = true;
                    response.data = new
                    {
                        driverId = driverId,
                        driverName = driverName
                    };
                }
                else
                {
                    response.success = false;
                    response.message = "Error occurred while assigning facility to him.";
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDuplicateUsersByLastname(string LastName, DateTime StartDate)
        {
            string result = string.Empty;
            var response = new AjaxResponse<string>();
            try
            {
                var lstMatchingUsers = this._userSecurity.GetDuplicateUsersByLastname(LastName, StartDate);

                if (lstMatchingUsers.Count > 0)
                    response.data = RenderPartialView("Partial/_formAddDriverDuplicate", lstMatchingUsers);
                else
                    response.data = "noduplicatedriversfound";

                response.success = true;
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error occurred while checking user information!";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddDriverFacility(int Index)
        {
            AddEditDriverViewModel driverModel = new AddEditDriverViewModel();
            var response = new AjaxResponse<string>();
            try
            {
                IEnumerable<FADTOFacility> AccessableFacilityList = this._userSecurity.GetDTOFacilityList();
                var DFModel = new Entities.DriverSchedule.DriverFacility
                {
                    DriverStartDate = DateTime.Now,
                    DriverEndDate = null,
                    FK_ApplicationTypeID = (int)Entities.Enums.Application.DriverApp,
                    DAAccessLevel = Entities.Enums.AccessLevel.SecondaryFacility,
                    DFStatus = DriverFacilityStatus.New,
                    Index = Index,
                    AccessableFacilityList = AccessableFacilityList
                };

                response.data = RenderPartialView("Partial/_formDriverFacility", DFModel);
                response.success = true;
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error occurred while getting user facilities.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SuggesstedUserName(string FirstName, string LastName)
        {
            var response = new AjaxResponse<string>();
            try
            {
                var jsonSerialiser = new System.Web.Script.Serialization.JavaScriptSerializer();
                response.data = jsonSerialiser.Serialize(this._userSecurity.suggestedUserName(FirstName, LastName));
                response.success = true;
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error occurred while getting user facilities.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckUserNameAvailability(string UserName)
        {
            var response = new AjaxResponse<string>();
            try
            {
                response.success = this._userSecurity.checkUserNameAvailable(UserName);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error occurred while getting user facilities.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveDriverDetails(SaveDriverRequest driverInfo)
        {
            var response = new AjaxResponse<string>();
            //implement driver details saving functionality
            try
            {
                if (!_userSecurity.CheckUserName(driverInfo.UserName, driverInfo.PK_UserId))
                {

                    var user = _userSecurity.GetUserDetailsByUserId(driverInfo.PK_UserId);
                    if (user != null && driverInfo.StatusID != UserStatus.Active && _userService.CheckLoadExistForGivenDateRange(user.UniqueID.Value, DateTime.Now, DateTime.MaxValue))
                    {
                        List<DateTime> nonSchedulableDates = _userService.GetDriverLoadDates(user.UniqueID.Value, DateTime.Now);

                        if (nonSchedulableDates.Count > 0)
                        {
                            string strNonScheduleDates = String.Join(", ", nonSchedulableDates.Select(s => s.ToShortDateString()));
                            response.message = "This driver is currently routed on " + strNonScheduleDates + " in Local Dispatch.  Please remove the touches in Local Dispatch before modifying the schedule.";
                        }

                        response.success = false;
                    }
                    else if (user != null && driverInfo.StatusID != UserStatus.Active && !user.FAGroupMaster.GroupName.Equals(Helper.DriverDefaultRoleGroupName, StringComparison.Ordinal))
                    {
                        response.data = "RoleMismatch";
                        response.success = false;
                        //response.message = "You are not authorized to disable this user as they are not assigned to the Driver role. Please contact the helpdesk if you want to reassign the user role or have any questions please contact helpdesk@1800packrat.com";
                        response.message = "By disabling this driver, the user will lose their Facility App access. Please contact Helpdesk if you want to reassign the user role or have any questions, please contact helpdesk@1800packrat.com";
                    }
                    else
                    {

                        //Before save driver info make sure pass encrypted password and group id. Here group id should be hard coded group for new drivers
                        driverInfo.EncryptPassword = PR.UtilityLibrary.CommonUtility.Encrypt(driverInfo.Password.Trim(), Helper.EnDeKey);

                        if (driverInfo.PK_UserId == 0)
                            driverInfo.GroupMasterID = this._userSecurity.RoleGroupIDByName(Helper.DriverDefaultRoleGroupName);

                        DriverFacility df = new DriverFacility()
                        {
                            UserFacilityID = this._userSecurity.GetStoreRowID(CurrentSiteInfo.LocationCode)
                        };

                        driverInfo.DriverFacility = new List<DriverFacility>();
                        driverInfo.DriverFacility.Add(df);

                        //Save driver information 
                        this._userSecurity.SaveDriverDetails(driverInfo, CurrentUser.ID);

                        response.success = true;
                        response.data = "success";

                    }
                }
                else
                {
                    response.success = false;
                    response.message = "User name already exists, please choose another.";
                }

            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error occurred while saving driver information. Please reload the page and try again!.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveWeeklyDriverSchedule(WeeklySchedule WeeklySchedule)
        {
            var response = new AjaxResponse<string>();

            try
            {
                List<DriverScheduleHour> lstDriverHourChange = new List<DriverScheduleHour>();
                DriverScheduleHour driverScheduleHr;
                DateTime date = WeeklySchedule.WeeklyScheduleFromDate;
                List<DateTime> lstReqScheduleDates = new List<DateTime>();
                List<DateTime> lstInactiveScheduleDates = new List<DateTime>();

                response.success = true;

                while (date <= WeeklySchedule.WeeklyScheduleToDate)
                {
                    if (DateTime.Now.Date <= date)
                    {
                        var todayTime = WeeklySchedule.WeeklyDayWiseSchedule.Where(w => (date.DayOfWeek == DayOfWeek.Sunday && w.Sunday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Monday && w.Monday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Tuesday && w.Tuesday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Wednesday && w.Wednesday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Thursday && w.Thursday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Friday && w.Friday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Saturday && w.Saturday == true)
                                                        ).FirstOrDefault();

                        if (todayTime != null)
                        {
                            if (_userSecurity.CanScheduleDriverHoursForThisDay(date, WeeklySchedule.UserID, Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum)))
                            {
                                driverScheduleHr = new DriverScheduleHour
                                {
                                    ScheduleStartTimeTS = todayTime.StartTime,
                                    ScheduleEndTimeTS = todayTime.EndTime,

                                    DayWork = todayTime.DayWork.ToString(),
                                    ScheduleDate = date,
                                    DriverID = WeeklySchedule.UserID,
                                };

                                lstDriverHourChange.Add(driverScheduleHr);

                                if (todayTime.DayWork != DriverDayOfWork.S)
                                {
                                    lstInactiveScheduleDates.Add(date);
                                }
                            }
                        }
                    }

                    date = date.AddDays(1);
                }

                var user = _userSecurity.GetUserDetailsByUserId(WeeklySchedule.UserID);

                //If there are any entries to off or oncall driver schedules, please exclude them if they have route for the day
                List<DateTime> nonInactiveSchedulableDates = new List<DateTime>();
                if (lstInactiveScheduleDates.Count > 0)
                    CheckAndGetDriverSchedulableDates(user.UniqueID.Value, DriverDayOfWork.C, lstInactiveScheduleDates, out nonInactiveSchedulableDates);

                ///Remove all dates which are non-removable, because on that dates there might be routed or routed and locked.
                lstDriverHourChange.RemoveAll(d => nonInactiveSchedulableDates.Select(sd => sd.Date).Contains(d.ScheduleDate.Date));

                if (lstDriverHourChange.Count > 0)
                    this._userSecurity.SaveDriverScheduleHours(lstDriverHourChange, CurrentUser.ID);

                if (nonInactiveSchedulableDates.Count > 0)
                {
                    response.success = false;
                    string strNonScheduleDates = String.Join(", ", nonInactiveSchedulableDates.Select(s => s.ToShortDateString()));
                    response.message = "This driver is currently routed on " + strNonScheduleDates + " in Local Dispatch.  Please remove the touches in Local Dispatch before modifying the schedule status to Off or On-Call.";
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error occurred while saving driver schedule hours.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #region Driver Schedule Grid Operations

        public ActionResult GetDriverScheduleHours(string sidx, string sord, int page, int rows, int driverId, DateTime startDate, bool _search, string weekday)
        {

            ResultSet<DriverScheduleHour> jsonData = new ResultSet<DriverScheduleHour>();
            try
            {
                int wkday = 0;
                int.TryParse(weekday, out wkday);
                var driverScheduleHours = this._userSecurity.GetDriverScheduleHourDetails(Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum), startDate, rows, driverId, wkday);
                int totalCount = driverScheduleHours.Count();
                var driverSchduelFHours = FilterGrid(driverScheduleHours, sidx, sord, page, rows);

                var pageSize = rows;
                var totalRecords = totalCount;
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

                jsonData.total = totalPages;
                jsonData.page = page;
                jsonData.records = totalRecords;
                jsonData.rows = driverSchduelFHours.ToList();

            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<DriverScheduleHour> FilterGrid(IEnumerable<DriverScheduleHour> data, string sidx, string sord, int page, int rows)
        {
            int skiprows = (page - 1) * rows;
            //Since in the UI we are displaying CapacityDate column as CapacityDateDisp.
            sidx = sidx.ToLower().Contains("scheduledate") ? "ScheduleDate" : sidx;

            if (sord == "desc")
                data = data.OrderByDescending(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);
            else
                data = data.OrderBy(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);

            return data;
        }

        private List<DateTime> CheckAndGetDriverSchedulableDates(int driverId, DriverDayOfWork dayWork, List<DateTime> scheduleDates, out List<DateTime> nonSchdulableDates)
        {
            List<DriverScheduleHourChangeAndStatus> lstDriverScheduleHourChangeAndStatus = _userService.CheckUpdateDriverScheduleHours(driverId, scheduleDates);

            List<DateTime> lstSchdulableDates = new List<DateTime>();
            nonSchdulableDates = new List<DateTime>();

            ///IF DayWork is Schedule change, then user can update timings for non locked route drivers
            ///IF we update driver timings for locked route datys, then it may create problem
            if (dayWork == DriverDayOfWork.S)
            {
                lstSchdulableDates.AddRange(lstDriverScheduleHourChangeAndStatus
                                    .Where(ds => ds.UpdateDriverScheduleHours == PR.UtilityLibrary.PREnums.DriverRouteStausForADay.NoRouteExists || ds.UpdateDriverScheduleHours == PR.UtilityLibrary.PREnums.DriverRouteStausForADay.RouteExistsAndNonLocked)
                                    .Select(s => s.ScheduleDate));

                nonSchdulableDates.AddRange(lstDriverScheduleHourChangeAndStatus
                                    .Where(ds => ds.UpdateDriverScheduleHours == PR.UtilityLibrary.PREnums.DriverRouteStausForADay.RouteExistsAndLocked)
                                    .Select(s => s.ScheduleDate));
            }
            else
            {
                //IF DayWork is Off or On-Call, this can be applied only for non routed days only
                lstSchdulableDates.AddRange(lstDriverScheduleHourChangeAndStatus
                                    .Where(ds => ds.UpdateDriverScheduleHours == PR.UtilityLibrary.PREnums.DriverRouteStausForADay.NoRouteExists)
                                    .Select(s => s.ScheduleDate));

                nonSchdulableDates.AddRange(lstDriverScheduleHourChangeAndStatus
                                    .Where(ds => ds.UpdateDriverScheduleHours == PR.UtilityLibrary.PREnums.DriverRouteStausForADay.RouteExistsAndLocked || ds.UpdateDriverScheduleHours == PR.UtilityLibrary.PREnums.DriverRouteStausForADay.RouteExistsAndNonLocked)
                                    .Select(s => s.ScheduleDate));
            }

            return lstSchdulableDates;
        }

        [HttpPost]
        public JsonResult CheckAndGetDriverLockedLoadsByDate(string[] dates, int driverID)
        {
            var response = new AjaxResponse<List<string>>();

            try
            {

                List<DateTime> listScheduleDates = dates.Select(d => DateTime.Parse(d)).Select(dt => dt.Date).ToList();

                response.success = true;
                response.data = GetNonSchedulableDates(listScheduleDates, driverID);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private List<string> GetNonSchedulableDates(List<DateTime> listScheduleDates, int driverId)
        {
            var user = _userSecurity.GetUserDetailsByUserId(driverId);
            List<string> lstNonSchedulableDates = new List<string>();

            if (user != null)
            {
                List<DateTime> nonSchedulableDates = new List<DateTime>();
                CheckAndGetDriverSchedulableDates(user.UniqueID.Value, DriverDayOfWork.S, listScheduleDates, out nonSchedulableDates);

                lstNonSchedulableDates = nonSchedulableDates.Select(s => s.ToString("MM/dd/yyyy")).ToList();
            }
            else
            {
                throw new Exception("No driver found in the system with this driver id.");
            }

            return lstNonSchedulableDates;
        }

        [HttpPost]
        public JsonResult CheckAndGetDriverLockedLoadsByDateForWeeklySchedule(WeeklySchedule WeeklySchedule)
        {
            var response = new AjaxResponse<List<string>>();

            try
            {
                DateTime date = WeeklySchedule.WeeklyScheduleFromDate;
                List<DateTime> lstReqScheduleDates = new List<DateTime>();

                while (date <= WeeklySchedule.WeeklyScheduleToDate)
                {
                    if (DateTime.Now.Date <= date)
                    {
                        var todayTime = WeeklySchedule.WeeklyDayWiseSchedule.Where(w => (date.DayOfWeek == DayOfWeek.Sunday && w.Sunday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Monday && w.Monday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Tuesday && w.Tuesday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Wednesday && w.Wednesday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Thursday && w.Thursday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Friday && w.Friday == true) ||
                                                            (date.DayOfWeek == DayOfWeek.Saturday && w.Saturday == true)
                                                        ).FirstOrDefault();

                        if (todayTime != null && todayTime.DayWork == DriverDayOfWork.S)
                        {
                            if (_userSecurity.CanScheduleDriverHoursForThisDay(date, WeeklySchedule.UserID, Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum)))
                            {
                                lstReqScheduleDates.Add(date);
                            }
                        }
                    }

                    date = date.AddDays(1);
                }

                response.success = true;
                response.data = GetNonSchedulableDates(lstReqScheduleDates, WeeklySchedule.UserID);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error occurred while saving driver schedule hours.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveDriverScheduleHours(FormCollection collection)
        {
            var response = new AjaxResponse<string>();

            try
            {
                List<DriverScheduleHour> lstDriverHourChange = new List<DriverScheduleHour>();
                DriverScheduleHour driverScheduleHr;

                string dates = (string.IsNullOrEmpty(collection["dates[]"]) ? collection["dates"] : collection["dates[]"]);
                bool isLocalDispatch = (string.IsNullOrEmpty(collection["isLocalDispatch"]) ? false : Convert.ToBoolean(collection["isLocalDispatch"]));
                var lstDriverWorkingHours = dates.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                List<DateTime> listScheduleDates = dates.Split(',').Select(d => DateTime.Parse(d)).Select(dt => dt.Date).ToList();

                int userID = 0;
                int driverID = 0;
                var user = new PRFacAppWF.Repository.models.FAUserMaster();

                ///For the requests from local dispatch, they will have DriverID (i.e., UniqueID). so get the actual driver information and do the action on pk_id
                if (isLocalDispatch)
                    user = _userSecurity.GetUserDetailsByDriverId(Convert.ToInt32(collection["driverID"]));
                else
                    user = _userSecurity.GetUserDetailsByUserId(Convert.ToInt32(collection["driverID"]));

                userID = user.PK_UserId;
                driverID = user.UniqueID.Value;

                var DayWork = collection["DayWork"];
                DriverDayOfWork daywork;
                Enum.TryParse(DayWork, out daywork);

                List<DateTime> nonSchedulableDates = new List<DateTime>();

                List<DateTime> lstDriverSchedulableDates = new List<DateTime>();

                //For Schedule - we need not to check any condition. but if the daywork is other than schedule type then we have to make sure what are the dates he can unschedule it.
                if (daywork == DriverDayOfWork.S)
                    lstDriverSchedulableDates = listScheduleDates;
                else
                    lstDriverSchedulableDates = CheckAndGetDriverSchedulableDates(driverID, daywork, listScheduleDates, out nonSchedulableDates);

                response.success = true;
                if (lstDriverSchedulableDates.Count > 0)
                {
                    foreach (var scheduleDate in lstDriverSchedulableDates)
                    {
                        if (DateTime.Now.Date <= scheduleDate)
                        {
                            driverScheduleHr = new DriverScheduleHour();

                            driverScheduleHr.ScheduleStartTimeTS = DateTime.ParseExact(collection["ScheduleStartTime"], "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                            driverScheduleHr.ScheduleEndTimeTS = DateTime.ParseExact(collection["ScheduleEndTime"], "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                            driverScheduleHr.DayWork = DayWork;
                            driverScheduleHr.ScheduleDate = scheduleDate;
                            driverScheduleHr.DriverID = userID;

                            lstDriverHourChange.Add(driverScheduleHr);
                        }
                    }

                    if (lstDriverHourChange.Count > 0)
                        this._userSecurity.SaveDriverScheduleHours(lstDriverHourChange, CurrentUser.ID);
                }

                if (nonSchedulableDates.Count > 0)
                {
                    response.success = false;
                    string strNonScheduleDates = String.Join(", ", nonSchedulableDates.Select(s => s.ToShortDateString()));
                    response.message = "This driver is currently routed on " + strNonScheduleDates + " in Local Dispatch. Please remove the touches in Local Dispatch before modifying the schedule status to Off or On-Call.";
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveDriverScheduleHoursFromLocalDispatch(FormCollection collection)
        {
            var response = new AjaxResponse<string>();

            try
            {
                List<DriverScheduleHour> lstDriverHourChange = new List<DriverScheduleHour>();
                DriverScheduleHour driverScheduleHr;

                string dates = (string.IsNullOrEmpty(collection["dates[]"]) ? collection["dates"] : collection["dates[]"]);
                bool isLocalDispatch = (string.IsNullOrEmpty(collection["isLocalDispatch"]) ? false : Convert.ToBoolean(collection["isLocalDispatch"]));
                var lstDriverWorkingHours = dates.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                List<DateTime> listScheduleDates = dates.Split(',').Select(d => DateTime.Parse(d)).Select(dt => dt.Date).ToList();

                int userID = 0;
                int driverID = 0;
                var user = new PRFacAppWF.Repository.models.FAUserMaster();

                ///For the requests from local dispatch, they will have DriverID (i.e., UniqueID). so get the actual driver information and do the action on pk_id
                if (isLocalDispatch)
                    user = _userSecurity.GetUserDetailsByDriverId(Convert.ToInt32(collection["driverID"]));

                userID = user.PK_UserId;
                driverID = user.UniqueID.Value;

                var DayWork = collection["DayWork"];
                DriverDayOfWork daywork;
                Enum.TryParse(DayWork, out daywork);

                response.success = true;

                foreach (var scheduleDate in listScheduleDates)
                {

                    driverScheduleHr = new DriverScheduleHour();

                    driverScheduleHr.ScheduleStartTimeTS = DateTime.ParseExact(collection["ScheduleStartTime"], "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                    driverScheduleHr.ScheduleEndTimeTS = DateTime.ParseExact(collection["ScheduleEndTime"], "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                    driverScheduleHr.DayWork = DayWork;
                    driverScheduleHr.ScheduleDate = scheduleDate;
                    driverScheduleHr.DriverID = userID;

                    lstDriverHourChange.Add(driverScheduleHr);

                }

                if (lstDriverHourChange.Count > 0)
                    this._userSecurity.SaveDriverScheduleHours(lstDriverHourChange, CurrentUser.ID);

            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public JsonResult SaveDriverRoverTransfer(FADTOUserAndFacilityAccess driverTransferDetails)
        {
            var response = new AjaxResponse<string>();

            try
            {
                string msg = string.Empty;

                //In the UI we are showing like in the End date this driver will be back to origin facility
                if (driverTransferDetails.EndDate.HasValue)
                {
                    driverTransferDetails.EndDate = driverTransferDetails.EndDate.Value.AddDays(-1);
                }

                bool canTransfer = _userSecurity.CanThisUserTransferThisDriverForRelocation(driverTransferDetails.FK_UserID, CurrentSiteInfo, driverTransferDetails.StartDate, driverTransferDetails.EndDate);

                if (canTransfer == false)
                    msg = "You cannot transfer this driver to any other facility, because he is belongs to other facility. Please contact his primary facility OM to do this action.";

                if (canTransfer == true && driverTransferDetails.FK_AccessLevelID == (int)AccessLevel.Rover)
                {
                    //check whether driver change be Rovered or not
                    if (_userSecurity.CanThisUserTransferThisDriverForRover(driverTransferDetails, CurrentSiteInfo) == false)
                    {
                        msg = "Driver with rover status is already exists for other facility, please end that record before proceeding.";
                        canTransfer = false;
                    }
                }

                ///Actually we need to check, Check this driver has any routes for this date range or not
                if (canTransfer == true)
                {
                    var user = _userSecurity.GetUserDetailsByUserId(driverTransferDetails.FK_UserID);
                    if (_userService.CheckLoadExistForGivenDateRange(user.UniqueID.Value, driverTransferDetails.StartDate, (driverTransferDetails.EndDate.HasValue ? driverTransferDetails.EndDate.Value : DateTime.MaxValue)))
                    {
                        msg = "You cannot transfer this driver other facility, because routes has been created.";
                        canTransfer = false;
                    }
                }

                //If this driver is not belongs to current site then we should not allow him to transfer 
                if (canTransfer)
                {
                    //Save driver transfer Info information 
                    driverTransferDetails.FromFacilityID = this._userSecurity.GetStoreRowID(CurrentSiteInfo.LocationCode);

                    _userSecurity.SaveDriverRoverAndPermanentTransfer(driverTransferDetails, CurrentUser.ID);

                    response.success = true;
                    response.data = "success";
                }
                else
                {
                    response.success = false;
                    response.message = msg;
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDriverRoverFacility(int DriverId)
        {
            AddEditDriverViewModel driverModel = new AddEditDriverViewModel();
            var response = new AjaxResponse<string>();
            try
            {
                IEnumerable<DriverFacility> RoverFacilityList = this._userSecurity.GetDriverRoverFacilityList(CurrentSiteInfo.GlobalSiteNum, DriverId);

                response.data = RenderPartialView("Partial/_formDriverRoverFacilityList", RoverFacilityList);
                response.success = true;
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error occurred while getting user facilities.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateDriverRoverFacilityInfo(DriverFacility roverFacility)
        {
            AddEditDriverViewModel driverModel = new AddEditDriverViewModel();
            var response = new AjaxResponse<string>();
            try
            {
                SaveRoverStatus oSaveRoverStatus = _userSecurity.CanSaveRoverRecord(roverFacility, Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum));

                ///Actually we need to check, Check this driver has any routes for this date range or not
                if (oSaveRoverStatus == SaveRoverStatus.CanSave)
                {
                    var user = _userSecurity.GetUserDetailsByUserId(roverFacility.DriverId);
                    if (_userService.CheckLoadExistForGivenDateRange(user.UniqueID.Value, roverFacility.DriverStartDate.Value, roverFacility.DriverEndDate.Value))
                    {
                        oSaveRoverStatus = SaveRoverStatus.LoadExistsCannotUpdate;
                    }
                }

                if (oSaveRoverStatus == SaveRoverStatus.CanSave)
                {
                    this._userSecurity.UpdateDriverRoverFacilityInfo(roverFacility, CurrentUser.ID);

                    IEnumerable<DriverFacility> RoverFacilityList = this._userSecurity.GetDriverRoverFacilityList(CurrentSiteInfo.GlobalSiteNum, roverFacility.DriverId);

                    response.data = RenderPartialView("Partial/_formDriverRoverFacilityList", RoverFacilityList);
                    response.success = true;
                }
                else
                {
                    response.success = false;

                    if (oSaveRoverStatus == SaveRoverStatus.NotPrimaryFacility)
                    {
                        response.message = "You are not primary owner for this driver, you cannot do this action. Please contact his primary facility OM to do this action.";
                    }
                    else if (oSaveRoverStatus == SaveRoverStatus.LoadExistsCannotUpdate)
                    {
                        response.message = "You cannot update/delete this driver transfers, because routes has been created for the given date range.";
                    }
                    else
                    {
                        response.message = "Given dates are overlapping with the existing Rover records for this driver, Please correct them and try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error occurred while submitting your action.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}