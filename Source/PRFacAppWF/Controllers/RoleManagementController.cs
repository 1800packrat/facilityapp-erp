﻿using PRFacAppWF.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PRFacAppWF.Repository.Services;
using PRFacAppWF.Repository.Services.Implementations;
using PRFacAppWF.CodeHelpers;
using PRFacAppWF.Entities;
using PRFacAppWF.Entities.UserManagement;

namespace PRFacAppWF.Controllers
{
    public class RoleManagementController : BaseController
    {
        private IUserSecurity _userSecurity = null;

        public RoleManagementController()
        {
            if (this._userSecurity == null)
                this._userSecurity = new UserSecurityService();
        }
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult RolesList(string sidx, string sord, int page, int rows, string roleName)
        {
            JQGridResultSet jsonData = new JQGridResultSet();
            try
            {
                List<FADTOGroupMaster> lclistRoles = new List<FADTOGroupMaster>();

                lclistRoles = _userSecurity.ListRoles();

                if (!string.IsNullOrEmpty(roleName))
                {
                    lclistRoles = lclistRoles.Where(x => x.GroupName.ToLower().Contains(roleName.ToLower())).ToList();
                }

                int pageIndex = Convert.ToInt32(page) - 1;
                int pageSize = rows;
                int totalRecords = lclistRoles.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

                var lstUsr = lclistRoles.OrderBy(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(pageIndex * pageSize).Take(pageSize);

                if (sord == "desc")
                {
                    lstUsr = lclistRoles.OrderByDescending(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(pageIndex * pageSize).Take(pageSize);
                }

                jsonData.total = totalPages;
                jsonData.page = page;
                jsonData.records = totalRecords;

                foreach (var lst in lstUsr)
                {
                    JQGridResultSet.Row row = new JQGridResultSet.Row();
                    row.id = lst.PK_GroupId;
                    row.cell = new string[]
                                {
                                    lst.PK_GroupId.ToString(),
                                    lst.GroupName,
                                    lst.Description,
                                    lst.IsActive == true ? "InActive" : "Active"
                                }.ToList();
                    jsonData.rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);                
            }
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddEditRole(int RoleId, string RoleName, string RoleDescription, ModuleAccessRelation[] selectedModules)
        {
            string errMsg = "success";
            bool success = true;

            try
            {
                if (!_userSecurity.CheckRoleName(RoleName, RoleId))
                {
                    _userSecurity.SaveRole(RoleId, RoleName, RoleDescription, selectedModules, CurrentUser.ID);
                }
                else
                {
                    errMsg = "RoleExists";
                    success = false;
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);

                errMsg = ex.Message;
                success = false;
            }

            var retJSON = new
            {
                success = success,
                msg = errMsg
            };

            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetRoleStatus(int RoleId, string status)
        {
            string errMsg = "success";
            bool success = true;

            try
            {
                _userSecurity.ToggleRoleStatus(RoleId, status, CurrentUser.ID);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);

                errMsg = "error";
                success = false;
            }

            var retJSON = new
            {
                success = success,
                msg = errMsg
            };

            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult CreateUpdateRole(int RoleId)
        {
            AddEditRoleViewModel model = new AddEditRoleViewModel();

            FADTOGroupMaster role = new FADTOGroupMaster();

            try
            {
                if (RoleId > 0)
                {
                    role = _userSecurity.RoleInfoById(RoleId);
                }

                model.FAGroupMaster = role;
                model.FAModuleArea = _userSecurity.ListAreas();
                model.FAModule = _userSecurity.GetModules();
                
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }
            return View(model);
        }
    }
}