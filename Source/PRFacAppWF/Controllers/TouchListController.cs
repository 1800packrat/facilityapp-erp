﻿namespace PRFacAppWF.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using PRFacAppWF.CodeHelpers;
    using System.Data;
    using Entities.Touches;
    using Entities;
    using System.Linq;

    [HandleJsonExceptionAttribute]
    [Authorize]
    public class TouchListController : BaseController
    {
        private readonly WarehouseTouches oWarehouseTouches;

        public TouchListController()
        {
            if (oWarehouseTouches == null)
                oWarehouseTouches = new WarehouseTouches();
        }


        public ActionResult Index()
        {
            GlobalSettings gs = new GlobalSettings();

            TouchListFilterCriteria FC = new TouchListFilterCriteria
            {
                StartDate = gs.FAWTSearchStartDate,
                EndDate = gs.FAWTSearchEndDate,
                StatusTypes = oWarehouseTouches.GetWHTStatusList()
            };

            return View(FC);
        }

        [HttpPost]
        public JsonResult SearchTouches(TouchListFilterCriteria FilterCriteria)
        {
            ResultSet<WATouchInfo> lstTI = new ResultSet<WATouchInfo>();
            AjaxResponse<ResultSet<WATouchInfo>> response = new AjaxResponse<ResultSet<WATouchInfo>>();
            try
            {
                FilterCriteria.LocationCode = CurrentSiteInfo.LocationCode;

                lstTI.rows = oWarehouseTouches.GetWATouchs(FilterCriteria);
                lstTI.total = lstTI.rows.Count;
                lstTI.page = 1;
                lstTI.records = lstTI.rows.Count();

                List<WATouchInfo> finalTouchList = new List<WATouchInfo>();

                response.data = lstTI;
                response.success = true;
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = ex.Message;
            }

            #region Added by Sohan - for "Invalid JSON primitive" unexpected Error when no data. 
            if (!response.success && response.message.Contains("Invalid JSON primitive"))
            {
                response.message = "Error - No Data Found !!";
                response.success = true;
                response.data = new ResultSet<WATouchInfo>() { rows = new List<WATouchInfo>(), page = 1, total = 0, records = 0 };
            }
            #endregion

            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public JsonResult SaveEditTouchInfo(WATouchInfo touchInfo)
        {
            var response = new AjaxResponse<object>();
            try
            {
                oWarehouseTouches.HandleUpdateWarehouseTouchInfo(touchInfo, CurrentUser.ID);
                response.success = true;
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EditTouchInfo(string touchType, int statusID, DateTime touchScheduleDate)
        {
            var response = new AjaxResponse<List<FAWTTouchActions>>();
            response.success = true;
            response.message = "";
            try
            {
                response.data = oWarehouseTouches.GetActionsByTouchType(touchType, statusID, touchScheduleDate, Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum));
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.message = ex.Message;
                response.success = false;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateUnitName(string unitName)
        {
            var response = new AjaxResponse<bool>();
            response.success = true;
            try
            {
                response.data = oWarehouseTouches.IsValidUnitName(unitName);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.message = ex.Message;
                response.success = false;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTouchHistory(int QORID, string TouchType, int SeqNumber)
        {
            var response = new AjaxResponse<object>();
            response.success = true;
            response.message = "";
            try
            {
                var touchHistory = oWarehouseTouches.GetTouchHistory(QORID, TouchType, SeqNumber);
                response.data = new { touchHistory = RenderPartialView("~/Views/TouchList/partial/_touchHistoryBody.cshtml", touchHistory.AsEnumerable()) };
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.message = ex.Message;
                response.success = false;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #region BOL

        public JsonResult GetBOLURL(string qorid)
        {
            return null;
        }
        #endregion


    }
}
