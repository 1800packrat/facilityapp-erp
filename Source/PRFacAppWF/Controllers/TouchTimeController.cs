﻿namespace PRFacAppWF.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using PRFacAppWF.CodeHelpers;
    using PRFacAppWF.models;
    using PRFacAppWF.Repository.Services;

    [HandleJsonExceptionAttribute]
    [Authorize]
    public class TouchTimeController : BaseController
    {
        private readonly IPRSService prsService;

        public TouchTimeController()
        {
            if (this.prsService == null)
                this.prsService = new PRSService();
        }

        public ActionResult Index()
        {
            ViewBag.FacilityList = this.prsService.GetFacilityListByUserId(CurrentUser.ID);
            return View();
        }

        public PartialViewResult SearchTouchTime(DateTime Date, string SortCol = null, string SortDir = null)
        {
            List<TouchTime> lstAS = new List<TouchTime>();
            try
            {
                SortCol = SortCol == "" ? "FacilityName" : SortCol;
                SortDir = SortDir == "" ? "ASC" : SortDir;
                lstAS = this.prsService.GetTouchTimeList(Date, CurrentUser.ID, SortCol, SortDir);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return PartialView("partial/_listTouchTimeSearchResults", lstAS);
        }

        public ActionResult GetFacilityTouchTimeList(string sidx, string sord, int page, int rows, int FacilityId)
        {
            try
            {
                var varTouchTimeResults = this.prsService.SearchFacilityTouchTimeList(FacilityId);
                int totalCount = varTouchTimeResults.Count();
                var sortedResults = FilterTouchTimeGrid(varTouchTimeResults, sidx, sord, page, rows);
                
                var pageSize = 10;
                var totalRecords = totalCount;
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
               
                var IsFullAccess = false;

                if (FacilityId > 0)
                    IsFullAccess = Security.SecuredEntityDelegate.CanUserExecute(Entities.Enums.ModuleName.TouchTime, Entities.Enums.AccessLevel.ReadAndWrite, Convert.ToString(FacilityId));
                else
                    IsFullAccess = Security.SecuredEntityDelegate.CanUserExecute(Entities.Enums.ModuleName.TouchTime, Entities.Enums.AccessLevel.ReadAndWrite, true);

                var jsonData = new
                {
                    total = totalPages,
                    page = page,
                    isfullaccess = IsFullAccess,
                    facilityId = FacilityId,
                    records = totalRecords,
                    rows = (from lst in sortedResults
                            select new
                            {
                                i = lst.CAPTouchtimeId,
                                cell = new string[]
                                {
                                    lst.CAPTouchtimeId.ToString(),
                                    lst.DE.ToString(),
                                    lst.DF.ToString(),
                                    lst.RE.ToString(),
                                    lst.RF.ToString(),
                                    lst.OBO.ToString(),
                                    lst.IBO.ToString(),
                                    lst.WA.ToString(),
                                    lst.CC.ToString(),
                                    lst.Startdate.HasValue ? lst.Startdate.Value.ToShortDateString():"",
                                    lst.Enddate.HasValue ? lst.Enddate.Value.ToShortDateString():"",                                    
                                    lst.State.ToString(),
                                    lst.FacilityId.ToString()
                                }
                            }).ToArray()
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);

                var jsonData = new
                {
                    total = 0,
                    page = 0,
                    records = 0,
                    rows = new string[0]
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        private IEnumerable<TouchTime> FilterTouchTimeGrid(IEnumerable<TouchTime> data, string sidx, string sord, int page, int rows)
        {

            int skiprows = (page - 1) * rows;
            ////Since in the UI we are displaying CapacityDate column as CapacityDateDisp.
            sidx = sidx == "" ? "Startdate" : sidx;
            if (sord == "desc")
                data = data.OrderByDescending(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);
            else
                data = data.OrderBy(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);

            return data;
        }

        public ActionResult UpdateTouchTimeRecords(int FacilityId)
        {
            ViewBag.FacilityId = FacilityId;
            return View();
        }

        [HttpPost]
        public JsonResult UpdateTouchTimeRecords(FormCollection formCollection)
        {
            DateTime? endDate = null;
            int DE = 0, DF = 0, RE = 0, RF = 0, OBO = 0, IBO = 0, WA = 0, CC = 0;
            bool isNullEnddate = false;
            string result = string.Empty;
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                int facilityid = Convert.ToInt32(formCollection["hidfacilityid"]);
                DateTime startDate = Convert.ToDateTime(formCollection["Startdate"]);
                isNullEnddate = Convert.ToBoolean(formCollection["hidNullEndDate"]);
                if (!isNullEnddate)
                    endDate = Convert.ToDateTime(formCollection["Enddate"]);
                DE = Convert.ToInt32(Convert.ToDecimal(formCollection["DE"]));
                DF = Convert.ToInt32(Convert.ToDecimal(formCollection["DF"]));
                RE = Convert.ToInt32(Convert.ToDecimal(formCollection["RE"]));
                RF = Convert.ToInt32(Convert.ToDecimal(formCollection["RF"]));
                OBO = Convert.ToInt32(Convert.ToDecimal(formCollection["OBO"]));
                IBO = Convert.ToInt32(Convert.ToDecimal(formCollection["IBO"]));
                WA = Convert.ToInt32(Convert.ToDecimal(formCollection["WA"]));
                CC = Convert.ToInt32(Convert.ToDecimal(formCollection["CC"]));

                string lstAS = this.prsService.UpdateFacilityTouchTimeValues(CurrentUser.ID, facilityid, startDate, endDate, DE, DF, RE, RF, OBO, IBO, WA, CC, isNullEnddate);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };
            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetTouchtimetoCorporateDefault(FormCollection formCollection)
        {
            DateTime startDate = DateTime.Now;
            int facilityid = 0;
            string result = string.Empty;
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                startDate = Convert.ToDateTime(formCollection["Startdate"]);
                facilityid = Convert.ToInt32(formCollection["hidfacilityid"]);
                string lstAS = this.prsService.SetTouchtimetoCorporateDefault(CurrentUser.ID, facilityid, startDate);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };
            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }
    }
}