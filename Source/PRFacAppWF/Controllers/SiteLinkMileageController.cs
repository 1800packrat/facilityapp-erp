﻿namespace PRFacAppWF.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using PRFacAppWF.CodeHelpers;
    using PRFacAppWF.models;
    using PRFacAppWF.Repository.Services;

    [HandleJsonExceptionAttribute]
    [Authorize]
    public class SiteLinkMileageController : BaseController
    {
        private readonly IPRSService prsService;

        public SiteLinkMileageController()
        {
            if (this.prsService == null)
                this.prsService = new PRSService();
        }

        public ActionResult Index()
        {
            ViewBag.FacilityList = this.prsService.GetFacilityListByUserId(CurrentUser.ID);
            return View();
        }

        public PartialViewResult SearchSLMileage(DateTime Date, string SortCol = null, string SortDir = null)
        {
            List<SLMileage> lstAS = new List<SLMileage>();
            try
            {
                SortCol = SortCol == "" ? "FacilityName" : SortCol;
                SortDir = SortDir == "" ? "ASC" : SortDir;
                lstAS = this.prsService.GetSLMileageList(Date, CurrentUser.ID, SortCol, SortDir);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return PartialView("partial/_listSLMileageSearchResults", lstAS);
        }

        public ActionResult GetSLMileageList(string sidx, string sord, int page, int rows, int FacilityId)
        {
            try
            {
                var varAvgMPHResults = this.prsService.SearchSLMileageList(FacilityId);
                int totalCount = varAvgMPHResults.Count();
                var sortedResults = FilterSLMileageGrid(varAvgMPHResults, sidx, sord, page, rows);

                                var pageSize = 10;
                var totalRecords = totalCount;
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                var IsFullAccess = false;
                
                if (FacilityId > 0)
                    IsFullAccess = Security.SecuredEntityDelegate.CanUserExecute(Entities.Enums.ModuleName.SiteLinkMileage, Entities.Enums.AccessLevel.ReadAndWrite, Convert.ToString(FacilityId));
                else
                    IsFullAccess = Security.SecuredEntityDelegate.CanUserExecute(Entities.Enums.ModuleName.SiteLinkMileage, Entities.Enums.AccessLevel.ReadAndWrite, true);

                var jsonData = new
                {
                    total = totalPages,
                    page = page,
                    isfullaccess = IsFullAccess,
                    facilityId = FacilityId,
                    records = totalRecords,
                    rows = (from lst in sortedResults
                            select new
                            {
                                i = lst.CAPSLMileageAdjustmentId,
                                cell = new string[]
                                {
                                    lst.CAPSLMileageAdjustmentId.ToString(),
                                    lst.SLMileageAdjustment.ToString(),
                                    lst.Startdate.HasValue ? lst.Startdate.Value.ToShortDateString():"",
                                    lst.Enddate.HasValue ? lst.Enddate.Value.ToShortDateString():"", 
                                    lst.State.ToString(),
                                    lst.FacilityId.ToString()
                                }
                            }).ToArray()
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);

                var jsonData = new
                {
                    total = 0,
                    page = 0,
                    records = 0,
                    rows = new string[0]
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        private IEnumerable<SLMileage> FilterSLMileageGrid(IEnumerable<SLMileage> data, string sidx, string sord, int page, int rows)
        {

            int skiprows = (page - 1) * rows;
            ////Since in the UI we are displaying CapacityDate column as CapacityDateDisp.
            sidx = sidx == "" ? "Startdate" : sidx;
            if (sord == "desc")
                data = data.OrderByDescending(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);
            else
                data = data.OrderBy(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(skiprows).Take(rows);

            return data;
        }

        public ActionResult UpdateSLMileageRecords(int FacilityId)
        {
            ViewBag.FacilityId = FacilityId;
            return View();
        }

        [HttpPost]
        public JsonResult UpdateSLMileageRecords(FormCollection formCollection)
        {
            DateTime? endDate = null;
            decimal slmileage = 0.00M;
            bool isNullEnddate = false;
            string result = string.Empty;
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                int facilityid = Convert.ToInt32(formCollection["hidfacilityid"]);
                DateTime startDate = Convert.ToDateTime(formCollection["Startdate"]);
                slmileage = Convert.ToDecimal(formCollection["SLMileageAdjustment"]);
                isNullEnddate = Convert.ToBoolean(formCollection["hidNullEndDate"]);
                if (!isNullEnddate)
                    endDate = Convert.ToDateTime(formCollection["Enddate"]);
                string lstAS = this.prsService.UpdateSLMileageValues(CurrentUser.ID, facilityid, startDate, endDate, slmileage, isNullEnddate);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };
            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetSLMileage(FormCollection formCollection)
        {
            DateTime startDate = DateTime.Now;
            int facilityid = 0;
            string result = string.Empty;
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                startDate = Convert.ToDateTime(formCollection["Startdate"]);
                facilityid = Convert.ToInt32(formCollection["hidfacilityid"]);
                string lstAS = this.prsService.SetSLMileagetoCorporateDefault(CurrentUser.ID, facilityid, startDate);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };
            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

    }
}