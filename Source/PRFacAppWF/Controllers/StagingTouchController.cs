﻿using System;
using System.Linq;
using System.Web.Mvc;
using PR.LocalLogisticsSolution.Interfaces;
using PR.LocalLogisticsSolution.Model;
using PRFacAppWF.CodeHelpers;
using PRFacAppWF.models;
using System.Web.Script.Serialization;

namespace PRFacAppWF.Controllers
{
    [HandleJsonExceptionAttribute]
    [Authorize]
    public class StagingTouchController : Controller
    {
        #region Data members

        private ITouchService _touchService = null;
        private IStagingTouchService _stagingTouchService = null;

        public string LoggedInUsername { get { return Convert.ToString(Session["UserId"]); } }

        #endregion

        #region Constructor

        //
        // GET: /LocalLogistics/
        internal StagingTouchController()
        {

        }

        // GET: /LocalLogistics/
        public StagingTouchController(ITouchService touchService, IStagingTouchService stagingTouchService)
        {
            _touchService = touchService;
            _stagingTouchService = stagingTouchService;
        }

        #endregion

        #region Action Methods

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SearchTruckStatusList()
        {
            SearchFilter<StagingTouchFilterCriteria> tsSearchFilter = new SearchFilter<StagingTouchFilterCriteria>();
            tsSearchFilter.filter = new StagingTouchFilterCriteria();

            tsSearchFilter.rows = Convert.ToInt32(Request.Params["rows"]);
            tsSearchFilter.page = Convert.ToInt32(Request.Params["page"]);
            tsSearchFilter.sidx = Request.Params["sidx"];
            tsSearchFilter.sord = Request.Params["sord"];
            string filter = Request.Params["filter"];

            if (!string.IsNullOrEmpty(filter))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                tsSearchFilter.filter = serializer.Deserialize<StagingTouchFilterCriteria>(filter);
            }
            return GetTruckStatusDetailLst(tsSearchFilter);
        }

        public JsonResult TransferOutTouch(int QORId, int dummyBillingQORId, int billingQORId, int TripId, int StarsId)
        {
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                _stagingTouchService.TransferOut(QORId, dummyBillingQORId, billingQORId, TripId, StarsId, "FacilityApp", LoggedInUsername);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                errMsg = ex.Message;
                success = false;
            }

            var retJSON = new
            {
                success = success,
                msg = errMsg
            };

            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TransferInTouch(string UnitName, int UnitId, int OrgQORId, int DestQORId, int BillingQORId, int TripId, int StarsId, string OrigLocationCode, string DestLocationCode, int ContainerType)
        {
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                _stagingTouchService.TransferIn(UnitName, UnitId, OrgQORId, DestQORId, BillingQORId, TripId, StarsId, OrigLocationCode, DestLocationCode, Guid.NewGuid().ToString(), ContainerType, "FacilityApp", LoggedInUsername);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                errMsg = ex.Message;
                success = false;
            }

            var retJSON = new
            {
                success = success,
                msg = errMsg
            };

            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        private ActionResult GetTruckStatusDetailLst(SearchFilter<StagingTouchFilterCriteria> tsSearchFilter)
        {
            var res = _stagingTouchService.GetStagingTouches(Helper.CurrentSiteInfo, tsSearchFilter.filter.StagingTouchStartDate.Value, tsSearchFilter.filter.StagingTouchEndDate.Value);
            SearchResults<Touch> rtObj = new SearchResults<Touch>();
            rtObj.rows = res.ToList();
            rtObj.page = tsSearchFilter.page;

            int records = (rtObj.rows.Count() > 0 ? rtObj.rows.Count() : 0);
            float totPages = (records > 0 ? ((float)records / tsSearchFilter.rows) : 1);
            int totpages = (int)Math.Ceiling(totPages);
            rtObj.records = records;
            rtObj.total = totpages;
            rtObj.error = "";

            return this.Json(rtObj);
        }

        #endregion
    }
}