﻿namespace PRFacAppWF.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using PRFacAppWF.CodeHelpers;
    using PRFacAppWF.Repository.Services;
    using PRFacAppWF.Repository.models;

    [HandleJsonExceptionAttribute]
    [Authorize]
    public class TruckTransferReasonController : BaseController
    {
        private readonly IPRSService prsService;

        public TruckTransferReasonController()
        {
            if (this.prsService == null)
                this.prsService = new PRSService();
        }

        public ActionResult Index()
        {
            this.ViewBag.GetAllTransferReasons = this.prsService.GetTransferReason();
            return View();
        }

        public PartialViewResult SearchTransferReasons(string SearchKey, string SortCol = null, string SortDir = null)
        {
            List<CAPTruckTransferReason> lstAS = new List<CAPTruckTransferReason>();
            try
            {
                SortCol = SortCol == "" ? "FacilityName" : SortCol;
                SortDir = SortDir == "" ? "ASC" : SortDir;
                lstAS = this.prsService.GetTransferReasonSearchresults(SearchKey);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }
            return PartialView("partial/_listTransferReasonSearchResults", lstAS);
        }

        public JsonResult GetTransferReasonbyId(int TransferReasonId)
        {
            string Transferreasontxt = "";
            if (TransferReasonId > 0)
            {
                try
                {
                    Transferreasontxt = this.prsService.GetTransferReasonbyId(TransferReasonId);
                }
                catch (Exception ex)
                {
                    Helper.LogError(ex);
                }
            }
            return Json(Transferreasontxt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveTransferReasonwithId(int TransferReasonId, string TransferReason)
        {
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                string result = this.prsService.SaveTransferReasonwithId(TransferReasonId, TransferReason);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };
            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetTransferReasonActiveInactive(int TransferReasonId, int status)
        {
            string errMsg = string.Empty;
            bool success = true;
            try
            {
                string result = this.prsService.SetTransferReasonActiveInactive(TransferReasonId, status);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                errMsg = ex.Message;
            }

            var retJSON = new
            {
                success = success,
                error = errMsg
            };
            return Json(retJSON, JsonRequestBehavior.AllowGet);
        }
    }
}