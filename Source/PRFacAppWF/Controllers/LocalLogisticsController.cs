﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PR.LocalLogisticsSolution.Interfaces;
using PR.LocalLogisticsSolution.Model;
using PR.Entities;
using PRFacAppWF.CodeHelpers;
using PRFacAppWF.models;
using PRFacAppWF.Repository.Services;
using PRFacAppWF.Entities;
using static PR.UtilityLibrary.ERP_Enums;

namespace PRFacAppWF.Controllers
{
    [HandleJsonExceptionAttribute]
    [Authorize]
    public class LocalLogisticsController : BaseController
    {
        #region Data members

        private ITouchService _touchService = null;
        private IUserService _userService = null;

        #endregion

        #region Constructor

        internal LocalLogisticsController()
        {

        }

        public LocalLogisticsController(ITouchService touchService, IUserService userService)
        {
            _touchService = touchService;
            _userService = userService;
        }

        #endregion

        #region Action Methods

        public ActionResult Index()
        {
            var date = TempData["scheduleDate"]; // scheduleDate we will get from the DispatchDashboard scree
            this.ViewBag.Date = date ?? DateTime.Now.ToShortDateString();

            this.ViewBag.latitude = CurrentSiteInfo.SiteAddress.Latitude;
            this.ViewBag.longitude = CurrentSiteInfo.SiteAddress.Longitude;
            var jsonSerialiser = new System.Web.Script.Serialization.JavaScriptSerializer();
            this.ViewBag.latLang = jsonSerialiser.Serialize(getLatLongSites());
            this.ViewBag.FacilitiesMarket = getFacilitiesUnderMarket();
            this.ViewBag.TouchHandlingTimes = FetchTouchStopHandlingTimes();

            this.ViewBag.USStates = Helper.GetUSStates();

            return View();
        }

        private Dictionary<string, string> getFacilitiesUnderMarket()
        {
            Dictionary<string, string> FacsMarket = new Dictionary<string, string>();

            FacsMarket.Add(CurrentSiteInfo.GlobalSiteNum, CurrentSiteInfo.SiteAddress.City);

            if (CurrentSiteInfo.MarketId != 0 && CurrentSiteInfo.MarketFacilities.Count > 0)
            {
                foreach (SiteInfo m in (List<SiteInfo>)(CurrentSiteInfo.MarketFacilities))
                {
                    FacsMarket.Add(m.GlobalSiteNum, m.SiteAddress.City);
                }
            }

            return FacsMarket;
        }

        private List<FacilityLatLong> getLatLongSites()
        {
            var LatLongSites = new List<FacilityLatLong>();
            try
            {
                if (CurrentSiteInfo.MarketId != 0 && CurrentSiteInfo.MarketFacilities.Count > 0)
                {
                    foreach (SiteInfo m in (List<SiteInfo>)(CurrentSiteInfo.MarketFacilities))
                    {
                        FacilityLatLong faclatlang = new FacilityLatLong();
                        faclatlang.Latitude = m.SiteAddress.Latitude;
                        faclatlang.Longitude = m.SiteAddress.Longitude;
                        LatLongSites.Add(faclatlang);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return LatLongSites;
        }

        public List<Touch> GetAllTouchesFromMarkets(Market market)
        {
            List<Touch> allTouches = new List<Touch>();

            foreach (var facility in market.Facilities)
            {
                allTouches.AddRange(facility.Touches);
            }

            return allTouches;
        }

        public JsonResult GetTransporationTouchesIntoSession(DateTime date)
        {
            var response = new AjaxResponse<string>();
            try
            {
                GlobalSettings gs = new GlobalSettings();

                SessionAllTransportationTouches = _touchService.GetTransportationTouches(CurrentSiteInfo, date, gs.IsGetLiveData);

                response.success = true;
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = ex.Message; // "Error occurred while loading touch information into session.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Before you call this method, GetTransporationTouchesIntoSession should be invoked to get SL touches to Session
        /// This has been changed as a part of SL Calls Optimization
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public PartialViewResult FetchUnassignedTouches(DateTime date)
        {
            try
            {
                var marketTouchResult = _touchService.GetUnassignedTouches(CurrentSiteInfo, date, SessionAllTransportationTouches);

                Session[SessionKeyForUnassignedTouches(date)] = GetAllTouchesFromMarkets(marketTouchResult);

                this.ViewBag.CurrentSiteInfo = CurrentSiteInfo;

                return PartialView("unassignedTouches", marketTouchResult);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return null;
            }
        }

        public PartialViewResult FetchUTouchesFromUpdatedAddressList(DateTime date)
        {
            try
            {
                var touchResult = GetUnAssignedTouchesFromSession(date).Where(x => x.TouchId == 0).ToList();
                return PartialView("unassignedTouches", touchResult);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return PartialView("unassignedTouches", null);
            }
        }

        public PartialViewResult GetDriverTouches(DateTime date)
        {
            Market lDriversModel = new Market();
            List<Trucks> lTrucks = new List<Trucks>();

            try
            {
                lDriversModel = _touchService.GetMarketLoads(date, CurrentSiteInfo, SessionAllTransportationTouches);

                lTrucks = GetTrucksFromPRDB(date, CurrentSiteInfo);
            }
            catch (Exception ex) { Helper.LogError(ex); }

            this.ViewBag.lTrucks = lTrucks;

            return PartialView("MarketLoad", lDriversModel);
        }

        public PartialViewResult PrintLoad(int loadId)
        {
            try
            {
                var load = _touchService.FetchLoadByLoadId(loadId);
                return PartialView("PrintLoad", load);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return PartialView("PrintLoad", null); ;
            }
        }

        /// <summary>
        /// This method assume, SessionAllTransportationTouches has SL touches for respective date
        /// </summary>
        /// <param name="market"></param>
        /// <returns></returns>
        private int GetUnassignedTouchesCount(Market market)
        {
            List<string> allLoadTouchesOrderNumber = new List<string>();
            market.Facilities.ForEach(f => f.Loads.ForEach(l =>
            {
                if (l.Touches != null)
                {
                    allLoadTouchesOrderNumber.AddRange((l.Touches ?? new List<Touch>()).Select(s => s.OrderNumber).ToList<string>());
                }
            }));

            return (from unassgined in SessionAllTransportationTouches
                    where !allLoadTouchesOrderNumber.Contains(unassgined.OrderNumber)
                    select unassgined).Count();
        }

        private List<Trucks> GetTrucksFromPRDB(DateTime date, SiteInfo currentSite)
        {
            var prsService = new PRSService();
            List<Trucks> lTrucks = new List<Trucks>();

            string storeNumbers = currentSite.GlobalSiteNum;
            if (currentSite.IsMarket)
                storeNumbers = $"{storeNumbers},{string.Join(",", currentSite.MarketFacilities.Select(m => m.GlobalSiteNum.ToString()).ToList())}";

            var lstTrucks = prsService.GetTruckDetails(storeNumbers, date);

            lTrucks.AddRange(lstTrucks.Select(tr => new Trucks
            {
                TruckNumber = tr.FleetID,
                FleetID = tr.FleetID,
                TruckStatus = tr.TruckStatus,
                TruckId = tr.TruckId,
                SiteNumber = tr.SiteNumber,
                IsAlreadyInLoad = tr.IsAlreadyInLoad
            }).ToList());

            return lTrucks;
        }



        public JsonResult GetTrucks(DateTime date)
        {
            try
            {
                List<Trucks> lTrucks = GetTrucksFromPRDB(date, CurrentSiteInfo);

                var rtTruck = (from s in lTrucks
                               select new
                               {
                                   Comments = s.Comments,
                                   EnterdBy = s.EnterdBy,
                                   EntryDate = s.EntryDate,
                                   Facility = s.Facility,
                                   FleetID = s.FleetID,
                                   TruckId = s.TruckId,
                                   TruckNumber = s.TruckNumber,
                                   TruckStatus = s.TruckStatus,
                                   SiteNumber = s.SiteNumber,
                                   FacCode = s.FacCode,
                                   IsAlreadyInLoad = s.IsAlreadyInLoad
                               });

                return Json(rtTruck, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
        }

        public List<Trailers> GetTrailer()
        {
            List<Trailers> lTrailers = _touchService.GetTrailers(CurrentSiteInfo.GlobalSiteNum);
            return lTrailers;
        }

        private SiteInfo GetSiteInformationBySiteNumber(string siteNumber)
        {
            return (CurrentSiteInfo.GlobalSiteNum == siteNumber ? CurrentSiteInfo : CurrentSiteInfo.MarketFacilities.Where(m => m.GlobalSiteNum == siteNumber).FirstOrDefault());
        }

        public ActionResult AddTouchtoLoad(DateTime date, int loadId, int? touchId, int driverId, string homeSiteNumber, string driverSiteNumber, string orderNumber, int truckID)
        {
            string result = "";
            try
            {
                SiteInfo touchHomeFacility = GetSiteInformationBySiteNumber(homeSiteNumber);
                SiteInfo touchDriverFacility = GetSiteInformationBySiteNumber(driverSiteNumber);

                if (touchDriverFacility == null)
                {
                    string errorMsg = "OrderNumber: " + orderNumber + ", not able to get Home facility. SiteNumber: " + touchDriverFacility;
                    Helper.LogError(new Exception(errorMsg));

                    throw new Exception(errorMsg);
                }

                int dbTouchId = 0;
                bool isOldLoad = loadId > 0;

                if (touchId != null && touchId.Value > 0)
                {
                    string reasons = string.Empty;
                    bool canReassign = _touchService.CheckTouchCanRemoveFromLoad(touchId.Value, out result);

                    if (canReassign == true)
                    {
                        _touchService.MoveMarketTouchToAnotherLoad(touchId.Value, driverId, ref loadId, date, touchDriverFacility, CurrentSiteInfo, truckID, Helper.CurrentUser.UserName);
                    }
                    else
                    {
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }

                    dbTouchId = Convert.ToInt32(touchId);
                }
                else
                {
                    //retrieve from session
                    Touch newTouch = GetUnAssignedTouchesFromSession(date, orderNumber);

                    if (newTouch != null)
                        dbTouchId = _touchService.AddTouchToMarketLoad(date, newTouch, touchDriverFacility, touchHomeFacility, loadId, driverId, truckID, Helper.CurrentUser.UserName);
                    else
                        throw new Exception("Touch is missing");

                    loadId = Convert.ToInt32(newTouch.LoadId);
                }

                Touch dbTouch = _touchService.FetchTouchByTouchId(dbTouchId, CurrentSiteInfo);
                return PartialView("AddTouchtoLoad", dbTouch);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                result = "Failure";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddBreakTouchStop(DateTime date, int loadId, int breakTimeInMinutes, string comment)
        {
            string msg = "";
            bool success = true;
            Touch touch = new Touch();
            try
            {
                if (loadId > 0)
                {
                    int touchId = _touchService.AddBreakTouch(date, loadId, CurrentSiteInfo, breakTimeInMinutes, comment);

                    touch = _touchService.FetchTouchByTouchId(touchId, CurrentSiteInfo);
                }
                else
                {
                    throw new Exception("You cannot add busy touch");
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                msg = ex.Message;
            }

            var rtObj = new
            {
                success = success,
                errorMsg = msg,
                TouchId = touch.TouchId,
                TouchStopId = touch.Stops[0].TouchStopId
            };

            return Json(rtObj, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveBreakTouchAndStop(int stopId)
        {
            string msg = "";
            bool success = true;
            Touch touch = new Touch();
            try
            {
                if (stopId > 0)
                {
                    success = _touchService.RemoveBreakTouchAndStop(stopId, Helper.CurrentUser.UserName);
                }
                else
                {
                    throw new Exception("You cannot delete busy touch");
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                msg = ex.Message;
            }

            var rtObj = new
            {
                success = success,
                errorMsg = msg
            };

            return Json(rtObj, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveDriverTouches(DateTime date, int loadId, int driverId, int truckID, string driverSiteNumber, string[] touchIdsAndOrderNumbers)
        {
            string result = "success";
            try
            {
                int dbTouchId = 0;
                bool isOldLoad = loadId > 0;
                int extTouchId = 0;
                string extOrdNumber = string.Empty;
                string reasonsForNotReassign = string.Empty;
                string reason = string.Empty;
                foreach (string touchdetails in touchIdsAndOrderNumbers)
                {
                    string[] io = touchdetails.Split('|');
                    if (io.Length == 2)
                    {
                        extTouchId = 0;
                        int.TryParse(io[0], out extTouchId);
                        if (extTouchId > 0)
                        {
                            reason = string.Empty;
                            bool canReassign = _touchService.CheckTouchCanRemoveFromLoad(extTouchId, out reason);

                            if (canReassign == true)
                            {
                                SiteInfo touchDriverFacility = GetSiteInformationBySiteNumber(driverSiteNumber);
                                _touchService.MoveMarketTouchToAnotherLoad(extTouchId, driverId, ref loadId, date, touchDriverFacility, CurrentSiteInfo, truckID, Helper.CurrentUser.UserName);
                            }
                            else
                            {
                                reasonsForNotReassign = reasonsForNotReassign + (string.IsNullOrEmpty(reasonsForNotReassign) ? "" : "<br/>") + extOrdNumber + ": " + reason;
                            }
                        }
                        else
                        {
                            extOrdNumber = io[1];

                            Touch newTouch = GetTouchFromSession(extOrdNumber);

                            if (newTouch != null)
                            {
                                SiteInfo touchDriverFacility = GetSiteInformationBySiteNumber(driverSiteNumber);
                                SiteInfo touchHomeFacility = GetSiteInformationBySiteNumber(newTouch.GlobalSiteNum);

                                dbTouchId = _touchService.AddTouchToMarketLoad(date, newTouch, (touchDriverFacility ?? CurrentSiteInfo), touchHomeFacility, loadId, driverId, truckID, Helper.CurrentUser.UserName);
                                loadId = Convert.ToInt32(newTouch.LoadId);
                            }
                            else
                                throw new Exception("Touch is missing");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                result = "Failure";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveTouchFromLoad(int touchId, DateTime date)
        {
            string result = string.Empty;
            var response = new AjaxResponse<string>();
            try
            {
                bool canRemove = _touchService.CheckTouchCanRemoveFromLoad(touchId, out result);

                if (canRemove == true)
                {
                    Touch dbTouch = _touchService.FetchTouchByTouchId(touchId, CurrentSiteInfo);

                    _touchService.RemoveTouchFromMarketLoad(touchId, CurrentSiteInfo, Helper.CurrentUser.UserName);

                    dbTouch.TouchId = 0;//making touchid = 0 as it is going to unassigned list
                    dbTouch.Stops.Clear(); //clearing all stops before adding to unassigned list

                    // adding touch back to unassgined list as it is removed from driver load.
                    AddRemovedTouchToUnAssignedTouchSession(dbTouch, date);

                    response.data = RenderPartialView("RemoveTouchFromLoad", dbTouch);
                    response.success = true;
                }
                else
                {
                    response.success = false;
                    response.message = result;
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error occurred while removing touch from load. Please reload the page and try again!";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveCancleTouch(int touchId, int loadId)
        {
            string result = "";
            try
            {
                bool canRemove = _touchService.CheckTouchCanRemoveFromLoad(touchId, out result);

                if (canRemove == true)
                {
                    _touchService.RemoveTouchFromLoad(touchId, CurrentSiteInfo, Helper.CurrentUser.UserName);

                    _touchService.UpdateLoadLock(loadId, false);

                    result = "Success";
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                result = ex.Message;

            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateLoadTruck(int loadId, int truckId)
        {
            string result = "";
            try
            {
                _touchService.UpdateLoadTruck(loadId, truckId);

                result = "Success";
            }
            catch (Exception ex)
            {
                result = ex.Message;
                Helper.LogError(ex);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadLock(int loadId, bool isLock)
        {
            string msg = "";
            bool success = true;
            try
            {

                if (loadId > 0)
                {
                    _touchService.UpdateLoadLock(loadId, isLock);
                }
                else
                {
                    success = false;
                    throw new Exception("You cannot lock load");
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                success = false;
                msg = ex.Message;
            }

            var rtObj = new
            {
                success = success,
                errorMsg = msg
            };

            return Json(rtObj, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Before you call this method, GetTransporationTouchesIntoSession should be invoked to get SL touches to Session
        /// This has been changed as a part of SL Calls Optimization
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public JsonResult GetDrivers(DateTime date)
        {
            try
            {
                Market market = _touchService.GetMarketLoads(date, CurrentSiteInfo, SessionAllTransportationTouches);

                List<Load> marketLoads = new List<Load>();

                foreach (var facility in market.Facilities)
                {
                    marketLoads.AddRange(facility.Loads);
                }

                var drLoadList = (from load in marketLoads
                                  select new
                                  {
                                      DriverId = load.DriverId,
                                      LoadId = load.LoadId,
                                      TruckID = load.TruckID,
                                      IsLoadLocked = load.IsLocked,
                                      DriverName = load.DriverName,
                                      HasSLUpdates = load.HasSLUpdates,
                                      HasDeletedTouchs = load.HasDeletedTouchs,
                                      GlobalSiteNumber = load.GlobalSiteNumber,
                                      MarketID = (CurrentSiteInfo.GlobalSiteNum == load.GlobalSiteNumber ? CurrentSiteInfo : CurrentSiteInfo.MarketFacilities.Where(m => m.GlobalSiteNum == load.GlobalSiteNumber).FirstOrDefault()).MarketId,
                                      TouchCount = load.DisplayTouches != null ? load.DisplayTouches.Count : 0,
                                      DayWork = load.DriverSchedule.DayWork.ToString()
                                  });
                var retJSON = new
                {
                    TotalUnassignedTouches = GetUnassignedTouchesCount(market),
                    driverLoadList = drLoadList
                };
                Session[SessionKeyForDirverLoads] = marketLoads;

                return Json(retJSON, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Before you call this method, GetTransporationTouchesIntoSession should be invoked to get SL touches to Session
        /// This has been changed as a part of SL Calls Optimization
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public JsonResult PlotMarkers(DateTime date)
        {
            List<Marker> markers = new List<Marker>();
            try
            {
                GlobalSettings gs = new GlobalSettings();
                List<Touch> touchResult = _touchService.GetTouchesForMarket(CurrentSiteInfo, date, SessionAllTransportationTouches);

                //Eleminate BusyTouches if it has
                touchResult = touchResult.Where(t => LoadHelper.ExcudeTouchTyps.ToList().Any(ex => ex.ToUpper().Contains(t.TouchType.ToUpper())) == false).OrderBy(ls => ls.RouteSequence).ToList();

                AllTouchesSession = touchResult;

                Session[SessionKeyForUnassignedTouches(date)] = touchResult.Where(X => X.TouchId <= 0).ToList();

                var groupFacilities = touchResult.Select(x => x.GlobalSiteNum).Distinct().ToList();

                Dictionary<string, string> assignedFacilityIds = new Dictionary<string, string>();
                int incrFacility = 1;

                foreach (string s in groupFacilities)
                {
                    assignedFacilityIds.Add(s, "F" + incrFacility);
                    incrFacility++;
                }

                touchResult.ForEach(X => SetMarker(X, assignedFacilityIds.Where(a => a.Key == X.GlobalSiteNum).Select(a => a.Value).FirstOrDefault().ToString(), ref markers));
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }
            return Json(markers.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteLoad(int loadId)
        {
            string result = string.Empty;
            try
            {
                _touchService.DeleteLoad(loadId);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                result = "Failure";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitOptimizationLoad(int loadId)
        {
            try
            {
                var load = _touchService.FetchLoadByLoadId(loadId);

                if (load != null)
                {
                    if (load.IsOptimized == true)
                    {
                        return StartStopOptimization(loadId, load);
                    }
                    else
                    {
                        return StartOptimization(loadId, load);
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return null;
            }
        }

        public PartialViewResult StartOptimization(int loadId, Load load = null)
        {
            try
            {
                this.ViewBag.Trailers = GetTrailer();
                load = load != null && load.LoadId > 0 ? load : _touchService.FetchLoadByLoadId(loadId);

                return PartialView("TouchOptimization", load);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return PartialView("TouchOptimization", null); ;
            }
        }

        public PartialViewResult RescheduleSkippedTouch(int loadId, int touchID)
        {
            try
            {
                _touchService.RescheduleSkippedTouch(touchID, CurrentSiteInfo);

                Load load = _touchService.FetchLoadByLoadId(loadId);
                this.ViewBag.Trailers = GetTrailer();

                return PartialView("TouchOptimization", load);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return PartialView("TouchOptimization", null); ;
            }
        }

        public ActionResult GroupTouches(int[] TouchIds, int[] AllTouchIdsOrder)
        {
            string rtStatus = "";
            try
            {
                var status = _touchService.GroupTouches(TouchIds, AllTouchIdsOrder);
                rtStatus = status.ToString();
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                rtStatus = ex.Message;
            }

            return Content(rtStatus.ToString());
        }

        public ActionResult UnGroupThisTouchGroup(int loadid, Guid guid)
        {
            try
            {
                var status = _touchService.UnGroupMarketTouchesBase(loadid, guid, CurrentSiteInfo);
                return Content(status.ToString());
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveTouchSortOrder(string[] touchTrailerMapIds, int loadId, string driverStartTime, TouchIdAndTime[] touchAndTime, bool isIgnoreWar)
        {
            try
            {
                if (!isIgnoreWar)
                {
                    _touchService.TruckScheduleValidation(loadId, Convert.ToDateTime(driverStartTime));
                }
                List<Touch> lstTouches = new List<Touch>();
                foreach (var touchinfo in touchAndTime)
                {
                    var touch = new Touch() { TouchId = touchinfo.touchID, ScheduledDate = touchinfo.touchDateTime };

                    lstTouches.Add(touch);
                }

                bool isTrailerReq = (touchTrailerMapIds != null && touchTrailerMapIds.Count() > 0);
                _touchService.SaveDriverStartTimeNTrailerRequired(loadId, driverStartTime, isTrailerReq);

                _touchService.SaveSortedMarketTouchInfo(loadId, lstTouches, touchTrailerMapIds, CurrentSiteInfo);

                return StartStopOptimization(loadId);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult StartStopOptimization(int loadId, Load load = null)
        {
            List<PR.Entities.Address> lstMarketAddress = new List<Address>();
            lstMarketAddress.Add(_touchService.ManageAddress(CurrentSiteInfo.SiteAddress));
            foreach (var marketfacility in CurrentSiteInfo.MarketFacilities)
            {
                lstMarketAddress.Add(_touchService.ManageAddress(marketfacility.SiteAddress));
            }

            List<PR.Entities.Address> thisloadAddress = _touchService.FetchAllAddressForThisLoad(loadId);

            ViewData["lstAddresss"] = thisloadAddress;
            this.ViewBag.lstAddress = thisloadAddress;
            this.ViewBag.lstWarehouseAddress = lstMarketAddress.Where(a => a.AddressType == PR.UtilityLibrary.PREnums.AddressType.Warehouse).ToList();
            load = ((load != null && load.LoadId > 0) ? load : _touchService.FetchLoadByLoadId(loadId));

            return PartialView("StopOptimization", load);
        }

        public ActionResult SaveStopOptimization(StopIdAndTime[] stopAndTime, int loadId, bool isOptimized)
        {
            string rtVal = "";
            try
            {
                List<Stops> lstStops = new List<Stops>();

                foreach (var stopinfo in stopAndTime)
                {
                    if (stopinfo.isOpenStop == true)
                    {
                        var stop = new Stops()
                        {
                            StopId = stopinfo.stopID,
                            StopSequence = stopinfo.sequence,
                            ScheduledStartTime = stopinfo.stopStartDateTime,
                            ScheduledEndTime = stopinfo.stopEndDateTime,
                            EstimatedStartDateTime = stopinfo.stopStartDateTime,
                            EstimatedEndDateTime = stopinfo.stopEndDateTime,
                            ChangedOrgAddressId = stopinfo.orgAddressId,
                            ChangedDestAddressId = stopinfo.destAddressId,
                            ActualDistance = stopinfo.actualDistance
                        };
                        lstStops.Add(stop);
                    }
                }

                _touchService.SaveOptimizedStops(lstStops, loadId);

                return StartStopOptimization(loadId);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                rtVal = "Failure";
            }
            return Json(rtVal, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAddress(AddressUpdateList[] addressList)
        {
            try
            {
                if (addressList != null)
                {
                    foreach (var address in addressList)
                    {
                        if (address.ID != 0 && !string.IsNullOrEmpty(address.Lat) && !string.IsNullOrEmpty(address.Long))
                        {
                            Address oaddress = new Address();
                            oaddress.ID = address.ID;
                            oaddress.Latitude = address.Lat;
                            oaddress.Longitude = address.Long;

                            //Update lat and Long values into DB
                            _touchService.UpdateAddressByID(oaddress);

                            //Update lat and long values into session
                            var LoadList = GetDriverLoadsFromSession();
                            LoadList.Where(l => l.Touches != null && l.Touches.Count > 0).ToList().ForEach(x => UpdateAddressInTouchList(x.Touches, address.Lat, address.Long, address.AddressType, address.OrderId, address.ID));
                            Session[SessionKeyForDirverLoads] = LoadList;
                            return Content("success");
                        }
                        else if (!string.IsNullOrEmpty(address.Lat) && !string.IsNullOrEmpty(address.Long))
                        {
                            var touchList = GetUnAssignedTouchesFromSession(DateTime.Today);
                            UpdateAddressInTouchList(touchList, address.Lat, address.Long, address.AddressType, address.OrderId, 0);
                            Session[SessionKeyForUnassignedTouches(DateTime.Today)] = touchList;
                            return Content("success");
                        }
                    }
                }
                return Content("error");
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return Content("error");
            }
        }

        private void UpdateAddressInTouchList(List<Touch> list, string lat, string longt, string addressType, string orderNum, int addressID = 0)
        {
            if (list != null)
            {
                foreach (var touch in list)
                {
                    if (addressID != 0)
                    {
                        if (touch.OriginAddress.ID == addressID)
                        {
                            touch.OriginAddress.Latitude = lat;
                            touch.OriginAddress.Longitude = longt;
                        }
                        if (touch.DestAddress.ID == addressID)
                        {
                            touch.DestAddress.Latitude = lat;
                            touch.DestAddress.Longitude = longt;
                        }
                    }
                    else if (touch.OrderNumber == orderNum && addressType == "origin")
                    {
                        touch.OriginAddress.Latitude = lat;
                        touch.OriginAddress.Longitude = longt;
                    }
                    else if (touch.OrderNumber == orderNum && addressType == "dest")
                    {
                        touch.DestAddress.Latitude = lat;
                        touch.DestAddress.Longitude = longt;
                    }
                }
            }
        }


        private List<Touch> getMissingTouches(DateTime date)
        { 
            List<Touch> touchResult = GetUnAssignedTouchesFromSession(date);

            #region Commented due to unreachable condition, added by Sohan

            ////SEFRP-TODO-RMVNICD
            //if (touchResult == null)  // This condition will never be true for ESB call
            //    touchResult = _touchService.FetchAllUnassignedTouches(CurrentSiteInfo, date);

            #endregion Commented due to unreachable condition, added by Sohan

            var drivertouches = GetDriverLoadsFromSession();

            foreach (var load in drivertouches)
            {
                if (load.Touches != null)
                {
                    foreach (var driverTouch in load.Touches)
                    {
                        if (touchResult.Where(t => t.OrderNumber == driverTouch.OrderNumber).Count() == 0)
                        {
                            touchResult.Add(driverTouch);
                        }
                    }
                }
            }
            return touchResult.Where(x => x.OriginAddress.IsAddressValid == false || x.DestAddress.IsAddressValid == false).Select(x => x).ToList();
        }

        public ActionResult GetMissingAddressListMapView(DateTime date)
        {
            try
            {
                List<Touch> touchResult = getMissingTouches(date);

                if (touchResult.Count > 0)
                {
                    return PartialView("partial/_AddressIssuePopupMapView", touchResult);
                }
                else
                    return Content("0");
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return Content("0");
            }
        }

        public ActionResult GetMissingAddressListForEditor(DateTime date)
        {
            try
            {
                List<Touch> touchResult = getMissingTouches(date);

                if (touchResult.Count > 0)
                {
                    return PartialView("partial/_EditAddresses", touchResult);
                }
                else
                    return Content("0");
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                return Content("0");
            }
        }

        public ActionResult ValidateAddress(Address addressToValidate)
        {
            var response = new AjaxResponse<ValidatedAddress>();
            try
            {
                PR.BusinessLogic.SMDBusinessLogic smd = new PR.BusinessLogic.SMDBusinessLogic(Session["UserId"] != null ? Convert.ToString(Session["UserId"]) : "");

                ValidatedAddress validatedAddress = smd.ValidateAddress(addressToValidate);
                string strReturnMessage = string.Empty;

                if (validatedAddress.IsAddressValid)
                {
                    //check if the zip code can be serviced.
                    strReturnMessage = ValidateNewZip(addressToValidate.Zip);

                    if (strReturnMessage == String.Empty)
                    {
                        response.success = true;
                        response.message = "Valid Address.";
                        response.data = validatedAddress;
                    }
                    else
                    {
                        response.success = false;
                        response.message = strReturnMessage;
                    }
                }
                else
                {
                    strReturnMessage = ValidateNewZip(addressToValidate.Zip);
                    string replaceWith = "";
                    string removedBreaks = validatedAddress.Error.Replace("\r\n", replaceWith).Replace("\n", replaceWith).Replace("\r", replaceWith);

                    response.success = false;
                    response.message = strReturnMessage + removedBreaks;
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error occurred, please try again.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateFullAddressFromMap(int QORID, Address startAddress, Address toAddress, string touchType, int sequenceNum, string isOrigin, string Instructions)
        {
            var response = new AjaxResponse<string>();
            try
            {
                PR.BusinessLogic.SMDBusinessLogic smd = new PR.BusinessLogic.SMDBusinessLogic(Helper.CurrentUser.UserName);

                Address StartAddress = startAddress.ID == 0 ? null : startAddress;
                Address ToAddress = toAddress.ID == 0 ? null : toAddress;
                string enumTouchType = string.Empty;

                switch (touchType)
                {
                    case "DE":
                        enumTouchType = eQTType.WarehouseToCurbEmpty.ToString();
                        break;
                    case "DF":
                        enumTouchType = eQTType.WarehouseToCurbFull.ToString();
                        break;
                    case "CC":
                        enumTouchType = eQTType.CurbToCurb.ToString();
                        break;
                    case "RE":
                        enumTouchType = eQTType.ReturnToWarehouseEmpty.ToString();
                        break;
                    case "RF":
                        enumTouchType = eQTType.ReturnToWarehouseEmpty.ToString();
                        break;
                    case "WT":
                        enumTouchType = eQTType.ReturnToWarehouseEmpty.ToString();
                        break;
                    case "CF":
                        enumTouchType = eQTType.ReturnToWarehouseEmpty.ToString();
                        break;
                    case "WA":
                        enumTouchType = eQTType.WarehouseAccess.ToString();
                        break;
                }

                smd.UpdateTouchData(QORID, StartAddress, ToAddress, Instructions, (eQTType)Enum.Parse(typeof(eQTType), enumTouchType), sequenceNum, false, Helper.CurrentUser.UserName);

                PR.ExternalInterfaces.PRAddressAPI prApi = new PR.ExternalInterfaces.PRAddressAPI();
                var info = prApi.GetLatitudeLongitude(startAddress.ID == 0 ? toAddress : startAddress);

                AddressUpdateList address = new AddressUpdateList();
                address.ID = startAddress.ID == 0 ? toAddress.ID : startAddress.ID;
                address.Lat = info.Latitude;
                address.Long = info.Longitude;

                if (address != null)
                {

                    if (address.ID != 0 && !string.IsNullOrEmpty(address.Lat) && !string.IsNullOrEmpty(address.Long))
                    {
                        Address oaddress = new Address();
                        oaddress.ID = address.ID;
                        oaddress.Latitude = address.Lat;
                        oaddress.Longitude = address.Long;

                        //Update lat and Long values into DB
                        _touchService.UpdateAddressByID(oaddress);

                        //Update lat and long values into session
                        var LoadList = GetDriverLoadsFromSession();
                        LoadList.Where(l => l.Touches != null && l.Touches.Count > 0).ToList().ForEach(x => UpdateAddressInTouchList(x.Touches, address.Lat, address.Long, address.AddressType, address.OrderId, address.ID));
                        Session[SessionKeyForDirverLoads] = LoadList;
                    }
                    else if (!string.IsNullOrEmpty(address.Lat) && !string.IsNullOrEmpty(address.Long))
                    {
                        var touchList = GetUnAssignedTouchesFromSession(DateTime.Today);
                        UpdateAddressInTouchList(touchList, address.Lat, address.Long, address.AddressType, address.OrderId, 0);
                        Session[SessionKeyForUnassignedTouches(DateTime.Today)] = touchList;
                    }
                }

                //removing cached addresses after successfully updating the lat and lang values in database.
                _touchService.RemoveCachedAddress(startAddress.ID == 0 ? toAddress : startAddress);

                response.success = true;
                response.message = "success";
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = "Error";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateAddressFromMap(AddressUpdateList address, string Instructions, int QORID, string touchType, int sequenceNo)
        {
            var response = new AjaxResponse<string>();
            try
            {
                if (address != null)
                {
                    if (address.ID != 0 && !string.IsNullOrEmpty(address.Lat) && !string.IsNullOrEmpty(address.Long))
                    {
                        Address oaddress = new Address();
                        oaddress.ID = address.ID;
                        oaddress.Latitude = address.Lat;
                        oaddress.Longitude = address.Long;

                        oaddress.IsIntersectingLatLong = true;
                        _touchService.UpdateLatLongForIntesection(oaddress);

                        //Update lat and long values into session
                        var LoadList = GetDriverLoadsFromSession();
                        LoadList.Where(l => l.Touches != null && l.Touches.Count > 0).ToList().ForEach(x => UpdateAddressInTouchList(x.Touches, address.Lat, address.Long, address.AddressType, address.OrderId, address.ID));
                        Session[SessionKeyForDirverLoads] = LoadList;
                        //return Content("success");
                        response.success = true;
                        response.message = "success";
                    }
                    else if (!string.IsNullOrEmpty(address.Lat) && !string.IsNullOrEmpty(address.Long))
                    {
                        var touchList = GetUnAssignedTouchesFromSession(DateTime.Today);
                        UpdateAddressInTouchList(touchList, address.Lat, address.Long, address.AddressType, address.OrderId, 0);
                        Session[SessionKeyForUnassignedTouches(DateTime.Today)] = touchList;
                        //return Content("success");
                        response.success = true;
                        response.message = "success";
                    }
                    //Write instructions save funcationality
                    PR.BusinessLogic.SMDBusinessLogic smd = new PR.BusinessLogic.SMDBusinessLogic();
                    string enumTouchType = string.Empty;

                    switch (touchType)
                    {
                        case "DE":
                            enumTouchType = eQTType.WarehouseToCurbEmpty.ToString();
                            break;
                        case "DF":
                            enumTouchType = eQTType.WarehouseToCurbFull.ToString();
                            break;
                        case "CC":
                            enumTouchType = eQTType.CurbToCurb.ToString();
                            break;
                        case "RE":
                            enumTouchType = eQTType.ReturnToWarehouseEmpty.ToString();
                            break;
                        case "RF":
                            enumTouchType = eQTType.ReturnToWarehouseEmpty.ToString();
                            break;
                        case "WT":
                            enumTouchType = eQTType.ReturnToWarehouseEmpty.ToString();
                            break;
                        case "CF":
                            enumTouchType = eQTType.ReturnToWarehouseEmpty.ToString();
                            break;
                        case "WA":
                            enumTouchType = eQTType.WarehouseAccess.ToString();
                            break;
                    }
                    smd.UpdateTouchData(QORID, null, null, Instructions, (eQTType)Enum.Parse(typeof(eQTType), enumTouchType), sequenceNo, false, Helper.CurrentUser.UserName);

                    //removing cached addresses after successfully updating the lat and lang values in database.
                    _touchService.RemoveCachedAddress(new Address() { ID = address.ID });
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private string ValidateNewZip(string zip)
        {
            var local = new PR.BusinessLogic.LocalComponent();
            string returnMessage = String.Empty;
            if (zip.Length > 5)
            {
                zip = zip.Substring(0, 5);
            }
            var zipValidationInfo = local.ValidateZipCodes(zip, String.Empty);
            if (zipValidationInfo.Zip1.LocationCode == CurrentSiteInfo.LocationCode.ToString())
            {
                if (zipValidationInfo.Zip1.IsLDMServiced)
                {
                    returnMessage = "Entered zip code cannot be serviced for this move.";
                }
            }
            else
            {
                if (!(local.IsZipCodeESATServiced(zip, CurrentSiteInfo.LocationCode.ToString())))
                {
                    if (zipValidationInfo.Zip1.LocationCode == String.Empty)
                    {
                        returnMessage = "The zip code is not serviceable by any facility.";
                    }
                }
            }
            return returnMessage;
        }
        #endregion

        #region Private methods

        private void SetMarker(Touch touch, string facilityId, ref List<Marker> markers)
        {
            Marker marker = new Marker();


            marker.loadId = Convert.ToInt32(touch.LoadId);
            marker.driverId = Convert.ToInt32(touch.DriverId);
            marker.trailerId = touch.TrailerId;
            marker.truckId = touch.TruckId != null ? Convert.ToInt32(touch.TruckId) : 0;
            marker.driverName = touch.DriverName == null ? "No Driver assigned" : touch.DriverName;

            marker.CustomerName = touch.CustomerName;
            marker.FromAddress = touch.OriginAddress.AddressLine1 + ", " + touch.OriginAddress.AddressLine2 + (string.IsNullOrEmpty(touch.OriginAddress.AddressLine2) ? "" : ", ") + touch.OriginAddress.City + ", " + touch.OriginAddress.Zip;
            marker.ToAddress = touch.DestAddress.AddressLine1 + ", " + touch.DestAddress.AddressLine2 + (string.IsNullOrEmpty(touch.DestAddress.AddressLine2) ? "" : ", ") + touch.DestAddress.City + ", " + touch.DestAddress.Zip;
            marker.orderNumber = touch.OrderNumber;
            marker.touchId = touch.TouchId;
            marker.TouchType = touch.TouchTypeAcronym;
            marker.StartTime = ConvertStartTimeToAMPM(touch.StartTime);
            marker.DoorPOS = (touch.DoorToFront && touch.DoorToRear ? "Door To Front, Door To Rear" : (touch.DoorToRear ? "Door To Rear" : (touch.DoorToFront ? "Door To Front" : "")));
            marker.TouchInstructions = touch.Instructions;
            marker.UnitNo = touch.UnitNumber;
            marker.WeightStnReqDisp = Convert.ToBoolean(touch.WeightStnReq) ? "Yes" : "No";
            marker.UnitSize = touch.Unitsize;
            marker.TouchNumberDisp = touch.TouchNumberDisp;
            marker.SiteLinkMileage = Math.Round(touch.SiteLinkMileage, 1);
            marker.StartTimeHHMM = (Convert.ToBoolean(touch.IsLocked) == true ? touch.ScheduledDate.ToShortTimeString() : "");
            marker.IsLoadLocked = Convert.ToBoolean(touch.IsLocked);
            marker.GlobalSiteNum = touch.GlobalSiteNum;
            marker.SiteName = (CurrentSiteInfo.GlobalSiteNum == touch.GlobalSiteNum ? CurrentSiteInfo : CurrentSiteInfo.MarketFacilities.Where(m => m.GlobalSiteNum == touch.GlobalSiteNum).FirstOrDefault()).SiteAddress.City;
            marker.ProvidedETASettings = touch.ProvidedETASettings;

            //MapIcon URL changing based on ZippyShell move or not
            string strMapIconURL = "~/Images/LocalLogistics/" + (touch.IsZippyShellQuote ? "ZippyIcons/" : string.Empty);

            switch (touch.TouchTypeAcronym)
            {
                case "DE":
                case "DF":
                    marker.Latitude = touch.DestAddress.Latitude;
                    marker.Longitude = touch.DestAddress.Longitude;

                    marker.ImageDefault = marker.driverId > 0 ? Url.Content("~/Images/LocalLogistics/truck.png") : Url.Content(strMapIconURL + GenerateImageName(touch, false, facilityId));
                    marker.ImageSelected = Url.Content(strMapIconURL + GenerateImageName(touch, true, facilityId));
                    break;

                case "CC":
                    marker.Latitude = touch.DestAddress.Latitude;
                    marker.Longitude = touch.DestAddress.Longitude;
                    marker.LatitudeCCOrg = touch.OriginAddress.Latitude;
                    marker.LongitudeCCOrg = touch.OriginAddress.Longitude;

                    marker.ImageDefault = marker.driverId > 0 ? Url.Content("~/Images/LocalLogistics/truck.png") : Url.Content(strMapIconURL + GenerateImageName(touch, false, facilityId));
                    marker.ImageSelected = Url.Content(strMapIconURL + GenerateImageName(touch, true, facilityId));
                    break;

                case "RE":
                    marker.Latitude = touch.OriginAddress.Latitude;
                    marker.Longitude = touch.OriginAddress.Longitude;
                    marker.ImageDefault = marker.driverId > 0 ? Url.Content("~/Images/LocalLogistics/truck.png") : Url.Content(strMapIconURL + GenerateImageName(touch, false, facilityId));
                    marker.ImageSelected = Url.Content(strMapIconURL + GenerateImageName(touch, true, facilityId));
                    break;
                case "RF":
                    marker.Latitude = touch.OriginAddress.Latitude;
                    marker.Longitude = touch.OriginAddress.Longitude;
                    marker.ImageDefault = marker.driverId > 0 ? Url.Content("~/Images/LocalLogistics/truck.png") : Url.Content(strMapIconURL + GenerateImageName(touch, false, facilityId));
                    marker.ImageSelected = Url.Content(strMapIconURL + GenerateImageName(touch, true, facilityId));
                    break;
            }
            markers.Add(marker);
        }

        private string ConvertStartTimeToAMPM(string StartTime)
        {
            if (StartTime.ToUpper() != "ANYTIME" && StartTime.ToUpper() != "AM" && StartTime.ToUpper() != "PM")
            {
                DateTime dateTime = DateTime.ParseExact(StartTime, "HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                return dateTime.ToString("tt", System.Globalization.CultureInfo.CurrentCulture);
            }
            else
            {
                return StartTime;
            }
        }

        private string GenerateImageName(Touch touch, bool star, string facilityId)
        {
            string time = string.Empty;
            if (touch.StartTime.ToUpper() != "ANYTIME")
            {
                time = ConvertStartTimeToAMPM(touch.StartTime);
            }
            else
            {
                time = touch.StartTime.ToUpper() == "ANYTIME" ? "Any" : touch.StartTime;
            }

            if (!star)
                return facilityId + touch.TouchTypeAcronym + time + ".png";
            else
                return facilityId + touch.TouchTypeAcronym + time + "Str.png";
        }

        private TouchStopHandlingTime FetchTouchStopHandlingTimes()
        {
            return _touchService.FetchTouchStopHandlingTimes(CurrentSiteInfo.GlobalSiteNum);
        }

        private List<DriverTouch> GetDriverTouches(string driverNumber, DateTime touchDate)
        {
            List<DriverTouch> lDriverTouch = new List<DriverTouch>();
            try
            {

            }

            catch (Exception ex)
            {
                Helper.LogError(ex);
                if (ex.Message.Contains("Transite Error Occurred - No touches found"))
                {
                    return lDriverTouch;
                }
                else
                {
                    throw ex;
                }
            }
            return lDriverTouch;
        }
        #endregion

        #region Session Realted Methods

        private List<Touch> SessionAllTransportationTouches
        {
            get
            {
                return (List<Touch>)Session["TransportationTouches"];
            }
            set
            {
                Session["TransportationTouches"] = value;
            }
        }

        private string SessionKeyForUnassignedTouches(DateTime date)
        {
            return "UnassignedTouches";
        }

        private readonly string SessionKeyForDirverLoads = "DriverTouches";

        private string SessionKeyForAllTouches
        {
            get { return "AllTouches"; }
        }

        private List<Load> GetDriverLoadsFromSession()
        {
            object driverTouches = Session[SessionKeyForDirverLoads];
            if (driverTouches != null)
            {
                return (List<Load>)driverTouches;
            }
            return new List<Load>();
        }

        private List<Touch> GetUnAssignedTouchesFromSession(DateTime date)
        {
            object unassignedTouches = Session[SessionKeyForUnassignedTouches(date)];

            if (unassignedTouches != null)
            {
                return (List<Touch>)(unassignedTouches);
            }

            return new List<Touch>();
        }

        private Touch GetUnAssignedTouchesFromSession(DateTime date, string orderNumber)
        {
            return GetUnAssignedTouchesFromSession(date).Where(t => t.OrderNumber == orderNumber).FirstOrDefault();
        }

        private void RemoveUnAssignedTouchFromSession(DateTime date, string orderNumber)
        {
            Session[SessionKeyForUnassignedTouches(date)] = GetUnAssignedTouchesFromSession(date).Where(t => t.OrderNumber != orderNumber);
        }

        private void AddRemovedTouchToUnAssignedTouchSession(Touch removedTouchFromLoad, DateTime date)
        {
            var unAssignedTouches = (List<Touch>)Session[SessionKeyForUnassignedTouches(date)];

            ///Before add any touch to session, make sure same touch should not exists in the session
            unAssignedTouches.RemoveAll(t => t.OrderNumber == removedTouchFromLoad.OrderNumber);

            unAssignedTouches.Add(removedTouchFromLoad);

            //Update Salesforce touch for Touch Assignment. TG-729
            _touchService.AddRemoveTouchtoDriverESB(DateTime.MinValue, removedTouchFromLoad, 0, Helper.CurrentUser.UserName);

            Session[SessionKeyForUnassignedTouches(date)] = unAssignedTouches;
        }

        private Touch GetTouchFromSession(string orderNumber)
        {
            return AllTouchesSession.Where(t => t.OrderNumber == orderNumber).FirstOrDefault();
        }

        #endregion

        #region Properties

        public new SiteInfo CurrentSiteInfo
        {
            get
            {
                List<SiteInfo> rtVal = Helper.CurrentUser.Facilities;// (List<SiteInfo>)Session["SiteInfo"];
                string FaclocationCode = Convert.ToString(Session["FaclocationCode"]);

                SiteInfo currentSiteInfo = rtVal.Where(x => x.LocationCode == FaclocationCode).First();

                if (currentSiteInfo.MarketId != null && currentSiteInfo.MarketFacilities.Count == 0)
                {
                    _userService.AddMarketFacilities(currentSiteInfo);
                }

                return currentSiteInfo;
            }
        }

        public List<Touch> AllTouchesSession
        {
            get
            {
                object allTouches = Session[SessionKeyForAllTouches];

                if (allTouches != null)
                {
                    return (List<Touch>)(allTouches);
                }

                return new List<Touch>();
            }
            set { Session[SessionKeyForAllTouches] = value; }
        }

        public string WareHouseCode
        {
            get { return Convert.ToString(this.Session["WareHouseCode"]); }
        }

        #endregion
    }

    public class TouchIdAndTime
    {
        public int touchID { get; set; }
        public DateTime touchDateTime { get; set; }
    }

    public class AddressUpdateList
    {
        public string OrderId { get; set; }
        public int ID { get; set; }
        public string AddressType { get; set; }
        //[RegularExpression(@"^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}", ErrorMessage = "Enter Latitude")]
        public string Lat { get; set; }
        //[RegularExpression(@"^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}", ErrorMessage = "Enter Longitude")]
        public string Long { get; set; }
    }

    public class StopIdAndTime
    {
        public decimal? actualDistance { get; set; }
        public int? orgAddressId { get; set; }
        public int? destAddressId { get; set; }
        public int sequence { get; set; }
        public bool isOpenStop { get; set; }
        public int stopID { get; set; }
        public DateTime stopStartDateTime { get; set; }
        public DateTime stopEndDateTime { get; set; }
    }

    public class FacilityLatLong
    {
        public string Latitude { set; get; }
        public string Longitude { get; set; }
    }
}
