﻿namespace PRFacAppWF.Controllers
{
    using CodeHelpers;
    using Entities;
    using Entities.Touches;
    using models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    public class WarehouseTouchContainerStatusController : BaseController
    {
        private readonly WarehouseTouches oWarehouseTouches;

        public WarehouseTouchContainerStatusController()
        {
            if (oWarehouseTouches == null)
                oWarehouseTouches = new WarehouseTouches();
        }

        // GET: WarehouseTouchContainerStatus
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SearchContainerStatus(ContainerStatusFilter csSearchFilter)
        {
            csSearchFilter.FacilityId = Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum);

            return new JsonResult { Data = GetContainerStatusDetailLst(csSearchFilter), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        private JsonResult GetContainerStatusDetailLst(ContainerStatusFilter csSearchFilter)
        {
            var res = oWarehouseTouches.SearchContainerStatus(Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum), csSearchFilter.StatusName);
            SearchResults<FAWTTouchActions> rtObj = new SearchResults<FAWTTouchActions>();
            rtObj.rows = res.ToList().OrderBy(r => r.Status).ThenBy(r => r.ActionName).ToList();
            rtObj.page = 1;

            int records = (rtObj.rows.Count() > 0 ? rtObj.rows.Count() : 0);
            float totPages = (records > 0 ? ((float)records / rtObj.rows.Count()) : 1);
            int totpages = (int)Math.Ceiling(totPages);
            rtObj.records = records;
            rtObj.total = totpages;
            rtObj.error = "";

            return Json(rtObj);
        }

        public JsonResult ToggleActiveAndInactiveStatus(int PKID, string Status, int IsGlobalStatus)
        {
            var response = new AjaxResponse<object>();
            try
            {
                oWarehouseTouches.ToggleActiveAndInactiveContainerStatus(PKID, Status, CurrentUser.ID, IsGlobalStatus, Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum));
                response.success = true;
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddEditContainerStatus(FAWTTouchActions oWATouchContainerStatus)
        {
            var response = new AjaxResponse<object>();
            try
            {
                oWATouchContainerStatus.StoreNumber = Convert.ToInt32(CurrentSiteInfo.GlobalSiteNum);

                oWarehouseTouches.AddContainerStatus(oWATouchContainerStatus, CurrentUser.ID);
                response.success = true;
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                response.success = false;
                response.message = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}