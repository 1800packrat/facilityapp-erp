﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using PRFacAppWF.CodeHelpers;
using PRFacAppWF.models;
using PRFacAppWF.Repository.Services;
using System.Linq;
using PR.LocalLogisticsSolution.Interfaces;
using PRFacAppWF.Repository.Services.Implementations;
using PRFacAppWF.Repository.models;
using PRFacAppWF.Entities;
using PRFacAppWF.Entities.UserManagement;

namespace PRFacAppWF.Controllers
{
    public class UserAssignmentController : BaseController
    {
        private IUserService _userService = null;
        private IUserSecurity _userSecurity = null;

        public UserAssignmentController(IUserService userService)
        {
            if (this._userSecurity == null)
                this._userSecurity = new UserSecurityService();

            _userService = userService;
        }

        public ActionResult Index()
        {
            var modelRoles = _userSecurity.ListRoles();

            return View(modelRoles);
        }

        public JsonResult listUsers(string sidx, string sord, int page, int rows, string userName, int RoleNo = 0, bool isActiveOnly = true)
        {
            JQGridResultSet jsonData = new JQGridResultSet();
            try
            {
                List<FAUserMaster> lcllistUser = new List<FAUserMaster>();

                lcllistUser = _userSecurity.GetAllUserDetails(userName, RoleNo, isActiveOnly);

                int pageIndex = Convert.ToInt32(page) - 1;
                int pageSize = rows;
                int totalRecords = lcllistUser.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

                lcllistUser = lcllistUser.OrderBy(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(pageIndex * pageSize).Take(pageSize).ToList();

                if (sord == "desc")
                {
                    lcllistUser = lcllistUser.OrderByDescending(a => a.GetType().GetProperty(sidx).GetValue(a, null)).Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                jsonData.total = totalPages;
                jsonData.page = page;
                jsonData.records = totalRecords;

                foreach (var lst in lcllistUser)
                {
                    JQGridResultSet.Row row = new JQGridResultSet.Row();
                    row.id = lst.PK_UserId;
                    row.cell = new string[]
                                {
                                    lst.PK_UserId.ToString(),
                                    lst.Username,
                                    lst.FirstName,
                                    lst.LastName,
                                    lst.EmailAddress,
                                    lst.FK_GroupID.ToString(),
                                    lst.FAGroupMaster.GroupName,
                                    lst.IsActive == true ? "InActive" : "Active",
                                    lst.UniqueID.ToString()
                                }.ToList();
                    jsonData.rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddEditUser(User userDetails, FacilityUserRelation[] selectedFacilities)
        {
            var response = new AjaxResponse<string>();

            try
            {
                if (userDetails.UserId > 0 && !userDetails.IsActive && _userService.CheckLoadExistForGivenDateRange(userDetails.UniqueID, DateTime.Now, DateTime.MaxValue))
                {
                    List<DateTime> nonSchedulableDates = _userService.GetDriverLoadDates(userDetails.UniqueID, DateTime.Now);

                    if (nonSchedulableDates.Count > 0)
                    {
                        string strNonScheduleDates = String.Join(", ", nonSchedulableDates.Select(s => s.ToShortDateString()));
                        response.message = "This driver is currently routed on " + strNonScheduleDates + " in Local Dispatch.  Please remove the touches in Local Dispatch before modifying the schedule.";
                    }

                    response.success = false;
                }
                else
                {
                    if (!_userSecurity.CheckUserName(userDetails.UserName, userDetails.UserId))
                    {
                        userDetails.EncryptPassword = PR.UtilityLibrary.CommonUtility.Encrypt(userDetails.Password.Trim(), Helper.EnDeKey);
                        _userSecurity.SaveUserDetails(userDetails, selectedFacilities, CurrentUser.ID);
                        response.success = true;
                    }
                    else
                    {
                        response.message = "UserExists";
                        response.success = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);

                response.message = "Error occurred while loading information. Please reload the page and try again!";
                response.success = false;
            }            

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetUserStatus(int UserId, int UniqueId, string status)
        {
            var response = new AjaxResponse<string>();

            try
            {
                if (!(status.Equals(Entities.Enums.UserStatus.Active)) && _userService.CheckLoadExistForGivenDateRange(UniqueId, DateTime.Now, DateTime.MaxValue))
                {
                    List<DateTime> nonSchedulableDates = _userService.GetDriverLoadDates(UniqueId, DateTime.Now);

                    if (nonSchedulableDates.Count > 0)
                    {
                        string strNonScheduleDates = String.Join(", ", nonSchedulableDates.Select(s => s.ToShortDateString()));
                        response.message = "This driver is currently routed on " + strNonScheduleDates + " in Local Dispatch.  Please remove the touches in Local Dispatch before modifying the schedule.";
                    }

                    response.success = false;
                }
                else
                {
                    _userSecurity.ToggleUserStatus(UserId, status, CurrentUser.ID);
                    response.success = true;
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);

                response.message = "Error occurred while loading information. Please reload the page and try again!";
                response.success = false;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddUser()
        {
            AddEditUserViewModel model = new AddEditUserViewModel();
            try
            {
                model.FAUserMaster = new FADTOUserMaster();
                model.Roles = _userSecurity.ListRoles();
                model.Facilities = _userSecurity.GetDTOFacilityList();
                model.FAUserMaster.LicenseClassList = _userSecurity.GetLicenseClass();
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return View(model);
        }

        public ActionResult EditUser(int UserId)
        {
            AddEditUserViewModel model = new AddEditUserViewModel();
            try
            {
                if (UserId > 0)
                {
                    model.FAUserMaster = _userSecurity.GetUserInfoById(UserId);
                    model.Roles = _userSecurity.ListRoles();
                    model.Facilities = _userSecurity.GetDTOFacilityList();
                    model.FAUserMaster.LicenseClassList = _userSecurity.GetLicenseClass();
                }
                else
                {
                    return RedirectToAction("AddUser");
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }

            return View(model);
        }
    }
}