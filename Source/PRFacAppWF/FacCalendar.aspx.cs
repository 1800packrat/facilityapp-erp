﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using PRFacAppWF.CodeHelpers;
using PR.BusinessLogic;
using PR.Entities;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.Web.UI;

namespace PRFacAppWF
{
    public partial class FacCalendar : System.Web.UI.Page
    {
        private DataSet _AvailableDates;
        private string _DateColumnName;
        private DateTime _DefaultSelectedDate = DateTime.Now;
        private DateTime deliveryDate;
        private SMDBusinessLogic smdObject;
        private ScheduleCalendar sceduleCalendar;
        protected const String ViewBookedMiles = "View Booked Miles";
        protected const String ViewFacilityMiles = "View Facility Miles";
        protected const String ViewReservedMiles = "View Reserved Miles";
        public DateTime DefaultSelectedDate
        {
            get { return _DefaultSelectedDate; }
            set { _DefaultSelectedDate = value; }
        }

        public DataSet AvailableDates
        {
            get { return _AvailableDates; }
            set { _AvailableDates = value; }
        }

        public string DateColumnName
        {
            get { return _DateColumnName; }
            set { _DateColumnName = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                smdObject = new SMDBusinessLogic(Session["UserId"] != null ? Convert.ToString(Session["UserId"]) : "");
                deliveryDate = smdObject.GetPreferredDeliveryDate(Convert.ToInt32(Session["qorid"]));
                calScheduleAnyTime.VisibleDate = _DefaultSelectedDate;
                calScheduleAM.VisibleDate = _DefaultSelectedDate;
                calSchedulePM.VisibleDate = _DefaultSelectedDate;
                BindMonths(); BindDDLYears();

                LoadScheduleCalendar(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
            }
        }

        private void BindMonths()
        {
            DateTime month = Convert.ToDateTime("1/1/2000");
            for (int i = 0; i < 12; i++)
            {

                DateTime NextMont = month.AddMonths(i);
                ListItem list = new ListItem();
                list.Text = NextMont.ToString("MMMM");
                list.Value = NextMont.Month.ToString();
                ddlMonth.Items.Add(list);
            }

            ddlMonth.Items.FindByValue(calScheduleAnyTime.VisibleDate.Month.ToString()).Selected = true;

        }

        public void BindDDLYears()
        {
            ListItem item = null;
            int currentYear;// = DateTime.Now.Year-3;
            for (int i = 0; i <= 15; i++)
            {
                currentYear = DateTime.Now.AddYears((i - 3)).Year;
                item = new ListItem(currentYear.ToString(), currentYear.ToString());

                ddlYear.Items.Add(item);
            }

            ddlYear.SelectedValue = calScheduleAnyTime.VisibleDate.Year.ToString();
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            var newDateTime = new DateTime(Convert.ToInt32(ddlYear.SelectedValue),
                                                              Convert.ToInt32(ddlMonth.SelectedValue),
                                                              1);
            LoadScheduleCalendar(newDateTime);
            calScheduleAnyTime.VisibleDate = newDateTime;
            calScheduleAM.VisibleDate = new DateTime(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth.SelectedValue), 1);
            calSchedulePM.VisibleDate = new DateTime(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth.SelectedValue), 1);


        }

        private void LoadScheduleCalendar(DateTime? startDate)
        {
            DateTime date;
            double noDays = 0, daysinMonth = 0;
            if (Request.QueryString["type"] == "DE-Deliver Empty" && startDate.Value.Month == DateTime.Now.Month)
            {
                date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime firstDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                daysinMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
                DateTime lastDay = firstDay.AddDays(daysinMonth);
                noDays = (lastDay - date).TotalDays;
            }
            else
            {
                date = startDate.Value;
                DateTime firstDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                noDays = DateTime.DaysInMonth(startDate.Value.Year, startDate.Value.Month);

            }

            smdObject = new SMDBusinessLogic();
            if (Request.QueryString != null)
            {
                PRCapacity tbl = new PRCapacity(Session["UserId"] != null ? Convert.ToString(Session["UserId"]) : "");

                try
                {
                    //Added by Sohan, pull data from ESB endpoint now.
                    sceduleCalendar = tbl.GetCapacity(Helper.GetSession("FaclocationCode").ToString(), date, Convert.ToInt32(noDays), Convert.ToDecimal(Request.QueryString["dis"]), null, null);
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript", "alert(" + ex.Message + ");", true);
                }

                //commented by Sohan
                //sceduleCalendar = tbl.GetCapacity(Helper.Corpcode,
                //                                          Helper.GetSession("FaclocationCode").ToString(),
                //                                          Helper.username, Helper.password, date,
                //                                          Convert.ToInt32(noDays),
                //                                          Convert.ToDecimal(Request.QueryString["dis"]),
                //                                          null,
                //                                           null
                //                                          );

                Session["dsAvailCalendar"] = sceduleCalendar;

                AvailableDates = sceduleCalendar;
                DateColumnName = "Date";
                DefaultSelectedDate = startDate.Value;
            }

        }

        protected void calScheduleAnyTime_DayRender1(object sender, DayRenderEventArgs e)
        {
            //DateTime dtavailable;
            //bool flag = false;
            //string IsRed = "";
            var calVar = (Calendar)sender;
            string ScheduleType = "";
            if (calVar.ID == "calScheduleAnyTime")
            {
                ScheduleType = "AnyTime";
            }
            else if (calVar.ID == "calScheduleAM")
            {
                ScheduleType = "AM";
            }
            else if (calVar.ID == "calSchedulePM")
            {
                ScheduleType = "PM";
            }

            if (_AvailableDates != null)
            {
                if ((_AvailableDates.Tables[ScheduleType].Rows.Count > 0))
                {
                    var dr = _AvailableDates.Tables[ScheduleType].Rows.OfType<DataRow>().FirstOrDefault(d => Convert.ToDateTime(d[_DateColumnName].ToString()).ToShortDateString() == e.Day.Date.ToShortDateString());

                    if (dr != null)
                    {
                        if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Green))
                            e.Cell.CssClass = "cellgreen";
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Red))
                            e.Cell.CssClass = "cellred";
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Blue))
                            e.Cell.CssClass = "cellblue";
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Black))
                        {
                            e.Cell.CssClass = "cellblack";
                            e.Cell.ForeColor = Color.White;
                        }
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Orange))
                            e.Cell.CssClass = "cellorange";
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.Purple))
                            e.Cell.CssClass = "cellpurple";
                        else if (GetColorCodeById(Convert.ToInt16(dr["ColorCode"])) == Convert.ToString(PR.UtilityLibrary.PREnums.ColorCodes.White))
                        {
                            e.Cell.CssClass = "cellwhite";
                            e.Cell.ForeColor = Color.Black;
                        }

                        #region Bind hover panel

                        decimal roundTripMiles = 0;
                        decimal tripMiles = Convert.ToDecimal(dr["TripMiles"]);
                        decimal touchMiles = tripMiles - roundTripMiles;
                        e.Cell.Attributes.Add("data-date", Convert.ToDateTime(dr[_DateColumnName]).ToString("d MMM, ddd"));
                        e.Cell.Attributes.Add("data-regularmiles", Convert.ToString(dr["RegularMiles"]));
                        e.Cell.Attributes.Add("data-salesregularmiles", Convert.ToString(dr["SalesRegularMiles"]));
                        e.Cell.Attributes.Add("data-serviceregularmiles", Convert.ToString(dr["ServiceRegularMiles"]));
                        e.Cell.Attributes.Add("data-tripmiles", tripMiles.ToString());
                        e.Cell.Attributes.Add("data-touchmiles", touchMiles.ToString());
                        e.Cell.Attributes.Add("data-roundtripmiles", roundTripMiles.ToString());
                        if (CheckIfRoleIsAllowed(string.Empty, ViewFacilityMiles))
                            e.Cell.Attributes.Add("data-facilitymiles", Convert.ToString(dr["FacilityMiles"]));
                        if (CheckIfRoleIsAllowed(string.Empty, ViewBookedMiles))
                            e.Cell.Attributes.Add("data-bookedmiles", Convert.ToString(dr["BookedMiles"]));
                        if (CheckIfRoleIsAllowed(string.Empty, ViewReservedMiles))
                            if (ScheduleType.ToUpper() == "ANYTIME")
                                e.Cell.Attributes.Add("data-reservedmiles", Convert.ToString(dr["ReservedMiles"]));
                        #endregion
                    }
                }
            }
        }

        //private string BuildToolTip(DataRow dr)
        //{
        //    return "TouchesScheduled: " + dr["TouchesScheduled"].ToString() + Environment.NewLine +
        //                       " MileagesBooked: " + Convert.ToDecimal(dr["MileagesBooked"]).ToString("0.00") + Environment.NewLine +
        //                       " TouchLimit: " + dr["TouchLimit"].ToString() + Environment.NewLine +
        //                       " MileageLimit: " + dr["MileageLimit"].ToString() + Environment.NewLine;
        //}



        private bool CheckDeliveryDateExpirationRange(DateTime date)
        {
            if ((date > deliveryDate.AddDays(-4)) && (date < deliveryDate.AddDays(4)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void BindCalender()
        {
            calScheduleAnyTime.DataBind();
        }



        protected string GetMonthName(int Month)
        {
            string MonthName = "";
            switch (Month)
            {
                case 1:
                    MonthName = "JANUARY";
                    break;
                case 2:
                    MonthName = "FEBRUARY";
                    break;
                case 3:
                    MonthName = "MARCH";
                    break;
                case 4:
                    MonthName = "APRIL";
                    break;
                case 5:
                    MonthName = "MAY";
                    break;
                case 6:
                    MonthName = "JUNE";
                    break;
                case 7:
                    MonthName = "JULY";
                    break;
                case 8:
                    MonthName = "AUGUST";
                    break;
                case 9:
                    MonthName = "SEPTEMBER";
                    break;
                case 10:
                    MonthName = "OCTOBER";
                    break;
                case 11:
                    MonthName = "NOVEMBER";
                    break;
                case 12:
                    MonthName = "DECEMBER";
                    break;
            }

            return MonthName;
        }

        protected void calScheduleAnyTime_SelectionChanged(object sender, EventArgs e)
        {
            Calendar ctrl = (Calendar)sender;
            //Response.Redirect("~/watouches.aspx?sDate=" + ctrl.SelectedDate);
            Response.Redirect("~/TouchList/Index?sDate= " + ctrl.SelectedDate);
        }
        string GetColorCodeById(int codeId)
        {
            var color = string.Empty;
            if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Black) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Black.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Blue) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Blue.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Green) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Green.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Orange) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Orange.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Purple) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Purple.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.Red) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.Red.ToString();
            else if (Convert.ToInt16(PR.UtilityLibrary.PREnums.ColorCodes.White) == codeId)
                color = PR.UtilityLibrary.PREnums.ColorCodes.White.ToString();
            return color;
        }
        bool CheckIfRoleIsAllowed(string color, string action)
        {
            DataSet dsColorActionRole;
            if (Session["ColorActionRole"] != null)
                dsColorActionRole = Session["ColorActionRole"] as DataSet;
            else
            {
                int roleLevel = Convert.ToInt16(ConfigurationManager.AppSettings["CalendarAccessRoleLevel"]);
                dsColorActionRole = (new PRCapacity()).GetCapacityColorActionsByRole(roleLevel);
                Session["ColorActionRole"] = dsColorActionRole;
            }
            return dsColorActionRole.Tables[0].Rows.OfType<DataRow>().Any(c => c["Color"].ToString().ToLower().Trim() == color.ToLower().Trim()
                && c["Action"].ToString().ToLower().Trim() == action.ToLower().Trim());
        }

    }
}