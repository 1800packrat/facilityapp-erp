﻿namespace PRFacAppWF.Filters
{
    using PR.LocalLogisticsSolution.Model;
    using PRFacAppWF.Repository.Services;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Entities.Enums;
    using CodeHelpers;

    public class AuthorizationFilter
    {   
        public bool CanExecute(string moduleName)
        {
            ///Here we get all accessable pages for logged in user 
            ///and will check requested page accessable for this user or not. this comparison should be ignore case
            return true;// GetRoleGroupPermissions.Any(g => g.Equals(moduleName, StringComparison.OrdinalIgnoreCase));
        }

        public bool CanExecute(int moduleId, Entities.Enums.AccessLevel accessType)
        {
            ///Here we get all accessable pages for logged in user 
            ///and will check requested page accessable for this user or not. this comparison should be ignore case
            return GetRoleGroupPermissions.Any(g => g.ModuleId == moduleId && g.AccessLevel == accessType);
        }

        //protected List<string> GetRoleGroupPermissions
        //{
        //    get
        //    {
        //        List<string> accessModules = new List<string>();
                
        //        if (HttpContext.Current.Session["CurrentUser"] != null)
        //        {
        //            string roleName = ((LogisticsUser)HttpContext.Current.Session["CurrentUser"]).GroupName;

        //            if (HttpContext.Current.Session["AccessModuleNames"] == null)
        //            {
        //                IPRSService oPRSService = new PRSService();
        //                HttpContext.Current.Session["AccessModuleNames"] = oPRSService.GetRoleGroupModuleAccess(roleName);
        //            }

        //            accessModules = (List<string>)HttpContext.Current.Session["AccessModuleNames"];
        //        }

        //        return accessModules;
        //    }
        //}

        public static List<Entities.ModuleAndAccessLevel> GetRoleGroupPermissions
        {
            get
            {
                List<Entities.ModuleAndAccessLevel> accessModules = new List<Entities.ModuleAndAccessLevel>();

                if(Helper.CurrentUser != null)
                {
                    if(HttpContext.Current.Session[typeof(Entities.ModuleAndAccessLevel).Name] == null)
                    {
                        IPRSService oPRSService = new PRSService();
                        HttpContext.Current.Session[typeof(Entities.ModuleAndAccessLevel).Name] = oPRSService.GetRoleGroupModuleAccess(Helper.CurrentUser.GroupName);
                    }

                    accessModules = (List<Entities.ModuleAndAccessLevel>)HttpContext.Current.Session[typeof(Entities.ModuleAndAccessLevel).Name];
                }

                return accessModules;
            }
        }
    }
}