﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="NonWatouches.aspx.cs" Inherits="PRFacAppWF.NonWatouches" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCenter" runat="server">
    <style>
        .li
        {
            color: white;
        }
    </style>
    <script type="text/javascript">
        function Print() {
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0,scrollbar=yes');
            var divdata = document.getElementById('divprint').innerHTML;
            WinPrint.document.write(divdata);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }


    </script>
    <asp:UpdatePanel runat="server" ID="upnl" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div class="form-panel">
                <!--header image starts-->
                <h2>
                    <span class="back-arrow"><a href="Dashboard.aspx">
                        <img src="Images/back.png" width="48" height="48" style="float: left; margin-right: 10px;" /></a></span>
                    <img src="Images/Driver.png" width="48" height="48" style="float: left; margin-right: 10px;" /><div
                        class="h-title">
                        <span style="float: left; font-size: 9pt; text-transform: uppercase;"> Non Warehouse</span><br />
                        <span style="float: left;">Touches</span>
                    </div>
                    <div class="nav-bar bg-color-none" style="float: right; width: auto; margin-top: 8px;">
                        <div class="nav-bar-inner new">
                            <ul class="menu">
                                <li>
                                    <%Page.Title.ToString();%>
                                </li>
                                <li data-role="dropdown">  <asp:HyperLink ID="hlWarehous" CssClass="bigfont" runat="server" NavigateUrl="~/Watouches.aspx" Text="View Warehouse Touches" />
                                    <ul class="dropdown-menu">
                                        <li>
                                      
                                           </li>
                                       
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                    <div class="clearfix">
                    </div>
                    <h2>
                    </h2>
                    <!--<div class="show-map"><button class="image-button bg-color-grayDark fg-color-white">Show route map<img src="Images/map2.png" class="bg-color-blue"></button></div>-->
                    <div class="clearfix">
                    </div>
                    <!--header image ends-->
                    <!--pivot item starts -->
                    <div class="metro-pivot touch-info-page">
                        <div class="pivot-item">
                           
                            <div class="clearfix">
                            </div>
                            <div class="inner-container">
                                <div>
                                    <div class="input-control text lft-all">
                                        <asp:TextBox ID="txtStartDt" runat="server" class="with-helper" placeholder="Start Date" />
                                        <ajaxToolkit:CalendarExtender ID="txtCal1" runat="server" TargetControlID="txtStartDt">
                                        </ajaxToolkit:CalendarExtender>
                                    </div>
                                    <div class="input-control text lft-all">
                                        <asp:TextBox ID="txtEndDt" runat="server" class="with-helper" placeholder="End Date"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtEndDt">
                                        </ajaxToolkit:CalendarExtender>
                                    </div>
                                    <asp:Button ID="btnView" runat="server" CssClass="bg-color-green" OnClick="btnView_Click"
                                        Text="View" />
                                </div>
                                <div class="clearfix">
                                </div>
                                <div style="text-align: right">
                                    <asp:ImageButton runat="server" Visible="false" ID="imagePrint" Height="20" Width="20"
                                        alt="Print" ImageUrl="Images/print.png" ToolTip="Print" OnClientClick="return Print();" />
                                </div>
                                <div class="grid">
             
                                </div>
                                <div id="divprint" style="display: none;">
                                    <asp:GridView ID="dtNWaTouches1" runat="server" AutoGenerateColumns="false" BorderWidth="1"
                                        CssClass="bordered" GridLines="None" OnRowCommand="dtNWaTouches1_RowCommand">
                                        <Columns>
                                       
                                            <asp:BoundField DataField="CustomerName" HeaderStyle-BorderWidth="1" ItemStyle-BorderWidth="1"
                                                HeaderText="CustomerName" />
                                            <asp:BoundField DataField="PhoneNumber" HeaderStyle-BorderWidth="1" ItemStyle-BorderWidth="1"
                                                HeaderText="PhoneNumber" />
                                            <asp:BoundField DataField="QORID" HeaderStyle-BorderWidth="1" ItemStyle-BorderWidth="1"
                                                HeaderText="QORID" />
                                            <asp:BoundField DataField="ScheduledDate" HeaderStyle-BorderWidth="1" ItemStyle-BorderWidth="1"
                                                DataFormatString="{0:MMM dd, yyyy}" HeaderText="Schedule date" />
                                           
                                            <asp:BoundField DataField="Status" HeaderStyle-BorderWidth="1" ItemStyle-BorderWidth="1"
                                                HeaderText="Status" />
                              
                                            <asp:BoundField DataField="TouchType" HeaderStyle-BorderWidth="1" ItemStyle-BorderWidth="1"
                                                HeaderText="TouchType" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="clerfix">
                        </div>
                    </div>
                    <div class="clerfix">
                    </div>
               
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
