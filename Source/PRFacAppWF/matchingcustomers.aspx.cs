﻿// SFERP-TODO-CTRMV-- Remove this complete page --TG-568

using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using PR.BusinessLogic;
using PRFacAppWF.CodeHelpers;

namespace PRFacAppWF
{
    public partial class matchingcustomers : BasePage
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    if (!IsPostBack)
        //    {
        //        try
        //        {
        //            BindDataToGrid();
        //        }
        //        catch (Exception ex)
        //        {
        //            Helper.LogError(ex);
        //            ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript", "alert('" + ex.Message + "');",
        //                                                true);
        //        }
        //    }
        //}

        //private void BindDataToGrid()
        //{
        //    var smdobject = new SMDBusinessLogic(Session["UserId"] != null ? Convert.ToString(Session["UserId"]) : "");
        //    SaleItems item = SaleItems.RetrieveFromSession();
        //    DataSet ds = smdobject.GetCustomer(Helper.Corpcode, Session["FaclocationCode"].ToString(), Helper.username, Helper.password, item.FirstName,
        //                                       item.LastName, item.Address, "", "", "", item.CustomerZip, "", item.Phone, item.CompanyName);
        //    Session["CustomerList"] = ds;
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        gvMatchingCustomers.DataSource = ds;
        //        gvMatchingCustomers.DataBind();
        //        gvMatchingCustomers.Visible = true;
        //        btnOk.Visible = true;
        //    }
        //    else
        //    {
        //        gvMatchingCustomers.Visible = false;
        //        lblmsg.Visible = true;
        //        lblmsg.Text = "<h1>No matching customers found</h1> ";
        //        btnOk.Visible = false;
        //    }
        //}

        //protected void btnOk_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        SaleItems item = SaleItems.RetrieveFromSession();
        //        item.SearchedCustLocation = hdLocationCode.Value;
        //        item.TennantId = Convert.ToInt32(hdTennantId.Value);
        //        item.LastName = hdLastName.Value;
        //        item.FirstName = hdFirstName.Value;
        //        item.Phone = hdPhone.Value;
        //        item.Email = hdEmail.Value;
        //        item.Address = hdAddress.Value;
        //        item.CustomerZip = hdZip.Value;
        //        item.CompanyName = hdcmp.Value;
        //        item.CommitToSession();
        //        Session["matchinginfoSaved"] = true;
        //        ScriptManager.RegisterStartupScript(this, GetType(), "popoupScript", PopulateParentControlsData(), true);

        //        // ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertScript", "this.window.close();", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        Helper.LogError(ex);
        //        ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript", "alert('" + ex.Message + "');", true);
        //    }
        //}

        //private string PopulateParentControlsData()
        //{
        //    SaleItems item = SaleItems.RetrieveFromSession();
        //    var builder = new StringBuilder();
        //    builder.Append(" window.opener.document.getElementById('ctl00_cphCenter_txtFirstName').value='" +
        //                   item.FirstName.Trim() + "';");
        //    builder.Append(" window.opener.document.getElementById('ctl00_cphCenter_txtLastName').value='" +
        //                   item.LastName.Trim() + "';");
        //    builder.Append(" window.opener.document.getElementById('ctl00_cphCenter_txtAddress').value='" +
        //                   item.Address.Trim() + "';");
        //    builder.Append(" window.opener.document.getElementById('ctl00_cphCenter_txtZip').value='" + item.CustomerZip.Trim() +
        //                   "';");
        //    builder.Append(" window.opener.document.getElementById('ctl00_cphCenter_txtEmail').value='" +
        //                   item.Email.Trim() + "';");
        //    builder.Append(" window.opener.document.getElementById('ctl00_cphCenter_txtPhone').value='" +
        //                   item.Phone.Trim() + "';");
        //    builder.Append(" window.opener.document.getElementById('ctl00_cphCenter_txtCompany').value='" +
        //                   item.CompanyName.Trim() + "';");
        //    builder.Append(" window.opener.document.getElementById('ctl00_cphCenter_hidMatchedCustomerflag').value='" +
        //                  "1" + "';");
        //    builder.Append("window.close();");
        //    return builder.ToString();
        //}

        //protected void gvMatchingCustomers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    gvMatchingCustomers.PageIndex = e.NewPageIndex;
        //    gvMatchingCustomers.DataSource = Session["CustomerList"] as DataSet;
        //    gvMatchingCustomers.DataBind();
        //}
    }
}