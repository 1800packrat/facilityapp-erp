﻿using System;
//using PRFacAppWF.ServiceReference1;
using PRFacAppWF.CodeHelpers;

namespace PRFacAppWF
{
    public partial class TouchTicket : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { 
                //ServiceReference1.eQTType touchtype = ServiceReference1.eQTType.WarehouseAccess;
                ////switch (Session["touchType"].ToString().Substring(0, 2))
                //string tt = Convert.ToString(Request.QueryString["touchType"]);
                //switch (tt)
                //{
                //    case "DE":

                //        touchtype = (ServiceReference1.eQTType)eQTType.WarehouseToCurbEmpty;
                //        break;
                //    case "DF":
                //        touchtype = (ServiceReference1.eQTType)eQTType.WarehouseToCurbFull;
                //        break;
                //    case "CC":

                //        touchtype = (ServiceReference1.eQTType)eQTType.CurbToCurb;
                //        break;
                //    case "RE":

                //        touchtype = (ServiceReference1.eQTType)eQTType.ReturnToWarehouseEmpty;
                //        break;
                //    case "RF":
                //        touchtype = (ServiceReference1.eQTType)eQTType.ReturnToWarehouseFull;
                //        break;
                //    //case "WT":
                //    //    //  touchtype = eQTType.w;
                //    //    break;
                //    //case "CF":
                //    //    touchtype = (ServiceReference1.eQTType)eQTType.;
                //    //   break;
                //    case "WA":

                //        touchtype = (ServiceReference1.eQTType)eQTType.WarehouseAccess;
                //        break;
                //    case "OBO":
                //        touchtype = (ServiceReference1.eQTType)eQTType.OutByOwner;
                //        break;
                //    case "IBO":
                //        touchtype = (ServiceReference1.eQTType)eQTType.InByOwner;
                //        break;
                //    default:
                //        throw new Exception("No touch type found!");
                //}

                int qorId = Convert.ToInt32(Request.QueryString["qorid"]);

                //ServiceReference1.PRTransiteServiceClient client = new ServiceReference1.PRTransiteServiceClient();
                //string en64PDF = client.GetTouchTicketPDF(qorId, touchtype);
                string en64PDF = "Will come form Salesforce";

                byte[] bytPDF = System.Convert.FromBase64String(en64PDF);

                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Disposition", "inline;filename=data.pdf");
                Response.BufferOutput = true;

                Response.AddHeader("Content-Length", bytPDF.Length.ToString());
                Response.BinaryWrite(bytPDF);
                Response.End();
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
            }
        }


    }
}