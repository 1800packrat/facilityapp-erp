﻿<%@ Page Title="Corporate Controls" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CorporateControls.aspx.cs" Inherits="PRFacAppWF.CorporateControls" %>

<asp:Content ID="CorporateReportsHead" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
    </style>
</asp:Content>
<asp:Content ID="CorporateReportsBody" ContentPlaceHolderID="cphCenter" runat="server">
    <div class="margin-left-1prct">
        <h1>Corporate Controls</h1>
        <asp:Panel ID="pnlDashBoard" runat="server" Style="width: 100%">
            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.UnitUnavailability, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile  bg-color-red">
                <a href="UnitUnavailability/Index" id="UnitAvailability">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Unit Unavailability</h3>
                    </div>
                </a>
            </div>
            <%} %>
            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.ApplicationSettings, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile  bg-color-blue">
                <a href="ApplicationSettings/Index" id="ApplicationSettings">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Application Settings</h3>
                    </div>
                </a>
            </div>
            <%} %>
            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.AverageMPH, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile  bg-color-green">
                <a href="AverageMPH/Index" id="AverageMPH">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Average MPH</h3>
                    </div>
                </a>
            </div>
            <%} %>
            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.TruckDownTime, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile bg-color-orangeDark">
                <a href="TruckDowntime/Index" id="TruckDowntime">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Truck Downtime</h3>
                    </div>
                </a>
            </div>
            <%} %>
            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.SiteLinkMileage, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile bg-color-green">
                <a href="SiteLinkMileage/Index" id="SLMileage">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">SL Mileage</h3>
                    </div>
                </a>
            </div>
            <%} %>
            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.TouchTime, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile bg-color-yellow">
                <a href="TouchTime/Index" id="touchTime">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Touch Time</h3>
                    </div>
                </a>
            </div>
            <%} %>
            <% if (PRFacAppWF.Security.SecuredEntityDelegate.CanUserExecute(PRFacAppWF.Entities.Enums.ModuleName.TruckTransferReason, PRFacAppWF.Entities.Enums.AccessLevel.Any, true))
                { %>
            <div class="tile bg-color-purple">
                <a href="TruckTransferReason/Index" id="TranfrReason">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Truck Transfer Reasons</h3>
                    </div>
                </a>
            </div>
            <%} %> 

            <div class="tile  bg-color-orange">
                <a href="DashBoard.aspx">
                    <div class="tile-content">
                        <h3 style="margin-bottom: 15px;">Return to DashBoard</h3>
                    </div>
                    <div class="brand bg-color-orange">
                        <div class="badge">
                            <i class="icon-arrow-left-3 fg-color-white" style="font-size: 29pt; margin-top: 5px;"></i>
                        </div>
                    </div>
                </a>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
