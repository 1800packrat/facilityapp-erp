﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PR.BusinessLogic;
using PR.Entities;
using PRFacAppWF.CodeHelpers;

namespace PRFacAppWF
{
    public partial class NonWatouches : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtStartDt.Text = DateTime.Now.Date.ToShortDateString();
                txtEndDt.Text = DateTime.Now.Date.AddDays(1).ToShortDateString();
            }
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            var startDate = Convert.ToDateTime(txtStartDt.Text);
            var endDt = Convert.ToDateTime(txtEndDt.Text);
            var totalDays = (endDt - startDate).Days;
            List<TransiteOrder> objTransiteOrders = new TransiteBusinessLogic().GetTransiteOrders(Convert.ToString(Session["WareHouseCode"]), startDate, endDt);
             
            dtNWaTouches.DataSource = objTransiteOrders;
            dtNWaTouches.DataBind();
            SMDBusinessLogic smd = new SMDBusinessLogic();
            dtNWaTouches1.DataSource = objTransiteOrders;
            dtNWaTouches1.DataBind();
            if (dtNWaTouches.Rows.Count > 0)
            {
                imagePrint.Visible = true;
                lblGridMsgNWT.Text = "";
                lblGridMsgNWT.Visible = false;
            }
            else
            {
                imagePrint.Visible = false;
                lblGridMsgNWT.Text = "No Records found!";
                lblGridMsgNWT.Visible = true;
            }
        }

        protected void dtNWaTouches1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Ed")
            {
                try
                {
                    var ds = Helper.QoridSearch(e.CommandArgument.ToString(),string.Empty);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        Session["qorid"] = ds.Tables[0].Rows[0]["iQRID_GlobalNum"].ToString();
                        Session["TennantId"] = ds.Tables[0].Rows[0]["TenantID"].ToString();
                        Session["rentalId"] = ds.Tables[0].Rows[0]["QTRentalID"].ToString();
                        Session["unitId"] = ds.Tables[0].Rows[0]["UnitID"].ToString();
                        Session["unitName"] = ds.Tables[0].Rows[0]["sUnitName"].ToString();
                        Session["RentalType"] = ds.Tables[0].Rows[0]["sRentalType"].ToString();
                        Session["RentalStatus"] = ds.Tables[0].Rows[0]["sRentalStatus"].ToString();
                        Session["locationCode"] = ds.Tables[0].Rows[0]["sLocationCode"].ToString();
                        Session["MoveType"] = ds.Tables[0].Rows[0]["sLDMOrderNum"].ToString();
                        DisplayCustomerinfo(ds.Tables[0].Rows[0]["sLocationCode"].ToString(), Convert.ToInt32(ds.Tables[0].Rows[0]["TenantID"]));
                        var localComponent = new LocalComponent();

                        string activity = string.Format("Qorid '{0}' Opened", Session["qorid"].ToString());
                        localComponent.InsertActivityLog(Convert.ToInt32(Helper.GetSession("qorid")), "Opened QOR", activity, Helper.GetSession("userId").ToString());
                        Helper.UpdateSlapi();
                        Response.Redirect("~/Touches.aspx", false);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertScript", "alert('" + ex.Message + "');", true);
                }
            }
        }
        protected void dtNWaTouches_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            dtNWaTouches.PageIndex = e.NewPageIndex;
            var startDate = Convert.ToDateTime(txtStartDt.Text);
            var endDt = Convert.ToDateTime(txtEndDt.Text);
            var totalDays = (endDt - startDate).Days;
            List<TransiteOrder> objTransiteOrders = new TransiteBusinessLogic().GetTransiteOrders(Convert.ToString(Session["WareHouseCode"]), startDate, endDt);
            dtNWaTouches.DataSource = objTransiteOrders;
            dtNWaTouches.DataBind();
        }
        public void QoridSearch(string qorid)
        {
            var smd = new SMDBusinessLogic(Session["UserId"] != null ? Convert.ToString(Session["UserId"]) : "");
            var lc = new LocalComponent();
            var ds = new DataSet();
            var dsXref = new DataSet();

            string unitId = string.Empty;

            int ratsQORID = 0;
            int slGlobalQORID = 0;
            if (qorid.ToLower().Contains("web")) //its a webQOR
            {
                dsXref = lc.GetSLGlobalQORFromXRef(-1, qorid);
                if (dsXref.Tables.Count > 0)
                {
                    if (dsXref.Tables[0].Rows.Count > 0)
                    {
                        if (dsXref.Tables[0].Rows[0]["GlobalQORID"] != DBNull.Value)
                        {
                            slGlobalQORID = Convert.ToInt32(dsXref.Tables[0].Rows[0]["GlobalQORID"]);
                        }
                        else
                        {
                            ratsQORID = (Convert.ToInt32(dsXref.Tables[0].Rows[0]["QORID"]));
                            slGlobalQORID =
                                (new SMDBusinessLogic(Session["UserId"] != null
                                                          ? Convert.ToString(Session["UserId"])
                                                          : "")).TransferQORFromRATStoSL(ratsQORID);
                        }
                        ds = smd.GetQORList(Helper.Corpcode, string.Empty, Helper.username, Helper.password, 0,
                                            string.Empty, slGlobalQORID, string.Empty);
                    }
                }

                string activity = string.Format("Search is made on '{0}' Qorid", qorid);
                lc.InsertActivityLog(slGlobalQORID, "Search by QORID", activity, Helper.GetSession("userId").ToString());
            }
            else if (qorid.Length > 6) //its a sitelink qorid
            {
                ds = smd.GetQORList(Helper.Corpcode, string.Empty, Helper.username, Helper.password, 0, string.Empty,
                                    Convert.ToInt32(qorid), string.Empty);
                string activity = string.Format("Search is made on '{0}' Qorid", qorid);
                lc.InsertActivityLog(Convert.ToInt32(qorid), "Search by QORID", activity, Helper.GetSession("userId").ToString());
            }
            else if (!string.IsNullOrEmpty(unitId))
            {
                //ds = smd.GetQORList(Helper.Corpcode, string.Empty, Helper.username, Helper.password, tenantId, string.Empty, 0, string.Empty);
                ds = smd.GetQORList(Helper.Corpcode, string.Empty, Helper.username, Helper.password, 0,
                                    unitId.ToString(), 0, string.Empty);
            }
            else // its a RATSQOR
            {
                dsXref = lc.GetSLGlobalQORFromXRef(Convert.ToInt32(qorid), "");

                # region commented
                //ds = smd.GetQORList(Helper.Corpcode, string.Empty, Helper.username, Helper.password, 0, string.Empty, lc.GetSLGlobalQORFromXRef(Convert.ToInt32(qorid), "-1"), string.Empty);
                //if (ds == null || ds.Tables.Count == 0) // Creating Quote in Sitelink DB
                //{
                //    int GlobalQOR =  (new SMDBusinessLogic(Session["UserId"] != null ? Convert.ToString(Session["UserId"]) : "")).TransferQORFromRATStoSL(Convert.ToInt32(qorid));
                //    if (GlobalQOR != 0)
                //    {
                //        ds = smd.GetQORList(Helper.Corpcode, string.Empty, Helper.username, Helper.password, 0, string.Empty, GlobalQOR, string.Empty);
                //    }
                //}

                #endregion

                if (dsXref.Tables.Count > 0)
                {
                    if (dsXref.Tables[0].Rows.Count > 0)
                    {
                        if (dsXref.Tables[0].Rows[0]["GlobalQORID"] != DBNull.Value)
                        {
                            slGlobalQORID = Convert.ToInt32(dsXref.Tables[0].Rows[0]["GlobalQORID"]);
                        }
                        else
                        {
                            slGlobalQORID =
                                (new SMDBusinessLogic(Session["UserId"] != null
                                                          ? Convert.ToString(Session["UserId"])
                                                          : "")).TransferQORFromRATStoSL(Convert.ToInt32(qorid));
                        }
                        ds = smd.GetQORList(Helper.Corpcode, string.Empty, Helper.username, Helper.password, 0,
                                            string.Empty, slGlobalQORID, string.Empty);
                    }
                    string activity = string.Format("Search is made on '{0}' Qorid", qorid);
                    lc.InsertActivityLog(slGlobalQORID, "Search by QORID", activity, Helper.GetSession("userId").ToString());
                }
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                // Session["locationCode"] = ds.Tables[0].Rows[0]["sLocationCode"].ToString();



            }
            else
            {
                throw new Exception("Qorid not found");
            }
        }

        private void DisplayCustomerinfo(string locationCode, int tenantId)
        {
            SMDBusinessLogic smdObject = new SMDBusinessLogic();
            var ds = smdObject.GetCustomerByTenantId(Helper.Corpcode, locationCode, Helper.username, Helper.password, tenantId);
            //  Helper.UpdateSlapi();
            if (ds.Tables[0].Rows.Count > 0)
            {
                var customerSession = new CustomerSession();
                ////bind the values to the textbox and store them into customerSession object////
                customerSession.firstName = ds.Tables[0].Rows[0]["sFName"].ToString();
                customerSession.lastName = ds.Tables[0].Rows[0]["sLName"].ToString();
                customerSession.phone = ds.Tables[0].Rows[0]["sPhone"].ToString();
                customerSession.addr2 = ds.Tables[0].Rows[0]["sAddr2"].ToString();
                customerSession.addr1 = ds.Tables[0].Rows[0]["sAddr1"].ToString();
                customerSession.city = ds.Tables[0].Rows[0]["sCity"].ToString();
                customerSession.state = ds.Tables[0].Rows[0]["sRegion"].ToString();
                customerSession.zip = ds.Tables[0].Rows[0]["sPostalCode"].ToString();
                customerSession.fax = ds.Tables[0].Rows[0]["sFax"].ToString();

                customerSession.mobilePhone = ds.Tables[0].Rows[0]["sMobile"].ToString();
                customerSession.email = ds.Tables[0].Rows[0]["sEmail"].ToString();
                customerSession.companyName = ds.Tables[0].Rows[0]["sCompany"].ToString();
                customerSession.companyName = ds.Tables[0].Rows[0]["sFNameAlt"].ToString();
                customerSession.companyName = ds.Tables[0].Rows[0]["sLNameAlt"].ToString();
                customerSession.companyName = ds.Tables[0].Rows[0]["sAddr1Alt"].ToString();
                customerSession.companyName = ds.Tables[0].Rows[0]["sAddr2Alt"].ToString();
                customerSession.companyName = ds.Tables[0].Rows[0]["sCityAlt"].ToString();
                customerSession.companyName = ds.Tables[0].Rows[0]["sRegionAlt"].ToString();
                customerSession.companyName = ds.Tables[0].Rows[0]["sPostalCodeAlt"].ToString();
                customerSession.companyName = ds.Tables[0].Rows[0]["sEMailAlt"].ToString();
                customerSession.companyName = ds.Tables[0].Rows[0]["sPhoneAlt"].ToString();
                customerSession.companyName = ds.Tables[0].Rows[0]["sRelationshipAlt"].ToString();
                //   txtExt.Text = customerSession.ext = ds.Tables[0].Rows[0]["sExt"].ToString();
                ///additional information on customer session

                customerSession.gateCode = ds.Tables[0].Rows[0]["sAccessCode"].ToString();

                customerSession.businessPhone = string.Empty;
                customerSession.middlename = string.Empty; // ds.Tables[0].Rows[0]["sPostalCode"].ToString();
                customerSession.pager = ds.Tables[0].Rows[0]["sPager"].ToString();

                customerSession.prefix = string.Empty; // ds.Tables[0].Rows[0]["sPostalCode"].ToString();
                customerSession.tenantNotes = string.Empty; //ds.Tables[0].Rows[0]["sPostalCode"].ToString();
                customerSession.dateOfBirth = ds.Tables[0].Rows[0]["dDOB"] == DBNull.Value
                                                  ? DateTime.MinValue
                                                  : Convert.ToDateTime(ds.Tables[0].Rows[0]["dDOB"]);
                customerSession.isCommercialTenant = Convert.ToBoolean(ds.Tables[0].Rows[0]["bCommercial"]);
                customerSession.isCompanyTenant = Convert.ToBoolean(ds.Tables[0].Rows[0]["bCompanyIsTenant"]);
                if (string.IsNullOrEmpty(customerSession.state) || string.IsNullOrEmpty(customerSession.city))
                {
                    LocalComponent localComponent = new LocalComponent();
                    PR.Entities.ZipAddressInfo zipAddressInfo = new ZipAddressInfo();
                    zipAddressInfo = localComponent.GetZipCodeInfo(customerSession.zip, string.Empty); customerSession.state = zipAddressInfo.State1;
                    customerSession.city = zipAddressInfo.City1;

                }
                Session["customer"] = customerSession;
            }
        }
    }
}