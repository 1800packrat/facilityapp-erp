﻿using PR.LocalLogisticsSolution.Model;
using PRFacAppWF.CodeHelpers;
using System;
using System.Web;
using System.Web.Security;

namespace PRFacAppWF
{
    public partial class Site : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {



        }

        protected void lbtnLgoout_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();
            HttpContext.Current.Response.Redirect("/Login/Index", true);
            //Response.Redirect("Login.aspx", false);
        }

        public LogisticsUser CurrentUser
        {
            get
            {
                return Helper.CurrentUser;

            }
        }
    }
}