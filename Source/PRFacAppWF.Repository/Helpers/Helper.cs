﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Repository.Helpers
{
    public class Helper
    {
        public static SqlParameter SqlParameterObj(string paramName, object val)
        {
            return new SqlParameter { ParameterName = paramName, Value = val ?? DBNull.Value };
        }

        public static string GetFullTouchType(string TouchType)
        {
            switch (TouchType)
            {
                case "DE":
                    return "WarehouseToCurbEmpty";
                case "DF":
                    return "WarehouseToCurbFull";
                case "CC":
                    return "CurbToCurb";
                case "RE":
                    return "ReturnToWarehouseEmpty";
                case "RF":
                    return "ReturnToWarehouseFull";
                case "WA":
                    return "WarehouseAccess";
                case "IBO":
                    return "InByOwner";
                case "OBO":
                    return "OutByOwner";
                case "TI":
                    return "LDMTransferIn";
                case "TO":
                    return "LDMTransferOut";
                default:
                    return TouchType;
            }
        }
    }
}
