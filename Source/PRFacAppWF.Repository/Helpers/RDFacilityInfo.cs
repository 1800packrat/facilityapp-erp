﻿namespace PRFacAppWF.Repository.Helpers
{
    using models;
    using System.Collections.Generic;
    using Services;
    using System.Linq;
    using PR.Entities;
    using System;
    using PR.UtilityLibrary;

    public class RDFacilityInfo
    {
        private IUserSecurity prsService;

        protected List<RDFacility> _facilities;

        protected static RDFacilityInfo theInstance;

        protected RDFacilityInfo()
        {
        }

        protected static RDFacilityInfo GetInstance()
        {
            if (theInstance == null)
            {
                theInstance = new RDFacilityInfo();
                theInstance.init();
            }

            return theInstance;
        }

        public static RDFacility GetFacilityInfo(int storeRowID)
        {
            return GetInstance()._facilities.Where(f => f.StoreRowID == storeRowID).FirstOrDefault();
        }

        public static  SiteInfo GetSiteInfo(int storeRowID)
        {
            var dsSite = GetFacilityInfo(storeRowID);

            SiteInfo siteInfo = new SiteInfo();

            siteInfo.SiteId = dsSite.SLSiteId.Value;
            siteInfo.LocationCode = dsSite.SLLocCode;
            siteInfo.SiteName = dsSite.CompDBAName;
            siteInfo.GlobalSiteNum = Convert.ToString(dsSite.StoreNo);
            siteInfo.SiteAddress.AddressLine1 = dsSite.Street1;
            siteInfo.SiteAddress.AddressLine2 = dsSite.Street2;
            siteInfo.SiteAddress.City = dsSite.City;
            siteInfo.SiteAddress.State = dsSite.State;
            siteInfo.SiteAddress.Zip = dsSite.ZipCode;
            siteInfo.SiteAddress.Latitude = Convert.ToString(dsSite.Latitude);
            siteInfo.SiteAddress.Longitude = Convert.ToString(dsSite.Longitude);
            siteInfo.SiteAddress.AddressType = PREnums.AddressType.Warehouse;
            siteInfo.LegalName = dsSite.CompDBAName;
            siteInfo.EMail = dsSite.Email;
            siteInfo.MarketId = dsSite.FK_MarketId;

            return siteInfo;
        }

        protected void init()
        {
            this.prsService = new Repository.Services.Implementations.UserSecurityService();
            this._facilities = prsService.GetFacilitiesList();
        }

        public static void Reload()
        {
            GetInstance().init();
        }
    }
}