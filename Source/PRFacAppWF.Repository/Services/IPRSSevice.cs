﻿using System;
using System.Collections.Generic;
using System.Linq;
using PRFacAppWF.models;
using PRFacAppWF.Repository.models;
using PR.LocalLogisticsSolution.Model;
using PRFacAppWF.Entities.Touches;
using PRFacAppWF.Entities.Enums;
using PRFacAppWF.Entities;

namespace PRFacAppWF.Repository.Services
{
    public interface IPRSService
    {
       
        IQueryable<TruckStatus> GetTruckStatus();

        List<Trucks> GetTruckDetails(string Facility, DateTime date);

        int CheckTruckStatusEntryExists(int TruckId);

        int SaveTruckStatusDetails(TruckStatusDetails truckStatusDetails);

        IQueryable<TruckStatusInfo> GetTruckStatusDetailList(TruckStatusFilterCriteria truckStatusCriteria);

        IQueryable<TruckStatusInfo> GetTruckStatusDetailList(SearchFilter<TruckStatusFilterCriteria> truckStatusCriteria);

        List<UnitAvailabilityInfo> GetUnitUnavailabilityInfo(DateTime startDate, DateTime endDate, string[] unitType, int UserID);

        UnitAvailabilityInfo GetUnitUnavailabilityInfo(int UnitAvailabilityInfoId);

        void SaveUnitAvaialability(UnitAvailabilityInfo uaInfo);

        IQueryable<TruckOpHoursInfo> GetTruckOpHoursDetails(DateTime FromDate, DateTime ToDate, string LocationCode);

        List<TruckOHReason> GetTruckOHReason();

        void AddTruckOpHours(TrkOpHours trkInfo);

        string SaveTruckOpearationalHour(TrkOpHours trkInfo);

        TrkOpHours GetTruckOpHrsDetails(int CAPTruckOperatingHourId);

        //IQueryable<FacilityOpHours> GetFacilityOpHours(DateTime FromDate, DateTime ToDate, string LocationCode);
        List<FacilityOpHours> GetFacilityOpHours(DateTime FromDate, DateTime ToDate, string LocationCode);

        List<CAPTruckTransferReason> GetTransferReason();

        List<TruckTempHourChange> GetTruckTempOpHourDetails(int facilityStoreNo, DateTime startDate, int noOfDays, int truckId, int weekday);

        void SaveTempHourChange(List<TruckTempHourChange> listTempHourChangeEntries, int storeNumber, int userId);

        void RevertToFacilityOperatingHours(List<TruckTempHourChange> listTempHourChangeEntries, int storeNumber, int userId);

        void TruckBackToService(int CAPTruckOperatingHourId, int userId);

        List<FacilityOperatingHours> AddAndGetFacilityOpHour(int facilityStoreNo, DateTime startDate, DateTime endDate, string userName, int weekday);

        int SaveFacilityHourChange(string rowIds, string startTime, string endTime, int userId, string storeOpen);

        #region Application Settings
        List<ApplicationSettings> GetAllApplicationSettings();

        List<ApplicationSettings> SearchApplicationSettingsList(string SettingName);

        string UpdateApplicationSettingsValues(int UserId, string SettingName, DateTime startDate, DateTime? endDate, string ApplicationValue, bool isNullEnddate);

        List<ApplicationSettings> GetApplicationSettingsList(DateTime date, string SortCol = null, string SortDir = null);

        #endregion

        #region Average MPH
        List<AverageMPH> GetAverageMPHList(DateTime date, int UserID, string SortCol = null, string SortDir = null);

        List<RDFacility> GetFacilitiesList();

        List<RDFacility> GetFacilityListByUserId(int UserId);

        List<AverageMPH> SearchFacilityAverageMPHList(int facilityId);

        string UpdateFacilityAvgMPHValues(int UserId, int facilityId, DateTime startDate, DateTime? endDate, decimal mphval, bool isNullEnddate);

        string SetAverageMPHtoCorporateDefault(int UserId, int facilityId, DateTime startDate);

        #endregion

        #region TruckDowntime
        List<TruckDowntimeModel> GetTruckDowntimeList(DateTime date, int UserID, string SortCol = null, string SortDir = null);

        List<TruckDowntimeModel> SearchFacilityTruckDowntimeList(int facilityId);

        string UpdateFacilityTruckDowntimeValues(int UserId, int facilityId, DateTime startDate, DateTime? endDate, int truckDowntime, bool isNullEnddate);

        string SetTruckDowntimetoCorporateDefault(int UserId, int facilityId, DateTime startDate);

        #endregion

        #region TouchTime
        List<TouchTime> GetTouchTimeList(DateTime date, int UserID, string SortCol = null, string SortDir = null);

        List<TouchTime> SearchFacilityTouchTimeList(int facilityId);

        string UpdateFacilityTouchTimeValues(int UserId, int facilityId, DateTime startDate, DateTime? endDate, int DE, int DF, int RE, int RF, int OBO, int IBO, int WA, int CC, bool isNullEnddate);

        string SetTouchtimetoCorporateDefault(int UserId, int facilityId, DateTime startDate);
        #endregion

        #region SLMileage
        List<SLMileage> GetSLMileageList(DateTime date, int UserID, string SortCol = null, string SortDir = null);

        List<SLMileage> SearchSLMileageList(int facilityId);

        string UpdateSLMileageValues(int UserId, int facilityId, DateTime startDate, DateTime? endDate, decimal SLMileageAdjustment, bool isNullEnddate);

        string SetSLMileagetoCorporateDefault(int UserId, int facilityId, DateTime startDate);

        #endregion

        #region TruckTransferReason
        List<CAPTruckTransferReason> GetTransferReasonSearchresults(string SearchKey);
        string GetTransferReasonbyId(int TransferReasonId);
        string SaveTransferReasonwithId(int TransferReasonId, string TransferReason);
        string SetTransferReasonActiveInactive(int TransferReasonId, int status);
        #endregion

        List<Entities.ModuleAndAccessLevel> GetRoleGroupModuleAccess(string roleGroupName);

        #region WareHouseTouches
 
        void SaveWTTouchInformation(WATouchInfo TouchInfo, int UserId);
 
        EmailTemplate GetTouchCompleteEmailTemplate(int TemplateId);

        List<WATouchInfoHistory> GetTouchHistory(int QORID, string TouchType, int SeqNumber);

        #endregion

        #region WareHouseTouchContainerStatus

        //List<WATouchContainerStatus> GetStatusByTouchTypeAndScheduleDate(string TouchType, DateTime TouchScheduleDate, int StoreNumber);

        List<PRFacAppWF.Entities.Touches.FAWTTouchStatus> GetWHTStatusList();

        List<FAWTTouchActions> SearchContainerStatus(int StoreNumber, string StatusName);

        void AddContainerStatus(FAWTTouchActions oContainerStatus, int UserId);

        void ToggleActiveAndInactiveContainerStatus(int PKID, string Status, int UserId, int IsGlobalStatus, int StoreNumber);

        //WATouchTypesAndStatus GetTouchStatusSetting(string TouchType, WATouchStatus StatusCategory);

        WATouchTypesAndActionCategory GetTouchActionSetting(string TouchType, WTTouchActionCategory ActionCategory);

        List<FAWTTouchActions> GetActionsByTouchType(string TouchType, int StatusId, DateTime TouchScheduleDate, int StoreNumber);
        #endregion

        #region EmailLog
        void InsertEmailLog(FAEmailHistoryLog LogDetails);
        #endregion

        FAGlobalSettings GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey SettingsKey);

        FAGlobalSettings GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey SettingsKey, string StoreNumber);
 
        List<WATouchInfo> GetWATouchInformation(DateTime startDate, DateTime endDate, int StoreNumber, List<WATouchInfo> SLTouches);

        USS_EmailConfiguration GetEmailTemplateDetails(int TemplateID);
 
    }
}
