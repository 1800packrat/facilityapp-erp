﻿namespace PRFacAppWF.Repository.Services.Implementations
{
    using PRFacAppWF.models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using PRFacAppWF.Repository.models;
    using PR.Entities;
    using PR.LocalLogisticsSolution.Model;
    using PRFacAppWF.Repository.Helpers;
    using System.Transactions;
    using PRFacAppWF.Entities.Enums;
    using Entities;
    using System.Data.Entity;
    using Entities.UserManagement;
    using Entities.DriverSchedule;

    public class UserSecurityService : IUserSecurity
    { 

        public UserSecurityService()
        { 
        }

        public List<PRFacAppWF.Entities.UserManagement.FADTOGroupMaster> ListRoles()
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                return context.FAGroupMaster.Select(gm => new PRFacAppWF.Entities.UserManagement.FADTOGroupMaster
                {
                    GroupName = gm.GroupName,
                    PK_GroupId = gm.PK_GroupId,
                    Description = gm.Description,
                    IsActive = gm.IsActive,
                    HasDriverAppAccess = gm.FAGroupAndModule.Any(g => (g.FK_AccessLevelID == (int)AccessLevel.ReadAndWrite) && g.IsActive == true && g.FAModule.FA_ModuleArea.FK_ApplicationID == (int)Entities.Enums.Application.DriverApp),
                    HasFacilityAppAccess = gm.FAGroupAndModule.Any(g => (g.FK_AccessLevelID == (int)AccessLevel.ReadAndWrite || g.FK_AccessLevelID == (int)AccessLevel.ReadOnly) && g.IsActive == true && g.FAModule.FA_ModuleArea.FK_ApplicationID == (int)Entities.Enums.Application.FacilityApp)
                }).ToList();
            } 
        }

        public int RoleGroupIDByName(string groupName)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var gm = context.FAGroupMaster.Where(r => r.GroupName == groupName).FirstOrDefault();

                if (gm != null)
                {
                    return gm.PK_GroupId;
                }

                return -1;
            }
        }

        public PRFacAppWF.Entities.UserManagement.FADTOGroupMaster RoleInfoById(int rollID)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var gm = context.FAGroupMaster.Find(rollID);
                PRFacAppWF.Entities.UserManagement.FADTOGroupMaster oFAGroupMaster = new Entities.UserManagement.FADTOGroupMaster();

                if (gm != null)
                {
                    oFAGroupMaster = new Entities.UserManagement.FADTOGroupMaster
                    {
                        GroupName = gm.GroupName,
                        PK_GroupId = gm.PK_GroupId,
                        Description = gm.Description,
                        IsActive = gm.IsActive,
                        FAGroupAndModule = gm.FAGroupAndModule.Where(x => x.IsActive == true).Select(m =>
                                                                    new PRFacAppWF.Entities.UserManagement.FADTOGroupAndModule
                                                                    {
                                                                        FK_GroupId = m.FK_GroupId,
                                                                        PK_Id = m.PK_Id,
                                                                        FK_ModuleID = m.FK_ModuleID,
                                                                        FK_AccessLevelID = m.FK_AccessLevelID,
                                                                        IsActive = m.IsActive
                                                                    }).ToList()

                    };
                }

                return oFAGroupMaster;
            }
        }

        public List<PRFacAppWF.Entities.UserManagement.FADTOModuleArea> ListAreas()
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FA_ModuleArea.Where(x => x.Status == true).Select(ma => new PRFacAppWF.Entities.UserManagement.FADTOModuleArea
                {
                    Area = ma.Area,
                    Description = ma.Description,
                    PK_ID = ma.PK_ID,
                    Status = ma.Status,
                    FK_ApplicationID = ma.FK_ApplicationID.Value
                }).ToList();
            }
        }

        public List<PRFacAppWF.Entities.UserManagement.FADTOModule> GetModules()
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FAModule.Where(x => x.IsActive == true).OrderBy(x => x.ModuleName).Select(fm => new PRFacAppWF.Entities.UserManagement.FADTOModule
                {
                    PK_ModuleID = fm.PK_ModuleID,
                    Description = fm.Description,
                    ModuleName = fm.ModuleName,
                    ParentID = fm.ParentID,
                    IsActive = fm.IsActive,
                    FK_AreaID = fm.FK_AreaID,
                    FK_AccessLevelID = fm.FK_DefaultAccessLevelID.Value,
                    FAModuleArea = new Entities.UserManagement.FADTOModuleArea
                    {
                        Area = fm.FA_ModuleArea.Area,
                        PK_ID = fm.FA_ModuleArea.PK_ID,
                        FK_ApplicationID = fm.FA_ModuleArea.FK_ApplicationID.Value
                    }
                }).ToList();
            }
        }

        public PRFacAppWF.Entities.UserManagement.FADTOUserMaster GetUserInfoById(int UserID)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var um = context.FAUserMaster.Find(UserID);
                PRFacAppWF.Entities.UserManagement.FADTOUserMaster oFAGroupMaster = new Entities.UserManagement.FADTOUserMaster();

                if (um != null)
                {
                    oFAGroupMaster = new Entities.UserManagement.FADTOUserMaster
                    {
                        PK_UserId = um.PK_UserId,
                        FirstName = um.FirstName,
                        LastName = um.LastName,
                        AddressLine1 = um.AddressLine1,
                        AddressLine2 = um.AddressLine2,
                        Password = um.Password,
                        State = um.State,
                        City = um.City,
                        ZipCode = um.ZipCode,
                        EmailAddress = um.EmailAddress,
                        Username = um.Username,
                        IsActive = um.IsActive,
                        FK_GroupID = um.FK_GroupID,
                        StatusID = um.FK_StatusID.HasValue ? um.FK_StatusID.Value : 1,
                        LicenseClassID = um.FK_LicenseClassID.HasValue ? um.FK_LicenseClassID.Value : 0,
                        UniqueID = um.UniqueID,
                        FAUserAndFacilityAccess = um.FAUserAndFacilityAccess.Where(f => f.IsActive == true && (f.FK_ApplicationID == (int)Application.FacilityApp || (f.FK_ApplicationID == (int)Application.DriverApp && f.EndDate.HasValue == false)))
                                                                             .Select(uf => new PRFacAppWF.Entities.UserManagement.FADTOUserAndFacilityAccess
                                                                             {
                                                                                 PK_ID = uf.PK_ID,
                                                                                 FK_AccessLevelID = uf.FK_AccessLevelID,
                                                                                 FK_ApplicationID = uf.FK_ApplicationID.Value,
                                                                                 FK_StoreRowID = uf.FK_StoreRowID,
                                                                                 FK_UserID = uf.FK_UserID,
                                                                                 StartDate = uf.StartDate.HasValue ? uf.StartDate.Value : DateTime.Now,
                                                                                 EndDate = uf.EndDate
                                                                             }).ToList()
                    };
                }
                else
                {
                    oFAGroupMaster = new Entities.UserManagement.FADTOUserMaster
                    {
                        StatusID = 1 //For new objects make status id default to one. it is active
                    };
                }

                return oFAGroupMaster;
            } 
        }

        /// <summary>
        /// This method is used to add and update roles and module accessibility to particular role.
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="RoleName"></param>
        /// <param name="RoleDescription"></param>
        /// <param name="moduleIds"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public void SaveRole(int RoleId, string RoleName, string RoleDescription, ModuleAccessRelation[] moduleIds, int LoginUserID)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
                                                                                                new TransactionOptions
                                                                                                {
                                                                                                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                                                                                }))
            {
                using (var context = new PRSEntities())
                {
                    context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    if (RoleId > 0)
                    {
                        FAGroupMaster gm = context.FAGroupMaster.Where(x => x.PK_GroupId == RoleId).FirstOrDefault();

                        gm.Description = RoleDescription;
                        gm.GroupName = RoleName;
                        gm.UpdatedBy = Convert.ToString(LoginUserID);
                        gm.UpdatedDateTime = DateTime.Now;

                        ///Make all existing records in-active and insert new set of records, this will be used in future
                        List<FAGroupAndModule> fgmlist = context.FAGroupAndModule.Where(x => x.FK_GroupId == RoleId && x.IsActive == true).ToList();
                        foreach (var fgm in fgmlist)
                        {
                            fgm.IsActive = false;
                            fgm.UpdatedBy = Convert.ToString(LoginUserID); ;
                            fgm.UpdatedDateTime = DateTime.Now;
                        }
                    }
                    else
                    {
                        FAGroupMaster gm = new FAGroupMaster();
                        gm.Description = RoleDescription;
                        gm.IsActive = true;
                        gm.GroupName = RoleName;
                        gm.CreatedBy = Convert.ToString(LoginUserID);
                        gm.CreatedDateTime = DateTime.Now;

                        context.FAGroupMaster.Add(gm);
                    }
                     
                    if (moduleIds != null && moduleIds.Count() > 0)
                    {
                        foreach (ModuleAccessRelation moduleid in moduleIds)
                        {
                            FAGroupAndModule fgm = new FAGroupAndModule();

                            fgm.FK_GroupId = RoleId;
                            fgm.FK_ModuleID = moduleid.ModuleID;
                            fgm.FK_AccessLevelID = moduleid.AccessLevelID;
                            fgm.CreatedBy = Convert.ToString(LoginUserID);
                            fgm.CreatedDateTime = DateTime.Now;
                            fgm.IsActive = true;

                            context.FAGroupAndModule.Add(fgm);
                        }
                    }

                    context.SaveChanges();

                    scope.Complete();
                    scope.Dispose();
                }
            }
        }
 

        public void ToggleRoleStatus(int RoleId, string Status, int LoginUserID)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                FAGroupMaster gm = context.FAGroupMaster.Where(x => x.PK_GroupId == RoleId).FirstOrDefault();
                gm.IsActive = Status == "InActive" ? false : true;
                gm.UpdatedBy = Convert.ToString(LoginUserID);
                gm.UpdatedDateTime = DateTime.Now;

                context.SaveChanges();
            }
        }

        public bool CheckRoleName(string RoleName, int RoleId)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FAGroupMaster.Any(x => x.GroupName == RoleName && x.PK_GroupId != RoleId);
            }
        }

        public List<PRDriverSchedule> AddAndGetDriverScheduleHours(DateTime StartDate, DateTime EndDate, int LogisticsUserId, int StoreNo, int CreatedBy, int DATEPART)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                return context.Database.SqlQuery<PRDriverSchedule>
                    (@"exec USP_AddAndGetLogisticsUserScheduleHour @StartDate, @EndDate, @LogisticsUserId, @StoreNo, @CreatedBy, @DATEPART",
                    Helpers.Helper.SqlParameterObj("StartDate", StartDate),
                    Helpers.Helper.SqlParameterObj("EndDate", EndDate),
                    Helpers.Helper.SqlParameterObj("LogisticsUserId", LogisticsUserId),
                    Helpers.Helper.SqlParameterObj("StoreNo", StoreNo),
                    Helpers.Helper.SqlParameterObj("CreatedBy", CreatedBy),
                    Helpers.Helper.SqlParameterObj("DATEPART", DATEPART)).ToList();
            }
        }

        public int SaveDriverHourChangesToDB(string rowIds, string startTime, string endTime, int userId, string storeOpen)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.Database.SqlQuery<int>
                    (@"exec USP_UpdateLogisticsUserScheduleHour @RowIds, @StartTime, @EndTime, @DayWork, @CreatedBy",
                    Helpers.Helper.SqlParameterObj("RowIds", rowIds),
                    Helpers.Helper.SqlParameterObj("StartTime", startTime),
                    Helpers.Helper.SqlParameterObj("EndTime", endTime),
                    Helpers.Helper.SqlParameterObj("DayWork", storeOpen),
                    Helpers.Helper.SqlParameterObj("CreatedBy", userId)).Single();
            }
        }
 

        public List<FAUserMaster> GetAllUserDetails(string Name, int RoleNo, bool IsActive)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                List<FAUserMaster> lstUsers = context.FAUserMaster.Where(u =>
                        (string.IsNullOrEmpty(Name)
                            || (!string.IsNullOrEmpty(Name)
                                &&
                                (u.Username.ToLower().Contains(Name)
                                || u.FirstName.ToLower().Contains(Name)
                                || u.LastName.ToLower().Contains(Name)
                                || (u.FirstName + " " + u.LastName).ToLower().Contains(Name))))
                        && (RoleNo <= 0 || (RoleNo > 0 && u.FK_GroupID == RoleNo))
                        && IsActive == u.IsActive)
                        .ToList()
                        .Select(u => new FAUserMaster
                        {
                            PK_UserId = u.PK_UserId,
                            Username = u.Username,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            EmailAddress = u.EmailAddress,
                            FK_GroupID = u.FK_GroupID,
                            UniqueID = u.UniqueID,
                            FAGroupMaster = new FAGroupMaster
                            {
                                PK_GroupId = u.FAGroupMaster.PK_GroupId,
                                GroupName = u.FAGroupMaster.GroupName,
                                IsActive = u.FAGroupMaster.IsActive,
                                Description = u.FAGroupMaster.Description
                            },
                            IsActive = u.IsActive
                        }).ToList();

                return lstUsers;
            }
        } 

        /// <summary>
        /// This method will save user details including accessable facilities and update username password
        /// </summary>
        /// <param name="objUser"></param>
        /// <param name="selectedFailities"></param>
        /// <param name="LoginUserID"></param>
        public void SaveUserDetails(PRFacAppWF.models.User objUser, FacilityUserRelation[] selectedFailities, int LoginUserID)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
                                                                                                new TransactionOptions
                                                                                                {
                                                                                                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                                                                                }))
            {
                using (var context = new PRSEntities())
                {
                    context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    if (objUser.UserId > 0)
                    {
                        FAUserMaster umaster = context.FAUserMaster.Where(x => x.PK_UserId == objUser.UserId).FirstOrDefault();

                        umaster.FirstName = objUser.FirstName;
                        umaster.LastName = objUser.LastName;
                        umaster.EmailAddress = objUser.EmailAddress;

                        if (!string.IsNullOrEmpty(objUser.Password))
                        {
                            if (objUser.EncryptPassword != umaster.Password)
                            {
                                //if user is trying to update the password, we need to enable isChangepasswordRequired flag to true.
                                //This flag will prompt Change password screen.
                                umaster.IsChangePasswordRequired = true;

                                FAUserPasswordHistory passHistory = new FAUserPasswordHistory()
                                {
                                    FK_UserId = objUser.UserId,
                                    Password = umaster.Password,
                                    CreatedBy = Convert.ToString(objUser.UserId),
                                    CreatedDateTime = DateTime.Now
                                };
                                context.FAUserPasswordHistory.Add(passHistory);
                            }
                            umaster.Password = objUser.EncryptPassword;
                        }

                        umaster.Username = objUser.UserName;
                        umaster.FK_GroupID = objUser.RoleId;
                        umaster.IsActive = objUser.IsActive;
                        umaster.UpdatedBy = Convert.ToString(LoginUserID);
                        umaster.UpdatedDateTime = DateTime.Now;
                        umaster.FK_StatusID = objUser.IsActive ? (int)UserStatus.Active : (int)UserStatus.Inactive;
                        if (objUser.LicenseClassID > 0)
                            umaster.FK_LicenseClassID = objUser.LicenseClassID;

                        ///WE ARE CHECKING SAME CONDTION IN PRFacAppWF.Entities.UserManagement.AddEditUserViewModel VIEW MODEL, IF YOU DO ANY CHANGES HERE, PLEASE MAKE SURE TO UPDATE SAME THERE AS WELL
                        bool canEditDAFacilities = (umaster.FAUserAndFacilityAccess.Any(f => f.FK_ApplicationID == (int)Application.DriverApp && f.FK_AccessLevelID == (int)AccessLevel.PrimaryFacility) == false);
                        List<FAUserAndFacilityAccess> userFacAccess = new List<FAUserAndFacilityAccess>();

                        if (canEditDAFacilities)
                        {
                            ///Make all existing records in-active and insert new set of records, this will be used in future
                            ///Since we are not allowing to edit driver facility list in UI, dont inactive them here. Only Facility app facilities make it inactive.
                            userFacAccess = umaster.FAUserAndFacilityAccess.Where(x => x.IsActive == true).ToList();
                        }
                        else
                        {
                            userFacAccess = umaster.FAUserAndFacilityAccess.Where(x => x.IsActive == true && x.FK_ApplicationID == (int)Application.FacilityApp).ToList();

                            ///Since we are not allowing to edit driver facility list in UI, work only with facility app facilities not driver app facilities.
                            selectedFailities = selectedFailities.Where(f => f.ApplicationTypeID == (int)Application.FacilityApp).ToArray();
                        }

                        foreach (var ufa in userFacAccess)
                        {
                            ufa.IsActive = false;
                            ufa.UpdatedBy = Convert.ToInt32(LoginUserID);
                            ufa.UpdatedDate = DateTime.Now;
                        }
                    }
                    else
                    {
                        FAUserMaster umaster = new FAUserMaster();
                        umaster.FirstName = objUser.FirstName;
                        umaster.LastName = objUser.LastName;
                        umaster.EmailAddress = objUser.EmailAddress;
                        umaster.Password = objUser.EncryptPassword;
                        umaster.Username = objUser.UserName;
                        umaster.FK_GroupID = objUser.RoleId;
                        umaster.IsActive = objUser.IsActive;
                        umaster.CreatedBy = Convert.ToString(LoginUserID);
                        umaster.CreatedDateTime = DateTime.Now;
                        umaster.FK_StatusID = objUser.IsActive ? (int)UserStatus.Active : (int)UserStatus.Inactive;                        
                        if (objUser.LicenseClassID > 0)
                            umaster.FK_LicenseClassID = objUser.LicenseClassID;

                        context.FAUserMaster.Add(umaster);
                    }
                     
                    if (selectedFailities != null && selectedFailities.Count() > 0)
                    {
                        foreach (FacilityUserRelation moduleid in selectedFailities)
                        {
                            FAUserAndFacilityAccess fufAccess = new FAUserAndFacilityAccess();

                            fufAccess.FK_UserID = objUser.UserId;
                            fufAccess.FK_StoreRowID = moduleid.FacilityID;
                            fufAccess.FK_ApplicationID = moduleid.ApplicationTypeID;
                            if (moduleid.ApplicationTypeID == (int)Entities.Enums.Application.DriverApp)
                            {
                                fufAccess.StartDate = DateTime.Now;
                                fufAccess.FK_AccessLevelID = (int)AccessLevel.PrimaryFacility;
                            }
                            else
                            {
                                fufAccess.FK_AccessLevelID = moduleid.AccessLevelID;
                            }
                            fufAccess.CreatedBy = LoginUserID;
                            fufAccess.CreatedDate = DateTime.Now;
                            fufAccess.IsActive = true;


                            context.FAUserAndFacilityAccess.Add(fufAccess);
                        }
                    }

                    context.SaveChanges();

                    scope.Complete();
                    scope.Dispose();
                }
            }
        }

        public bool CheckUserName(string UserName, int UserId)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FAUserMaster.Any(x => x.Username == UserName && x.PK_UserId != UserId);
            }
        }

         

        public List<Entities.UserManagement.FADTOFacility> GetDTOFacilityList()
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var distinctRegions = GetFacilitiesList().Where(y => string.IsNullOrEmpty(y.Region) == false).Distinct();

                return distinctRegions.Select(y => new Entities.UserManagement.FADTOFacility
                {
                    CompDBAName = y.CompDBAName,
                    FriendlyName = y.FriendlyName,
                    Region = y.Region,
                    StoreRowID = y.StoreRowID,
                    StoreNo = y.StoreNo,
                }).OrderBy(x => x.CompDBAName).ToList();
            }
        }

       

        public void ToggleUserStatus(int UserId, string Status, int LoginUserID)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                FAUserMaster userMaster = context.FAUserMaster.Where(x => x.PK_UserId == UserId).FirstOrDefault();
                userMaster.IsActive = Status == "InActive" ? false : true;
                userMaster.FK_StatusID = userMaster.IsActive ? (int)UserStatus.Active : (int)UserStatus.Inactive;
                userMaster.UpdatedBy = Convert.ToString(LoginUserID);
                userMaster.UpdatedDateTime = DateTime.Now;

                context.SaveChanges();
            }
        }

        public List<RDFacility> GetFacilitiesList()
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return (from fc in context.RDFacility
                        where fc.StatusRowID >= 65 && fc.StoreType >= 10
                        select fc).Distinct().OrderBy(f => f.Region).ToList();
            }
        }

        public List<FAModule> ListFAModule()
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return (from fm in context.FAModule
                        where fm.IsActive == true
                        select fm).Distinct().ToList();
            }
        }
 

        public FAPasswordConfig GetPasswordConfig()
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FAPasswordConfig.Where(x => x.Status == true).FirstOrDefault();
            }
        }

        public bool CheckOldPassword(int UserID, string Password)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FAUserMaster.Any(x => x.PK_UserId == UserID && x.Password == Password);
            }
            //Need to implement it as the password is encrypted, need to take clarity on this.
            // return true;
        }

        public bool IsPasswordAlreadyUsed(int UserID, string NewPassword, int LastNumberOfPasswords)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FAUserPasswordHistory.Where(x => x.FK_UserId == UserID).OrderByDescending(y => y.CreatedDateTime).Take(LastNumberOfPasswords).Any(y => y.Password == NewPassword);
            }
        }

        public void UpdateNewPassword(int UserID, string NewPassword, string oldPassword, bool IsAutoPassword)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                FAUserMaster umaster = context.FAUserMaster.Where(x => x.PK_UserId == UserID).FirstOrDefault();
                if (umaster != null)
                {
                    //as we need to log only User Remembered passwords, so we are exempting logging of Forgot Password changes by Admin into history
                    if (umaster.IsChangePasswordRequired == false && IsAutoPassword == false)
                    {
                        FAUserPasswordHistory passHistory = new FAUserPasswordHistory()
                        {
                            FK_UserId = UserID,
                            Password = oldPassword,
                            CreatedBy = UserID.ToString(),
                            CreatedDateTime = DateTime.Now
                        };
                        context.FAUserPasswordHistory.Add(passHistory);
                    }
                    if (IsAutoPassword)
                    {
                        umaster.IsChangePasswordRequired = true;

                        FAUserPasswordHistory passHistory = new FAUserPasswordHistory()
                        {
                            FK_UserId = UserID,
                            Password = oldPassword,
                            CreatedBy = UserID.ToString(),
                            CreatedDateTime = DateTime.Now
                        };
                        context.FAUserPasswordHistory.Add(passHistory);
                    }
                    else
                    {
                        umaster.IsChangePasswordRequired = false;
                    }
                    umaster.Password = NewPassword;
                    umaster.PasswordUpdatedDate = DateTime.Now;
                    context.SaveChanges();
                }
            }
        }

        public FAUserMaster GetUserDetailsByUserName(string UserName)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FAUserMaster.Where(x => x.Username == UserName).FirstOrDefault();
            }
        }

        public FAUserMaster GetUserDetailsByDriverId(int DriverID)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FAUserMaster.Where(x => x.UniqueID == DriverID).FirstOrDefault();
            }
        }

        public FAUserMaster GetUserDetailsByUserId(int UserID)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                FAUserMaster oFAUserMaster = new FAUserMaster();
                var  userDetails = context.FAUserMaster.Where(x => x.PK_UserId == UserID).FirstOrDefault();

                if (userDetails != null)
                {
                    oFAUserMaster = new FAUserMaster
                    {
                        PK_UserId = userDetails.PK_UserId,
                        FirstName = userDetails.FirstName,
                        LastName = userDetails.LastName,
                        AddressLine1 = userDetails.AddressLine1,
                        AddressLine2 = userDetails.AddressLine2,
                        Password = userDetails.Password,
                        State = userDetails.State,
                        City = userDetails.City,
                        ZipCode = userDetails.ZipCode,
                        EmailAddress = userDetails.EmailAddress,
                        Username = userDetails.Username,
                        IsActive = userDetails.IsActive,
                        FK_GroupID = userDetails.FK_GroupID,                       
                        FAGroupMaster = userDetails.FAGroupMaster
                    };
                }

                return userDetails;
            }
        }

        #region Driver Maintenance

        public List<DriverInfoWithWeekSchedule> GetDriversScheduleHrsFromSP(DateTime Startdate, DateTime EndDate, string LocationCode)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                List<DriverInfoWithWeekSchedule> oListFADTODriverSchedule = new List<DriverInfoWithWeekSchedule>();


                var oDriverWeekScheduleList = context.Database.SqlQuery<DriverWeekSchedule>(@"exec USP_FA_GetDriverAvailability @FromDate, @ToDate, @LocationCode",
                                                Helper.SqlParameterObj("FromDate", Startdate),
                                                Helper.SqlParameterObj("ToDate", EndDate),
                                                Helper.SqlParameterObj("LocationCode", LocationCode)).ToList();

                List<int> driverIDs = oDriverWeekScheduleList.OrderBy(s => s.FirstName).Select(t => t.LLogisticsUserID).Distinct().ToList();

                foreach (var driverid in driverIDs)
                {
                    var driverInfo = oDriverWeekScheduleList.Where(d => d.LLogisticsUserID == driverid).FirstOrDefault();

                    oListFADTODriverSchedule.Add(new DriverInfoWithWeekSchedule
                    {
                        FacilityName = driverInfo.FacilityName,
                        FirstName = driverInfo.FirstName,
                        LastName = driverInfo.LastName,
                        LicenseName = driverInfo.LicenseName,
                        PK_ID = driverid,
                        Day1 = ReturnDTODriverScheduleFromSP(oDriverWeekScheduleList, Startdate, 0, driverid),
                        Day2 = ReturnDTODriverScheduleFromSP(oDriverWeekScheduleList, Startdate, 1, driverid),
                        Day3 = ReturnDTODriverScheduleFromSP(oDriverWeekScheduleList, Startdate, 2, driverid),
                        Day4 = ReturnDTODriverScheduleFromSP(oDriverWeekScheduleList, Startdate, 3, driverid),
                        Day5 = ReturnDTODriverScheduleFromSP(oDriverWeekScheduleList, Startdate, 4, driverid),
                        Day6 = ReturnDTODriverScheduleFromSP(oDriverWeekScheduleList, Startdate, 5, driverid),
                        Day7 = ReturnDTODriverScheduleFromSP(oDriverWeekScheduleList, Startdate, 6, driverid)
                    });
                }

                return oListFADTODriverSchedule;
            }
        }

        private FADTODriverSchedule ReturnDTODriverScheduleFromSP(List<DriverWeekSchedule> objDriverSchedule, DateTime StartDate, int Index, int driverId)
        {
            FADTODriverSchedule oFADTODriverSchedule = new FADTODriverSchedule();

            var dateWiseDriverSchedule = objDriverSchedule.Where(x => x.LLogisticsUserID == driverId && x.ScheduleDate.Date == StartDate.AddDays(Index).Date).FirstOrDefault();

            oFADTODriverSchedule = new FADTODriverSchedule
            {
                DriverScheduleID = dateWiseDriverSchedule.DriverScheduleID,
                DayWork = dateWiseDriverSchedule.DayWork,
                ScheduleDate = dateWiseDriverSchedule.ScheduleDate,
                ScheduleEndTime = dateWiseDriverSchedule.ScheduleEndTime,
                ScheduleStartTime = dateWiseDriverSchedule.ScheduleStartTime,
                UserFacilityAccessStatus = dateWiseDriverSchedule.UserFacilityAccessStatus
            };

            return oFADTODriverSchedule;
        }

        private FADTODriverSchedule ReturnDTODriverSchedule(List<FADriverSchedule> objDriverSchedule, DateTime StartDate, int Index, DateTime? DriverStartDate, DateTime? DriverEndDate)
        {
            FADTODriverSchedule oFADTODriverSchedule = new FADTODriverSchedule();
            StartDate = StartDate.AddDays(Index);

            var dateWiseDriverSchedule = objDriverSchedule.Where(x => x.Status == true && x.ScheduleDate.Date == StartDate.Date).FirstOrDefault();
            if (dateWiseDriverSchedule == null)
                dateWiseDriverSchedule = new FADriverSchedule();

            oFADTODriverSchedule = new FADTODriverSchedule
            {
                DriverScheduleID = dateWiseDriverSchedule.PK_ID,
                DayWork = dateWiseDriverSchedule.DayWork, 
                ScheduleDate = StartDate,
                ScheduleEndTime = dateWiseDriverSchedule.ScheduleEndTime,
                ScheduleStartTime = dateWiseDriverSchedule.ScheduleStartTime,
                DriverStartDate = DriverStartDate,
                DriverEndDate = DriverEndDate
            };

            return oFADTODriverSchedule;
        }

        public void SaveDriverScheduleHours(List<DriverScheduleHour> lstDriverScheduleHours, int userID)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
                                                                                                new TransactionOptions
                                                                                                {
                                                                                                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                                                                                }))
            {
                using (var context = new PRSEntities())
                {
                    context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    foreach (var driverScheduleHour in lstDriverScheduleHours)
                    {
                        FADriverSchedule dbDriverHrs = context.FADriverSchedule
                            .Where(t => t.Status == true && t.FK_UserID == driverScheduleHour.DriverID &&
                                (t.ScheduleDate != null && (t.ScheduleDate.Year == driverScheduleHour.ScheduleDate.Year &&
                                t.ScheduleDate.Month == driverScheduleHour.ScheduleDate.Month &&
                                t.ScheduleDate.Day == driverScheduleHour.ScheduleDate.Day)))
                                .FirstOrDefault();

                        if (dbDriverHrs != null)
                        {
                            dbDriverHrs.Status = false;
                            dbDriverHrs.UpdatedBy = userID;
                            dbDriverHrs.UpdatedDate = DateTime.Now;
                        }

                        FADriverSchedule newDriverSchedule = new FADriverSchedule
                        {
                            FK_UserID = driverScheduleHour.DriverID,
                            ScheduleDate = driverScheduleHour.ScheduleDate,
                            ScheduleStartTime = driverScheduleHour.ScheduleStartTimeTS,
                            ScheduleEndTime = driverScheduleHour.ScheduleEndTimeTS,
                            DayWork = driverScheduleHour.DayWork,
                            CreatedDate = DateTime.Now,
                            CreatedBy = userID,
                            Status = true
                        };

                        context.FADriverSchedule.Add(newDriverSchedule);
                    }

                    context.SaveChanges();
                }

                scope.Complete();
                scope.Dispose();
            }
        }

        public bool CanScheduleDriverHoursForThisDay(DateTime scheduleDate, int DriverId, int StoreNo)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                DateTime maxDate = DateTime.MaxValue;
                var isThisDriverWorkingWithThisFacility = (from usersandfacilities in context.FAUserAndFacilityAccess
                                                           join f in context.RDFacility on usersandfacilities.FK_StoreRowID equals f.StoreRowID
                                                           where f.StoreNo == StoreNo && usersandfacilities.IsActive == true && usersandfacilities.FK_UserID == DriverId
                                                           && DbFunctions.TruncateTime(usersandfacilities.StartDate) <= DbFunctions.TruncateTime(scheduleDate.Date) && DbFunctions.TruncateTime(scheduleDate.Date) <= (usersandfacilities.EndDate.HasValue ? DbFunctions.TruncateTime(usersandfacilities.EndDate.Value) : DbFunctions.TruncateTime(maxDate))
                                                           && (usersandfacilities.FK_AccessLevelID == (int)AccessLevel.PrimaryFacility || usersandfacilities.FK_AccessLevelID == (int)AccessLevel.Rover)
                                                           && usersandfacilities.FK_ApplicationID == (int)Application.DriverApp
                                                           && !(context.FAUserAndFacilityAccess.Any(r =>
                                                                                                       r.FK_AccessLevelID == (int)AccessLevel.Rover
                                                                                                       && r.FK_ApplicationID == (int)Application.DriverApp
                                                                                                       && r.IsActive == true
                                                                                                       && r.FK_UserID == DriverId
                                                                                                       && r.FK_StoreRowID != usersandfacilities.FK_StoreRowID
                                                                                                       && DbFunctions.TruncateTime(r.StartDate) <= DbFunctions.TruncateTime(scheduleDate.Date)
                                                                                                       && DbFunctions.TruncateTime(scheduleDate.Date) <= (r.EndDate.HasValue ? DbFunctions.TruncateTime(r.EndDate.Value) : DbFunctions.TruncateTime(maxDate))))
                                                           select usersandfacilities
                                                           ).Any();

                return isThisDriverWorkingWithThisFacility;
            }
        }

        public List<FADTOUserStatus> GetUserStatus()
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FAUserStatus.Where(s => s.IsActive).Select(s =>
                                                                                new FADTOUserStatus
                                                                                {
                                                                                    PK_ID = s.PK_ID,
                                                                                    StatusName = s.StatusName,
                                                                                    CreatedDate = s.CreatedDate
                                                                                }).ToList();
            }
        }

        public List<FADTOLicenseClass> GetLicenseClass()
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FALicenseClass.Where(s => s.IsActive).Select(s =>
                                                                                new FADTOLicenseClass
                                                                                {
                                                                                    PK_ID = s.PK_ID,
                                                                                    Name = s.Name,
                                                                                    Description = s.Description,
                                                                                    CreatedDate = s.CreatedDate
                                                                                }).ToList();
            }
        }

        public int GetStoreRowID(string LocationCode)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.RDFacility.Where(s => s.SLLocCode == LocationCode).Select(f => f.StoreRowID).FirstOrDefault();
            }
        }

        public bool checkUserNameAvailable(string UserName)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return !context.FAUserMaster.Any(s => s.Username == UserName);
            }
        }

        public List<string> suggestedUserName(string FirstName, string LastName)
        {
            //Need to do code refractoring, if we don't get any further modifications for suggestable usernames.
            List<string> suggestableNames = new List<string> { FirstName.Substring(0, 1) + LastName };
            List<string> returnSuggestableNames = new List<string>();

            foreach (string s in suggestableNames)
            {
                if (checkUserNameAvailable(s))
                {
                    returnSuggestableNames.Add(s);
                }
            }

            if (returnSuggestableNames.Count < 5)
            {
                int count = 1;
                string loopUserName = string.Empty;
                while (returnSuggestableNames.Count < 5)
                {
                    loopUserName = FirstName.Substring(0, 1) + LastName + count.ToString();
                    if (checkUserNameAvailable(loopUserName))
                    {
                        returnSuggestableNames.Add(loopUserName);
                    }
                    count++;
                }
            }

            return returnSuggestableNames;
        }

        public void SaveDriverDetails(SaveDriverRequest driverForm, int userID)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
                                                                                                new TransactionOptions
                                                                                                {
                                                                                                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                                                                                }))
            {
                using (var context = new PRSEntities())
                {
                    context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    var userMaster = context.FAUserMaster.Find(driverForm.PK_UserId);

                    if (userMaster != null)
                    {
                        userMaster.FirstName = driverForm.FirstName;
                        userMaster.LastName = driverForm.LastName;
                        userMaster.IsActive = driverForm.IsStatusActive;
                        userMaster.FK_LicenseClassID = driverForm.LicenseClassID;
                        userMaster.FK_StatusID = (int)driverForm.StatusID;
                        userMaster.Password = driverForm.EncryptPassword;
                        userMaster.EmailAddress = driverForm.EmailAddress; 
                    }
                    else
                    {
                        userMaster = new FAUserMaster
                        {
                            FirstName = driverForm.FirstName,
                            LastName = driverForm.LastName,
                            IsActive = driverForm.IsStatusActive,
                            FK_GroupID = driverForm.GroupMasterID,
                            FK_LicenseClassID = driverForm.LicenseClassID,
                            FK_StatusID = (int)driverForm.StatusID,
                            Username = driverForm.UserName,
                            Password = driverForm.EncryptPassword,
                            EmailAddress = driverForm.EmailAddress,
                            CreatedBy = userID.ToString(),
                            CreatedDateTime = DateTime.Now,
                            IsChangePasswordRequired = false,
                            ActivatedDateTime = DateTime.Now
                        };

                        foreach (var df in driverForm.DriverFacility)
                        {
                            userMaster.FAUserAndFacilityAccess.Add(new FAUserAndFacilityAccess
                            {
                                FK_StoreRowID = df.UserFacilityID,
                                FK_AccessLevelID = (int)AccessLevel.PrimaryFacility,
                                FK_ApplicationID = (int)Application.DriverApp,
                                IsActive = true,
                                StartDate = DateTime.Now,
                                EndDate = null,
                                CreatedDate = DateTime.Now,
                                CreatedBy = userID
                            });
                        }

                        context.FAUserMaster.Add(userMaster);
                    }

                    context.SaveChanges();
                }

                scope.Complete();
                scope.Dispose();
            }
        }

        public void SaveDriverRoverAndPermanentTransfer(FADTOUserAndFacilityAccess driverTransferDetails, int UserId)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
                                                                                                new TransactionOptions
                                                                                                {
                                                                                                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                                                                                }))
            {
                using (var context = new PRSEntities())
                {
                    context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                  
                    if (driverTransferDetails.FK_AccessLevelID == (int)AccessLevel.PrimaryFacility)
                    {
                        var transferRecord = context.FAUserAndFacilityAccess.Where(dfdb => dfdb.FK_UserID == driverTransferDetails.FK_UserID
                        && dfdb.FK_StoreRowID == driverTransferDetails.FromFacilityID && dfdb.IsActive == true
                        && dfdb.FK_AccessLevelID == driverTransferDetails.FK_AccessLevelID).FirstOrDefault();

                        if (transferRecord != null)
                        {
                            transferRecord.EndDate = driverTransferDetails.StartDate.AddDays(-1);
                            transferRecord.UpdatedDate = DateTime.Now;
                            transferRecord.UpdatedBy = UserId;
                        }

                        FAUserAndFacilityAccess transferredRecord = new FAUserAndFacilityAccess()
                        {
                            FK_UserID = driverTransferDetails.FK_UserID,
                            FK_StoreRowID = driverTransferDetails.ToFacilityID,
                            FK_AccessLevelID = (int)AccessLevel.PrimaryFacility,
                            FK_ApplicationID = (int)Application.DriverApp,
                            FK_TransferredStoreRowID = driverTransferDetails.FromFacilityID,
                            IsActive = true,
                            StartDate = driverTransferDetails.StartDate,
                            EndDate = null,
                            CreatedDate = DateTime.Now,
                            CreatedBy = UserId
                        };

                        context.FAUserAndFacilityAccess.Add(transferredRecord);

                    }
                    else if (driverTransferDetails.FK_AccessLevelID == (int)AccessLevel.Rover)
                    {
                        FAUserAndFacilityAccess transferredRecord = new FAUserAndFacilityAccess()
                        {
                            FK_UserID = driverTransferDetails.FK_UserID,
                            FK_StoreRowID = driverTransferDetails.ToFacilityID,
                            FK_AccessLevelID = (int)AccessLevel.Rover,
                            FK_ApplicationID = (int)Application.DriverApp,
                            FK_TransferredStoreRowID = driverTransferDetails.FromFacilityID,
                            IsActive = true,
                            StartDate = driverTransferDetails.StartDate,
                            EndDate = driverTransferDetails.EndDate,
                            CreatedDate = DateTime.Now,
                            CreatedBy = UserId
                        };

                        context.FAUserAndFacilityAccess.Add(transferredRecord);
                    }

                    context.SaveChanges();
                }
                scope.Complete();
                scope.Dispose();
            }
        }


        /// <summary>
        /// If transfer driver site info is not a current siteinfo then we should not allow him to transfer to other facility including Rover and Permanent transfer
        /// </summary>
        /// <param name="driverID"></param>
        /// <param name="currentSiteInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public bool CanThisUserTransferThisDriverForRelocation(int driverID, SiteInfo currentSiteInfo, DateTime startDate, DateTime? endDate)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                DateTime maxdate = DateTime.MaxValue.Date;
                var result = (from usersandfacilities in context.FAUserAndFacilityAccess
                              join facility in context.RDFacility on usersandfacilities.FK_StoreRowID equals facility.StoreRowID
                              where
                                usersandfacilities.FK_ApplicationID == (int)Application.DriverApp
                                && usersandfacilities.FK_UserID == driverID
                                && usersandfacilities.FK_AccessLevelID == (int)AccessLevel.PrimaryFacility
                                && facility.SLLocCode == currentSiteInfo.LocationCode
                                && ((endDate.HasValue == false &&
                                    ((DbFunctions.TruncateTime(usersandfacilities.StartDate.Value) <= DbFunctions.TruncateTime(startDate) && !usersandfacilities.EndDate.HasValue)))
                                    ||
                                    (endDate.HasValue == true &&
                                        (DbFunctions.TruncateTime(usersandfacilities.StartDate.Value) <= DbFunctions.TruncateTime(startDate) &&
                                             ((usersandfacilities.EndDate.HasValue ? DbFunctions.TruncateTime(usersandfacilities.EndDate.Value) : DbFunctions.TruncateTime(maxdate)) >= DbFunctions.TruncateTime(endDate.Value))))
                                )
                              select facility).Any();


                return result;
            }
        }

        public bool CanThisUserTransferThisDriverForRover(FADTOUserAndFacilityAccess driverTransferDetails, SiteInfo currentSiteInfo)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return !context.FAUserAndFacilityAccess.Any(dfdb => dfdb.FK_UserID == driverTransferDetails.FK_UserID
                          && dfdb.IsActive == true
                          && dfdb.FK_AccessLevelID == driverTransferDetails.FK_AccessLevelID
                          && (DbFunctions.TruncateTime(dfdb.StartDate.Value) <= driverTransferDetails.EndDate.Value)
                          && (DbFunctions.TruncateTime(dfdb.EndDate.Value) >= driverTransferDetails.StartDate.Date));
            }
        }
 
        public List<Entities.UserManagement.FADTOFacility> GetDTODriverFacilityList(int driverID)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var driverFacilities = from userandFacilities in context.FAUserAndFacilityAccess
                                       join facility in context.RDFacility on userandFacilities.FK_StoreRowID equals facility.StoreRowID
                                       where userandFacilities.IsActive == true
                                       && userandFacilities.FK_ApplicationID == (int)Entities.Enums.Application.DriverApp
                                       && userandFacilities.FK_UserID == driverID
                                       && facility.StoreType >= 10 && facility.StatusRowID >= 65
                                       select new Entities.UserManagement.FADTOFacility
                                       {
                                           FriendlyName = facility.FriendlyName,
                                           CompDBAName = facility.CompDBAName,
                                           StoreRowID = facility.StoreRowID,
                                           DAAccessLevel = (Entities.Enums.AccessLevel)userandFacilities.FK_AccessLevelID,
                                           DriverStartDate = userandFacilities.StartDate.HasValue ? (DateTime?)userandFacilities.StartDate.Value : null,
                                           DriverEndDate = userandFacilities.EndDate.HasValue ? (DateTime?)userandFacilities.EndDate.Value : null
                                       };


                return driverFacilities.ToList();
            }
        }

        public List<FADTOUserMaster> GetDuplicateUsersByLastname(string LastName, DateTime StartDate)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var newdriverFacilities = (from user in context.FAUserMaster
                                           join primaryFacility in context.FAUserAndFacilityAccess on user.PK_UserId equals primaryFacility.FK_UserID into primaryFacility
                                           where user.LastName.Equals(LastName, StringComparison.OrdinalIgnoreCase)
                                           select new FADTOUserMaster
                                           {
                                               PK_UserId = user.PK_UserId,
                                               FirstName = user.FirstName,
                                               LastName = user.LastName,
                                               Username = user.Username,
                                               IsActive = user.IsActive,
                                               StatusName = user.FAUserStatus.StatusName,
                                               IsDriverApp = user.FAGroupMaster.FAGroupAndModule.Any(x => x.FAModule.FA_ModuleArea.FK_ApplicationID == (int)Application.DriverApp),
                                               PrimaryFacility = (from pf in primaryFacility
                                                                  join facility in context.RDFacility on pf.FK_StoreRowID equals facility.StoreRowID
                                                                  where facility.StoreType >= 10 && facility.StatusRowID >= 65
                                                                  && pf.IsActive == true
                                                                  && pf.FK_ApplicationID == (int)Application.DriverApp
                                                                  && pf.FK_AccessLevelID == (int)AccessLevel.PrimaryFacility
                                                                  && DbFunctions.TruncateTime(pf.StartDate) <= StartDate.Date && StartDate.Date <= (pf.EndDate.HasValue ? DbFunctions.TruncateTime(pf.EndDate) : DateTime.MaxValue)
                                                                  select new FADTOFacility
                                                                  {
                                                                      FriendlyName = facility.FriendlyName,
                                                                      CompDBAName = facility.CompDBAName,
                                                                      StoreRowID = facility.StoreRowID,
                                                                      StoreNo = facility.StoreNo,
                                                                      DAAccessLevel = (Entities.Enums.AccessLevel)pf.FK_AccessLevelID,
                                                                      DriverStartDate = pf.StartDate.Value
                                                                  }).FirstOrDefault()
                                           }).ToList();

                return newdriverFacilities.OrderBy(d => d.StatusName).ThenBy(d => d.FirstName).ToList();
            }
        }

        public bool AssingDriverToFacility(int driverId, int storeNo, int createdUserId)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                bool recordAdded = false;

                var storeRowId = context.RDFacility.Where(facility => facility.StoreNo == storeNo && facility.StoreType >= 10 && facility.StatusRowID >= 65).Select(f => f.StoreRowID).First();

                if (storeRowId > 0)
                {
                    FAUserAndFacilityAccess ufa = new FAUserAndFacilityAccess
                    {
                        FK_UserID = driverId,
                        FK_ApplicationID = (int)Application.DriverApp,
                        FK_AccessLevelID = (int)AccessLevel.PrimaryFacility,
                        StartDate = DateTime.Now,
                        FK_StoreRowID = storeRowId,
                        CreatedBy = createdUserId,
                        CreatedDate = DateTime.Now,
                        IsActive = true
                    };

                    context.FAUserAndFacilityAccess.Add(ufa);

                    context.SaveChanges();

                    recordAdded = true;
                }

                return recordAdded;
            }
        }

        private FADTOFacility RetPrimaryFacility(int UserId)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return (from userandFacilities in context.FAUserAndFacilityAccess
                        join facility in context.RDFacility on userandFacilities.FK_StoreRowID equals facility.StoreRowID
                        where userandFacilities.IsActive == true
                        && userandFacilities.FK_ApplicationID == (int)Entities.Enums.Application.DriverApp
                        && (Entities.Enums.AccessLevel)userandFacilities.FK_AccessLevelID == AccessLevel.PrimaryFacility
                        && userandFacilities.FK_UserID == UserId
                        && facility.StoreType >= 10 && facility.StatusRowID >= 65
                        select new FADTOFacility
                        {
                            FriendlyName = facility.FriendlyName,
                            CompDBAName = facility.CompDBAName,
                            StoreRowID = facility.StoreRowID,
                            DAAccessLevel = (Entities.Enums.AccessLevel)userandFacilities.FK_AccessLevelID,
                            DriverStartDate = userandFacilities.StartDate.Value
                        }).FirstOrDefault();
            }
        }

        public List<DriverScheduleHour> GetDriverScheduleHourDetails(int facilityStoreNo, DateTime startDate, int noOfDays, int driverID, int weekday)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                DateTime endDate = startDate.AddDays(noOfDays);
                return context.Database.SqlQuery<DriverScheduleHour>(@"exec USP_FA_GetDriverScheduleHours @StartDate, @EndDate, @StoreNumber, @DriverID, @DATEPART",
                    Helper.SqlParameterObj("StartDate", startDate),
                    Helper.SqlParameterObj("EndDate", endDate),
                    Helper.SqlParameterObj("StoreNumber", facilityStoreNo),
                    Helper.SqlParameterObj("DriverID", driverID),
                    Helper.SqlParameterObj("DATEPART", weekday)).AsQueryable().ToList();
            }
        }

        #endregion

        public List<DriverFacility> GetDriverRoverFacilityList(string locationCode, int UserID)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                List<Entities.DriverSchedule.DriverFacility> lstFacilities = new List<Entities.DriverSchedule.DriverFacility>();

                lstFacilities.AddRange((from facility in context.RDFacility
                                        join df in context.FAUserAndFacilityAccess on facility.StoreRowID equals df.FK_StoreRowID
                                        where df.FK_UserID == UserID && facility.StoreType >= 10 && facility.StatusRowID >= 65
                                        && df.FK_AccessLevelID == (int)Entities.Enums.AccessLevel.Rover
                                        && df.FK_ApplicationID == (int)Entities.Enums.Application.DriverApp
                                        && df.IsActive == true
                                        select new Entities.DriverSchedule.DriverFacility
                                        {
                                            DriverId = UserID,
                                            UserFacilityID = df.PK_ID,
                                            FriendlyName = facility.FriendlyName,
                                            CompDBAName = facility.CompDBAName,
                                            StoreRowID = facility.StoreRowID,
                                            DriverStartDate = df.StartDate,
                                            DriverEndDate = df.EndDate,
                                            Region = facility.Region,
                                            FK_ApplicationTypeID = (int)Entities.Enums.Application.DriverApp,
                                            StoreNo = facility.StoreNo,
                                            MarketID = facility.FK_MarketId,
                                            DAAccessLevel = (Entities.Enums.AccessLevel)df.FK_AccessLevelID,
                                            DFStatus = DriverFacilityStatus.None
                                        }).OrderByDescending(f => f.DriverStartDate).ToList());

                IEnumerable<FADTOFacility> FacilityList = GetDTOFacilityList().AsEnumerable();

                //Update index and accessable facility list
                int index = -1;
                lstFacilities.ForEach(f =>
                {
                    f.Index = ++index;
                    f.AccessableFacilityList = FacilityList;
                });

                return lstFacilities;
            }
        }

        public SaveRoverStatus CanSaveRoverRecord(DriverFacility roverFacility, int CurrentStoreNo)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                DateTime StartDate = roverFacility.DriverStartDate.Value.Date;
                DateTime EndDate = roverFacility.DriverEndDate.Value.Date;
                DateTime maxdate = DateTime.MaxValue.Date;
                SaveRoverStatus oSaveRoverStatus = SaveRoverStatus.CanSave;

                bool isHePrimaryFacilityForThisDriverForGivenDates = (from usersandfacilities in context.FAUserAndFacilityAccess
                                                                      join facility in context.RDFacility on usersandfacilities.FK_StoreRowID equals facility.StoreRowID
                                                                      where 
                                                                      (DbFunctions.TruncateTime(usersandfacilities.StartDate.Value) <= DbFunctions.TruncateTime(StartDate) && ((usersandfacilities.EndDate.HasValue ? DbFunctions.TruncateTime(usersandfacilities.EndDate.Value) : DbFunctions.TruncateTime(maxdate)) >= DbFunctions.TruncateTime(EndDate)))
                                                                       && usersandfacilities.FK_UserID == roverFacility.DriverId
                                                                       && usersandfacilities.IsActive == true
                                                                       && usersandfacilities.FK_AccessLevelID == (int)AccessLevel.PrimaryFacility
                                                                       && usersandfacilities.FK_ApplicationID == (int)Application.DriverApp
                                                                       && facility.StoreNo == CurrentStoreNo
                                                                      select usersandfacilities).Any();

                if (isHePrimaryFacilityForThisDriverForGivenDates == false)
                {
                    oSaveRoverStatus = SaveRoverStatus.NotPrimaryFacility;
                }
                else
                {
                    bool uaf = (from usersandfacilities in context.FAUserAndFacilityAccess
                                where DbFunctions.TruncateTime(usersandfacilities.StartDate.Value) <= roverFacility.DriverEndDate.Value
                                        && (DbFunctions.TruncateTime(usersandfacilities.EndDate.Value) >= roverFacility.DriverStartDate.Value)
                                        && usersandfacilities.FK_UserID == roverFacility.DriverId
                                        && usersandfacilities.IsActive == true
                                        && usersandfacilities.FK_AccessLevelID == (int)AccessLevel.Rover
                                        && usersandfacilities.FK_ApplicationID == (int)Application.DriverApp
                                        && usersandfacilities.PK_ID != roverFacility.UserFacilityID
                                select usersandfacilities).Any();

                    if (uaf)
                    {
                        oSaveRoverStatus = SaveRoverStatus.DatesOverLap;
                    }
                }

                return oSaveRoverStatus;
            }
        }

        public void UpdateDriverRoverFacilityInfo(DriverFacility roverFacility, int CurrentUserID)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
                                                                                                new TransactionOptions
                                                                                                {
                                                                                                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                                                                                }))
            {
                using (var context = new PRSEntities())
                {
                    context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    var dbRoverfacility = context.FAUserAndFacilityAccess.Find(roverFacility.UserFacilityID);

                    if (roverFacility == null)
                    {
                        throw new Exception("There no facility exists in the system, please double check and update this facility.");
                    }
                    else
                    {
                        bool doNeedToInsertNewRecord = true;

                        ///When deleting future driver facility, then we dont insert new entry.
                        ///If the record is future dated record then make it inactive and dont insert new entry for this update
                        if (roverFacility.DFStatus == DriverFacilityStatus.Deleted && roverFacility.DriverStartDate.Value.Date >= DateTime.Now.Date)
                            doNeedToInsertNewRecord = false;

                        //Before you update existing rover facility, please inactive existing record and then insert new record
                        dbRoverfacility.UpdatedBy = CurrentUserID;
                        dbRoverfacility.UpdatedDate = DateTime.Now;
                        dbRoverfacility.IsActive = false;

                        if (doNeedToInsertNewRecord == true)
                        {
                            FAUserAndFacilityAccess newfa = new FAUserAndFacilityAccess
                            {
                                FK_UserID = dbRoverfacility.FK_UserID,
                                FK_StoreRowID = roverFacility.StoreRowID,
                                FK_AccessLevelID = dbRoverfacility.FK_AccessLevelID,
                                FK_ApplicationID = dbRoverfacility.FK_ApplicationID,
                                StartDate = roverFacility.DriverStartDate,
                                EndDate = roverFacility.DriverEndDate,
                                CreatedBy = CurrentUserID,
                                CreatedDate = DateTime.Now,
                                IsActive = true
                            };

                            context.FAUserAndFacilityAccess.Add(newfa);
                        }
                        context.SaveChanges();
                    }
                }
                scope.Complete();
                scope.Dispose();
            }
        }
    }
}