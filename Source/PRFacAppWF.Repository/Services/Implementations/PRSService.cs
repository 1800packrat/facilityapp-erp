﻿namespace PRFacAppWF.Repository.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using PRFacAppWF.Repository.Helpers;
    using PRFacAppWF.models;
    using PR.LocalLogisticsSolution.Model;
    using PRFacAppWF.Repository.models;
    using PRFacAppWF.Entities.Touches;
    using PRFacAppWF.Entities.Enums;
    using PRFacAppWF.Entities;
    using System.Data;
    using System.Data.SqlClient;

    public class PRSService : CacheableService, IPRSService
    {
        private readonly PRSEntities prsEntities;
        protected const string AllFacilityUnits = "All_FacilityUnits";

        public PRSService()
        {
            if (this.prsEntities == null)
                this.prsEntities = new PRSEntities();
        }


        public IQueryable<TruckStatus> GetTruckStatus()
        {
            return this.prsEntities.TruckStatus.AsQueryable();
        }

        public List<Trucks> GetTruckDetails(string StoreNumbers, DateTime date)
        {
            return this.prsEntities.Database.SqlQuery<Trucks>("exec USP_GetTruckDetails @StoreNumbers, @Date", Helper.SqlParameterObj("StoreNumbers", StoreNumbers), Helper.SqlParameterObj("Date", date)).ToList();
        }

        public int CheckTruckStatusEntryExists(int TruckId)
        {
            var rtId = this.prsEntities.Database.SqlQuery<int>(@"exec USP_GetTruckStatusEntry @TruckId", Helper.SqlParameterObj("TruckId", TruckId)).Single();

            return rtId > 0 ? rtId : -1;
        }

        public int SaveTruckStatusDetails(TruckStatusDetails truckStatusDetails)
        {
            var rtId = this.prsEntities.Database.SqlQuery<int>(@"exec USP_AddTruckstatusDetails 
																		@TruckId,
																		@TruckStatusId,
																		@TruckNxtPreventiveDate,
																		@Comments,
																		@Createdby,
																		@TruckDetailsId,
																		@IsTruckTransfer,
																		@IsPermanentTransfer,
																		@TransferStoreNo,
																		@TransferValidFrom,
																		@TransferValidTo",
                                                                     Helper.SqlParameterObj("TruckId", truckStatusDetails.TruckNumber),
                                                                     Helper.SqlParameterObj("TruckStatusId", truckStatusDetails.TruckStatus),
                                                                     Helper.SqlParameterObj("TruckNxtPreventiveDate", truckStatusDetails.NextPreventiveMaintDate),
                                                                     Helper.SqlParameterObj("Comments", truckStatusDetails.Comments),
                                                                     Helper.SqlParameterObj("Createdby", truckStatusDetails.Createdby),
                                                                     Helper.SqlParameterObj("TruckDetailsId", truckStatusDetails.TruckDetailsId),
                                                                     Helper.SqlParameterObj("IsTruckTransfer", truckStatusDetails.IsFleetTransfer),
                                                                     Helper.SqlParameterObj("IsPermanentTransfer", !truckStatusDetails.IsTransferTemparory),
                                                                     Helper.SqlParameterObj("TransferStoreNo", truckStatusDetails.TransferStoreNo),
                                                                     Helper.SqlParameterObj("TransferValidFrom", truckStatusDetails.TransferValidFrom),
                                                                     Helper.SqlParameterObj("TransferValidTo", truckStatusDetails.TransferValidTo)
                                                                     ).Single();

            return rtId > 0 ? rtId : -1;
        }

        public IQueryable<TruckStatusInfo> GetTruckStatusDetailList(TruckStatusFilterCriteria truckStatusCriteria)
        {
            return this.prsEntities.Database.SqlQuery<TruckStatusInfo>(@"exec USP_GetTruckStatusReport 
																		@FromDate,
																		@ToDate,
																		@Facility,
																		@Status,
																		@TruckWithNoStatus",
                                                                     Helper.SqlParameterObj("FromDate", truckStatusCriteria.FromDate),
                                                                     Helper.SqlParameterObj("ToDate", truckStatusCriteria.ToDate),
                                                                     Helper.SqlParameterObj("Facility", truckStatusCriteria.Facility),
                                                                     Helper.SqlParameterObj("Status", truckStatusCriteria.TruckStatus),
                                                                     Helper.SqlParameterObj("TruckWithNoStatus", truckStatusCriteria.TruckWithNoStatus)).AsQueryable();
        }

        public IQueryable<TruckStatusInfo> GetTruckStatusDetailList(SearchFilter<TruckStatusFilterCriteria> truckStatusCriteria)
        {
            return this.prsEntities.Database.SqlQuery<TruckStatusInfo>(@"exec USP_GetTruckStatusReport 
																		@FromDate,
																		@ToDate,
																		@Facility,
																		@Status,
																		@TruckWithNoStatus,
																		@PageNo,
																		@RecordCntperPage,
																		@OrderBy,
																		@Order",
                                                                     Helper.SqlParameterObj("FromDate", truckStatusCriteria.filter.FromDate),
                                                                     Helper.SqlParameterObj("ToDate", truckStatusCriteria.filter.ToDate),
                                                                     Helper.SqlParameterObj("Facility", truckStatusCriteria.filter.Facility),
                                                                     Helper.SqlParameterObj("Status", truckStatusCriteria.filter.TruckStatus),
                                                                     Helper.SqlParameterObj("TruckWithNoStatus", truckStatusCriteria.filter.TruckWithNoStatus),
                                                                     Helper.SqlParameterObj("PageNo", truckStatusCriteria.page),
                                                                     Helper.SqlParameterObj("RecordCntperPage", truckStatusCriteria.rows),
                                                                     Helper.SqlParameterObj("OrderBy", truckStatusCriteria.sidx),
                                                                     Helper.SqlParameterObj("Order", truckStatusCriteria.sord)).AsQueryable();
        }

        public void SaveUnitAvaialability(UnitAvailabilityInfo uaInfo)
        {
            using (var context = new PRSEntities())
            {
                List<CAPUnitUnavailability> lstDBUnit = context.CAPUnitUnavailability.Where(u => u.FacilityId == uaInfo.StoreNo && u.UnitTypeId == uaInfo.UnitSize).ToList();

                CAPUnitUnavailability dbUnit = new CAPUnitUnavailability();

                // Need to handle when we get multiple records
                List<CAPUnitUnavailability> dbUnits = lstDBUnit.Where(u => ((u.Startdate >= uaInfo.FromDate && u.Startdate <= uaInfo.ThroughDate)
                     || (u.Enddate >= uaInfo.FromDate && u.Enddate <= uaInfo.ThroughDate)) ||
                     ((u.Startdate <= uaInfo.FromDate && uaInfo.FromDate <= u.Enddate)
                     || (u.Startdate <= uaInfo.ThroughDate && uaInfo.ThroughDate <= u.Enddate))).OrderBy(x => x.Startdate).ToList();

                if (dbUnits.Count > 1)
                {
                    throw new Exception("Please check the dates, the dates are confilcting with existing record.");
                }
                else
                {
                    dbUnit = dbUnits.FirstOrDefault();
                }

                //Update existing Unit
                if (dbUnit != null && dbUnit.CAPUnitUnavailabilityId == uaInfo.UnitAvailabilityInfoId)
                {
                    DateTime startDate = uaInfo.FromDate;
                    DateTime endDate = uaInfo.ThroughDate;

                    dbUnit.ReportedBy = uaInfo.ReportedBy;
                    dbUnit.UnitTypeId = uaInfo.UnitSize;
                    dbUnit.Startdate = startDate;
                    dbUnit.Enddate = endDate;
                    dbUnit.ModifiedBy = uaInfo.CreatedBy;
                    dbUnit.ModifiedOn = DateTime.Now;
                    dbUnit.Status = 1;
                }
                else if (dbUnit != null)
                {
                    throw new Exception("Please check the dates, the dates are confilcting with existing record.");
                }
                else //Add new Unit Avaialability
                {
                    dbUnit = new CAPUnitUnavailability
                    {
                        ReportedBy = uaInfo.ReportedBy,
                        UnitTypeId = uaInfo.UnitSize,
                        CreatedBy = uaInfo.CreatedBy,
                        Startdate = uaInfo.FromDate,
                        Enddate = uaInfo.ThroughDate,
                        FacilityId = uaInfo.StoreNo,
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = uaInfo.CreatedBy,
                        Status = 1
                    };

                    context.CAPUnitUnavailability.Add(dbUnit);

                }

                context.SaveChanges();
            }
        }

        public List<UnitAvailabilityInfo> GetUnitUnavailabilityInfo(DateTime startDate, DateTime endDate, string[] unitType, int UserID)
        {
            using (var context = new PRSEntities())
            {
                var units = (from unit in context.CAPUnitUnavailability
                             join facility in context.RDFacility on unit.FacilityId equals facility.StoreNo
                             join userFacility in context.FAUserAndFacilityAccess on facility.StoreRowID equals userFacility.FK_StoreRowID
                             //where unit.Startdate >= startDate && unit.Enddate <= endDate
                             where ((unit.Startdate >= startDate && unit.Enddate <= endDate) || (unit.Enddate >= startDate && unit.Startdate <= endDate))
                             && unitType.Contains(unit.UnitTypeId.ToString()) && unit.Status == 1
                             && userFacility.FK_UserID == UserID && userFacility.IsActive == true && userFacility.FK_AccessLevelID > 1
                             select new
                             {
                                 startDate = unit.Startdate,
                                 endDate = unit.Enddate,
                                 unitAvailabilityInfoId = unit.CAPUnitUnavailabilityId,
                                 facilityName = facility.MarketName,
                                 compDBAName = facility.CompDBAName,
                                 unitType = unit.UnitTypeId,
                                 storeNo = unit.FacilityId
                             });

                UnitAvailabilityInfo unAvailInfo;
                List<UnitAvailabilityInfo> lstUnitUnavailability = new List<UnitAvailabilityInfo>();

                foreach (var unit in units)
                {
                    unAvailInfo = new UnitAvailabilityInfo();
                    unAvailInfo.FacilityName = unit.compDBAName;
                    unAvailInfo.FromDate = unit.startDate;
                    unAvailInfo.StoreNo = unit.storeNo;
                    unAvailInfo.ThroughDate = (DateTime)unit.endDate;
                    unAvailInfo.UnitAvailabilityInfoId = (int)unit.unitAvailabilityInfoId;
                    unAvailInfo.UnitSize = unit.unitType;

                    lstUnitUnavailability.Add(unAvailInfo);
                }


                return lstUnitUnavailability;
            }
        }

        public UnitAvailabilityInfo GetUnitUnavailabilityInfo(int UnitAvailabilityInfoId)
        {
            using (var context = new PRSEntities())
            {
                UnitAvailabilityInfo unAvailable = new UnitAvailabilityInfo();

                unAvailable = (from unit in context.CAPUnitUnavailability
                               join facility in context.RDFacility on unit.FacilityId equals facility.StoreNo
                               where unit.CAPUnitUnavailabilityId == UnitAvailabilityInfoId
                               select new UnitAvailabilityInfo
                               {
                                   FromDate = unit.Startdate,
                                   ThroughDate = (DateTime)unit.Enddate,
                                   UnitAvailabilityInfoId = (int)unit.CAPUnitUnavailabilityId,
                                   FacilityName = facility.MarketName,
                                   //compDBAName = facility.CompDBAName,
                                   UnitSize = unit.UnitTypeId,
                                   StoreNo = unit.FacilityId,
                                   ReportedBy = unit.ReportedBy
                               }).FirstOrDefault();

                return unAvailable;
            }
        }

        public List<TruckOHReason> GetTruckOHReason()
        {
            using (var context = new PRSEntities())
            {
                return (from truckOHReason in context.CAPTruckOHReason
                        where truckOHReason.Status == 1
                        select new TruckOHReason
                        {
                            CAPTruckOHReasonId = truckOHReason.CAPTruckOHReasonId,
                            CategoryReason = truckOHReason.CategoryReason,
                            ReasonDescription = truckOHReason.ReasonDescription,
                            Status = truckOHReason.Status,
                        }).ToList();
            }
        }

        public List<FacilityOpHours> GetFacilityOpHours(DateTime FromDate, DateTime ToDate, string LocationCode)
        {
            return this.prsEntities.Database.SqlQuery<FacilityOpHours>(@"exec USP_CAP_GetFacilityHrsAvailability @FromDate, @ToDate, @LocationCode",
                Helper.SqlParameterObj("FromDate", FromDate),
                Helper.SqlParameterObj("ToDate", ToDate),
                Helper.SqlParameterObj("LocationCode", LocationCode)).AsQueryable().ToList();
        }

        public IQueryable<TruckOpHoursInfo> GetTruckOpHoursDetails(DateTime FromDate, DateTime ToDate, string LocationCode)
        {
            return this.prsEntities.Database.SqlQuery<TruckOpHoursInfo>(@"exec USP_CAP_GetTruckAvailability @FromDate, @ToDate, @LocationCode",
                Helper.SqlParameterObj("FromDate", FromDate),
                Helper.SqlParameterObj("ToDate", ToDate),
                Helper.SqlParameterObj("LocationCode", LocationCode)).AsQueryable();
        }

        public bool IsDuplicateTrkOpHrExists(TrkOpHours trkInfo)
        {
            bool duplicateEntryExists = false;
            using (var context = new PRSEntities())
            {
                int[] groupNoService = new int[] { 1, 4 }; //Maintainance and Repair
                int[] groupHrChange = new int[] { 2, 3 }; //Hour change
                int[] groupAvailability = new int[] { 5, 6 }; //Temporary and permanent transfers
                string rtMessage = string.Empty;

                //Check this truck is available with this facility or not

                //Check this truck availability with this facility before he make any operation on this facility
                #region Check This truck is available with this facility for this time or now

                var entryExists = context.CAPTruckOperatingHour.Where(db => db.TruckId == trkInfo.TruckId &&
                                                                            db.Status == 1).ToList();


                bool isThisTruckPermanentlyAvailableWithThisFacility = entryExists.Any(db => db.FacilityId == trkInfo.CurrentFacilityId &&
                                                                                                db.TruckOHReasonId == 6 &&
                                                                                                IsDBDatesWithInNewDates(db.Startdate, (db.Enddate ?? DateTime.Now.AddYears(10)), trkInfo.Startdate, trkInfo.Enddate));

                if (isThisTruckPermanentlyAvailableWithThisFacility == true)
                {
                    DateTime? startDate = (trkInfo.TruckOHReasonId == 6 || trkInfo.TruckOHReasonId == 5 ? trkInfo.TransferOut : trkInfo.Startdate);
                    DateTime? endDate = (trkInfo.TruckOHReasonId == 6 ? trkInfo.Startdate : (trkInfo.TruckOHReasonId == 5 ? trkInfo.TransferIn : trkInfo.Enddate));

                    bool isThisTruckTempTruckForThisFacility = entryExists.Any(db => db.FacilityId != trkInfo.CurrentFacilityId &&
                                                                                db.TruckOHReasonId == 5 &&
                                                                                IsDBDatesOverlapWithNewDates(db.TransferOut, db.TransferIn, startDate, endDate));

                    if (isThisTruckTempTruckForThisFacility == true)
                    {
                        throw new Exception("This truck is not available with this facility for the selected date range. Please contact operational manager.");
                    }
                }
                else
                {
                    //TODO if the transfer is permanent then this truck shuld be permanently availabel with this facility other wise through an exception -- For we are restricting in the UI level.
                    if (trkInfo.TruckOHReasonId == 6)
                        throw new Exception("This truck is not available with this facility for the selected date range. Please contact operational manager.");


                    bool isThisTruckTempTruckForThisFacility = entryExists.Any(db => db.FacilityId == trkInfo.CurrentFacilityId &&
                                                                                db.TruckOHReasonId == 5 &&
                                                                                IsDBDatesWithInNewDates(db.Startdate, db.Enddate, trkInfo.Startdate, trkInfo.Enddate));
                    if (isThisTruckTempTruckForThisFacility == false)
                    {
                        throw new Exception("This truck is not available with this facility for the selected date range. Please contact operational manager.");
                    }
                }

                #endregion

                if (groupHrChange.Contains(trkInfo.TruckOHReasonId))
                {
                    duplicateEntryExists = entryExists.Where(db => db.CAPTruckOperatingHourId != trkInfo.CAPTruckOperatingHourId &&
                                                                    db.FacilityId == trkInfo.FacilityId &&
                                                                    groupHrChange.Contains(db.TruckOHReasonId)).ToList().Any(db =>
                                                                    IsDBDatesOverlapWithNewDates(db.Startdate, db.Enddate, trkInfo.Startdate, trkInfo.Enddate));
                }
                else if (groupNoService.Contains(trkInfo.TruckOHReasonId))
                {
                    duplicateEntryExists = entryExists.Where(db => db.CAPTruckOperatingHourId != trkInfo.CAPTruckOperatingHourId &&
                                                                                    db.TruckId == trkInfo.TruckId &&
                                                                                    db.FacilityId == trkInfo.FacilityId &&
                                                                                    db.Status == 1 &&
                                                                                    groupNoService.Contains(db.TruckOHReasonId)).ToList().Any(db =>
                                                                                    IsDBDatesOverlapWithNewDates(db.Startdate, db.Enddate, trkInfo.Startdate, trkInfo.Enddate));
                }
                else if (trkInfo.TruckOHReasonId == 5)
                {
                    //Before you transfer to other facility made sure that should not have maintanance overlap with that date range
                    bool checkDoesItHasUnderMaintananceWithThisDateRange = entryExists.Any(db => groupNoService.Contains(db.TruckOHReasonId) &&
                                                                                                IsDBDatesOverlapWithNewDates(db.Startdate, (db.Enddate ?? DateTime.Now.AddYears(10)), trkInfo.TransferOut, trkInfo.TransferIn));

                    if (checkDoesItHasUnderMaintananceWithThisDateRange == true)
                        throw new Exception("There is a maintenance or repair scheduled with this date range. Please adjust them and then transfer.");

                    duplicateEntryExists = entryExists.Where(db => db.CAPTruckOperatingHourId != trkInfo.CAPTruckOperatingHourId &&
                                                                                    db.TruckId == trkInfo.TruckId &&
                                                                                    db.FacilityId == trkInfo.FacilityId &&
                                                                                    db.Status == 1 &&
                                                                                    db.TruckOHReasonId == 5).ToList().Any(db =>
                                                                                    IsDBDatesOverlapWithNewDates(db.TransferOut, db.TransferIn, trkInfo.TransferOut, trkInfo.TransferIn));
                }
                else if (trkInfo.TruckOHReasonId == 6)
                {
                    //Before you transfer to other facility made sure that should not have maintanance overlap with that date range
                    bool checkDoesItHasUnderMaintananceWithThisDateRange = entryExists.Any(db => groupNoService.Contains(db.TruckOHReasonId) &&
                                                                                                IsDBDatesOverlapWithNewDates(db.Startdate, (db.Enddate ?? DateTime.Now.AddYears(10)), trkInfo.TransferOut, trkInfo.TransferIn));

                    if (checkDoesItHasUnderMaintananceWithThisDateRange == true)
                        throw new Exception("There is a maintenance or repair scheduled with this date range. Please adjust them and then transfer.");

                    duplicateEntryExists = entryExists.Where(db => db.CAPTruckOperatingHourId != trkInfo.CAPTruckOperatingHourId &&
                                                                                    db.TruckId == trkInfo.TruckId &&
                                                                                    db.TransferFacilityId == trkInfo.TransferFacilityId &&
                                                                                    db.Status == 1 &&
                                                                                    db.TruckOHReasonId == 6).ToList().Any(db =>
                                                                                    IsDBDatesOverlapWithNewDates(db.TransferOut, db.TransferIn, trkInfo.TransferOut, trkInfo.TransferIn));
                }
            }

            return duplicateEntryExists;
        }

        private static bool IsDBDatesOverlapWithNewDates(DateTime? dbStartDateA, DateTime? dbEndDateA, DateTime? newStartDateA, DateTime? newEndDateA)
        {
            DateTime dbStartDate = dbStartDateA ?? DateTime.Now;
            DateTime newStartDate = newStartDateA ?? DateTime.Now;
            DateTime dbEndDate = dbEndDateA ?? DateTime.Now;
            DateTime newEndDate = newEndDateA ?? DateTime.Now;

            //OR(AND(A5<=D5,D5<=B5), AND(A5<=E5,E5<=B5), AND(D5<=A5, A5<=E5), AND(D5<=B5, B5<=E5))
            return ((dbStartDate.Date <= newStartDate.Date && newStartDate.Date <= dbEndDate.Date) || (dbStartDate.Date <= newEndDate.Date && newEndDate.Date <= dbEndDate.Date) ||
                (newStartDate.Date <= dbStartDate.Date && dbStartDate.Date <= newEndDate.Date) || (newStartDate.Date <= dbEndDate.Date && dbEndDate.Date <= newEndDate.Date));
        }

        private static bool IsDBDatesWithInNewDates(DateTime? dbStartDateA, DateTime? dbEndDateA, DateTime? newStartDateA, DateTime? newEndDateA)
        {
            DateTime dbStartDate = dbStartDateA ?? DateTime.Now;
            DateTime newStartDate = newStartDateA ?? DateTime.Now;
            DateTime dbEndDate = dbEndDateA ?? DateTime.Now;
            DateTime newEndDate = newEndDateA ?? DateTime.Now;

            //AND(AND(A5<=D5,D5<=B5), AND(A5<=E5,E5<=B5))
            return ((dbStartDate.Date <= newStartDate.Date && newStartDate.Date <= dbEndDate.Date) && (dbStartDate.Date <= newEndDate.Date && newEndDate.Date <= dbEndDate.Date));
        }

        public List<CAPTruckTransferReason> GetTransferReason()
        {
            using (var context = new PRSEntities())
            {
                List<CAPTruckTransferReason> lstTransferReason = new List<CAPTruckTransferReason>();

                foreach (var reason in context.CAPTruckTransferReason.Where(x => x.Status == 1))
                {
                    CAPTruckTransferReason uUnavailability = new CAPTruckTransferReason();
                    lstTransferReason.Add(new CAPTruckTransferReason
                    {
                        CAPTruckTransferReasonId = reason.CAPTruckTransferReasonId,
                        TransferReason = reason.TransferReason,
                        Status = reason.Status,
                        SendAlert = reason.SendAlert,
                        AlertGroups = reason.AlertGroups
                    });
                }
                return lstTransferReason;
            }
        }

        public void AddTruckOpHours(TrkOpHours trkInfo)
        {
            bool isAnyEntryExists = IsDuplicateTrkOpHrExists(trkInfo);

            if (isAnyEntryExists == true)
                throw new Exception("This entry will be a duplicate entry with same group reason. Please check and save information.");

            SaveTruckOpHours(trkInfo);
        }

        public string SaveTruckOpearationalHour(TrkOpHours trkInfo)
        {
            return this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_SaveTruckOperatingHour 
																		@CAPTruckOperatingHourId,
																		@FacilityId,
																		@TransferFacilityId,
																		@TransferOut,
																		@TransferIn,
																		@TruckId,
																		@TruckOHReasonId,
																		@Startdate,
																		@Enddate,
																		@MinutesAvailable,
																		@CreatedBy,
																		@ModifiedBy,
																		@Status,
																		@TransferReasonId",
                                                                     Helper.SqlParameterObj("CAPTruckOperatingHourId", trkInfo.CAPTruckOperatingHourId),
                                                                     Helper.SqlParameterObj("FacilityId", trkInfo.CurrentFacilityId),
                                                                     Helper.SqlParameterObj("TransferFacilityId", trkInfo.TransferFacilityId),
                                                                     Helper.SqlParameterObj("TransferOut", trkInfo.TransferOut),
                                                                     Helper.SqlParameterObj("TransferIn", trkInfo.TransferIn),
                                                                     Helper.SqlParameterObj("TruckId", trkInfo.TruckId),
                                                                     Helper.SqlParameterObj("TruckOHReasonId", trkInfo.TruckOHReasonId),
                                                                     Helper.SqlParameterObj("Startdate", trkInfo.Startdate),
                                                                     Helper.SqlParameterObj("Enddate", trkInfo.Enddate),
                                                                     Helper.SqlParameterObj("MinutesAvailable", trkInfo.MinutesAvailable),
                                                                     Helper.SqlParameterObj("CreatedBy", trkInfo.CreatedBy),
                                                                     Helper.SqlParameterObj("ModifiedBy", trkInfo.ModifiedBy),
                                                                     Helper.SqlParameterObj("Status", trkInfo.Status),
                                                                     Helper.SqlParameterObj("TransferReasonId", trkInfo.TransferReasonId)
                                                                     ).AsQueryable().FirstOrDefault();
        }

        public void SaveTruckOpHours(TrkOpHours trkInfo)
        {
            using (var context = new PRSEntities())
            {
                //If user trying to update with same reason for the over laping reasons then we have to make this entry inactive and enter new entry for new dates

                //In active existing entry and create new entry for edit
                if (trkInfo.CAPTruckOperatingHourId > 0 && trkInfo.TruckOHReasonId != 6)
                {
                    CAPTruckOperatingHour existingTruckOH = context.CAPTruckOperatingHour.Find(trkInfo.CAPTruckOperatingHourId);
                    if (existingTruckOH.CAPTruckOperatingHourId > 0)
                    {
                        existingTruckOH.Status = 0;
                        existingTruckOH.ModifiedBy = trkInfo.ModifiedBy;
                        existingTruckOH.ModifiedOn = DateTime.Now;
                    }
                }
                else if (trkInfo.TruckOHReasonId == 6)// && trkInfo.CAPTruckOperatingHourId > 0)//we are not giving edit option for Permanent Transfer, so we never get CAPTruckOperatingHourId
                {
                    //Don't have CAPTruckOperatingHourId, so filtering data with storeNumber, TruckId and Reason
                    //CAPTruckOperatingHour existingTruckOH = context.CAPTruckOperatingHour.Find(trkInfo.CAPTruckOperatingHourId);
                    CAPTruckOperatingHour existingTruckOH = context.CAPTruckOperatingHour.Where(t => t.FacilityId == trkInfo.CurrentFacilityId &&
                                                                                                    t.TruckId == trkInfo.TruckId &&
                                                                                                    t.TruckOHReasonId == 6 && t.Enddate == null).FirstOrDefault();
                    if (existingTruckOH != null)//existingTruckOH.CAPTruckOperatingHourId > 0)
                    {
                        existingTruckOH.Enddate = trkInfo.TransferOut.Value.AddDays(-1);
                        existingTruckOH.ModifiedBy = trkInfo.ModifiedBy;
                        existingTruckOH.ModifiedOn = DateTime.Now;
                    }
                }


                CAPTruckOperatingHour dbTruck = new CAPTruckOperatingHour
                {
                    FacilityId = trkInfo.FacilityId,
                    MinutesAvailable = Convert.ToInt16(trkInfo.MinutesAvailable),
                    ModifiedBy = trkInfo.ModifiedBy,
                    ModifiedOn = DateTime.Now,
                    CreatedBy = trkInfo.CreatedBy,
                    CreatedOn = DateTime.Now,
                    Startdate = trkInfo.Startdate,
                    Enddate = trkInfo.Enddate,
                    TransferFacilityId = trkInfo.TransferFacilityId,
                    TransferOut = trkInfo.TransferOut,
                    TransferIn = trkInfo.TransferIn,
                    TruckId = trkInfo.TruckId,
                    TruckOHReasonId = trkInfo.TruckOHReasonId,
                    Status = 1
                };

                context.CAPTruckOperatingHour.Add(dbTruck);
                context.SaveChanges();
            }
        }

        public TrkOpHours GetTruckOpHrsDetails(int CAPTruckOperatingHourId)
        {
            using (var context = new PRSEntities())
            {
                CAPTruckOperatingHour dbUnit = context.CAPTruckOperatingHour.Find(CAPTruckOperatingHourId);

                TrkOpHours trkOpHoursDetails = new TrkOpHours();

                if (dbUnit != null && dbUnit.CAPTruckOperatingHourId > 0)
                {
                    trkOpHoursDetails.CAPTruckOperatingHourId = (int)dbUnit.CAPTruckOperatingHourId;
                    trkOpHoursDetails.FacilityId = dbUnit.FacilityId;
                    trkOpHoursDetails.MinutesAvailable = dbUnit.MinutesAvailable.HasValue ? (int)dbUnit.MinutesAvailable : 0;
                    trkOpHoursDetails.Status = dbUnit.Status;
                    trkOpHoursDetails.TransferFacilityId = dbUnit.TransferFacilityId;
                    trkOpHoursDetails.TruckId = dbUnit.TruckId;
                    trkOpHoursDetails.TruckOHReasonId = dbUnit.TruckOHReasonId;

                    trkOpHoursDetails.TransferOut = dbUnit.TransferOut ?? DateTime.Now.AddYears(-10);
                    trkOpHoursDetails.TransferIn = dbUnit.TransferIn;
                    trkOpHoursDetails.Startdate = dbUnit.Startdate;
                    trkOpHoursDetails.Enddate = dbUnit.Enddate.HasValue ? (DateTime?)dbUnit.Enddate : DateTime.Now.AddYears(10);
                }

                return trkOpHoursDetails;
            }
        }

        public List<TruckTempHourChange> GetTruckTempOpHourDetails(int facilityStoreNo, DateTime startDate, int noOfDays, int truckId, int weekday)
        {
            DateTime endDate = startDate.AddDays(noOfDays);
            return this.prsEntities.Database.SqlQuery<TruckTempHourChange>(@"exec USP_CAP_GetTruckTempHourChange @StartDate, @EndDate, @StoreNumber, @TruckId, @DATEPART",
                Helper.SqlParameterObj("StartDate", startDate),
                Helper.SqlParameterObj("EndDate", endDate),
                Helper.SqlParameterObj("StoreNumber", facilityStoreNo),
                Helper.SqlParameterObj("TruckId", truckId),
                Helper.SqlParameterObj("DATEPART", weekday)).AsQueryable().ToList();
        }

        public void SaveTempHourChange(List<TruckTempHourChange> listTempHourChangeEntries, int storeNumber, int userId)
        {
            using (var context = new PRSEntities())
            {
                foreach (var tempHourChange in listTempHourChangeEntries)
                {
                    CAPTruckOperatingHour dbTruckOpHr = context.CAPTruckOperatingHour
                        .Where(t => t.Status == 1 && t.TruckOHReasonId == 3 && t.TruckId == tempHourChange.TruckId &&
                            (t.Startdate != null && (t.Startdate.Year == tempHourChange.CapacityDate.Year && t.Startdate.Month == tempHourChange.CapacityDate.Month &&
                            t.Startdate.Day == tempHourChange.CapacityDate.Day)) && t.FacilityId == storeNumber).FirstOrDefault();

                    if (dbTruckOpHr != null)
                    {
                        dbTruckOpHr.Status = 0;
                        dbTruckOpHr.ModifiedBy = userId;
                        dbTruckOpHr.ModifiedOn = DateTime.Now;
                    }

                    DateTime startDate = tempHourChange.CapacityDate;
                    DateTime endDate = tempHourChange.CapacityDate;
                    short minutesAvailable = (short)0;

                    //If user selectes store close then we have to consider operating hours are zero
                    if (tempHourChange.StoreOpen.ToLower() == "o")
                    {
                        startDate = DateTime.Parse(tempHourChange.CapacityDate.ToShortDateString() + " " + tempHourChange.StartTime);
                        endDate = DateTime.Parse(tempHourChange.CapacityDate.ToShortDateString() + " " + tempHourChange.EndTime);
                        minutesAvailable = short.Parse((endDate - startDate).TotalMinutes.ToString());
                    }

                    CAPTruckOperatingHour newdbTempHourChange = new CAPTruckOperatingHour
                    {
                        FacilityId = storeNumber,
                        TruckId = tempHourChange.TruckId,
                        TruckOHReasonId = 3,
                        Startdate = startDate,
                        Enddate = endDate,
                        MinutesAvailable = minutesAvailable,
                        CreatedBy = userId,
                        CreatedOn = DateTime.Now,
                        Status = 1
                    };

                    context.CAPTruckOperatingHour.Add(newdbTempHourChange);
                }

                context.SaveChanges();
            }
        }

        public void RevertToFacilityOperatingHours(List<TruckTempHourChange> listTempHourChangeEntries, int storeNumber, int userId)
        {
            using (var context = new PRSEntities())
            {
                foreach (var tempHourChange in listTempHourChangeEntries)
                {
                    CAPTruckOperatingHour dbTruckOpHr = context.CAPTruckOperatingHour
                        .Where(t => t.Status == 1 && t.TruckOHReasonId == 3 && t.TruckId == tempHourChange.TruckId &&
                            (t.Startdate != null && (t.Startdate.Year == tempHourChange.CapacityDate.Year && t.Startdate.Month == tempHourChange.CapacityDate.Month &&
                            t.Startdate.Day == tempHourChange.CapacityDate.Day)) && t.FacilityId == storeNumber).FirstOrDefault();

                    if (dbTruckOpHr != null)
                    {
                        dbTruckOpHr.Status = 0;
                        dbTruckOpHr.ModifiedBy = userId;
                        dbTruckOpHr.ModifiedOn = DateTime.Now;
                    }
                }

                context.SaveChanges();
            }
        }

        public void TruckBackToService(int CAPTruckOperatingHourId, int userId)
        {
            using (var context = new PRSEntities())
            {
                CAPTruckOperatingHour dbTruckOpHr = context.CAPTruckOperatingHour.Find(CAPTruckOperatingHourId);

                if (dbTruckOpHr != null)
                {
                    dbTruckOpHr.Status = 0;
                    dbTruckOpHr.ModifiedBy = userId;
                    dbTruckOpHr.ModifiedOn = DateTime.Now;

                    //If NoService start date is already passed then we have to added new entry with previous day as ending day and bring back to service for this truck
                    if (dbTruckOpHr.Startdate.Date < DateTime.Now.Date)
                    {
                        CAPTruckOperatingHour newdbTempHourChange = new CAPTruckOperatingHour
                        {
                            FacilityId = dbTruckOpHr.FacilityId,
                            TruckId = dbTruckOpHr.TruckId,
                            TruckOHReasonId = dbTruckOpHr.TruckOHReasonId,
                            Startdate = dbTruckOpHr.Startdate,
                            Enddate = DateTime.Now.AddDays(-1),
                            MinutesAvailable = 0,
                            CreatedBy = userId,
                            CreatedOn = DateTime.Now,
                            Status = 1
                        };

                        context.CAPTruckOperatingHour.Add(newdbTempHourChange);
                    }

                    context.SaveChanges();
                }
            }
        }

        #region Facility Operating Hours

        public List<FacilityOperatingHours> AddAndGetFacilityOpHour(int facilityStoreNo, DateTime startDate, DateTime endDate, string userName, int weekday)
        {
            return this.prsEntities.Database.SqlQuery<FacilityOperatingHours>(@"exec USP_CAP_AddAndGetFacilityOpHour @StartDate, @EndDate, @StoreNumber, @CreatedBy, @DATEPART",
                Helper.SqlParameterObj("StartDate", startDate),
                Helper.SqlParameterObj("EndDate", endDate),
                Helper.SqlParameterObj("StoreNumber", facilityStoreNo),
                Helper.SqlParameterObj("CreatedBy", userName),
                Helper.SqlParameterObj("DATEPART", weekday)).AsQueryable().ToList();
        }

        public int SaveFacilityHourChange(string rowIds, string startTime, string endTime, int userId, string storeOpen)
        {
            return this.prsEntities.Database.SqlQuery<int>(@"exec USP_CAP_UpdateFacilityOpHour @RowIds, @StartTime, @EndTime, @StoreOpen, @CreatedBy",
                Helper.SqlParameterObj("RowIds", rowIds),
                Helper.SqlParameterObj("StartTime", startTime),
                Helper.SqlParameterObj("EndTime", endTime),
                Helper.SqlParameterObj("StoreOpen", storeOpen),
                Helper.SqlParameterObj("CreatedBy", userId)).Single();
        }

        #endregion

        #region Application Settings

        public List<ApplicationSettings> GetAllApplicationSettings()
        {
            return (from ta in this.prsEntities.CAPApplicationSetting.Where(a => a.Status == 1)
                    select new ApplicationSettings
                    {
                        CAPApplicationParameterId = 0,
                        ApplicationParameter = ta.ApplicationParameter
                    }).Distinct().ToList();
        }

        public List<ApplicationSettings> GetApplicationSettingsList(DateTime date, string SortCol = null, string SortDir = null)
        {
            return this.prsEntities.Database.SqlQuery<ApplicationSettings>(@"exec USP_CAP_GetApplicationSettingsList @Date,@SortCol,@SortDir",
                Helper.SqlParameterObj("Date", date),
                Helper.SqlParameterObj("@SortCol", SortCol),
                Helper.SqlParameterObj("@SortDir", SortDir)).AsQueryable().ToList();

        }

        public List<ApplicationSettings> SearchApplicationSettingsList(string SettingName)
        {
            return this.prsEntities.Database.SqlQuery<ApplicationSettings>(@"exec USP_CAP_GetApplicationSettingsDetailList @SettingName",
                Helper.SqlParameterObj("SettingName", SettingName)).AsQueryable().ToList();
        }

        public string UpdateApplicationSettingsValues(int UserId, string SettingName, DateTime startDate, DateTime? endDate, string ApplicationValue, bool isNullEnddate)
        {
            string result = string.Empty;
            result = this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_EditApplicationSettings @UserId, @ApplicationParameter, @ApplicationValue, @VarStartDate, @VarEndDate, @IsNullEndDate",
                Helper.SqlParameterObj("UserId", UserId),
                Helper.SqlParameterObj("ApplicationParameter", SettingName),
                Helper.SqlParameterObj("ApplicationValue", ApplicationValue),
                Helper.SqlParameterObj("VarStartDate", startDate),
                Helper.SqlParameterObj("VarEndDate", endDate),
                Helper.SqlParameterObj("IsNullEndDate", isNullEnddate)).AsQueryable().FirstOrDefault();
            return result;
        }

        #endregion

        #region Average MPH

        public List<RDFacility> GetFacilitiesList()
        {
            return (from fc in this.prsEntities.RDFacility
                    where fc.StatusRowID >= 65 && fc.StoreType >= 10
                    select fc).Distinct().OrderBy(f => f.CompDBAName).ToList();
        }

        public List<RDFacility> GetFacilityListByUserId(int UserId)
        {
            return (from fc in this.prsEntities.RDFacility
                    join userFacility in this.prsEntities.FAUserAndFacilityAccess on fc.StoreRowID equals userFacility.FK_StoreRowID
                    where fc.StatusRowID >= 65 && fc.StoreType >= 10 && userFacility.FK_UserID == UserId
                        && userFacility.IsActive == true
                        && userFacility.FK_AccessLevelID > 1
                    select fc).OrderBy(f => f.CompDBAName).ToList();
        }

        public List<AverageMPH> GetAverageMPHList(DateTime date, int UserID, string SortCol = null, string SortDir = null)
        {
            return this.prsEntities.Database.SqlQuery<AverageMPH>(@"exec USP_CAP_GetFacilitiesListwithMPH @Date,@SortCol,@SortDir, @UserID",
                Helper.SqlParameterObj("Date", date),
                Helper.SqlParameterObj("@SortCol", SortCol),
                Helper.SqlParameterObj("@SortDir", SortDir),
                Helper.SqlParameterObj("@UserID", UserID)).AsQueryable().ToList();
        }

        public List<AverageMPH> SearchFacilityAverageMPHList(int facilityId)
        {
            return this.prsEntities.Database.SqlQuery<AverageMPH>(@"exec USP_CAP_GetFacilityAverageMPHDetailList @FacilityId",
                Helper.SqlParameterObj("FacilityId", facilityId)).AsQueryable().ToList();
        }

        public string UpdateFacilityAvgMPHValues(int UserId, int facilityId, DateTime startDate, DateTime? endDate, decimal mphval, bool isNullEnddate)
        {
            string result = string.Empty;
            result = this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_EditFacilityAverageMPH @UserId, @FacilityId, @AvgMPH, @VarStartDate, @VarEndDate, @IsNullEndDate",
                Helper.SqlParameterObj("UserId", UserId),
                Helper.SqlParameterObj("FacilityId", facilityId),
                Helper.SqlParameterObj("AvgMPH", mphval),
                Helper.SqlParameterObj("VarStartDate", startDate),
                Helper.SqlParameterObj("VarEndDate", endDate),
                Helper.SqlParameterObj("IsNullEndDate", isNullEnddate)).AsQueryable().FirstOrDefault();
            return result;
        }

        public string SetAverageMPHtoCorporateDefault(int UserId, int facilityId, DateTime startDate)
        {
            string result = string.Empty;
            result = this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_SETFacilityAverageMPHtoCorporateDefault @UserId, @FacilityId, @VarStartDate",
                Helper.SqlParameterObj("UserId", UserId),
                Helper.SqlParameterObj("FacilityId", facilityId),
                Helper.SqlParameterObj("VarStartDate", startDate)).AsQueryable().FirstOrDefault();
            return result;
        }

        #endregion

        #region TruckDowntime

        public List<TruckDowntimeModel> GetTruckDowntimeList(DateTime date, int UserID, string SortCol = null, string SortDir = null)
        {
            return this.prsEntities.Database.SqlQuery<TruckDowntimeModel>(@"exec USP_CAP_GetFacilitiesListwithTruckDowntime @Date,@SortCol,@SortDir, @UserID",
                Helper.SqlParameterObj("Date", date),
                Helper.SqlParameterObj("@SortCol", SortCol),
                Helper.SqlParameterObj("@SortDir", SortDir),
                Helper.SqlParameterObj("@UserID", UserID)).AsQueryable().ToList();
        }

        public List<TruckDowntimeModel> SearchFacilityTruckDowntimeList(int facilityId)
        {
            return this.prsEntities.Database.SqlQuery<TruckDowntimeModel>(@"exec USP_CAP_GetFacilityTruckDowntimeDetailList @FacilityId",
                Helper.SqlParameterObj("FacilityId", facilityId)).AsQueryable().ToList();
        }

        public string UpdateFacilityTruckDowntimeValues(int UserId, int facilityId, DateTime startDate, DateTime? endDate, int truckDowntime, bool isNullEnddate)
        {
            string result = string.Empty;
            result = this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_EditFacilityTruckDowntime @UserId,@FacilityId, @TruckDowntime, @VarStartDate, @VarEndDate, @IsNullEndDate",
                Helper.SqlParameterObj("UserId", UserId),
                Helper.SqlParameterObj("FacilityId", facilityId),
                Helper.SqlParameterObj("TruckDowntime", truckDowntime),
                Helper.SqlParameterObj("VarStartDate", startDate),
                Helper.SqlParameterObj("VarEndDate", endDate),
                Helper.SqlParameterObj("IsNullEndDate", isNullEnddate)).AsQueryable().FirstOrDefault();
            return result;
        }

        public string SetTruckDowntimetoCorporateDefault(int UserId, int facilityId, DateTime startDate)
        {
            string result = string.Empty;
            result = this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_SETFacilityTruckDowntimetoCorporateDefault @UserId, @FacilityId, @VarStartDate",
                Helper.SqlParameterObj("UserId", UserId),
                Helper.SqlParameterObj("FacilityId", facilityId),
                Helper.SqlParameterObj("VarStartDate", startDate)).AsQueryable().FirstOrDefault();
            return result;
        }

        #endregion

        #region Touchtime

        public List<TouchTime> GetTouchTimeList(DateTime date, int UserID, string SortCol = null, string SortDir = null)
        {
            return this.prsEntities.Database.SqlQuery<TouchTime>(@"exec USP_CAP_GetFacilitiesListwithTouchTime @Date,@SortCol,@SortDir, @UserID",
                Helper.SqlParameterObj("Date", date),
                Helper.SqlParameterObj("@SortCol", SortCol),
                Helper.SqlParameterObj("@SortDir", SortDir),
                Helper.SqlParameterObj("@UserID", UserID)).AsQueryable().ToList();
        }

        public List<TouchTime> SearchFacilityTouchTimeList(int facilityId)
        {
            return this.prsEntities.Database.SqlQuery<TouchTime>(@"exec USP_CAP_GetFacilityTouchtimeDetailList @FacilityId",
                Helper.SqlParameterObj("FacilityId", facilityId)).AsQueryable().ToList();
        }

        public string UpdateFacilityTouchTimeValues(int UserId, int facilityId, DateTime startDate, DateTime? endDate, int DE, int DF, int RE, int RF, int OBO, int IBO, int WA, int CC, bool isNullEnddate)
        {
            string result = string.Empty;
            result = this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_EditFacilityTouchTime @UserId, @FacilityId, @DE, @DF, @RE, @RF,"
                + " @OBO, @IBO, @WA, @CC, @VarStartDate, @VarEndDate, @IsNullEndDate",
                Helper.SqlParameterObj("UserId", UserId),
                Helper.SqlParameterObj("FacilityId", facilityId),
                Helper.SqlParameterObj("DE", DE),
                Helper.SqlParameterObj("DF", DF),
                Helper.SqlParameterObj("RE", RE),
                Helper.SqlParameterObj("RF", RF),
                Helper.SqlParameterObj("OBO", OBO),
                Helper.SqlParameterObj("IBO", IBO),
                Helper.SqlParameterObj("WA", WA),
                Helper.SqlParameterObj("CC", CC),
                Helper.SqlParameterObj("VarStartDate", startDate),
                Helper.SqlParameterObj("VarEndDate", endDate),
                Helper.SqlParameterObj("IsNullEndDate", isNullEnddate)).AsQueryable().FirstOrDefault();
            return result;
        }

        public string SetTouchtimetoCorporateDefault(int UserId, int facilityId, DateTime startDate)
        {
            string result = string.Empty;
            result = this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_SETFacilityTouchtimeToCorporateDefault @UserId, @FacilityId, @VarStartDate",
                Helper.SqlParameterObj("UserId", UserId),
                Helper.SqlParameterObj("FacilityId", facilityId),
                Helper.SqlParameterObj("VarStartDate", startDate)).AsQueryable().FirstOrDefault();
            return result;
        }

        #endregion

        #region SLMileage
        public List<SLMileage> GetSLMileageList(DateTime date, int UserID, string SortCol = null, string SortDir = null)
        {
            return this.prsEntities.Database.SqlQuery<SLMileage>(@"exec USP_CAP_GetFacilitiesListwithSLMileAdjmnt @Date,@SortCol,@SortDir, @UserID",
                Helper.SqlParameterObj("Date", date),
                Helper.SqlParameterObj("@SortCol", SortCol),
                Helper.SqlParameterObj("@SortDir", SortDir),
                Helper.SqlParameterObj("@UserID", UserID)).AsQueryable().ToList();
        }

        public List<SLMileage> SearchSLMileageList(int facilityId)
        {
            return this.prsEntities.Database.SqlQuery<SLMileage>(@"exec USP_CAP_GetFacilitySLMileAdjmntDetailList @FacilityId",
                Helper.SqlParameterObj("FacilityId", facilityId)).AsQueryable().ToList();
        }

        public string UpdateSLMileageValues(int UserId, int facilityId, DateTime startDate, DateTime? endDate, decimal SLMileageAdjustment, bool isNullEnddate)
        {
            string result = string.Empty;
            result = this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_EditFacilitySLMileAdjmnt @UserId,@FacilityId, @SLMileageAdjustment, @VarStartDate, @VarEndDate, @IsNullEndDate",
                Helper.SqlParameterObj("UserId", UserId),
                Helper.SqlParameterObj("FacilityId", facilityId),
                Helper.SqlParameterObj("SLMileageAdjustment", SLMileageAdjustment),
                Helper.SqlParameterObj("VarStartDate", startDate),
                Helper.SqlParameterObj("VarEndDate", endDate),
                Helper.SqlParameterObj("IsNullEndDate", isNullEnddate)).AsQueryable().FirstOrDefault();
            return result;
        }

        public string SetSLMileagetoCorporateDefault(int UserId, int facilityId, DateTime startDate)
        {
            string result = string.Empty;
            result = this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_SETFacilitySLMileAdjmntToCorporateDefault @UserId, @FacilityId, @VarStartDate ",
                Helper.SqlParameterObj("UserId", UserId),
                Helper.SqlParameterObj("FacilityId", facilityId),
                Helper.SqlParameterObj("VarStartDate", startDate)).AsQueryable().FirstOrDefault();
            return result;
        }

        #endregion

        #region TruckTransferReason
        public List<CAPTruckTransferReason> GetTransferReasonSearchresults(string SearchKey)
        {
            List<CAPTruckTransferReason> lstTransferReason = new List<CAPTruckTransferReason>();
            lstTransferReason = this.prsEntities.Database.SqlQuery<CAPTruckTransferReason>(@"exec USP_CAP_SearchTruckTransferReasons @SearchKey",
                Helper.SqlParameterObj("SearchKey", SearchKey)).AsQueryable().ToList();
            return lstTransferReason;
        }
        public string GetTransferReasonbyId(int TransferReasonId)
        {
            string Transferreasontxt = "";
            using (var context = new PRSEntities())
            {
                Transferreasontxt = context.CAPTruckTransferReason.Where(x => x.CAPTruckTransferReasonId == TransferReasonId).Select(x => x.TransferReason).FirstOrDefault();
            }
            return Transferreasontxt;
        }

        public string SaveTransferReasonwithId(int TransferReasonId, string TransferReason)
        {
            string result = string.Empty;
            result = this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_SaveUpdateTruckTransferReason @TransferReasonId,@TransferReason",
                Helper.SqlParameterObj("TransferReasonId", TransferReasonId),
                Helper.SqlParameterObj("TransferReason", TransferReason)).AsQueryable().FirstOrDefault();
            return result;
        }
        public string SetTransferReasonActiveInactive(int TransferReasonId, int status)
        {
            string result = string.Empty;
            result = this.prsEntities.Database.SqlQuery<string>(@"exec USP_CAP_SetTruckTransferReasonActiveInactive @TransferReasonId,@status",
                Helper.SqlParameterObj("TransferReasonId", TransferReasonId),
                Helper.SqlParameterObj("status", status)).AsQueryable().FirstOrDefault();
            return result;
        }

        #endregion

        #region User Management
        public List<Entities.ModuleAndAccessLevel> GetRoleGroupModuleAccess(string roleGroupName)
        {
            using (var context = new PRSEntities())
            {
                return (from module in context.FAModule
                        join groupmodule in context.FAGroupAndModule on module.PK_ModuleID equals groupmodule.FK_ModuleID
                        join groupn in context.FAGroupMaster on groupmodule.FK_GroupId equals groupn.PK_GroupId
                        where groupmodule.IsActive == true && groupn.GroupName.Equals(roleGroupName, StringComparison.OrdinalIgnoreCase)
                         && groupmodule.FK_AccessLevelID > 1
                        select new Entities.ModuleAndAccessLevel
                        {
                            ModuleId = module.PK_ModuleID,
                            ModuleName = module.ModuleName,
                            AccessLevel = (Entities.Enums.AccessLevel)groupmodule.FK_AccessLevelID
                        }).ToList();
            }
        }
        #endregion

        #region  WareHouseTouches


        public void SaveWTTouchInformation(WATouchInfo TouchInfo, int UserId)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var dbTouchInfo = context.FAWTTouchInformation.Where(t => t.QORId == TouchInfo.QORId && t.TouchType == TouchInfo.TouchType && t.SeqNumber == TouchInfo.SequenceNum).FirstOrDefault();

                int? OldStagedLocation = 0;

                if (dbTouchInfo != null)
                {
                    OldStagedLocation = dbTouchInfo.FK_TouchActionID;

                    //Need to edit touchinfo
                    dbTouchInfo.Comments = TouchInfo.Comments;
                    dbTouchInfo.SLTouchStatus = TouchInfo.SLTouchStatus.ToString();
                    dbTouchInfo.UnitNumber = TouchInfo.UnitName;

                    if (TouchInfo.DontChangeStatusForTheFirstUpdateButChangeInSecondUpdate == false && TouchInfo.CanUpdateTouchStatusID)
                    {
                        dbTouchInfo.FK_TouchStatusID = TouchInfo.TouchStatusID;

                        //IF status is not stage status then we have to make FK_StageActionID to null other wise get StageActionID and save it
                        //bool isActionStage = context.FAWTTouchAction.Any(a => a.PK_ID == TouchInfo.ActionID && a.FAWTTouchActionCategory.ActionCategoryName == WATouchStatus.Stage.ToString());
                        bool isActionStage = context.FAWTTouchStatus.Any(s => s.PK_ID == dbTouchInfo.FK_TouchStatusID && s.StatusName == WATouchStatus.Stage.ToString() && s.IsActive == true);

                        if (isActionStage)
                        {
                            if (TouchInfo.StageActionID.HasValue && TouchInfo.StageActionID.Value > 0)
                                dbTouchInfo.FK_StageActionID = TouchInfo.StageActionID;
                        }
                        else
                            dbTouchInfo.FK_StageActionID = null;
                    }

                    if (TouchInfo.ActionID > 0)
                    {
                        dbTouchInfo.FK_TouchActionID = TouchInfo.ActionID;
                        dbTouchInfo.ScheduledDate = TouchInfo.SLScheduledDate;
                        dbTouchInfo.CompletedDate = TouchInfo.CompletedDateTime;
                    }
                    //This codition will be true when the touch has been unscheduled but there are no actions available to change. 
                    else if (TouchInfo.ByPassActionChangeToSyncData)
                    {
                        dbTouchInfo.ScheduledDate = TouchInfo.SLScheduledDate;
                        dbTouchInfo.CompletedDate = TouchInfo.CompletedDateTime;
                    }

                    dbTouchInfo.UpdatedBy = UserId;
                    dbTouchInfo.UpdatedDate = DateTime.Now;
                }
                else
                {
                    //Add touch info 
                    dbTouchInfo = new FAWTTouchInformation
                    {
                        FirstName = TouchInfo.FirstName,
                        LastName = TouchInfo.LastName,
                        StoreNumber = TouchInfo.StoreNumber,
                        QORId = TouchInfo.QORId,
                        TouchType = TouchInfo.TouchType,
                        SeqNumber = Convert.ToInt32(TouchInfo.SequenceNum),
                        FK_TouchStatusID = TouchInfo.TouchStatusID,
                        FK_TouchActionID = TouchInfo.ActionID,
                        FK_StageActionID = TouchInfo.StageActionID,
                        StarsID = TouchInfo.StarsId,
                        //StarsUnitID = TouchInfo.StarsUnitId,  //UNITID-CHECK
                        TouchTime = TouchInfo.TouchTime,
                        UnitNumber = TouchInfo.UnitName,
                        ScheduledDate = Convert.ToDateTime(TouchInfo.ScheduledDate.Value),
                        CompletedDate = TouchInfo.CompletedDateTime,
                        SLTouchStatus = TouchInfo.SLTouchStatus.ToString(),
                        Comments = TouchInfo.Comments,
                        PhoneNumber = TouchInfo.Phonenumber,
                        CreatedBy = UserId,
                        CreatedDate = DateTime.Now,

                        CustAddress1 = TouchInfo.CustAddress1,
                        CustAddress2 = TouchInfo.CustAddress2,
                        CustCity = TouchInfo.CustCity,
                        CustRegion = TouchInfo.CustRegion,
                        CustZip = TouchInfo.CustZip,

                        FromFirstName = TouchInfo.FromFirstName,
                        FromLastName = TouchInfo.FromLastName,
                        FromPhone = TouchInfo.FromPhone,
                        FromAddr1 = TouchInfo.FromAddr1,
                        FromAddr2 = TouchInfo.FromAddr2,
                        FromCity = TouchInfo.FromCity,
                        FromState = TouchInfo.FromState,
                        FromZip = TouchInfo.FromZip,

                        ToFirstName = TouchInfo.ToFirstName,
                        ToLastName = TouchInfo.ToLastName,
                        ToPhone = TouchInfo.ToPhone,
                        ToAddr1 = TouchInfo.ToAddr1,
                        ToAddr2 = TouchInfo.ToAddr2,
                        ToCity = TouchInfo.ToCity,
                        ToState = TouchInfo.ToState,
                        ToZip = TouchInfo.ToZip
                    };

                    context.FAWTTouchInformation.Add(dbTouchInfo);
                }

                context.SaveChanges();

                //This call to save history for noshow and return to warehouse
                if (TouchInfo.DontChangeStatusForTheFirstUpdateButChangeInSecondUpdate == true && TouchInfo.CanUpdateTouchStatusID)
                {
                    dbTouchInfo.FK_TouchStatusID = TouchInfo.TouchStatusID;
                    dbTouchInfo.FK_TouchActionID = TouchInfo.UpdateCustomActionID;

                    //IF status is not stage status then we have to make FK_StageActionID to null other wise get StageActionID and save it
                    bool isActionStage = context.FAWTTouchStatus.Any(s => s.PK_ID == dbTouchInfo.FK_TouchStatusID && s.StatusName == WATouchStatus.Stage.ToString() && s.IsActive == true);

                    if (isActionStage)
                        dbTouchInfo.FK_StageActionID = TouchInfo.StageActionID;
                    else
                        dbTouchInfo.FK_StageActionID = null;

                    context.SaveChanges();
                }
            }
        }



        public List<WATouchInfo> GetWATouchInformation(DateTime startDate, DateTime endDate, int StoreNumber, List<WATouchInfo> SLTouches)
        {
            using (var context = new PRSEntities())
            {
                string k = string.Join(",", SLTouches.Select(s => string.Format("{0}_{1}_{2}", s.QORId, s.TouchType, s.SequenceNum)).ToList());

                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                DataTable dtTouchInformation = new DataTable();

                using (var conn = context.Database.Connection)
                {
                    var connectionState = conn.State;
                    if (connectionState != ConnectionState.Open)
                        conn.Open();
                    using (var cmd = context.Database.Connection.CreateCommand())
                    {
                        cmd.CommandText = "USP_FAWT_GetWATouchInformation";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("StoreNumber", StoreNumber));
                        cmd.Parameters.Add(new SqlParameter("StartDate", startDate));
                        cmd.Parameters.Add(new SqlParameter("EndDATE", endDate));
                        cmd.Parameters.Add(new SqlParameter("TouchUniqueKeys", k));
                        using (var reader = cmd.ExecuteReader())
                        {
                            dtTouchInformation.Load(reader);
                        }
                    }

                    if (connectionState != ConnectionState.Closed)
                        conn.Close();
                }

                List<WATouchInfo> touchList = new List<WATouchInfo>();

                if (dtTouchInformation != null && dtTouchInformation.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow dr in dtTouchInformation.Rows)
                    {
                        WATouchInfo TouchInfo = new WATouchInfo
                        {
                            QORId = Convert.ToInt32(dr["QORId"]),
                            StoreNumber = Convert.ToInt32(dr["StoreNumber"]),
                            TouchTypeFull = Helper.GetFullTouchType(Convert.ToString(dr["TouchType"])),
                            SequenceNum = Convert.ToInt32(dr["SeqNumber"]),
                            StarsId = dr["StarsID"] != DBNull.Value ? Convert.ToInt32(dr["StarsID"]) : (int?)null,
                            //StarsUnitId = dr["StarsUnitID"] != DBNull.Value ? Convert.ToInt32(dr["StarsUnitID"]) : (int?)null,   //UNITID-CHECK
                            TouchStatusID = Convert.ToInt32(dr["FK_TouchStatusID"]),
                            TouchStatus = (WATouchStatus)Enum.Parse(typeof(WATouchStatus), Convert.ToString(dr["StatusName"])),
                            Comments = dr["Comments"] != DBNull.Value ? Convert.ToString(dr["Comments"]) : null,
                            UnitName = Convert.ToString(dr["UnitNumber"]),
                            ScheduledDate = dr["ScheduledDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduledDate"]) : (DateTime?)null,
                            TouchActionCategory = (WTTouchActionCategory)Enum.Parse(typeof(WTTouchActionCategory), Convert.ToString(dr["TouchActionCategory"])),
                            ActionName = Convert.ToString(dr["ActionName"]),
                            ActionID = Convert.ToInt32(dr["FK_TouchActionID"]),
                            StageActionID = dr["FK_StageActionID"] != DBNull.Value ? Convert.ToInt32(dr["FK_StageActionID"]) : (int?)null,
                            StageActionName = Convert.ToString(dr["ActionName"]),
                            FirstName = Convert.ToString(dr["FirstName"]),
                            LastName = Convert.ToString(dr["LastName"]),
                            TouchTime = Convert.ToString(dr["TouchTime"])
                        };

                        touchList.Add(TouchInfo);
                    }
                }

                return touchList;
            }
        }

        public EmailTemplate GetTouchCompleteEmailTemplate(int templateId)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.FAWTEmailTemplate.Where(t => t.PK_ID == templateId && t.IsActive == true)
                                                .Select(e => new EmailTemplate
                                                {
                                                    TemplateId = e.PK_ID,
                                                    EmailTemplateBody = e.TemplateHtml,
                                                    Subject = e.Subject
                                                })
                                                .FirstOrDefault();
            }
        }

        public WATouchTypesAndActionCategory GetTouchActionSetting(string TouchType, WTTouchActionCategory ActionCategory)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var TouchAndActionSettings = (from ts in context.FAWTTouchTypeAndActionCategory
                                              join sc in context.FAWTTouchActionCategory on ts.FK_ActionCategoryID equals sc.PK_ID
                                              join t in context.FAWTTouchType on ts.FK_TouchTypeID equals t.PK_ID
                                              where t.TouchType.Equals(TouchType, StringComparison.OrdinalIgnoreCase) && sc.ActionCategoryName.Equals(ActionCategory.ToString(), StringComparison.OrdinalIgnoreCase) && ts.IsActive == true
                                              select new WATouchTypesAndActionCategory
                                              {
                                                  CanUpdateStatusInSL = ts.CanUpdateStatusInSL,
                                                  CanSendNotification = ts.CanSendNotification,
                                                  FK_EmailTemplateIDs = ts.FK_EmailTemplateIDs,
                                                  CanUpdateLocalStatus = ts.CanUpdateLocalStatus,
                                                  UpdateLocalStatusID = ts.FK_UpdateLocalStatusID,
                                                  UpdateLocalStatusName = ts.FAWTTouchStatus != null ? ts.FAWTTouchStatus.StatusName : string.Empty
                                              })
                        .FirstOrDefault();

                return TouchAndActionSettings;
            }
        }

        public List<WATouchInfoHistory> GetTouchHistory(int QORID, string TouchType, int SeqNumber)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                return context.Database.SqlQuery<WATouchInfoHistory>(@"exec USP_FAWT_GetTouchHistoryByQorTouchTypeAndSeqNo @QorId, @TouchType, @SeqNumber",
                                                                   Helper.SqlParameterObj("QorId", QORID),
                                                                   Helper.SqlParameterObj("TouchType", TouchType),
                                                                   Helper.SqlParameterObj("SeqNumber", SeqNumber)).ToList();
            }
        }
        #endregion

        #region EmailLog
        public void InsertEmailLog(FAEmailHistoryLog LogDetails)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                FAEmailHistory oFAEmail = new FAEmailHistory()
                {
                    ApplicationName = LogDetails.ApplicationName,
                    IPAddress = LogDetails.IPAddress,
                    UserID = LogDetails.UserID,
                    Subject = LogDetails.Subject,
                    ToEmailIDs = LogDetails.ToEmailIDs,
                    FromEmailID = LogDetails.FromEmailID,
                    MethodName = LogDetails.MethodName,
                    EmailTypeID = LogDetails.EmailTypeID,
                    QORID = LogDetails.QORID,
                    TouchType = LogDetails.TouchType,
                    SLSeqNo = LogDetails.SLSeqNo,
                    StartTime = LogDetails.StartTime,
                    EndTime = LogDetails.EndTime,
                    CreatedDate = DateTime.Now,
                    CreatedBy = (int)LogDetails.UserID,
                    EmailTemplateId = LogDetails.TemplateID.ToString()
                };

                context.FAEmailHistory.Add(oFAEmail);
                context.SaveChanges();
            }
        }
        #endregion

        #region WareHouseTouchContainerStatus

        public List<FAWTTouchActions> GetActionsByTouchType(string TouchType, int StatusId, DateTime TouchScheduleDate, int StoreNumber)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                return context.Database.SqlQuery<FAWTTouchActions>(@"exec USP_FAWT_GetActionsByStatusTouchType @TouchScheduledDate, @TouchType, @StatusId, @StoreNumber",
                                                                           Helper.SqlParameterObj("TouchScheduledDate", TouchScheduleDate),
                                                                           Helper.SqlParameterObj("TouchType", TouchType),
                                                                           Helper.SqlParameterObj("StatusId", StatusId),
                                                                           Helper.SqlParameterObj("StoreNumber", StoreNumber)
                                                                           ).AsQueryable().OrderBy(x => x.ActionName).ToList();
            }
        }

        public List<PRFacAppWF.Entities.Touches.FAWTTouchStatus> GetWHTStatusList()
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                //StoreNumber = null means, getting all global statuses
                return (from faWATStatusCat in this.prsEntities.FAWTTouchStatus.Where(a => a.IsActive == true && a.canDisplay == true)
                        select new PRFacAppWF.Entities.Touches.FAWTTouchStatus
                        {
                            TouchStatusID = faWATStatusCat.PK_ID,
                            SequenceOrder = faWATStatusCat.SequenceOrder,
                            Description = faWATStatusCat.Description,
                            IsActive = faWATStatusCat.IsActive,
                            DispStatusName = faWATStatusCat.DispStatusName,
                            StatusName = faWATStatusCat.StatusName
                        }).Distinct().OrderBy(x => x.StatusName).ToList();
            }
        }

        public List<FAWTTouchActions> SearchContainerStatus(int StoreNumber, string StatusName)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                return context.Database.SqlQuery<FAWTTouchActions>(@"exec USP_FAWT_GetContainerStatusForFacility @StatusName, @StoreNumber",
                                                                          Helper.SqlParameterObj("StatusName", StatusName),
                                                                          Helper.SqlParameterObj("StoreNumber", StoreNumber)).AsQueryable().ToList();
            }
        }

        public void AddContainerStatus(FAWTTouchActions oContainerStatus, int UserId)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                if (oContainerStatus.IsGlobalStatus == 1 || oContainerStatus.ActionID == 0)
                {
                    int dbStagingID = context.FAWTTouchAction.Where(x => x.ActionName == "Stage").FirstOrDefault().PK_ID;

                    var dbActionInfo = new models.FAWTTouchAction
                    {
                        FK_ActionCategoryID = oContainerStatus.ActionCategoryID,
                        FK_TouchActionID = oContainerStatus.ActionID == 0 ? dbStagingID : oContainerStatus.ActionID,
                        Status = oContainerStatus.Status,
                        StoreNumber = oContainerStatus.StoreNumber,
                        ActionName = oContainerStatus.ActionName,
                        Description = oContainerStatus.Description,
                        IsActive = true,
                        CreatedBy = UserId,
                        CreatedDate = DateTime.Now
                    };
                    context.FAWTTouchAction.Add(dbActionInfo);
                }
                else
                {
                    var dbTouchInfo = context.FAWTTouchAction.Where(t => t.PK_ID == oContainerStatus.ActionID).FirstOrDefault();

                    if (dbTouchInfo != null)
                    {
                        dbTouchInfo.FK_ActionCategoryID = oContainerStatus.ActionCategoryID;
                        dbTouchInfo.Status = oContainerStatus.Status;
                        dbTouchInfo.StoreNumber = oContainerStatus.StoreNumber;
                        dbTouchInfo.ActionName = oContainerStatus.ActionName;
                        dbTouchInfo.Description = oContainerStatus.Description;
                    }
                }

                context.SaveChanges();
            }
        }

        public void ToggleActiveAndInactiveContainerStatus(int PKID, string Status, int UserId, int IsGlobalStatus, int StoreNumber)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var ts = context.FAWTTouchAction.Where(x => x.PK_ID == PKID).FirstOrDefault();

                if (IsGlobalStatus == 1)
                {
                    var dbTouchInfo = new models.FAWTTouchAction
                    {
                        FK_ActionCategoryID = ts.FK_ActionCategoryID,
                        FK_TouchActionID = ts.PK_ID,
                        Status = "N",
                        StoreNumber = StoreNumber,
                        ActionName = ts.ActionName,
                        Description = ts.Description,
                        IsActive = true,
                        CreatedBy = UserId,
                        CreatedDate = DateTime.Now
                    };
                    context.FAWTTouchAction.Add(dbTouchInfo);
                }
                else if (ts != null)
                {
                    ts.Status = Status == "InActive" ? "N" : "A";
                    ts.UpdatedBy = UserId;
                    ts.UpdatedDate = DateTime.Now;
                }

                context.SaveChanges();
            }
        }
        #endregion

        #region FA Global Settings

        public FAGlobalSettings GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey SettingsKey)
        {
            return GetGlobalSettingsVal(SettingsKey, null);
        }

        public FAGlobalSettings GetGlobalSettingsVal(Entities.Enums.FAGlobalSettingsKey SettingsKey, string StoreNumber)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                return context.Database.SqlQuery<FAGlobalSettings>(@"exec USP_FA_GetGlobalSettingsValueByKey @StoreNumber, @SettingsKey",
                                                                    Helper.SqlParameterObj("StoreNumber", StoreNumber),
                                                                    Helper.SqlParameterObj("SettingsKey", SettingsKey.ToString())).FirstOrDefault();
            }
        }

        #endregion

        #region sendGridEmail

        public USS_EmailConfiguration GetEmailTemplateDetails(int TemplateID)
        {
            using (var context = new PRSEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                return context.USS_EmailConfiguration.Where(x => x.Id == TemplateID && x.IsActive == true).FirstOrDefault();
            }
        }

        #endregion


    }
}
