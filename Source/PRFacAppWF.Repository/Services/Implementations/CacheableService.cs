﻿namespace PRFacAppWF.Repository.Services
{
    using System;
    using System.Web;
    using System.Web.Caching;

    // TODO: Nice to have would be a way to periodically track the cache memory usage.
    //      HttpRuntime.Cache.EffectivePrivateBytesLimit would be good information
    public class CacheableService : ICacheable
    {
        public const int FiveMinutes = 5;                   // 5 minutes
        public const int ThirtyMinutes = 30;                // 30 minutes
        public const int OneHourInMinutes = 60;             // 1 hour
        public const int TwoHoursInMinutes = 60 * 2;        // 2 hours
        public const int FourHoursInMinutes = 60 * 4;       // 4 hours
        public const int SixHoursInMinutes = 60 * 6;        // 6 hours
        public const int EightHoursInMinutes = 60 * 8;      // 8 hours
        public const int TenHoursInMinutes = 60 * 10;       // 10 hours
        public const int TwelveHoursInMinutes = 60 * 12;    // 12 hours
        public const int OneDayInMinutes = 60 * 24;         // 1 day
        public const int TwoDaysInMinutes = 60 * 24 * 2;    // 2 days
        public const int OneWeekInMinutes = 60 * 24 * 7;    // 1 week
        public const int TwoWeeksInMinutes = 60 * 24 * 14;  // 2 weeks
        public const int OneMonthInMinutes = 60 * 24 * 30;  // 1 month

        CacheItemRemovedCallback onRemove = null;

        // Overwrites previous value if it exists
        public void InsertToCache(string key, object value, int durationInMinutes)
        {
            if (key == null || value == null)
            {
                return;
            }

            this.onRemove = new CacheItemRemovedCallback(this.RemovedCallback);

            // TODO: Log adding to the cache
            HttpRuntime.Cache.Insert(
                key,
                value,
                null,
                DateTime.Now.AddMinutes(durationInMinutes),
                Cache.NoSlidingExpiration,
                CacheItemPriority.Default,
                this.onRemove);
        }

        // Overwrites previous value if it exists
        public void RemoveFromCache(string key)
        {
            if (key == null)
            {
                return;
            }
            HttpRuntime.Cache.Remove(key);
        }

        // Overwrites previous value if it exists
        // The sliding expiration keeps the item in memory for a minimum of the sliding expiration and then
        // resets the count each time the item is accessed.
        public void InsertToCacheSlidingExpiration(string key, object value, int slidingExpirationInMinutes)
        {
            if (key == null || value == null)
            {
                return;
            }

            this.onRemove = new CacheItemRemovedCallback(this.RemovedCallback);

            // TODO: Log adding to the cache
            HttpRuntime.Cache.Insert(
                key,
                value,
                null,
                Cache.NoAbsoluteExpiration,
                TimeSpan.FromMinutes(slidingExpirationInMinutes),
                CacheItemPriority.Default,
                this.onRemove);
        }

        public object GetFromCache(string key)
        {
            return HttpRuntime.Cache.Get(key);
        }

        public void RemovedCallback(string k, object v, CacheItemRemovedReason r)
        {
            // TODO: Log this event and ensure that the key and reason are mentioned
        }
    }
}
