﻿namespace PRFacAppWF.Repository.Services
{
    public interface ICacheable
    {
        // Overwrites previous value if it exists
        void InsertToCache(string key, object value, int durationInMinutes);

        void RemoveFromCache(string key);

        object GetFromCache(string key);
    }
}
