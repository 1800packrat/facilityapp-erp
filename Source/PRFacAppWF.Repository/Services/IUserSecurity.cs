﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PRFacAppWF.Repository.models;
using PR.Entities;
using PRFacAppWF.models;
using PR.LocalLogisticsSolution.Model;
using PRFacAppWF.Entities.Enums;
using PRFacAppWF.Entities;
using PRFacAppWF.Entities.UserManagement;
using PRFacAppWF.Entities.DriverSchedule;

namespace PRFacAppWF.Repository.Services
{
    public interface IUserSecurity
    {
        List<PRFacAppWF.Entities.UserManagement.FADTOGroupMaster> ListRoles();

        PRFacAppWF.Entities.UserManagement.FADTOGroupMaster RoleInfoById(int rollID);

        PRFacAppWF.Entities.UserManagement.FADTOUserMaster GetUserInfoById(int UserID);


        List<Entities.UserManagement.FADTOModuleArea> ListAreas();

        //List<FA_ModuleArea> ListAreas();

        void SaveRole(int RoleId, string RoleName, string RoleDescription, PRFacAppWF.models.ModuleAccessRelation[] moduleIds, int LoginUserID);

        void ToggleRoleStatus(int RoleId, string Status, int LoginUserID);

        bool CheckRoleName(string RoleName, int RoleId);

        List<PRFacAppWF.Entities.UserManagement.FADTOModule> GetModules();

        List<PRDriverSchedule> AddAndGetDriverScheduleHours(DateTime StartDate, DateTime EndDate, int LogisticsUserId, int StoreNo, int CreatedBy, int DATEPART);
        int SaveDriverHourChangesToDB(string rowIds, string startTime, string endTime, int userId, string storeOpen);
        
        //FAUserMaster GetUserDetailsByID(int UserID);

        List<FAUserMaster> GetAllUserDetails(string Name, int RoleNo, bool IsActive);

        void SaveUserDetails(PRFacAppWF.models.User objUser, FacilityUserRelation[] selectedFailities, int LoginUserID);

        void SaveDriverRoverAndPermanentTransfer(FADTOUserAndFacilityAccess driverTransferDetails, int UserId);

        bool CheckUserName(string UserName, int UserId);

        //void InstUserFacility(int UserId, FacilityUserRelation[] selectedFailities);

        //List<PRFacAppWF.models.FacilityRegions> GetFacilityRegions();

        void ToggleUserStatus(int UserId, string Status, int LoginUserID);

        List<RDFacility> GetFacilitiesList();

        List<Entities.UserManagement.FADTOFacility> GetDTOFacilityList();

        List<FAModule> ListFAModule();

        //LoginStatus Login(string username, string password, ref LogisticsUser user);

        FAPasswordConfig GetPasswordConfig();

        bool CheckOldPassword(int UserID, string Password);

        bool IsPasswordAlreadyUsed(int UserID, string NewPassword, int LastNumberOfPasswords);

        void UpdateNewPassword(int UserID, string NewPassword, string oldPassword, bool IsAutoPassword);

        FAUserMaster GetUserDetailsByUserName(string UserName);
        
        void SaveDriverScheduleHours(List<DriverScheduleHour> lstDriverScheduleHours, int driverID);

        List<FADTOUserMaster> GetDuplicateUsersByLastname(string LastName, DateTime StartDate);

        List<FADTOUserStatus> GetUserStatus();

        List<FADTOLicenseClass> GetLicenseClass();

        int GetStoreRowID(string LocationCode);

        //List<Entities.DriverSchedule.DriverFacility> GetDTOGetDriverFacilityList(string LocationCode, int? UserID);

        List<Entities.UserManagement.FADTOFacility> GetDTODriverFacilityList(int driverID);
        
        bool checkUserNameAvailable(string UserName);

        List<string> suggestedUserName(string FirstName, string LastName);

        void SaveDriverDetails(Entities.DriverSchedule.SaveDriverRequest driverForm, int UserID);

        int RoleGroupIDByName(string groupName);

        List<DriverScheduleHour> GetDriverScheduleHourDetails(int facilityStoreNo, DateTime startDate, int noOfDays, int driverID, int weekday);

        bool CanThisUserTransferThisDriverForRelocation(int driverID, SiteInfo currentSiteInfo, DateTime startDate, DateTime? endDate);

        bool CanThisUserTransferThisDriverForRover(FADTOUserAndFacilityAccess driverTransferDetails, SiteInfo currentSiteInfo);

        List<Entities.DriverSchedule.DriverFacility> GetDriverRoverFacilityList(string LocationCode, int DriverId);

        void UpdateDriverRoverFacilityInfo(DriverFacility roverFacility, int CurrentUserID);

        SaveRoverStatus CanSaveRoverRecord(DriverFacility roverFacility, int CurrentStoreNo);

        List<DriverInfoWithWeekSchedule> GetDriversScheduleHrsFromSP(DateTime Startdate, DateTime EndDate, string LocationCode);

        FAUserMaster GetUserDetailsByDriverId(int DriverId);

        FAUserMaster GetUserDetailsByUserId(int UserID);

        bool CanScheduleDriverHoursForThisDay(DateTime scheduleDate, int DriverId, int StoreNo);

        bool AssingDriverToFacility(int driverId, int storeNo, int createdUserId);
    }
}
