//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PRFacAppWF.Repository.models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UnitPart
    {
        public int PartsId { get; set; }
        public string PartsName { get; set; }
    }
}
