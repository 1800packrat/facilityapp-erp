//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PRFacAppWF.Repository.models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CAPTruckOperatingHour
    {
        public long CAPTruckOperatingHourId { get; set; }
        public int FacilityId { get; set; }
        public Nullable<int> TransferFacilityId { get; set; }
        public Nullable<System.DateTime> TransferOut { get; set; }
        public Nullable<System.DateTime> TransferIn { get; set; }
        public int TruckId { get; set; }
        public int TruckOHReasonId { get; set; }
        public System.DateTime Startdate { get; set; }
        public Nullable<System.DateTime> Enddate { get; set; }
        public Nullable<short> MinutesAvailable { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public int Status { get; set; }
        public Nullable<int> TransferReasonId { get; set; }
    }
}
