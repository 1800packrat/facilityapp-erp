//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PRFacAppWF.Repository.models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FA_ModuleArea
    {
        public FA_ModuleArea()
        {
            this.FAModule = new HashSet<FAModule>();
        }
    
        public int PK_ID { get; set; }
        public Nullable<int> FK_ApplicationID { get; set; }
        public string Area { get; set; }
        public bool Status { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<FAModule> FAModule { get; set; }
        public virtual FAApplications FAApplications { get; set; }
    }
}
