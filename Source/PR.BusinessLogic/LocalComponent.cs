using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using PR.DataHandler;
using PR.Entities;
using PR.ExternalInterfaces;
using PR.UtilityLibrary;

namespace PR.BusinessLogic
{
    public class LocalComponent : BaseBusinessLogicClass
    {
        public LocalComponent()
            : base()
        {
        }

        //private string _userId;

        

        /// <summary>
        /// this method is used by non CW apps .
        /// TODO : make it config base for third party service
        /// </summary>
        /// <param name="zip1"></param>
        /// <param name="zip2"></param>
        /// <returns></returns>
        [Obsolete("This method is deprecated, please use ValidteZipcodesWithTrailers instead. Add add TrailerServices Settings in your web.config file. Possible values for this key 'ABF', 'OD' etc..")]
        public ZipValidationInfo ValidateZipCodes(string zip1, string zip2)
        {
            ZipCodeInfo zip1Info = new ZipCodeInfo();
            ZipCodeInfo zip2Info = new ZipCodeInfo();
            ZipValidationInfo zipValidationInfo = new ZipValidationInfo();

            string connectionString = String.Empty;

            zip1Info = GetFirstZipCodeInfo(zip1);
            zip2Info = GetSecondZipCodeInfo(zip2, zip1Info);

            zipValidationInfo.Zip1 = zip1Info;
            zipValidationInfo.Zip2 = zip2Info;

            //Set the move type value
            if ((zip1Info.ErrorCode == String.Empty) && (zip2Info.ErrorCode == String.Empty))
            {
                if (zip1Info.IsESAT == false && zip1Info.IsLDMServiced == false && zip1Info.IsServiced == false) // Origination is Unserviced
                {
                    zipValidationInfo.MoveType = PREnums.MoveType.NoService;
                    zipValidationInfo.ErrorCode = PRErrorCode.Move_Not_Supported;
                    zipValidationInfo.ErrorMessage = PRErrorMessages.Move_Not_Supported;
                }
                else if (zip1Info.IsServiced)
                {
                    if (zip2Info.IsServiced)
                    {
                        if ((zip1Info.CorpCode == zip2Info.CorpCode) && (zip1Info.LocationCode == zip2Info.LocationCode))
                        {
                            zipValidationInfo.MoveType = PREnums.MoveType.Local;
                        }
                        else
                        {
                            zipValidationInfo.MoveType = PREnums.MoveType.LDM;
                        }
                    }
                    else if (zip2Info.IsESAT)
                    {
                        if ((zip1Info.CorpCode == zip2Info.CorpCode) && (zip1Info.LocationCode == zip2Info.LocationCode))
                        {
                            zipValidationInfo.MoveType = PREnums.MoveType.ESAT;
                        }
                    }
                    else if (zip2Info.IsLDMServiced)
                    {
                        if ((zip1Info.CorpCode == zip2Info.CorpCode) && (zip1Info.LocationCode == zip2Info.LocationCode))
                        {
                            zipValidationInfo.MoveType = PREnums.MoveType.TransferFacility;
                        }
                        else
                        {
                            zipValidationInfo.MoveType = PREnums.MoveType.LDM;
                        }
                    }
                    else // Zip2 not serviced
                    {
                        zipValidationInfo.MoveType = PREnums.MoveType.TransferFacility;
                    }
                }
                else if (zip1Info.IsLDMServiced)
                {
                    if (zip2Info.IsServiced)
                    {
                        if ((zip1Info.CorpCode == zip2Info.CorpCode) && (zip1Info.LocationCode == zip2Info.LocationCode))
                        {
                            zipValidationInfo.MoveType = PREnums.MoveType.TransferFacility;
                        }
                        else
                        {
                            zipValidationInfo.MoveType = PREnums.MoveType.LDM;
                        }
                    }
                    else if (zip2Info.IsLDMServiced)
                    {
                        if ((zip1Info.CorpCode == zip2Info.CorpCode) && (zip1Info.LocationCode == zip2Info.LocationCode))
                        {
                            zipValidationInfo.MoveType = PREnums.MoveType.TransferFacility;
                        }
                        else
                        {
                            zipValidationInfo.MoveType = PREnums.MoveType.LDM;
                        }
                    }
                    else if (zip2Info.IsESAT)
                    {
                        if ((zip1Info.CorpCode == zip2Info.CorpCode) && (zip1Info.LocationCode == zip2Info.LocationCode))
                        {
                            zipValidationInfo.MoveType = PREnums.MoveType.TransferFacility;
                        }
                    }
                    else // Zip2 not serviced
                    {
                        zipValidationInfo.MoveType = PREnums.MoveType.TransferFacility;
                    }
                }
            }

            //If the zip code(s) are unserviced check for Old Dominion move
            if (zipValidationInfo.MoveType == PREnums.MoveType.NoService)
            {
                LocalDataHandler handler = new LocalDataHandler();
                if (zipValidationInfo.Zip1.IsServiced || zipValidationInfo.Zip1.IsLDMServiced || zipValidationInfo.Zip1.IsESAT)
                {
                    if (handler.IsOldDominionZip(zipValidationInfo.Zip2.ZipCode))
                    {
                        zipValidationInfo.MoveType = PREnums.MoveType.OD;
                    }
                }
                else if (zipValidationInfo.Zip2.IsServiced || zipValidationInfo.Zip2.IsLDMServiced || zipValidationInfo.Zip2.IsESAT)
                {
                    if (handler.IsOldDominionZip(zipValidationInfo.Zip1.ZipCode))
                    {
                        zipValidationInfo.MoveType = PREnums.MoveType.OD;
                    }
                }
                else if (handler.IsOldDominionZip(zipValidationInfo.Zip1.ZipCode))
                {
                    if (handler.IsOldDominionZip(zipValidationInfo.Zip2.ZipCode))
                    {
                        zipValidationInfo.MoveType = PREnums.MoveType.OD;
                    }
                }

            }

            return zipValidationInfo;
        }
        
        #region New zipcode validation methods

        public ZipCodeInfo GetFirstZipCodeInfo(string zip)
        {
            LocalDataHandler dh = new LocalDataHandler();
            ZipCodeInfo zipCodeInfo = new ZipCodeInfo();
            DataSet dsZipCode = new DataSet();
            DataSet dsLocationCorp = new DataSet();
            bool isZipCodeServicedOrLDMServiced = false;
            int siteId = 0;
            string dbName = String.Empty;
            string slConnString = String.Empty;

            zipCodeInfo.ZipCode = zip;
            dsZipCode = dh.GetZipCodeInfoFromGlobalView(zip);

            //Check for the serviced by zip code
            if (dsZipCode.Tables[0].Rows.Count > 0)
            {
                if (dsZipCode.Tables[0].Select("bServicedBy=1").Length > 0)
                {
                    zipCodeInfo.IsServiced = true;
                    dbName = dsZipCode.Tables[0].Select("bServicedBy=1")[0]["DBName"].ToString();
                    siteId = Convert.ToInt32(dsZipCode.Tables[0].Select("bServicedBy=1")[0]["SiteID"]);
                    if (Data._connStringPR.IndexOf("PRSLLocalTest") > 0)
                        slConnString = Data._connStringPR.Replace("PRSLLocalTest", dbName);
                    else
                        slConnString = Data._connStringPR.Replace("PRSLLocal", dbName);
                    isZipCodeServicedOrLDMServiced = true;
                    if (dsZipCode.Tables[0].Select("bServicedBy=1").Length > 0)
                    {
                        foreach (DataRow drow in dsZipCode.Tables[0].Select("bServicedBy=1"))
                        {
                            if (drow["MarketId"] != DBNull.Value && Convert.ToString(drow["MarketId"]) != "")
                            {
                                zipCodeInfo.MarketId = Convert.ToInt32(drow["MarketId"]);
                                break;
                            }
                        }
                    }
                }
                else if (dsZipCode.Tables[0].Select("bLDMServiced=1").Length > 0)
                {
                    zipCodeInfo.IsLDMServiced = true;
                    dbName = dsZipCode.Tables[0].Select("bLDMserviced=1")[0]["DBName"].ToString();
                    siteId = Convert.ToInt32(dsZipCode.Tables[0].Select("bLDMserviced=1")[0]["SiteID"]);
                    //slConnString = Data._connStringPR.Replace("PRSLLocal", dbName);
                    if (Data._connStringPR.IndexOf("PRSLLocalTest") > 0)
                        slConnString = Data._connStringPR.Replace("PRSLLocalTest", dbName);
                    else
                        slConnString = Data._connStringPR.Replace("PRSLLocal", dbName);
                    isZipCodeServicedOrLDMServiced = true;

                }
            }

            if (isZipCodeServicedOrLDMServiced)
            {
                dsLocationCorp = dh.GetCorpCodeAndLocationCode(siteId, slConnString);
                if (dsLocationCorp.Tables.Count > 0)
                {
                    if (dsLocationCorp.Tables[0].Rows.Count > 0)
                    {
                        zipCodeInfo.CorpCode = dsLocationCorp.Tables[0].Rows[0]["sCorpCode"].ToString();
                        zipCodeInfo.LocationCode = dsLocationCorp.Tables[0].Rows[0]["sLocationCode"].ToString();
                        zipCodeInfo.SiteId = siteId;
                        PRAddressAPI pcmInterface = new PRAddressAPI();
                        Address siteAddress = GetSiteInformation(zipCodeInfo.LocationCode).SiteAddress;
                        zipCodeInfo.DistanceFromSite = pcmInterface.GetDistance(siteAddress, new Address() { Zip = zip }).Distance;
                    }
                }
            }
            else
            {
                //If the zip code is not locally or LDM serviced set the error message
                zipCodeInfo.ErrorCode = PRErrorCode.Zip_Code_Not_Serviced;
                zipCodeInfo.ErrorMessage = PRErrorMessages.Zip_Code_Not_Serviced;
            }


            return zipCodeInfo;
        }

        private ZipCodeInfo GetSecondZipCodeInfo(string zip, ZipCodeInfo zip1Info)
        {
            LocalDataHandler dh = new LocalDataHandler();
            ZipCodeInfo zipCodeInfo = new ZipCodeInfo();
            DataSet dsZipCode = new DataSet();
            DataSet dsLocationCorp = new DataSet();
            bool isZipCodeServicedOrLDMServiced = false;
            int siteId = 0;
            string dbName = String.Empty;
            string slConnString = String.Empty;

            zipCodeInfo.ZipCode = zip;
            dsZipCode = dh.GetZipCodeInfoFromGlobalView(zip);

            //Check if the second zip code is either serviced or ESAT serviced by the first zip code facility
            if (dsZipCode.Tables[0].Rows.Count > 0)
            {
                if (zip1Info.IsServiced)
                {
                    if (dsZipCode.Tables[0].Select("SiteID=" + zip1Info.SiteId).Length > 0)
                    {
                        if (Convert.ToBoolean(dsZipCode.Tables[0].Select("SiteID=" + zip1Info.SiteId)[0]["bServicedBy"]))
                        {
                            zipCodeInfo.IsServiced = true;
                            isZipCodeServicedOrLDMServiced = true;
                            siteId = zip1Info.SiteId;
                            dbName = dsZipCode.Tables[0].Select("SiteID=" + zip1Info.SiteId)[0]["DBName"].ToString();
                            //slConnString = Data._connStringPR.Replace("PRSLLocal", dbName);
                            if (Data._connStringPR.IndexOf("PRSLLocalTest") > 0)
                                slConnString = Data._connStringPR.Replace("PRSLLocalTest", dbName);
                            else
                                slConnString = Data._connStringPR.Replace("PRSLLocal", dbName);
                            if (dsZipCode.Tables[0].Select("bServicedBy=1")[0]["MarketId"] != DBNull.Value)
                            {
                                zipCodeInfo.MarketId = Convert.ToInt32(Convert.ToInt32(dsZipCode.Tables[0].Select("bServicedBy=1")[0]["MarketId"]));
                            }
                        }
                        else if (Convert.ToBoolean(dsZipCode.Tables[0].Select("SiteID=" + zip1Info.SiteId)[0]["bESATServiced"]))
                        {
                            zipCodeInfo.IsESAT = true;
                            isZipCodeServicedOrLDMServiced = true;
                            siteId = zip1Info.SiteId;
                            dbName = dsZipCode.Tables[0].Select("SiteID=" + zip1Info.SiteId)[0]["DBName"].ToString();

                            if (Data._connStringPR.IndexOf("PRSLLocalTest") > 0)
                                slConnString = Data._connStringPR.Replace("PRSLLocalTest", dbName);
                            else
                                slConnString = Data._connStringPR.Replace("PRSLLocal", dbName);
                            if (dsZipCode.Tables[0].Select("bESATServiced=1")[0]["MarketId"] != DBNull.Value)
                            {
                                zipCodeInfo.MarketId = Convert.ToInt32(Convert.ToInt32(dsZipCode.Tables[0].Select("bESATServiced=1")[0]["MarketId"]));
                            }
                        }
                    }
                }

                if (!(zipCodeInfo.IsServiced) && !(zipCodeInfo.IsESAT))
                {
                    if (dsZipCode.Tables[0].Select("bServicedBy=1").Length > 0)
                    {
                        zipCodeInfo.IsServiced = true;
                        dbName = dsZipCode.Tables[0].Select("bServicedBy=1")[0]["DBName"].ToString();
                        siteId = Convert.ToInt32(dsZipCode.Tables[0].Select("bServicedBy=1")[0]["SiteID"]);

                        if (Data._connStringPR.IndexOf("PRSLLocalTest") > 0)
                            slConnString = Data._connStringPR.Replace("PRSLLocalTest", dbName);
                        else
                            slConnString = Data._connStringPR.Replace("PRSLLocal", dbName);
                        isZipCodeServicedOrLDMServiced = true;
                        if (dsZipCode.Tables[0].Select("bServicedBy=1")[0]["MarketId"] != DBNull.Value)
                        {
                            zipCodeInfo.MarketId = Convert.ToInt32(Convert.ToInt32(dsZipCode.Tables[0].Select("bServicedBy=1")[0]["MarketId"]));
                        }
                    }
                    else if (dsZipCode.Tables[0].Select("bLDMServiced=1").Length > 0)
                    {
                        zipCodeInfo.IsLDMServiced = true;
                        if (dsZipCode.Tables[0].Select("bLDMserviced=1 and SiteID=" + zip1Info.SiteId).Length > 0)
                        {
                            dbName = dsZipCode.Tables[0].Select("bLDMserviced=1 and SiteID=" + zip1Info.SiteId)[0]["DBName"].ToString();
                            siteId = Convert.ToInt32(dsZipCode.Tables[0].Select("bLDMserviced=1 and SiteID=" + zip1Info.SiteId)[0]["SiteID"]);
                        }
                        else
                        {
                            dbName = dsZipCode.Tables[0].Select("bLDMserviced=1")[0]["DBName"].ToString();
                            siteId = Convert.ToInt32(dsZipCode.Tables[0].Select("bLDMserviced=1")[0]["SiteID"]);
                        }

                        if (Data._connStringPR.IndexOf("PRSLLocalTest") > 0)
                            slConnString = Data._connStringPR.Replace("PRSLLocalTest", dbName);
                        else
                            slConnString = Data._connStringPR.Replace("PRSLLocal", dbName);
                        isZipCodeServicedOrLDMServiced = true;

                    }
                }
            }

            if (isZipCodeServicedOrLDMServiced)
            {
                dsLocationCorp = dh.GetCorpCodeAndLocationCode(siteId, slConnString);
                if (dsLocationCorp.Tables.Count > 0)
                {
                    if (dsLocationCorp.Tables[0].Rows.Count > 0)
                    {
                        zipCodeInfo.CorpCode = dsLocationCorp.Tables[0].Rows[0]["sCorpCode"].ToString();
                        zipCodeInfo.LocationCode = dsLocationCorp.Tables[0].Rows[0]["sLocationCode"].ToString();
                        zipCodeInfo.SiteId = siteId;
                    }
                }
            }
            else
            {
                //If the zip code is not locally or LDM serviced set the error message
                zipCodeInfo.ErrorCode = PRErrorCode.Zip_Code_Not_Serviced;
                zipCodeInfo.ErrorMessage = PRErrorMessages.Zip_Code_Not_Serviced;
            }

            return zipCodeInfo;
        }

        #endregion New zipcode validation methods
        
        public bool IsZipCodeESATServiced(string zip, string locCode)
        {
            DataSet dsZipCode = new DataSet();
            bool isESATServiced = false;
            LocalDataHandler dh = new LocalDataHandler();
            dsZipCode = dh.GetZipCodeInfoFromGlobalView(zip);
            if (dsZipCode.Tables.Count > 0)
            {
                if (dsZipCode.Tables[0].Select("bESATServiced=1").Length > 0)
                {
                    foreach (DataRow dr in dsZipCode.Tables[0].Select("bESATServiced=1"))
                    {
                        if (Convert.ToInt32(dr["SiteID"]) == GetSiteInformation(locCode).SiteId)
                        {
                            isESATServiced = true;
                            break;
                        }
                    }
                }
            }
            return isESATServiced;
        }

        public SiteInfo GetSiteInformation(string locationCode)
        {
            LocalDataHandler dh = new LocalDataHandler();
            DataSet dsSite = new DataSet();
            SiteInfo siteInfo = new SiteInfo();
            dsSite = dh.GetRDFacilityData(locationCode);

            if (dsSite.Tables.Count > 0)
            {
                if (dsSite.Tables[0].Rows.Count > 0)
                {
                    siteInfo.SiteId = Convert.ToInt32(dsSite.Tables[0].Rows[0]["SLSiteId"]);
                    siteInfo.LocationCode = dsSite.Tables[0].Rows[0]["SLLocCode"].ToString();
                    siteInfo.GlobalSiteNum = dsSite.Tables[0].Rows[0]["CompDBAName"].ToString();
                    //siteInfo.ContactName = dsSite.Tables[0].Rows[0]["sContactName"].ToString();
                    siteInfo.SiteAddress.AddressLine1 = dsSite.Tables[0].Rows[0]["Street1"].ToString();
                    siteInfo.SiteAddress.AddressLine2 = dsSite.Tables[0].Rows[0]["Street2"].ToString();
                    siteInfo.SiteAddress.City = dsSite.Tables[0].Rows[0]["City"].ToString();
                    siteInfo.SiteAddress.State = dsSite.Tables[0].Rows[0]["State"].ToString();
                    siteInfo.SiteAddress.Zip = dsSite.Tables[0].Rows[0]["ZipCode"].ToString();
                    siteInfo.SiteAddress.Latitude = dsSite.Tables[0].Rows[0]["Latitude"].ToString();
                    siteInfo.SiteAddress.Longitude = dsSite.Tables[0].Rows[0]["Longitude"].ToString();
                    siteInfo.SiteAddress.AddressType = PREnums.AddressType.Warehouse;
                    siteInfo.LegalName = dsSite.Tables[0].Rows[0]["CompDBAName"].ToString();
                    siteInfo.EMail = dsSite.Tables[0].Rows[0]["Email"].ToString();
                    siteInfo.MarketId = dsSite.Tables[0].Rows[0]["FK_MarketId"] != DBNull.Value ? Convert.ToInt32(dsSite.Tables[0].Rows[0]["FK_MarketId"]) : (int?)null;

                    //siteInfo.FacCode = dsSite.Tables[0].Rows[0]["FacCode"].ToString();
                }
            }
            return siteInfo;
        }

        public SiteInfo GetSiteInformationWithMarkets(string siteNumber)
        {
            LocalDataHandler dh = new LocalDataHandler();
            DataSet dsSite = new DataSet();
            SiteInfo siteInfo = new SiteInfo();
            dsSite = dh.GetRDFacilityDataByStoreNumber(siteNumber);

            if (dsSite.Tables.Count > 0)
            {
                if (dsSite.Tables[0].Rows.Count > 0)
                {
                    siteInfo.SiteId = Convert.ToInt32(dsSite.Tables[0].Rows[0]["SLSiteId"]);
                    siteInfo.LocationCode = dsSite.Tables[0].Rows[0]["SLLocCode"].ToString();
                    siteInfo.GlobalSiteNum = dsSite.Tables[0].Rows[0]["StoreNo"].ToString();
                    //siteInfo.ContactName = dsSite.Tables[0].Rows[0]["sContactName"].ToString();
                    siteInfo.SiteAddress.AddressLine1 = dsSite.Tables[0].Rows[0]["Street1"].ToString();
                    siteInfo.SiteAddress.AddressLine2 = dsSite.Tables[0].Rows[0]["Street2"].ToString();
                    siteInfo.SiteAddress.City = dsSite.Tables[0].Rows[0]["City"].ToString();
                    siteInfo.SiteAddress.State = dsSite.Tables[0].Rows[0]["State"].ToString();
                    siteInfo.SiteAddress.Zip = dsSite.Tables[0].Rows[0]["ZipCode"].ToString();
                    siteInfo.SiteAddress.Latitude = dsSite.Tables[0].Rows[0]["Latitude"].ToString();
                    siteInfo.SiteAddress.Longitude = dsSite.Tables[0].Rows[0]["Longitude"].ToString();
                    siteInfo.SiteAddress.AddressType = PREnums.AddressType.Warehouse;
                    siteInfo.LegalName = dsSite.Tables[0].Rows[0]["CompDBAName"].ToString();
                    siteInfo.EMail = dsSite.Tables[0].Rows[0]["Email"].ToString();
                    siteInfo.MarketId = dsSite.Tables[0].Rows[0]["FK_MarketId"] != DBNull.Value ? Convert.ToInt32(dsSite.Tables[0].Rows[0]["FK_MarketId"]) : (int?)null;

                    if (siteInfo.MarketId.HasValue)
                    {
                        var marketFacilities = dh.GetRDFacilitiesDataByMarket(siteInfo.MarketId.Value);

                        if (marketFacilities != null && marketFacilities.Tables != null && marketFacilities.Tables.Count > 0 && marketFacilities.Tables[0].Rows != null && marketFacilities.Tables[0].Rows.Count > 0)
                        {
                            DataSet dsMarketSite;
                            SiteInfo marketSiteInfo;
                            foreach (DataRow marketFacility in marketFacilities.Tables[0].Rows)
                            {
                                dsMarketSite = new DataSet();
                                marketSiteInfo = new SiteInfo();
                                //string marketFacilityLocationCode = marketFacilities.Tables[0].Rows[0]["SLLocCode"].ToString();
                                string marketSiteNumber = marketFacility["StoreNo"].ToString();

                                if (marketSiteNumber != siteNumber)
                                {
                                    dsMarketSite = dh.GetRDFacilityDataByStoreNumber(marketSiteNumber);

                                    if (dsMarketSite.Tables.Count > 0 && dsMarketSite.Tables[0].Rows.Count > 0)
                                    {
                                        marketSiteInfo.SiteId = Convert.ToInt32(dsMarketSite.Tables[0].Rows[0]["SLSiteId"]);
                                        marketSiteInfo.LocationCode = dsMarketSite.Tables[0].Rows[0]["SLLocCode"].ToString();
                                        marketSiteInfo.GlobalSiteNum = dsMarketSite.Tables[0].Rows[0]["StoreNo"].ToString();
                                        //siteInfo.ContactName = dsSite.Tables[0].Rows[0]["sContactName"].ToString();
                                        marketSiteInfo.SiteAddress.AddressLine1 = dsMarketSite.Tables[0].Rows[0]["Street1"].ToString();
                                        marketSiteInfo.SiteAddress.AddressLine2 = dsMarketSite.Tables[0].Rows[0]["Street2"].ToString();
                                        marketSiteInfo.SiteAddress.City = dsMarketSite.Tables[0].Rows[0]["City"].ToString();
                                        marketSiteInfo.SiteAddress.State = dsMarketSite.Tables[0].Rows[0]["State"].ToString();
                                        marketSiteInfo.SiteAddress.Zip = dsMarketSite.Tables[0].Rows[0]["ZipCode"].ToString();
                                        marketSiteInfo.SiteAddress.Latitude = dsMarketSite.Tables[0].Rows[0]["Latitude"].ToString();
                                        marketSiteInfo.SiteAddress.Longitude = dsMarketSite.Tables[0].Rows[0]["Longitude"].ToString();
                                        marketSiteInfo.SiteAddress.AddressType = PREnums.AddressType.Warehouse;
                                        marketSiteInfo.LegalName = dsMarketSite.Tables[0].Rows[0]["CompDBAName"].ToString();
                                        marketSiteInfo.EMail = dsMarketSite.Tables[0].Rows[0]["Email"].ToString();
                                        marketSiteInfo.MarketId = dsMarketSite.Tables[0].Rows[0]["FK_MarketId"] != DBNull.Value ? Convert.ToInt32(dsSite.Tables[0].Rows[0]["FK_MarketId"]) : (int?)null;

                                        siteInfo.MarketFacilities.Add(marketSiteInfo);
                                    }
                                }
                            }
                        }
                    }
                    //siteInfo.FacCode = dsSite.Tables[0].Rows[0]["FacCode"].ToString();
                }
            }
            return siteInfo;
        }

        public List<SiteInfo> GetSiteInformationWithMarkets()
        {
            LocalDataHandler dh = new LocalDataHandler();
            DataSet dsSite = new DataSet();
            List<SiteInfo> lstSiteInfo = new List<SiteInfo>();
            //dsSite = dh.GetRDFacilityData();
            dsSite = dh.GetActivePackRatFacilitiesInfo();

            if (dsSite != null && dsSite.Tables.Count > 0 && dsSite.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow datarow in dsSite.Tables[0].Rows)
                {
                    if (datarow["StoreNo"] != DBNull.Value)
                    {
                        string GlobalSiteNum = Convert.ToString(datarow["StoreNo"]);
                        if (!string.IsNullOrEmpty(GlobalSiteNum))
                            lstSiteInfo.Add(GetSiteInformationWithMarkets(GlobalSiteNum));
                    }
                }
            }

            return lstSiteInfo;
        }

        public List<SiteInfo> GetSiteInformation()
        {
            LocalDataHandler dh = new LocalDataHandler();
            DataSet dsSite = new DataSet();
            List<SiteInfo> lstSiteInfo = new List<SiteInfo>();
            dsSite = dh.GetRDFacilityData();

            if (dsSite != null && dsSite.Tables.Count > 0 && dsSite.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow datarow in dsSite.Tables[0].Rows)
                {
                    if (datarow["SLSiteId"] != DBNull.Value)
                    {
                        SiteInfo siteInfo = new SiteInfo();
                        siteInfo.SiteId = Convert.ToInt32(datarow["SLSiteId"]);
                        siteInfo.LocationCode = datarow["SLLocCode"].ToString();
                        siteInfo.GlobalSiteNum = datarow["StoreNo"].ToString(); 
                        siteInfo.SiteAddress.AddressLine1 = datarow["Street1"].ToString();
                        siteInfo.SiteAddress.AddressLine2 = datarow["Street2"].ToString();
                        siteInfo.SiteAddress.City = datarow["City"].ToString();
                        siteInfo.SiteAddress.State = datarow["State"].ToString();
                        siteInfo.SiteAddress.Zip = datarow["ZipCode"].ToString();
                        siteInfo.SiteAddress.Latitude = datarow["Latitude"].ToString();
                        siteInfo.SiteAddress.Longitude = datarow["Longitude"].ToString();
                        siteInfo.LegalName = datarow["CompDBAName"].ToString();
                        siteInfo.EMail = datarow["Email"].ToString();

                        lstSiteInfo.Add(siteInfo);
                    }
                }
            }
         
            return lstSiteInfo;
        }

       
        public int GetUnitIdByUnitName(string locationCode, string unitName)
        {
            LocalDataHandler dh = new LocalDataHandler();
            DataSet dsUnit = new DataSet();
            int unitId = -1;
            dsUnit = dh.GetUnitByUnitName(locationCode, unitName);
            if (dsUnit.Tables.Count > 0)
            {
                if (dsUnit.Tables[0].Rows.Count > 0)
                {
                    unitId = Convert.ToInt32(dsUnit.Tables[0].Rows[0]["UnitId"]);
                }
            }
            return unitId;
        }

        

        public DataSet GetAllFacilities()
        {
            LocalDataHandler dh = new LocalDataHandler();

            return dh.GetAllPackratFacilities();
        }

        
        public UserInfo LogIn(string firstName, string lastName, string password)
        {
            UserInfo userInfo = new UserInfo();
            LocalDataHandler dh = new LocalDataHandler();
            DataSet dsLogin = dh.GetLoginInfo(firstName, lastName, password);
            if (dsLogin.Tables.Count > 0)
            {
                if (dsLogin.Tables[0].Rows.Count > 0)
                {
                    userInfo.UserID = dsLogin.Tables[0].Rows[0]["UserID"].ToString();
                    userInfo.FirstName = dsLogin.Tables[0].Rows[0]["FirstName"].ToString();
                    userInfo.LastName = dsLogin.Tables[0].Rows[0]["LastName"].ToString();
                    userInfo.Role = dsLogin.Tables[0].Rows[0]["SecLevel"].ToString();
                    userInfo.EMail = dsLogin.Tables[0].Rows[0]["EMail"].ToString();
                }
            }
            return userInfo;
        }

        

        public void InsertActivityLog(string userId, DateTime dtm, int siteId, int qorid, int tenantId, string activity)
        {
            LocalDataHandler dh = new LocalDataHandler();
            dh.InsertActivityLog(userId, dtm, siteId, qorid, tenantId, activity);
        }

        public int InsertActivityLog(int qorid, string activityType, string activityText, string activityBy)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.InsertActivityLog(qorid, activityType, activityText, activityBy);
        }        

        public void InsertPRErrorLog(DateTime errorDateTime, string userId, string exceptionType, string exceptionMsg, string stackTrace, int qrid)
        {
            LocalDataHandler dh = new LocalDataHandler();
            dh.InsertPRErrorLog(errorDateTime, userId, exceptionType, exceptionMsg, stackTrace, qrid);
        }

        public void InsertPRErrorLog(DateTime errorDateTime, string userId, string exceptionType, string exceptionMsg, string stackTrace, int qrid, string AppURL, string DataDump)
        {
            LocalDataHandler dh = new LocalDataHandler();
            dh.InsertPRErrorLog(errorDateTime, userId, exceptionType, exceptionMsg, stackTrace, qrid, AppURL, DataDump);
        }        

        public int InsertActivityLog(int quoteId, int qorId, string activityTypeName, string activityText, string activityBy, string tODRef, int USSCustId)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.InsertActivityLog(quoteId, qorId, activityTypeName, activityText, activityBy, tODRef, USSCustId);
        }        
        
        public DataSet GetRDFacilitiesDataByMarket(int storeNo)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetRDFacilitiesDataByMarket(storeNo);
        }
        

        public DataSet GetPRGQuoteOrderLogData(int qorid)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetPRGOrderContractLogData(qorid);
        }

        //SPERP-TODO-CTRMV , Remove after done with ESB
        //public DataSet GetTripDetails(int T26ContainerId)
        //{
        //    LocalDataHandler dh = new LocalDataHandler();
        //    return dh.GetTripDetails(T26ContainerId);
        //}

        public DataSet GetLoadDriverDetails(int QorId, string TouchType, int SLSeqNumber)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetLoadDriverDetails(QorId, TouchType, SLSeqNumber);
        }

        

        public DataSet GetCallBlastData(string locationCode)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetCallBlastData(locationCode);
        }

        

        public DataSet GetCapacity(DateTime startDate, DateTime endDate, string locationCode)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetCapacity(startDate, endDate, locationCode);
        }

        
        public DataSet GetCapCategoryTouchTypes()
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetCapCategoryTouchTypes();
        }
        public DataSet GetTouchMilesByTypeByFacilityByDateRange(int touchTypeId, string locationCode, DateTime startDate, DateTime endDate)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetTouchMilesByTypeByFacilityByDateRange(touchTypeId, locationCode, startDate, endDate);
        }

        public DataSet GetAllCapTouchTypes()
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetAllCapTouchTypes();
        }

        public DataSet GetCapacityColorActionsByRole(int? roleLevelId)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetCapacityColorActionsByRole(roleLevelId);
        }
        public string GetCapCategoryByCategoryId(int categoryId)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetCapCategoryByCategoryId(categoryId);
        }
        public DataSet GetFacilitiesWithLimitedCapacities(string locationCode)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetFacilitiesWithLimitedCapacities(locationCode);
        }
         
        public ETASettings GetETASetting(int QorID, string TouchType, int SLSeqNo)
        {
            LocalDataHandler dh = new LocalDataHandler();
            ETASettings es = new ETASettings();
            DataSet ds = dh.GetETASetting(QorID, TouchType, SLSeqNo);

            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dt = TransposeDataTable.Pivot(ds.Tables[0]);

                if (dt != null && dt.Rows.Count > 0)
                {
                    es.CanDisplayETAStatus = Convert.ToBoolean(dt.Rows[0]["CanETAStatusDisplay"]);
                    es.Status = dt.Rows[0]["ETAStatus"].ToString();
                    es.BackGroundColor = dt.Rows[0][es.Status + "_BackGroundColor"].ToString();
                    es.TextColor = dt.Rows[0][es.Status + "_TextColor"].ToString();
                    es.ProvidedETARange = dt.Rows[0]["ProvidedETARange"].ToString();
                }
            }

            return es;
        }

        

        public List<ETASettings> GetETASettingList(string uniqueIdsToGetETASettings)
        {
            LocalDataHandler dh = new LocalDataHandler();
            List<ETASettings> esList = new List<ETASettings>();
            DataSet ds = dh.GetETASettingList(uniqueIdsToGetETASettings);

            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    ETASettings es;
                    foreach (DataRow row in dt.Rows)
                    {
                        es = new ETASettings();
                        es.CanDisplayETAStatus = Convert.ToBoolean(row["CanETAStatusDisplay"]);
                        es.Status = row["ETAStatus"].ToString();
                        es.BackGroundColorKey = Convert.ToString(row["BackGroundColorKey"]);
                        es.BackGroundColor = Convert.ToString(row["BackGroundColor"]);
                        es.TextColorKey = Convert.ToString(row["TextColorKey"]);
                        es.TextColor = Convert.ToString(row["TextColor"]);
                        es.ProvidedETARange = Convert.ToString(row["ProvidedETARange"]);
                        es.TouchKey = row["TouchKey"].ToString();

                        esList.Add(es);
                    }
                }
            }

            return esList;
        }

        #region Methods to get Capacity and save data DB for TG-450

        public int Add_TG450_CalendarData(DataTable cAPTouchLogDetailDataTable, string locationCode)
        {
            try
            {
                LocalDataHandler dh = new LocalDataHandler();
                dh.TG450_LogCapacityCalendarTouchData(cAPTouchLogDetailDataTable, locationCode);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        } 

        public ScheduleCalendar TG450_GetScheduleCalendarData(string locationCode, DateTime startDate, int numberOfDays)
        {
            try
            {
                LocalDataHandler dh = new LocalDataHandler();
                DataSet dsCapacity = dh.TG450_GetTouchScheduleAvailableData(locationCode, startDate, numberOfDays);
                return TG450_GetScheduleCalendarData(locationCode, startDate, numberOfDays, dsCapacity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ScheduleCalendar TG450_GetScheduleCalendarData(string locationCode, DateTime startDate, int numberOfDays, DataSet dsCapacity)
        {
            LocalDataHandler dh = new LocalDataHandler();
            ScheduleCalendar scheduleCalendar = new ScheduleCalendar();

            if (dsCapacity != null && dsCapacity.Tables.Count > 0 && dsCapacity.Tables[0] != null && dsCapacity.Tables[0].Rows.Count > 0)
            {
                for (DateTime dtSchedule = startDate; dtSchedule < startDate.AddDays(numberOfDays); dtSchedule = dtSchedule.AddDays(1))
                {
                    int idx = 0;
                    DataRow[] drTouches = new DataRow[3];

                    #region Initialize Schedule Calendar tables for Scheduled day
                    ScheduleCalendar.AMDataTable amCalendar = scheduleCalendar.AM;
                    ScheduleCalendar.PMDataTable pmCalendar = scheduleCalendar.PM;
                    ScheduleCalendar.AnyTimeDataTable anytimeCalendar = scheduleCalendar.AnyTime;

                    ScheduleCalendar.AMRow amRow = amCalendar.NewAMRow();
                    ScheduleCalendar.PMRow pmRow = pmCalendar.NewPMRow();
                    ScheduleCalendar.AnyTimeRow anyTimeRow = anytimeCalendar.NewAnyTimeRow();
                    #endregion

                    amRow.StoreStatus = "";
                    pmRow.StoreStatus = "";
                    anyTimeRow.StoreStatus = "";

                    amRow.TripMiles = 0;
                    pmRow.TripMiles = 0;
                    anyTimeRow.TripMiles = 0;

                    amRow.Date = dtSchedule;
                    pmRow.Date = dtSchedule;
                    anyTimeRow.Date = dtSchedule;


                    foreach (DataRow drTemp in dsCapacity.Tables[0].Rows)
                    {
                        if (idx <= 3 && drTemp["CAPDate"] != null && Convert.ToDateTime(drTemp["CAPDate"]).ToShortDateString() == dtSchedule.ToShortDateString())
                        {
                            drTouches[idx] = drTemp;
                            idx++;
                        }
                    }

                    foreach (DataRow drTouch in drTouches)
                    {
                        switch (drTouch["TouchTime"].ToString().ToUpper())
                        {
                            case "AM":
                                GetRowAM(drTouch, amRow);
                                break;
                            case "PM":
                                GetRowPM(drTouch, pmRow);
                                break;
                            case "ANYTIME":
                                GetRowAnytime(drTouch, anyTimeRow);
                                break;
                        }
                    }

                    amCalendar.AddAMRow(amRow);
                    pmCalendar.AddPMRow(pmRow);
                    anytimeCalendar.AddAnyTimeRow(anyTimeRow);
                }

            }

            return scheduleCalendar;
        }

        private static void GetRowAM(DataRow drTouch, ScheduleCalendar.AMRow amRow)
        {
            amRow.StoreStatus = drTouch["StoreStatus"] == DBNull.Value ? "" : drTouch["StoreStatus"].ToString();
            amRow.FacilityMiles = drTouch["FacilityMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["FacilityMiles"]);
            amRow.BookedMiles = drTouch["BookedMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["BookedMiles"]);
            amRow.RegularMiles = drTouch["RegularMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["RegularMiles"]);

            amRow.SalesRegularMiles = drTouch["SalesRegularMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["SalesRegularMiles"]);
            amRow.ServiceRegularMiles = drTouch["ServiceRegularMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["ServiceRegularMiles"]);
            amRow.ReservedMiles = drTouch["ReservedMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["ReservedMiles"]);

            amRow.ColorCode = Convert.ToInt16(drTouch["ColorCode"]);
        }

        private static void GetRowPM(DataRow drTouch, ScheduleCalendar.PMRow pmRow)
        {
            pmRow.StoreStatus = drTouch["StoreStatus"] == DBNull.Value ? "" : drTouch["StoreStatus"].ToString();
            pmRow.FacilityMiles = drTouch["FacilityMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["FacilityMiles"]);
            pmRow.BookedMiles = drTouch["BookedMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["BookedMiles"]);
            pmRow.RegularMiles = drTouch["RegularMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["RegularMiles"]);

            pmRow.SalesRegularMiles = drTouch["SalesRegularMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["SalesRegularMiles"]);
            pmRow.ServiceRegularMiles = drTouch["ServiceRegularMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["ServiceRegularMiles"]);
            pmRow.ReservedMiles = drTouch["ReservedMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["ReservedMiles"]);

            pmRow.ColorCode = Convert.ToInt16(drTouch["ColorCode"]);
        }

        private static void GetRowAnytime(DataRow drTouch, ScheduleCalendar.AnyTimeRow anytimeRow)
        {
            anytimeRow.StoreStatus = drTouch["StoreStatus"] == DBNull.Value ? "" : drTouch["StoreStatus"].ToString();
            anytimeRow.FacilityMiles = drTouch["FacilityMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["FacilityMiles"]);
            anytimeRow.BookedMiles = drTouch["BookedMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["BookedMiles"]);
            anytimeRow.RegularMiles = drTouch["RegularMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["RegularMiles"]);

            anytimeRow.SalesRegularMiles = drTouch["SalesRegularMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["SalesRegularMiles"]);
            anytimeRow.ServiceRegularMiles = drTouch["ServiceRegularMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["ServiceRegularMiles"]);
            anytimeRow.ReservedMiles = drTouch["ReservedMiles"] == DBNull.Value ? 0M : Convert.ToDecimal(drTouch["ReservedMiles"]);

            anytimeRow.ColorCode = Convert.ToInt16(drTouch["ColorCode"]);
        }


        #endregion

    }
}
