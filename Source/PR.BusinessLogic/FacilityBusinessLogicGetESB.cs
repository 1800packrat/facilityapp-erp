﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using PR.DataHandler;
using PR.Entities;
using PR.UtilityLibrary;
using Newtonsoft.Json;
using PR.Entities.EsbEntities;
using System.IO;
using System.Configuration;

namespace PR.BusinessLogic
{
    public partial class SMDBusinessLogic : BaseBusinessLogicClass
    {
        public DataSet GetUnitInfo()
        {
            // return new LocalDataHandler().GetUnitInfoForStubMethod();

            return GetUnitInfo("L157", "");
        }

        /// <summary>
        /// DONE
        /// Method to validate if UnitName exists for LocationCode from Salesforce database using ESB endpoint - TG-692
        /// </summary>
        /// <param name="locationCode"></param>
        /// <param name="unitName"></param>
        /// <returns></returns>
        public int ValidateUnitByUnitName(string locationCode, string unitName)
        {
            int unitCount = -1;

            try
            {
                DataSet dsUnitInfo = GetUnitInfo(locationCode, unitName);

                if (dsUnitInfo != null && dsUnitInfo.Tables.Count > 0 && dsUnitInfo.Tables[0] != null && dsUnitInfo.Tables[0].Rows.Count > 0)
                {
                    DataRow[] drUnits = dsUnitInfo.Tables[0].Select("LocationCode='" + locationCode + "' and UnitName='" + unitName + "'");

                    unitCount = drUnits.Count(); // Convert.ToInt32(drUnits[0]["UnitId"]);
                }
                else
                {
                    throw new Exception("EsbMethod.ValidateUnitByUnitName - No unit info available.");
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            return unitCount;

        }

        /// <summary>
        /// DONE
        /// Method to get Unit Details based on UnitName and LocationCode from Salesforce database using ESB endpoint - TG-692
        /// </summary>
        /// <param name="locationCode"></param>
        /// <param name="unitName"></param>
        /// <returns></returns>
        public DataSet GetUnitInformationByUnitName(string locationCode, string unitName)
        {
            DataSet filteredUnits = null;
            try
            {
                filteredUnits = new DataSet();
                DataSet dsUnitInfo = GetUnitInfo(locationCode, unitName);

                if (dsUnitInfo != null && dsUnitInfo.Tables.Count > 0 && dsUnitInfo.Tables[0] != null && dsUnitInfo.Tables[0].Rows.Count > 0)
                {
                    var dataView = dsUnitInfo.Tables[0].DefaultView;
                    dataView.RowFilter = "LocationCode='" + locationCode + "' and UnitName='" + unitName + "'";

                    filteredUnits.Tables.Add(dataView.ToTable());
                }
                else
                {
                    throw new Exception("EsbMethod.GetUnitInformationByUnitName - No unit info available.");
                }

            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            return filteredUnits;
        }

        /// <summary>
        /// DONE
        /// Method to get Unit Type based on UnitName and LocationCode from Salesforce database using ESB endpoint - TG-692
        /// </summary>
        /// <param name="locationCode"></param>
        /// <param name="unitName"></param>
        /// <returns></returns>
        public string GetUnitTypeNameByUnitName(string locationCode, string unitName)
        {
            string unitTypeName = string.Empty;

            try
            {
                DataSet dsUnits = GetUnitInfo(locationCode, unitName);

                if (dsUnits != null && dsUnits.Tables.Count > 0 && dsUnits.Tables[0] != null && dsUnits.Tables[0].Rows.Count > 0)
                {
                    DataRow[] drUnits = dsUnits.Tables[0].Select("LocationCode='" + locationCode + "' and UnitName='" + unitName + "'");

                    unitTypeName = drUnits.Count() > 0 ? drUnits[0]["UnitTypeName"].ToString() : "";
                }
                else
                {
                    throw new Exception("EsbMethod.GetUnitTypeNameByUnitName - No unit info available.");
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //LocalDataHandler handler = new LocalDataHandler();

            //try
            //{
            //    DataSet dsUnit = GetUnitInfoByUnitNameFromSL(corpCode, locationCode, UserName, password, unitName);

            //    if (dsUnit.Tables != null && dsUnit.Tables["Table"] != null && dsUnit.Tables["Table"].Rows != null && dsUnit.Tables["Table"].Rows.Count > 0)
            //    {
            //        unitTypeName = Convert.ToString(dsUnit.Tables["Table"].Rows[0]["sTypeName"]);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    handler.InsertPRErrorLog(DateTime.Now, _userId, string.Empty, ex.Message, ex.StackTrace.ToString(), -1);
            //}

            #endregion

            return unitTypeName;
        }

        /// <summary>
        ///  Method to get Unit Details based on qorId and sessionId from Salesforce database using ESB endpoint - TG-692
        /// </summary>
        /// <param name="qorId"></param> 
        /// <returns></returns>
        public UnitInfo GetUnitInfoByQorId(int qorId)
        {
            UnitInfo unitInfo = new UnitInfo();
            UnitInfo.UnitsDataTable dtUnits = unitInfo.Units;

            try
            {
                DataSet dsUnits = GetUnitInfo("", "", qorId.ToString());

                if (dsUnits != null && dsUnits.Tables.Count > 0 && dsUnits.Tables[0] != null && dsUnits.Tables[0].Rows.Count > 0)
                {
                    DataRow[] drUnits = dsUnits.Tables[0].Select("Origin_QORID = '" + qorId + "' OR Destination_QORID='" + qorId + "'");

                    if (drUnits.Count() > 0)
                    {
                        UnitInfo.UnitsRow row = dtUnits.NewUnitsRow();
                        row.UnitID = Convert.ToInt32(drUnits[0]["UnitId"]);
                        row.UnitName = drUnits[0]["UnitName"].ToString();
                        row.UnitTypeID = Convert.ToInt32(drUnits[0]["UnitTypeId"]);
                        row.UnitLength = Convert.ToDecimal(drUnits[0]["UnitSize"]);
                        //row.UnitWidth = Convert.ToDecimal(drUnits[0]["UnitWidth"]); // Not present in SF
                        row.UnitDescription = drUnits[0]["UnitTypeName"].ToString();
                        dtUnits.AddUnitsRow(row);
                    }
                    else
                    {
                        throw new Exception("Invalid QORId.");
                    }
                }
                else
                {
                    throw new Exception("EsbMethod.GetUnitInfoByQorId - No unit info available.");
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            return unitInfo;
        }


        /// <summary>
        ///  Method to get Unit Details based on qorId and sessionId from Salesforce database using ESB endpoint - TG-692
        /// </summary>
        /// <param name="qorId"></param> 
        /// <returns></returns>
        public string GetAddedUnit(int qorId)
        {
            string unitName = string.Empty;

            try
            {
                DataSet dsUnits = GetUnitInfo("", "", qorId.ToString());

                if (dsUnits != null && dsUnits.Tables.Count > 0 && dsUnits.Tables[0] != null && dsUnits.Tables[0].Rows.Count > 0)
                {
                    DataRow[] drUnits = dsUnits.Tables[0].Select("Origin_QORID ='" + qorId + "'");

                    if (drUnits.Count() > 0)
                        unitName = drUnits[0]["UnitName"].ToString();
                    else
                        throw new Exception("Invalid QORId.");
                }
                else
                {
                    throw new Exception("EsbMethod.GetAddedUnit - No unit info available.");
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //SLAPIMobileTemp slapi = new SLAPIMobileTemp();
            //string unitName = string.Empty;

            //var dsQORList = GetQORList(CorpCode, String.Empty, UserName, Password, 0, String.Empty, qorId, String.Empty);
            //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //{
            //    unitName = Convert.ToString(dsQORList.Tables[0].Rows[0]["sUnitName"]);
            //}
            //else
            //    throw new Exception("Invalid QORId.");

            #endregion

            return unitName;
        }


        /// <summary>
        /// Method to get Warehouse + Transportation touches based on date and locaion from Salesforce database using ESB endpoint - TG-713
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        public PR.Entities.EsbEntities.TouchDashboard.RootObject GetWareHouseTouches(WarehouseTouchRequest requestObject)
        {
            PR.Entities.EsbEntities.TouchDashboard.RootObject touchListObj = null;
            try
            {
                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.GetWareHouseTouches, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    touchListObj = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.TouchDashboard.RootObject>(response);
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            return touchListObj;
        }

        /// <summary>
        /// Method to get Transporation touches based on date and locaion from Salesforce database using ESB endpoint - TG-714
        /// </summary>
        /// <param name="criteriaSitelinkTouches"></param>
        /// <returns></returns>
        public List<SLTouch> GetTransportationTouches(CriteriaSitelinkTouches criteriaSitelinkTouches)
        {
            #region SFERP-TODO-ESBMTD

            object requestObject = new
            {
                locationCodes = criteriaSitelinkTouches.FLocationCodes,
                touchDate = criteriaSitelinkTouches.ScheduleFromDate.Value.ToString("MM/dd/yyyy"),
                TouchTypeIDs = criteriaSitelinkTouches.QTTypeIDs,
                TouchStatusIDs = criteriaSitelinkTouches.QTStatusIDs,
                TouchRentalTypeIDs = criteriaSitelinkTouches.QTRentalTypeIDs,
                TouchRentalstatusIDs = criteriaSitelinkTouches.QTRentalstatusIDs,
            };

            PR.Entities.EsbEntities.LocalDispatch.RootObject touchList = null;
            string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

            var response = ExecuteMethodESB(EsbMethod.GetTransportationTouches, requestJson);

            if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                touchList = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.LocalDispatch.RootObject>(response);

            List<SLTouch> slTouches = GetTransportationTouches(touchList);

            return GetProvidedETASettings(slTouches);

            #endregion

            ////Comment this section to get data from stub/ dummy json files. 
            //var jsonData = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetSLTouchesBySP-{criteriaSitelinkTouches.FLocationCodes}.txt"));
            //var touchList = (List<SLTouch>)JsonConvert.DeserializeObject(jsonData, (typeof(List<SLTouch>)));
        }

        private List<SLTouch> GetProvidedETASettings(List<SLTouch> slTouches)
        {
            string uniqueTouchIds = string.Join(",", slTouches.Select(t => t.TouchKey).ToList());

            LocalComponent local = new LocalComponent();
            List<ETASettings> lstETASettings = new List<ETASettings>();

            if (!string.IsNullOrEmpty(uniqueTouchIds))
            {
                var etaSettings = local.GetETASettingList(uniqueTouchIds);

                etaSettings.ForEach(e =>
                {
                    slTouches.Where(t => $"{t.Qorid}_{t.TouchType}_{t.SequenceNO}" == e.TouchKey).FirstOrDefault().ProvidedETASettings = e;
                });
            }

            return slTouches;
        }

        private List<SLTouch> GetTransportationTouches(PR.Entities.EsbEntities.LocalDispatch.RootObject touchList)
        {
            List<SLTouch> finalTouchList = new List<SLTouch>();

            if (touchList != null && touchList.SF_TransportationTouch != null)
            {
                foreach (var sfTouch in touchList.SF_TransportationTouch)
                {
                    string touchUnitSize = CommonUtility.GetUnitSize(sfTouch.UnitSize.ToLower());

                    var objTouch = new SLTouch()
                    {
                        Unitsize = touchUnitSize, // Get Default if Empty
                        Qorid = string.IsNullOrWhiteSpace(sfTouch.QORID) ? 0 : Convert.ToInt32(sfTouch.QORID),
                        CustomerName = string.IsNullOrWhiteSpace(sfTouch.Customer_Name) ? "" : sfTouch.Customer_Name,
                        PhoneNumber = string.IsNullOrWhiteSpace(sfTouch.PhoneNo) ? "" : sfTouch.PhoneNo,
                        TouchType = string.IsNullOrWhiteSpace(sfTouch.TouchTypeShort) ? "" : sfTouch.TouchTypeShort,
                        SequenceNO = string.IsNullOrWhiteSpace(sfTouch.SequenceNo) ? 0 : Convert.ToInt32(sfTouch.SequenceNo),

                        UnitNumber = CommonUtility.GetUnitNameOrNumber(sfTouch.UnitName, touchUnitSize),  // Get Default if Empty
                        Status = string.IsNullOrWhiteSpace(sfTouch.TouchStatus) ? "" : sfTouch.TouchStatus,
                        IsLDMOrderTouch = string.IsNullOrWhiteSpace(sfTouch.IsLDMOrderTouch) ? false : Convert.ToBoolean(sfTouch.IsLDMOrderTouch),

                        RentalStatus = string.IsNullOrWhiteSpace(sfTouch.RentalStatus) ? "" : sfTouch.RentalStatus,
                        RentalType = string.IsNullOrWhiteSpace(sfTouch.RentalType) ? "" : sfTouch.RentalType,
                        Directions = string.IsNullOrWhiteSpace(sfTouch.Directions) ? "" : sfTouch.Directions,

                        SiteLinkMileage = string.IsNullOrWhiteSpace(sfTouch.TouchMiles) ? Convert.ToDecimal(0) : Convert.ToDecimal(sfTouch.TouchMiles),
                        StarsUnitId = string.IsNullOrWhiteSpace(sfTouch.UnitId) ? 0 : Convert.ToInt32(sfTouch.UnitId),
                        FacCode = string.IsNullOrWhiteSpace(sfTouch.LocationCode) ? "" : sfTouch.LocationCode,

                        Instructions = string.IsNullOrWhiteSpace(sfTouch.Instructions) ? "" : sfTouch.Instructions,
                        StartTime = string.IsNullOrWhiteSpace(sfTouch.TouchTime) ? "Anytime" : sfTouch.TouchTime,
                        //EndTime = string.IsNullOrWhiteSpace(sfTouch.EndTime) ? "Anytime" : sfTouch.EndTime,
                        StarsID = string.IsNullOrWhiteSpace(sfTouch.OrderNo) ? "" : sfTouch.OrderNo,

                        Email = string.IsNullOrWhiteSpace(sfTouch.Customer_Email) ? "" : sfTouch.Customer_Email,
                        //EmailAlt = string.IsNullOrWhiteSpace(sfTouch.EmailAlt) ? "" : sfTouch.EmailAlt,
                        DoorToFront = string.IsNullOrWhiteSpace(sfTouch.DoorToFront) ? false : Convert.ToBoolean(sfTouch.DoorToFront),
                        DoorToRear = string.IsNullOrWhiteSpace(sfTouch.DoorToRear) ? false : Convert.ToBoolean(sfTouch.DoorToRear),
                        ScheduledDate = Convert.ToDateTime(sfTouch.ScheduledDate),

                        OriginAddress = new Entities.Address
                        {
                            AddressLine1 = string.IsNullOrWhiteSpace(sfTouch.Origin_Address1) ? "" : sfTouch.Origin_Address1,
                            //AddressLine2 = string.IsNullOrWhiteSpace(sfTouch.Origin_Address2) ? "" : sfTouch.Origin_Address2, // Not present in SF
                            State = string.IsNullOrWhiteSpace(sfTouch.Origin_State) ? "" : sfTouch.Origin_State,
                            Country = string.IsNullOrWhiteSpace(sfTouch.Origin_Country) ? "" : sfTouch.Origin_Country,
                            City = string.IsNullOrWhiteSpace(sfTouch.Origin_City) ? "" : sfTouch.Origin_City,
                            Zip = string.IsNullOrWhiteSpace(sfTouch.Origin_Zip) ? "" : sfTouch.Origin_Zip,
                            //Latitude = string.IsNullOrWhiteSpace(sfTouch.Origin_Latitude) ? "" : sfTouch.Origin_Latitude, // Not present in SF
                            //Longitude = string.IsNullOrWhiteSpace(sfTouch.Origin_Longitude) ? "" : sfTouch.Origin_Longitude, // Not present in SF
                            Company = string.IsNullOrWhiteSpace(sfTouch.Origin_Company) ? "" : sfTouch.Origin_Company,
                            
                            
                        },
                        DestAddress = new Entities.Address
                        {
                            AddressLine1 = string.IsNullOrWhiteSpace(sfTouch.Destination_Address1) ? "" : sfTouch.Destination_Address1,
                            //AddressLine2 = string.IsNullOrWhiteSpace(sfTouch.Destination_Address2) ? "" : sfTouch.Destination_Address2, // Not present in SF
                            State = string.IsNullOrWhiteSpace(sfTouch.Destination_State) ? "" : sfTouch.Destination_State,
                            Country = string.IsNullOrWhiteSpace(sfTouch.Destination_Country) ? "" : sfTouch.Destination_Country,
                            City = string.IsNullOrWhiteSpace(sfTouch.Destination_City) ? "" : sfTouch.Destination_City,
                            Zip = string.IsNullOrWhiteSpace(sfTouch.Destination_Zip) ? "" : sfTouch.Destination_Zip,
                            //Latitude = string.IsNullOrWhiteSpace(sfTouch.Destination_Latitude) ? "" : sfTouch.Destination_Latitude, // Not present in SF
                            //Longitude = string.IsNullOrWhiteSpace(sfTouch.Destination_Longitude) ? "" : sfTouch.Destination_Longitude, // Not present in SF
                            Company = string.IsNullOrWhiteSpace(sfTouch.Destination_Company) ? "" : sfTouch.Destination_Company
                        },

                        IsWeightTicket = string.IsNullOrWhiteSpace(sfTouch.IsWeightTicket) ? false : Convert.ToBoolean(sfTouch.IsWeightTicket),
                        IsZippyShellQuote = string.IsNullOrWhiteSpace(sfTouch.IsZippyShellQuote) ? false : Convert.ToBoolean(sfTouch.IsZippyShellQuote),

                    };

                    finalTouchList.Add(objTouch);
                }
            }
            //else
            //{
            //    throw new Exception("EsbMethod.GetTransportationTouches - No touch available."); 
            //}

            return finalTouchList;
        }


        /// <summary>
        /// PENDING
        /// Get Order settlement state with billing QORID
        /// </summary>
        /// <param name="dummyBillingQorid"></param>
        /// <returns></returns>
        public bool IsLDMOrderSettled(int qorId, int orderNo = 0)
        {
            #region SFERP-TODO-ESBMTD
            PR.Entities.EsbEntities.OrderBase.RootObject orderInfo = null;

            try
            {
                var requestObject = new
                {
                    orderId = orderNo > 0 ? orderNo.ToString() : "",
                    qorid = qorId > 0 ? qorId.ToString() : ""  //dummyBillingQorId
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.GetOrderInfo, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    orderInfo = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.OrderBase.RootObject>(response);

                return orderInfo != null ? orderInfo.SF_OrderInfo.IsSettled : false;

            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }
            return false;

            #endregion

        }

        /// <summary>
        /// PENDING
        /// Get Weight ticket status for particualr order from Salesforce database using ESB endpoint
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public bool GetWeigthTicket(int orderNo, int qorId = 0)
        {
            PR.Entities.EsbEntities.OrderBase.RootObject orderInfo = null;

            try
            {
                var requestObject = new
                {
                    orderId = orderNo > 0 ? orderNo.ToString() : "",
                    qorId = qorId > 0 ? qorId.ToString() : "",
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.GetOrderInfo, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    orderInfo = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.OrderBase.RootObject>(response);

                return orderInfo != null ? orderInfo.SF_OrderInfo.WeightTicket : false;

            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }
            return false;
        }

        /// <summary>
        /// DONE
        /// Get Brand Details for particualr Unit based on QORID from Salesforce database using ESB endpoint - TG-721
        /// </summary>
        /// <param name="qorid"></param>
        /// <returns></returns>
        public Brand GetBrandInfo(int qorid)
        {
            Brand brandInfo = null;
            try
            {
                brandInfo = new Brand();
                PR.Entities.EsbEntities.Brand.RootObject brandData = GetBrandInfo(qorid.ToString());

                if (brandData != null)
                {
                    brandInfo.BrandID = Convert.ToInt32(brandData.SF_BrandInfo.BrandId);
                    brandInfo.BrandName = brandData.SF_BrandInfo.BrandName;
                }
                else
                {
                    throw new Exception("EsbMethod.GetBrandInfo - No brand info available.");
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            return brandInfo;
        }

        /// <summary>
        /// DONE
        /// Get UnAssigned touches based on request object from Salesforce database using ESB endpoint - TG-717
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        public PR.Entities.EsbEntities.TouchUnAssign.RootObject GetUnassignedTouches(StagingUnassignTouchRequest requestObject)
        {
            PR.Entities.EsbEntities.TouchUnAssign.RootObject touchList = null;
            try
            {
                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.GetUnassignedTouches, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    touchList = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.TouchUnAssign.RootObject>(response);

            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }
            return touchList;

        }

        /// <summary>
        /// DONE
        /// Get Staging touches based on request object from Salesforce database using ESB endpoint - TG-716
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        public PR.Entities.EsbEntities.TouchStage.RootObject GetStagingTouches(StagingUnassignTouchRequest requestObject)
        {
            PR.Entities.EsbEntities.TouchStage.RootObject touchList = null;
            try
            {
                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.GetStagingTouches, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    touchList = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.TouchStage.RootObject>(response);
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }
            return touchList;
        }


        /// <summary>
        /// TESTING DATA
        /// Method to get Calendar basic data using touch and several table data calculations - TG-733
        /// This method is for testing the calendar data, it can be modified or replaced by another method after Migration.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="locationCode"></param>
        /// <returns></returns>
        public ScheduleCalendar Get_TG733_GetFacilityCapacityData(DateTime startDate, DateTime endDate, string locationCode)
        {
            #region SFERP-TODO-ESBMTD
            /* 
             * Added by Sohan
             * Get capacity data 
             * Comment below section to remove SL dlls to build the code.
             * Remove all below section after adding get ESB method and testing.
             */



            /* 
             * TODO - Add ESB method call to verify.
             * Return Unit Type Name if Unit and Unit Type exists with name.   
             */

            #endregion

            ScheduleCalendar scheduleCalendar = new ScheduleCalendar();

            LocalDataHandler dh = new LocalDataHandler();
            DataSet dsCapacity = dh.Get_TG733_GetFacilityCapacityData(startDate, endDate, locationCode);

            if (dsCapacity != null && dsCapacity.Tables.Count > 0 && dsCapacity.Tables[0] != null && dsCapacity.Tables[0].Rows.Count > 0)
            {
                for (DateTime dtSchedule = startDate; dtSchedule < endDate.AddDays(1); dtSchedule = dtSchedule.AddDays(1))
                {
                    #region Initialize Schedule Calendar tables for Scheduled day
                    ScheduleCalendar.AMDataTable amCalendar = scheduleCalendar.AM;
                    ScheduleCalendar.PMDataTable pmCalendar = scheduleCalendar.PM;
                    ScheduleCalendar.AnyTimeDataTable anytimeCalendar = scheduleCalendar.AnyTime;

                    ScheduleCalendar.AMRow amRow = amCalendar.NewAMRow();
                    ScheduleCalendar.PMRow pmRow = pmCalendar.NewPMRow();
                    ScheduleCalendar.AnyTimeRow anyTimeRow = anytimeCalendar.NewAnyTimeRow();

                    amRow.StoreStatus = "";
                    pmRow.StoreStatus = "";
                    anyTimeRow.StoreStatus = "";

                    amRow.TripMiles = 0;
                    pmRow.TripMiles = 0;
                    anyTimeRow.TripMiles = 0;

                    amRow.Date = dtSchedule;
                    pmRow.Date = dtSchedule;
                    anyTimeRow.Date = dtSchedule;

                    #endregion

                    DataRow[] drAnytime = dsCapacity.Tables[0].Select("TouchTime='Anytime' and CAPDate='" + dtSchedule.ToString("MM/dd/yyyy") + "'");
                    if (drAnytime.Length > 0)
                    {
                        anyTimeRow.FacilityMiles = Convert.ToDecimal(drAnytime[0]["FacilityMiles"]);
                        anyTimeRow.FullFacilityMiles = Convert.ToDecimal(drAnytime[0]["FullFacilityMiles"]);
                        anyTimeRow.BookedMiles = Convert.ToDecimal(drAnytime[0]["BookedMiles"]);
                        anytimeCalendar.AddAnyTimeRow(anyTimeRow);
                    }

                    DataRow[] drAM = dsCapacity.Tables[0].Select("TouchTime='AM' and CAPDate='" + dtSchedule.ToString("MM/dd/yyyy") + "'");
                    if (drAM.Length > 0)
                    {
                        amRow.FacilityMiles = Convert.ToDecimal(drAM[0]["FacilityMiles"]);
                        amRow.BookedMiles = Convert.ToDecimal(drAM[0]["BookedMiles"]);
                        amCalendar.AddAMRow(amRow);
                    }

                    DataRow[] drPM = dsCapacity.Tables[0].Select("TouchTime='PM' and CAPDate='" + dtSchedule.ToString("MM/dd/yyyy") + "'");
                    if (drPM.Length > 0)
                    {
                        pmRow.FacilityMiles = Convert.ToDecimal(drPM[0]["FacilityMiles"]);
                        pmRow.BookedMiles = Convert.ToDecimal(drPM[0]["BookedMiles"]);
                        pmCalendar.AddPMRow(pmRow);
                    }
                }
            }

            return scheduleCalendar;
        }


        /// <summary>
        /// DONE
        /// Method to get touch available schedule for capacity calculation for particular facility - TG-776.
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        public DataSet GetTouchAvailableSchedule(TouchAvailableScheduleRequest requestObject)
        {
            DataSet dsSchedule = null;

            try
            {
                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.GetTouchAvailableSchedule, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    dsSchedule = JsonConvert.DeserializeObject<DataSet>(response);
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            return dsSchedule;
        }

        /// <summary>
        /// DONE
        /// Method to get touch available schedule for capacity calculation for particular facility - TG-776.
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        public DataSet GetUnitInfo(string locationCode, string unitName, string qorId = null)
        {
            DataSet dsUnitInfo = null;

            try
            {
                var requestObject = new UnitInfoRequest
                {
                    qorId = qorId,
                    locationCode = locationCode,
                    unitName = unitName
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.GetUnitInfo, requestJson);

                //SPERP-TODO-CTRMV , Comment this section to get data from stub/dummy json files. 
                response = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetAddedUnit_2912.txt"));

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    dsUnitInfo = JsonConvert.DeserializeObject<DataSet>(response);

            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            return dsUnitInfo;

        }

        /// <summary>
        /// DONE
        /// Method to get touch available schedule for capacity calculation for particular facility - TG-776.
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        public PR.Entities.EsbEntities.Brand.RootObject GetBrandInfo(string qorId)
        {
            PR.Entities.EsbEntities.Brand.RootObject brandInfo = null;
            try
            {
                var requestObject = new BrandInfoRequest
                {
                    qorId = qorId
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.GetBrandInfo, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    brandInfo = JsonConvert.DeserializeObject<PR.Entities.EsbEntities.Brand.RootObject>(response);
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            return brandInfo;

        }


        /// <summary>
        /// PENDING
        /// Method to get Unit TripNumber for email. - TG-776.
        /// </summary>
        /// <param name="qorId"></param> 
        /// <returns></returns>
        public string GetTripNumber(int qorId = 0)
        {
            string tripNumber = "";
            try
            {
                DataSet dsUnitInfo = GetUnitInfo("", "", qorId.ToString());

                if (dsUnitInfo != null && dsUnitInfo.Tables.Count > 0 && dsUnitInfo.Tables[0] != null && dsUnitInfo.Tables[0].Rows.Count > 0)
                {
                    tripNumber = Convert.ToString(dsUnitInfo.Tables[0].Rows[0]["TripNumber"]);
                }
                else
                {
                    throw new Exception("EsbMethod.GetTripNumber.GetUnitInfo - No unit info available.");
                }

            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            return tripNumber;
        }
    }
}
