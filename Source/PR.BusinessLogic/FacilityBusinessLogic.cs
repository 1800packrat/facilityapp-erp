using System;
using System.Collections.Generic;
using System.Data;
using PR.Entities;
using PR.ExternalInterfaces;
using PR.DataHandler;

namespace PR.BusinessLogic
{
    public partial class SMDBusinessLogic : BaseBusinessLogicClass
    {
        private string _userId = String.Empty;
        public SMDBusinessLogic(string logUserId)
        {
            _userId = logUserId;
            handler = new LocalDataHandler();
        }

        /// <summary>
        /// Method to get list of all Sites/Facilities
        /// </summary>
        /// <returns></returns>
        public List<SiteInfo> GetSiteInformation()
        {
            DataSet dsSite = new DataSet();
            LocalComponent local = new LocalComponent();
            return local.GetSiteInformation();
        }

        /// <summary>
        /// Method to get Site/Facility based on Location Code.
        /// </summary> 
        /// <param name="locationCode"></param> 
        /// <returns></returns>
        public SiteInfo GetSiteInformation(string locationCode)
        {
            LocalComponent local = new LocalComponent();
            return local.GetSiteInformation(locationCode);
        }

        /// <summary>
        /// Method to get Site/Facility based on storeNumber.
        /// </summary>
        /// <param name="storeNumber"></param>
        /// <returns></returns>
        public SiteInfo GetSiteInformationWithMarkets(string storeNumber)
        {
            LocalComponent local = new LocalComponent();

            return local.GetSiteInformationWithMarkets(storeNumber);
        }

        /// <summary>
        /// Method to get list of Site/Facility along with market Facilities.
        /// </summary> 
        /// <param name="locationCode"></param> 
        /// <returns></returns>
        public List<SiteInfo> GetSiteInformationWithMarkets()
        {
            LocalComponent local = new LocalComponent();

            return local.GetSiteInformationWithMarkets();
        }

        /// <summary>
        /// Method to Calculate distance by using PC*Miler service
        /// </summary>
        /// <param name="orgination"></param>
        /// <param name="destination"></param>
        /// <returns></returns> 
        public AddressValidator GetDistance(Address origination, Address destination)
        {
            //PCMInterface pcmInterface = new PCMInterface();
            PRAddressAPI pcmInterface = new PRAddressAPI();

            //PCMilerReurnClass pcmReturn = new PCMilerReurnClass();
            AddressValidator pcmReturn = new AddressValidator();

            pcmReturn = pcmInterface.GetDistance(origination, destination);

            return pcmReturn;
        }

        /// <summary>
        /// Mesthod to Validate address through ZP4 service.
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public ValidatedAddress ValidateAddress(Address address)
        {
            //string validatedAddr1 = String.Empty;
            //string validatedAddr2 = String.Empty;
            //string validatedCity = String.Empty;
            //string validatedState = String.Empty;
            //string validatedZip = String.Empty;
            //string validationReturn = String.Empty;
            //string error = String.Empty;
            //string warning = String.Empty;
            //string DPV = String.Empty;
            // int code = 0;

            ValidatedAddress validAddress = new ValidatedAddress();
            //ClassAddressValidation2 addressValidator = new ClassAddressValidation2(String.Empty);
            PRAddressAPI oPRAddressAPI = new PRAddressAPI();
            //if (IsUsZipCode(address.Zip))
            //{
            //validationReturn = addressValidator.CheckAddressEx(address.AddressLine1, address.AddressLine2, address.City, address.State,
            //                                            address.Zip, ref validatedAddr1, ref validatedAddr2, ref validatedCity, ref validatedState,
            //                                            ref validatedZip, ref error, ref warning, ref DPV, ref code);

            validAddress = oPRAddressAPI.ValidateAddress(address);

            //if (error == String.Empty)
            //{
            //    validAddress.AddressLine1 = validatedAddr1;
            //    validAddress.AddressLine2 = validatedAddr2;
            //    validAddress.City = validatedCity;
            //    validAddress.State = validatedState;
            //    if (validatedZip.Length > 5 && !IsUsZipCode(validatedZip))
            //    {
            //        validAddress.Zip = validatedZip;// 
            //    }
            //    else if (validatedZip.Length > 5 && IsUsZipCode(validatedZip))
            //    {
            //        validAddress.Zip = validatedZip.Substring(0, 5);
            //    }
            //    else
            //    {
            //        validAddress.Zip = validatedZip;
            //    }
            //    validAddress.Error = error;
            //    validAddress.Warning = warning;
            //    validAddress.DPV = DPV;
            //    validAddress.IsAddressValid = true;
            //}
            //else
            //{
            //    validAddress.AddressLine1 = address.AddressLine1;
            //    validAddress.AddressLine2 = address.AddressLine2;
            //    validAddress.City = address.City;
            //    validAddress.State = address.State;
            //    validAddress.Zip = address.Zip;
            //    validAddress.Error = error;
            //    validAddress.Warning = warning;
            //    validAddress.DPV = DPV;
            //    validAddress.IsAddressValid = false;
            //}
            //}
            //else
            //{
            //    //PCMInterface pcmiler = new PCMInterface();
            //    PRAddressAPI pcmiler = new PRAddressAPI();

            //    validAddress = pcmiler.ValidateAddress(address);
            //}


            return validAddress;
        }

        /// <summary>
        /// Method to get Preferred Delivery Date for any touch based on QORID
        /// </summary>
        /// <param name="qorid"></param>
        /// <returns></returns>
        public DateTime GetPreferredDeliveryDate(int qorid)
        {
            DateTime preferredDeliveryDate = DateTime.MinValue;
            LocalComponent local = new LocalComponent();
            DataSet dsPRGQuoteorderLog = new DataSet();
            dsPRGQuoteorderLog = local.GetPRGQuoteOrderLogData(qorid);
            if (dsPRGQuoteorderLog.Tables.Count > 0 && dsPRGQuoteorderLog.Tables[0].Rows.Count > 0 && dsPRGQuoteorderLog.Tables[0].Rows[0]["PreferredDeliveryDate"] != DBNull.Value)
            {
                preferredDeliveryDate = Convert.ToDateTime(dsPRGQuoteorderLog.Tables[0].Rows[0]["PreferredDeliveryDate"]);
            }
            return preferredDeliveryDate;
        }


    }
}
