using System;

namespace ERP.DataHandler
{
    public class InvalidWebAccessExceptions : ApplicationException
    {
        public InvalidWebAccessExceptions()
            : base()
        {
        }

        public InvalidWebAccessExceptions(string message)
            : base(message)
        {
        }

        public InvalidWebAccessExceptions(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class InvalidLocationCodeException : ApplicationException
    {
        public InvalidLocationCodeException()
            : base()
        {
        }

        public InvalidLocationCodeException(string message)
            : base(message)
        {
        }

        public InvalidLocationCodeException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class InvalidLogonCredentialsException : ApplicationException
    {
        public InvalidLogonCredentialsException()
            : base()
        {
        }

        public InvalidLogonCredentialsException(string message)
            : base(message)
        {
        }

        public InvalidLogonCredentialsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class CouldNotShapeNonTransportationChargesException : ApplicationException
    {
        public CouldNotShapeNonTransportationChargesException()
            : base()
        {
        }

        public CouldNotShapeNonTransportationChargesException(string message)
            : base(message)
        {
        }

        public CouldNotShapeNonTransportationChargesException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class DatasetNotInitializedException : ApplicationException
    {
        public DatasetNotInitializedException()
            : base()
        {
        }

        public DatasetNotInitializedException(string message)
            : base(message)
        {
        }

        public DatasetNotInitializedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    public class PromotionHasBeenApplied : ApplicationException
    {
      public PromotionHasBeenApplied()
        : base()
      {
      }

      public PromotionHasBeenApplied(string message)
        : base(message)
      {
      }

      public PromotionHasBeenApplied(string message, Exception innerException)
        : base(message, innerException)
      {
      }
    }
    public class WebConnectionNotAvailableException : ApplicationException
    {
        public WebConnectionNotAvailableException()
            : base()
        {
        }
        public WebConnectionNotAvailableException(string message)
            : base(message)
        {
        }
        public WebConnectionNotAvailableException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class SessionTimeOutException : ApplicationException
    {
        public SessionTimeOutException()
            : base()
        {
        }
        public SessionTimeOutException(string message)
            : base(message)
        {
        }
        public SessionTimeOutException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    public class PromoNotAppliedException : ApplicationException
    {
        public PromoNotAppliedException()
            : base()
        {
        }
        public PromoNotAppliedException(string message)
            : base(message)
        {
        }
        public PromoNotAppliedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    public class OfferNotAssociatedException : ApplicationException
    {
        public OfferNotAssociatedException()
            : base()
        {
        }
        public OfferNotAssociatedException(string message)
            : base(message)
        {
        }
        public OfferNotAssociatedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    public class DuplicateQORException : ApplicationException
    {
        public DuplicateQORException()
            : base()
        {
        }
        public DuplicateQORException(string message)
            : base(message)
        {
        }
        public DuplicateQORException(string message, Exception innerException)
            : base(message,innerException)
        {
        }
    }

    public class PriceIncreaseException : ApplicationException
    {
        public PriceIncreaseException()
            : base()
        {
        }
        public PriceIncreaseException(string message)
            : base(message)
        {
        }
        public PriceIncreaseException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

    }

    public class QuoteErxpiredException : ApplicationException
    {
        public QuoteErxpiredException()
            : base()
        {
        }
        public QuoteErxpiredException(string message)
            : base(message)
        {
        }
        public QuoteErxpiredException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class QORNotFountException : ApplicationException
    {
        public QORNotFountException()
            : base()
        {
        }
        public QORNotFountException(string message)
            : base(message)
        {
        }
        public QORNotFountException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class CustomerEmailNotMatchedException : ApplicationException
    {
        public CustomerEmailNotMatchedException()
            : base()
        {
        }
        public CustomerEmailNotMatchedException(string message)
            : base(message)
        {
        }
        public CustomerEmailNotMatchedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class NotActiveQuoteException : ApplicationException
    {
        public NotActiveQuoteException()
            : base()
        {
        }
        public NotActiveQuoteException(string message)
            : base(message)
        {
        }
        public NotActiveQuoteException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class ReservationProcessStartedException : ApplicationException
    {
        public ReservationProcessStartedException()
            : base()
        {
        }
        public ReservationProcessStartedException(string message)
            : base(message)
        {
        }
        public ReservationProcessStartedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class PromotionExpiredException : ApplicationException
    {
        public PromotionExpiredException()
            : base()
        {
        }
        public PromotionExpiredException(string message)
            : base(message)
        {
        }
        public PromotionExpiredException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class QuoteConvertedToOrderException : ApplicationException
    {
         public QuoteConvertedToOrderException()
            : base()
        {
        }
        public QuoteConvertedToOrderException(string message)
            : base(message)
        {
        }
        public QuoteConvertedToOrderException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class TransiteCallException : ApplicationException
    {
        public TransiteCallException()
            : base()
        {
        }
        public TransiteCallException(string message)
            : base(message)
        {
        }
        public TransiteCallException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class BluePayCreditCardAuthorizationFailedException : ApplicationException
    {
        public BluePayCreditCardAuthorizationFailedException()
            : base()
        {
        }
        public BluePayCreditCardAuthorizationFailedException(string message)
            : base(message)
        {
        }
        public BluePayCreditCardAuthorizationFailedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    public class SitelinkCreditCardPaymentException : ApplicationException
    {
        public SitelinkCreditCardPaymentException()
            : base()
        {
        }
        public SitelinkCreditCardPaymentException(string message)
            : base(message)
        {
        }
        public SitelinkCreditCardPaymentException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class SiteNotFoundException : ApplicationException
    {
        public SiteNotFoundException()
            : base()
        {
        }
        public SiteNotFoundException(string message)
            : base(message)
        {
        }
        public SiteNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class CustomerMovedOutInactiveLedgerException : ApplicationException
    {
        public CustomerMovedOutInactiveLedgerException()
            : base()
        {
        }
        public CustomerMovedOutInactiveLedgerException(string message)
            : base(message)
        {
        }
        public CustomerMovedOutInactiveLedgerException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class UnitAlreadyExistsException : ApplicationException
    {
        public UnitAlreadyExistsException()
            : base()
        {
        }
        public UnitAlreadyExistsException(string message)
            : base(message)
        {
        }
        public UnitAlreadyExistsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class InvalidUnitException : ApplicationException
    {
        public InvalidUnitException()
            : base()
        {
        }
        public InvalidUnitException(string message)
            : base(message)
        {
        }
        public InvalidUnitException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class OnlinePaymentsDisabledException : ApplicationException
    {
        public OnlinePaymentsDisabledException()
            : base()
        {
        }
        public OnlinePaymentsDisabledException(string message)
            : base(message)
        {
        }
        public OnlinePaymentsDisabledException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class InvalidQorListException : ApplicationException
    {
        public InvalidQorListException()
            : base()
        {
        }
        public InvalidQorListException(string message)
            : base(message)
        {
        }
        public InvalidQorListException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class ValidateQuoteException : ApplicationException
    {
        public ValidateQuoteException()
            : base()
        {
        }
        public ValidateQuoteException(string message)
            : base(message)
        {
        }
        public ValidateQuoteException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
