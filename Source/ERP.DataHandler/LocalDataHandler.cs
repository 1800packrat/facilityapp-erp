using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using PR.Entities; 
using System.Web; 

namespace ERP.DataHandler
{
    public class LocalDataHandler : BaseDataHandler
    {
        public LocalDataHandler()
        {
            Data.LoadSettings();
        }

        public DataSet GetUserAndRole(string UserName, string Password)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string sqlText = @"SELECT  u.RowID, u.UserName, u.PWD, u.FirstName, u.LastName, u.Address1, u.Address2, u.City, u.State, u.PostalCode, r.RoleDesc AS Role, r.ID AS RoleID, u.Active AS IsActive 
FROM [User] u 
INNER JOIN [Role] r ON r.ID = u.RoleID 
WHERE u.UserName = @Username AND u.PWD = @pwd AND u.Active = 1";

                //SqlParameter[] param = new SqlParameter[2];
                //param[0] = new SqlParameter();
                //param[0].ParameterName = "@Username";
                //param[0].Value = UserName;
                //param[1] = new SqlParameter();
                //param[1].ParameterName = "@pwd";
                //param[1].Value = Password;
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@Username", Value = UserName };
                param[1] = new SqlParameter { ParameterName = "@pwd", Value = Password };

                conn = Data.GetPRConnection();
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText, param);

                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetZipCodeInfo(string zip, string connString)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                String sqlText = "Select SiteID,bServicedBy,bESATServiced,bLDMServiced from ZipCodeOwners WHERE sZipCode = @ZipCode";
                conn = Data.GetConnection(connString);

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@ZipCode";
                param[0].Value = zip;

                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText, param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetZipCodeInfo(string zip, int siteId, string connString)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                String sqlText = "Select SiteID,bServicedBy,bESATServiced,bLDMServiced from ZipCodeOwners WHERE sZipCode = @sZipCode AND SiteId = @SiteId";
                conn = Data.GetConnection(connString);

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@sZipCode";
                param[0].Value = zip;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@SiteId";
                param[1].Value = zip;

                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText, param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetZipCodeRestriction(string zip, string connString)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string sqlText = @"select o.sZipCode as ZipCode, r.sDescShort as ShortDesc, r.sDesc as Restriction 
from ZipCodeRestrictions r 
inner join ZipCodeOwners o on r.ZipCodeRestrictionID = o.ZipCodeRestrictionID 
where o.sZipCode = @sZipCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@sZipCode";
                param[0].Value = zip;

                conn = Data.GetConnection(connString);
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText, param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public bool IsOldDominionZip(string zip)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string sqlText = string.Format("select ZipCode from ODZipCodes where ZipCode=@ZipCode and IsDeleted=0", zip);

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@ZipCode";
                param[0].Value = zip;

                conn = Data.GetPRConnection();
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText, param);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet FetchCustomerAccountDetails(int loginId)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@loginId";
            param[0].Value = loginId;
            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_sel_CWCustLogin", param);
            Data.CloseConnection(conn);
            return ds;
        }

        public DataSet GetMktgReasons(int SiteID, string connString)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                DateTime todaysDate = DateTime.Today;
                String sqlText = @"SELECT MarketingID, SiteID, MarketingDesc_TermID, sDefMarketingDesc, sMarketingDesc FROM Marketing WHERE (SiteID = 1) AND (dDeleted IS NULL or dDeleted <=  @todaysDate')";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@todaysDate";
                param[0].Value = todaysDate;

                conn = Data.GetConnection(connString);
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText, param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetMarketingSDources(string locationCode)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@LocationCode";
                param[0].Value = locationCode;
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetMarketingList", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ds;
        }

        public DataSet GetWarehouseAddress(string locationCode, int marketingId)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@LocationCode";
                param[0].Value = locationCode;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@MarketId";
                param[1].Value = marketingId;
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetWarehouseAddress", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ds;
        }

        public DataSet GetUnitByUnitName(string locationCode, string unitName)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[0].ParameterName = "@LocationCode";
                param[0].Value = locationCode;
                param[1].ParameterName = "@UnitName";
                param[1].Value = unitName;
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetUnitInfo", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ds;
        }

        public DataSet GetDisposition(string connString)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                DateTime todaysDate = DateTime.Today;
                String sqlText = @"SELECT [QTCancellationTypeID] AS ID, [sCancellationDesc] AS DispositionDesc FROM [QTCancellationTypes] WHERE (dDeleted IS NULL or dDeleted <=  @todaysDate)";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@todaysDate";
                param[0].Value = todaysDate;

                conn = Data.GetConnection(connString);
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText, param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetCorpCodeAndLocationCode(string zip, string connstring)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                StringBuilder sqlText = new StringBuilder();
                sqlText.Append("SELECT Sites.sLocationCode, Owners.sCorpCode ");
                sqlText.Append("FROM Sites ");
                sqlText.Append("INNER JOIN ZipCodeOwners ON Sites.SiteID = ZipCodeOwners.SiteID ");
                sqlText.Append("INNER JOIN Owners ON Sites.OwnerID = Owners.OwnerID ");
                sqlText.Append("WHERE ZipCodeOwners.sZipCode = @zip");

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@zip";
                param[0].Value = zip;

                conn = Data.GetConnection(connstring);
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText.ToString(), param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetCorpCodeAndLocationCode(string zip, int siteId, string connstring)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                StringBuilder sqlText = new StringBuilder();
                sqlText.Append("SELECT Sites.sLocationCode, Owners.sCorpCode, Sites.SiteID ");
                sqlText.Append("FROM Sites ");
                sqlText.Append("INNER JOIN ZipCodeOwners ON Sites.SiteID = ZipCodeOwners.SiteID ");
                sqlText.Append("INNER JOIN Owners ON Sites.OwnerID = Owners.OwnerID ");
                sqlText.Append("WHERE ZipCodeOwners.sZipCode = @zip And ZipCodeOwners.SiteID = @siteId");

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@zip";
                param[0].Value = zip;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@siteId";
                param[1].Value = siteId;

                conn = Data.GetConnection(connstring);
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText.ToString(), param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetZipCodeInfoFromGlobalView(string zip)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string sql = "Select ZipCodeOwnerID, SiteID, sZipCode, bOwned, bServicedBy, bLDMServiced, bESATServiced, MarketId, DBName from VW_ZipCodeowners where sZipCode=@zip";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@zip";
                param[0].Value = zip;

                conn = Data.GetPRConnection();
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        /// <summary>
        /// Added by SFT Amit
        /// Date Added on 05/19/2016
        /// These method only used in Passport, as we do have Product Line in Passport and that to only for LDM, neither for Local nor for Trailer
        /// Not changing anything existing which are being used by other Application (CATS, CW) as they do not use product line as of now
        /// This is new Tables VW_ZipCodeOwners_ALL which consist of all facilities including StatusRowID > 64
        /// </summary>
        /// <param name="zip"></param>
        /// <returns></returns>
        public DataSet GetALLProductLineZipCodeInfo(string zip)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string sql = "Select ZipCodeOwnerID, SiteID, sZipCode, bOwned, bServicedBy, bLDMServiced, bESATServiced, MarketId, DBName, StatusRowID from VW_ZipCodeOwners_ALL where sZipCode= @zip";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@zip";
                param[0].Value = zip;

                conn = Data.GetPRConnection();
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetCorpCodeAndLocationCode(int SiteID, string connstring)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                StringBuilder sqlText = new StringBuilder();
                sqlText.Append("SELECT Sites.sLocationCode, Owners.sCorpCode ");
                sqlText.Append("FROM Sites INNER JOIN ");
                sqlText.Append("Owners ON Sites.OwnerID = Owners.OwnerID ");
                sqlText.Append("WHERE Sites.SiteID = @SiteID");

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@SiteID";
                param[0].Value = SiteID;

                conn = Data.GetConnection(connstring);
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText.ToString(), param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public int GetSiteID(string LocationCode, string CorpCode, string connstring)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                StringBuilder sqlText = new StringBuilder();
                sqlText.Append("SELECT Sites.SiteID ");
                sqlText.Append("FROM Owners ");
                sqlText.Append("INNER JOIN Sites ON Sites.OwnerID = Owners.OwnerID ");
                sqlText.Append(string.Format("WHERE Sites.sLocationCode = @LocationCode"));
                sqlText.Append(string.Format("AND Owners.sCorpCode = @CorpCode"));

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@LocationCode";
                param[0].Value = LocationCode;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@CorpCode";
                param[1].Value = CorpCode;

                conn = Data.GetConnection(connstring);
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText.ToString(), param);
                return (int)ds.Tables[0].Rows[0]["SiteID"];
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public int GetSiteID(string locationCode)
        {
            int siteId = 0;
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                string sql = "select slsiteid from rdfacility where slloccode= @locationCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@locationCode";
                param[0].Value = locationCode;

                siteId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return siteId;
        }

        public DataSet GetLoginInfo(string firstName, string lastName, string password)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsLogin = new DataSet();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();

                param[0].ParameterName = "@firstName";
                param[0].Value = firstName;
                param[1].ParameterName = "@lastName";
                param[1].Value = lastName;
                param[2].ParameterName = "@password";
                param[2].Value = password;

                dsLogin = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_LogIn", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsLogin;
        }

        public DataSet CheckUser(string userId)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsLogin = new DataSet();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[1];
                param[0] = new SqlParameter();


                param[0].ParameterName = "@UserID";
                param[0].Value = userId;


                dsLogin = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_CheckUser", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsLogin;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="company"></param>
        /// <param name="email"></param>
        /// <param name="phoneNo"></param>
        /// <param name="createdDate"></param>
        /// <param name="cretaedBy"></param>
        /// <param name="zipCode1"></param>
        /// <param name="zipCode2"></param>
        /// <param name="moveType"></param>
        public void InsertUnservicedZipCodeInfo(string firstName, string lastName, string company, string email, string phoneNo, DateTime createdDate,
                string createdBy, string zipCode1, string zipCode2, string moveType)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlParameter[] param;
                param = new SqlParameter[10];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();
                param[6] = new SqlParameter();
                param[7] = new SqlParameter();
                param[8] = new SqlParameter();
                param[9] = new SqlParameter();

                param[0].ParameterName = "@FirstName";
                param[0].Value = firstName;
                param[1].ParameterName = "@LastName";
                param[1].Value = lastName;
                param[2].ParameterName = "@Company";
                param[2].Value = company;
                param[3].ParameterName = "@EMail";
                param[3].Value = email;
                param[4].ParameterName = "@PhoneNo";
                param[4].Value = phoneNo;
                param[5].ParameterName = "@CreatedDate";
                param[5].Value = createdDate;
                param[6].ParameterName = "@CreatedBy";
                param[6].Value = createdBy;
                param[7].ParameterName = "@ZipCode1";
                param[7].Value = zipCode1;
                param[8].ParameterName = "@ZipCode2";
                param[8].Value = zipCode2;
                param[9].ParameterName = "@MoveType";
                param[9].Value = moveType;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRGRequestedZipCodes", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="dtm"></param>
        /// <param name="siteId"></param>
        /// <param name="qorid"></param>
        /// <param name="tenantId"></param>
        /// <param name="activity"></param>
        public void InsertActivityLog(string userId, DateTime dtm, int siteId, int qorid, int tenantId, string activity)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[6];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();

                param[0].ParameterName = "@UserID";
                param[0].Value = userId;
                param[1].ParameterName = "@DTM";
                param[1].Value = dtm;
                param[2].ParameterName = "@SiteID";
                param[2].Value = siteId;
                param[3].ParameterName = "@QORID";
                param[3].Value = qorid;
                param[4].ParameterName = "@TenantID";
                param[4].Value = tenantId;
                param[5].ParameterName = "@Activity";
                param[5].Value = activity;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRGActivityLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        /// <summary>
        /// Adds the log entries to track the quote/order created by and created date.
        /// </summary>
        /// <param name="qorid"></param>
        /// <param name="quoteCreateddBy"></param>
        /// <param name="convertedToorderBy"></param>
        public void InsertPRQuoteOrderLog(int qorid, string zip1, string zip2, string moveType, string quoteCreateddBy, int desiredNumberOfMonth)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //First check if the QORID already has an entry in the table
                DataSet dsQOR = new DataSet();
                string query = "Select RowId from PRGQuoteOrderLog Where QORID = @qorid";
                //dsQOR = SqlHelper.ExecuteDataset(conn, CommandType.Text, String.Format("Select RowId from PRGQuoteOrderLog Where QORID={0}", qorid));
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@qorid", qorid);
                dsQOR = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);

                if (dsQOR.Tables.Count > 0)
                {
                    if (dsQOR.Tables[0].Rows.Count > 0)
                    {
                        throw new DuplicateQORException(String.Format("The QOR {0} already exist in PRGQuoteOrderLogTable", qorid));
                    }
                }

                //SqlParameter[] param;
                param = new SqlParameter[6];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();

                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1].ParameterName = "@Zip1";
                param[1].Value = zip1;
                param[2].ParameterName = "@Zip2";
                param[2].Value = zip2;
                param[3].ParameterName = "@MoveType";
                param[3].Value = moveType;
                param[4].ParameterName = "@QuoteCreatedBy";
                param[4].Value = quoteCreateddBy;
                param[5].ParameterName = "@DesiredNumberOfMonth";
                param[5].Value = desiredNumberOfMonth;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRGQuoteOrderLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        /// <summary>
        /// Adds the log entries to track the quote/order created by and created date.
        /// </summary>
        /// <param name="qorid"></param>
        /// <param name="quoteCreateddBy"></param>
        /// <param name="convertedToorderBy"></param>
        public void InsertPRQuoteOrderLog(int qorid, string zip1, string zip2, string moveType, string quoteCreateddBy, int desiredNumberOfMonth, DateTime preferredDeliveryDate)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //First check if the QORID already has an entry in the table
                DataSet dsQOR = new DataSet();

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@qorid";
                param[0].Value = qorid;

                dsQOR = SqlHelper.ExecuteDataset(conn, CommandType.Text, ("Select RowId from PRGQuoteOrderLog Where QORID=@qorid"), param);
                if (dsQOR.Tables.Count > 0)
                {
                    if (dsQOR.Tables[0].Rows.Count > 0)
                    {
                        throw new DuplicateQORException(String.Format("The QOR {0} already exist in PRGQuoteOrderLogTable", qorid));
                    }
                }

                //                string sql = String.Format(@"insert into prgquoteorderlog(QORID,Zip1,Zip2,MoveType,PreferredDeliveryDate,QuoteCreatedBy,QuoteCreatedDate,DesiredNumberOfMonth) 
                //values({0},'{1}','{2}','{3}','{4}','{5}','{6}',{7})", qorid, zip1, zip2, moveType, preferredDeliveryDate, quoteCreateddBy, DateTime.Now, desiredNumberOfMonth);
                string sql = @"insert into prgquoteorderlog(QORID,Zip1,Zip2,MoveType,PreferredDeliveryDate,QuoteCreatedBy,QuoteCreatedDate,DesiredNumberOfMonth) 
values(@qorid, @zip1, @zip2, @moveType, @preferredDeliveryDate, @quoteCreateddBy,@createdDate, @desiredNumberOfMonth)";

                param = new SqlParameter[8];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@qorid";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@zip1";
                param[1].Value = zip1;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@zip2";
                param[2].Value = zip2;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@moveType";
                param[3].Value = moveType;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@preferredDeliveryDate";
                param[4].Value = preferredDeliveryDate;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@quoteCreateddBy";
                param[5].Value = quoteCreateddBy;
                param[6] = new SqlParameter();
                param[6].ParameterName = "@createdDate";
                param[6].Value = DateTime.Now;
                param[7] = new SqlParameter();
                param[7].ParameterName = "@desiredNumberOfMonth";
                param[7].Value = desiredNumberOfMonth;

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void InsertPRQuoteOrderLogForSitelinkOrder(int qorid, string zip1, string zip2, string moveType, string quoteCreateddBy, int desiredNumberOfMonth, DateTime preferredDeliveryDate)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //First check if the QORID already has an entry in the table
                DataSet dsQOR = new DataSet();

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@qorid";
                param[0].Value = qorid;

                dsQOR = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select RowId from PRGQuoteOrderLog Where QORID=@qorid", param);
                if (dsQOR.Tables.Count > 0)
                {
                    if (dsQOR.Tables[0].Rows.Count > 0)
                    {
                        throw new DuplicateQORException(String.Format("The QOR {0} already exist in PRGQuoteOrderLogTable", qorid));
                    }
                }

                //                string sql = @"insert into prgquoteorderlog(QORID,Zip1,Zip2,MoveType,PreferredDeliveryDate,QuoteCreatedBy,DesiredNumberOfMonth) 
                //values({0},'{1}','{2}','{3}','{4}','{5}','{6}')", qorid, zip1, zip2, moveType, preferredDeliveryDate, quoteCreateddBy, desiredNumberOfMonth;
                string sql = @"insert into prgquoteorderlog(QORID,Zip1,Zip2,MoveType,PreferredDeliveryDate,QuoteCreatedBy,DesiredNumberOfMonth) 
values(@qorid, @zip1, @zip2, @moveType, @preferredDeliveryDate, @quoteCreateddBy,@desiredNumberOfMonth)";

                param = new SqlParameter[7];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@qorid";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@zip1";
                param[1].Value = zip1;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@zip2";
                param[2].Value = zip2;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@moveType";
                param[3].Value = moveType;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@preferredDeliveryDate";
                param[4].Value = preferredDeliveryDate;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@quoteCreateddBy";
                param[5].Value = quoteCreateddBy;
                param[6] = new SqlParameter();
                param[6].ParameterName = "@desiredNumberOfMonth";
                param[6].Value = desiredNumberOfMonth;

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        /// <summary>
        /// Updates the log entries to track the quote/order created by and created date.
        /// </summary>
        /// <param name="qorid"></param>
        /// <param name="convertedToorderBy"></param>
        public void UpdatePRQuoteOrderLog(int qorid, string convertedToorderBy, string cancelledBy, string purchaseOrder)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[4];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();

                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1].ParameterName = "@ConvertedToOrderBy";
                if (convertedToorderBy == String.Empty)
                    param[1].Value = System.DBNull.Value;
                else
                    param[1].Value = convertedToorderBy;

                param[2].ParameterName = "@CancelledBy";
                if (cancelledBy == string.Empty)
                    param[2].Value = System.DBNull.Value;
                else
                    param[2].Value = cancelledBy;

                param[3].ParameterName = "@purchaseORder";
                if (purchaseOrder == string.Empty)
                    param[3].Value = System.DBNull.Value;
                else
                    param[3].Value = purchaseOrder;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_upd_PRGQuoteOrderLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        /// <summary>
        /// Updates the log entries to track the quote/order created by and created date.
        /// </summary>
        /// <param name="qorid"></param>
        /// <param name="convertedToorderBy"></param>
        public void UpdatePRQuoteOrderLog(int qorid, string convertedToorderBy, string cancelledBy, decimal DueOnDelivery, decimal FutureTransportationCharge, decimal RecurringMonthlyCharge)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[6];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();

                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1].ParameterName = "@ConvertedToOrderBy";
                if (convertedToorderBy == String.Empty)
                    param[1].Value = System.DBNull.Value;
                else
                    param[1].Value = convertedToorderBy;

                param[2].ParameterName = "@CancelledBy";
                if (cancelledBy == string.Empty)
                    param[2].Value = System.DBNull.Value;
                else
                    param[2].Value = cancelledBy;

                param[3].ParameterName = "@DueOnDelivery";
                if (DueOnDelivery == decimal.MinValue)
                    param[3].Value = 0;
                else
                    param[3].Value = DueOnDelivery;

                param[4].ParameterName = "@FutureTransportationCharge";
                if (FutureTransportationCharge == decimal.MinValue)
                    param[4].Value = 0;
                else
                    param[4].Value = FutureTransportationCharge;

                param[5].ParameterName = "@RecurringMonthlyCharge";
                if (RecurringMonthlyCharge == decimal.MinValue)
                    param[5].Value = 0;
                else
                    param[5].Value = RecurringMonthlyCharge;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_upd_PRGQuoteOrderLogWithPrices", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void InsertPRErrorLog(DateTime errorDateTime, string userId, string exceptionType, string exceptionMsg, string stackTrace, int qrid)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[6];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();

                param[0].ParameterName = "@ErrorDTM";
                param[0].Value = errorDateTime;
                param[1].ParameterName = "@UserId";
                param[1].Value = userId;
                param[2].ParameterName = "@ExceptionType";
                param[2].Value = exceptionType;
                param[3].ParameterName = "@ExceptionMsg";
                param[3].Value = exceptionMsg;
                param[4].ParameterName = "@StackTrace";
                param[4].Value = stackTrace;
                param[5].ParameterName = "@QRID";
                param[5].Value = qrid;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRGErrorLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void InsertPRErrorLog(DateTime errorDateTime, string userId, string exceptionType, string exceptionMsg, string stackTrace, int qrid, string strAppURL, string DataDump)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[8];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();
                param[6] = new SqlParameter();
                param[7] = new SqlParameter();

                param[0].ParameterName = "@ErrorDTM";
                param[0].Value = errorDateTime;
                param[1].ParameterName = "@UserId";
                param[1].Value = userId;
                param[2].ParameterName = "@ExceptionType";
                param[2].Value = exceptionType;
                param[3].ParameterName = "@ExceptionMsg";
                param[3].Value = exceptionMsg;
                param[4].ParameterName = "@StackTrace";
                param[4].Value = stackTrace;
                param[5].ParameterName = "@QRID";
                param[5].Value = qrid;
                param[6].ParameterName = "@AppURL";
                param[6].Value = strAppURL;
                param[7].ParameterName = "@DataDump";
                param[7].Value = DataDump;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRGErrorLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        /// <summary>
        /// Gets all the CPP available for today.
        /// </summary>
        /// <returns></returns>
        public DataSet GetCPPInfo()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsCPP = new DataSet();
            try
            {
                dsCPP = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_sel_PRGCPPInfo");
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsCPP;
        }

        public DataSet GetFacilityConfigData(string locationCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsFacilityConfig = new DataSet();
            try
            {
                string sqlFacilityConfig = "Select * from WebFacConfig where LocationCode = @locationCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0].ParameterName = "@locationCode";
                param[0].Value = locationCode;

                dsFacilityConfig = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlFacilityConfig, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsFacilityConfig;
        }

        public DataSet GetZipCodeInfo(string zipCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsZip = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[1];

                param[0] = new SqlParameter();
                param[0].ParameterName = "@Zipcode";
                param[0].Value = zipCode;

                dsZip = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_sel_USPSZipCode", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsZip;
        }

        public string GetSLDBConnStringByLocCode(string slLocCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            string slDBName = String.Empty;
            string slDBConnString = String.Empty;
            try
            {
                SqlCommand cmd = new SqlCommand();
                string sql = "Select SLDBName from RDFacility where SLLocCode = @slLocCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@slLocCode";
                param[0].Value = slLocCode;

                slDBName = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param).ToString();
                Data.CloseConnection(conn);
                if (Data._connStringPR.IndexOf("PRSLLocalTest") > 0)
                    slDBConnString = Data._connStringPR.Replace("PRSLLocalTest", slDBName);
                else
                    slDBConnString = Data._connStringPR.Replace("PRSLLocal", slDBName);
            }
            catch (Exception ex)
            {
                try
                {
                    LocalDataHandler local = new LocalDataHandler();
                    local.InsertPRErrorLog(DateTime.Now, "", "Connection String LocationCode: " + slLocCode, ex.Message, ex.StackTrace.ToString(), -1);
                }
                catch
                {
                    throw ex;
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return slDBConnString;
        }

        public void InsertRGLDMAllCust(int ldmId, string firstName, string lastName, decimal numOfUnits, string origfacilityNum, string origFacilityCity, string origFacilityState, string destFacilityNum, string destFacilityCity, string destFacilityState,
                int starsId, string userId)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlCommand cmd = new SqlCommand();
                firstName = firstName.Replace("'", "''");
                lastName = lastName.Replace("'", "''");

                //                string sql = @"Insert into dbo.RGLDMAllCust(LDMID,FirstName,LastName,NumofUnits,OrigFacNum,OrigFacCity,OrigFacState,DestFacNum,DestFacCity,DestFacState,StarsID,StatusFlag,Bookeddate,BookedBy) 
                //values(" + ldmId.ToString() + ",'" + firstName + "','" + lastName + "'," + numOfUnits.ToString() + ",'" + origfacilityNum + "','" + origFacilityCity + "','" + origFacilityState + "','" + 
                //destFacilityNum + "','" + destFacilityCity + "','" + destFacilityState + "','" + starsId.ToString() + "','" + "Active" + "','" + DateTime.Now.Date.ToShortDateString() + "','" + userId + "')";
                string sql = @"Insert into dbo.RGLDMAllCust(LDMID,FirstName,LastName,NumofUnits,OrigFacNum,OrigFacCity,OrigFacState,DestFacNum,DestFacCity,DestFacState,StarsID,StatusFlag,Bookeddate,BookedBy) 
values(@ldmId, @firstName, @lastName, @numOfUnits, @origfacilityNum, @origFacilityCity, @origFacilityState, @destFacilityNum, @destFacilityCity, @destFacilityState, @starsId, @Active, @Bookeddate, @userId)";

                SqlParameter[] param = new SqlParameter[14];
                param[0] = new SqlParameter { ParameterName = "@ldmId", Value = ldmId.ToString() };
                param[1] = new SqlParameter { ParameterName = "@firstName", Value = firstName };
                param[2] = new SqlParameter { ParameterName = "@lastName", Value = lastName };
                param[3] = new SqlParameter { ParameterName = "@numOfUnits", Value = numOfUnits.ToString() };
                param[4] = new SqlParameter { ParameterName = "@origfacilityNum", Value = origfacilityNum };
                param[5] = new SqlParameter { ParameterName = "@origFacilityCity", Value = origFacilityCity };
                param[6] = new SqlParameter { ParameterName = "@origFacilityState", Value = origFacilityState };
                param[7] = new SqlParameter { ParameterName = "@destFacilityNum", Value = destFacilityNum };
                param[8] = new SqlParameter { ParameterName = "@destFacilityCity", Value = destFacilityCity };
                param[9] = new SqlParameter { ParameterName = "@destFacilityState", Value = destFacilityState };
                param[10] = new SqlParameter { ParameterName = "@starsId", Value = starsId.ToString() };
                param[11] = new SqlParameter { ParameterName = "@Active", Value = "Active" };
                param[12] = new SqlParameter { ParameterName = "@Bookeddate", Value = DateTime.Now.Date.ToShortDateString() };
                param[13] = new SqlParameter { ParameterName = "@userId", Value = userId };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
                Data.CloseConnection(conn);

                //Update RGLDMAllCust table with the data from StarsDB
                DataSet dsStarsData = new DataSet();
                // = "usp_get_StarsData";
                //SqlParameter[] param;
                param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@StarsID";
                param[0].Value = starsId;
                conn = Data.GetPRConnection();
                dsStarsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_get_StarsData", param);
                Data.CloseConnection(conn);
                if ((dsStarsData.Tables[0] != null))
                {
                    if ((dsStarsData.Tables[0].Rows.Count > 0))
                    {
                        param = new SqlParameter[12];
                        param[0] = new SqlParameter();
                        param[1] = new SqlParameter();
                        param[2] = new SqlParameter();
                        param[3] = new SqlParameter();
                        param[4] = new SqlParameter();
                        param[5] = new SqlParameter();
                        param[6] = new SqlParameter();
                        param[7] = new SqlParameter();
                        param[8] = new SqlParameter();
                        param[9] = new SqlParameter();
                        param[10] = new SqlParameter();
                        param[11] = new SqlParameter();

                        param[0].ParameterName = "@StarsID";
                        param[0].Value = starsId;
                        param[1].ParameterName = "@LeadSource";
                        param[1].Value = dsStarsData.Tables[0].Rows[0]["LeadSource"];
                        param[2].ParameterName = "@OrigZone";
                        param[2].Value = dsStarsData.Tables[0].Rows[0]["OrigZone"];
                        param[3].ParameterName = "@DestZone";
                        param[3].Value = dsStarsData.Tables[0].Rows[0]["DestZone"];
                        param[4].ParameterName = "@TotalQuotedwoTax";
                        param[4].Value = dsStarsData.Tables[0].Rows[0]["TotalQuotedwoTax"];
                        param[5].ParameterName = "@TotalQuotedwTax";
                        param[5].Value = dsStarsData.Tables[0].Rows[0]["TotalQuotedwTax"];
                        param[6].ParameterName = "@TransportwoTax";
                        param[6].Value = dsStarsData.Tables[0].Rows[0]["TransportwoTax"];
                        param[7].ParameterName = "@OrigXtraMileChg";
                        param[7].Value = dsStarsData.Tables[0].Rows[0]["OrigXtraMileChg"];
                        param[8].ParameterName = "@OrigRentwoTax";
                        param[8].Value = dsStarsData.Tables[0].Rows[0]["OrigRentwoTax"];
                        param[9].ParameterName = "@DestXtraMileChg";
                        param[9].Value = dsStarsData.Tables[0].Rows[0]["DestXtraMileChg"];
                        param[10].ParameterName = "@DestRentwoTax";
                        param[10].Value = dsStarsData.Tables[0].Rows[0]["DestRentwoTax"];
                        param[11].ParameterName = "@ExpediteFee";
                        param[11].Value = dsStarsData.Tables[0].Rows[0]["ExpediteFee"];
                        conn = Data.GetPRConnection();
                        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_upd_RGLDMAllCust_Part", param);
                    }
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void InsertRGWebLDMQuotes(string firstName, string lastName, string originCity, string originState,
          string originZipCode, string destinationCity, string destinationState, string destinantionZipCode,
          DateTime estInitialDelivery, string phone, string email, int numUnits, string refrerralSource)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[14];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();
                param[6] = new SqlParameter();
                param[7] = new SqlParameter();
                param[8] = new SqlParameter();
                param[9] = new SqlParameter();
                param[10] = new SqlParameter();
                param[11] = new SqlParameter();
                param[12] = new SqlParameter();
                param[13] = new SqlParameter();

                param[0].ParameterName = "@Status";
                param[0].Value = 0;
                param[1].ParameterName = "@FirstName";
                param[1].Value = firstName;
                param[2].ParameterName = "@LastName";
                param[2].Value = lastName;
                param[3].ParameterName = "@OriginCity";
                param[3].Value = originCity;
                param[4].ParameterName = "@OriginState";
                param[4].Value = originState;
                param[5].ParameterName = "@OriginZipCode";
                param[5].Value = originZipCode;
                param[6].ParameterName = "@DestinationCity";
                param[6].Value = destinationCity;
                param[7].ParameterName = "@DestinationState";
                param[7].Value = destinationState;
                param[8].ParameterName = "@DestinationZipCode";
                param[8].Value = destinantionZipCode;
                param[9].ParameterName = "@EstInitialDelivery";
                param[9].Value = estInitialDelivery;
                param[10].ParameterName = "@Phone";
                param[10].Value = phone;
                param[11].ParameterName = "@EMail";
                param[11].Value = email;
                param[12].ParameterName = "@NumUnits";
                param[12].Value = numUnits;
                param[13].ParameterName = "@ReferralSource";
                param[13].Value = refrerralSource;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_RGWebLDMQuotes", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetSLGlobalQORFromXRef(int RATSQOR, string WebQOR)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();

                param[0].ParameterName = "@RatsGlobalQORID";
                if (RATSQOR == -1)
                {
                    param[0].Value = DBNull.Value;
                }
                else
                {
                    param[0].Value = RATSQOR;
                }
                param[1].ParameterName = "@WebQORID";
                if (WebQOR == "-1")
                {
                    param[1].Value = DBNull.Value;
                }
                else
                {
                    param[1].Value = WebQOR;
                }

                return SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetSLGlobalQORFromXRef", param);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        #region "Connection Manager"

        public string GetConnection(string applicationName, string sessionId, string firstName, string lastName)
        {
            SqlConnection conn = Data.GetPRConnection();
            string spName = String.Empty;
            DataSet dsURL = new DataSet();
            string url = String.Empty;
            SqlParameter[] param;
            switch (applicationName)
            {
                case "CATS":
                    spName = "usp_GetConnection_CATS";
                    break;
                case "STARS":
                    spName = "usp_GetConnection_STARS";
                    break;
                case "CW":
                    spName = "usp_GetConnection_CW";
                    break;
            }

            if (applicationName == "CW")
            {
                param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@SessionId";
                param[0].Value = sessionId;
            }
            else
            {
                param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();

                param[0].ParameterName = "@SessionId";
                param[0].Value = sessionId;
                param[1].ParameterName = "@FirstName";
                param[1].Value = firstName;
                param[2].ParameterName = "@LastName";
                param[2].Value = lastName;
            }

            try
            {
                dsURL = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, spName, param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            if (dsURL.Tables[0].Rows.Count > 0)
            {
                url = dsURL.Tables[0].Rows[0]["URL"].ToString();
            }
            else
            {
                throw new WebConnectionNotAvailableException("Web connection is not avialable at this time. Please conatct Tech Support.");
            }
            return url;
        }

        public string UpdateConnection(string applicationName, string sessionId)
        {
            SqlConnection conn = Data.GetPRConnection();
            string spName = String.Empty;
            string url = String.Empty;
            SqlParameter[] param;
            DataSet dsURL = new DataSet();
            switch (applicationName)
            {
                case "CATS":
                    spName = "usp_UpdateConnection_CATS";
                    break;
                case "STARS":
                    spName = "usp_UpdateConnection_STARS";
                    break;
                case "CW":
                    spName = "usp_UpdateConnection_CW";
                    break;
            }
            param = new SqlParameter[1];
            param[0] = new SqlParameter();

            param[0].ParameterName = "@SessionId";
            param[0].Value = sessionId;

            try
            {
                dsURL = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, spName, param);
                if (dsURL.Tables[0].Rows.Count > 0)
                {
                    url = dsURL.Tables[0].Rows[0]["URL"].ToString();
                }
                else
                {
                    throw new SessionTimeOutException("Session has been timed out.");
                }
            }
            catch
            {
                throw new SessionTimeOutException("Session has been timed out.");
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return url;
        }

        public void ReleaseConnection(string applicationName, string sessionId)
        {
            SqlConnection conn = Data.GetPRConnection();
            string spName = String.Empty;
            string url = String.Empty;
            SqlParameter[] param;
            switch (applicationName)
            {
                case "CATS":
                    spName = "usp_ReleaseConnection_CATS";
                    break;
                case "STARS":
                    spName = "usp_ReleaseConnection_STARS";
                    break;
                case "CW":
                    spName = "usp_ReleaseConnection_CW";
                    break;
            }
            param = new SqlParameter[1];
            param[0] = new SqlParameter();

            param[0].ParameterName = "@SessionId";
            param[0].Value = sessionId;

            try
            {
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, spName, param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        #endregion "Connection Manager"

        public DataSet GetTouchLimitsFromRats(string locationCode, string startDate, string endDate)
        {
            DataSet dsAvailDates = null;
            SqlParameter[] param;
            param = new SqlParameter[3];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@LocationCode";
            param[0].Value = locationCode;

            param[1] = new SqlParameter();
            param[1].ParameterName = "@STARTDATE";
            param[1].Value = startDate;

            param[2] = new SqlParameter();
            param[2].ParameterName = "@ENDDATE";
            param[2].Value = endDate;
            SqlConnection conn = Data.GetPRConnection();
            dsAvailDates = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_new_ExecScriptForMileagesAndTouches", param);
            Data.CloseConnection(conn);
            return dsAvailDates;
        }

        public DataSet GetFacilityBulletin(string locationCode)
        {
            DataSet dsFacilityBulletin = null;
            SqlParameter[] param;
            param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@LocationCode";
            param[0].Value = locationCode;

            SqlConnection conn = Data.GetPRConnection();
            dsFacilityBulletin = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_new_ExecScriptForFacilityBulletin", param);
            Data.CloseConnection(conn);
            return dsFacilityBulletin;
        }

        public DataSet GetPRGOrderLog(Int64 QORID)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = QORID;
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_sel_PRGQuoteOrderLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ds;
        }

        public DataSet GetCallInfoByExt(string ext)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@Ext";
                param[0].Value = ext;
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_Get_CallInfo", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ds;
        }

        public int SaveCallInfo(int qorid, string ucid, string ani, string tfn, string zip1, string zip2, string promo, string wcid, DateTime callDate, string agentFirstName, string agentLastName, string ext, string source, string clid, DateTime createdDate, int ivrInputQRID)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[16];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QRID";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@UCID";
                param[1].Value = ucid;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@ANI";
                param[2].Value = ani;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@TFN";
                param[3].Value = tfn;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@Zip1";
                param[4].Value = zip1;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@Zip2";
                param[5].Value = zip2;
                param[6] = new SqlParameter();
                param[6].ParameterName = "@Promo";
                param[6].Value = promo;
                param[7] = new SqlParameter();
                param[7].ParameterName = "@WCID";
                param[7].Value = wcid;
                param[8] = new SqlParameter();
                param[8].ParameterName = "@CallDate";
                param[8].Value = callDate;
                param[9] = new SqlParameter();
                param[9].ParameterName = "@AgentFirstName";
                param[9].Value = agentFirstName;
                param[10] = new SqlParameter();
                param[10].ParameterName = "@AgentLastName";
                param[10].Value = agentLastName;
                param[11] = new SqlParameter();
                param[11].ParameterName = "@Ext";
                param[11].Value = ext;
                param[12] = new SqlParameter();
                param[12].ParameterName = "@Source";
                param[12].Value = source;
                param[13] = new SqlParameter();
                param[13].ParameterName = "@CLID";
                param[13].Value = clid;
                param[14] = new SqlParameter();
                param[14].ParameterName = "@CreatedDateTime";
                param[14].Value = createdDate;
                param[15] = new SqlParameter();
                param[15].ParameterName = "@IVRInputQRID";
                param[15].Value = ivrInputQRID;
                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_RGCallInfo", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public int UpdateCallInfo(int rowId, int qrid)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@RowID";
                param[0].Value = rowId;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@QRID";
                param[1].Value = qrid;
                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_upd_CallInfo", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public int InsertActivityLog(int qorid, string activityType, string activityText, string activityBy)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@ActivityType";
                param[1].Value = activityType;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@ActivityText";
                param[2].Value = activityText;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@ActivityBy";
                param[3].Value = activityBy;

                param[4] = new SqlParameter();
                param[4].ParameterName = "@ApplicationName";
                param[4].Value = GetApplicationName();

                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRActivityLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public int InsertActivityLog(int qorid, string activityType, string activityText, string activityBy, int count, decimal pricePerItem, string poscppname)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[8];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@ActivityType";
                param[1].Value = activityType;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@ActivityText";
                param[2].Value = activityText;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@ActivityBy";
                param[3].Value = activityBy;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@Count";
                param[4].Value = count;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@PricePerItem";
                param[5].Value = pricePerItem;
                param[6] = new SqlParameter();
                param[6].ParameterName = "@POSCPPName";
                param[6].Value = poscppname;

                param[7] = new SqlParameter();
                param[7].ParameterName = "@ApplicationName";
                param[7].Value = GetApplicationName();

                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRActivityLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public int InsertActivityLog(int qorid, string activityType, string activityText, string activityBy, int touchLimit, decimal mileageLimit, int touchCount, decimal mileageCount, DateTime touchScheduleDate, string facilityLocCode, string touchApproverName)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[12];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@ActivityType";
                param[1].Value = activityType;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@ActivityText";
                param[2].Value = activityText;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@ActivityBy";
                param[3].Value = activityBy;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@TouchLimit";
                param[4].Value = touchLimit;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@MileageLimit";
                param[5].Value = mileageLimit;
                param[6] = new SqlParameter();
                param[6].ParameterName = "@TouchCount";
                param[6].Value = touchCount;
                param[7] = new SqlParameter();
                param[7].ParameterName = "@MileageCount";
                param[7].Value = mileageCount;
                param[8] = new SqlParameter();
                param[8].ParameterName = "@TouchScheduleDate";
                param[8].Value = touchScheduleDate;
                param[9] = new SqlParameter();
                param[9].ParameterName = "@FacilityLocCode";
                param[9].Value = facilityLocCode;
                param[10] = new SqlParameter();
                param[10].ParameterName = "@TouchApproverName";
                param[10].Value = touchApproverName;

                param[11] = new SqlParameter();
                param[11].ParameterName = "@ApplicationName";
                param[11].Value = GetApplicationName();

                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRActivityLog_TouchScheduling", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        private string GetApplicationName()
        {
            ///Since we are adding below code now added exception block because, dont know how many places this method beeing used. 
            ///If this method hits by any job then we may not get host name, that the reason why added exception block here.
            string applicationName = string.Empty;
            try
            {
                var request = HttpContext.Current.Request;
                applicationName = request.ServerVariables["HTTP_HOST"];

                if (!string.IsNullOrEmpty(applicationName))
                {
                    applicationName = applicationName.ToUpper();
                    if (applicationName.EndsWith(".1800PACKRAT.COM"))
                        applicationName = applicationName.Replace(".1800PACKRAT.COM", "");
                    if (applicationName.Length > 20)
                        applicationName = applicationName.Substring(0, 19); //Since database field name lenght 20 charectors then we are stripping out to 20 characters
                }
            }
            catch { }

            return applicationName;
        }

        public int InsertActivityLog(int qorid, string activityType, string activityText, string activityBy, decimal totalMiles, decimal remainingMiles, DateTime touchScheduleDate, string facilityLocCode, string touchApproverName, decimal touchMiles, decimal reservedMiles, decimal regularMiles, int CAPCategoryId, int CAPTouchTypeId)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[17];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@ActivityType";
                param[1].Value = activityType;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@ActivityText";
                param[2].Value = activityText;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@ActivityBy";
                param[3].Value = activityBy;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@TouchLimit";
                param[4].Value = 0; // We do not capture this with Capacity
                param[5] = new SqlParameter();
                param[5].ParameterName = "@MileageLimit";
                param[5].Value = totalMiles;
                param[6] = new SqlParameter();
                param[6].ParameterName = "@TouchCount";
                param[6].Value = 0; // We do not capture this with Capacity
                param[7] = new SqlParameter();
                param[7].ParameterName = "@MileageCount";
                param[7].Value = remainingMiles;
                param[8] = new SqlParameter();
                param[8].ParameterName = "@TouchScheduleDate";
                param[8].Value = touchScheduleDate;
                param[9] = new SqlParameter();
                param[9].ParameterName = "@FacilityLocCode";
                param[9].Value = facilityLocCode;
                param[10] = new SqlParameter();
                param[10].ParameterName = "@TouchApproverName";
                param[10].Value = touchApproverName;

                param[11] = new SqlParameter();
                param[11].ParameterName = "@TouchMiles";
                param[11].Value = totalMiles;

                param[12] = new SqlParameter();
                param[12].ParameterName = "@ReservedMiles";
                param[12].Value = reservedMiles;

                param[13] = new SqlParameter();
                param[13].ParameterName = "@RegularMiles";
                param[13].Value = regularMiles;

                param[14] = new SqlParameter();
                param[14].ParameterName = "@CAPCategoryId";
                param[14].Value = CAPCategoryId;

                param[15] = new SqlParameter();
                param[15].ParameterName = "@CAPTouchTypeId";
                param[15].Value = CAPTouchTypeId;

                param[16] = new SqlParameter();
                param[16].ParameterName = "@ApplicationName";
                param[16].Value = GetApplicationName();


                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "USP_CAP_INS_PRActivityLog_TouchScheduling", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public int InsertIPrequestData(string requestIp, string zip1, string zip2, string size, DateTime preferredDeliveryDate, int QORID, string wcid, string landingPageId, string quoteType)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[9];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@strRequestIP";
                param[0].Value = requestIp;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@strFromZip";
                param[1].Value = zip1;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@strToZip";
                param[2].Value = zip2;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@strSize";
                param[3].Value = size;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@ProjectedDeliveryDate";
                param[4].Value = preferredDeliveryDate;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@WebQID";
                param[5].Value = QORID.ToString();
                param[6] = new SqlParameter();
                param[6].ParameterName = "@WCID";
                param[6].Value = wcid;
                param[7] = new SqlParameter();
                param[7].ParameterName = "@LandingPageId";
                param[7].Value = landingPageId;
                param[8] = new SqlParameter();
                param[8].ParameterName = "@QuoteType";
                param[8].Value = quoteType;

                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_iprequestdata", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public int InsertActivityLog(int quoteId, int qorId, string activityTypeName, string activityText, string activityBy, string tODRef, int USSCustId)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[9];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@STARSID";
                param[0].Value = quoteId;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@QORID";
                param[1].Value = qorId;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@ActivityTypeName";
                param[2].Value = activityTypeName;

                param[3] = new SqlParameter();
                param[3].ParameterName = "@ActivityText";
                param[3].Value = activityText;

                param[4] = new SqlParameter();
                param[4].ParameterName = "@ActivityBy";
                param[4].Value = activityBy;

                param[5] = new SqlParameter();
                param[5].ParameterName = "@ActivityDateTime";
                param[5].Value = DateTime.Now;

                param[6] = new SqlParameter();
                param[6].ParameterName = "@ApplicationName";
                param[6].Value = GetApplicationName();

                param[7] = new SqlParameter();
                param[7].ParameterName = "@TODRef";
                param[7].Value = tODRef;//TODO

                param[8] = new SqlParameter();
                param[8].ParameterName = "@USSCustId";
                param[8].Value = USSCustId;

                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "USS_Insert_ActivityLogBYActivityName", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public int InsertIPrequestData(string requestIp, string zip1, string zip2, string size, DateTime preferredDeliveryDate, int QORID, string wcid, string landingPageId, string quoteType, string trackingPromoCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[10];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@strRequestIP";
                param[0].Value = requestIp;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@strFromZip";
                param[1].Value = zip1;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@strToZip";
                param[2].Value = zip2;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@strSize";
                param[3].Value = size;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@ProjectedDeliveryDate";
                param[4].Value = preferredDeliveryDate;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@WebQID";
                param[5].Value = QORID.ToString();
                param[6] = new SqlParameter();
                param[6].ParameterName = "@WCID";
                param[6].Value = wcid;
                param[7] = new SqlParameter();
                param[7].ParameterName = "@LandingPageId";
                param[7].Value = landingPageId;
                param[8] = new SqlParameter();
                param[8].ParameterName = "@QuoteType";
                param[8].Value = quoteType;
                param[9] = new SqlParameter();
                param[9].ParameterName = "@TrackingPromoCode";
                param[9].Value = trackingPromoCode;

                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_iprequestdata", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public int InsertIPrequestData(string requestIp, string zip1, string zip2, string size, DateTime preferredDeliveryDate, int QORID, string wcid, string landingPageId, string quoteType, string trackingPromoCode, string email, string LocationCode, int TenantID, bool UserBanflag)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[14];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@strRequestIP";
                param[0].Value = requestIp;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@strFromZip";
                param[1].Value = zip1;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@strToZip";
                param[2].Value = zip2;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@strSize";
                param[3].Value = size;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@ProjectedDeliveryDate";
                param[4].Value = preferredDeliveryDate;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@WebQID";
                param[5].Value = QORID.ToString();
                param[6] = new SqlParameter();
                param[6].ParameterName = "@WCID";
                param[6].Value = wcid;
                param[7] = new SqlParameter();
                param[7].ParameterName = "@LandingPageId";
                param[7].Value = landingPageId;
                param[8] = new SqlParameter();
                param[8].ParameterName = "@QuoteType";
                param[8].Value = quoteType;
                param[9] = new SqlParameter();
                param[9].ParameterName = "@TrackingPromoCode";
                param[9].Value = trackingPromoCode;
                param[10] = new SqlParameter();
                param[10].ParameterName = "@Email";
                param[10].Value = email;
                param[11] = new SqlParameter();
                param[11].ParameterName = "@LocationCode";
                param[11].Value = LocationCode;
                param[12] = new SqlParameter();
                param[12].ParameterName = "@TenantID";
                param[12].Value = TenantID;
                param[13] = new SqlParameter();
                param[13].ParameterName = "@UserBanflag";
                param[13].Value = UserBanflag;

                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_iprequestdata", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        /// <summary>
        /// This method will be called when the user is logged in as business user and trying to create quote.
        /// </summary>
        public int InsertIPrequestData(string requestIp, string zip1, string zip2, string size, DateTime preferredDeliveryDate, int QORID, string wcid, string landingPageId, string quoteType, string trackingPromoCode, string email, string LocationCode, int TenantID, bool UserBanflag, int BusDevID)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[15];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@strRequestIP";
                param[0].Value = requestIp;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@strFromZip";
                param[1].Value = zip1;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@strToZip";
                param[2].Value = zip2;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@strSize";
                param[3].Value = size;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@ProjectedDeliveryDate";
                param[4].Value = preferredDeliveryDate;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@WebQID";
                param[5].Value = QORID.ToString();
                param[6] = new SqlParameter();
                param[6].ParameterName = "@WCID";
                param[6].Value = wcid;
                param[7] = new SqlParameter();
                param[7].ParameterName = "@LandingPageId";
                param[7].Value = landingPageId;
                param[8] = new SqlParameter();
                param[8].ParameterName = "@QuoteType";
                param[8].Value = quoteType;
                param[9] = new SqlParameter();
                param[9].ParameterName = "@TrackingPromoCode";
                param[9].Value = trackingPromoCode;
                param[10] = new SqlParameter();
                param[10].ParameterName = "@Email";
                param[10].Value = email;
                param[11] = new SqlParameter();
                param[11].ParameterName = "@LocationCode";
                param[11].Value = LocationCode;
                param[12] = new SqlParameter();
                param[12].ParameterName = "@TenantID";
                param[12].Value = TenantID;
                param[13] = new SqlParameter();
                param[13].ParameterName = "@UserBanflag";
                param[13].Value = UserBanflag;
                param[14] = new SqlParameter();
                param[14].ParameterName = "@BusDevID";
                param[14].Value = BusDevID;

                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_iprequestdata", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public int InsertIPrequestData(string requestIp, string zip1, string zip2, string size, DateTime preferredDeliveryDate, int QORID, string wcid, string landingPageId, string quoteType, string trackingPromoCode, string email, string LocationCode, int TenantID, bool UserBanflag, int BusDevID, string ab_pagecode)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[16];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@strRequestIP";
                param[0].Value = requestIp;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@strFromZip";
                param[1].Value = zip1;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@strToZip";
                param[2].Value = zip2;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@strSize";
                param[3].Value = size;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@ProjectedDeliveryDate";
                param[4].Value = preferredDeliveryDate;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@WebQID";
                param[5].Value = QORID.ToString();
                param[6] = new SqlParameter();
                param[6].ParameterName = "@WCID";
                param[6].Value = wcid;
                param[7] = new SqlParameter();
                param[7].ParameterName = "@LandingPageId";
                param[7].Value = landingPageId;
                param[8] = new SqlParameter();
                param[8].ParameterName = "@QuoteType";
                param[8].Value = quoteType;
                param[9] = new SqlParameter();
                param[9].ParameterName = "@TrackingPromoCode";
                param[9].Value = trackingPromoCode;
                param[10] = new SqlParameter();
                param[10].ParameterName = "@Email";
                param[10].Value = email;
                param[11] = new SqlParameter();
                param[11].ParameterName = "@LocationCode";
                param[11].Value = LocationCode;
                param[12] = new SqlParameter();
                param[12].ParameterName = "@TenantID";
                param[12].Value = TenantID;
                param[13] = new SqlParameter();
                param[13].ParameterName = "@UserBanflag";
                param[13].Value = UserBanflag;
                param[14] = new SqlParameter();
                param[14].ParameterName = "@BusDevID";
                param[14].Value = BusDevID;
                param[15] = new SqlParameter();
                param[15].ParameterName = "@AB_PageCode";
                param[15].Value = ab_pagecode;

                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_iprequestdata", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        /// <summary>
        /// This method will be called only when the saved marketing response is not available in teh current active list of
        /// marketing items.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="marketingId"></param>
        /// <returns></returns>
        public DataSet GetSelectedMarketingItem(int siteId, int marketingId)
        {
            DataSet dsMarketing = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@Siteid";
                param[0].Value = siteId;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@MarketingId";
                param[1].Value = marketingId;
                dsMarketing = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetDeletedMarketingSources", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsMarketing;
        }

        public string GetSiteName(int siteId)
        {
            SqlConnection conn = Data.GetPRConnection();
            string retValue = String.Empty;
            try
            {
                string sql = "Select CompDBAName from RDFacility where SLSiteId= @Siteid";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@Siteid", Value = siteId };

                retValue = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param).ToString();
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public string GetWebSpecial(string locCode, int month)
        {
            SqlConnection conn = Data.GetPRConnection();
            string retValue = String.Empty;
            try
            {
                string sql = "Select Special from WebSpecialEligibility where LocCode= @locCode and StoragePeriodMonths= @month";

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@locCode", Value = locCode };
                param[1] = new SqlParameter { ParameterName = "@month", Value = month };

                retValue = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param).ToString();
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public string GetMarketingId(string Zipcode, string locCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            string retValue = String.Empty;
            try
            {

                string sql = "select marketid from vw_zipcodeowners where  siteid= (select SLSiteid from RDFacility where SLLocCode= @locCode and StatusRowID=70) and ZipCode= @Zipcode";

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@locCode", Value = locCode };
                param[1] = new SqlParameter { ParameterName = "@Zipcode", Value = Zipcode };

                retValue = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param).ToString();
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public DataSet GetVendorInfo(string firstName, string lastName, string password)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsVendor = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@FirstName";
                param[0].Value = firstName;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@LastName";
                param[1].Value = lastName;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@Password";
                param[2].Value = password;
                dsVendor = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_sel_vendor_info", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsVendor;
        }

        /// <summary>
        /// Gets teh data from t
        /// </summary>
        /// <param name="qorid"></param>
        /// <returns></returns>
        public DataSet GetActivityLog(int qorid)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsActivityLog = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                dsActivityLog = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetActivityLogData", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsActivityLog;
        }

        public DataSet GetGeocode(string zip)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsZipCode = new DataSet();
            try
            {
                string sql = "Select Latitude, longitude from USPSZipCodeList where ZipCode= @zip";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@zip", Value = zip };

                dsZipCode = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsZipCode;
        }

        public bool Is8FtAvailableForLocal(string locCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            //object ret = new object();
            DataSet ret = null;
            bool isAvail = false;
            try
            {
                string sql = "Select s8FtMoveType from RDFacility where SLLocCode= @locCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@locCode", Value = locCode };

                ret = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                //if (!ret.Equals(System.DBNull.Value))
                if (ret != null && ret.Tables.Count > 0 && ret.Tables[0].Rows.Count > 0 && ret.Tables[0].Rows[0]["s8FtMoveType"] != DBNull.Value)
                {
                    if ((ret.Tables[0].Rows[0]["s8FtMoveType"].ToString() == "Both" || (ret.Tables[0].Rows[0]["s8FtMoveType"].ToString() == "Local")))
                    {
                        isAvail = true;
                    }
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isAvail;
        }

        public bool Is8FtAvailableForLDM(string locCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            //object ret = new object();
            DataSet ret = null;
            bool isAvail = false;
            try
            {
                string sql = "Select s8FtMoveType from RDFacility where SLLocCode= @locCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@locCode", Value = locCode };

                ret = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                //if (!ret.Equals(System.DBNull.Value))
                if (ret != null && ret.Tables.Count > 0 && ret.Tables[0].Rows.Count > 0 && ret.Tables[0].Rows[0]["s8FtMoveType"] != DBNull.Value)
                {
                    if ((ret.Tables[0].Rows[0]["s8FtMoveType"].ToString() == "Both" || (ret.Tables[0].Rows[0]["s8FtMoveType"].ToString() == "LDM")))
                    {
                        isAvail = true;
                    }
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isAvail;
        }

        public bool IsDryVanAvailable(string locCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            //object ret = new object();
            DataSet ret = null;
            bool isAvail = false;
            try
            {
                string sql = "Select bDryVanAvail from RDFacility where SLLocCode= @locCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@locCode", Value = locCode };

                ret = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                //if (!ret.Equals(System.DBNull.Value))
                if (ret != null && ret.Tables.Count > 0 && ret.Tables[0].Rows.Count > 0 && ret.Tables[0].Rows[0]["bDryVanAvail"] != DBNull.Value)
                {
                    isAvail = Convert.ToBoolean(ret.Tables[0].Rows[0]["bDryVanAvail"]);
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isAvail;
        }

        public int GetScheduleBlockDaysValue(string locCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            //object ret = new object();
            DataSet ret = null;
            int scheduleBlockDays = 0;
            try
            {
                string sql = "Select ScheduleBlockDays from RDFacility where SLLocCode= @locCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@locCode", Value = locCode };

                ret = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                //if (!ret.Equals(System.DBNull.Value))
                if (ret != null && ret.Tables.Count > 0 && ret.Tables[0].Rows.Count > 0 && ret.Tables[0].Rows[0]["ScheduleBlockDays"] != DBNull.Value)
                {
                    scheduleBlockDays = Convert.ToInt32(ret.Tables[0].Rows[0]["ScheduleBlockDays"]);
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return scheduleBlockDays;
        }

        public bool IsFacilityClosed(string locCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsConfig = new DataSet();
            bool isClosed = false;
            try
            {
                string sql = "Select IsClosed from PrFacilityConfig where LocCode= @locCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@locCode", Value = locCode };

                dsConfig = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                if (dsConfig.Tables.Count > 0 && dsConfig.Tables[0].Rows.Count > 0 && dsConfig.Tables[0].Rows[0]["IsClosed"] != DBNull.Value)
                {
                    isClosed = Convert.ToBoolean(dsConfig.Tables[0].Rows[0]["IsClosed"]);
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isClosed;
        }

        public int InsertCWPricingDetails(int QORID, string unitName, string chargeDescription, decimal standardPrice, decimal discount, decimal tax, decimal totalPrice)
        {
            SqlConnection conn = Data.GetPRConnection();
            int Result = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[7];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = QORID;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@UnitName";
                param[1].Value = unitName;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@ChargeDescription";
                param[2].Value = chargeDescription;

                param[3] = new SqlParameter();
                param[3].ParameterName = "@StandardPrice";
                param[3].Value = standardPrice;

                param[4] = new SqlParameter();
                param[4].ParameterName = "@Discount";
                param[4].Value = discount;

                param[5] = new SqlParameter();
                param[5].ParameterName = "@Tax";
                param[5].Value = tax;

                param[6] = new SqlParameter();
                param[6].ParameterName = "@TotalPrice";
                param[6].Value = totalPrice;

                Result = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRConsumerWebQuotePricing", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return Result;
        }

        public void DeleteCWPricingDetail(int QORID)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                string sql = "Delete from PRConsumerWebQuotePricing where QORID = @QORID";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@QORID", Value = QORID.ToString() };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public bool IPExceededTries(string ip, int requestTimeInterval, int requestPerTimeLimit)
        {
            SqlConnection conn = Data.GetPRConnection();

            string sql = "";
            bool retVal = false;
            int hits = 0;

            try
            {
                sql += "SELECT count(*) FROM IPRequestData ";
                sql += " where dtDateTime between dateadd(minute, @requestTimeInterval, getdate()) and getdate() ";
                sql += " and strRequestIP = @ip";

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@requestTimeInterval", Value = -requestTimeInterval };
                param[1] = new SqlParameter { ParameterName = "@ip", Value = ip };

                hits = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            if (hits >= requestPerTimeLimit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static int IPCountRequests(string ip)
        {
            SqlConnection conn = Data.GetPRConnection();

            string sql = "";
            int retVal = 0;

            DateTime d = DateTime.Now;
            string dtS = d.Year + "-" + d.Month + "-" + d.Day;

            sql += "select count(*) from IPRequestData where strRequestIP = @ip";

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter { ParameterName = "@ip", Value = ip };


            SqlDataReader dr = SqlHelper.ExecuteReader(conn, CommandType.Text, sql, param);

            dr.Read();

            if (dr.HasRows)
            {
                retVal = (int)dr[0];
            }
            else
            {
                retVal = 0;
            }

            dr.Close();

            return retVal;
        }

        public DataSet GetSTARSPromo()
        {
            SqlConnection conn = Data.GetSTARSConnection();
            DataSet dsPromo = new DataSet();
            try
            {
                string sql = "Select T62_PromoSlabID,T62_PromocodeName,T62_IsActive from T62_TblPromocodeSlab where T62_IsActive=0";
                dsPromo = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsPromo;
        }

        public string[] GetPromoCodesFromSTARS(string promoComboCode)
        {
            SqlConnection conn = Data.GetSTARSConnection();
            DataSet dsPromo = new DataSet();
            string[] promos = new string[2] { String.Empty, String.Empty };
            try
            {
                string sql = "Select T149_PromoCombo_Code,T149_T62_PromocodeName,T149_T130_PromoEvent from T149_promocombo where T149_Active=1 and T149_PromoCombo_Code= @promoComboCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@promoComboCode", Value = promoComboCode };

                dsPromo = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                if ((dsPromo.Tables.Count > 0) && (dsPromo.Tables[0].Rows.Count > 0))
                {
                    promos[0] = dsPromo.Tables[0].Rows[0]["T149_T62_PromocodeName"].ToString();
                    promos[1] = dsPromo.Tables[0].Rows[0]["T149_T130_PromoEvent"].ToString();
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return promos;
        }

        public DataSet GetConcessionPlan(int promoglobalnum, string chgDescription, string locationCode)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string connString = String.Empty;
                DataSet dsConcessionPlan = new DataSet();
                int siteId = 0;
                connString = GetSLDBConnStringByLocCode(locationCode);
                if (connString != String.Empty)
                {
                    siteId = GetSiteID(locationCode);
                    conn = Data.GetConnection(connString);
                    //                string sql = string.Format(@"select cp.iConcessionGlobalNum, cp.sPlanName, cp.bNeverExpires, cp.iExpirMonths, cp.dcChgAmt, cp.dcFixedDiscount, cp.dcPCDiscount 
                    //from concessionplans cp 
                    //inner join promoofferitems poi on cp.iConcessionGlobalNum = poi.iConcessionGlobalNum 
                    //inner join promotions p on p.promoofferid = poi.promoofferid 
                    //inner join chargedesc cd on cp.chargedescid = cd.chargedescid 
                    //where cd.schgdesc='{0}' and p.ipromoglobalnum='{1}' and cp.siteid={2}", chgDescription, promoglobalnum, siteId);
                    string sql = @"select cp.iConcessionGlobalNum, cp.sPlanName, cp.bNeverExpires, cp.iExpirMonths, cp.dcChgAmt, cp.dcFixedDiscount, cp.dcPCDiscount 
from concessionplans cp 
inner join promoofferitems poi on cp.iConcessionGlobalNum = poi.iConcessionGlobalNum 
inner join promotions p on p.promoofferid = poi.promoofferid 
inner join chargedesc cd on cp.chargedescid = cd.chargedescid 
where cd.schgdesc=@chgDescription and p.ipromoglobalnum=@promoglobalnum and cp.siteid=@siteId";

                    SqlParameter[] param = new SqlParameter[3];
                    param[0] = new SqlParameter { ParameterName = "@chgDescription", Value = chgDescription };
                    param[1] = new SqlParameter { ParameterName = "@promoglobalnum", Value = promoglobalnum };
                    param[2] = new SqlParameter { ParameterName = "@siteId", Value = siteId };

                    dsConcessionPlan = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                }
                return dsConcessionPlan;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetConcessionPlan(int promoglobalnum, string locationCode)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string connString = String.Empty;
                DataSet dsConcessionPlan = new DataSet();
                int siteId = 0;
                connString = GetSLDBConnStringByLocCode(locationCode);
                if (connString != String.Empty)
                {
                    siteId = GetSiteID(locationCode);
                    conn = Data.GetConnection(connString);
                    //string sql = string.Format("select cp.iConcessionGlobalNum, cp.sPlanName, cp.bNeverExpires, cp.iExpirMonths, cp.dcChgAmt, cp.dcFixedDiscount, cp.dcPCDiscount, p.sPromoDescription, cd.schgdesc from concessionplans cp inner join promoofferitems poi on cp.iConcessionGlobalNum = poi.iConcessionGlobalNum inner join promotions p on p.promoofferid = poi.promoofferid inner join chargedesc cd on cp.chargedescid = cd.chargedescid where and p.ipromoglobalnum='{0}' and cp.siteid={1}", promoglobalnum, siteId);
                    string sql = @"select cp.iConcessionGlobalNum, cp.sPlanName, cp.bNeverExpires, cp.iExpirMonths, cp.dcChgAmt, cp.dcFixedDiscount, cp.dcPCDiscount, p.sPromoDescription, cd.schgdesc 
from concessionplans cp 
inner join promoofferitems poi on cp.iConcessionGlobalNum = poi.iConcessionGlobalNum 
inner join promotions p on p.promoofferid = poi.promoofferid 
inner join chargedesc cd on cp.chargedescid = cd.chargedescid 
where and p.ipromoglobalnum=@promoglobalnum and cp.siteid=@siteId";

                    SqlParameter[] param = new SqlParameter[2];
                    param[0] = new SqlParameter { ParameterName = "@promoglobalnum", Value = promoglobalnum };
                    param[1] = new SqlParameter { ParameterName = "@siteId", Value = siteId };

                    dsConcessionPlan = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                }
                return dsConcessionPlan;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public int GetPromoGlobalNum(string promoCode, string locationCode)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string connString = String.Empty;
                int promoGlobalNum = 0;
                connString = GetSLDBConnStringByLocCode(locationCode);
                DataSet dsPromo = new DataSet();

                if (connString != String.Empty)
                {
                    conn = Data.GetConnection(connString);
                    string sql = "select iPromoGlobalNum from Promotions where sPromotionCode=@promoCode and dDeleted is null";

                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter { ParameterName = "@promoCode", Value = promoCode };

                    dsPromo = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                    if (dsPromo.Tables.Count > 0 && dsPromo.Tables[0].Rows.Count > 0)
                    {
                        promoGlobalNum = Convert.ToInt32(dsPromo.Tables[0].Rows[0]["iPromoGlobalNum"]);
                    }
                }
                return promoGlobalNum;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public string GetPromoCode(int promoGlobalNum, string locationCode)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string connString = String.Empty;
                string promoCode = String.Empty;
                connString = GetSLDBConnStringByLocCode(locationCode);
                DataSet dsPromo = new DataSet();

                if (connString != String.Empty)
                {
                    conn = Data.GetConnection(connString);
                    string sql = "Select sPromotionCode from Promotions where iPromoGlobalNum= @promoGlobalNum";

                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter { ParameterName = "@promoGlobalNum", Value = promoGlobalNum };

                    dsPromo = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);

                    if (dsPromo.Tables.Count > 0 && dsPromo.Tables[0].Rows.Count > 0)
                    {
                        promoCode = dsPromo.Tables[0].Rows[0]["sPromotionCode"].ToString();
                    }
                }
                return promoCode;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public bool IsPromoEligible(int promoGlobalNum, string locationCode)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string connString = String.Empty;
                int siteid = GetSiteID(locationCode);
                bool isEligible = false;
                connString = GetSLDBConnStringByLocCode(locationCode);
                DataSet ds = new DataSet();

                if (connString != String.Empty)
                {
                    conn = Data.GetConnection(connString);
                    //string sql = String.Format("select PromotionId, bForAllSites from Promotions where iPromoGlobalNum={0} and dDeleted is null and dDisabled is null and dPromoStrt<getdate() and dPromoEnd>getdate()", promoGlobalNum);
                    string sql = @"select PromotionId, bForAllSites 
from Promotions 
where iPromoGlobalNum = @promoGlobalNum and dDeleted is null and dDisabled is null and dPromoStrt < getdate() and dPromoEnd > getdate()";

                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter { ParameterName = "@promoGlobalNum", Value = promoGlobalNum };

                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        isEligible = Convert.ToBoolean(ds.Tables[0].Rows[0]["bForAllSites"]);
                        if (!isEligible)
                        {
                            //sql = String.Format("select * from PromotionEligibility where promotionid={0} and siteid={1} and dDeleted is null", Convert.ToInt32(ds.Tables[0].Rows[0]["PromotionId"]), siteid);
                            sql = @"select * from PromotionEligibility 
where promotionid=@promotionid and siteid=@siteid and dDeleted is null";

                            param = new SqlParameter[2];
                            param[0] = new SqlParameter { ParameterName = "@promotionid", Value = Convert.ToInt32(ds.Tables[0].Rows[0]["PromotionId"]) };
                            param[1] = new SqlParameter { ParameterName = "@siteid", Value = siteid };

                            ds = new DataSet();
                            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                isEligible = Convert.ToBoolean(ds.Tables[0].Rows[0]["bSiteIsEligible"]);
                            }
                        }
                    }
                }
                return isEligible;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public int UpdatePromotionDataInQuoteOrderLog(int qorid, int promoGlobalNum)
        {
            int updateCount = 0;
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@PromoGlobalNum";
                param[1].Value = promoGlobalNum;
                updateCount = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_UpdatePRGQuoteOrderLog_Promotion", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return updateCount;
        }

        public int RemovePromotionData(int qorid)
        {
            SqlConnection conn = Data.GetPRConnection();
            int updateCount = 0;
            try
            {
                string sql = "update PRGQuoteOrderLog set PromoGlobalNum=null where qorid= @qorid";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                updateCount = SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return updateCount;
        }

        public int UpdateRevenueDataInQuoteOrderLog(int qorid, decimal dueAtDelivery, decimal futureTransportationCharge, decimal recurringCharge)
        {
            SqlConnection conn = Data.GetPRConnection();
            int updateCount = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@DueOnDelivery";
                param[1].Value = dueAtDelivery;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@FutureTransportationCharge";
                param[2].Value = futureTransportationCharge;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@RecurringMonthlyCharge";
                param[3].Value = recurringCharge;
                updateCount = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_Update_PRGQuoteorderLog_Price", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return updateCount;
        }

        public int UpdateContactInfoAvauilabilityInQuoteOrderLog(int qorid, bool isEMailAvailable, bool isPhoneNumberAvailable)
        {
            SqlConnection conn = Data.GetPRConnection();
            int updateCount = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@EmailAvailableForQOR";
                param[1].Value = isEMailAvailable;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@PhoneNumAvailableForQOR";
                param[2].Value = isPhoneNumberAvailable;
                updateCount = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_update_PRGQuoteorderLog_ContactInfoAvailability", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return updateCount;
        }

        public int GetQORAge(int qorid)
        {
            SqlConnection conn = Data.GetPRConnection();
            object priceUpdateddateTime = null;
            int age = 0;
            try
            {
                string sql = "select LastPriceUpdateDateTime from prgquoteorderlog where qorid= @qorid";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                priceUpdateddateTime = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param);
                if (priceUpdateddateTime != null && priceUpdateddateTime != DBNull.Value)
                {
                    age = (DateTime.Today - Convert.ToDateTime(priceUpdateddateTime)).Days;
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return age;
        }

        public int GetQORSoldAge(int qorid)
        {
            SqlConnection conn = Data.GetPRConnection();
            object priceUpdateddateTime = null;
            int age = -1;
            try
            {
                string sql = "select * from prgquoteorderlog where qorid= @qorid";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                DataSet dsprgquoteorderlog = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);

                if (dsprgquoteorderlog.Tables.Count > 0 && dsprgquoteorderlog.Tables[0].Rows.Count > 0)
                {
                    if (dsprgquoteorderlog.Tables[0].Rows[0]["ConvertedToOrderDate"] == null || dsprgquoteorderlog.Tables[0].Rows[0]["ConvertedToOrderDate"] == DBNull.Value)
                    {
                        age = 0;
                    }
                    else
                    {
                        age = (DateTime.Today - Convert.ToDateTime(dsprgquoteorderlog.Tables[0].Rows[0]["ConvertedToOrderDate"]).Date).Days;
                    }
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return age;
        }

        public DataSet GetCPPValues()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsCPP = new DataSet();
            try
            {
                string sql = "select CPPDescription,Price from PRGCPPInfo";
                dsCPP = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsCPP;
        }

        public DataSet GetQORInfo(int qorid)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsQORInfo = new DataSet();
            try
            {
                string sql = "select MoveType,DesiredNumberOfMonth,Zip1, Zip2 from PRGQuoteOrderLog where QORID = @qorid";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                dsQORInfo = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsQORInfo;
        }

        public DataSet GetCATSScriptCheckListData(int qorid)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsCheckList = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                dsCheckList = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_sel_CATSScriptTracking", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsCheckList;
        }

        public int UpdateCATSCheckListData(int qorid, int screenId, string userId)
        {
            SqlConnection conn = Data.GetPRConnection();
            int ret = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@ScreenId";
                param[1].Value = screenId;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@UserId";
                param[2].Value = userId;
                ret = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_CATSScriptTracking", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ret;
        }

        public int UpdateVendorCodeInQuoteLog(int qorid, string vendorCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retVal = 0;
            try
            {
                string sql = "Update PRGQuoteOrderLog Set VendorCode = @VendorCode where QORID = @QORID";

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@VendorCode", Value = vendorCode };
                param[1] = new SqlParameter { ParameterName = "@QORID", Value = qorid };

                retVal = SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retVal;
        }

        public int UpdateSourceInQUoteLog(int qorid, string source)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retVal = 0;
            try
            {
                string sql = "Update PRGQuoteOrderLog Set Source = @Source where QORID = @QORID";

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@Source", Value = source };
                param[1] = new SqlParameter { ParameterName = "@QORID", Value = qorid };

                retVal = SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retVal;
        }

        public int UpdateStarsIdInQuoteLog(int qorid, int starsId)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retVal = 0;
            try
            {
                string sql = "Update PRGQuoteOrderLog Set StartsId=@starsId where QORID=@qorid";

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@starsId", Value = starsId };
                param[1] = new SqlParameter { ParameterName = "@QORID", Value = qorid };

                retVal = SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retVal;
        }

        public DataSet GetVendorInfo(string vendorCode)
        {
            SqlConnection conn = Data.GetSTARSConnection();
            DataSet dsVendorInfo = new DataSet();
            try
            {
                string sql = "SELECT T88_CustNo AS VendorCode, T88_Company AS VendorName, T88_UserName AS VendorID, T88_Password AS VendorPassword FROM STARSDB..T88_tblVendor WHERE T88_CustNo = @vendorCode";

                SqlParameter[] arrParam = { new SqlParameter("@vendorCode", SqlDbType.NVarChar) };
                arrParam[0].Value = vendorCode;

                dsVendorInfo = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, arrParam);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsVendorInfo;
        }

        public DataSet GetVendorInfoWithTrackingCode(string vendorCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsVendorInfo = new DataSet();

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@VendorCode";
                param[0].Value = vendorCode;

                dsVendorInfo = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_Get_VendorInfo", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsVendorInfo;
        }

        public DataSet GetActivePackRatFacilities()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsFacilities = new DataSet();

            try
            {
                dsFacilities = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select SLSiteId from RDFacility where statusrowid=70 and storetype>0");
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsFacilities;
        }

        public DataSet GetActivePackRatFacilitiesInfo()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsFacilities = new DataSet();

            try
            {
                dsFacilities = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select * from RDFacility where statusrowid=70 and storetype>0");
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsFacilities;
        }

        public DataSet GetOldUnitPrice(int qorid, string unitName)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsUnits = new DataSet();

            try
            {
                string query = @"SELECT QORID, UnitName, ChargeDescription, StandardPrice, Discount, Tax, TotalPrice, LastUpdated 
FROM PRConsumerWebQuotePricing 
WHERE QORID = @QORID AND ChargeDescription = @ChargeDescription";

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@QORID", Value = qorid };
                param[1] = new SqlParameter { ParameterName = "@ChargeDescription", Value = unitName };

                //dsUnits = SqlHelper.ExecuteDataset(conn, CommandType.Text, String.Format("SELECT QORID, UnitName, ChargeDescription, StandardPrice, Discount, Tax, TotalPrice, LastUpdated FROM PRConsumerWebQuotePricing WHERE QORID = {0} AND ChargeDescription = '{1}'", qorid, unitName));
                dsUnits = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsUnits;
        }

        public DataSet GetConsumerWebQuoteInfo(int qorid)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsQuoteInfo = new DataSet();

            try
            {
                //dsQuoteInfo = SqlHelper.ExecuteDataset(conn, CommandType.Text, String.Format("SELECT RowId, QORID, Zip1, Zip2, MoveType, QuoteCreatedBy, QuoteCreatedDate, ConvertedToOrderBy, ConvertedToOrderDate, CancelledBy, CancellationDate, DesiredNumberOfMonth, DueOnDelivery, FutureTransportationCharge, RecurringMonthlyCharge, PromoGlobalNumAppliedFirst, PromoGlobalNum, LastPriceUpdateDateTime, EmailAvailableForQOR, PhoneNumAvailableForQOR, VendorCode, Source, StarsId FROM  PRGQuoteOrderLog WHERE QORID = {0}", qorid));
                string query = @"SELECT RowId, QORID, Zip1, Zip2, MoveType, QuoteCreatedBy, QuoteCreatedDate, ConvertedToOrderBy, ConvertedToOrderDate, CancelledBy, CancellationDate, DesiredNumberOfMonth, DueOnDelivery, FutureTransportationCharge, RecurringMonthlyCharge, PromoGlobalNumAppliedFirst, PromoGlobalNum, LastPriceUpdateDateTime, EmailAvailableForQOR, PhoneNumAvailableForQOR, VendorCode, Source, StarsId 
FROM  PRGQuoteOrderLog WHERE QORID = @QORID";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@QORID", Value = qorid };

                dsQuoteInfo = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsQuoteInfo;
        }

        public bool IsFuelSurchargeApplicable(string locCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            bool isApplicable = false;
            try
            {
                string query = @"Select bFuelCharge from RDFacility where SLLocCode = @locCode";
                //object obj = SqlHelper.ExecuteScalar(conn, CommandType.Text, String.Format("Select bFuelCharge from RDFacility where SLLocCode='{0}'", locCode));

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@locCode", Value = locCode };

                object obj = SqlHelper.ExecuteScalar(conn, CommandType.Text, query, param);
                if (obj != System.DBNull.Value)
                {
                    isApplicable = Convert.ToBoolean(obj);
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isApplicable;
        }

        public bool UpdatePaymentAgreement(int qorid, bool isPayAgee)
        {
            SqlConnection conn = Data.GetPRConnection();
            bool isSuccess = false;
            try
            {
                //SqlHelper.ExecuteNonQuery(conn, CommandType.Text, String.Format("Update PRGQuoteOrderLog set PayAgreeFlag={0},PayAgreeDate='{1}' where qorid={2}", Convert.ToInt16(isPayAgee), DateTime.Now, qorid));

                string query = @"Update PRGQuoteOrderLog set PayAgreeFlag=@PayAgreeFlag,PayAgreeDate=@PayAgreeDate where qorid=@qorid";

                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter { ParameterName = "@PayAgreeFlag", Value = Convert.ToInt16(isPayAgee) };
                param[1] = new SqlParameter { ParameterName = "@PayAgreeDate", Value = DateTime.Now };
                param[2] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, query, param);

                isSuccess = true;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isSuccess;
        }

        public bool UpdateCPPAgreemanet(int qorid, bool isCPPBasic, bool isCPPSilver, bool isCPPNone)
        {
            SqlConnection conn = Data.GetPRConnection();
            bool isSuccess = false;
            try
            {
                //SqlHelper.ExecuteNonQuery(conn, CommandType.Text, String.Format("Update PRGQuoteOrderLog set CPPBasFlag={0}, CPPSilFlag={1}, CPPNoneFlag={2} where qorid={3}", Convert.ToInt16(isCPPBasic), Convert.ToInt16(isCPPSilver), Convert.ToInt16(isCPPNone), qorid));

                string query = @"Update PRGQuoteOrderLog 
set CPPBasFlag = @CPPBasFlag, CPPSilFlag = @CPPSilFlag, CPPNoneFlag = @CPPNoneFlag 
where qorid = @qorid";

                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter { ParameterName = "@CPPBasFlag", Value = Convert.ToInt16(isCPPBasic) };
                param[1] = new SqlParameter { ParameterName = "@CPPSilFlag", Value = Convert.ToInt16(isCPPSilver) };
                param[2] = new SqlParameter { ParameterName = "@CPPNoneFlag", Value = Convert.ToInt16(isCPPNone) };
                param[3] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, query, param);
                isSuccess = true;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isSuccess;
        }

        public bool UpdateRentalAgreement(int qorid, bool isReantalAgrrement, bool isLienAgreement, bool isCAAgreement, string ipadress)
        {
            SqlConnection conn = Data.GetPRConnection();
            bool isSuccess = false;
            try
            {
                //SqlHelper.ExecuteNonQuery(conn, CommandType.Text, String.Format("Update PRGQuoteOrderLog set RentAgreeFlag={0}, LienAgreeFlag={1}, CAAgreeFlag={2}, IPAdress='{3}', RentAgreedate='{4}' where qorid={5}", Convert.ToInt16(isReantalAgrrement), Convert.ToInt16(isLienAgreement), Convert.ToInt16(isCAAgreement), ipadress, DateTime.Now, qorid));
                string query = @"Update PRGQuoteOrderLog 
set RentAgreeFlag = @RentAgreeFlag, LienAgreeFlag = @LienAgreeFlag, CAAgreeFlag = @CAAgreeFlag, IPAdress = @IPAdress, RentAgreedate = @RentAgreedate
where qorid = @qorid";

                SqlParameter[] param = new SqlParameter[6];
                param[0] = new SqlParameter { ParameterName = "@RentAgreeFlag", Value = Convert.ToInt16(isReantalAgrrement) };
                param[1] = new SqlParameter { ParameterName = "@LienAgreeFlag", Value = Convert.ToInt16(isLienAgreement) };
                param[2] = new SqlParameter { ParameterName = "@CAAgreeFlag", Value = Convert.ToInt16(isCAAgreement) };
                param[3] = new SqlParameter { ParameterName = "@IPAdress", Value = ipadress };
                param[4] = new SqlParameter { ParameterName = "@RentAgreedate", Value = DateTime.Now };
                param[5] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, query, param);
                isSuccess = true;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isSuccess;
        }

        public bool InsertPRGOrderContractLog(int qorid, string ipaddress, bool PayAgreeFlag, bool CPPBasFlag, bool CPPSilFlag, bool CPPNoneFlag, bool RentAgreeFlag, bool LienAgreeFlag, bool CAAgreeFlag, bool ActMilitarySrvcFlag)
        {
            SqlConnection conn = Data.GetPRConnection();
            bool isSuccess = false;
            try
            {
                SqlParameter[] param = new SqlParameter[11];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@CreatedDate";
                param[1].Value = DateTime.Now;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@IPAdress";
                param[2].Value = ipaddress;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@PayAgreeFlag";
                param[3].Value = PayAgreeFlag;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@CPPBasFalg";
                param[4].Value = CPPBasFlag;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@CPPSilFlag";
                param[5].Value = CPPSilFlag;
                param[6] = new SqlParameter();
                param[6].ParameterName = "@CPPNoneFlag";
                param[6].Value = CPPNoneFlag;
                param[7] = new SqlParameter();
                param[7].ParameterName = "@RentAgreeFlag";
                param[7].Value = RentAgreeFlag;
                param[8] = new SqlParameter();
                param[8].ParameterName = "@LienAgreeFlag";
                param[8].Value = LienAgreeFlag;
                param[9] = new SqlParameter();
                param[9].ParameterName = "@CAAgreeFlag";
                param[9].Value = CAAgreeFlag;
                param[10] = new SqlParameter();
                param[10].ParameterName = "@ActMilitarySrvcFlag";
                param[10].Value = ActMilitarySrvcFlag;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRGOrderContractLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isSuccess;
        }

        public DataSet GetRDFacilityData(string locCode)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility where slloccode='" + locCode + "'");

                string query = @"select * from RDfacility where slloccode = @slloccode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@slloccode", Value = locCode };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetRDFacilityDataByStoreNumber(string storeNumber)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility where StoreNo ='" + storeNumber + "'");
                string query = @"select * from RDfacility where statusrowid>=65 and storetype > 0 and  StoreNo = @StoreNo";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@StoreNo", Value = storeNumber };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetRDFacilityData()
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility");
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetRDFacilityData(int siteId)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility where slsiteId=" + siteId);
                string query = "select * from RDfacility where slsiteId = @slsiteId";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@slsiteId", Value = siteId };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetRDFacilityDataByStoreNum(int StoreNum)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility where storeNo=" + StoreNum);
                string query = "select * from RDfacility where storeNo = @storeNo";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@storeNo", Value = StoreNum };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetFAGroupInfo(string roleGroupName)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //string query = string.Format("select * from [dbo].[FAGroupMaster] where Groupname = '{0}'", roleGroupName);
                string query = "select * from [dbo].[FAGroupMaster] where Groupname = @Groupname";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@Groupname", Value = roleGroupName };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetRDFacilitiesByRoleGroup(int roleGroupId)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //string query = string.Format("select RDFacility.* from RDFacility join FAGroupAndFacilityAccess on FK_StoreRowID = StoreRowID and FK_GroupID = {0}", roleGroupId);
                string query = @"select RDFacility.* from RDFacility 
join FAGroupAndFacilityAccess on FK_StoreRowID = StoreRowID and FK_GroupID = @roleGroupId";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@roleGroupId", Value = roleGroupId };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetRDFacilityDataByLocationCode(string SLLocCode)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility where SLLocCode='" + SLLocCode + "'");

                string query = "select * from RDfacility where SLLocCode = @SLLocCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@SLLocCode", Value = SLLocCode };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetRDFacilitiesDataByMarket(int marketId)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility where fk_marketId=" + marketId);
                string query = "select * from RDfacility where fk_marketId= @marketId";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@marketId", Value = marketId };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetPRGQuoteOrderLogData(int qorid)
        {
            DataSet dsQuoteorderLog = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsQuoteorderLog = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from prgordercontractlog where QORID=" + qorid);
                string query = "select * from prgordercontractlog where QORID = @qorid";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                dsQuoteorderLog = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsQuoteorderLog;
        }

        public DataSet GetPRGOrderContractLogData(int qorid)
        {
            DataSet dsOrderContractLog = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsOrderContractLog = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from PRGQuoteorderLog where QORID=" + qorid);
                string query = "select * from PRGQuoteorderLog where QORID=@qorid";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                dsOrderContractLog = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsOrderContractLog;
        }

        public DataSet GetFuturePrices(string locationCode, string zipCode, DateTime estimatedDeliveryDate)
        {
            DataSet dsFutureprices = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@LocationCode";
                param[0].Value = locationCode;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@Zip";
                param[1].Value = zipCode;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@EstimatedDeliveryDate";
                param[2].Value = estimatedDeliveryDate;

                dsFutureprices = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetFuturePrices", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsFutureprices;
        }

        public DataSet GetTenantInfoByPhoneNumber(string phoneNumber)
        {
            DataSet dsTenant = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@PhoneNumber";
                param[0].Value = phoneNumber;

                dsTenant = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetTenantInfoByPhoneNumber", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsTenant;
        }

        public DataSet GetTripDetails(int T26ContainerId)
        {
            DataSet dsTenant = new DataSet();
            SqlConnection conn = Data.GetSTARSConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@ContainerId";
                param[0].Value = T26ContainerId;

                dsTenant = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_FAWT_GetTripDetails", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsTenant;
        }

        public DataSet GetLoadDriverDetails(int QorId, string TouchType, int SLSeqNumber)
        {
            DataSet dsTenant = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QorId";
                param[0].Value = QorId;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@TouchType";
                param[1].Value = TouchType;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@SLSeqNumber";
                param[2].Value = SLSeqNumber;

                dsTenant = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_FAWT_GetDriverDetailsForQuote", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsTenant;
        }

        public void UpdatePreferredDeliveryDate(DateTime preferredDeliveryDate, int qorid)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //string sql = String.Format("update prgquoteorderlog set PreferredDeliveryDate='{0}',LastPriceUpdateDateTime='{1}' where qorid={2}", preferredDeliveryDate, DateTime.Now, qorid);
                string sql = @"
update prgquoteorderlog 
set PreferredDeliveryDate = @PreferredDeliveryDate, 
LastPriceUpdateDateTime = @LastPriceUpdateDateTime 
where qorid = @qorid";

                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter { ParameterName = "@PreferredDeliveryDate", Value = preferredDeliveryDate };
                param[1] = new SqlParameter { ParameterName = "@LastPriceUpdateDateTime", Value = DateTime.Now };
                param[2] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public int InsertSLAuditLog(string methodName, string applicationName, string userName, DateTime startDateTime)
        {
            int auditLogId = 0;
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@MethodName";
                param[0].Value = methodName;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@ApplicationName";
                param[1].Value = applicationName;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@UserName";
                param[2].Value = userName;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@ServerName";
                param[3].Value = Environment.MachineName;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@StartTime";
                param[4].Value = startDateTime;

                auditLogId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_ins_SLCallAuditLog", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return auditLogId;
        }

        public int InsertSLAuditLog(string methodName, string applicationName, string userName, int qorid, DateTime startDateTime)
        {
            int auditLogId = 0;
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlParameter[] param = new SqlParameter[6];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@MethodName";
                param[0].Value = methodName;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@ApplicationName";
                param[1].Value = applicationName;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@UserName";
                param[2].Value = userName;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@ServerName";
                param[3].Value = Environment.MachineName;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@QORID";
                param[4].Value = qorid;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@StartTime";
                param[5].Value = startDateTime;

                auditLogId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_ins_SLCallAuditLog", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return auditLogId;
        }

        public void InsertJaguarContainerLog(string unitName, string Originlocode, string destloccode, DateTime CreatedDateTime)
        {
            int auditLogId = 0;
            SqlConnection conn = Data.GetSTARSConnection();

            try
            {
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@unitname";
                param[0].Value = unitName;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@OrgLoc";
                param[1].Value = Originlocode;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@destloc";
                param[2].Value = destloccode;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@CreatedTime";
                param[3].Value = CreatedDateTime;

                auditLogId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_ins_JaguarContainerLog", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void InsertTransitErrorLog(string methodName, string Error, string userName, int qorid, DateTime CreatedDateTime)
        {
            int auditLogId = 0;
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@MethodName";
                param[0].Value = methodName;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@ErrorDesc";
                param[1].Value = Error;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@UserName";
                param[2].Value = userName;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@QORID";
                param[3].Value = qorid;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@CreatedTime";
                param[4].Value = CreatedDateTime;

                auditLogId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_ins_TransiteErrorLog", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void UpdateSLAuditLog(int auditLogId, DateTime endTime)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@AuditLogId";
                param[0].Value = auditLogId;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@EndTime";
                param[1].Value = endTime;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_upd_SLCallAuditLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void UpdateSLAuditLog(int auditLogId, int QORID, DateTime endTime)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //string sql = String.Format("update SLCallAuditLog set EndTime='{0}', QORID={1} where AuditLogId={2}", endTime, QORID, auditLogId);
                string sql = "update SLCallAuditLog set EndTime=@endTime, QORID=@QORID where AuditLogId=@auditLogId";

                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter { ParameterName = "@endTime", Value = endTime };
                param[1] = new SqlParameter { ParameterName = "@QORID", Value = QORID };
                param[2] = new SqlParameter { ParameterName = "@auditLogId", Value = auditLogId };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetFuturePriceForSite(string locationCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsPrice = new DataSet();
            try
            {
                //string sql = String.Format("select * from slfutureprices where LocationCode = '{0}' and isActive=1 and isDeleted=0 order by ChargeDesc, StartDate", locationCode);
                string sql = @"select * from slfutureprices where LocationCode = @locationCode and isActive=1 and isDeleted=0 order by ChargeDesc, StartDate";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@locationCode", Value = locationCode };

                dsPrice = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsPrice;
        }

        public DataSet GetSiteLinkUnitPrice(string locationCode, string zipCode)
        {
            DataSet dsRDFacility = GetRDFacilityData(locationCode);
            DataSet dsUnitPrice = new DataSet();
            if (dsRDFacility != null && dsRDFacility.Tables.Count > 0 && dsRDFacility.Tables[0].Rows.Count > 0)
            {
                string dbName = dsRDFacility.Tables[0].Rows[0]["SLDBName"].ToString();
                int siteId = Convert.ToInt32(dsRDFacility.Tables[0].Rows[0]["SLSiteId"]);

                string connString = Data._connStringSTARS;
                connString = connString.Replace("StarsDb", dbName);
                SqlConnection conn = Data.GetConnection(connString);

                //Get the unit price for 16, 12 and 8 ft units from Units table
                //string sql = String.Format("select SiteId, sUnitName, dcStdRate from units where siteid={0} and sunitname in ('000016', '000012', '000008') and dDeleted is null", siteId);
                string sql = "select SiteId, sUnitName, dcStdRate from units where siteid = @siteId and sunitname in ('000016', '000012', '000008') and dDeleted is null";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@siteId", Value = siteId };

                dsUnitPrice = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);

                try
                {
                    //sql = String.Format("select * from zipcodeowners where szipcode='{0}' and siteid={1} and dDeleted is null", zipCode, siteId);
                    sql = "select * from zipcodeowners where szipcode = @zipCode and siteid = @siteId and dDeleted is null";

                    param = new SqlParameter[2];
                    param[0] = new SqlParameter { ParameterName = "@zipCode", Value = zipCode };
                    param[1] = new SqlParameter { ParameterName = "@siteId", Value = siteId };

                    DataSet dsZipCode = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                    if (dsZipCode != null && dsZipCode.Tables.Count > 0 && dsZipCode.Tables[0].Rows.Count > 0)
                    {
                        if (dsZipCode.Tables[0].Rows[0]["ZipCodeZoneId"] != null && dsZipCode.Tables[0].Rows[0]["ZipCodeZoneId"] != DBNull.Value)
                        {
                            int zipCodeZoneId = Convert.ToInt32(dsZipCode.Tables[0].Rows[0]["ZipCodeZoneId"]);
                            //sql = String.Format("select zcp.* from zipcodeprices zcp inner join chargedesc cd on zcp.chargedescid=cd.chargedescid where cd.schgdesc='Rent' and dcChgAmt <>0 and zcp.ddeleted is null and zipcodezoneid={0}", zipCodeZoneId);
                            sql = "select zcp.* from zipcodeprices zcp inner join chargedesc cd on zcp.chargedescid=cd.chargedescid where cd.schgdesc='Rent' and dcChgAmt <>0 and zcp.ddeleted is null and zipcodezoneid = @zipCodeZoneId";

                            param = new SqlParameter[1];
                            param[0] = new SqlParameter { ParameterName = "@zipCodeZoneId", Value = zipCodeZoneId };

                            DataSet dsZipCodePrice = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                            if (dsZipCodePrice != null && dsZipCodePrice.Tables.Count > 0 && dsZipCodePrice.Tables[0].Rows.Count > 0)
                            {
                                decimal chgAmt = Convert.ToDecimal(dsZipCodePrice.Tables[0].Rows[0]["dcChgAmt"]);
                                foreach (DataRow drUnitPrice in dsUnitPrice.Tables[0].Rows)
                                {
                                    drUnitPrice["dcStdRate"] = Convert.ToDecimal(drUnitPrice["dcStdRate"]) + chgAmt;
                                }
                            }
                        }
                    }
                }
                finally
                {
                    Data.CloseConnection(conn);
                }
            }
            return dsUnitPrice;
        }

        public DataSet GetUnitTypesForLDM(string locationCode)
        {
            DataSet dsRDFacility = GetRDFacilityData(locationCode);
            DataSet dsUnitType = new DataSet();

            if (dsRDFacility != null && dsRDFacility.Tables.Count > 0 && dsRDFacility.Tables[0].Rows.Count > 0)
            {
                string dbName = dsRDFacility.Tables[0].Rows[0]["SLDBName"].ToString();
                int siteId = Convert.ToInt32(dsRDFacility.Tables[0].Rows[0]["SLSiteId"]);

                string connString = Data._connStringSTARS;
                connString = connString.Replace("StarsDb", dbName);
                SqlConnection conn = Data.GetConnection(connString);

                //Get the unit type data
                //string sql = String.Format("SELECT UnitTypeID,SiteID,sTypeName FROM UnitTypes  where dDeleted is null and SiteId={0} and (sTypeName like '%FT%' and sTypeName not like '%DAMAGED%' and sTypeName not like '%12%')", siteId);
                string sql = "SELECT UnitTypeID,SiteID,sTypeName FROM UnitTypes  where dDeleted is null and SiteId=@siteId and (sTypeName like '%FT%' and sTypeName not like '%DAMAGED%' and sTypeName not like '%12%')";

                var param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@siteId", Value = siteId };

                try
                {
                    dsUnitType = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                }
                finally
                {
                    Data.CloseConnection(conn);
                }
            }
            return dsUnitType;
        }

        public DataSet GetUnitTypeByUnitName(string unitName, string locationCode)
        {
            DataSet dsUnitType = new DataSet();
            DataSet dsRDFacility = GetRDFacilityData(locationCode);

            if (dsRDFacility != null && dsRDFacility.Tables.Count > 0 && dsRDFacility.Tables[0].Rows.Count > 0)
            {
                string dbName = dsRDFacility.Tables[0].Rows[0]["SLDBName"].ToString();

                string connString = Data._connStringSTARS;
                connString = connString.Replace("StarsDb", dbName);
                SqlConnection conn = Data.GetConnection(connString);

                try
                {
                    //string sql = String.Format("select ut.UnitTypeID, ut.sTypeName from units u inner join unittypes ut on u.unittypeid=ut.unittypeid where u.sunitName='{0}' and u.ddeleted is null", unitName);
                    string sql = "select ut.UnitTypeID, ut.sTypeName from units u inner join unittypes ut on u.unittypeid=ut.unittypeid where u.sunitName=@unitName and u.ddeleted is null";

                    var param = new SqlParameter[1];
                    param[0] = new SqlParameter { ParameterName = "@unitName", Value = unitName };

                    dsUnitType = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                }
                finally
                {
                    Data.CloseConnection(conn);
                }
            }
            return dsUnitType;
        }

        public DataSet GetPromoGlobalNumber(string promoCode, string locationCode)
        {
            DataSet dsRDFacility = GetRDFacilityData(locationCode);
            DataSet dsPrmo = new DataSet();

            if (dsRDFacility != null && dsRDFacility.Tables.Count > 0 && dsRDFacility.Tables[0].Rows.Count > 0)
            {
                string dbName = dsRDFacility.Tables[0].Rows[0]["SLDBName"].ToString();
                int siteId = Convert.ToInt32(dsRDFacility.Tables[0].Rows[0]["SLSiteId"]);

                string connString = Data._connStringSTARS;
                connString = connString.Replace("StarsDb", dbName);
                SqlConnection conn = Data.GetConnection(connString);

                try
                {
                    //string sql = String.Format("select p.iPromoGlobalNum,e.SiteId, p.sPromoDescription from Promotions p inner join PromotionEligibility e on p.PromotionID=e.PromotionID where e.SiteID={0} and p.sPromotionCode='{1}' and '{2}' between p.dPromoStrt and dPromoEnd and p.dDeleted is null and e.dDeleted is null", siteId, promoCode, DateTime.Now.ToShortDateString());
                    string sql = @"select p.iPromoGlobalNum,e.SiteId, p.sPromoDescription 
from Promotions p 
inner join PromotionEligibility e on p.PromotionID=e.PromotionID 
where e.SiteID=@siteId and p.sPromotionCode=@promoCode and @promoDate between p.dPromoStrt and dPromoEnd and p.dDeleted is null and e.dDeleted is null";

                    var param = new SqlParameter[3];
                    param[0] = new SqlParameter { ParameterName = "@siteId", Value = siteId };
                    param[1] = new SqlParameter { ParameterName = "@promoCode", Value = promoCode };
                    param[2] = new SqlParameter { ParameterName = "@promoDate", Value = DateTime.Now.ToShortDateString() };

                    dsPrmo = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                }
                finally
                {
                    Data.CloseConnection(conn);
                }
            }

            return dsPrmo;
        }

        public DataSet GetCallBlastData(string locationCode)
        {
            DataSet dsCallBlast = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //string sql = String.Format("select u.StoreRowid, u.StoreNumber, u.City, u.Level1Name, u.Level1Phone, u.Level2Name, u.Level2Phone, u.Level3Name, u.level3Phone, ltrim(rtrim(u.Email1)) as Email1, ltrim(rtrim(u.Email2)) as Email2 from UniqueCallBlast u inner join RDFacility f on u.StoreRowId = f.StoreRowId where f.slLocCode = '{0}'", locationCode);
                string sql = @"select u.StoreRowid, u.StoreNumber, u.City, u.Level1Name, u.Level1Phone, u.Level2Name, u.Level2Phone, u.Level3Name, u.level3Phone, ltrim(rtrim(u.Email1)) as Email1, ltrim(rtrim(u.Email2)) as Email2 
from UniqueCallBlast u 
inner join RDFacility f on u.StoreRowId = f.StoreRowId 
where f.slLocCode = @locationCode";

                var param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@locationCode", Value = locationCode };

                dsCallBlast = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
            return dsCallBlast;
        }

        public DataSet GetContainerType(string Code)
        {
            DataSet dstype = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //string sql = String.Format("select * from Transite_Container_Type where Jaguar_Code = '{0}'", Code);
                string sql = "select * from Transite_Container_Type where Jaguar_Code = @Code";

                var param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@Code", Value = Code };

                dstype = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
            return dstype;
        }

        public DataSet GetContainerTypes(bool islocal)
        {
            DataSet dstype = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                string sql = "";
                if (islocal == true)
                {
                    sql = String.Format("select * from Transite_Container_Type where ISLOCAL = 1");
                }
                else
                {
                    sql = String.Format("select * from Transite_Container_Type where ISLDM = 1");
                }
                dstype = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql);
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
            return dstype;
        }

        public void UpdateConvertedToOrderBy(int qorid, string userid)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //string sql = String.Format("update PRGQuoteOrderLog set ConvertedToOrderBy='{0}', ConvertedToOrderDate='{1}' where qorid={2}", userid, DateTime.Now, qorid);
                string sql = "update PRGQuoteOrderLog set ConvertedToOrderBy=@ConvertedToOrderBy, ConvertedToOrderDate=@ConvertedToOrderDate where qorid=@qorid";

                var param = new SqlParameter[3];
                param[0] = new SqlParameter { ParameterName = "@ConvertedToOrderBy", Value = userid };
                param[1] = new SqlParameter { ParameterName = "@ConvertedToOrderDate", Value = DateTime.Now };
                param[2] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetQuoteDispositionCodes()
        {
            DataSet dsDispCodes = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                dsDispCodes = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetDispositionCodes");
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
            return dsDispCodes;
        }

        public DataSet GetQuoteDispositionReasons(int qorid)
        {
            DataSet dsReasons = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter paramQorid = new SqlParameter("@qorid", qorid);
                dsReasons = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetDispositionList", paramQorid);
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
            return dsReasons;
        }

        public DataSet GetScheduledTouches(string locationcode, DateTime schdate)
        {
            DataSet dsTouches = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //SqlParameter paramQorid = new SqlParameter("@Facility", locationcode);
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@Facility";
                param[0].Value = locationcode;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@TouchDate";
                param[1].Value = schdate;
                dsTouches = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_sel_ScheduledTouches", param);
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
            return dsTouches;
        }

        public void UpdateDispositionReason(int qorid, int DispositionId, string comment)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@qorid";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@DispId";
                param[1].Value = DispositionId;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@comment";
                param[2].Value = comment;
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_Ins_DispositionReason", param);
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetCorporatePOSItems(string locationCode)
        {
            int siteId = 0;
            siteId = GetSiteID(locationCode);
            DataSet posItems = new DataSet();

            if (siteId > 0)
            {
                SqlConnection conn = Data.GetPRConnection();
                try
                {
                    //posItems = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select ChargeDescId, SiteId, sChgDesc, dcPrice, dcTax1Rate, dcTax2Rate from VW_Corp_POSItems where SiteId=" + siteId);
                    string sql = "Select ChargeDescId, SiteId, sChgDesc, dcPrice, dcTax1Rate, dcTax2Rate from VW_Corp_POSItems where SiteId= @siteId";

                    var param = new SqlParameter[1];
                    param[0] = new SqlParameter { ParameterName = "@siteId", Value = siteId };

                    posItems = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                }
                finally
                {
                    //conn.Close();
                    Data.CloseConnection(conn);
                }
            }
            return posItems;
        }

        public DataSet GetDieselPrices()
        {
            DataSet dsDieselPrices = new DataSet();
            string sql = "select * from WDieselPrices";
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                dsDieselPrices = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql);
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }

            return dsDieselPrices;
        }

        public int InsertWSCallLogData(string webserviceName, string methodName, Dictionary<string, string> paramList)
        {
            //string sql = "Insert into WSCallLog(WebServiceName,WebMethodName,StartTime) values('" + webserviceName + "','" + methodName + "','" + DateTime.Now + "'); select @@identity";
            string sql = "Insert into WSCallLog(WebServiceName,WebMethodName,StartTime) values(@webserviceName, @methodName, @StartTime); select @@identity";

            var param1 = new SqlParameter[3];
            param1[0] = new SqlParameter { ParameterName = "@webserviceName", Value = webserviceName };
            param1[1] = new SqlParameter { ParameterName = "@methodName", Value = methodName };
            param1[2] = new SqlParameter { ParameterName = "@StartTime", Value = DateTime.Now };

            SqlConnection conn = Data.GetPRConnection();
            int wsCallLogId = 0;
            try
            {
                object id = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param1);
                wsCallLogId = Convert.ToInt32(id);

                foreach (KeyValuePair<string, string> param in paramList)
                {
                    //sql = "Insert into WSMethodParams(WSCallLogId,ParamName,ParamValue) values(" + wsCallLogId + ",'" + param.Key + "','" + param.Value + "')";
                    sql = "Insert into WSMethodParams(WSCallLogId,ParamName,ParamValue) values(@wsCallLogId, @paramName, @paramValue)";
                    param1 = new SqlParameter[3];
                    param1[0] = new SqlParameter { ParameterName = "@wsCallLogId", Value = wsCallLogId };
                    param1[1] = new SqlParameter { ParameterName = "@paramName", Value = param.Key };
                    param1[2] = new SqlParameter { ParameterName = "@paramValue", Value = param.Value };

                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param1);
                }
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
            return wsCallLogId;
        }

        public void UpdateWSCallLogData(int wsCallLogId, string response)
        {
            //string sql = "Update WSCallLog set Response='" + response + "',EndTime='" + DateTime.Now + "' where WSCallLogId=" + wsCallLogId;
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                string sql = "Update WSCallLog set Response= @response, EndTime= @endTime where WSCallLogId= @wsCallLogId";

                var param = new SqlParameter[3];
                param[0] = new SqlParameter { ParameterName = "@response", Value = response };
                param[1] = new SqlParameter { ParameterName = "@endTime", Value = DateTime.Now };
                param[2] = new SqlParameter { ParameterName = "@wsCallLogId", Value = wsCallLogId };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
        }

        public void InsertJaguarUnitMappingErrors(string orderNum, string message, string storeNo)
        {
            //string sql = "insert into JaguarUnitMappingErrorLog  (OrderNum,Exception,Date,StoreNo) values('" + orderNum + "','" + message + "','" + DateTime.Now + "','" + storeNo + "')";
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                string sql = "insert into JaguarUnitMappingErrorLog  (OrderNum,Exception,Date,StoreNo) values(@orderNum, @message, @Dt, @storeNo)";

                var param = new SqlParameter[4];
                param[0] = new SqlParameter { ParameterName = "@orderNum", Value = orderNum };
                param[1] = new SqlParameter { ParameterName = "@message", Value = message };
                param[2] = new SqlParameter { ParameterName = "@Dt", Value = DateTime.Now };
                param[3] = new SqlParameter { ParameterName = "@storeNo", Value = storeNo };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
        }

        #region Consumer Web Customer Account Management

        public int CheckIfAccountExists(string emailAddress)
        {
            SqlConnection conn = Data.GetPRConnection();
            int loginId = 0;
            try
            {
                if (!string.IsNullOrEmpty(emailAddress))
                {
                    //string sql = String.Format("select LoginId from CWCustLogin where Emailaddress='{0}'", emailAddress);
                    string sql = "select LoginId from CWCustLogin where Emailaddress = @emailAddress";

                    var param = new SqlParameter[1];
                    param[0] = new SqlParameter { ParameterName = "@emailAddress", Value = emailAddress };

                    object obj = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param);
                    if (obj != null && obj != DBNull.Value)
                        loginId = Convert.ToInt32(obj);
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return loginId;
        }

        public int CheckIfAccountActive(string emailAddress)
        {
            SqlConnection conn = Data.GetPRConnection();
            int loginId = 0;
            try
            {
                //string sql = String.Format("select LoginId from CWCustLogin where Emailaddress='{0}' and isActive=1", emailAddress);
                string sql = "select LoginId from CWCustLogin where Emailaddress=@emailAddress and isActive=1";

                var param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@emailAddress", Value = emailAddress };

                object obj = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param);

                if (obj != null && obj != DBNull.Value)
                    loginId = Convert.ToInt32(obj);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return loginId;
        }

        /// <summary>
        /// Create a new log in account for Consumer Web
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="siteId"></param>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public int CreateNewAccount(string emailAddress, string password, string firstName, string lastName, string addressLine1, string addressLine2, string phone, string city, string state, string zip)
        {
            SqlConnection conn = Data.GetPRConnection();
            int loginId = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[10];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@EmailAddress";
                param[0].Value = emailAddress;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@Password";
                param[1].Value = password;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@FirstName";
                param[2].Value = firstName;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@LastName";
                param[3].Value = lastName;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@AddressLine1";
                param[4].Value = addressLine1;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@AddressLine2";
                param[5].Value = addressLine2;
                param[6] = new SqlParameter();
                param[6].ParameterName = "@PhoneNumber";
                param[6].Value = phone;
                param[7] = new SqlParameter();
                param[7].ParameterName = "@City";
                param[7].Value = city;
                param[8] = new SqlParameter();
                param[8].ParameterName = "@State";
                param[8].Value = state;
                param[9] = new SqlParameter();
                param[9].ParameterName = "@Zipcode";
                param[9].Value = zip;

                loginId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_ins_CWCustLogin", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return loginId;
        }

        public void UpdateCustomerAccountDetails(int loginId, string firstName, string lastName, string addressLine1, string addressLine2, string phone, string city, string state, string zip)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlParameter[] param = new SqlParameter[10];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@IsActive";
                param[0].Value = true;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@LoginId";
                param[1].Value = loginId;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@FirstName";
                param[2].Value = firstName;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@LastName";
                param[3].Value = lastName;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@AddressLine1";
                param[4].Value = addressLine1;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@AddressLine2";
                param[5].Value = addressLine2;
                param[6] = new SqlParameter();
                param[6].ParameterName = "@Phone";
                param[6].Value = phone;
                param[7] = new SqlParameter();
                param[7].ParameterName = "@City";
                param[7].Value = city;
                param[8] = new SqlParameter();
                param[8].ParameterName = "@State";
                param[8].Value = state;
                param[9] = new SqlParameter();
                param[9].ParameterName = "@Zipcode";
                param[9].Value = zip;

                loginId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_upd_CWCustLogin", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public int AddSiteTenantCombinationToCWAccount(int siteId, int tenantId, int loginId)
        {
            SqlConnection conn = Data.GetPRConnection();
            int siteTenantLoginId = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@SiteId";
                param[0].Value = siteId;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@TenantId";
                param[1].Value = tenantId;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@LoginId";
                param[2].Value = loginId;

                siteTenantLoginId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_ins_CWSiteTenantLogin", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return siteTenantLoginId;
        }

        public int AddCWQORListRecord(int siteTenantLoginId, int qorid)
        {
            SqlConnection conn = Data.GetPRConnection();
            int qorlistId = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@SiteTenantLoginId";
                param[0].Value = siteTenantLoginId;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@QORID";
                param[1].Value = qorid;

                qorlistId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_ins_CWQORList", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return qorlistId;
        }

        public bool UpdateCWQORListRentaStatus(int qorid)
        {
            bool isSuccess = false;
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //string sql = String.Format("Update CWQORList set RentalType='Order' where QORID={0}", qorid);
                string sql = "Update CWQORList set RentalType='Order' where QORID= @qorid";

                var param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
                isSuccess = true;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isSuccess;
        }

        public bool UpdateCancelStatusInCWQORList(int qorid)
        {
            //hari
            bool isSuccess = false;
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //string sql = String.Format("Update CWQORList set RentalStatus='Cancelled' where QORID={0}", qorid);
                string sql = "Update CWQORList set RentalStatus='Cancelled' where QORID = @qorid";

                var param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
                isSuccess = true;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isSuccess;
        }

        public DataSet FetchCWQorList(int loginId)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsQorlist = new DataSet();

            try
            {
                SqlParameter param = new SqlParameter();

                param.ParameterName = "@loginid";
                param.Value = loginId;
                dsQorlist = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_sel_CWQORIDs", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsQorlist;
        }

        /// <summary>
        /// Consumer web customer login
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public int LoginToConsumerWeb(string emailAddress, string password)
        {
            SqlConnection conn = Data.GetPRConnection();
            int loginId = 0;
            DataSet dsLogin = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@EmailAddress";
                param[0].Value = emailAddress;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@Password";
                param[1].Value = password;

                dsLogin = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_Login_CW", param);
                if (dsLogin.Tables.Count > 0 && dsLogin.Tables[0].Rows.Count > 0)
                {
                    loginId = Convert.ToInt32(dsLogin.Tables[0].Rows[0]["LoginId"]);
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return loginId;
        }

        public bool ChangePassword(string emailAddress, string newPassword)
        {
            SqlConnection conn = Data.GetPRConnection();
            bool isSuccess = false;

            try
            {
                //string sql = String.Format("IF EXISTS(SELECT 1 FROM CWCustLogin WHERE EmailAddress='{0}')begin select 1   update CWCustLogin set [Password]='{1}' where EmailAddress='{2}' end else  select 0", emailAddress, newPassword, emailAddress);
                string sql = @"
IF EXISTS(SELECT 1 FROM CWCustLogin WHERE EmailAddress = @emailAddress)
begin 
    select 1   
    update CWCustLogin 
    set [Password] = @newPassword
    where EmailAddress = @emailAddress
end 
else  
select 0";

                var param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@emailAddress", Value = emailAddress };
                param[1] = new SqlParameter { ParameterName = "@newPassword", Value = newPassword };

                return Convert.ToBoolean(SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isSuccess;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        public DataSet GetSiteTenantList(int loginId)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsSiteTenantList = new DataSet();
            try
            {
                //string sql = String.Format("select distinct cw.SiteId, cw.LoginId, cw.TenantId, cw.SiteTenantLoginId, f.SlLocCode, f.CompDBAName from CWSiteTenantLogin  cw inner join RdFacility f on cw.siteid=f.slsiteid where LoginId={0}", loginId);
                string sql = @"
select distinct cw.SiteId, cw.LoginId, cw.TenantId, cw.SiteTenantLoginId, f.SlLocCode, f.CompDBAName 
from CWSiteTenantLogin  cw 
inner join RdFacility f on cw.siteid=f.slsiteid where LoginId= @loginId";

                var param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@loginId", Value = loginId };

                dsSiteTenantList = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsSiteTenantList;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        public DataSet GetSiteTenantList(int loginId, int siteId)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsSiteTenantList = new DataSet();
            try
            {
                //string sql = String.Format("select cw.SiteTenantLoginId,cw.SiteId, cw.TenantId, cw.LoginId, f.SlLocCode, f.CompDBAName from CWSiteTenantLogin  cw inner join RdFacility f on cw.siteid=f.slsiteid where LoginId={0} and SiteId={1}", loginId, siteId);
                string sql = @"
select cw.SiteTenantLoginId,cw.SiteId, cw.TenantId, cw.LoginId, f.SlLocCode, f.CompDBAName 
from CWSiteTenantLogin  cw 
inner join RdFacility f on cw.siteid=f.slsiteid 
where LoginId=@loginId and SiteId=@siteId";

                var param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@loginId", Value = loginId };
                param[1] = new SqlParameter { ParameterName = "@siteId", Value = siteId };

                dsSiteTenantList = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsSiteTenantList;
        }

        /// <summary>
        /// Active the consumer web account
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        public bool ActivateCWUserAccount(int loginId)
        {
            bool isSuucess = false;
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@LoginId";
                param[0].Value = loginId;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_Activate_CW_Account", param);
                isSuucess = true;
            }
            catch
            {
                isSuucess = false;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isSuucess;
        }

        /// <summary>
        /// Gets the active QORs associated
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        public DataSet GetCWQORList(int siteTenantLoginId)
        {
            DataSet dsQORList = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@SiteTenantLoginId";
                param[0].Value = siteTenantLoginId;

                dsQORList = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_sel_CWQORList", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsQORList;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="qotrid"></param>
        /// <returns></returns>
        public DataSet CheckIfQORIDExists(int qorid)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = Data.GetPRConnection();


            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;

                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_CheckQORIDExists_CW", param);

                //if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                //{
                //    loginId = Convert.ToInt32(ds.Tables[0].Rows[0]["LoginId"]);
                //}
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }


        }

        public int CheckIfQORIDLDMExists(int qorid)
        {

            SqlConnection conn = Data.GetPRConnection();
            int loginId = 0;

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@Qorid";
                param[0].Value = qorid;

                loginId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_CheckLdmIdExists", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return loginId;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="loginId"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public int GetSiteTenantLoginID(int loginId, int siteId)
        {
            SqlConnection conn = Data.GetPRConnection();
            int siteTenantLoginId = 0;

            try
            {
                //string sql = String.Format("select SiteTenantLoginId from CWSiteTenantLogin where LoginId={0} and SiteId={1}", loginId, siteId);
                string sql = "select SiteTenantLoginId from CWSiteTenantLogin where LoginId = @loginId and SiteId = @siteId";

                var param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@loginId", Value = loginId };
                param[1] = new SqlParameter { ParameterName = "@siteId", Value = siteId };

                object id = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param);
                if (id != null && id != DBNull.Value)
                {
                    siteTenantLoginId = Convert.ToInt32(id);
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return siteTenantLoginId;
        }

        #endregion Consumer Web Customer Account Management

        public int AddCWLDMList(int ldmRefId, int loginId, bool isActive)
        {
            SqlConnection conn = Data.GetPRConnection();
            int ans = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@ldmRefId";
                param[0].Value = ldmRefId;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@loginId";
                param[1].Value = loginId;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@isActive";
                param[2].Value = isActive;

                //param[0] = new SqlParameter();
                //param[0].ParameterName = "@createdDate";
                //param[0].Value = createdDate;

                //param[0] = new SqlParameter();
                //param[0].ParameterName = "@expirationDate";
                //param[0].Value = expirationDate;

                ans = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_CWLDMList", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ans;
        }

        public void LogToIVRWebServiceTable(string ip, string methodName, string attributes)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@Source_Ip";
                param[0].Value = ip;
                param[0].DbType = DbType.String;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@Web_Method_Called";
                param[1].Value = methodName;
                param[1].DbType = DbType.String;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@Parameter_Values";
                param[2].Value = attributes;
                param[2].DbType = DbType.String;

                //param[0] = new SqlParameter();
                //param[0].ParameterName = "@createdDate";
                //param[0].Value = createdDate;

                //param[0] = new SqlParameter();
                //param[0].ParameterName = "@expirationDate";
                //param[0].Value = expirationDate;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_IvrWebServiceLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

        }

        public string GetLocationCodeForFacilityManager(int storeNum)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@storeNum";
                param[0].Value = storeNum;

                return Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetLocationCodebyStoreNo", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public bool InsertPRGUserInfo(string firstname, string lastname, string email, string username, string password, string state, string country, string city, string zip, string role, string Address)
        {
            throw new NotImplementedException();
        }

        public double FetchRDFee(int feetype)
        {
            DataSet ds = new DataSet();
            string sql = "select * from RDFee";
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql);
            }
            finally
            {
                conn.Close();
            }
            if (ds != null && ds.Tables[0].Rows.Count > 0)
                return Convert.ToDouble(ds.Tables[0].Select("FeeTypeID =" + feetype)[0]["Fee"]);
            else
                return 0;

        }

        public DataSet GetAllPackratFacilities()
        {
            DataSet dsFacilites = new DataSet();
            string sql = "select * from RdFacility";
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                dsFacilites = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql);
            }
            finally
            {
                conn.Close();
            }

            return dsFacilites;
        }

        public DataSet GetOBCallDuplicateTenants(int tenantId, int rentalId)
        {
            DataSet dsList;
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@TenantId";
            param[0].Value = tenantId;
            param[1] = new SqlParameter();
            param[1].ParameterName = "@RentalId";
            param[1].Value = rentalId;

            try
            {
                dsList = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetOBCallDuplicateTenants", param);
            }
            finally
            {
                conn.Close();
            }

            return dsList;
        }

        public void UpdateConfrimStatusOBCalltenant(int id)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@id";
            param[0].Value = id;
            SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_updateConfrimStatusOBCalltenant", param);
        }

        public void InsertOBCallDuplicateTenants(int tenantId, int rentalId, int qorid, string firstname, string lastname, string phone, DateTime deliverydate, string locationCode, int unitId, string touchtyp, string jaguarOrder)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[12];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@TenantId";
            param[0].Value = tenantId;
            param[1] = new SqlParameter();
            param[1].ParameterName = "@RentalId";
            param[1].Value = rentalId;
            param[2] = new SqlParameter();
            param[2].ParameterName = "@QORID";
            param[2].Value = qorid;
            param[3] = new SqlParameter();
            param[3].ParameterName = "@FirstName";
            param[3].Value = firstname;
            param[4] = new SqlParameter();
            param[4].ParameterName = "@LastName";
            param[4].Value = lastname;
            param[5] = new SqlParameter();
            param[5].ParameterName = "@Phone";
            param[5].Value = phone;
            param[6] = new SqlParameter();
            param[6].ParameterName = "@DeliveryDate";
            param[6].Value = deliverydate;
            param[7] = new SqlParameter();
            param[7].ParameterName = "@Locationcode";
            param[7].Value = locationCode;
            param[8] = new SqlParameter();
            param[8].ParameterName = "@UnitId";
            param[8].Value = unitId;
            param[9] = new SqlParameter();
            param[9].ParameterName = "@CreatedDate";
            param[9].Value = DateTime.Today;
            param[10] = new SqlParameter();
            param[10].ParameterName = "@TouchType";
            param[10].Value = touchtyp;
            param[11] = new SqlParameter();
            param[11].ParameterName = "@JaguarOrder";
            param[11].Value = jaguarOrder;

            SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_InsertOBCallDuplicateTenant", param);
        }

        public void InsertCatsCustomer(string firstName, string middileName, string lastName, string email,
            string phone, string altPhone, string companyName, string address1, string address2, string city,
            string state, string zip, string mktQuestionDesc, int siteId, int tenantId)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[15];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@FName";
            param[0].Value = firstName;
            param[1] = new SqlParameter();
            param[1].ParameterName = "@MI";
            param[1].Value = middileName;
            param[2] = new SqlParameter();
            param[2].ParameterName = "@LName";
            param[2].Value = lastName;
            param[3] = new SqlParameter();
            param[3].ParameterName = "@Email";
            param[3].Value = email;
            param[4] = new SqlParameter();
            param[4].ParameterName = "@PhoneNo";
            param[4].Value = phone;
            param[5] = new SqlParameter();
            param[5].ParameterName = "@AltPhoneNo";
            param[5].Value = altPhone;
            param[6] = new SqlParameter();
            param[6].ParameterName = "@Company";
            param[6].Value = companyName;
            param[7] = new SqlParameter();
            param[7].ParameterName = "@Add1";
            param[7].Value = address1;
            param[8] = new SqlParameter();
            param[8].ParameterName = "@Add2";
            param[8].Value = address2;
            param[9] = new SqlParameter();
            param[9].ParameterName = "@City";
            param[9].Value = city;
            param[10] = new SqlParameter();
            param[10].ParameterName = "@State";
            param[10].Value = state;
            param[11] = new SqlParameter();
            param[11].ParameterName = "@ZipCode";
            param[11].Value = zip;

            param[12] = new SqlParameter();
            param[12].ParameterName = "@SOI";
            param[12].Value = mktQuestionDesc;

            param[13] = new SqlParameter();
            param[13].ParameterName = "@TenantId";
            param[13].Value = tenantId;

            param[14] = new SqlParameter();
            param[14].ParameterName = "@SiteId";
            param[14].Value = siteId;
            //param[15] = new SqlParameter();
            //param[15].ParameterName = "@QORID";
            //param[15].Value = qorId;

            var ans = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "USP_Ins_CATS_Customer", param);
        }

        public void UpdateCatsCustomer(string firstName, string middileName, string lastName, string email,
          string phone, string altPhone, string companyName, string address1, string address2, string city,
          string state, string zip, string mktQuestionDesc, int siteId, int tenantId)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[15];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@FName";
            param[0].Value = firstName;
            param[1] = new SqlParameter();
            param[1].ParameterName = "@MI";
            param[1].Value = middileName;
            param[2] = new SqlParameter();
            param[2].ParameterName = "@LName";
            param[2].Value = lastName;
            param[3] = new SqlParameter();
            param[3].ParameterName = "@Email";
            param[3].Value = email;
            param[4] = new SqlParameter();
            param[4].ParameterName = "@PhoneNo";
            param[4].Value = phone;
            param[5] = new SqlParameter();
            param[5].ParameterName = "@AltPhoneNo";
            param[5].Value = altPhone;
            param[6] = new SqlParameter();
            param[6].ParameterName = "@Company";
            param[6].Value = companyName;
            param[7] = new SqlParameter();
            param[7].ParameterName = "@Add1";
            param[7].Value = address1;
            param[8] = new SqlParameter();
            param[8].ParameterName = "@Add2";
            param[8].Value = address2;
            param[9] = new SqlParameter();
            param[9].ParameterName = "@City";
            param[9].Value = city;
            param[10] = new SqlParameter();
            param[10].ParameterName = "@State";
            param[10].Value = state;
            param[11] = new SqlParameter();
            param[11].ParameterName = "@ZipCode";
            param[11].Value = zip;

            param[12] = new SqlParameter();
            param[12].ParameterName = "@SOI";
            param[12].Value = mktQuestionDesc;

            param[13] = new SqlParameter();
            param[13].ParameterName = "@TenantId";
            param[13].Value = tenantId;

            param[14] = new SqlParameter();
            param[14].ParameterName = "@SiteId";
            param[14].Value = siteId;
            //param[15] = new SqlParameter();
            //param[15].ParameterName = "@QORID";
            //param[15].Value = qorId;

            SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "USS_UPD_CATS_Customer", param);
        }

        public void CATSMapping(int tenantId, int siteId, int qorid)
        {

            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@QORID";
            param[0].Value = qorid;
            param[1] = new SqlParameter();
            param[1].ParameterName = "@TenantId";
            param[1].Value = tenantId;
            param[2] = new SqlParameter();
            param[2].ParameterName = "@SiteId";
            param[2].Value = siteId;
            //param[3] = new SqlParameter();
            //param[3].ParameterName = "@QORID";
            //param[3].Value = qorid;

            try
            {
                SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "USP_UPD_CATS_Mapping", param);
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// get all Price config
        /// </summary>
        /// <returns></returns>
        public DataSet GetPriceConfigurationInfo()
        {
            return GetPriceConfigurationInfo(string.Empty);
        }
        /// <summary>
        /// get pricing config by item ItemDescription
        /// </summary>
        /// <param name="ItemDescription">name of pricing item</param>
        /// <returns></returns>
        public DataSet GetPriceConfigurationInfo(string ItemDescription)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsPriceConfiguration = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@ItemDescription";
                param[0].Value = ItemDescription;
                dsPriceConfiguration = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetPriceConfiguration", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsPriceConfiguration;
        }

        //public DataSet GetNewPriceConfigurationInfo()
        //{
        //    SqlConnection conn = Data.GetPRConnection();
        //    string sql = "select * from PriceConfiguration where IsActive=1";
        //    DataSet dsPriceConfiguration = new DataSet();

        //    try
        //    {
        //        dsPriceConfiguration = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql);
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }

        //    return dsPriceConfiguration;
        //}

        public bool GetPriceConfigurationInfo(string locationCode, string chargeType)
        {

            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@LocationCode";
            param[0].Value = locationCode;
            param[1] = new SqlParameter();
            param[1].ParameterName = "@ChargeType";
            param[1].Value = chargeType;

            try
            {
                return Convert.ToBoolean(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "USP_Pricing_FeeApplicability", param));
            }
            finally
            {
                conn.Close();
            }
        }

        public List<QuoteConfiguration> GetQuoteConfiguration()
        {
            List<QuoteConfiguration> quoteConfiguration = new List<QuoteConfiguration>();
            string sql = "select * from QuoteConfiguration";
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlDataReader reader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        QuoteConfiguration config = new QuoteConfiguration();
                        config.ItemDescription = reader["ItemDescription"] == DBNull.Value ? "" : reader["ItemDescription"].ToString();
                        config.ItemName = reader["ItemName"] == DBNull.Value ? "" : reader["ItemName"].ToString();
                        //config.ServiceType = (PREnums.ServiceType)Convert.ToInt32(reader["ServiceType"]);
                        config.IncludeQuoteCreation = reader["IncludeQuoteCreation"] == DBNull.Value ? false : Convert.ToBoolean(reader["IncludeQuoteCreation"]);
                        config.ExcludeAdminFee = reader["ExcludeAdminFee"] == DBNull.Value ? false : Convert.ToBoolean(reader["ExcludeAdminFee"]);
                        config.AgentCanDelete = reader["AgentCanDelete"] == DBNull.Value ? false : Convert.ToBoolean(reader["AgentCanDelete"]);
                        config.SupervisorCanDelete = reader["SupervisorCanDelete"] == DBNull.Value ? false : Convert.ToBoolean(reader["SupervisorCanDelete"]);
                        config.ManagerCanDelete = reader["ManagerCanDelete"] == DBNull.Value ? false : Convert.ToBoolean(reader["ManagerCanDelete"]);
                        quoteConfiguration.Add(config);
                    }
                }
                return quoteConfiguration;
            }
            finally
            {
                conn.Close();
            }
        }

        public PostalCodes GetPostalCodeByZip(string Zipcode)
        {
            SqlConnection conn = Data.GetPRConnection();
            PostalCodes oPostalCodes = null;

            try
            {

                var param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@Zipcode", Value = Zipcode };

                var dsPostalCode = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_GetPostalCodes", param);

                if (dsPostalCode?.Tables?.Count > 0 && dsPostalCode.Tables[0]?.Rows?.Count > 0)
                {
                    DataRow dr = dsPostalCode.Tables[0]?.Rows[0];
                    oPostalCodes = new PostalCodes
                    {
                        City = Convert.ToString(dr["City"]),
                        State = Convert.ToString(dr["State"]),
                        County = Convert.ToString(dr["County"]),
                        Zipcode = Convert.ToString(dr["Zipcode"]),
                        Latitude = Convert.ToDecimal(dr["Latitude"]),
                        Longitude = Convert.ToDecimal(dr["Longitude"]),
                        Status = (PostalCodeValidationStatus)Enum.Parse(typeof(PostalCodeValidationStatus), Convert.ToString(dr["Status"]))
                    };
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return oPostalCodes;
        }

        public List<PostalCodes> GetPostalCodes()
        {
            SqlConnection conn = Data.GetPRConnection();
            List<PostalCodes> oPostalCodes = null;
            List<string> failedZipCodes = new List<string>();

            try
            {
                var dsPostalCode = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_GetPostalCodes");

                if (dsPostalCode?.Tables?.Count > 0 && dsPostalCode.Tables[0]?.Rows?.Count > 0)
                {
                    oPostalCodes = new List<PostalCodes>();
                    foreach (DataRow dr in dsPostalCode.Tables[0]?.Rows)
                    {
                        try
                        {
                            oPostalCodes.Add(new PostalCodes
                            {
                                City = dr["City"] != DBNull.Value ? Convert.ToString(dr["City"]) : string.Empty,
                                State = dr["State"] != DBNull.Value ? Convert.ToString(dr["State"]) : string.Empty,
                                County = dr["County"] != DBNull.Value ? Convert.ToString(dr["County"]) : string.Empty,
                                Zipcode = Convert.ToString(dr["Zipcode"]),
                                Latitude = dr["Latitude"] != DBNull.Value ? Convert.ToDecimal(dr["Latitude"]) : 0,
                                Longitude = dr["Longitude"] != DBNull.Value ? Convert.ToDecimal(dr["Longitude"]) : 0
                                //,Status = (PostalCodeValidationStatus)Enum.Parse(typeof(PostalCodeValidationStatus), Convert.ToString(dr["Status"]))
                            });
                        }
                        catch
                        {
                            string zipcode = dr["Zipcode"] != DBNull.Value ? Convert.ToString(dr["Zipcode"]) : "NoZipCode";
                            failedZipCodes.Add(zipcode);
                        }
                    }
                }
            }
            finally
            {
                Data.CloseConnection(conn);

                if (failedZipCodes.Count > 0)
                {
                    try
                    {
                        LocalDataHandler local = new LocalDataHandler();
                        local.InsertPRErrorLog(DateTime.Now, "", "Postal Code Read Error", "Error Reading Zipcode Data", "GetPostalCodes Method: " + String.Join(", ", failedZipCodes.ToArray()), -1);
                    }
                    catch
                    { //Ignore it 
                    }
                }
            }
            return oPostalCodes;
        }

        public void UpdatePostalCodeStatus(string Zipcode, PostalCodeValidationStatus Status)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                var param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@Zipcode", Value = Zipcode };
                param[1] = new SqlParameter { ParameterName = "@Status", Value = Status.ToString() };

                SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "USP_UpdatePostalCodes", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void InsertPostalCode(PostalCodes postalCode)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                var param = new SqlParameter[6];
                param[0] = new SqlParameter { ParameterName = "@Zipcode", Value = postalCode.Zipcode };
                param[1] = new SqlParameter { ParameterName = "@City", Value = postalCode.City };
                param[2] = new SqlParameter { ParameterName = "@State", Value = postalCode.State };
                param[3] = new SqlParameter { ParameterName = "@Latitude", Value = postalCode.Latitude };
                param[4] = new SqlParameter { ParameterName = "@Longitude", Value = postalCode.Longitude };
                param[5] = new SqlParameter { ParameterName = "@Status", Value = postalCode.Status.ToString() };

                SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "USP_InsertPostalCodes", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void LogGAPIRequestResponse(string request, string response, DateTime startTime, DateTime endTime, string methodName)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                GoogleMapAuditLog googleMapAuditLog = new GoogleMapAuditLog { Request = request, Response = response, StartTime = startTime, EndTime = endTime, MethodName = methodName };

                LogGAPIRequestResponse(googleMapAuditLog);

                //string sql = " insert into GoogleMapAuditLog (GRequest,GResponse,StartTime,EndTime,MethodName) VALUES('" + request + "','" + response + "','" + startTime + "','" + endTime + "','" + methodName + "')";
                //                string sql = @"
                //insert into GoogleMapAuditLog (GRequest,GResponse,StartTime,EndTime,MethodName) 
                //VALUES(@request, @response, @startTime, @endTime, @methodName)";

                //                var param = new SqlParameter[5];
                //                param[0] = new SqlParameter { ParameterName = "@request", Value = request };
                //                param[1] = new SqlParameter { ParameterName = "@response", Value = response };
                //                param[2] = new SqlParameter { ParameterName = "@startTime", Value = startTime };
                //                param[3] = new SqlParameter { ParameterName = "@endTime", Value = endTime };
                //                param[4] = new SqlParameter { ParameterName = "@methodName", Value = methodName };

                //                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        #region Testmethod
        //      public void InsertGoogleLatLongValuesToPostalCodes(string zip, string city, string state, string lat, string lang)
        //      {
        //          SqlConnection conn = Data.GetPRConnection();

        //          try
        //          {
        //              string sql = @"
        //             	 update PostalCodes
        //set Google_City = @city,
        //Google_State = @state,
        //Google_Latitude = @lat,
        //Google_Longitude = @lang
        //where zipcode = @zipcode";

        //              var param = new SqlParameter[5];
        //              param[0] = new SqlParameter { ParameterName = "@city", Value = city };
        //              param[1] = new SqlParameter { ParameterName = "@state", Value = state };
        //              param[2] = new SqlParameter { ParameterName = "@lat", Value = lat };
        //              param[3] = new SqlParameter { ParameterName = "@lang", Value = lang};
        //              param[4] = new SqlParameter { ParameterName = "@zipcode", Value = zip};

        //              SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
        //          }
        //          finally
        //          {
        //              Data.CloseConnection(conn);
        //          }
        //      }
        #endregion

        public void LogGAPIRequestResponse(GoogleMapAuditLog googleMapAuditLog)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                var param = new SqlParameter[7];
                param[0] = new SqlParameter { ParameterName = "@Request", Value = googleMapAuditLog.Request };
                param[1] = new SqlParameter { ParameterName = "@Response", Value = googleMapAuditLog.Response };
                param[2] = new SqlParameter { ParameterName = "@StartTime", Value = googleMapAuditLog.StartTime };
                param[3] = new SqlParameter { ParameterName = "@EndTime", Value = googleMapAuditLog.EndTime };
                param[4] = new SqlParameter { ParameterName = "@MethodName", Value = googleMapAuditLog.MethodName };
                param[5] = new SqlParameter { ParameterName = "@ApplicationName", Value = googleMapAuditLog.ApplicationName };
                param[6] = new SqlParameter { ParameterName = "@ServerName", Value = googleMapAuditLog.ServerName };

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_InsertGoogleMapAuditLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void LogUspsRequestResponse(string request, string response, DateTime dateTime, string methodName)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter
                {
                    ParameterName = "@Request",
                    Value = request
                };

                param[1] = new SqlParameter
                {
                    ParameterName = "@Response",
                    Value = response
                };

                param[2] = new SqlParameter
                {
                    ParameterName = "@DateTime",
                    Value = dateTime
                };

                param[3] = new SqlParameter
                {
                    ParameterName = "@methodName",
                    Value = methodName
                };

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_USPS_INS_Log", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        // Added to get catpacity
        public DataSet GetCapacity(DateTime startDate, DateTime endDate, string locationCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsCapacity = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@StartDate";
                param[0].Value = startDate;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@EndDate";
                param[1].Value = endDate;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@LocationCode";
                param[2].Value = locationCode;

                dsCapacity = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetCapacity", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsCapacity;
        }

        // Get
        public int? GetCapCategoryByTouchType(int TouchTypeId)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsResult = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@TouchTypeId";
                param[0].Value = TouchTypeId;

                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetCapCategoryBYTouchType", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            if (dsResult != null && dsResult.Tables.Count > 0 && dsResult.Tables[0].Rows.Count > 0)
            {
                if (dsResult.Tables[0].Rows[0][0] != DBNull.Value)
                    return Convert.ToInt32(dsResult.Tables[0].Rows[0][0]);
                else
                    return null;
            }
            else
                return null;
        }

        public int? GetCapCategoryForHoliday()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetCapCategoryForHoliday");
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            if (dsResult != null && dsResult.Tables.Count > 0 && dsResult.Tables[0].Rows.Count > 0)
            {
                if (dsResult.Tables[0].Rows[0][0] != DBNull.Value)
                    return Convert.ToInt32(dsResult.Tables[0].Rows[0][0]);
                else
                    return null;
            }
            else
                return null;
        }

        public DataSet GetCapCategoryTouchTypes()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetCapCategoryTouchTypes");
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public DataSet GetTouchMilesByTypeByFacilityByDateRange(int touchTypeId, string locationCode, DateTime startDate, DateTime endDate)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@TouchTypeId";
            param[0].Value = touchTypeId;

            param[1] = new SqlParameter();
            param[1].ParameterName = "@locationCode";
            param[1].Value = locationCode;

            param[2] = new SqlParameter();
            param[2].ParameterName = "@StartDate";
            param[2].Value = startDate;

            param[3] = new SqlParameter();
            param[3].ParameterName = "@EndDate";
            param[3].Value = endDate;

            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetTouchMilesByTypeByFacilityBYDateRange", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public DataSet GetAllCapTouchTypes()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetAllTouchTypes");
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public DataSet GetCapacityColorActionsByRole(int? roleLevelId)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@RoleLevelId";
            param[0].Value = roleLevelId;


            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure,
                    "USP_CAP_GetCapacityColorActionsByRole", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public String GetCapCategoryByCategoryId(int categoryId)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@CategoryId";
            param[0].Value = categoryId;
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetCapCategory", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            if (dsResult != null && dsResult.Tables.Count > 0 && dsResult.Tables[0].Rows.Count > 0)
            {
                if (dsResult.Tables[0].Rows[0][0] != DBNull.Value)
                    return Convert.ToString(dsResult.Tables[0].Rows[0][0]);
                else
                    return null;
            }
            else
                return null;
        }

        public DataSet GetFacilitiesWithLimitedCapacities(string locationCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@LocationCode";
            param[0].Value = locationCode;
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetFacilitiesWithLimitedCapacity", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public bool ChangeMyPackratEmail(string oldEmailAddress, string newEmailAddress)
        {
            SqlConnection conn = Data.GetPRConnection();
            bool isSuccess = false;

            try
            {
                //string sql = String.Format("IF EXISTS(SELECT 1 FROM CWCustLogin WHERE EmailAddress='{0}')begin select 1   update CWCustLogin set [EmailAddress]='{1}' where EmailAddress='{2}' end else  select 0", oldEmailAddress, newEmailAddress, oldEmailAddress);
                string sql = @"
IF EXISTS(SELECT 1 FROM CWCustLogin WHERE EmailAddress = @oldEmailAddress)
begin 
    select 1   
    update CWCustLogin set [EmailAddress] = @newEmailAddress 
    where EmailAddress = @oldEmailAddress
end 
else  
    select 0";

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@oldEmailAddress", Value = oldEmailAddress };
                param[1] = new SqlParameter { ParameterName = "@newEmailAddress", Value = newEmailAddress };

                isSuccess = Convert.ToBoolean(SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return isSuccess;
        }

        public String GetMyPackratLastLoginTime(string email)
        {
            SqlConnection conn = Data.GetPRConnection();
            string lastLogin = string.Empty;

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@EmailId";
                param[0].Value = email;
                lastLogin = Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_sel_LastLoginDatebyMyPackratEmail", param));
                //string sql = String.Format("SELECT LastLoginDateTime FROM CWCustLogin cl WHERE cl.EmailAddress='{0}'", email);
                //lastLogin = Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.Text, sql));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return lastLogin;
        }

        public String GetMyPackratEmailbyQORIdorLDMId(int qorId, int starId)
        {
            SqlConnection conn = Data.GetPRConnection();
            string emailId = string.Empty;

            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORId";
                param[0].Value = qorId;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@StarsId";
                param[1].Value = starId;
                emailId = Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_sel_MyPackratEmailfromQORIdorStarsId", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return emailId;
        }

        public DataSet GetETASetting(int QorID, string TouchType, int SLSeqNo)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet ds = new DataSet();

            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QorID";
                param[0].Value = QorID;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@TouchType";
                param[1].Value = TouchType;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@SLSeqNo";
                param[2].Value = SLSeqNo;
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_GetETASettings", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ds;
        }

        public DataSet GetETASettingList(string uniqueIdsToGetETASettings)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet ds = new DataSet();

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@TouchUniqueKeys";
                param[0].Value = uniqueIdsToGetETASettings;

                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_GetETASettings_MultipleTouches", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ds;
        }

        //public IEnumerable<TrailerInterstates> GetTrailerInterStates(string provider)
        //{
        //    var conn = Data.GetPRConnection();
        //    try
        //    {
        //        var param = new SqlParameter[1];
        //        param[0] = new SqlParameter
        //        {
        //            ParameterName = "@ProviderName",
        //            Value = provider
        //        };
        //        var ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_TRA_GetTrailerInterstates", param);

        //        return ds?.Tables?[0]?.Rows.OfType<DataRow>().Select(r => new TrailerInterstates()
        //        {
        //            Allowed = (bool)r["Allowed"],
        //            StateCode = (string)r["InterStateCode"],
        //            ProviderName = (string)r["ProviderName"],
        //            Id = (long)r["Id"]
        //        });
        //    }
        //    finally
        //    {
        //        Data.CloseConnection(conn);
        //    }
        //}


        public DataSet GetLDMOriginQorID(int StarsID, int StarsUnitId)
        {
            DataSet dsOriginDetails = new DataSet();
            SqlConnection conn = Data.GetSTARSConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@StarsQuoteId";
                param[0].Value = StarsID;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@ContainerId";
                param[1].Value = StarsUnitId;

                dsOriginDetails = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_FAWT_GetUnitDetails", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsOriginDetails;
        }

        #region NEW FUEL ADJUSTMENT

        //public DataSet GetQTRentalsRecord(string sldbname, int qorid)
        //{
        //    DataSet dsQTRentals = new DataSet();
        //    string sldbConnString = String.Empty;
        //    string sql = String.Format("select * from QTRentals where iQRID_GlobalNum={0} and dDisabled is null", qorid);

        //    if (Data._connStringPR.IndexOf("PRSLLocalTest") > 0)
        //        sldbConnString = Data._connStringPR.Replace("PRSLLocalTest", sldbname);
        //    else
        //        sldbConnString = Data._connStringPR.Replace("PRSLLocal", sldbname);

        //    SqlConnection conn = Data.GetConnection(sldbConnString);

        //    try
        //    {
        //        dsQTRentals = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql);
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }

        //    return dsQTRentals;
        //}

        public DataSet GetQTRentalsRecord(int qorid)
        {
            SqlConnection conn = Data.GetPRConnection();
            //bool FuelAdjusmentAlreadyAdded = false;
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QorId";
                param[0].Value = qorid;

                return SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ups_GetRentalCreatedDate", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public bool CheckFuelAdjusmentAlreadyAdded(string ItemDescription, string ItemsDescription)
        {
            SqlConnection conn = Data.GetPRConnection();
            bool FuelAdjusmentAlreadyAdded = false;
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@ItemDescription";
                param[0].Value = ItemDescription;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@ItemsDescription";
                param[1].Value = ItemsDescription;

                var ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ups_CheckFuelAdjusmentAlreadyAdded", param);

                return ds?.Tables?[0]?.Rows.OfType<DataRow>().Select(r => (bool)r["FuelAdjusmentAlreadyAdded"]).FirstOrDefault() ?? false;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetPriceConfigurationMappingNames(string ItemDescription)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet PriceMappingKeys = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@ItemDescription";
                param[0].Value = ItemDescription;

                PriceMappingKeys = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ups_GetPriceConfigurationMappingNames", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return PriceMappingKeys;
        }

        #endregion
        //public bool IsQuoteArchived(int QorID)
        //{
        //    DataSet ds = GetArchivedStatus(QorID.ToString());

        //    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //        return Convert.ToBoolean(ds.Tables[0].Rows[0]["IsArchived"]);

        //    return false;
        //}

        public int[] GetArchivedQorIDs(string QorIDs)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet ds = new DataSet();

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORIDs";
                param[0].Value = QorIDs;

                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_sel_GetArchivedQorIds", param);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows.Cast<DataRow>().Select(row => Convert.ToInt32(row["QORID"].ToString())).ToArray();
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return null;
        }

        //public DataSet GetTouches(CriteriaSitelinkTouches criteriaSitelinkTouches)
        //{
        //    DataSet dsOriginDetails = new DataSet();
        //    SqlConnection conn = Data.GetPRConnection();
        //    try
        //    {
        //        SqlParameter[] param = new SqlParameter[12];
        //        param[0] = new SqlParameter();
        //        param[0].ParameterName = "@QTTypeIDs";
        //        param[0].Value = criteriaSitelinkTouches.QTTypeIDs;

        //        param[1] = new SqlParameter();
        //        param[1].ParameterName = "@ScheduleFromDate";
        //        param[1].Value = criteriaSitelinkTouches.ScheduleFromDate;

        //        param[2] = new SqlParameter();
        //        param[2].ParameterName = "@ScheduleToDate";
        //        param[2].Value = criteriaSitelinkTouches.ScheduleToDate;

        //        param[3] = new SqlParameter();
        //        param[3].ParameterName = "@FLocationCodes";
        //        param[3].Value = criteriaSitelinkTouches.FLocationCodes;

        //        param[4] = new SqlParameter();
        //        param[4].ParameterName = "@QorIDs";
        //        param[4].Value = criteriaSitelinkTouches.QorIDs;

        //        param[5] = new SqlParameter();
        //        param[5].ParameterName = "@UnitNos";
        //        param[5].Value = criteriaSitelinkTouches.UnitNos;

        //        param[6] = new SqlParameter();
        //        param[6].ParameterName = "@Customerfirstname";
        //        param[6].Value = criteriaSitelinkTouches.Customerfirstname;

        //        param[7] = new SqlParameter();
        //        param[7].ParameterName = "@CustomerLastName";
        //        param[7].Value = criteriaSitelinkTouches.Customerlastname;

        //        param[8] = new SqlParameter();
        //        param[8].ParameterName = "@CustomerCompanyName";
        //        param[8].Value = criteriaSitelinkTouches.CustomerCompanyName;

        //        param[9] = new SqlParameter();
        //        param[9].ParameterName = "@QTRentalstatusIDs";
        //        param[9].Value = criteriaSitelinkTouches.QTRentalstatusIDs;

        //        param[10] = new SqlParameter();
        //        param[10].ParameterName = "@QTStatusID";
        //        param[10].Value = criteriaSitelinkTouches.QTStatusIDs;

        //        param[11] = new SqlParameter();
        //        param[11].ParameterName = "@QTRentalTypeID";
        //        param[11].Value = criteriaSitelinkTouches.QTRentalTypeIDs;

        //        dsOriginDetails = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "spOpApiPkrTouchSearch", param);
        //    }
        //    finally
        //    {
        //        Data.CloseConnection(conn);
        //    }

        //    return dsOriginDetails;
        //}


        ///// <summary>
        ///// insert ip request data using full table class
        ///// </summary>
        ///// <param name="requestDate"></param>
        ///// <returns></returns>
        //public int InsertIPrequestData(IPRequestData requestDate)
        //{
        //    SqlConnection conn = Data.GetPRConnection();
        //    int retValue = 0;
        //    try
        //    {
        //        SqlParameter[] param = new SqlParameter[25];
        //        param[0] = new SqlParameter();
        //        param[0].ParameterName = "@strRequestIP";
        //        param[0].Value = requestDate.strRequestIP;
        //        param[1] = new SqlParameter();
        //        param[1].ParameterName = "@strFromZip";
        //        param[1].Value = requestDate.strFromZip;
        //        param[2] = new SqlParameter();
        //        param[2].ParameterName = "@strToZip";
        //        param[2].Value = requestDate.strToZip;
        //        param[3] = new SqlParameter();
        //        param[3].ParameterName = "@strSize";
        //        param[3].Value = requestDate.strSize;
        //        param[4] = new SqlParameter();
        //        param[4].ParameterName = "@ProjectedDeliveryDate";
        //        param[4].Value = requestDate.ProjectedDeliveryDate;
        //        param[5] = new SqlParameter();
        //        param[5].ParameterName = "@WebQID";
        //        param[5].Value = requestDate.WebQID.ToString();
        //        param[6] = new SqlParameter();
        //        param[6].ParameterName = "@WCID";
        //        param[6].Value = requestDate.WCID;
        //        param[7] = new SqlParameter();
        //        param[7].ParameterName = "@LandingPageId";
        //        param[7].Value = requestDate.LandingPageId;
        //        param[8] = new SqlParameter();
        //        param[8].ParameterName = "@QuoteType";
        //        param[8].Value = requestDate.QuoteType;
        //        param[9] = new SqlParameter();
        //        param[9].ParameterName = "@TrackingPromoCode";
        //        param[9].Value = requestDate.TrackingPromoCode;
        //        param[10] = new SqlParameter();
        //        param[10].ParameterName = "@Email";
        //        param[10].Value = requestDate.Email;
        //        param[11] = new SqlParameter();
        //        param[11].ParameterName = "@LocationCode";
        //        param[11].Value = requestDate.SLLocCode;
        //        param[12] = new SqlParameter();
        //        param[12].ParameterName = "@TenantID";
        //        param[12].Value = requestDate.TenantID;
        //        param[13] = new SqlParameter();
        //        param[13].ParameterName = "@UserBanflag";
        //        param[13].Value = requestDate.UserBanFlag;
        //        param[14] = new SqlParameter();
        //        param[14].ParameterName = "@BusDevID";
        //        param[14].Value = requestDate.BusDevID;
        //        param[15] = new SqlParameter();
        //        param[15].ParameterName = "@AB_PageCode";
        //        param[15].Value = requestDate.AB_PageCode;



        //        param[16] = new SqlParameter();
        //        param[16].ParameterName = "@utm_campaign";
        //        param[16].Value = requestDate.utm_campaign;

        //        param[17] = new SqlParameter();
        //        param[17].ParameterName = "@utm_content";
        //        param[17].Value = requestDate.utm_content;

        //        param[18] = new SqlParameter();
        //        param[18].ParameterName = "@utm_medium";
        //        param[18].Value = requestDate.utm_medium;

        //        param[19] = new SqlParameter();
        //        param[19].ParameterName = "@utm_source";
        //        param[19].Value = requestDate.utm_source;

        //        param[20] = new SqlParameter();
        //        param[20].ParameterName = "@utm_term";
        //        param[20].Value = requestDate.utm_term;

        //        param[21] = new SqlParameter();
        //        param[21].ParameterName = "@CreatedBy";
        //        param[21].Value = requestDate.CreatedBy;

        //        param[22] = new SqlParameter();
        //        param[22].ParameterName = "@ToZipIsServiced";
        //        param[22].Value = requestDate.ToZipIsServiced;

        //        param[23] = new SqlParameter();
        //        param[23].ParameterName = "@FromZipIsServiced";
        //        param[23].Value = requestDate.FromZipIsServiced;

        //        param[24] = new SqlParameter();
        //        param[24].ParameterName = "@IsServiced";
        //        param[24].Value = requestDate.IsServiced;

        //        retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_iprequestdata", param);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        Data.CloseConnection(conn);
        //    }
        //    return retValue;
        //}

        #region Methods to get Capacity and save data DB for TG-450 
        
        public void TG450_LogCapacityCalendarTouchData(DataTable cAPTouchLogDetailDataTable, string locationCode)
        {
            Data.LoadSettings();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[2];
                param[0] = new SqlParameter() { ParameterName = "@CAPTouchLogDetailDataTable", Value = cAPTouchLogDetailDataTable };
                param[1] = new SqlParameter() { ParameterName = "@LocationCode", Value = locationCode };

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "TG450_SP_AddCapacityCalendarTouchData", param);
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }

        }

        public void TG450_LogTouchScheduleAvailableData(DataTable cAPTouchAvailableScheduleTable, string locationCode)
        {
            Data.LoadSettings();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[2];
                param[0] = new SqlParameter() { ParameterName = "@TouchAvailableScheduleTable", Value = cAPTouchAvailableScheduleTable };
                param[1] = new SqlParameter() { ParameterName = "@LocationCode", Value = locationCode };

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "USP_AddTouchAvailableScheduleTable_Temp", param);
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }

        }

        #endregion


    }

}
