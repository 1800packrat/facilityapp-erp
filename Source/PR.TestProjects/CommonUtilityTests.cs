﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text.RegularExpressions;

namespace PR.UtilityLibrary.Tests
{
    [TestClass()]
    public class CommonUtilityTests
    {
        [TestMethod()]
        public void GetHashTest()
        {
            var hash1 = CommonUtility.GetHash("test@1234", PREnums.HashType.SHA256_UTF8);


            var hash2 = CommonUtility.GetHash("test@1234", PREnums.HashType.SHA256_UTF8);
            var t="test@1234".Hashed();
            
            Assert.Equals(hash1, hash2);

        }

        [TestMethod()]
        public void TestSQLInjections()
        {
            var tb = "test venkat';";
            //var rgex = @"/[\t\r\n]|(--[^\r\n]*)|(\/\*[\w\W]*?(?=\*)\*\/)/gi";
            var rgex = @"((WHERE|OR)[ ]+[\(]*[ ]*([\(]*[0-9]+[\)]*)[ ]*=[ ]*[\)]*[ ]*\3)|AND[ ]+[\(]*[ ]*([\(]*1[0-9]+|[2-9][0-9]*[\)]*)[ ]*[\(]*[ ]*=[ ]*[\)]*[ ]*\4";

            var regExText = @"('(''|[^'])*')|(;)|(\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\b)";

            var matches = Regex.Matches(tb, regExText, RegexOptions.IgnoreCase);
            if (!(matches.Count > 0))
            {
                //MessageBox.Show("Invalid input.", "Text", MessageBoxButton.OK, MessageBoxImage.Error);
                //textBox1.Text = "";
            }

            //Assert.Equals();

        }

        [TestMethod()]
        public void TestS()
        {
            try
            {
                var tb = "GA Forest Park 6160";
                //var rgex = @"/[\t\r\n]|(--[^\r\n]*)|(\/\*[\w\W]*?(?=\*)\*\/)/gi";
                var rgex = @"^[0-9a-zA-Z ]+$";
                var pattern = new Regex(rgex);
                if (pattern.IsMatch(tb))
                {

                }
                var matchCase = pattern.Match(tb);
            }
            catch (Exception ex)
            {

            }
            //var matches = Regex.Matches(tb, rgex, RegexOptions.IgnoreCase);
            //if (!(matches.Count > 0))
            //{
            //    //MessageBox.Show("Invalid input.", "Text", MessageBoxButton.OK, MessageBoxImage.Error);
            //    //textBox1.Text = "";
            //}

            //Assert.Equals();

        }

        [TestMethod()]
        public void TestRegexAlphaNumWithoutSpecialChars()
        {
            try
            {
                var tb = "GA4 ., .,Forest .,Park 6160";
                
                var rgex = @"^$|^[a-zA-Z0-9 ]+([,. ][a-zA-Z0-9]?[a-zA-Z0-9]*)*$";
                var pattern = new Regex(rgex);
                if (pattern.IsMatch(tb))
                {

                }
                var matchCase = pattern.Match(tb);
            }
            catch (Exception ex)
            {

            }

        }

        [TestMethod()]
        public void TextRegexAllowAlphaNumForExTouchSeardh()
        {
            try
            {
                var tb = "Apt #5, forestpark 6160";
                var rgex = @"^$|^[a-zA-Z0-9 ]+([, #][a-zA-Z0-9]?[a-zA-Z0-9]*)*$";
                var pattern = new Regex(rgex);
                if (pattern.IsMatch(tb))
                {

                }
                var matchCase = pattern.Match(tb);
            }
            catch (Exception ex)
            {

            }
        }

        [TestMethod()]
        public void TestRegexAllowAlphaNumeric()
        {
            try
            {
                var tb = "GA456hd";
                var rgex = @"^[0-9a-zA-Z]+$";
                var pattern = new Regex(rgex);
                if (pattern.IsMatch(tb))
                {

                }
                var matchCase = pattern.Match(tb);
            }
            catch (Exception ex)
            {

            }
        }

        [TestMethod()]
        public void TestRegexAllowIntegersOnly()
        {
            try
            {
                var tb = "6160";
                var rgex = @"^[0-9]*$";
                var pattern = new Regex(rgex);
                if (pattern.IsMatch(tb))
                {

                }
                var matchCase = pattern.Match(tb);
            }
            catch (Exception ex)
            {

            }
        }

        [TestMethod()]
        public void TestRegexAllowDecimals()
        {
            try
            {
                var tb = "45.61";
                var rgex = @"^$|^((\d+)((\.\d{1,2})?))$";
                var pattern = new Regex(rgex);
                if (pattern.IsMatch(tb))
                {

                }
                var matchCase = pattern.Match(tb);
            }
            catch (Exception ex)
            {

            }
        }
    }
}