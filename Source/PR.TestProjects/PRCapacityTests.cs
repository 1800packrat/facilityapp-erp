﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace PR.BusinessLogic.Tests
{
    [TestClass()]
    public class ERP_TouchTests
    {

        [TestMethod()]
        public void CheckPassword()
        {
            try
            {
                // var EnDeKey = "P@ckr@tFac!l1tyApp"; //from config file ( Facility App)
                var EnDeKey = "R@j1b@2013"; //from config file ( Passport)

                var encPwd = "37rBLFP5CKAVg7mMwaGKAQ==";
                var decPwd = PR.UtilityLibrary.CommonUtility.Decrypt(encPwd, EnDeKey);

                //for Testing Purpose only
                encPwd = PR.UtilityLibrary.CommonUtility.Encrypt("tglass@123", EnDeKey);
                decPwd = PR.UtilityLibrary.CommonUtility.Decrypt(encPwd, EnDeKey);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        [TestMethod()]
        public void GetFacilityCapacityTest()
        {
            try
            {
                SMDBusinessLogic smd = new SMDBusinessLogic();

                DateTime startDate = Convert.ToDateTime("12/20/2019");
                DateTime endDate = Convert.ToDateTime("12/27/2019");

                var ds = smd.Get_TG733_GetFacilityCapacityData(startDate, endDate, "L157");
                ds = smd.Get_TG733_GetFacilityCapacityData(startDate, endDate, "L167");
                ds = smd.Get_TG733_GetFacilityCapacityData(startDate, endDate, "L160");
                ds = smd.Get_TG733_GetFacilityCapacityData(startDate, endDate, "L001");

                Assert.Fail();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        [TestMethod()]
        public void GetCapacityTest()
        {
            try
            {
                PRCapacity oPRCapacity = new PRCapacity();

                int numberOfDays = 90;
                DateTime startDate = Convert.ToDateTime("02/01/2020"); 
                  
                Entities.ScheduleCalendar schCalendar = null;
                schCalendar = oPRCapacity.GetCapacity_TG450("L167", startDate, numberOfDays, 0, null, null);
                schCalendar = oPRCapacity.GetCapacity_TG450("L160", startDate, numberOfDays, 0, null, null);
                schCalendar = oPRCapacity.GetCapacity_TG450("L157", startDate, numberOfDays, 0, null, null);
                schCalendar = oPRCapacity.GetCapacity_TG450("L001", startDate, numberOfDays, 0, null, null);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

    }
}