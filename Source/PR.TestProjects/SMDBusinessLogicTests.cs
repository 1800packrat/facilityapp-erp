﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using PR.Entities;
using PR.UtilityLibrary;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using PR.Entities.EsbEntities;
using System.Data;

namespace PR.BusinessLogic.Tests
{
    [TestClass()]
    public class SMDBusinessLogicTests
    {
        string corpcode = ConfigurationManager.AppSettings["corpCode"];
        string username = ConfigurationManager.AppSettings["SMDUsername"];
        string password = ConfigurationManager.AppSettings["SMDPassword"];

        [TestMethod()]
        public void GetUnitInfoForStubMethod()
        {
            try
            {
                SMDBusinessLogic smdTest = new SMDBusinessLogic();
                var dsUnits = smdTest.GetUnitInfo();
            }
            catch (Exception ex)
            {
            }

            Assert.Fail();
        }

        //////SEFRP-TODO-RMVNICD
        /////// <summary>
        /////// Test to create LDM order will billing logic in Site link
        /////// </summary>
        ////[TestMethod()]
        ////public void CreateLDMOrderWithBillingQORTest()
        ////{

        ////    Address addressOrg;
        ////    Address addressDest;
        ////    LDMFacility ldmFacOrg;
        ////    LDMFacility ldmFacDest;
        ////    SMDBusinessLogic smdTest;
        ////    int tenentIdBilling;
        ////    int tenentIdOrigin;
        ////    int tenentIdDestination;
        ////    Address addressOrgSite;
        ////    Address addressDestSite;
        ////    List<LDMUnitEntity> ldmUnitEntities = new List<LDMUnitEntity>();
        ////    //LDMUnitEntity ldmUnitEntity;
        ////    ldmUnitEntities.Add(GetLDMUnitsObject(out addressOrg, out addressDest, out ldmFacOrg, out ldmFacDest, out smdTest, out tenentIdBilling, out tenentIdOrigin, out tenentIdDestination, out addressOrgSite, out addressDestSite, true));
        ////    ldmUnitEntities.Add(GetLDMUnitsObject(out addressOrg, out addressDest, out ldmFacOrg, out ldmFacDest, out smdTest, out tenentIdBilling, out tenentIdOrigin, out tenentIdDestination, out addressOrgSite, out addressDestSite));

        ////    var watch = System.Diagnostics.Stopwatch.StartNew();

        ////    var ldmResponse = smdTest.CreateLDMOrderWithBillingQOR(

        ////                                             "L167",  //"L499",
        ////                                             "L001",
        ////                                             "L499",

        ////                                             "UnitTest",
        ////                                             "UnitTest", tenentIdOrigin, tenentIdDestination, tenentIdBilling, "27587", "94539",
        ////                                             addressOrgSite,
        ////                                             addressDestSite,
        ////                                             addressOrg, addressDest,
        ////                                             ldmFacOrg,
        ////                                             ldmFacDest,
        ////                                             "Individual Referral",
        ////                                             "Sushil Fund", ""

        ////                                             , "976894",
        ////                                             "http://stars.1800packrat.com/M02QuoteTransferLDMUnit.aspx?QuoteId=671532",
        ////                                            string.Empty,
        ////                                             ldmUnitEntities
        ////                                             );

        ////    watch.Stop();
        ////    var elapsedMs = watch.ElapsedMilliseconds;

        ////    Assert.IsTrue(ldmResponse.First().IsBillingUnit);
        ////    Assert.IsNotNull(ldmResponse.First().OriginSLQORId);
        ////    Assert.IsNotNull(ldmResponse.First().DestinationSLQORId);
        ////    Assert.IsNotNull(ldmResponse.First().BillingSLQORId);
        ////    foreach (var ldmQORIDs in ldmResponse)
        ////    {
        ////        Console.WriteLine(string.Format("OriginSLQORId : {0}  :: DestinationSLQORId : {1}  :: BillingSLQORId : {2}  ::  IsFirstUnit :  {3} :: Method Execution Time PraallelTask: {4}", ldmQORIDs.OriginSLQORId, ldmQORIDs.DestinationSLQORId, ldmQORIDs.BillingSLQORId, ldmQORIDs.IsBillingUnit, elapsedMs));
        ////    }

        ////}


        //////SEFRP-TODO-RMVNICD
        ////[TestMethod()]
        ////public void CreateLDMOrderWithBillingQORParallelTaskTest()
        ////{

        ////    Address addressOrg;
        ////    Address addressDest;
        ////    LDMFacility ldmFacOrg;
        ////    LDMFacility ldmFacDest;
        ////    SMDBusinessLogic smdTest;
        ////    int tenentIdBilling;
        ////    int tenentIdOrigin;
        ////    int tenentIdDestination;
        ////    Address addressOrgSite;
        ////    Address addressDestSite;
        ////    List<LDMUnitEntity> ldmUnitEntities = new List<LDMUnitEntity>();
        ////    //LDMUnitEntity ldmUnitEntity;
        ////    ldmUnitEntities.Add(GetLDMUnitsObject(out addressOrg, out addressDest, out ldmFacOrg, out ldmFacDest, out smdTest, out tenentIdBilling, out tenentIdOrigin, out tenentIdDestination, out addressOrgSite, out addressDestSite, true));
        ////    ldmUnitEntities.Add(GetLDMUnitsObject(out addressOrg, out addressDest, out ldmFacOrg, out ldmFacDest, out smdTest, out tenentIdBilling, out tenentIdOrigin, out tenentIdDestination, out addressOrgSite, out addressDestSite));

        ////    var watch = System.Diagnostics.Stopwatch.StartNew();

        ////    var ldmResponse = smdTest.CreateLDMOrderWithBillingQORParallelTask(

        ////                                             "L167",  //"L499",
        ////                                             "L001",
        ////                                             "L499",

        ////                                             "UnitTest",
        ////                                             "UnitTest", tenentIdOrigin, tenentIdDestination, tenentIdBilling, "27587", "94539",
        ////                                             addressOrgSite,
        ////                                             addressDestSite,
        ////                                             addressOrg, addressDest,
        ////                                             ldmFacOrg,
        ////                                             ldmFacDest,
        ////                                             "Individual Referral",
        ////                                             "Sushil Fund", ""

        ////                                             , "976894",
        ////                                             "http://stars.1800packrat.com/M02QuoteTransferLDMUnit.aspx?QuoteId=671532",
        ////                                            string.Empty,
        ////                                             ldmUnitEntities
        ////                                             );

        ////    watch.Stop();
        ////    var elapsedMs = watch.ElapsedMilliseconds;

        ////    Assert.IsTrue(ldmResponse.First().IsBillingUnit);
        ////    Assert.IsNotNull(ldmResponse.First().OriginSLQORId);
        ////    Assert.IsNotNull(ldmResponse.First().DestinationSLQORId);
        ////    Assert.IsNotNull(ldmResponse.First().BillingSLQORId);
        ////    foreach (var ldmQORIDs in ldmResponse)
        ////    {
        ////        Console.WriteLine(string.Format("OriginSLQORId : {0}  :: DestinationSLQORId : {1}  :: BillingSLQORId : {2}  ::  IsFirstUnit :  {3}  :: Method Execution Time PraallelTask: {4}", ldmQORIDs.OriginSLQORId, ldmQORIDs.DestinationSLQORId, ldmQORIDs.BillingSLQORId, ldmQORIDs.IsBillingUnit, elapsedMs));
        ////    }

        ////}


        ////SEFRP-TODO-RMVNICD - TBD
        //private LDMUnitEntity GetLDMUnitsObject(
        //                                        out Address addressOrg,
        //                                        out Address addressDest,
        //                                        out LDMFacility ldmFacOrg,
        //                                        out LDMFacility ldmFacDest,
        //                                        out SMDBusinessLogic smdTest,
        //    out int tenentIdBilling, out int tenentIdOrigin,
        //    out int tenentIdDestination, out Address addressOrgSite,
        //    out Address addressDestSite,

        //    bool isBillingunit = false)
        //{

        //    addressOrg = new Address()
        //    {
        //        AddressLine1 = "11640 North Park Drive",
        //        City = "Wake Forest",
        //        Country = "USA",
        //        PhoneNumber = "(344) 4444444",
        //        State = "NC",
        //        Zip = "27587"

        //    };
        //    addressDest = new Address()
        //    {
        //        AddressLine1 = "Walnut Creek Corporate Center, 9101 Wall St.",
        //        City = "Austin",
        //        Country = "USA",
        //        PhoneNumber = "(344) 4444444",
        //        State = "TX",
        //        Zip = "78754"

        //    };

        //    ldmFacOrg = new LDMFacility();

        //    ldmFacDest = new LDMFacility();

        //    string corpcode = ConfigurationManager.AppSettings["corpCode"];
        //    string username = ConfigurationManager.AppSettings["SMDUsername"];

        //    string password = ConfigurationManager.AppSettings["SMDPassword"];

        //    smdTest = new SMDBusinessLogic(corpcode, username, password);



        //    int BillingPromoGlobalNum = Convert.ToInt32(ConfigurationManager.AppSettings["LDMOrigNB"]);

        //    ////Commented by Sohan
        //    //SLAPIMobile slapiMobile = new SLAPIMobile(true, (new Guid()).ToString());

        //    // LDMUnits ldmUnit = new LDMUnits() ;
        //    Promotions promo = null; // smdTest.GetPromotions(corpcode, username, password);
        //    /// Get unit IDs for Billing , origin , dest
        //    var unitIdBilling = smdTest.GetUnitIdByUnitNameFromSL(corpcode, "L499", username, password, "000016"); // 000008, 000012 .etc   //Billing 
        //    var unitIdOrigin = smdTest.GetUnitIdByUnitName(corpcode, "L167", username, password, "000016"); // 000008, 000012 .etc   // Origin
        //    var unitIdDestination = smdTest.GetUnitIdByUnitName(corpcode, "L001", username, password, "000016"); // 000008, 000012 .etc   // Destination


        //    //SEFRP-TODO-RMVNICD
        //    tenentIdBilling = 0;
        //    tenentIdOrigin = 0;
        //    tenentIdDestination = 0;
        //    ///// Get tenent IDs for Billing , origin , dest
        //    //tenentIdBilling = (int)smdTest.AddNewCustomer(corpcode, "L499", username, password, "", "unittest", "", "unittest", "", "Billing Address", "apt #1", "Morrisville", "NC", "27560", "4444444444", "", "", "test@test.com", "", "", "", false, false, DateTime.MinValue, "", "", "", "").Tables["RT"].Rows[0]["TenantID"];//Billing 
        //    //tenentIdOrigin = (int)smdTest.AddNewCustomer(corpcode, "L167", username, password, "", "unittest", "", "unittest", "", "Origin Address", "apt #1", "Morrisville", "NC", "27560", "4444444444", "", "", "test@test.com", "", "", "", false, false, DateTime.MinValue, "", "", "", "").Tables["RT"].Rows[0]["TenantID"];//Origin 
        //    //tenentIdDestination = (int)smdTest.AddNewCustomer(corpcode, "L001", username, password, "", "unittest", "", "unittest", "", "Dest Address", "apt #1", "Morrisville", "NC", "94539", "4444444444", "", "", "test@test.com", "", "", "", false, false, DateTime.MinValue, "", "", "", "").Tables["RT"].Rows[0]["TenantID"];//Destination 


        //    //Get Address 
        //    addressOrgSite = smdTest.GetSiteInformation(corpcode, "L167", username, password).SiteAddress;
        //    addressDestSite = smdTest.GetSiteInformation(corpcode, "L001", username, password).SiteAddress;
        //    var addressBillingSite = smdTest.GetSiteInformation(corpcode, "L499", username, password).SiteAddress;




        //    var ldmUnitEntity = new LDMUnitEntity();
        //    ldmUnitEntity.OriginSLUnitID = unitIdOrigin;
        //    ldmUnitEntity.DestinationSLUnitID = unitIdDestination;
        //    ldmUnitEntity.BillingSLUnitID = unitIdBilling;

        //    //ldmUnitEntity.IsLDMBillingUnit = true;
        //    ldmUnitEntity.StarsUnitID = 110998;
        //    ldmUnitEntity.RecurringItems = new List<LDMPricingEntity>();
        //    ldmUnitEntity.BillingPromoCode = BillingPromoGlobalNum;
        //    ldmUnitEntity.NonRecurringItems = new List<LDMPricingEntity>();
        //    ldmUnitEntity.MarchandisItems = new List<LDMPricingEntity>();
        //    ldmUnitEntity.IsUnitAtOrigin = true;
        //    List<LDMTouchesEntity> touches = new List<LDMTouchesEntity>();


        //    //POS item


        //    if (isBillingunit)
        //    {
        //        ldmUnitEntity.MarchandisItems.Add(new LDMPricingEntity() { Price = 44.99m, ItemDescription = "Moving Kit", LDMPricingEntityName = "Moving Kit", Quantity = 1 });

        //        //Non recurring Item
        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 25.00m, ItemDescription = "LDM - Change Fee", LDMPricingEntityName = "LDM - Change Fee", Quantity = 1 });
        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 50m, ItemDescription = "LDM - Next\\Same Day Delv", LDMPricingEntityName = "LDM - Next\\Same Day Delv", Quantity = 1 });
        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 100.00m, ItemDescription = "LDM - Sunday Delv", LDMPricingEntityName = "LDM - Sunday Delv", Quantity = 1 });

        //        //LDM - Sunday Delv

        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 249.00m, ItemDescription = "LDM - Weight Ticket", LDMPricingEntityName = "LDM - Weight Ticket", Quantity = 1 });

        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 249.00m, ItemDescription = "LDM - Addl Touch", LDMPricingEntityName = "LDM - Addl Touch", Quantity = 1 });

        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 249.00m, ItemDescription = "Blankets", LDMPricingEntityName = "Blankets", Quantity = 1 });
        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 249.00m, ItemDescription = "LDM - FullPackRat Move", LDMPricingEntityName = "LDM - FullPackRat Move", Quantity = 1 });



        //        ldmUnitEntity.IsLDMBillingUnit = true;

        //        ldmUnitEntity.UnitPriceOrigin = 0m;

        //        ldmUnitEntity.UnitPriceDestination = 0m;

        //        touches.Add(new LDMTouchesEntity() { TouchScheduleDate = DateTime.Now, TouchType = UtilityLibrary.PREnums.TouchType.OutByOwner, TouchScheduleTime = "AM" });
        //        touches.Add(new LDMTouchesEntity() { TouchScheduleDate = DateTime.Now, TouchType = UtilityLibrary.PREnums.TouchType.InByOwner, TouchScheduleTime = "AM" });
        //    }
        //    else
        //    {
        //        ldmUnitEntity.PromoCodeOrigin = Convert.ToInt32(ConfigurationManager.AppSettings["LDMORIGBase"]);
        //        ldmUnitEntity.PromoCodeDestination = Convert.ToInt32(ConfigurationManager.AppSettings["LDMDEST"]);
        //        //ldmUnitEntity.BillingPromoCode = Convert.ToInt32(ConfigurationManager.AppSettings["LDMOrigNB"]);
        //        ldmUnitEntity.UnitPriceOrigin = 209.00m;

        //        ldmUnitEntity.UnitPriceDestination = 189.00m;

        //        // Recurring item
        //        ldmUnitEntity.RecurringItems.Add(new LDMPricingEntity() { Price = 4.00m, ItemDescription = "Per Prop Recovery Fee", LDMPricingEntityName = "Per Prop Recovery Fee", Quantity = 1 });
        //        ldmUnitEntity.RecurringItems.Add(new LDMPricingEntity() { Price = 2.12m, ItemDescription = "Regulatory License Fee", LDMPricingEntityName = "Regulatory License Fee", Quantity = 1 });
        //        ldmUnitEntity.RecurringItems.Add(new LDMPricingEntity() { Price = 44.99m, ItemDescription = "CPP", LDMPricingEntityName = "CPP", Quantity = 1 });
        //        //Non recurring Item
        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 25.00m, ItemDescription = "LDM - Change Fee", LDMPricingEntityName = "LDM - Change Fee", Quantity = 1 });
        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 50m, ItemDescription = "LDM - Next\\Same Day Delv", LDMPricingEntityName = "LDM - Next\\Same Day Delv", Quantity = 1 });
        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 100.00m, ItemDescription = "LDM - Sunday Delv", LDMPricingEntityName = "LDM - Sunday Delv", Quantity = 1 });

        //        //LDM - Sunday Delv

        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 249.00m, ItemDescription = "LDM - Weight Ticket", LDMPricingEntityName = "LDM - Weight Ticket", Quantity = 1 });

        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 249.00m, ItemDescription = "LDM - Addl Touch", LDMPricingEntityName = "LDM - Addl Touch", Quantity = 1 });

        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 249.00m, ItemDescription = "Blankets", LDMPricingEntityName = "Blankets", Quantity = 1 });
        //        ldmUnitEntity.NonRecurringItems.Add(new LDMPricingEntity() { Price = 249.00m, ItemDescription = "LDM - FullPackRat Move", LDMPricingEntityName = "LDM - FullPackRat Move", Quantity = 1 });



        //        touches.Add(new LDMTouchesEntity() { TouchScheduleDate = DateTime.Now, TouchType = UtilityLibrary.PREnums.TouchType.DeliverEmpty, TouchScheduleTime = "AM" });
        //        touches.Add(new LDMTouchesEntity() { TouchScheduleDate = DateTime.MinValue, TouchType = UtilityLibrary.PREnums.TouchType.ReturnFull, TouchScheduleTime = "AM" });
        //        touches.Add(new LDMTouchesEntity() { TouchScheduleDate = DateTime.MinValue, TouchType = UtilityLibrary.PREnums.TouchType.LDMTransferOut, TouchScheduleTime = "AM" });
        //        touches.Add(new LDMTouchesEntity() { TouchScheduleDate = DateTime.MinValue, TouchType = UtilityLibrary.PREnums.TouchType.LDMTransferIn, TouchScheduleTime = "AM" });
        //        touches.Add(new LDMTouchesEntity() { TouchScheduleDate = DateTime.MinValue, TouchType = UtilityLibrary.PREnums.TouchType.DeliverFull, TouchScheduleTime = "AM" });
        //        touches.Add(new LDMTouchesEntity() { TouchScheduleDate = DateTime.MinValue, TouchType = UtilityLibrary.PREnums.TouchType.ReturnEmpty, TouchScheduleTime = "AM" });
        //    }
        //    // LDMTouchesEntity ldmTouch = new LDMTouchesEntity() { TouchScheduleDate = DateTime.MinValue, TouchType = UtilityLibrary.PREnums.TouchType.DeliverEmpty, TouchScheduleTime = "Any Time" };
        //    ldmUnitEntity.LDMToucheItems = touches;

        //    return ldmUnitEntity;



        //}



        //////SEFRP-TODO-RMVNICD
        /////// <summary>
        /////// test to add LDM unit to existing order 
        /////// </summary>
        ////[TestMethod()]
        ////public void LDMAddUintToExistingOrderTest()
        ////{



        ////    Address addressOrg;
        ////    Address addressDest;
        ////    LDMFacility ldmFacOrg;
        ////    LDMFacility ldmFacDest;
        ////    SMDBusinessLogic smdTest;
        ////    int tenentIdBilling;
        ////    int tenentIdOrigin;
        ////    int tenentIdDestination;
        ////    Address addressOrgSite;
        ////    Address addressDestSite;
        ////    List<LDMUnitEntity> ldmUnitEntities = new List<LDMUnitEntity>();
        ////    //LDMUnitEntity ldmUnitEntity;
        ////    ldmUnitEntities.Add(GetLDMUnitsObject(out addressOrg, out addressDest, out ldmFacOrg, out ldmFacDest, out smdTest, out tenentIdBilling, out tenentIdOrigin, out tenentIdDestination, out addressOrgSite, out addressDestSite, true));
        ////    ldmUnitEntities.Add(GetLDMUnitsObject(out addressOrg, out addressDest, out ldmFacOrg, out ldmFacDest, out smdTest, out tenentIdBilling, out tenentIdOrigin, out tenentIdDestination, out addressOrgSite, out addressDestSite));

        ////    ldmUnitEntities.Where(x => x.IsLDMBillingUnit == true).Single().DummyUnitBillingQORID = 6705104;

        ////    var ldmResponse = smdTest.LDMAddUintToExistingOrder(

        ////                                             "L167",  //"L499",
        ////                                             "L513",
        ////                                             "L499",

        ////                                             "UnitTest",
        ////                                             "UnitTest", tenentIdOrigin, tenentIdDestination, tenentIdBilling, "27587", "94539",
        ////                                             addressOrgSite,
        ////                                             addressDestSite,
        ////                                             addressOrg, addressDest,
        ////                                             ldmFacOrg,
        ////                                             ldmFacDest,
        ////                                             "Individual Referral",
        ////                                             "Sushil Fund", ""

        ////                                             , "976894",
        ////                                             "http://stars.1800packrat.com/M02QuoteTransferLDMUnit.aspx?QuoteId=671532",
        ////                                            (new Guid()).ToString(),
        ////                                            ldmUnitEntities
        ////                                             );

        ////    // Assert.IsTrue(ldmResponse.IsFirstUnit);
        ////    Assert.IsNotNull(ldmResponse.OriginSLQORId);
        ////    Assert.IsNotNull(ldmResponse.DestinationSLQORId);
        ////    Assert.IsNotNull(ldmResponse.BillingSLQORId);

        ////    Console.WriteLine(string.Format("OriginSLQORId : {0}  :: DestinationSLQORId : {1}  :: BillingSLQORId : {2}  ::  IsFirstUnit :  {3}", ldmResponse.OriginSLQORId, ldmResponse.DestinationSLQORId, ldmResponse.BillingSLQORId));

        ////}


        ////////SEFRP-TODO-RMVNICD
        /////// <summary>
        /////// Test to get list of non recurring items
        /////// </summary>
        ////[TestMethod()]
        ////public void GetNonRecurringItemsTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic();
        ////    string corpcode = ConfigurationManager.AppSettings["corpCode"];
        ////    string username = ConfigurationManager.AppSettings["SMDUsername"];
        ////    LocalComponent local = new LocalComponent();
        ////    string password = ConfigurationManager.AppSettings["SMDPassword"];

        ////    int auditLogId = 0;
        ////    //Create new Billing QOR record
        ////    DataSet tenentIdBilling = smdTest.AddNewCustomer(corpcode, "L499", username, password, "", "unittest", "", "unittest", "", "Billing Address", "apt #1", "Morrisville", "NC", "27560", "4444444444", "", "", "test@test.com", "", "", "", false, false, DateTime.MinValue, "", "", "", "");//Billing 
        ////    DataSet dsRental = new DataSet();
        ////    QTRentalItem qtRentalItem = new QTRentalItem();
        ////    SLAPIMobile slapiMobile = new SLAPIMobile(true, (new Guid()).ToString());
        ////    auditLogId = local.InsertSLAuditLog("L_NewQOR", String.Empty, String.Empty, DateTime.Now);
        ////    DataSet ds = slapiMobile.L_NewQOR(corpcode, "L499", username, password, eMoveInUsedFor.MOBILE_NEW_RESERVATION, (int)tenentIdBilling.Tables["RT"].Rows[0]["TenantID"], "27587", "94539", String.Empty);
        ////    local.UpdateSLAuditLog(auditLogId, DateTime.Now);



        ////    var response = smdTest.GetNonRecurringItems(slapiMobile);
        ////    Assert.IsNotNull(response);
        ////    Assert.IsNotNull(response.SingleOrDefault(x => x.ItemDescription == "Administrative Fee"));
        ////}



        //////////SEFRP-TODO-RMVNICD
        /////// <summary>
        /////// Get list of QOR details
        /////// </summary>
        ////[TestMethod()]
        ////public void GetQORListTest()
        ////{

        ////    //SMDBusinessLogic smdTest = new SMDBusinessLogic();


        ////    LocalComponent local = new LocalComponent();

        ////    //string billingLocationCode = "L499";
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);

        ////    //int auditLogId = 0;
        ////    ////Create new Billing QOR record
        ////    //int tenentIdBilling = (int)smdTest.AddNewCustomer(corpcode, "L499", username, password, "", "unittest", "", "unittest", "", "Billing Address", "apt #1", "Morrisville", "NC", "27560", "4444444444", "", "", "test@test.com", "", "", "", false, false, DateTime.MinValue, "", "", "", "").Tables["RT"].Rows[0]["TenantID"];//Billing 




        ////    var response = smdTest.GetQORList(corpcode, "", username, password, 0, "", 0, "681846");
        ////    Assert.Fail();
        ////}



        ////////SEFRP-TODO-RMVNICD
        ////[TestMethod()]
        ////public void UpdateLDMPriceForTransportaionBillingQORTest()
        ////{


        ////    string corpcode = ConfigurationManager.AppSettings["corpCode"];
        ////    string username = ConfigurationManager.AppSettings["SMDUsername"];

        ////    string password = ConfigurationManager.AppSettings["SMDPassword"];

        ////    int BillingPromoGlobalNum = Convert.ToInt32(ConfigurationManager.AppSettings["BillingPromoGlobalNum"]);
        ////    int BillingPromoGlobalNumUnBundled = Convert.ToInt32(ConfigurationManager.AppSettings["BillingPromoGlobalNumUnBundled"]);

        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password, "sfund");




        ////    List<LDMUnitEntity> ldmUnits = new List<LDMUnitEntity>();

        ////    //Dummy unit
        ////    LDMUnitEntity ldmUnitEntityDummy = new LDMUnitEntity();

        ////    ldmUnitEntityDummy.DummyUnitBillingQORID = 6032341; //charges update with slapi object 6032215;  // charge update with service 6031969
        ////    ldmUnitEntityDummy.IsLDMBillingUnit = true;

        ////    ldmUnitEntityDummy.NonRecurringItems = new List<LDMPricingEntity>();

        ////    ldmUnitEntityDummy.MarchandisItems = new List<LDMPricingEntity>();




        ////    //Non recurring Item
        ////    // ldmUnitEntityDummy.NonRecurringItems.Add(new LDMPricingEntity() { Price = 1000.00m, ItemDescription = @"LDM - Excess Mileage", Quantity = 1 });
        ////    ldmUnitEntityDummy.NonRecurringItems.Add(new LDMPricingEntity() { Price = 100.98m, ItemDescription = "LDM - Full PackRat Move", Quantity = 1 });
        ////    ldmUnitEntityDummy.NonRecurringItems.Add(new LDMPricingEntity() { Price = 100.00m, ItemDescription = "Administrative Fee", Quantity = 1 });

        ////    //  ldmUnitEntityDummy.NonRecurringItems.Add(new LDMPricingEntity() { Price = 23562.0m, ItemDescription = "Outstanding Rent Balance", Quantity = 1 });

        ////    ldmUnits.Add(ldmUnitEntityDummy);



        ////    // actual unit 
        ////    LDMUnitEntity ldmUnitEntityUnit = new LDMUnitEntity();

        ////    //POS item Merchandise
        ////    ldmUnitEntityUnit.BillingQORIDOfUnit = 6028549;
        ////    ldmUnitEntityUnit.IsLDMBillingUnit = false;

        ////    //  ldmUnitEntityUnit.RecurringItems = new List<LDMPricingEntity>();

        ////    //  ldmUnitEntityUnit.NonRecurringItems = new List<LDMPricingEntity>();

        ////    //    ldmUnitEntityUnit.MarchandisItems = new List<LDMPricingEntity>();

        ////    //Non recurring Item

        ////    // ldmUnitEntityUnit.RecurringItems.Add(new LDMPricingEntity() { Price = 75.00m, ItemDescription = @"Blankets", Quantity = 1 });
        ////    // ldmUnitEntityUnit.NonRecurringItems.Add(new LDMPricingEntity() { Price = 3000.00m, ItemDescription = "LDM - Full PackRat Move", Quantity = 1 });

        ////    //ldmUnitEntityUnit.MarchandisItems.Add(new LDMPricingEntity() { Price = 20.29m, ItemDescription = "O-Prem Del - Next Day", Quantity = 1 });

        ////    //ldmUnitEntityUnit.MarchandisItems.Add(new LDMPricingEntity() { Price = 7.0m, ItemDescription = "O-Prem Del - Guarantee AM-PM", Quantity = 1 });
        ////    //ldmUnitEntityUnit.RecurringItems.Add(new LDMPricingEntity() { Price = 20.00m, ItemDescription = @"Blankets", Quantity = 1 });
        ////    //ldmUnitEntityUnit.RecurringItems.Add(new LDMPricingEntity() { Price = 75.00m, ItemDescription = @"CPP", Quantity = 1 });
        ////    ldmUnitEntityUnit.IsBundledQuote = true;
        ////    ldmUnitEntityUnit.BillingPromoCode = (ldmUnitEntityUnit.IsBundledQuote) ? BillingPromoGlobalNum : BillingPromoGlobalNumUnBundled;

        ////    //ldmUnitEntityUnit.NonRecurringItems.Add(new LDMPricingEntity() { Price = 712.73m, ItemDescription = "Administrative Fee", Quantity = 1 });


        ////    // ldmUnits.Add(ldmUnitEntityUnit);

        ////    //  LDMUnitEntity ldmUnitEntityUnitRecurring = new LDMUnitEntity();

        ////    ////POS item Merchandise
        ////    //  ldmUnitEntityUnitRecurring.BillingQORIDOfUnit = 6717231;
        ////    //  ldmUnitEntityUnitRecurring.IsLDMBillingUnit = false;

        ////    //  ldmUnitEntityUnitRecurring.RecurringItems = new List<LDMPricingEntity>();

        ////    //  ldmUnitEntityUnitRecurring.NonRecurringItems = new List<LDMPricingEntity>();

        ////    //  ldmUnitEntityUnitRecurring.MarchandisItems = new List<LDMPricingEntity>();

        ////    ////Non recurring Item

        ////    //  ldmUnitEntityUnitRecurring.RecurringItems.Add(new LDMPricingEntity() { Price = 75.00m, ItemDescription = "CPP : DW and $15,000", Quantity = 1 });

        ////    //  ldmUnits.Add(ldmUnitEntityUnitRecurring);


        ////    // smdTest.UpdateLDMPriceForBillingQORChargesAllByLedgerID("L499", new Guid().ToString(), ldmUnits.FirstOrDefault());
        ////    smdTest.UpdateLDMPriceForTransportaionBillingQOR("L499", new Guid().ToString(), ldmUnits, "testUser");
        ////    //                                        "L499",


        ////    //                                         "UnitTest",
        ////    //                                         "UnitTest",
        ////    //                                         ldmUnitEntity.StarsUnitID.ToString(),
        ////    //                                       ldmUnitEntity,0,0.0m  
        ////    //                                         );

        ////    //Assert.IsTrue(ldmResponse.IsFirstUnit);
        ////    //Assert.IsNotNull(ldmResponse.OriginSLQORId);
        ////    //Assert.IsNotNull(ldmResponse.DestinationSLQORId);
        ////    //Assert.IsNotNull(ldmResponse.BillingSLQORId);

        ////    //Console.WriteLine(string.Format("OriginSLQORId : {0}  :: DestinationSLQORId : {1}  :: BillingSLQORId : {2}  ::  IsFirstUnit :  {3}", ldmResponse.OriginSLQORId, ldmResponse.DestinationSLQORId, ldmResponse.BillingSLQORId, ldmResponse.IsFirstUnit));

        ////}

        ///// <summary>
        ///// TODO: This method to load DATAmigration excel 
        ///// </summary>
        ///// <returns></returns>
        //public dynamic loadXSL()
        //{
        //    var book = new ExcelQueryFactory("pathToExcelFile");
        //    book.Worksheet();

        //    return null;

        //}

        ////////SEFRP-TODO-RMVNICD
        ////[TestMethod()]
        ////public void UpdateLDMUnitRentToSiteLinkTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        ////    smdTest.UpdateLDMUnitRentToSiteLink(6002125, 500.00m, Convert.ToInt32(ConfigurationManager.AppSettings["BillingPromoGlobalNum"]));

        ////}


        //////SEFRP-TODO-CTRMV
        ////[TestMethod()]
        ////public void GetPaymentInfoDataTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        ////    var t = smdTest.GetPaymentInfoData("L499", 6700811);
        ////    Assert.Fail();
        ////}

        //////SEFRP-TODO-CTRMV
        ////[TestMethod()]
        ////public void SavePaymentInfoDataTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        ////    var t = smdTest.SavePaymentInfoData(1, PREnums.PaymentTypesSiteLinkEnums.Visa, "4351676027356264", new DateTime(2021, 11, 1), "GIFT CARD RECIPIENT", "address 111", "34234", "L499", 6700811);
        ////    Assert.Fail();
        ////}

        //////SEFRP-TODO-RMVNICD
        ////[TestMethod()]
        ////public void CheckTenantBalance()
        ////{
        ////    DataSet dsAccountBalance = new DataSet();
        ////    TenantBalance tenantBalance = new TenantBalance();
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        ////    int tenantId = 452;
        ////    int unitId = 0;
        ////    int qorid = 6707125;
        ////    int qtRentalId = 0;
        ////    string locationCode = string.Empty;

        ////    var dsQORList = smdTest.GetQORList(corpcode, String.Empty, username, password, 0, String.Empty, qorid, String.Empty);
        ////    if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
        ////    {
        ////        tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
        ////        unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
        ////        qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
        ////        locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();
        ////    }

        ////    smdTest.CheckTenantBalance(corpcode, locationCode, username, password, tenantId, unitId, 1);
        ////}

        //////SEFRP-TODO-RMVNICD
        ////[TestMethod()]
        ////public void PayByCreditCard()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        ////    int tenantId = 452;
        ////    int unitId = 0;
        ////    int qorid = 6706894;
        ////    int qtRentalId = 0;
        ////    string locationCode = string.Empty;

        ////    var dsQORList = smdTest.GetQORList(corpcode, String.Empty, username, password, 0, String.Empty, qorid, String.Empty);
        ////    if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
        ////    {
        ////        tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
        ////        unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
        ////        qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
        ////        locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();
        ////    }

        ////    var t = smdTest.PayByCreditCard(corpcode, "L499", username, password, tenantId, unitId,
        ////    500, PREnums.CreditCardPaymentType.VISA, "4111111111111111", "123", DateTime.Now.AddMonths(5),
        ////    "Test A", "MIG-II", "27587", true, PREnums.SourceOfPayment.CallCenter);

        ////    Assert.Fail();
        ////}

        //[TestMethod()]
        //public void TransferUnitFromOrigToDestForLDM()
        //{
        //    //SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        //    //int tenantId = 240051;
        //    //int unitId = 9982;
        //    //smdTest.MoveInForLDM(6011438, "354007", "L102", "354007", Guid.NewGuid().ToString());

        //    //Assert.Fail();

        //}

        //[TestMethod()]
        //public void ReservationFeeAddWithSourceForMobileStorage()
        //{
        //    int qorid = 6706894;
        //    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        //    //CallCenterWs service = new CallCenterWs();

        //    ////SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        //    //int tenantId = 452;
        //    //int unitId = 0;
        //    ////int qorid = 6706894;
        //    //int qtRentalId = 0;
        //    //string locationCode = string.Empty;

        //    //var dsQORList = smdTest.GetQORList(corpcode, String.Empty, username, password, 0, String.Empty, qorid, String.Empty);
        //    //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
        //    //{
        //    //    tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
        //    //    unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
        //    //    qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
        //    //    locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();
        //    //}

        //    ////PREnums.CreditCardPaymentType.VISA, "4111111111111111", "123", DateTime.Now.AddMonths(5),
        //    ////"Test A", "MIG-II", "27587", true, PREnums.SourceOfPayment.CallCenter
        //    //var rtDs = service.ReservationFeeAddWithSourceForMobileStorage(corpcode, locationCode, username, password, tenantId, qtRentalId,(int)PREnums.CreditCardPaymentType.VISA
        //    //    , "4111111111111111", "123", DateTime.Now.AddMonths(5), "Venkat Test", "Mig 2", "27587", true, (int)PREnums.SourceOfPayment.CallCenter);

        //    //DataSet rtPaymentHistory = smdTest.ShowPaymentHistory(corpcode, qorid);

        //    //  DataSet rtPaymentHistory = smdTest.ReservationFeeAddWithSourceForMobileStorage(corpcode, username, password);

        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ShowPaymentHistory()
        //{
        //    //int qorid = 6707125;
        //    //SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);

        //    //DataSet rtPaymentHistory = smdTest.ShowPaymentHistory(corpcode, qorid);

        //    //Assert.Fail();
        //}

        ////////SEFRP-TODO-RMVNICD
        ////[TestMethod()]
        ////public void GetBillingInfoByTenantIDTest()
        ////{

        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        ////    int tenantId = 452;
        ////    int unitId = 0;
        ////    int qorid = 6707125;
        ////    int qtRentalId = 0;
        ////    string locationCode = string.Empty;

        ////    var dsQORList = smdTest.GetQORList(corpcode, String.Empty, username, password, 0, String.Empty, qorid, String.Empty);
        ////    if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
        ////    {
        ////        tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
        ////        unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
        ////        qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
        ////        locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

        ////        DataSet retBillingInfo = smdTest.GetBillingInfoByTenantID(locationCode, tenantId, qorid);
        ////    }

        ////    Assert.Fail();
        ////}

        //[TestMethod()]
        //public void DeleteLDMOrderTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void UpdateLDMUnitRentToSiteLinkTest1()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void UpdateLDMPriceForBillingQORTest()
        //{

        //}


        //[TestMethod()]
        //public void GetUnitDetails()
        //{

        //    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        //    smdTest.GetUnitIdByUnitNameFromSL(corpcode, "L499", username, password, "A4DAB");
        //}

        //////// SFERP-TODO-CTRMV-- Remove this method
        ////[TestMethod()]
        ////public void RemoveAppliedPromotionsForUnbundledQuoteTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        ////    smdTest.AddRemoveAppliedPromotionsForOriginQorid(6008240, "L506", 1318691427, (new Guid()).ToString());

        ////}

        ////[TestMethod()]
        ////public void RemoveAppliedPromotionsForBundledQuoteTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        ////    smdTest.AddRemoveAppliedPromotionsForOriginQorid(6008240, "L506", 561541189, (new Guid()).ToString());
        ////}

        //////SEFRP-TODO-RMVNICD -- TG-619
        ////[TestMethod()]
        ////public void UpdateTouchInstructionTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        ////    SLAPIMobile slapi = smdTest.EditQuote(corpcode, "L499", username, password, 6009133, 621, (new Guid()).ToString());


        ////}


        //////SEFRP-TODO-RMVNICD
        ////[TestMethod()]
        ////public void UpdateNonRecItemsDateForBillingQOR()
        ////{
        ////    //6009127
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        ////    smdTest.UpdateNonRecItemsDateForBillingQOR(6009127, (new Guid()).ToString());
        ////}

        ////[TestMethod()]
        ////public void GetScheduleCalendarTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);

        ////    var test = smdTest.GetScheduleCalendar(corpcode, "L167", username, password, DateTime.Today, 30, 0);
        ////    Assert.Fail();
        ////}



        //#region TouchUpdates Test
        //[TestMethod()]
        //public void UpdateLDMTouchInformationTestDeliverEmpty()
        //{
        //    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        //    Address fromAddress = new Address() { AddressLine1 = "2315 Atlantic Ave", AddressLine2 = "", City = "Raleigh", State = "NC", Zip = "27604" };
        //    Address toAddress = new Address() { AddressLine1 = "TestDE", AddressLine2 = "TestDE", City = "Morrisville", State = "NC", Zip = "27560" };

        //    Address siteAddress = new Address() { AddressLine1 = "2315 Atlantic Ave", AddressLine2 = "", City = "Raleigh", State = "NC", Zip = "27604" };


        //    var response = smdTest.UpdateLDMTouchInformation(6016124, "sfund", Guid.NewGuid().ToString(), fromAddress, toAddress, "L163", 7.0m, PREnums.TouchType.DeliverEmpty, 0);

        //    Assert.IsTrue(string.IsNullOrWhiteSpace(response));


        //}


        //[TestMethod()]
        //public void UpdateLDMTouchInformationTestReturnFull()
        //{
        //    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        //    Address fromAddress = new Address() { AddressLine1 = "2315 Atlantic Ave", AddressLine2 = "", City = "Raleigh", State = "NC", Zip = "27604" };
        //    Address toAddress = new Address() { AddressLine1 = "TestDE", AddressLine2 = "TestDE", City = "Morrisville", State = "NC", Zip = "27560" };

        //    Address siteAddress = new Address() { AddressLine1 = "2315 Atlantic Ave", AddressLine2 = "", City = "Raleigh", State = "NC", Zip = "27604" };


        //    var response = smdTest.UpdateLDMTouchInformation(6016124, "sfund", Guid.NewGuid().ToString(), fromAddress, toAddress, "L163", 7.0m, PREnums.TouchType.ReturnFull, 2);

        //    Assert.IsTrue(string.IsNullOrWhiteSpace(response));


        //}


        //[TestMethod()]
        //public void UpdateLDMTouchInformationTestWareHouseAccess()
        //{
        //    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        //    Address fromAddress = new Address() { AddressLine1 = "2315 Atlantic Ave", AddressLine2 = "", City = "Raleigh", State = "NC", Zip = "27604" };
        //    Address toAddress = new Address() { AddressLine1 = "TestDE", AddressLine2 = "TestDE", City = "Morrisville", State = "NC", Zip = "27560" };

        //    Address siteAddress = new Address() { AddressLine1 = "2315 Atlantic Ave", AddressLine2 = "", City = "Raleigh", State = "NC", Zip = "27604" };


        //    var response = smdTest.UpdateLDMTouchInformation(6016124, "sfund", Guid.NewGuid().ToString(), fromAddress, toAddress, "L163", 7.0m, PREnums.TouchType.WareHouseAccess, 3);

        //    Assert.IsTrue(string.IsNullOrWhiteSpace(response));


        //}

        //[TestMethod()]
        //public void UpdateLDMTouchInformationTestCurbToCurb()
        //{
        //    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        //    Address fromAddress = new Address() { AddressLine1 = "TestCCFrom", AddressLine2 = "TestCCFrom", City = "Raleigh", State = "NC", Zip = "27604" };
        //    Address toAddress = new Address() { AddressLine1 = "TestCCTO", AddressLine2 = "TestCCTO", City = "Morrisville", State = "NC", Zip = "27560" };

        //    Address siteAddress = new Address() { AddressLine1 = "2315 Atlantic Ave", AddressLine2 = "", City = "Raleigh", State = "NC", Zip = "27604" };


        //    var response = smdTest.UpdateLDMTouchInformation(6016124, "sfund", Guid.NewGuid().ToString(), fromAddress, toAddress, "L163", 7.0m, PREnums.TouchType.CurbToCurb, 4);

        //    Assert.IsTrue(string.IsNullOrWhiteSpace(response));


        //}



        //#endregion TouchUpdates Test

        //[TestMethod()]
        //public void InsertPRErrorLogTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void InsertPRErrorLogTest1()
        //{
        //    Exception ex = new Exception("test Ececption");

        //    LDMComponent.InsertPRErrorLog(DateTime.Now, "sfund", ex.GetType().ToString(), ex.Message, ex.StackTrace, 0, "tets", "", 63553, 0);
        //    Assert.Fail();
        //}


        //[TestMethod()]
        //public void UpdateLDMTouchInformationTestProdIssue()
        //{
        //    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        //    Address fromAddress = new Address() { AddressLine1 = "2315 Atlantic Ave", AddressLine2 = "", City = "Raleigh", State = "NC", Zip = "27604" };
        //    Address toAddress = new Address() { AddressLine1 = "TestDE", AddressLine2 = "TestDE", City = "Morrisville", State = "NC", Zip = "27560" };

        //    Address siteAddress = new Address() { AddressLine1 = "2315 Atlantic Ave", AddressLine2 = "", City = "Raleigh", State = "NC", Zip = "27604" };


        //    var response = smdTest.UpdateLDMTouchInformation(3112990, "Sushil Fund", Guid.NewGuid().ToString(), fromAddress, toAddress, "L163", 7.0m, PREnums.TouchType.DeliverEmpty, 0);

        //    Assert.IsTrue(string.IsNullOrWhiteSpace(response));


        //}

        //[TestMethod()]
        //public void MoveInTest()
        //{
        //    //SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        //    //smdTest.MoveIn("ADDF7", 6031413, (new Guid()).ToString());//.MoveInForLDM(6030933, "85399", "L167", "000016", (new Guid()).ToString());
        //}

        //[TestMethod()]
        //public void DataTest()
        //{
        //    var leaseDate = new DateTime(2015, 9, 28);

        //    DateTime dt = new DateTime(DateTime.Today.Year, leaseDate.Day > DateTime.Today.Day ? DateTime.Today.AddMonths(-1).Month : DateTime.Today.Month, leaseDate.Day);

        //    leaseDate = new DateTime(2015, 9, 16);
        //    dt = new DateTime(DateTime.Today.Year, leaseDate.Day > DateTime.Today.Day ? DateTime.Today.AddMonths(-1).Month : DateTime.Today.Month, leaseDate.Day);


        //    leaseDate = new DateTime(2015, 9, 17);
        //    dt = new DateTime(DateTime.Today.Year, leaseDate.Day > DateTime.Today.Day ? DateTime.Today.AddMonths(-1).Month : DateTime.Today.Month, leaseDate.Day);

        //}

        //////SEFRP-TODO-RMVNICD -- TG-619
        ////[TestMethod()]
        ////public void UpdateChargeDateToLedgerOnTransferOutTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password, "Sushil Fund");
        ////    smdTest.UpdateChargeDateToLedgerOnTransferOut(6032745, 970740, Guid.NewGuid().ToString());
        ////    Assert.Fail();
        ////}



        //////SEFRP-TODO-RMVNICD -- TG-619
        ///*
        ////[TestMethod]
        ////public void GetAllLDMTouchesTest()
        ////{
        ////    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic =
        ////        new PR.BusinessLogic.SMDBusinessLogic(corpcode, username, password, "Amit");

        ////    DataSet dsQORList = smdBusinessLogic.GetQORList(corpcode, "L167", username, password, 0, "", 3723707, string.Empty);
        ////    if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
        ////    {
        ////        int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
        ////        int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
        ////        int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
        ////        string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

        ////        SLAPIMobile slapiMobile = smdBusinessLogic.EditQuote(corpcode, "L167", username, password, qtRentalId, tenantId, (Guid.NewGuid().ToString()));

        ////        var touches = smdBusinessLogic.GetAppliedTouches(slapiMobile);

        ////        //POSItems items = smdBusinessLogic.GetPOSItmes(slapiMobile);
        ////        slapiMobile.DisposeSLAPI();
        ////        slapiMobile = null;
        ////    }
        ////}

        ////[TestMethod()]
        ////public void GetLDMOrderDetailDestinationTest()
        ////{
        ////    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();
        ////    List<LDMOrderDetail> lstLDmOrderDetail = new List<LDMOrderDetail>();

        ////    SLAPIMobile slapiMobile = null;
        ////    //677595
        ////    lstLDmOrderDetail = smdBusinessLogic.GetLDMOrderDetail(corpcode, username, password, 675859)
        ////        .Where(x => x.StarsUnitID == 981986).ToList();

        ////    if (lstLDmOrderDetail.Where(x => x.LDMType == "D").Any())
        ////    {
        ////        slapiMobile = smdBusinessLogic.EditQuote(corpcode, "L001",
        ////          username, password, lstLDmOrderDetail.Where(x => x.LDMType == "D").Single().QTRentalID,
        ////          lstLDmOrderDetail.Where(x => x.LDMType == "D").Single().TenantID, Guid.NewGuid().ToString());

        ////        Touches touchDest = smdBusinessLogic.GetAppliedTouches(slapiMobile);
        ////        slapiMobile.DisposeSLAPI();
        ////        slapiMobile = null;
        ////    }
        ////}

        ////[TestMethod()]
        ////public void GetLDMOrderDetailOriginTest()
        ////{
        ////    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();
        ////    List<LDMOrderDetail> lstLDmOrderDetail = new List<LDMOrderDetail>();

        ////    SLAPIMobile slapiMobile = null;
        ////    //677595
        ////    lstLDmOrderDetail = smdBusinessLogic.GetLDMOrderDetail(corpcode, username, password, 675859)
        ////        .Where(x => x.StarsUnitID == 981986).ToList();

        ////    if (lstLDmOrderDetail.Where(x => x.LDMType == "O").Any())
        ////    {
        ////        slapiMobile = smdBusinessLogic.EditQuote(corpcode, "L167",
        ////          username, password, lstLDmOrderDetail.Where(x => x.LDMType == "O").Single().QTRentalID,
        ////          lstLDmOrderDetail.Where(x => x.LDMType == "O").Single().TenantID, Guid.NewGuid().ToString());

        ////        Touches touchOrigin = smdBusinessLogic.GetAppliedTouches(slapiMobile);
        ////        slapiMobile.DisposeSLAPI();
        ////        slapiMobile = null;

        ////    }
        ////}



        ////[TestMethod()]
        ////public void GetLDMTouchesTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password, "Sushil Fund");
        ////      var touchDetails = smdTest.GetLDMTouches(2391817, "L167");  // Production data 

        ////    Assert.IsNotNull(touchDetails);
        ////    foreach (DataRow drCompletedTouch in touchDetails.Tables[0].Rows)
        ////    { 
        ////        Assert.IsNotNull(drCompletedTouch["AddrFrom_Addr1"]);
        ////        Assert.IsNotNull(drCompletedTouch["AddrFrom_City"]);
        ////        Assert.IsNotNull(drCompletedTouch["AddrFrom_State"]);
        ////        Assert.IsNotNull(drCompletedTouch["AddrFrom_Zip"]);
        ////        Assert.IsNotNull(drCompletedTouch["AddrTo_Addr1"]);
        ////        Assert.IsNotNull(drCompletedTouch["AddrTo_City"]);

        ////        Assert.IsNotNull(drCompletedTouch["AddrTo_State"]);
        ////        Assert.IsNotNull(drCompletedTouch["AddrTo_Zip"]);
        ////        Assert.IsNotNull(drCompletedTouch["Status"]);
        ////        Assert.IsNotNull(drCompletedTouch["Service"]);
        ////        Assert.IsNotNull(drCompletedTouch["ServiceType"]); 

        ////    } 
        ////}

        ////[TestMethod()]
        ////public void GetPaymentTypeTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password, "Sushil Fund");
        ////    var ds = smdTest.GetPaymentTypes("L499");
        ////    Assert.IsNotNull(ds);
        ////}

        ////[TestMethod()]
        ////public void GetSpiritFeedsByDateRangeTest()
        ////{
        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password, "Sushil Fund");
        ////    smdTest.GetSpiritFeedsByDateRange(string.Empty, DateTime.Today.AddYears(-500), DateTime.Today);
        ////    Assert.Fail();
        ////}

        //*/

        //[TestMethod()]
        //public void SearchOrdersSiteLinkLiveTest()
        //{
        //    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password, "Venkat");
        //    // var dsSearchResponse= smdTest.SearchOrdersSiteLinkLive("", new SearchOrderSitelink() { QTRentalstatusID = 3, SpiritStoreFirstName = "spirit" ,QORID= "3636989, 3636995, 3637003, 3637007, 3637013, 3636993, 3636999, 3637005, 3637011, 3637019,3636991, 3636997, 3637001, 3637009, 3637014,3637023, 3637029, 3637037, 3637041, 3637050, 3637055, 3637061, 3637065,3637074, 3637080, 3637086, 3637094, 3637102, 3637108, 3637116, 3637121, 3637126, 3637134, 3637140, 3637146,3637027, 3637033, 3637039, 3637046, 3637053, 3637059, 3637067, 3637071, 3637078, 3637084,3637091, 3637098, 3637104, 3637114, 3637120, 3637128, 3637136, 3637142, 3637148, 3637025, 3637031, 3637035, 3637048, 3637057, 3637069, 3637076, 3637082, 3637088, 3637096, 3637100, 3637106,3637112, 3637118, 3637124, 3637130, 3637138, 3637144,3637246, 3637253, 3637260, 3637266, 3637272, 3637278, 3637284, 3637298, 3637304, 3637310, 3637318, 3637324, 3637332, 3637338, 3637345, 3637351, 3637357,3637363, 3637242, 3637248, 3637256, 3637262, 3637268, 3637274, 3637280, 3637286, 3637294, 3637302, 3637308, 3637314, 3637320, 3637330, 3637336, 3637343, 3637349, 3637355, 3637361, 3637368, 3637244, 3637250, 637258, 3637264, 3637270, 3637276, 3637288, 3637296, 3637300, 3637306, 3637316, 3637328, 3637334, 3637340, 3637347, 3637353, 3637359, 3637365, 3637371, 3637373, 3637375, 3637377, 3637379" });
        //    var dsSearchResponse = smdTest.SearchOrdersSiteLinkLive("L167", new SearchOrderSitelink() { QTRentalstatusID = 0, QORID = "5188303,5188304" });

        //    if (dsSearchResponse != null && dsSearchResponse.Tables[0].Rows.Count > 0)
        //    {
        //        string JSONString = string.Empty;
        //        JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(dsSearchResponse.Tables[0]);
        //    }

        //    Assert.IsNotNull(dsSearchResponse);
        //    Assert.IsNotNull(dsSearchResponse.Tables[0]);
        //    Assert.IsNotNull(dsSearchResponse.Tables[0].Rows.Count > 0);

        //}


        //[TestMethod()]
        //public void SLAPITestPaymentWebService()
        //{
        //    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password, "Sushil1");
        //    // var dsSearchResponse= smdTest.SearchOrdersSiteLinkLive("", new SearchOrderSitelink() { QTRentalstatusID = 3, SpiritStoreFirstName = "spirit" ,QORID= "3636989, 3636995, 3637003, 3637007, 3637013, 3636993, 3636999, 3637005, 3637011, 3637019,3636991, 3636997, 3637001, 3637009, 3637014,3637023, 3637029, 3637037, 3637041, 3637050, 3637055, 3637061, 3637065,3637074, 3637080, 3637086, 3637094, 3637102, 3637108, 3637116, 3637121, 3637126, 3637134, 3637140, 3637146,3637027, 3637033, 3637039, 3637046, 3637053, 3637059, 3637067, 3637071, 3637078, 3637084,3637091, 3637098, 3637104, 3637114, 3637120, 3637128, 3637136, 3637142, 3637148, 3637025, 3637031, 3637035, 3637048, 3637057, 3637069, 3637076, 3637082, 3637088, 3637096, 3637100, 3637106,3637112, 3637118, 3637124, 3637130, 3637138, 3637144,3637246, 3637253, 3637260, 3637266, 3637272, 3637278, 3637284, 3637298, 3637304, 3637310, 3637318, 3637324, 3637332, 3637338, 3637345, 3637351, 3637357,3637363, 3637242, 3637248, 3637256, 3637262, 3637268, 3637274, 3637280, 3637286, 3637294, 3637302, 3637308, 3637314, 3637320, 3637330, 3637336, 3637343, 3637349, 3637355, 3637361, 3637368, 3637244, 3637250, 637258, 3637264, 3637270, 3637276, 3637288, 3637296, 3637300, 3637306, 3637316, 3637328, 3637334, 3637340, 3637347, 3637353, 3637359, 3637365, 3637371, 3637373, 3637375, 3637377, 3637379" });
        //    //dynamic dsSearchResponse = "";//smdTest.SLAPITestNewWs();
        //    //Assert.IsNotNull(dsSearchResponse);
        //    //Assert.IsNotNull(dsSearchResponse.Tables[0]);
        //    //Assert.IsNotNull(dsSearchResponse.Tables[0].Rows.Count > 0);

        //}

        //[TestMethod()]
        //public void DeleteUnitForLDMTest()
        //{
        //    //try
        //    //{
        //    //    SMDBusinessLogic smdtest = new SMDBusinessLogic();
        //    //    smdtest.DeleteUnitForLDM(corpcode, username, password, "L167", "RLSS49");
        //    //}
        //    //catch (Exception ex)
        //    //{

        //    //}

        //    //Assert.Fail();
        //}

        //[TestMethod()]
        //public void TransferUnitFromOrigToDestForLDMTest()
        //{
        //    //try
        //    //{
        //    //    SMDBusinessLogic smdtest = new SMDBusinessLogic();
        //    //    smdtest.TransferUnitFromOrigToDestForLDM(corpcode, "L160", "L001", username, password, "160176", "000016", false);
        //    //}
        //    //catch (Exception ex)
        //    //{

        //    //}
        //    //Assert.Fail();
        //}

        ////SEFRP-TODO-RMVNICD
        ////[TestMethod()]
        ////public void CompletTIAndMoveInTITouchTest()
        ////{
        ////    try
        ////    {
        ////        SMDBusinessLogic smdtest = new SMDBusinessLogic(corpcode, username, password, "venkat");
        ////        string unitname = string.Empty;
        ////        smdtest.AddUnitCompletTIAndMoveInTITouch(3836516, "681860", 988903, (new Guid()).ToString(), out unitname);
        ////    }
        ////    catch (Exception ex)
        ////    {

        ////    }
        ////    Assert.Fail();
        ////}



        //[TestMethod()]

        //public void LedgerTransferToNewTenantTest()
        //{

        //    //SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password, "Venkat");
        //    //DataSet ds = smdTest.LedgerTransferToNewTenant(309090, 234324, "L001");
        //}
        //public void CompletTOAndMoveOutTOTouchTest()
        //{
        //    try
        //    {
        //        SMDBusinessLogic smdtest = new SMDBusinessLogic(corpcode, username, password);
        //        smdtest.CompletTOAndMoveOutTOTouch(3839093);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}


        //[TestMethod()]
        //public void ScheduleTouchTest()
        //{
        //    try
        //    {
        //        //DateTime dt = DateTime.Parse("1/1/1753");
        //        //SMDBusinessLogic smdtest = new SMDBusinessLogic(corpcode, username, password, "venkat");
        //        //smdtest.ScheduleTouch(3837139, PREnums.TouchTypeShort.OBO, 1, dt, (new Guid()).ToString());

        //        decimal s = 23.345M;

        //        decimal a = Math.Round(s, 2);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void IsUnitAvailableToRentTest()
        //{
        //    try
        //    {
        //        SMDBusinessLogic smdtest = new SMDBusinessLogic(corpcode, username, password, "venkat");
        //        smdtest.IsUnitAvailableToRent(corpcode, "L160", username, password, "160128");
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void GetCapacityTest()
        //{
        //    PRCapacity oPRCapacity = new PRCapacity();

        //    DateTime startDate = DateTime.Now;
        //    int numberOfDays = 30;

        //    var ds = oPRCapacity.GetCapacity(corpcode, "L167", username, password, startDate, numberOfDays, 0, null, null);

        //    Assert.Fail();
        //}

        //////SEFRP-TODO-RMVNICD -- TG-619
        ///*
        ////[TestMethod()]
        ////public void ScheduleTOTouch()
        ////{

        ////    SMDBusinessLogic smdTest = new SMDBusinessLogic(corpcode, username, password);
        ////    int tenantId = 452;
        ////    int unitId = 0;
        ////    int qorid = 3892994;
        ////    int qtRentalId = 0;
        ////    string locationCode = string.Empty;

        ////    var dsQORList = smdTest.GetQORList(corpcode, String.Empty, username, password, 0, String.Empty, qorid, String.Empty);
        ////    if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
        ////    {
        ////        tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
        ////        unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
        ////        qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
        ////        locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();
        ////        var slapi = smdTest.EditQuote(corpcode, locationCode, username, password, qtRentalId, tenantId, (new Guid()).ToString());

        ////        POSItems appliedPOSItems = smdTest.GetAppliedPOSItems(unitId, slapi);

        ////        var calculateFuelSurcharge = !smdTest.IsFuelAdjusmentAlreadyAdded(PREnums.TouchType.DeliverFull, unitId, slapi);


        ////        Touches touches = smdTest.GetAppliedTouches(slapi);
        ////        //Touches.TouchRow drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='" + eQTType.LDMTransferOut.ToString() + "'")[0];
        ////        var drTouchRow = touches.Touch.Select("ServiceType='LDM Transfer Out'");
        ////        if (drTouchRow != null && drTouchRow.Length > 0)
        ////        {
        ////            Touches.TouchRow drTouchRowReqType = (Touches.TouchRow)drTouchRow[0];

        ////            int startTimeID = smdTest.GetScheduleTime("12:00", slapi);
        ////            int endTimeID = smdTest.GetScheduleTime("12:00", slapi);

        ////            smdTest.ScheduleTouch(DateTime.Now, startTimeID, endTimeID, drTouchRowReqType.EditIdentifier, slapi);

        ////            smdTest.SaveQOR(corpcode, locationCode, username, password, slapi);
        ////        }

        ////    }

        ////    Assert.Fail();
        ////}

        /////// <summary>
        /////// Complete DE, RF and then execute this method to schedule TO touch. TI touch you can schedule 
        /////// </summary>
        ////[TestMethod]
        ////public void ScheduleTOAndTITouches()
        ////{
        ////    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic =
        ////        new PR.BusinessLogic.SMDBusinessLogic(corpcode, username, password, "Venkat");

        ////    var u = smdBusinessLogic.GetUnitIdByUnitNameFromSL("CPKR", "L454", username, password, "000016");

        ////    int qorid = 3723707;
        ////    //string locationCode = "L167";

        ////    DataSet dsQORList = smdBusinessLogic.GetQORList(corpcode, string.Empty, username, password, 0, "", qorid, string.Empty);
        ////    if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
        ////    {
        ////        int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
        ////        int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
        ////        int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
        ////        string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

        ////        SLAPIMobile slapiMobile = smdBusinessLogic.EditQuote(corpcode, locationCode, username, password, qtRentalId, tenantId, (Guid.NewGuid().ToString()));

        ////        var touches = smdBusinessLogic.GetAppliedTouches(slapiMobile);
        ////        string touchtype = "LDM Transfer Out";//LDM Transfer In

        ////        var drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='" + touchtype + "'")[0];

        ////        if (drTouchRow != null)
        ////        {
        ////            int startTimeID = smdBusinessLogic.GetScheduleTime("Anytime", slapiMobile);
        ////            int endTimeID = smdBusinessLogic.GetScheduleTime("Anytime", slapiMobile);

        ////            smdBusinessLogic.ScheduleTouch(DateTime.Now, startTimeID, endTimeID, drTouchRow.EditIdentifier, slapiMobile);

        ////            smdBusinessLogic.SaveQORNew(corpcode, username, password, locationCode, tenantId, slapiMobile);
        ////        }


        ////        //POSItems items = smdBusinessLogic.GetPOSItmes(slapiMobile);
        ////        slapiMobile.DisposeSLAPI();
        ////        slapiMobile = null;
        ////    }
        ////} 


        ////[TestMethod()]
        ////public void UpdateSpiritOrderTest()
        ////{
        ////    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic(corpcode, username, password, "Venkat");

        ////    int qorid = 3673031;
        ////    //string locationCode = "L167";

        ////    DataSet dsQORList = smdBusinessLogic.GetQORList(corpcode, string.Empty, username, password, 0, "", qorid, string.Empty);
        ////    if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
        ////    {
        ////        int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
        ////        int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
        ////        int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
        ////        string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

        ////        SLAPIMobile slapiMobile = smdBusinessLogic.EditQuote(corpcode, locationCode, username, password, qtRentalId, tenantId, (Guid.NewGuid().ToString()));

        ////        var touches = smdBusinessLogic.GetAppliedTouches(slapiMobile);
        ////        string touchtype = "WarehouseToCurbFull";//LDM Transfer In

        ////        var drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='" + touchtype + "'")[0];

        ////        if (drTouchRow != null)
        ////        {
        ////            //int startTimeID = smdBusinessLogic.GetScheduleTime("Anytime", slapiMobile);
        ////            //int endTimeID = smdBusinessLogic.GetScheduleTime("Anytime", slapiMobile);

        ////            //smdBusinessLogic.ScheduleTouch(DateTime.Now, startTimeID, endTimeID, drTouchRow.EditIdentifier, slapiMobile);

        ////            //smdBusinessLogic.SaveQORNew(corpcode, username, password, locationCode, tenantId, slapiMobile);

        ////            //smdBusinessLogic.UpdateTransportationCharge(slapiMobile, 52.5M, 52.5M, 52.5M, 52.5M, 52.5M, 1.9M, 0M);

        ////            smdBusinessLogic.SpiritTouchMilesUpdate(100, drTouchRow.EditIdentifier, slapiMobile);

        ////            //smdBusinessLogic.UpdateTransportationCharge(slapiMobile, 52.5M, 52.5M, 52.5M, 52.5M, 52.5M, 1.9M, 75M);

        ////            smdBusinessLogic.SaveQORNew(corpcode, username, password, locationCode, tenantId, slapiMobile);
        ////        }


        ////        //POSItems items = smdBusinessLogic.GetPOSItmes(slapiMobile);
        ////        slapiMobile.DisposeSLAPI();
        ////        slapiMobile = null;
        ////    }
        ////}

        ////[TestMethod()]
        ////public void GetLocalPriceDetailsFor16FtTest()
        ////{
        ////    try
        ////    {
        ////        int qorid = 4740835;
        ////        //string locationCode = "L167";

        ////        SMDBusinessLogic smdBusinessLogic = new SMDBusinessLogic(corpcode, username, password);

        ////        DataSet dsQORList = smdBusinessLogic.GetQORList(corpcode, string.Empty, username, password, 0, "", qorid, string.Empty);
        ////        if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
        ////        {
        ////            int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
        ////            int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
        ////            int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
        ////            string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

        ////            SLAPIMobile slapiMobile = smdBusinessLogic.EditQuote(corpcode, locationCode, username, password, qtRentalId, tenantId, (Guid.NewGuid().ToString()));

        ////            //                   var abc = smdBusinessLogic.SaveMarketingSources(corpcode, locationCode, username, password, string.Empty, "Accounting", tenantId, slapiMobile);

        ////            //POSItems items = smdBusinessLogic.GetPOSItmes(slapiMobile);


        ////            var promo = smdBusinessLogic.GetPromotions(corpcode, username, password);

        ////            var res = smdBusinessLogic.GetLocalPriceDetailsFor16Ft(slapiMobile, promo);

        ////            slapiMobile.DisposeSLAPI();
        ////            slapiMobile = null;
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {

        ////    }
        ////    Assert.Fail();
        ////}

        ////[TestMethod()]
        ////public void SaveMarketingSourcesTest()
        ////{
        ////    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic(corpcode, username, password, "Venkat");

        ////    int qorid = 4149069;
        ////    //string locationCode = "L167";

        ////    DataSet dsQORList = smdBusinessLogic.GetQORList(corpcode, string.Empty, username, password, 0, "", qorid, string.Empty);
        ////    if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
        ////    {
        ////        int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
        ////        int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
        ////        int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
        ////        string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

        ////        SLAPIMobile slapiMobile = smdBusinessLogic.EditQuote(corpcode, locationCode, username, password, qtRentalId, tenantId, (Guid.NewGuid().ToString()));

        ////        var abc = smdBusinessLogic.SaveMarketingSources(corpcode, locationCode, username, password, string.Empty, "Accounting", tenantId, slapiMobile);

        ////        //POSItems items = smdBusinessLogic.GetPOSItmes(slapiMobile);
        ////        slapiMobile.DisposeSLAPI();
        ////        slapiMobile = null;
        ////    }
        ////}


        ////[TestMethod()]
        ////public void UpdateTouchTest()
        ////{
        ////    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic(corpcode, username, password, "Venkat");

        ////    Address oAddress = new Address();
        ////    Address dAddress = new Address();

        ////    smdBusinessLogic.UpdateTouch(4056734, oAddress, dAddress, "Code updated", SLNet.Common.eQTType.WarehouseToCurbEmpty, 0, true);

        ////    Assert.Fail();
        ////}
        //*/

        ////// SFERP-TODO-CTRMV-- Remove this method --TG-568
        ////[TestMethod()]
        ////public void GetCustomerTest()
        ////{
        ////    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic(corpcode, username, password, "Venkat");
        ////    var d = smdBusinessLogic.GetCustomer(corpcode, "L001", username, password, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "6796796967");

        ////    Assert.Fail();
        ////}


        ////////SEFRP-TODO-RMVNICD
        ////[TestMethod()]
        ////public void GetQORListTest1()
        ////{
        ////    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic(corpcode, username, password, "Venkat");
        ////    try
        ////    {
        ////        //var d = smdBusinessLogic.GetQORList(corpcode, "L001", username, password, 59221, string.Empty, -1, "");
        ////        var d = smdBusinessLogic.GetQORList(corpcode, "L001", username, password, 0, string.Empty, 2324234, "");
        ////    }
        ////    catch (Exception ex)
        ////    {

        ////    }
        ////    Assert.Fail();
        ////}


        ////////SEFRP-TODO-RMVNICD
        ////[TestMethod()]
        ////public void GetPromotionsTest()
        ////{
        ////    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic(corpcode, username, password, "Venkat");
        ////    try
        ////    {
        ////        //var d = smdBusinessLogic.GetQORList(corpcode, "L001", username, password, 59221, string.Empty, -1, "");
        ////        var d = smdBusinessLogic.GetPromotions(corpcode, username, password);
        ////    }
        ////    catch (Exception ex)
        ////    {

        ////    }

        ////    Assert.Fail();
        ////}

        //[TestMethod()]
        //public void GetWareHouseTouchesBySPFromSLTest()
        //{
        //    //try
        //    //{
        //    //    CriteriaSitelinkTouches criteriaSitelinkTouches = new CriteriaSitelinkTouches
        //    //    {
        //    //        FLocationCodes = "L160",
        //    //        ScheduleFromDate = DateTime.Now,
        //    //        ScheduleToDate = DateTime.Now.AddDays(1),
        //    //        QTStatusIDs = "1,2,3",
        //    //        QTRentalstatusIDs = "1,3",
        //    //        QTTypeIDs = $"{(int)eQTType.WarehouseToCurbEmpty},{(int)eQTType.WarehouseToCurbFull},{(int)eQTType.CurbToCurb},{(int)eQTType.ReturnToWarehouseEmpty},{(int)eQTType.ReturnToWarehouseFull}"
        //    //    };

        //    //    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic(corpcode, username, password, "Venkat");

        //    //    var a = smdBusinessLogic.GetWareHouseTouchesBySPFromSL(criteriaSitelinkTouches);
        //    //}
        //    //catch (Exception ex)
        //    //{

        //    //}
        //    //Assert.Fail();
        //}



        //[TestMethod()]
        //public void GetSLTouchesBySP()
        //{
        //    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic(corpcode, username, password, "Venkat");

        //    var locationCode = "L160";
        //    var dst = smdBusinessLogic.GetWareHouseTouchesByFacility(corpcode, locationCode, username, password, DateTime.Now.AddDays(-20), 30, 0);

        //    if (dst.Tables != null && dst.Tables.Count > 0 && dst.Tables["QTs"] != null && dst.Tables["QTs"].Rows != null && dst.Tables["QTs"].Rows.Count > 0)
        //    {
        //        DataRow[] dRowsStagingTouches = dst.Tables["QTs"].Select("QTTypeID in (7, 8, 9, 10,11)");
        //        dst.Tables["QTs"].Rows.Clear();
        //        dst.Tables["QTs"].Rows.Add(dRowsStagingTouches);
        //    }

        //    //Convert Data from Table to Json, Added for SFERP-TODO
        //    string jsonSchedule1 = JsonConvert.SerializeObject(dst.Tables["QTs"]);
        //    CommonUtility.WriteToFile(jsonSchedule1, Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetStagingTouches-{locationCode}-TouchesAvailableSchedule.txt"));

        //    //Convert Data from JsontoTable , Added for SFERP-TODO
        //    string jsonSchedule2 = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetCapacity-{locationCode}-TouchesAvailableSchedule.txt"));
        //    DataSet dsSchedule = new DataSet();
        //    dsSchedule.Tables.Add((DataTable)JsonConvert.DeserializeObject(jsonSchedule2, (typeof(DataTable))));
        //    dsSchedule.Tables[0].TableName = "QTs";  // to match with SL datatable

        //    string TransportationTouchTypeIds = $"{(int)eQTType.WarehouseToCurbEmpty},{(int)eQTType.WarehouseToCurbFull},{(int)eQTType.CurbToCurb},{(int)eQTType.ReturnToWarehouseEmpty},{(int)eQTType.ReturnToWarehouseFull}";
        //    string WarehouseTouchTypeIds = $"{(int)eQTType.WarehouseAccess},{(int)eQTType.InByOwner},{(int)eQTType.OutByOwner},{(int)eQTType.LDMTransferIn},{(int)eQTType.LDMTransferOut}";
        //    string RentalstatusIDs_OpenCompleted = $"{(int)eQTStatus.Open},{(int)eQTStatus.Completed}";
        //    string RentalStatusIDs_ActiveCompleted = $"{(int)eQTRentalStatuses.Active},{(int)eQTStatus.Completed}";
        //    string RentalTypeIDs_OrderRental = $"{(int)eQTRentalTypes.Order},{(int)eQTRentalTypes.Rental}";


        //    CriteriaSitelinkTouches criteriaSitelinkTouches = new CriteriaSitelinkTouches
        //    {
        //        FLocationCodes = "L160",
        //        IsGetLiveData = true,
        //        ScheduleFromDate = Convert.ToDateTime("11/25/2019"),// DateTime.Now.Date,
        //        ScheduleToDate = Convert.ToDateTime("12/30/2019"), //DateTime.Now.Date,
        //        QTTypeIDs = TransportationTouchTypeIds,
        //        QTStatusIDs = RentalstatusIDs_OpenCompleted,
        //        QTRentalstatusIDs = RentalStatusIDs_ActiveCompleted,
        //        QTRentalTypeIDs = RentalTypeIDs_OrderRental
        //    };

        //    //commented by Sohan
        //    //var ds = smdBusinessLogic.GetSLTouchesBySP(criteriaSitelinkTouches);

        //    //int qorid = 4149069;
        //    //string locationCode = "L167";

        //    //smdBusinessLogic.GetInvoices(corpcode, string.Empty, username, password, 0, false);

        //}

        //[TestMethod]
        //public void GoogleAPiTest()
        //{
        //    PRAddressAPI addressAPI = new PRAddressAPI();
        //    //var address = addressAPI.ValidateAddress(new PR.Entities.Address() { Zip = "22303" });
        //    var address = addressAPI.ValidateAddress(new PR.Entities.Address() { Zip = "64111" });

        //}

        //[TestMethod()]
        //public void UpdateCreditCardInfoForRentalTest()
        //{
        //    try
        //    {
        //        SMDBusinessLogic smd = new SMDBusinessLogic(corpcode, username, password);

        //        string locationcode = "L167";
        //        int QORID = 4601296;
        //        int TenantID = 41275;
        //        int creditCardTypeid = 133;
        //        string creditCardNumber = "4111111111111111";
        //        DateTime expirationDate = DateTime.Now.AddMonths(12);
        //        string cardHolderName = "Test Card Venkat";
        //        string billingStreetAddress = "123 SW Main St";
        //        string billingZip = "27560";

        //        smd.UpdateCreditCardInfoForRental(corpcode, locationcode, username, password, TenantID, QORID, creditCardTypeid, creditCardNumber, expirationDate, cardHolderName, billingStreetAddress, billingZip);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void CallStoredProcedureTest()
        //{
        //    try
        //    {
        //        SMDBusinessLogic smd = new SMDBusinessLogic(corpcode, username, password);
        //        string locationcode = "L167";
        //        int QORID = 4601296;

        //        DataSet ds1 = smd.CallStoredProcedure(corpcode, username, locationcode, password, QORID, "spOpApiPkrPaymentHistoryByQRID");

        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    Assert.Fail();
        //}

        //////SEFRP-TODO-RMVNICD
        ////[TestMethod()]
        ////public void GetLocalQuoteBillingInfoTest()
        ////{
        ////    try
        ////    {
        ////        SMDBusinessLogic smd = new SMDBusinessLogic(corpcode, username, password);
        ////        string locationcode = "L167";
        ////        int QORID = 4601296;

        ////        var ds = smd.GetLocalQuoteBillingInfo(corpcode, username, locationcode, password, QORID);

        ////    }
        ////    catch (Exception ex)
        ////    {

        ////    }

        ////    Assert.Fail();
        ////}



        //[TestMethod()]
        //public void GetSLTouchesBySP_SFERP()
        //{
        //    PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic(corpcode, username, password, "Venkat");

        //    //var dst = smdBusinessLogic.GetWareHouseTouchesByFacility(corpcode, "L157", username, password, DateTime.Now, 10, 0);

        //    string TransportationTouchTypeIds = $"{(int)eQTType.WarehouseToCurbEmpty},{(int)eQTType.WarehouseToCurbFull},{(int)eQTType.CurbToCurb},{(int)eQTType.ReturnToWarehouseEmpty},{(int)eQTType.ReturnToWarehouseFull}";
        //    string WarehouseTouchTypeIds = $"{(int)eQTType.WarehouseAccess},{(int)eQTType.InByOwner},{(int)eQTType.OutByOwner},{(int)eQTType.LDMTransferIn},{(int)eQTType.LDMTransferOut}";
        //    string RentalstatusIDs_OpenCompleted = $"{(int)eQTStatus.Open},{(int)eQTStatus.Completed}";
        //    string RentalStatusIDs_ActiveCompleted = $"{(int)eQTRentalStatuses.Active},{(int)eQTStatus.Completed}";
        //    string RentalTypeIDs_OrderRental = $"{(int)eQTRentalTypes.Order},{(int)eQTRentalTypes.Rental}";


        //    CriteriaSitelinkTouches criteriaSitelinkTouches = new CriteriaSitelinkTouches
        //    {
        //        FLocationCodes = "L160",
        //        IsGetLiveData = true,
        //        ScheduleFromDate = DateTime.Now.Date,
        //        ScheduleToDate = DateTime.Now.Date.AddDays(30),
        //        QTTypeIDs = TransportationTouchTypeIds,
        //        QTStatusIDs = RentalstatusIDs_OpenCompleted,
        //        QTRentalstatusIDs = RentalStatusIDs_ActiveCompleted,
        //        QTRentalTypeIDs = RentalTypeIDs_OrderRental
        //    };

        //    //commented by Sohan
        //    // var ds = smdBusinessLogic.GetSLTouchesBySP(criteriaSitelinkTouches);

        //    //int qorid = 4149069;
        //    //string locationCode = "L167";

        //    //smdBusinessLogic.GetInvoices(corpcode, string.Empty, username, password, 0, false);

        //}


        [TestMethod()]
        public void TestGetFacilityCapacityDataStub()
        {
            try
            {
                DateTime startDate = Convert.ToDateTime("12/20/2019");
                DateTime endDate = Convert.ToDateTime("12/27/2019");

                SMDBusinessLogic smd = new SMDBusinessLogic();
                ScheduleCalendar scheduleCalendar = smd.Get_TG733_GetFacilityCapacityData(startDate, endDate, "L167");

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void TestGetScheduleCalendarDataStub()
        {
            try
            {
                string locationCode = "L157";
                DateTime startDate = Convert.ToDateTime("12/01/2019");
                int numberOfDays = 31;

                LocalComponent lc = new LocalComponent();
                ScheduleCalendar scheduleCalendar = lc.TG450_GetScheduleCalendarData(locationCode, startDate, numberOfDays);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void Test_GetWhereHouseTouches_ESB()
        {
            try
            {
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                // string methodName = "WhseTouches";
                // string requestJson = "{ \"LocationCode\":\"L167\", \"waStartDate\":\"12/01/2019\", \"trStartDate\":\"12/18/2019\", \"endDate\":\"12/19/2019\" }";

                object requestObject = new
                {
                    locationCode = "L157",
                    waStartDate = "11/18/2018",
                    waEndDate = "12/18/2018",
                    trStartDate = "12/18/2018",
                    endDate = "12/20/2019"
                };

                string requestJson = new JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.GetWareHouseTouches, requestJson);

                PR.Entities.EsbEntities.TouchDashboard.RootObject objRoot = new PR.Entities.EsbEntities.TouchDashboard.RootObject();

                objRoot = new JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.TouchDashboard.RootObject>(response);
                //List<Entities.ERPEntities.SFWarehouseTouch> obj1 = new JavaScriptSerializer().Deserialize<List<Entities.ERPEntities.SFWarehouseTouch>>(response);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void Test_GetTouchAvailabelSchedule_ESB()
        {
            try
            {
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new
                {
                    locationCode = "L157",
                    startDate = "12/01/2018",
                    //endDate = "12/20/2019",
                    numberOfDays = 90
                };

                DataSet dsSchedule = null;
                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.GetTouchAvailableSchedule, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    dsSchedule = Newtonsoft.Json.JsonConvert.DeserializeObject<DataSet>(response);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void Test_GetTransportaionTouches_ESB()
        {
            try
            {
                PR.Entities.EsbEntities.LocalDispatch.RootObject objRoot = null;
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new
                {
                    locationCodes = "L157",
                    touchDate = "12/01/2018",
                    qTTypeIDs = "2,3,4,5,6",
                    qTStatusIDs = "1,2",
                    qTRentalstatusIDs = "1,3",
                    qTRentalTypeIDs = "2,3"
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.GetTransportationTouches, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    objRoot = new JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.LocalDispatch.RootObject>(response);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }


        [TestMethod()]
        public void Test_GetCustomerInfo_ESB()
        {
            try
            {
                PR.Entities.EsbEntities.CustomerData.RootObject objRoot = null;
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new
                {
                    locationCode = "L160",
                    CustomerID = "118464"
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.GetCustomerInfo, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    objRoot = new JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.CustomerData.RootObject>(response);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void Test_GetStagingTouches_ESB()
        {
            try
            {
                PR.Entities.EsbEntities.TouchStage.RootObject objRoot = null;
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new
                {
                    locationCode = "L201",
                    touchDate = "12/15/2018",
                    qTTypeIDs = "2,3,4,5,6",
                    qTStatusIDs = "1,2,3,4,5"
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.GetStagingTouches, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    objRoot = new JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.TouchStage.RootObject>(response);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }


        [TestMethod()]
        public void Test_GetUnAssignedTouches_ESB()
        {
            try
            {
                PR.Entities.EsbEntities.TouchUnAssign.RootObject objRoot = null;
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new
                {
                    locationCode = "L201",
                    touchDate = "12/15/2018",
                    qTTypeIDs = "2,3,4,5,6",
                    qTStatusIDs = "1,2,3,4,5"
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.GetUnassignedTouches, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    objRoot = new JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.TouchUnAssign.RootObject>(response);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }


        [TestMethod()]
        public void Test_GetUnitInfo_ESB()
        {
            try
            {
                PR.Entities.EsbEntities.UnitData.RootObject objRoot = null;
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new UnitInfoRequest
                {
                    qorId = "",
                    locationCode = "L160",
                    unitName = "701624"
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.GetUnitInfo, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    objRoot = new JavaScriptSerializer().Deserialize<PR.Entities.EsbEntities.UnitData.RootObject>(response);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }


        [TestMethod()]
        public void Test_GetBrandInfo_ESB()
        {
            try
            {

                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new UnitInfoRequest
                {
                    qorId = "23162"
                };

                DataSet dsBrandInfo = null;
                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.GetBrandInfo, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    dsBrandInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<DataSet>(response);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }


        [TestMethod()]
        public void Test_GetTouchStatus_ESB()
        {
            try
            {
                PR.Entities.EsbEntities.TouchData.RootObject objRoot = null;
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new TouchStatusRequest
                {
                    qorId = "1927199353",
                    touchTypeId = "2"
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.GetTouchStatus, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.TouchData.RootObject>(response);

                var sts = (PR.UtilityLibrary.ERP_Enums.eQTStatus)Convert.ToInt32(objRoot.SF_TouchStatus.TouchStatusId);


                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void Test_UpdateTouchStatus_ESB()
        {
            try
            {
                PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new
                {
                    QorId = "299649224",
                    touchTypId = "2",
                    sequenceNo = "1",
                    touchStatus = "1",
                    updatedBy = "facility Manager"
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.UpdateTouchStatus, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);

                var sts = Convert.ToInt32(objRoot.ResponseCode);


                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void Test_UpdateTouchSchedule_ESB()
        {
            try
            {
                PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new
                {
                    QorId = "299649224",
                    touchTypId = "2",
                    sequenceNo = "1",
                    scheduleDate = "1/8/2020",
                    touchTime = "Anytime",
                    updatedBy = "facility Manager"
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.UpdateTouchSchedule, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);

                var sts = Convert.ToInt32(objRoot.ResponseCode);


                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void Test_CompleteCCTouch_ESB()
        {
            try
            {
                PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new
                {
                    QorId = 299649224,
                    touchTypId = 2,
                    sequenceNo = 2,
                    scheduleDate = "1/8/2020",
                    touchTime = "Anytime",
                    updatedBy = "facility Manager"
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.CompleteCCTouch, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);

                var sts = Convert.ToInt32(objRoot.ResponseCode);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void Test_UpdateTouchAssignment_ESB()
        {
            try
            {
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                SLTouch touch = new SLTouch
                {
                    Qorid = 6701792,
                    SequenceNO = 0,
                    ScheduledDate = Convert.ToDateTime("2015-01-22 11:50:00.000"),
                    TouchType = "DeliverEmpty"
                };

                smdBusinessLogic.UpdateTouchAssignment(touch.ScheduledDate, touch, 1097, "facility Manager");

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void Test_UpdateTouchData_ESB()
        {
            try
            {
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                var QorID = 299649224;
                var iSequenceNum = 1;
                string instructions = "testing packrat instructions";
                var updateFromAddress = true;
                var activityBy = "Facility Manager";
                var touchType = ERP_Enums.eQTType.WarehouseToCurbEmpty;

                Address startAddress = new Address
                {
                    FirstName = "FirstName11",
                    LastName = "LastName11",
                    Company = "TestCompany2",
                    AddressLine1 = "101",
                    AddressLine2 = "Cotten Square Ln",
                    City = "Morrisville",
                    State = "NC",
                    Zip = "27560",
                    PhoneNumber = "2345678901",
                    MobilePhoneNumber = ""
                };
                Address toAddress = new Address
                {
                    FirstName = "FirstName22",
                    LastName = "LastName22",
                    Company = "TestCompany2",
                    AddressLine1 = "25",
                    AddressLine2 = "Vintage Dr,",
                    City = "Covington",
                    State = "GA",
                    Zip = "30014",
                    PhoneNumber = "2345678901",
                    MobilePhoneNumber = ""
                };

                bool result = smdBusinessLogic.UpdateTouchData(QorID, startAddress, toAddress, instructions, touchType, iSequenceNum, updateFromAddress, activityBy);
 
                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void Test_CompleteTransferInMoveIn_ESB()
        {
            try
            {
                PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new
                {
                    destQorId = 56948,// 345215,
                    orderId = 123456, //345215,
                    starsUnitId = "82037", //458762, 
                    updatedBy = "facility Manager"
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.CompleteTransferInMoveIn, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);

                var sts = Convert.ToInt32(objRoot.ResponseCode);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }

        [TestMethod()]
        public void Test_CompleteTransferOutMoveOut_ESB()
        {
            try
            {
                PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;
                PR.BusinessLogic.SMDBusinessLogic smdBusinessLogic = new PR.BusinessLogic.SMDBusinessLogic();

                object requestObject = new
                {
                    QorId = 299649224, 
                    updatedBy = "facility Manager"
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = smdBusinessLogic.ExecuteMethodESB(EsbMethod.CompleteTransferOutMoveOut, requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);

                var sts = Convert.ToInt32(objRoot.ResponseCode);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                var exp = ex.Message;
            }
        }


    }
}
