﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities
{
    public class FAEmailHistoryLog
    {
        public int PK_ID { get; set; }
        public string ApplicationName { get; set; }
        public string IPAddress { get; set; }
        public Nullable<int> UserID { get; set; }
        public string Subject { get; set; }
        public string ToEmailIDs { get; set; }
        public string FromEmailID { get; set; }
        public string MethodName { get; set; }
        public int EmailTypeID { get; set; }
        public Nullable<int> QORID { get; set; }
        public string TouchType { get; set; }
        public Nullable<int> SLSeqNo { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        public Nullable<System.DateTime> EndTime { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public int TemplateID { get; set; }
    }
}
