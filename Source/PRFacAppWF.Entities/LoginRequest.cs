﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities
{
    public class LoginRequest
    {
        [Required(ErrorMessage = "Login Name is required")]
        public string LoginName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }

        public bool IsRememberMe { get; set; }

        public string Extension { get; set; }
    }

    public class ChangePasswordRequest {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string RetypeNewPassword { get; set; }
        public int UserId { get; set; }
    }

    public class ForgotPasswordRequest {
        public string UserName { get; set; }
    }
}