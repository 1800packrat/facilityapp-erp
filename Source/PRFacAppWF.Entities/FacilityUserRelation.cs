﻿namespace PRFacAppWF.models
{
    public class FacilityUserRelation
    {
        public int FacilityID { get; set; }
        public int AccessLevelID { get; set; }
        public int ApplicationTypeID { get; set; }
    }
}
