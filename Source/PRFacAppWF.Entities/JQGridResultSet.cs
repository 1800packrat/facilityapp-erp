﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PRFacAppWF.Entities
{
    public class JQGridResultSet
    {
        public class Row
        {
            public int id { get; set; }
            public List<string> cell { get; set; }

            public Row()
            {
                cell = new List<string>();
            }
        }
        [DefaultValue(0)]
        public int page { get; set; }

        [DefaultValue(0)]
        public int total { get; set; }

        [DefaultValue(0)]
        public int records { get; set; }

        [DefaultValue(new string[0])]
        public List<Row> rows { get; set; }

        public JQGridResultSet()
        {
            rows = new List<Row>();
        }
    }

    public class ResultSet<T>
    {
        [DefaultValue(0)]
        public int page { get; set; }

        [DefaultValue(0)]
        public int total { get; set; }

        [DefaultValue(0)]
        public int records { get; set; }

        [DefaultValue(new string[0])]
        public List<T> rows { get; set; }

        public ResultSet()
        {
            rows = new List<T>();
        }
    }
}
