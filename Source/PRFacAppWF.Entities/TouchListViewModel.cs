﻿namespace PRFacAppWF.models
{
    public class TouchListViewModel
    {

        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public string Unit { get; set; }
        public string  ScheduleDate { get; set; }
        public string StartTime { get; set; }
        public string  EndTime { get; set; }
        public string Status { get; set; }
        public string Touchtype { get; set; }
        public int Qorid { get; set; }
        public string BolLink { get; set; }

        public string Time { get; set; }

        public string starsID { get; set; }

        public string TenantID { get; set; }


        public string TouchInstructions { get; set; }

        public string isConfirmed { get; set; }
        
        public string ProvidedETARange { get; set; }

        public PR.Entities.ETASettings ETASettings { get; set; }
    }
}