﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRFacAppWF.models
{
    public class ModuleAccessRelation
    {
        public int ModuleID { get; set; }
        public int AccessLevelID { get; set; }
    }
}
