﻿using PR.Entities;

namespace PRFacAppWF.models
{
    public class UnAssignedTouch
    {
        public TransiteOrder oTransiteOrder;
        public string FromSteet { get; set; }
        public string FromCity { get; set; }
        public string FromState { get; set; }
        public string FromCountry { get; set; }
        public string FromLatitude { get; set; }
        public string FromLongtitude { get; set; }
        public string ToSteet { get; set; }
        public string ToCity { get; set; }
        public string ToState { get; set; }
        public string ToCountry { get; set; }
        public string ToLatitude { get; set; }
        public string ToLongtitude { get; set; }
        public string FromZip { get; set; }
        public string ToZip { get; set; }
        public string Color { get; set; }
      
    }
}