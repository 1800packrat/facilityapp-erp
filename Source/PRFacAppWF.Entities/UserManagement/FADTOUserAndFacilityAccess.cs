﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.UserManagement
{
    public class FADTOUserAndFacilityAccess
    {
        public int PK_ID { get; set; }
        public int FK_UserID { get; set; }
        public int FK_StoreRowID { get; set; }
        public int FK_AccessLevelID { get; set; }
        public int FK_ApplicationID { get; set; }

        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public bool IsActive { get; set; }

        public virtual FADTOAccessLevels FAAccessLevels { get; set; }
        public virtual FADTOUserMaster FAUserMaster { get; set; }

        public Nullable<DateTime> EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public int FromFacilityID { get; set; }
        public int ToFacilityID { get; set; }
        public int FK_TransferStatusID { get; set; }
    }
}
