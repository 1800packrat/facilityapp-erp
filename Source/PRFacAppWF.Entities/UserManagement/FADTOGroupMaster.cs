﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.UserManagement
{
    public partial class FADTOGroupMaster
    {
        public FADTOGroupMaster()
        {
            this.FAGroupAndFacilityAccess = new HashSet<FADTOGroupAndFacilityAccess>();
            this.FAGroupAndModule = new HashSet<FADTOGroupAndModule>();
            this.FAUserMaster = new HashSet<FADTOUserMaster>();
        }

        public int PK_GroupId { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool HasFacilityAppAccess { get; set; }
        public bool HasDriverAppAccess { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDateTime { get; set; }

        public virtual ICollection<FADTOGroupAndFacilityAccess> FAGroupAndFacilityAccess { get; set; }
        public virtual ICollection<FADTOGroupAndModule> FAGroupAndModule { get; set; }
        public virtual ICollection<FADTOUserMaster> FAUserMaster { get; set; }
    }
}
