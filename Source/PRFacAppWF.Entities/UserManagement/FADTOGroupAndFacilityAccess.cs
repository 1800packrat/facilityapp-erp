﻿namespace PRFacAppWF.Entities.UserManagement
{
    using System;

    public partial class FADTOGroupAndFacilityAccess
    {
        public int PK_Id { get; set; }
        public int FK_GroupID { get; set; }
        public int FK_StoreRowID { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDateTime { get; set; }
        public bool IsActive { get; set; }

        public virtual FADTOGroupMaster FAGroupMaster { get; set; }
    }
}
