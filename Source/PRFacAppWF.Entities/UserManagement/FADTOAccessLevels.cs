﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.UserManagement
{
    public class FADTOAccessLevels
    {
        public FADTOAccessLevels()
        {
            this.FAGroupAndModule = new HashSet<FADTOGroupAndModule>();
            this.FAModule = new HashSet<FADTOModule>();
            this.FAUserAndFacilityAccess = new HashSet<FADTOUserAndFacilityAccess>();
        }

        public int PK_ID { get; set; }
        public string Access { get; set; }
        public bool Status { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }

        public virtual ICollection<FADTOGroupAndModule> FAGroupAndModule { get; set; }
        public virtual ICollection<FADTOModule> FAModule { get; set; }
        public virtual ICollection<FADTOUserAndFacilityAccess> FAUserAndFacilityAccess { get; set; }
    }
}
