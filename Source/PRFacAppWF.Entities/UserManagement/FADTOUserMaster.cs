﻿using PRFacAppWF.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PRFacAppWF.Entities.UserManagement
{
    public class FADTOUserMaster
    {
        public FADTOUserMaster()
        {
            this.FAUserAndFacilityAccess = new HashSet<FADTOUserAndFacilityAccess>();
        }

        public int PK_UserId { get; set; }
        public int FK_GroupID { get; set; }
        [Display(Name = "Email")]
        public string EmailAddress { get; set; }
        public string Username { get; set; }

        [DefaultValue("password")]
        public string Password { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PhoneNumber { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public bool IsLocked { get; set; }

        private bool isActive = true;
        public bool IsActive { get { return isActive; } set { isActive = value; } }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public Nullable<System.DateTime> ActivatedDateTime { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDateTime { get; set; }
        public Nullable<System.DateTime> LastLoginDateTime { get; set; }
        public bool IsChangePasswordRequired { get; set; }
        public Nullable<System.DateTime> PasswordUpdatedDate { get; set; }

        public virtual FADTOGroupMaster FAGroupMaster { get; set; }

        public virtual ICollection<FADTOUserAndFacilityAccess> FAUserAndFacilityAccess { get; set; }

        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        //[Display(Name = "Primary Facility")]
        //public int PrimaryFacilityID { get; set; }

        public FADTOFacility PrimaryFacility { get; set; }

        public List<FADTOFacility> AccessableFacilityList { get; set; }

        //public List<SelectListItem> SLItemAccessableFacilityList
        //{
        //    get
        //    {
        //        return AccessableFacilityList.Select(f => new SelectListItem { Value = f.StoreRowID.ToString(), Text = f.displayFacilityName, Selected = f.StoreRowID == PrimaryFacilityID ? true : false }).ToList();
        //    }
        //}

        [DefaultValue(1)]
        public int StatusID { get; set; }

        public string StatusName { get; set; }

        public List<FADTOUserStatus> StatusList { get; set; }
        //selected as false for pk_userid==0 as we are implementing add, please changing it during record updating.
        public IEnumerable<SelectListItem> SLItemStatusList
        {
            get
            {
                return StatusList.Select(f => new SelectListItem { Value = f.PK_ID.ToString(), Text = f.StatusName, Selected = f.PK_ID == StatusID ? true : false }).ToList();
            }
        }

        [Display(Name = "License Class")]
        public int LicenseClassID { get; set; }

        public List<FADTOLicenseClass> LicenseClassList { get; set; }

        public List<SelectListItem> SLItemLicenseClassList { get { return LicenseClassList.Select(f => new SelectListItem { Value = f.PK_ID.ToString(), Text = f.Name }).ToList(); } }

        public List<Entities.DriverSchedule.DriverFacility> DriverFacilityList { get; set; }

        //public List<FADTOFacility> AddDriverFacilityList { get; set; }

        public bool IsDriverApp { get; set; }

        public int? UniqueID { get; set; }
    }
}
