﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.UserManagement
{
    public class FADTOApplications
    {
        public int PK_ID { get; set; }
        public string ApplicationName { get; set; }
        public string DisplayName { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public bool Status { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public virtual ICollection<FADTOModuleArea> FAModuleArea { get; set; }
    }
}
