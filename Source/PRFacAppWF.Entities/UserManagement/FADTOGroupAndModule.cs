﻿namespace PRFacAppWF.Entities.UserManagement
{
    using Enums;
    using System;

    public class FADTOGroupAndModule
    {
        public int PK_Id { get; set; }
        public int FK_GroupId { get; set; }
        public int FK_ModuleID { get; set; }
        public Nullable<int> FK_AccessLevelID { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDateTime { get; set; }
        public bool IsActive { get; set; }
        
        public AccessLevel AccessLevel
        {
            get
            {
                return FK_AccessLevelID.HasValue ? (AccessLevel)FK_AccessLevelID.Value : AccessLevel.None;                
            }
        }

        public virtual FADTOAccessLevels FAAccessLevels { get; set; }
        public virtual FADTOGroupMaster FAGroupMaster { get; set; }
        public virtual FADTOModule FAModule { get; set; }
    }
}
