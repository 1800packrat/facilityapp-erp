﻿namespace PRFacAppWF.Entities.UserManagement
{
    using System;
    using System.Collections.Generic;
    using PRFacAppWF.Entities.UserManagement;
    using System.Linq;
    using Enums;

    public class AddEditRoleViewModel
    {
        public AddEditRoleViewModel()
        {

        }

        public FADTOGroupMaster FAGroupMaster { get; set; }

        public List<FADTOModuleArea> FAModuleArea { get; set; }

        public List<FADTOModule> FAModule { get; set; }

        public List<FADTOModule> FacilityAppModule
        {
            get
            {
                var lstModules = this.FAModule.Where(m => m.FAModuleArea.FK_ApplicationID == (int)Application.FacilityApp).ToList();


                lstModules.ForEach(m =>
                {
                    if (this.FAGroupMaster != null && this.FAGroupMaster.PK_GroupId > 0)
                    {
                        var FAAccess = this.FAGroupMaster.FAGroupAndModule.Where(g => g.FK_ModuleID == m.PK_ModuleID).FirstOrDefault();
                        m.FK_AccessLevelID = (FAAccess != null && FAAccess.PK_Id > 0) ? FAAccess.FK_AccessLevelID.Value : 0;
                    }
                });

                return lstModules;
            }
        }

        public List<FADTOModule> DriverAppModule
        {
            get
            {
                var lstModules = this.FAModule.Where(m => m.FAModuleArea.FK_ApplicationID == (int)Application.DriverApp).ToList();
                lstModules.ForEach(m =>
                {
                    if (this.FAGroupMaster != null && this.FAGroupMaster.PK_GroupId > 0)
                    {
                        var DAAccess = this.FAGroupMaster.FAGroupAndModule.Where(g => g.FK_ModuleID == m.PK_ModuleID).FirstOrDefault();
                        m.FK_AccessLevelID = (DAAccess != null && DAAccess.PK_Id > 0) ? DAAccess.FK_AccessLevelID.Value : 0;
                    }
                });

                return lstModules;
            }
        }        
    }
}
