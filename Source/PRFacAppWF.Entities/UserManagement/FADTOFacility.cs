﻿using PRFacAppWF.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.UserManagement
{
    public class FADTOFacility
    {
        public string CompDBAName { get; set; }

        public string FriendlyName { get; set; }

        public string Region { get; set; }

        public int StoreRowID { get; set; }

        public int StoreNo { get; set; }

        public int? MarketID { get; set; }

        public AccessLevel FAAccessLevel { get; set; }

        public AccessLevel DAAccessLevel { get; set; }

        public int FK_ApplicationTypeID { get; set; }

        public DateTime? DriverStartDate { get; set; }

        public DateTime? DriverEndDate { get; set; }

        public string DisplayDriverStartDate
        {
            get
            {
                return DriverStartDate.HasValue ? DriverStartDate.Value.ToString("MM/dd/yyyy") : string.Empty;
            }
        }

        public string DisplayDriverEndDate
        {
            get
            {
                return DriverEndDate.HasValue ? DriverEndDate.Value.ToString("MM/dd/yyyy") : string.Empty;
            }
        }

        public string displayFacilityName
        {
            get
            {
                //return FriendlyName + " - " + CompDBAName;
                return CompDBAName;
            }
        }

        public bool CanEdit
        {
            get { return DriverEndDate.HasValue == false || (DriverEndDate.HasValue == true && DriverEndDate.Value.Date > DateTime.Now.Date); }
        }

        public bool CanDelete
        {
            get { return DriverEndDate.HasValue == false || (DriverEndDate.HasValue == true && DriverEndDate.Value.Date > DateTime.Now.Date); }
        }

        public bool CanEditStartDate
        {
            get { return DriverStartDate.HasValue == false || (DriverStartDate.HasValue == true && DriverStartDate.Value.Date >= DateTime.Now.Date); }
        }

        public bool CanEditEndDate
        {
            get { return DriverEndDate.HasValue == false || (DriverEndDate.HasValue == true && DriverEndDate.Value.Date >= DateTime.Now.Date); }
        }
    }
}
