﻿using PRFacAppWF.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.UserManagement
{
    public partial class FADTOModule
    {
        public FADTOModule()
        {
        }

        public int PK_ModuleID { get; set; }
        public string ModuleName { get; set; }

        public string AreaName { get; set; }

        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDateTime { get; set; }
        public string Description { get; set; }
        public Nullable<int> FK_AreaID { get; set; }
        public Nullable<int> ParentID { get; set; }
        public int FK_AccessLevelID { get; set; }
        public AccessLevel AccessLevel
        {
            get
            {
                return (AccessLevel)FK_AccessLevelID;
            }
        }
        public virtual FADTOModuleArea FAModuleArea { get; set; }
        public virtual ICollection<FADTOGroupAndModule> FAGroupAndModule { get; set; }
        public virtual FADTOAccessLevels FAAccessLevels { get; set; }
    }
}