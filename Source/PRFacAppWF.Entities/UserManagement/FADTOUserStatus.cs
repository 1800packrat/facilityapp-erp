﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.UserManagement
{
    public class FADTOUserStatus
    {
        public int PK_ID { get; set; }

        public string StatusName { get; set; }

        public bool IsActive { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public int CreatedBy { get; set; }

        public Nullable<System.DateTime> UpdatedDate { get; set; }

        public Nullable<int> UpdatedBy { get; set; }
    }
}
