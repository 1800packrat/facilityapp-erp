﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.UserManagement
{
    public partial class FADTOModuleArea
    {
        public int PK_ID { get; set; }

        public int FK_ApplicationID { get; set; }

        public string Area { get; set; }

        public bool Status { get; set; }

        public int CreatedBy { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public Nullable<int> UpdatedBy { get; set; }

        public Nullable<System.DateTime> UpdatedDate { get; set; }

        public string Description { get; set; }

        public virtual ICollection<FADTOModule> FAModule { get; set; }

        public virtual FADTOApplications FAApplications { get; set; }
    }
}
