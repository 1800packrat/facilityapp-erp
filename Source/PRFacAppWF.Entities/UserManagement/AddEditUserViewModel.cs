﻿namespace PRFacAppWF.Entities.UserManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;

    public class AddEditUserViewModel
    {
        public AddEditUserViewModel()
        { }

        public FADTOUserMaster FAUserMaster { get; set; }

        public List<FADTOUserAndFacilityAccess> FAUserAndFacilityAccess { get; set; }

        private List<FADTOFacility> lstFacilities = new List<FADTOFacility>();
        public List<FADTOFacility> Facilities
        {
            get
            {
                lstFacilities.ForEach(f =>
                {
                    if (FAUserMaster != null && FAUserMaster.PK_UserId > 0)
                    {
                        var faAccess = FAUserMaster.FAUserAndFacilityAccess.Where(uf => uf.FK_StoreRowID == f.StoreRowID && uf.FK_ApplicationID == (int)Enums.Application.FacilityApp).FirstOrDefault();
                        f.FAAccessLevel = (faAccess != null && faAccess.PK_ID > 0) ? (Enums.AccessLevel)faAccess.FK_AccessLevelID : Enums.AccessLevel.None;

                        var daAccess = FAUserMaster.FAUserAndFacilityAccess.Where(uf => uf.FK_StoreRowID == f.StoreRowID && uf.FK_ApplicationID == (int)Enums.Application.DriverApp).FirstOrDefault();
                        f.DAAccessLevel = (daAccess != null && daAccess.PK_ID > 0) ? (Enums.AccessLevel)daAccess.FK_AccessLevelID : Enums.AccessLevel.None;
                    }
                });

                return lstFacilities;
            }
            set { lstFacilities = value; }
        }


        /// <summary>
        /// THIS SHOLD RETURN TRUE IF USER IS NEW OR USER DOES NOT HAVE ANY DIRVER APP FACILITIES ASSIGNED
        /// </summary>
        public bool CanEditDAFacilities
        {
            get
            {                
                return this.FAUserMaster.PK_UserId <= 0 || 
                    (this.FAUserMaster.PK_UserId > 0 && this.FAUserMaster.FAUserAndFacilityAccess.Any(uf => uf.FK_ApplicationID == (int)Enums.Application.DriverApp && uf.FK_AccessLevelID == (int)Enums.AccessLevel.PrimaryFacility) == false);
            }
        }

        public List<FADTOGroupMaster> Roles { get; set; }

        public IEnumerable<SelectListItem> UserRoles { get { return new SelectList(Roles, "PK_GroupId", "GroupName", (FAUserMaster != null ? FAUserMaster.FK_GroupID : -1)); } }

        public List<string> Regions
        {
            get
            {
                return Facilities.Select(f => f.Region).Distinct().ToList();
            }
        }        
    }
}
