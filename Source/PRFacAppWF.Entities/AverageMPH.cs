﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRFacAppWF.models
{
    public class AverageMPH
    {
        public long CAPAvgMPHId { get; set; }
        public Nullable<int> FacilityId { get; set; }
        public string FacilityName { get; set; }
        public string State { get; set; }
        public Nullable<System.DateTime> Startdate { get; set; }
        public Nullable<System.DateTime> Enddate { get; set; }
        public decimal AvgMPH { get; set; }
        //public int NoofTrucks { get; set; }
        public int Operatinghours { get; set; }
        public decimal Mileage { get; set; }
        public int Status { get; set; }
        public Nullable<System.DateTime> Nextdate { get; set; }
        public  decimal CorporateMPH { get; set; }
    }
}