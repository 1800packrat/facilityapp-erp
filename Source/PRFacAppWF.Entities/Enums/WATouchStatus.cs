﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Enums
{
    public enum WATouchStatus
    {
        Open = 0, ///This status will not exists in DB, but this we will get it from SiteLink
        Scheduled = 1,
        Stage = 2,
        Completed = 3,
        OnHold = 4,
        UnScheduled = 5 
    }
}
