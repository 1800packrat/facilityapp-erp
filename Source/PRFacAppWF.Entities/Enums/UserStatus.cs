﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Enums
{
    public enum UserStatus
    {

        Active = 1,
        Inactive = 2,
        In_Training = 3
    }
}
