﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Enums
{
    //public enum LoginStatus
    //{
    //    Success,
    //    InvalidCredentials,
    //    InactiveUser,
    //    InactiveRole
    //}

    //Need to reflect the FAEmailTypes table
    public enum EmailTypes
    {
        AutoPassword = 1,
        PasswordReset = 2,
        Scheduled = 3,
        Stage = 4,
        NoShow = 5,
        TouchCompleted = 6
    }
}
