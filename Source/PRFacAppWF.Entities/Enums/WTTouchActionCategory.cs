﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Enums
{
    public enum WTTouchActionCategory
    {
        None = 0,
        Stage = 1,
        NoShowDispatchAlerted = 2,
        NoShowLeaveStaged = 3,
        NoShowReturnToStack = 4,
        Completed = 5,
        ReturnToStack = 6,
        LeaveStaged = 7
    }
}
