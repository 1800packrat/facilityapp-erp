﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Enums
{
    public enum Application
    {
        FacilityApp = 1,
        DriverApp = 2
    }
}
