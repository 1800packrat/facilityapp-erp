﻿namespace PRFacAppWF.Entities.Enums
{
    ///This keys are same as module name in FAAccessLevel table
    ///In this enum Any is the option which is not present in FAAccessLevel table, this will be used to check whether he has access for this page or not
    public enum AccessLevel
    {
        Any = 0, //This option to show button to enter into particular page
        None = 1,
        ReadOnly = 2,
        ReadAndWrite = 3,
        PrimaryFacility = 4,
        SecondaryFacility = 5,
        Rover = 6
    }

    public enum UserFacilityAccessStatus
    {
        Permanent = 1,
        RoverIn = 2,
        RoverOut = 3
    }

    ///Here keys needs be added when you implement new feature into facility app 
    ///This keys are same as module name in FAModule table
    public enum ModuleName
    {
        AdminControls,
        AdvanceSearch,
        ApplicationSettings,
        AverageMPH,
        BillingInfo,
        calpopup,
        CorporateControls,
        CorporateReports,
        DashBoard,
        DispatchDashboard,
        DriverList,
        DriverMaintenance,
        FacCalendar,
        FacilityOperatingHour,
        FacilityOperations,
        FleetStatusReport,
        LocalLogistics,
        Pricing,
        promotions,
        QuickQuote,
        QuickQuoteResults,
        QuoteCancellation,
        RoleManagement,
        SearchQuote,
        SiteLinkMileage,
        Touches,
        TouchList,
        TouchTicket,
        TouchTime,
        TruckDownTime,
        TruckStatus,
        TruckTransferReason,
        UnitMaintenace,
        UnitMaintenance,
        UnitMaintenanceReport,
        UnitUnavailability,
        UserAssignment,
        Watouches,
        MatchingCustomers,
        WarehouseTouchContainerStatus,
        WeeklyDieselPrice
    }
}
