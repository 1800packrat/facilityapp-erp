﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Enums
{
    public enum DriverFacilityStatus
    {
        None = 0, //This status applies when the existing information is loaded.
        New = 1,
        Updated = 2,
        Deleted = 3
    }
}
