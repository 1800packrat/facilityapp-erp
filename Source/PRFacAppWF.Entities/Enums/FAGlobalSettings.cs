﻿namespace PRFacAppWF.Entities.Enums
{
    public enum FAGlobalSettingsKey
    {
        FAWTStartDate,
        FAWTEndDate,
        FAWTPRDispatch,
        FAWTCustomerLoyalty,
        FAWTCentralAdmin,
        FAWTShowPriorNotCompletedTouchesDays,
        SendGridAPIKey,
        DieselPriceFilterBeginDateInMonths,
        DieselPriceFilterEndDateInMonths,
        DieselPriceLocalTransSubsidyDefaultValue,
        IsGetLiveData
    }
}
