﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRFacAppWF.models
{
    public class TruckDowntimeModel
    {
        public long CAPTruckDowntimeId { get; set; }
        public Nullable<int> FacilityId { get; set; }
        public string FacilityName { get; set; }
        public string State { get; set; }
        public Nullable<System.DateTime> Startdate { get; set; }
        public Nullable<System.DateTime> Enddate { get; set; }
        public int TruckDowntime { get; set; }
        public int Operatinghours { get; set; }
        public int DowntimeMinutes { get; set; }
        public int Status { get; set; }
        public Nullable<System.DateTime> Nextdate { get; set; }
        public int CorporateDowntime { get; set; }
    }
}