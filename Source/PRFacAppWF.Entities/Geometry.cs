﻿namespace PRFacAppWF.models
{
    public class Geometry
    {
        public string LocationType { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Address { get; set; }
        public string TouchType { get; set; }
        public string StopType { get; set; }
        public string QORId { get; set; }
        public string OrderNum { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public bool IsFake { get; set; } 
    }
}