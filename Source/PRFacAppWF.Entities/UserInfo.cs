﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRFacAppWF.models
{
    public class UserInfo
    {
        public int UserId { get; set; }

        public string Username { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string PhoneNumber { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public bool IsLocked { get; set; }

        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime? ActivatedDateTime { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDateTime { get; set; }

        public DateTime? LastLoginDateTime { get; set; }
    }
}