﻿using PR.Entities;
using System.Collections.Generic;

namespace PRFacAppWF.models
{
    public class DriversModel
    {
        public List<DriverTouch> lDriverTouch { get; set; }
        public string DriverName { get; set; }
        public string DriverId { get; set; }
        public string Date { get; set; }
        public bool CanDeleteLoad { get; set; }
        public bool IsTrailerRequired { get; set; }

        public string TruckNumber { get; set; }

        public bool IsLocked { get; set; }
        public string TrailerNumber { get; set; }
    }
}