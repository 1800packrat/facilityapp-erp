﻿using System;
using System.Collections.Generic;

namespace PRFacAppWF.models
{
    #region UnitAvailabilityInfo

    public class UnitAvailabilityInfo
    {
        public UnitAvailabilityInfo()
        {
        }

        public UnitAvailabilityInfo(int UnitAvailabilityInfoId, string FacilityName, int UnitSize)
        {
            this.UnitAvailabilityInfoId = UnitAvailabilityInfoId;
            this.FacilityName = FacilityName;
            this.UnitSize = UnitSize;
        }

        public int UnitAvailabilityInfoId { get; set; }

        public string FacilityName { get; set; }

        public int StoreNo { get; set; }

        public int UnitSize { get; set; }

        public int CreatedBy { get; set; }

        public string ReportedBy { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ThroughDate { get; set; }
    }

    public class UnitAvailability
    {
        public DateTime UADate { get; set; }

        public List<UnitAvailabilityInfo> UnitAvailabilityInfo { get; set; }
    }

    public class lstUnitUnAvailability
    {
        public tblunitAvailability Head = new tblunitAvailability();

        public List<tblunitAvailability> unitDetails = new List<tblunitAvailability>();
    }

    public class tblunitAvailability
    {
        public string Head { get; set; }

        public string Day1 { get; set; }

        public string Day1Id { get; set; }

        public string Day2 { get; set; }

        public string Day2Id { get; set; }

        public string Day3 { get; set; }

        public string Day3Id { get; set; }

        public string Day4 { get; set; }

        public string Day4Id { get; set; }

        public string Day5 { get; set; }

        public string Day5Id { get; set; }

        public string Day6 { get; set; }

        public string Day6Id { get; set; }

        public string Day7 { get; set; }

        public string Day7Id { get; set; }
    }

    #endregion

    #region Truck Operational Hours

    public class TruckOpHours
    {
        public DateTime TruckOpHoursDate { get; set; }

        public List<TruckOpHoursInfo> TruckOpHoursInfo { get; set; }
    }

    public class FacilityOpHours
    {
        public DateTime CapacityDate { get; set; }
        public int FacilityOperatingMiuntes { get; set; }
    }

    #endregion
}