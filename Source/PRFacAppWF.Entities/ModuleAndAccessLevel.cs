﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities
{
    using Enums;

    public class ModuleAndAccessLevel
    {
        public int ModuleId { get; set; }

        public string ModuleName { get; set; }

        public AccessLevel AccessLevel { get; set; }
    }
}
