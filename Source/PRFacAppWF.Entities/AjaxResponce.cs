﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities
{
    public class AjaxResponse<T>
    {
        public string message { get; set; }

        public bool success { get; set; }

        public T data { get; set; }
    }
}
