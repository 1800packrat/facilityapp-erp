﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Touches
{
    public class WATouchTypesAndActionCategory
    {
        public bool CanUpdateStatusInSL { get; set; }

        public bool CanSendNotification { get; set; }

        public string FK_EmailTemplateIDs { get; set; }

        public int[] EmailTemplateIDs { get { return FK_EmailTemplateIDs.Split(',').Select(x => int.Parse(x)).ToArray(); } }

        public bool CanUpdateLocalStatus { get; set; }

        public int? UpdateLocalStatusID { get; set; }

        public string UpdateLocalStatusName { get; set; }
    }
}
