﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Touches
{
    public class FAWTTouchStatus
    {
        public int TouchStatusID { get; set; }

        public string StatusName { get; set; }

        public string DispStatusName { get; set; }

        public Nullable<int> SequenceOrder { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }
    }
}
