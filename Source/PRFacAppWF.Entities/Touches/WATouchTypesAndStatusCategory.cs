﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Touches
{
    public class WATouchTypesAndStatus
    {
        public bool CanUpdateStatusInSL { get; set; }

        public bool CanSendNotification { get; set; }

        public int? FK_EmailTemplateID { get; set; }

    }
}
