﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Touches
{
    public class WATouchInfoHistory
    {
        public string DispStatusName { get; set; }
        public string ActionName { get; set; }
        public string Comments { get; set; }
        public DateTime HistCreatedDate { get; set; }
    }
}
