﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Touches
{
    public class EmailTemplate
    {
        public int TemplateId { get; set; }

        public string Subject { get; set; }

        public string EmailTemplateBody { get; set; }
    }
}
