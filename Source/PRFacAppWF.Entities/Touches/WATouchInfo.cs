﻿namespace PRFacAppWF.Entities.Touches
{
    using Enums;
    using System;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    public class WATouchInfo
    {
        public int RowID { get; set; }

        public int AlertLevel { get; set; }

        public string TouchKey
        {
            get { return string.Format("{0}_{1}_{2}", QORId, TouchType, SequenceNum); } //Used in TS files
        }

        public bool IsLdmOrder { get; set; }

        public int StoreNumber { get; set; }

        public int QORId { get; set; }

        public string UnitName { get; set; }

        public string UnitDescription { get; set; }

        public int? SequenceNum { get; set; }

        public int? StarsId { get; set; }

        public int? StarsUnitId { get; set; }

        public bool IsLDMOrderTouch { get; set; }

        public string TouchTypeFull { get; set; }

        public string TouchType { get { return GetTouchType(TouchTypeFull); } }

        public bool IsZippyShellQuote { get; set; }

        public int? TenantID { get; set; }

        public string Phonenumber { get; set; }

        public DateTime? ScheduledDate { get; set; }

        public DateTime? CompletedDateTime { get; set; }

        public DateTime? SLScheduledDate { get; set; }

        public string DispScheduledDate { get { return ScheduledDate?.ToString("MM/dd/yyyy"); } } //Used in TS files

        public string DispSLScheduledDate { get { return SLScheduledDate?.ToString("MM/dd/yyyy"); } } //Used in TS files

        public string TouchTime { get; set; }

        public string ETAActualTime
        {
            get
            {
                return ETASettings != null ? ETASettings.ProvidedETARange : string.Empty; //Used in TS files
            }
        }

        public PR.Entities.ETASettings ETASettings { get; set; }

        public string RentalStatus { get; set; }

        public bool CanUpdateTouchStatusID { get; set; }

        public int TouchStatusID { get; set; }

        public WATouchStatus TouchStatus { get; set; }

        public string LocalTouchStatus { get { return TouchStatus.ToString(); } } //Used in TS files

        [Description("If SiteLink touch status scheduled then take local status other wise consider SL status as final status")]
        public string DispTouchStatus
        {
            get
            {
                if (SLTouchStatus == WATouchStatus.Scheduled.ToString())
                {
                    //If status is staging then append action name to show exact place where he staged container
                    if (TouchStatus == WATouchStatus.Stage)
                    {
                        return TouchStatus.ToString() + (string.IsNullOrEmpty(StageActionName) ? string.Empty : " - " + StageActionName);
                    }

                    return TouchStatus.ToString();
                }
                else
                    return SLTouchStatus;
            }
        }

        public int? StageActionID { get; set; }
        public string StageActionName { get; set; }

        public string SLTouchStatus { get; set; }

        public string BOL { get; set; }

        public DateTime? dDelivered { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public decimal? dcMileage { get; set; }

        //public string StartTime { get; set; }

        //public string EndTime { get; set; }

        public int? QTStatusID { get; set; }

        public string Comments { get; set; }

        /// <summary>
        /// SiteInspection = 1,
        /// WarehouseToCurbEmpty = 2,
        /// WarehouseToCurbFull = 3,
        /// CurbToCurb = 4,
        /// ReturnToWarehouseEmpty = 5,
        /// ReturnToWarehouseFull = 6,
        /// WarehouseAccess = 7,
        /// InByOwner = 8,
        /// OutByOwner = 9,
        /// LDMTransferIn = 10,
        /// LDMTransferOut = 11
        /// </summary>
        /// <param name="TouchType"></param>
        /// <returns></returns>
        private string GetTouchType(string TouchType)
        {
            switch (TouchType)
            {
                case "Deliver Empty":               // From Salesforce
                case "WarehouseToCurbEmpty":        // From Sitelink
                    return "DE";
                case "Deliver Full":                // From Salesforce
                case "WarehouseToCurbFull":         // From Sitelink
                    return "DF";
                case "Curb-to-Curb":                // From Salesforce
                case "CurbToCurb":                  // From Sitelink
                    return "CC";
                case "Return Empty":                // From Salesforce
                case "ReturnToWarehouseEmpty":      // From Sitelink
                    return "RE";
                case "Return Full":                 // From Salesforce
                case "ReturnToWarehouseFull":       // From Sitelink
                    return "RF";
                case "WarehouseAccess":             // From Sitelink
                    return "WA";
                case "InByOwner":                   // From Sitelink
                    return "IBO";
                case "OutByOwner":                  // From Sitelink
                    return "OBO";
                case "Long-Distance Move: In at Destination Warehouse": // From Salesforce
                case "LDMTransferIn":               // From Sitelink
                    return "TI";
                case "Long-Distance Move: Out from Origin Location":    // From Salesforce
                case "LDMTransferOut":              // From Sitelink
                    return "TO";

                default:
                    return TouchType;
            }
        }

        public int ActionID { get; set; }

        public string ActionName { get; set; }

        public string ActionNameWithCategory
        {
            get
            {
                //For staging actions, we need to append category to display proper info
                if (TouchActionCategory == WTTouchActionCategory.Stage)
                {
                    return TouchActionCategory.ToString() + (string.IsNullOrEmpty(StageActionName) ? string.Empty : " - " + StageActionName);   //Used in TS files
                }

                return ActionName;
            }
        }

        public WTTouchActionCategory TouchActionCategory { get; set; }

        public string DispTouchActionCategory { get { return TouchActionCategory.ToString(); } }  //Used in TS files

        public bool IsChangeAction { get; set; }

        public bool IsCommentsUpdated { get; set; }

        public bool IsConflictWithScheduleDate { get { return DispTouchStatus != "Completed" && ScheduledDate.HasValue && SLScheduledDate.HasValue && !ScheduledDate.Value.Date.Equals(SLScheduledDate.Value.Date); } }

        public bool IsTouchMissed { get; set; }

        public bool ByPassActionChangeToSyncData { get; set; }


        [Description("This property used to update status two time, one is for history other for future purpose(Ex. NoShow and ReturnToStack status shold become none action)")]
        [IgnoreDataMember]
        public int UpdateCustomActionID { get; set; }

        [Description("This property used to update status two time, one is for history other for future purpose(Ex. NoShow and ReturnToStack status shold become none action)")]
        [IgnoreDataMember]
        public bool DontChangeStatusForTheFirstUpdateButChangeInSecondUpdate { get; set; }


        #region Email Properties
        public string CustAddress1 { get; set; }
        public string CustAddress2 { get; set; }
        public string CustCity { get; set; }
        public string CustRegion { get; set; }
        public string CustZip { get; set; }

        public string FromFirstName { get; set; }
        public string FromLastName { get; set; }
        public string FromPhone { get; set; }
        public string FromAddr1 { get; set; }
        public string FromAddr2 { get; set; }
        public string FromCity { get; set; }
        public string FromState { get; set; }
        public string FromZip { get; set; }

        public string ToFirstName { get; set; }
        public string ToLastName { get; set; }
        public string ToPhone { get; set; }
        public string ToAddr1 { get; set; }
        public string ToAddr2 { get; set; }
        public string ToCity { get; set; }
        public string ToState { get; set; }
        public string ToZip { get; set; }

        public string DriverID { get; set; }
        public string DriverName { get; set; }
        public string PurchaseOrder { get; set; }
        public string TripNumber { get; set; }
        #endregion
    }
}
