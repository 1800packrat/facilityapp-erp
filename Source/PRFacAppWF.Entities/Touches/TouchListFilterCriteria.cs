﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.Touches
{
    public class TouchListFilterCriteria : jqGridRequest
    {
        public TouchListFilterCriteria() { }

        public DateTime StartDate { get; set; }

        public string LocationCode { get; set; }

        public DateTime EndDate { get; set; }

        public int TotalDays { get { return (EndDate - StartDate).Days; } }

        public string Status { get; set; }

        public List<FAWTTouchStatus> StatusTypes { get; set; }

        public string CustomerName { get; set; }

        public string TouchType { get; set; }

        /// <summary>
        /// This property will search for Ordernumber, Unit number and STARS Id
        /// </summary>
        public string TouchIdentificationNumber { get; set; }

        public bool IsShowCompletedTouches { get; set; }

        public bool IsShowPriorNotCompletedTouches { get; set; }

        public bool IsDETouches { get; set; }

        public bool IsRFTouches { get; set; }

        public bool IsCCToches { get; set; }

        public bool IsDFTouches { get; set; }

        public bool IsRETouches { get; set; }

        public bool IsWATouches { get; set; }

        public bool IsLDMOutTouches { get; set; }

        public bool IsLDMInTouches { get; set; }

        public bool IsIBOTouches { get; set; }

        public bool IsOBOTouches { get; set; }
    }
}
