﻿namespace PRFacAppWF.Entities.Touches
{
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class FAWTTouchActions
    {
        public FAWTTouchActions()
        {
            ActionID = 0;
            ActionCategoryID = 0;
            ActionName = "None";
        }

        public int ActionID { get; set; }

        public Nullable<int> FK_ActionID { get; set; }

        public string ActionName { get; set; }

        public string DispActionCategoryName { get; set; }

        public string ActionCategoryName { get; set; }

        public Nullable<int> ActionCategoryID { get; set; }

        public WTTouchActionCategory ActionCategory { get; set; }
        
        public Nullable<int> StoreNumber { get; set; }

        public int? SequenceOrder { get; set; }
        
        public string Description { get; set; }

        [Description("A: Active and N: In-Active")]
        public string Status { get; set; }

        public int IsGlobalStatus { get; set; }

        public bool IsActive { get; set; }
    }
}