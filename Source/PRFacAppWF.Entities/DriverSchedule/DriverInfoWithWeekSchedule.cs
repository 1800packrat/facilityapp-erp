﻿using PRFacAppWF.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities
{
    public class DriverInfoWithWeekSchedule
    {
        public int PK_ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get { return FirstName + " " + LastName; } }

        public string FacilityName { get; set; }

        public string LicenseName { get; set; }

        public FADTODriverSchedule Day1 { get; set; }

        public FADTODriverSchedule Day2 { get; set; }

        public FADTODriverSchedule Day3 { get; set; }

        public FADTODriverSchedule Day4 { get; set; }

        public FADTODriverSchedule Day5 { get; set; }

        public FADTODriverSchedule Day6 { get; set; }

        public FADTODriverSchedule Day7 { get; set; }

        public string DispTotalWorkHours
        {
            get
            {
                long totalWorkMinutes = Day1.HoursDifference + Day2.HoursDifference + Day3.HoursDifference + Day4.HoursDifference + Day5.HoursDifference + Day6.HoursDifference + Day7.HoursDifference;

                TimeSpan ts = TimeSpan.FromMinutes(totalWorkMinutes);
                return string.Format("{0} hr {1} min", (int)ts.TotalHours, ts.Minutes);
                //return string.Format("{0:%h} hr {0:%m} min", ts);
            }
        }
    }

    public class DriverWeekSchedule
    {
        public int DriverScheduleID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get { return FirstName + " " + LastName; } }

        public string FacilityName { get; set; }

        public string LicenseName { get; set; }

        public DateTime ScheduleDate { get; set; }
        
        public int LLogisticsUserID { get; set; }
        
        public System.TimeSpan ScheduleStartTime { get; set; }

        public System.TimeSpan ScheduleEndTime { get; set; }

        public string DayWork { get; set; }

        public int StoreNo { get; set; }

        public string AccessType { get; set; }

        public string AccessStatus { get; set; }

        public UserFacilityAccessStatus UserFacilityAccessStatus
        {
            get {
                if (!string.IsNullOrEmpty(AccessStatus))
                    return (UserFacilityAccessStatus)Enum.Parse(typeof(UserFacilityAccessStatus), AccessStatus);
                else
                    return PRFacAppWF.Entities.Enums.UserFacilityAccessStatus.Permanent;
            }
        }

        public int PrimaryStoreNo { get; set; }
        
        public string StoreName { get; set; }

        public string PrimaryStoreName { get; set; }
    }
}
