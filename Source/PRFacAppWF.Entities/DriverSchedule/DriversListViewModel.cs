﻿namespace PRFacAppWF.Entities
{
    using models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class DriversListViewModel
    {
        public DriversListViewModel()
        {
            DriverScheduleList = new List<DriverInfoWithWeekSchedule>();
        }

        public List<DriverInfoWithWeekSchedule> DriverScheduleList { get; set; }

        public DriverSummaryForDriverScreen DriverScheduleSummary
        {
            get
            {
                DriverSummaryForDriverScreen dis = new DriverSummaryForDriverScreen();

                dis.Day1 = new DayScheduleHours(DriverScheduleList.Sum(s => s.Day1.HoursDifference));
                dis.Day2 = new DayScheduleHours(DriverScheduleList.Sum(s => s.Day2.HoursDifference));
                dis.Day3 = new DayScheduleHours(DriverScheduleList.Sum(s => s.Day3.HoursDifference));
                dis.Day4 = new DayScheduleHours(DriverScheduleList.Sum(s => s.Day4.HoursDifference));
                dis.Day5 = new DayScheduleHours(DriverScheduleList.Sum(s => s.Day5.HoursDifference));
                dis.Day6 = new DayScheduleHours(DriverScheduleList.Sum(s => s.Day6.HoursDifference));
                dis.Day7 = new DayScheduleHours(DriverScheduleList.Sum(s => s.Day7.HoursDifference));

                return dis;
            }
        }

        public DriverListHead GridHead { get; set; }

        public List<FacilityCapacity> TrucksHours = new List<FacilityCapacity>();

        public FacilityCapacity TruckHoursSummary
        {
            get
            {
                FacilityCapacity ths = new FacilityCapacity();

                ths.oDay1 = new TruckOHDay(TrucksHours.Sum(th => th.oDay1.DayOpMins));
                ths.oDay2 = new TruckOHDay(TrucksHours.Sum(th => th.oDay2.DayOpMins));
                ths.oDay3 = new TruckOHDay(TrucksHours.Sum(th => th.oDay3.DayOpMins));
                ths.oDay4 = new TruckOHDay(TrucksHours.Sum(th => th.oDay4.DayOpMins));
                ths.oDay5 = new TruckOHDay(TrucksHours.Sum(th => th.oDay5.DayOpMins));
                ths.oDay6 = new TruckOHDay(TrucksHours.Sum(th => th.oDay6.DayOpMins));
                ths.oDay7 = new TruckOHDay(TrucksHours.Sum(th => th.oDay7.DayOpMins));

                return ths;
            }
        }

        public ActiveTrucksForDriverScreen ActiveTrucks
        {
            get
            {
                ActiveTrucksForDriverScreen atds = new ActiveTrucksForDriverScreen();

                atds.Day1 = TrucksHours.Count(a => a.oDay1.DayOpMins > 0);
                atds.Day2 = TrucksHours.Count(a => a.oDay2.DayOpMins > 0);
                atds.Day3 = TrucksHours.Count(a => a.oDay3.DayOpMins > 0);
                atds.Day4 = TrucksHours.Count(a => a.oDay4.DayOpMins > 0);
                atds.Day5 = TrucksHours.Count(a => a.oDay5.DayOpMins > 0);
                atds.Day6 = TrucksHours.Count(a => a.oDay6.DayOpMins > 0);
                atds.Day7 = TrucksHours.Count(a => a.oDay7.DayOpMins > 0);

                return atds;
            }
        }

        public DifTruckDriverScheduleHours DifTruckDriverHours
        {
            get
            {
                DifTruckDriverScheduleHours dtdsh = new DifTruckDriverScheduleHours(DriverScheduleSummary, TruckHoursSummary);
                return dtdsh;
            }
        }
    }

    public class DriverListHead
    {
        private DateTime _startDate = DateTime.Now;
        public DriverListHead(DateTime StartDate)
        {
            _startDate = StartDate;
        }

        public string col1 { get { return "Driver Name"; } }

        public string col2 { get { return "Facility"; } }

        public string col3 { get { return "License"; } }

        public string col4 { get { return string.Format("{0}<br/>{1}", _startDate.DayOfWeek.ToString(), _startDate.ToString("MM/dd/yyyy")); } }

        public string col5 { get { return string.Format("{0}<br/>{1}", _startDate.AddDays(1).DayOfWeek.ToString(), _startDate.AddDays(1).ToString("MM/dd/yyyy")); } }

        public string col6 { get { return string.Format("{0}<br/>{1}", _startDate.AddDays(2).DayOfWeek.ToString(), _startDate.AddDays(2).ToString("MM/dd/yyyy")); } }

        public string col7 { get { return string.Format("{0}<br/>{1}", _startDate.AddDays(3).DayOfWeek.ToString(), _startDate.AddDays(3).ToString("MM/dd/yyyy")); } }

        public string col8 { get { return string.Format("{0}<br/>{1}", _startDate.AddDays(4).DayOfWeek.ToString(), _startDate.AddDays(4).ToString("MM/dd/yyyy")); } }

        public string col9 { get { return string.Format("{0}<br/>{1}", _startDate.AddDays(5).DayOfWeek.ToString(), _startDate.AddDays(5).ToString("MM/dd/yyyy")); } }

        public string col10 { get { return string.Format("{0}<br/>{1}", _startDate.AddDays(6).DayOfWeek.ToString(), _startDate.AddDays(6).ToString("MM/dd/yyyy")); } }

        //public string col11 { get { return string.Format("{0}<br/>{1}", _startDate.AddDays(7).DayOfWeek.ToString(), _startDate.AddDays(7).ToString("MM/dd")); } }

        public string col11 = "Total";
    }

    public class ActiveTrucksForDriverScreen
    {
        public int Day1 { get; set; }

        public int Day2 { get; set; }

        public int Day3 { get; set; }

        public int Day4 { get; set; }

        public int Day5 { get; set; }

        public int Day6 { get; set; }

        public int Day7 { get; set; }

        public int Total { get { return Day1 + Day2 + Day3 + Day4 + Day5 + Day6 + Day7; } }
    }

    public class DriverSummaryForDriverScreen
    {
        public DayScheduleHours Day1 { get; set; }

        public DayScheduleHours Day2 { get; set; }

        public DayScheduleHours Day3 { get; set; }

        public DayScheduleHours Day4 { get; set; }

        public DayScheduleHours Day5 { get; set; }

        public DayScheduleHours Day6 { get; set; }

        public DayScheduleHours Day7 { get; set; }

        public DayScheduleHours Total { get { return new DayScheduleHours(Day1.WorkInMinutes + Day2.WorkInMinutes + Day3.WorkInMinutes + Day4.WorkInMinutes + Day5.WorkInMinutes + Day6.WorkInMinutes + Day7.WorkInMinutes); } }
    }

    public class DayScheduleHours
    {
        public DayScheduleHours(int minutes) { this.WorkInMinutes = minutes; }

        public int WorkInMinutes { get; set; }

        public string DispWorkTime
        {
            get
            {
                TimeSpan ts = TimeSpan.FromMinutes(WorkInMinutes);
                return string.Format("{0} hr {1} min", (int)ts.TotalHours, ts.Minutes);
                //return string.Format("{0:%h} hr {0:%m} min", ts);
            }
        }
    }

    public class DifTruckDriverScheduleHours
    {
        DriverSummaryForDriverScreen DriverSchedule = new DriverSummaryForDriverScreen();
        FacilityCapacity TruckSchedule = new FacilityCapacity();

        public DifTruckDriverScheduleHours(DriverSummaryForDriverScreen dsfds, FacilityCapacity ts)
        {
            this.DriverSchedule = dsfds;
            this.TruckSchedule = ts;

        }
        public int Day1DiffTruckDriver
        {
            get
            {
                return Convert.ToInt32(TruckSchedule.oDay1.DayOpMins) - Convert.ToInt32(DriverSchedule.Day1.WorkInMinutes);
            }
        }

        public string Day1DiffTruckDriverDisp
        {
            get
            {
                // return Convert.ToInt32(TruckSchedule.oDay1.DayOpMins) - Convert.ToInt32(DriverSchedule.Day1.WorkInMinutes);
                TimeSpan ts = TimeSpan.FromMinutes(Day1DiffTruckDriver);
                return string.Format("{0} hr {1} min", (int)ts.TotalHours, ts.Minutes);
            }
        }

        public int Day2DiffTruckDriver
        {
            get
            {
                return Convert.ToInt32(TruckSchedule.oDay2.DayOpMins) - Convert.ToInt32(DriverSchedule.Day2.WorkInMinutes);
            }
        }

        public string Day2DiffTruckDriverDisp
        {
            get
            {
                // return Convert.ToInt32(TruckSchedule.oDay1.DayOpMins) - Convert.ToInt32(DriverSchedule.Day1.WorkInMinutes);
                TimeSpan ts = TimeSpan.FromMinutes(Day2DiffTruckDriver);
                return string.Format("{0} hr {1} min", (int)ts.TotalHours, ts.Minutes);
            }
        }
        public int Day3DiffTruckDriver
        {
            get
            {
                return Convert.ToInt32(TruckSchedule.oDay3.DayOpMins) - Convert.ToInt32(DriverSchedule.Day3.WorkInMinutes);
            }
        }

        public string Day3DiffTruckDriverDisp
        {
            get
            {
                // return Convert.ToInt32(TruckSchedule.oDay1.DayOpMins) - Convert.ToInt32(DriverSchedule.Day1.WorkInMinutes);
                TimeSpan ts = TimeSpan.FromMinutes(Day3DiffTruckDriver);
                return string.Format("{0} hr {1} min", (int)ts.TotalHours, ts.Minutes);
            }
        }
        public int Day4DiffTruckDriver
        {
            get
            {
                return Convert.ToInt32(TruckSchedule.oDay4.DayOpMins) - Convert.ToInt32(DriverSchedule.Day4.WorkInMinutes);
            }
        }

        public string Day4DiffTruckDriverDisp
        {
            get
            {
                // return Convert.ToInt32(TruckSchedule.oDay1.DayOpMins) - Convert.ToInt32(DriverSchedule.Day1.WorkInMinutes);
                TimeSpan ts = TimeSpan.FromMinutes(Day4DiffTruckDriver);
                return string.Format("{0} hr {1} min", (int)ts.TotalHours, ts.Minutes);
            }
        }

        public int Day5DiffTruckDriver
        {
            get
            {
                return Convert.ToInt32(TruckSchedule.oDay5.DayOpMins) - Convert.ToInt32(DriverSchedule.Day5.WorkInMinutes);
            }
        }

        public string Day5DiffTruckDriverDisp
        {
            get
            {
                // return Convert.ToInt32(TruckSchedule.oDay1.DayOpMins) - Convert.ToInt32(DriverSchedule.Day1.WorkInMinutes);
                TimeSpan ts = TimeSpan.FromMinutes(Day5DiffTruckDriver);
                return string.Format("{0} hr {1} min", (int)ts.TotalHours, ts.Minutes);
            }
        }
        public int Day6DiffTruckDriver
        {
            get
            {
                return Convert.ToInt32(TruckSchedule.oDay6.DayOpMins) - Convert.ToInt32(DriverSchedule.Day6.WorkInMinutes);
            }
        }

        public string Day6DiffTruckDriverDisp
        {
            get
            {
                // return Convert.ToInt32(TruckSchedule.oDay1.DayOpMins) - Convert.ToInt32(DriverSchedule.Day1.WorkInMinutes);
                TimeSpan ts = TimeSpan.FromMinutes(Day6DiffTruckDriver);
                return string.Format("{0} hr {1} min", (int)ts.TotalHours, ts.Minutes);
            }
        }

        public int Day7DiffTruckDriver
        {
            get
            {
                return Convert.ToInt32(TruckSchedule.oDay7.DayOpMins) - Convert.ToInt32(DriverSchedule.Day7.WorkInMinutes);
            }
        }

        public string Day7DiffTruckDriverDisp
        {
            get
            {
                // return Convert.ToInt32(TruckSchedule.oDay1.DayOpMins) - Convert.ToInt32(DriverSchedule.Day1.WorkInMinutes);
                TimeSpan ts = TimeSpan.FromMinutes(Day7DiffTruckDriver);
                return string.Format("{0} hr {1} min", (int)ts.TotalHours, ts.Minutes);
            }
        }


        public int TotalDiffTruckDriver
        {
            get
            {
                return Convert.ToInt32(TruckSchedule.Total.DayOpMins) - Convert.ToInt32(DriverSchedule.Total.WorkInMinutes);
            }
        }

        public string TotalDiffTruckDriverDisp
        {
            get
            {
                // return Convert.ToInt32(TruckSchedule.oDay1.DayOpMins) - Convert.ToInt32(DriverSchedule.Day1.WorkInMinutes);
                TimeSpan ts = TimeSpan.FromMinutes(TotalDiffTruckDriver);
                return string.Format("{0} hr {1} min", (int)ts.TotalHours, ts.Minutes);
            }
        }
    }
}
