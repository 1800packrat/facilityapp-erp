﻿using PRFacAppWF.Entities.Enums;
using PRFacAppWF.Entities.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.DriverSchedule
{
    public class DriverFacility : FADTOFacility
    {
        public int DriverId { get; set; }

        public int UserFacilityID { get; set; }

        public int Index { get; set; }

        public int FK_AccessLevelID { get; set; }
        
        [System.ComponentModel.DefaultValue("new")]
        public DriverFacilityStatus DFStatus { get; set; }

        public IEnumerable<FADTOFacility> AccessableFacilityList { get; set; }
    }
}
