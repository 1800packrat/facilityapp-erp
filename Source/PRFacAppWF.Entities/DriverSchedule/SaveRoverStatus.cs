﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.DriverSchedule
{
    public enum SaveRoverStatus
    {
        CanSave = 0,
        NotPrimaryFacility = 1,
        DatesOverLap = 2,
        LoadExistsCannotUpdate = 3
    }
}
