﻿using PRFacAppWF.Entities.Enums;
using PRFacAppWF.Entities.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.DriverSchedule
{
    public class SaveDriverRequest
    {
        public int PK_UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        //public int PrimaryFacilityID { get; set; }

        //public DateTime StartDate { get; set; }

        public int LicenseClassID { get; set; }

        //public int StatusID { get; set; }

        public UserStatus StatusID { get; set; }

        public bool IsStatusActive { get { return StatusID == UserStatus.Active; } }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string EncryptPassword { get; set; }

        public string EmailAddress { get; set; }

        public int GroupMasterID { get; set; }

        public List<DriverFacility> DriverFacility { get; set; }
    }
}
