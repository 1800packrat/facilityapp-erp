﻿using PRFacAppWF.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities
{
    public class FADTODriverSchedule
    {
        public FADTODriverSchedule() { }

        public int DriverScheduleID { get; set; }

        public System.DateTime ScheduleDate { get; set; }

        public System.TimeSpan ScheduleStartTime { get; set; }

        public UserFacilityAccessStatus UserFacilityAccessStatus { get; set; }

        public System.TimeSpan ScheduleEndTime { get; set; }

        public string DayWork { get; set; }

        public string StrDayWork
        {
            get
            {
                string dispDayWork = "Off";

                if (DayWork == DriverDayOfWork.S.ToString())
                {
                    dispDayWork = "Schedule";
                }
                else if (DayWork == DriverDayOfWork.O.ToString())
                {
                    dispDayWork = "On-Call";
                }
                else if (DayWork == DriverDayOfWork.C.ToString())
                {
                    dispDayWork = "Off";
                }
                else
                {
                    dispDayWork = "N/A";
                }

                return dispDayWork;
            }
        }

        public bool Status { get; set; }

        //public bool IsHoliday { get { return ScheduleDate.DayOfWeek == DayOfWeek.Saturday || ScheduleDate.DayOfWeek == DayOfWeek.Sunday; } }

        public string DispDayDate
        {
            get
            {
                if (UserFacilityAccessStatus == UserFacilityAccessStatus.RoverOut)
                {
                    return "Rover";
                }
                else if (DriverScheduleID > 0 && (ScheduleEndTime - ScheduleStartTime).TotalMinutes != 0 && DayWork == DriverDayOfWork.S.ToString())
                {                    
                    return string.Format("{0}&nbsp;-&nbsp;{1}<br />{2}", StartTime, EndTime, DispHoursDifference);
                }
                //IF schedule hours are zero, then in main grid we will show as it is off status
                else if ((ScheduleEndTime - ScheduleStartTime).TotalMinutes <= 0 && DayWork == DriverDayOfWork.S.ToString())
                {
                    return "Off";
                }
                else
                {
                    return StrDayWork;
                }
            }
        }

        private string StartTime
        {
            get
            {
                DateTime dateTime = DateTime.Today.Add(ScheduleStartTime);
                return dateTime.ToString("hh:mm tt");
            }
        }

        private string EndTime
        {
            get
            {
                DateTime dateTime = DateTime.Today.Add(ScheduleEndTime);
                return dateTime.ToString("hh:mm tt");
            }
        }

        public string DispHoursDifference
        {
            get
            {
                TimeSpan timeDiff = new TimeSpan(0);
                if (DayWork == DriverDayOfWork.S.ToString())
                {
                    timeDiff = ScheduleEndTime - ScheduleStartTime;
                }

                return string.Format("{0:%h} hr {0:%m} min", timeDiff);
                //                return Convert.ToInt32((ScheduleEndTime - ScheduleStartTime).Hours);
            }
        }
        public int HoursDifference
        {
            get
            {
                if (DispDayDate != "Rover" && DispDayDate != "Off" && DayWork == DriverDayOfWork.S.ToString())
                {
                    return Convert.ToInt32((ScheduleEndTime - ScheduleStartTime).TotalMinutes);
                }
                else
                    return 0;
            }
        }

        public DateTime? DriverStartDate { set; get; }
        public DateTime? DriverEndDate { get; set; }

        public bool CanDriverScheduleEditable
        {
            get
            {
                //Rewrite this logic
                //if (DriverStartDate < ScheduleDate && (DriverEndDate.HasValue ? DriverEndDate : DriverStartDate.Value.AddYears(1).Date) > ScheduleDate)
                //{
                //    return false;
                //}
                //else
                //{
                return true;
                //}
            }
        }
    }
}
