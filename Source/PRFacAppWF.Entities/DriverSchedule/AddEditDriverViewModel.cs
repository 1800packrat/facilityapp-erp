﻿namespace PRFacAppWF.Entities
{
    using DriverSchedule;
    using UserManagement;
    
    public class AddEditDriverViewModel
    {
        public AddEditDriverViewModel()
        {
            FAUserMaster = new FADTOUserMaster();
            WeeklySchedule = new WeeklySchedule();
        }

        public FADTOUserMaster FAUserMaster { get; set; }

        //public List<FADTOUserAndFacilityAccess> FAUserAndFacilityAccess { get; set; }
 
        public WeeklySchedule WeeklySchedule { get; set; }
    }
}
