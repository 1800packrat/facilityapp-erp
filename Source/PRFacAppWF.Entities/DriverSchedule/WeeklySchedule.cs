﻿using PRFacAppWF.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities.DriverSchedule
{
    public class WeeklySchedule
    {
        public int UserID { get; set; }

        [Display(Name = "From Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM-dd-yyyy}")]
        public DateTime WeeklyScheduleFromDate { get; set; }

        [Display(Name = "To Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM-dd-yyyy}")]
        public DateTime WeeklyScheduleToDate { get; set; }

        public List<WeeklyDayWiseSchedule> WeeklyDayWiseSchedule { get; set; }
    }

    public class WeeklyDayWiseSchedule
    {
        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        //public string DayWork {get;set;}

        public DriverDayOfWork DayWork { get; set; }

        public bool Sunday { get; set; }

        public bool Monday { get; set; }

        public bool Tuesday { get; set; }

        public bool Wednesday { get; set; }

        public bool Thursday { get; set; }

        public bool Friday { get; set; }

        public bool Saturday { get; set; }
    }
}
