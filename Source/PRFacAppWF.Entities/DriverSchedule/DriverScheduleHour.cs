﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities
{
    public class DriverScheduleHour
    {
        public int RowID { get; set; }

        public int DriverScheduleID { get; set; }

        public int DriverID { get; set; }

        public int StoreNumber { get; set; }

        public DateTime ScheduleDate { get; set; }

        public string DriverScheduleDateIndex { get { return ScheduleDate.ToString("MM/dd/yyyy"); } }

        public string WeekDay { get; set; }

        public System.TimeSpan ScheduleStartTimeTS { get; set; }

        public System.TimeSpan ScheduleEndTimeTS { get; set; }

        public int DriverWorkingHours { get; set; }
        
        public string DayWork { get; set; }

        //public bool IsHoliday { get; set; }
        
        public string ScheduleStartTime
        {
            get
            {
                DateTime dateTime = DateTime.Today.Add(ScheduleStartTimeTS);
                return dateTime.ToString("hh:mm tt");
            }
        }

        public string ScheduleEndTime
        {
            get
            {
                DateTime dateTime = DateTime.Today.Add(ScheduleEndTimeTS);
                return dateTime.ToString("hh:mm tt");
            }
        }

        public string DispDriverWorkingHours
        {
            get
            {
                TimeSpan ts = TimeSpan.FromMinutes(DriverWorkingHours);
                return string.Format("{0} hr {1} min", (int)ts.TotalHours, ts.Minutes);
                //return string.Format("{0:%h} hr {0:%m} min", ts);
            }
        }

    }
}
