﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRFacAppWF.models
{
    public class TouchTime
    {

        public long CAPTouchtimeId { get; set; }
        public Nullable<int> FacilityId { get; set; }
        public string FacilityName { get; set; }
        public Nullable<System.DateTime> Startdate { get; set; }
        public Nullable<System.DateTime> Enddate { get; set; }
        public int DE { get; set; }
        public int DF { get; set; }
        public int RE { get; set; }
        public int RF { get; set; }
        public int CC { get; set; }
        public int WA { get; set; }
        public int IBO { get; set; }
        public int OBO { get; set; }
        public string State { get; set; }
        public int CorporateTouchtime { get; set; }
        public Nullable<System.DateTime> Nextdate { get; set; }        

    }
}