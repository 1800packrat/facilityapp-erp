﻿using System;

namespace PRFacAppWF.models
{
    public class ApplicationSettings
    {
        public int CAPApplicationParameterId { get; set; }
        
        public string ApplicationParameter { get; set; }
        
        public string ApplicationValue { get; set; }

        public string ApplicationValueDisp { get {
            if (ValueUnit.Equals("time", StringComparison.OrdinalIgnoreCase))
                return (new DateTime(TimeSpan.Parse(this.ApplicationValue).Ticks)).ToString("h:mm tt");

            return this.ApplicationValue;
        } }
        
        public string ValueUnit { get; set; }

        public Nullable<System.DateTime> Startdate { get; set; }

        //public string StartDateDisp { get { return StartDate.ToShortDateString(); } }

        //public bool IsFutureDateRecord { get { return StartDate > DateTime.Now; } }

        public Nullable<System.DateTime> Enddate { get; set; }

        public Nullable<System.DateTime> Nextdate { get; set; }
        
        public Nullable<int> Status { get; set; }
    }
}