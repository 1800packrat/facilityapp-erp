﻿using System;
using System.Collections.Generic;

namespace PRFacAppWF.models
{
    public class TrkOpHours
    {
        public int? CAPTruckOperatingHourId { get; set; }
        public int FacilityId { get; set; }
        public int CurrentFacilityId { get; set; }
        public int? TransferFacilityId { get; set; }
        public DateTime? TransferIn { get; set; }
        public DateTime? TransferOut { get; set; }
        public int TruckId { get; set; }
        public int TruckOHReasonId { get; set; }
        public DateTime Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public int MinutesAvailable { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int Status { get; set; }
        public int? TransferReasonId { get; set; }
    }

    public class TruckTempHourChange
    {
        public int RowID { get; set; }
        public int CAPTruckOperatingHourId { get; set; }
        public string FacilityId { get; set; }
        public string StoreOpen { get; set; }
        public string FleetID { get; set; }
        public int TruckId { get; set; }
        public DateTime CapacityDate { get; set; }
        public string CapacityDateDisp { get { return CapacityDate.ToString("MM/dd/yyyy"); } }
        public string CapacityDateIndex { get { return CapacityDate.ToString("MM/dd/yyyy"); } }
        public string WeekDay { get; set; }

        private string _StartTime;

        public string StartTime
        {
            get { return DateTime.Parse(_StartTime).ToString("hh:mm tt"); }
            set { _StartTime = value; }
        }

        private string _EndTime;

        public string EndTime
        {
            get { return DateTime.Parse(_EndTime).ToString("hh:mm tt"); }
            set { _EndTime = value; }
        }

        //public string StartTime { get; set; }
        //public string EndTime { get; set; }
        public string TruckOperatingTimeMinutesDisp
        {
            get
            {
                TimeSpan ts = TimeSpan.FromMinutes(TruckOperatingTimeMinutes);
                return string.Format("{0:%h} hr {0:%m} min", ts);
            }
        }
        public int TruckOperatingTimeMinutes { get; set; }
        public string Status { get; set; }
    }

    public class FacilityOperatingHours
    {
        public int RowID { get; set; }
        public string FacilityId { get; set; }
        public string StoreOpen { get; set; }
        public DateTime CalDate { get; set; }
        public string WeekDay { get; set; }
        private string _StartTime;

        public string StartTime
        {
            get { return DateTime.Parse(_StartTime).ToString("hh:mm tt"); }
            set { _StartTime = value; }
        }

        private string _EndTime;

        public string EndTime
        {
            get { return DateTime.Parse(_EndTime).ToString("hh:mm tt"); }
            set { _EndTime = value; }
        }

        public string FacilityOperatingTimeMinutesDisp
        {
            get
            {
                TimeSpan ts = TimeSpan.FromMinutes(FaciltyOperatingTimeMinutes);
                return string.Format("{0:%h} hr {0:%m} min", ts);
            }
        }
        public int FaciltyOperatingTimeMinutes { get; set; }
        public string Status { get; set; }
        public string UpdatedBy { get; set; }

        public string CalDateDisp
        {
            get
            {
                return CalDate.ToString("MM/dd/yyyy");
            }
        }
    }

    public enum ValidEntryStatus
    {
        Valid = 1,
        TruckIsNotAvailable = 2,
        ConflictWithEntry = 3
    }

    public class TruckOpHoursInfo
    {
        public int? CAPTruckOperatingHourId { get; set; }

        public DateTime CapacityDate { get; set; }

        public int TruckId { get; set; }

        public string FleetID { get; set; }

        public int? FacilityId { get; set; }

        public int TruckOperatingAnyTimeMinutes { get; set; }

        public decimal OpHours { get { return TruckOperatingAnyTimeMinutes / 60; } }

        public string Acronym { get; set; }

        public int? CAPTruckOHReasonId { get; set; }

        public int? IsPermanentTruck { get; set; }

        public bool? IsPermanentTruckBool { get { return IsPermanentTruck.HasValue ? IsPermanentTruck.Value == 1 : (bool?)null; } }

        public string StoreOpen { get; set; }
    }

    public class TruckOHReason
    {
        public int CAPTruckOHReasonId { get; set; }

        public string CategoryReason { get; set; }

        public string ReasonDescription { get; set; }

        public int Status { get; set; }
    }

    public class TruckFacilityCapacity
    {
        public FacilityCapacity Head = new FacilityCapacity();

        public FacilityCapacity FacilityHours = new FacilityCapacity();

        public List<FacilityCapacity> TruckCapacility = new List<FacilityCapacity>();

        public List<DayWiseFacilityCapacity> MarketStatistics = new List<DayWiseFacilityCapacity>();
    }

    public class TruckOHDay
    {
        #region Constructor

        public TruckOHDay()
        {
            DayOpMins = 0;
            DayOpHourId = 0;
            DayOhReason = 0;
        }

        public TruckOHDay(DateTime dayHead)
        {
            _dayHead = dayHead;
        }

        public TruckOHDay(int DayOpMins, int? DayOpHourId = 0, int? DayOhReason = 0, bool? isPermanentTruck = null, int? FacilityId = null, string StoreOpen = "", string Achronym = "")
        {
            this.DayOpMins = DayOpMins;
            this.DayOpHourId = DayOpHourId ?? 0;
            this.DayOhReason = DayOhReason ?? 0;
            this.isPermanentTruck = isPermanentTruck;
            this.FacilityId = FacilityId;
            this.Achronym = Achronym;
            this.StoreOpen = StoreOpen;
        }

        #endregion

        private DateTime _dayHead = DateTime.Now;
        public DateTime TruckOpHead { get { return _dayHead; } set { _dayHead = value; } }
        public string TruckOpHeadDisp { get { return TruckOpHead.ToShortDateString() + "<br />" + TruckOpHead.DayOfWeek.ToString(); } }

        public int DayOpMins { get; set; }
        //public string DayDisplayValue { get { return string.Format("{0:00}", (DayOpMins / 60)) + ":" + string.Format("{0:00}", (DayOpMins % 60)); } }
        public string DayDisplayValue
        {
            get
            {
                TimeSpan ts = TimeSpan.FromMinutes(DayOpMins);
                return string.Format("{0}hr {1}min",
                     (int)ts.TotalHours,
                     ts.Minutes);
                //return string.Format("{0:%h} hr {0:%m} min", ts);
            }
        }
        public int DayOpHourId { get; set; }
        public string DayOfWeek { get; set; }
        public int DayOhReason { get; set; }
        public bool? isPermanentTruck { get; set; }
        public string Achronym { get; set; }
        public string DispTruckOpHours { get { return (string.IsNullOrEmpty(Achronym) ? (StoreOpen == "C" ? "Closed" : DayDisplayValue) : Achronym + " <br/> " + DayDisplayValue); } }
        public string IsPermanentTruck { get { return isPermanentTruck.HasValue && isPermanentTruck.Value ? "true" : "false"; } }
        public TruckAvailabilityStatus TruckAvailabilityStatus { get { return TruckAvailabilityStatus.CanEditAll; } }
        public int? FacilityId { get; set; }
        public string StoreOpen { get; set; }
    }

    public enum TruckAvailabilityStatus
    {
        PermanentTruck = 1,
        TemparoryTruck = 2,
        CanNotEdit = 3,
        CanEditAll = 4,
        CanEditNonOpHoursOnly = 5,
        CanEditTransferOnly = 6
    }

    public class FacilityCapacity
    {
        public string Head { get; set; }

        public int TruckId { get; set; }

        public bool IsPermanentTruckForThisFacility
        {
            get
            {
                return (oDay1.isPermanentTruck ?? true) && (oDay2.isPermanentTruck ?? true) && (oDay3.isPermanentTruck ?? true) && (oDay4.isPermanentTruck ?? true) && (oDay5.isPermanentTruck ?? true) && (oDay6.isPermanentTruck ?? true) && (oDay7.isPermanentTruck ?? true);
            }
        }

        public TruckOHDay oDay1 { get; set; }
        public TruckOHDay oDay2 { get; set; }
        public TruckOHDay oDay3 { get; set; }
        public TruckOHDay oDay4 { get; set; }
        public TruckOHDay oDay5 { get; set; }
        public TruckOHDay oDay6 { get; set; }
        public TruckOHDay oDay7 { get; set; }

        public TruckOHDay Total
        {
            get
            {
                var total = oDay1.DayOpMins + oDay2.DayOpMins + oDay3.DayOpMins + oDay4.DayOpMins + oDay5.DayOpMins + oDay6.DayOpMins + oDay7.DayOpMins;
                return new TruckOHDay(total);
            }
        }
    }

    public class DayWiseFacilityCapacity
    {
        public string Head { get; set; }

        private decimal _day1;

        public decimal Day1
        {
            get { return Math.Round(_day1); }
            set { _day1 = value; }
        }

        private decimal _day2;

        public decimal Day2
        {
            get { return Math.Round(_day2); }
            set { _day2 = value; }
        }

        private decimal _day3;

        public decimal Day3
        {
            get { return Math.Round(_day3); }
            set { _day3 = value; }
        }

        private decimal _day4;

        public decimal Day4
        {
            get { return Math.Round(_day4); }
            set { _day4 = value; }
        }

        private decimal _day5;

        public decimal Day5
        {
            get { return Math.Round(_day5); }
            set { _day5 = value; }
        }

        private decimal _day6;

        public decimal Day6
        {
            get { return Math.Round(_day6); }
            set { _day6 = value; }
        }

        private decimal _day7;

        public decimal Day7
        {
            get { return Math.Round(_day7); }
            set { _day7 = value; }
        }
    }

    public class FacilityCapacityInfo
    {
        public decimal Maximum { get; set; }

        public decimal Downtime { get; set; }

        public decimal Available { get; set; }

        public decimal Booked { get; set; }

        public decimal Open { get; set; }
    }

}