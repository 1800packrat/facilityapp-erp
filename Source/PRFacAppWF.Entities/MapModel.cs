﻿using PR.Entities;
using System.Collections.Generic;

namespace PRFacAppWF.models
{
    public class MapModel
    {
        public string CC { get; set; }
        public string DE { get; set; }
        public string DF { get; set; }
        public string RE { get; set; }
        public string RF { get; set; }
        public string StartTime { get; set; }
        public List<DriverTouch> lDriverTouch { get; set; } 
    }
}