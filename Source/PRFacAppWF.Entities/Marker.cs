﻿namespace PRFacAppWF.models
{
    public class Marker
    {
        public string Latitude {get;set;}
        public string Longitude { get; set; }

        public string LatitudeCCOrg { get; set; }

        public string LongitudeCCOrg { get; set; }

        public string FromAddress { get; set; }

        public string ToAddress { get; set; }
       
        public string TouchType { get; set; }

        public string ImageSelected { get; set; }
        
        public string ImageDefault { get; set; }

        public string DoorPOS { get; set; }

        public bool WeightStnReq { get; set; }

        public string WeightStnReqDisp { get; set; }

        private int mLoadId = -1;
        private int mTouchId = -1;
        private int mDriverId = -1;
        private int mTruckId = -1;
        private int mTrailerId = -1;
        private string mOrderNumber = string.Empty;
        private string _driverName = "no driver assigned";

        public int loadId { get { return mLoadId; } set { mLoadId = value; } }

        public int touchId { get { return mTouchId; } set { mTouchId = value; } }

        public int driverId { get { return mDriverId; } set { mDriverId = value; } }

        public string driverName { get { return _driverName; } set {_driverName=value ;} }

        public int truckId { get { return mTruckId; } set { mTruckId = value; } }

        public int trailerId { get { return mTrailerId; } set { mTrailerId = value; } }

        public string orderNumber { get { return mOrderNumber; } set { mOrderNumber = value; } }

        public string StartTime { get; set; }
        
        public string StartTimeHHMM { get; set; }

        public string CustomerName { get; set; }

        //public string ToolTip { get { return "<div id='mapInfoWindow'><label id='CustNm' style='font-weight:bold;'>" + CustomerName + "</label><hr /><span><b> Touch Type :</b> <label id='tchType'>" + TouchType + "</label></span><br /><span><b> Touch Type :</b> <label id='ordrNum'>" + orderNumber + "</label></span><br /><span><b> From Address :</b> <label id='frmAdd'>" + FromAddress + "</label></span><br /><span><b> To Address :</b> <label id='toAdd'>" + ToAddress + "</label></span><br /><span><b> Instructions :</b> <label id='tchIns'>" + TouchInstructions + "</label></span><br /><span><b>Door Position:</b> <label id='doorPOS'>" + DoorPOS + "</label></span><br /><b>Unit Number:</b><label id='unitCt'>"+UnitNo+"</label></div>"; } }

        public string TouchInstructions { get; set; }

        public string UnitNo { get; set; }
        
        public string UnitSize { get; set; }

        public string TouchNumberDisp { get; set; }

        public decimal SiteLinkMileage { get; set; }

        public bool IsLoadLocked { get; set; }

        public string FacCode { get; set; }

        public string SiteName { get; set; }

        public string GlobalSiteNum { get; set; }

        public string ProvidedETA { get; set; }

        public PR.Entities.ETASettings ProvidedETASettings { get; set; }
    }
}