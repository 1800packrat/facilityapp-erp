﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities
{
    public class FAGlobalSettings
    {
        public int PK_ID { get; set; }

        public string SettingsKey { get; set; }

        public string SettingsValue { get; set; }

        public string Status { get; set; }

        public Nullable<int> StoreNumber { get; set; }

        public bool IsActive { get; set; }
    }
}
