﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFacAppWF.Entities
{
    public class WeeklyDieselPrice
    {
        public int ID { get; set; }

        [DisplayName("Diesel Price")]
        [Required(ErrorMessage = "Please enter price")]
        public decimal DieselPrice { get; set; }

        [DisplayName("Begin Date")]
        [Required(ErrorMessage = "Please choose a begin date")]
        public DateTime BeginDate { get; set; }

        public string BeginDateDisp { get { return BeginDate.ToShortDateString(); } }

        [DisplayName("End Date")]
        [Required(ErrorMessage = "Please choose a end date")]
        public DateTime EndDate { get; set; }
        public string EndDateDisp { get { return EndDate.ToShortDateString(); } }

        [DisplayName("Fuel Adjustment")]
        [Required(ErrorMessage = "Please enter price")]
        public decimal FuelAdjustmentRate { get; set; }

        [DisplayName("LDM Multiplier")]
        [Required(ErrorMessage = "Please enter value")]
        public double? LDMMultiplier { get; set; }

        [DisplayName("Local Multiplier")]
        [Required(ErrorMessage = "Please enter value")]
        public double? LocalMultiplier { get; set; }

        [DisplayName("Actual TransCost")]
        [Required(ErrorMessage = "Please enter value")]
        public Nullable<decimal> ActualTransCost { get; set; }

        [DisplayName("Base TransCost")]
        [Required(ErrorMessage = "Please enter value")]
        public Nullable<decimal> BaseTransCost { get; set; }

        [DisplayName("Local Trans Subsidy")]
        [Required(ErrorMessage = "Please enter value")]
        public Nullable<decimal> LocalTransSubsidy { get; set; }

        [DisplayName("Repositioning Fee")]
        [Required(ErrorMessage = "Please enter value")]
        public Nullable<decimal> RepositioningFee { get; set; }
    }
}
