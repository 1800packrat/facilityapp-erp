﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRFacAppWF.models
{
    public class DriverMaintenance
    {
        public int DriverID { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string FullName { set; get; }
        public string Password { set; get; }
        public bool Status { set; get; }
        public string UserName { set; get; }
        public string Facility { set; get; }
        public DateTime startTime { set; get; }
        public DateTime endTime { set; get; }
        public int StoreNumber { set; get; }

    }
}