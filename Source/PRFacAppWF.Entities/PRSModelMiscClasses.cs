﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PRFacAppWF.models
{
    public class FacilityUnit
    {
        public int UMUnitId { get; set; }
        public string UnitName { get; set; }
        public string UnitSize { get; set; }
        public string FacilityName { get; set; }
        public string Location { get; set; }
    }

    public class UnitMaintenanceDetails
    {
        public UnitMaintenanceDetails()
        {
            Date = DateTime.Now;
            //IsRentable = true;
            //IsTotalLoss = false;
        }

        public int UnitMaintenanceId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public int UMUnitId { get; set; }
        public string UnitNumber { get; set; }
        public string Location { get; set; }
        public string Size { get; set; }
        public bool IsTotalLoss { get; set; }
        public bool IsRentable { get; set; }
        public string Comments { get; set; }
        public string Createdby { get; set; }
        public string UpdatedBy { get; set; }
        public IList<UMPartDetails> UMPartDetails { get; set; }
        public string UnitPartsDetails
        {
            get
            {
                string combinedPartDetails = string.Empty;
                if (IsTotalLoss == false && UMPartDetails != null)
                {
                    foreach (var partDetail in UMPartDetails)
                    {
                        if (partDetail.IsDeleted == false)
                        {
                            combinedPartDetails += (combinedPartDetails.Length > 0 ? "|" : "") + partDetail.UnitPartsId + "," + partDetail.NumberOfParts;
                        }
                    }
                }
                return combinedPartDetails;
            }
        }
    }

    public class UMSearchResults
    {
        public int page;
        public int total;
        public int records;
        public string error;
        public List<UnitMaintenanceInfo> rows;
        public bool isfullaccess;
    }

    public class UMSearchFilter
    {
        public int page;
        public int rows;
        public string sidx;
        public string sord;
        public UnitMaintenanceFilterCriteria filter;
        public bool isfullaccess;
    }

    public class UnitMaintenanceFilterCriteria
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        private string _unitNumber;
        public string UnitNumber { get { if (string.IsNullOrEmpty(_unitNumber)) return null; else return _unitNumber; } set { _unitNumber = value; } }
        private string _unitSize;
        public string UnitSize { get { if (string.IsNullOrEmpty(_unitSize)) return null; else return _unitSize; } set { _unitSize = value; } }

        public int UMUnitId { get; set; }
        public int? Location { get; set; }
        public bool? TotalLoss { get; set; }
        public bool? Rentable { get; set; }
        public string EnterdBy { get; set; }
        public string UpdatedBy { get; set; }
        public int? PartType { get; set; }
        public string EnteredFacility { get; set; }

        public string Facility { get; set; }
        public bool OnlyMaintenanceRec { get; set; }
        public bool IsCorporateReport { get; set; }

        public int PageNo { get; set; }
        public int RecordCntperPage { get; set; }
    }

    public class UnitMaintenanceInfo
    {
        public int UnitMaintenanceDetailId { get; set; }
        public int UnitMaintenanceId { get; set; }
        public string UnitMaintenaceIDAndDetailsID { get { return Convert.ToString(UnitMaintenanceId) + "," + Convert.ToString(UnitMaintenanceDetailId); } }
        public Nullable<DateTime> EntryDate { get; set; }
        public string EntryDateDisp { get { return EntryDate != null ? Convert.ToDateTime(EntryDate).ToString("d") : null; } }
        public string UnitName { get; set; }
        public int UMUnitId { get; set; }
        public string Location { get; set; }
        public string UnitSize { get; set; }
        public Nullable<bool> TotalLoss { get; set; }
        public Nullable<bool> Rentable { get; set; }
        public string Comments { get; set; }
        public string EnterdBy { get; set; }
        public string UpdatedBy { get; set; }
        public string PartType { get; set; }
        public int NumberOfParts { get; set; }
        public string EnteredFacility { get; set; }
        public string FacilityName { get; set; }
        public int TotalRows { get; set; }
    }

    public class UnitMaintenanceInfoReport
    {
        public int UnitMaintenanceDetailId { get; set; }
        public Nullable<DateTime> EntryDate { get; set; }
        public string EntryDateDisp { get { return EntryDate != null ? Convert.ToDateTime(EntryDate).ToString("d") : null; } }
        public string UnitName { get; set; }
        public int UMUnitId { get; set; }
        public string Location { get; set; }
        public string UnitSize { get; set; }
        public Nullable<bool> TotalLoss { get; set; }
        public Nullable<bool> Rentable { get; set; }
        public string Comments { get; set; }
        public string EnteredBy { get; set; }
        public string FacilityName { get; set; }

        public int Roof { get; set; }
        public int LeftDoor { get; set; }
        public int RightDoor { get; set; }
        public int DoorFrame { get; set; }
        public int RollUpDoor { get; set; }
        public int SidingwithSign { get; set; }
        public int SidingwithoutSign { get; set; }
        public int BackWallSiding { get; set; }
        public int FloorPanels { get; set; }
        public int Corner { get; set; }
        public int Latch { get; set; }
    }

    public class UMPartDetails
    {
        public string UnitPartsId { get; set; }
        public string NumberOfParts { get; set; }
        public bool IsDeleted { get; set; }
    }

    #region Truck Status
    public class TruckStatusDetails
    {
        public TruckStatusDetails()
        {
            Date = DateTime.Now;
        }

        public int TruckDetailsId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public int TruckNumber { get; set; }
        public int TruckStatus { get; set; }
        public Nullable<DateTime> NextPreventiveMaintDate { get; set; }
        public string Comments { get; set; }

        public string Createdby { get; set; }
        public string UpdatedBy { get; set; }

        public bool IsFleetTransfer { get; set; }
        public bool IsTransferTemparory { get; set; }
        public int TransferStoreNo { get; set; }
        public Nullable<DateTime> TransferValidFrom { get; set; }
        public Nullable<DateTime> TransferValidTo { get; set; }
    }

    public class SearchResults<T>
    {
        public int page;
        public int total;
        public int records;
        public string error;
        public List<T> rows;
        public List<string> colNames;
        public List<string> colModels;
        public List<string> colData;
    }

    public class SearchFilter<T>
    {
        public int page;
        public int rows;
        public string sidx;
        public string sord;
        public T filter;
    }

    public class TruckStatusFilterCriteria
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Facility { get; set; }
        public int? TruckStatus { get; set; }
        public bool TruckWithNoStatus { get; set; }

        public string EnterdBy { get; set; }
        public string UpdatedBy { get; set; }

        public int PageNo { get; set; }
        public int RecordCntperPage { get; set; }
        public string sidx { get; set; }
        public string sord { get; set; }
    }

    public class TruckStatusInfo
    {
        //public int TruckDetailsId { get; set; }
        public string Facility { get; set; }
        public string EnterdBy { get; set; }
        public DateTime? EntryDate { get; set; }
        public string EntryDateDisp { get { return EntryDate != null ? Convert.ToDateTime(EntryDate).ToString("d") : null; } }
        public string TruckNumber { get; set; }
        public string FleetID { get; set; }
        public string TruckStatus { get; set; }
        public string Comments { get; set; }
        public int TotalRows { get; set; }

        //public int TotalRows { get; set; }
    }
    #endregion

    #region Container Status
    public class ContainerStatusFilter
    {
        public string StatusName { get; set; }
        public int? FacilityId { get; set; }


        public int PageNo { get; set; }
        public int RecordCntperPage { get; set; }
        public string sidx { get; set; }
        public string sord { get; set; }
    }
    #endregion

    #region Staging touches

    public class StagingTouchFilterCriteria
    {
        public DateTime? StagingTouchStartDate { get; set; }
        public DateTime? StagingTouchEndDate { get; set; }

        public DateTime? date { get; set; }

        public string[] unitType { get; set; }

        public int PageNo { get; set; }
        public int RecordCntperPage { get; set; }
        public string sidx { get; set; }
        public string sord { get; set; }
    }



    #endregion
}