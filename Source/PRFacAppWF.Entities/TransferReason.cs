﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRFacAppWF.models
{
    public class TransferReason
    {
        public int CAPTruckTransferReasonId { get; set; }
        public string TransferReasonName { get; set; }
        public int Status { get; set; }
    }
}