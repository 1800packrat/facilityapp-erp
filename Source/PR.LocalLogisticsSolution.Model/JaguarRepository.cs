﻿namespace PR.LocalLogisticsSolution.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using PR.LocalLogisticsSolution.Interfaces;
    using PR.Entities;
    using PR.BusinessLogic;
    using System.Data;
    using PR.LocalLogisticsSolution.Infrastructure.Model;
    using System.Data.Entity;
    using static PR.UtilityLibrary.PREnums;
    using PR.UtilityLibrary;

    public class JaguarRepository : IDBRepository
    {
        public JaguarRepository()
        {

        }

        IRouteRepository _routeRepository;
        public JaguarRepository(IRouteRepository routeRepository)
        {
            _routeRepository = routeRepository;
        }

        public List<PR.Entities.PRDriver> GetAllDriversByFacility(string StoreNo, DateTime date, int? marketId = null)
        {
            List<PRDriver> prDrivers = new List<PRDriver>();
            using (var context1 = new PRSLLocalTestEntities())
            {
                context1.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                int storeno = 0;
                int.TryParse(StoreNo, out storeno);
                DateTime nextday = date.AddDays(1);

                prDrivers = context1.Database.SqlQuery<PRDriver>(@"exec USP_FA_GetAvailableDriversForLD @Date, @StoreNumber",
                                                                PRHelper.SqlParameterObj("Date", date),
                                                                PRHelper.SqlParameterObj("StoreNumber", StoreNo)).AsQueryable().ToList();
            }
            return prDrivers;
        }

        public List<DriverScheduleHourChangeAndStatus> CheckUpdateDriverScheduleHours(int driverId, List<DateTime> ListScheduleDates)
        {
            List<DriverScheduleHourChangeAndStatus> lstDriverScheduleHourChangeAndStatus = new List<DriverScheduleHourChangeAndStatus>();

            using (var context = new RouteOptimzationEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                DriverRouteStausForADay oUpdateDriverScheduleHours = DriverRouteStausForADay.NoRouteExists;

                foreach (var scheduleDate in ListScheduleDates)
                {
                    oUpdateDriverScheduleHours = DriverRouteStausForADay.NoRouteExists;

                    var loads = context.LoadInformation.Where(l => l.DriverID == driverId && DbFunctions.TruncateTime(l.CreatedDate) == DbFunctions.TruncateTime(scheduleDate)).FirstOrDefault();

                    if (loads == null)
                        oUpdateDriverScheduleHours = DriverRouteStausForADay.NoRouteExists;
                    else if (loads != null && (loads.IsLocked == null || loads.IsLocked == false))
                        oUpdateDriverScheduleHours = DriverRouteStausForADay.RouteExistsAndNonLocked;
                    else if (loads != null && loads.IsLocked == true)
                        oUpdateDriverScheduleHours = DriverRouteStausForADay.RouteExistsAndLocked;

                    lstDriverScheduleHourChangeAndStatus.Add(new DriverScheduleHourChangeAndStatus
                    {
                        ScheduleDate = scheduleDate,
                        UpdateDriverScheduleHours = oUpdateDriverScheduleHours
                    });
                }

                return lstDriverScheduleHourChangeAndStatus;
            }
        }

        public bool CheckLoadExistForGivenDateRange(int driverId, DateTime StartDate, DateTime EndDate)
        {
            using (var context = new RouteOptimzationEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return context.LoadInformation.Where(l => l.DriverID == driverId && DbFunctions.TruncateTime(StartDate) <= DbFunctions.TruncateTime(l.CreatedDate) && DbFunctions.TruncateTime(l.CreatedDate) <= DbFunctions.TruncateTime(EndDate)).Any();
            }
        }

        public List<DateTime> GetDriverLoadDates(int driverId, DateTime StartDate)
        {
            List<DateTime> scheduledDates = new List<DateTime>();
            using (var context = new RouteOptimzationEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var existingLoads = context.LoadInformation.Where(l => l.DriverID == driverId && DbFunctions.TruncateTime(StartDate) <= DbFunctions.TruncateTime(l.CreatedDate));

                if (existingLoads != null)
                {
                    foreach (var load in existingLoads)
                    {
                        scheduledDates.Add((DateTime)load.CreatedDate);
                    }
                }

                return scheduledDates;
            }
        }

        public DriverSchedule GetDriverSchedule(DateTime date, int driverId)
        {
            DriverSchedule driverSchedule = null;
            using (var context = new PRSLLocalTestEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                DateTime nextday = date.AddDays(1);

                driverSchedule = (from ds in context.FADriverSchedules
                                  join driver in context.FAUserMasters on ds.FK_UserID equals driver.PK_UserId
                                  where ds.Status == true
                                  && driverId == driver.UniqueID
                                  && (ds.ScheduleDate >= date.Date && ds.ScheduleDate < nextday.Date)
                                  select new DriverSchedule
                                  {
                                      ScheduleStartTime = ds.ScheduleStartTime,
                                      ScheduleEndTime = ds.ScheduleEndTime,
                                      ScheduleDate = date
                                  }).FirstOrDefault();

            }

            return driverSchedule;
        }
 
        public FADALoginStatus Login(string username, string encryptPassword, string IPAddress, Application Application, ref LogisticsUser user)
        {
            FADALoginStatus status = new FADALoginStatus();

            using (var context = new PRSLLocalTestEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                //Check with user name if any user find then look for the password.
                FAUserMaster dbUser = context.FAUserMasters.Where(u => u.Username.Equals(username, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                if (dbUser != null)
                {
                    if (dbUser.Password.Equals(encryptPassword, StringComparison.Ordinal))
                    {
                        if (dbUser.IsActive == true)
                        {
                            if (dbUser.FAGroupMaster.IsActive == true)
                            {
                                bool groupHasApplicationAccess = dbUser.FAGroupMaster.FAGroupAndModules.Any(m =>
                                                                                                                m.IsActive == true &&
                                                                                                                m.FAModule.FA_ModuleArea.FAApplication.PK_ID == (int)Application &&
                                                                                                                (m.FK_AccessLevelID == (int)AccessLevel.ReadAndWrite || m.FK_AccessLevelID == (int)AccessLevel.ReadOnly));
                                if (groupHasApplicationAccess == true)
                                {
                                    user.ID = dbUser.PK_UserId;
                                    user.UserName = dbUser.Username;
                                    user.FirstName = dbUser.FirstName;
                                    user.LastName = dbUser.LastName;
                                    user.UniqueID = dbUser.UniqueID;
                                    user.UserIsActive = dbUser.IsActive;

                                    user.GroupId = dbUser.FAGroupMaster.PK_GroupId;
                                    user.GroupName = dbUser.FAGroupMaster.GroupName;
                                    user.IsChangePasswordRequired = dbUser.IsChangePasswordRequired;
                                    user.PasswordUpdatedDate = Convert.ToDateTime(dbUser.PasswordUpdatedDate);

                                    user.Facilities = new List<SiteInfo>();
                                    foreach (var userFacilities in dbUser.FAUserAndFacilityAccesses.Where(f => (f.FK_AccessLevelID == (int)AccessLevel.ReadOnly || f.FK_AccessLevelID == (int)AccessLevel.ReadAndWrite) && f.IsActive == true))
                                    {
                                        SiteInfo site = GetSiteInfo(userFacilities.FK_StoreRowID, context);

                                        if (site != null && site.SiteId > 0)
                                        {
                                            site.AccessLevel = userFacilities.FK_AccessLevelID;
                                            user.Facilities.Add(site);
                                        }
                                    }

                                    //update last login date and time 
                                    dbUser.LastLoginDateTime = DateTime.Now;

                                    status = FADALoginStatus.Success;
                                }
                                else
                                {
                                    status = FADALoginStatus.NoApplicationAccessToYourRole;
                                }
                            }
                            else
                            {
                                status = FADALoginStatus.InactiveRole;
                            }
                        }
                        else
                        {
                            status = FADALoginStatus.InactiveUser;
                        }
                    }
                    else
                    {
                        status = FADALoginStatus.InvalidCredentials;
                    }

                    //Log events for success or failure cases
                    FALoginHistory lh = new FALoginHistory
                    {
                        FK_UserID = dbUser.PK_UserId,
                        ApplicationName = Application.ToString(),
                        CreatedDateTime = DateTime.Now,
                        IPAddress = IPAddress,
                        Comments = status.ToString(),
                        IsSuccessFullLogin = (status == FADALoginStatus.Success)
                    };

                    context.FALoginHistories.Add(lh);
                    context.SaveChanges();
                }
                else
                {
                    status = FADALoginStatus.InvalidCredentials;
                }
            }
            return status;
        }

        public RDFacility GetFacilityInfo(int storeRowID, PRSLLocalTestEntities context)
        {
            return (from fc in context.RDFacilities
                    where fc.StatusRowID >= 65 && fc.StoreType >= 10
                    && fc.StoreRowID == storeRowID
                    select fc).Distinct().OrderBy(f => f.CompDBAName).FirstOrDefault();
        }

        public SiteInfo GetSiteInfo(int storeRowID, PRSLLocalTestEntities context)
        {
            var dsSite = GetFacilityInfo(storeRowID, context);
            SiteInfo siteInfo = new SiteInfo();

            if (dsSite != null)
            {
                siteInfo.SiteId = dsSite.SLSiteId.Value;
                siteInfo.LocationCode = dsSite.SLLocCode;
                siteInfo.SiteName = dsSite.CompDBAName;
                siteInfo.GlobalSiteNum = Convert.ToString(dsSite.StoreNo); 
                siteInfo.SiteAddress.AddressLine1 = dsSite.Street1;
                siteInfo.SiteAddress.AddressLine2 = dsSite.Street2;
                siteInfo.SiteAddress.City = dsSite.City;
                siteInfo.SiteAddress.State = dsSite.State;
                siteInfo.SiteAddress.Zip = dsSite.ZipCode;
                siteInfo.SiteAddress.Latitude = Convert.ToString(dsSite.Latitude);
                siteInfo.SiteAddress.Longitude = Convert.ToString(dsSite.Longitude);
                siteInfo.SiteAddress.AddressType = PREnums.AddressType.Warehouse;
                siteInfo.LegalName = dsSite.CompDBAName;
                siteInfo.EMail = dsSite.Email;
                siteInfo.MarketId = dsSite.FK_MarketId;
            }

            return siteInfo;
        }

        public List<SiteInfo> GetAllFacilities()
        {
            LocalComponent local = new LocalComponent();
            List<SiteInfo> lstFacilities = new List<SiteInfo>();
            DataSet dsFacility = local.GetAllFacilities();

            if (dsFacility.Tables.Count > 0 && dsFacility.Tables[0].Rows.Count > 0)
            {
                lstFacilities = dsFacility.Tables[0].AsEnumerable().Select(row => new SiteInfo
                {
                    GlobalSiteNum = row["StoreNo"].ToString(),
                    SiteName = row["CompDBAName"].ToString()
                }).ToList();
            }

            return lstFacilities;
        }

        public List<Trucks> GetTruckStatus(string storeNumber)
        {
            int storeNum = Convert.ToInt32(storeNumber);
            using (var context1 = new PRSLLocalTestEntities())
            {
                context1.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var result = (from td in context1.TruckMasters
                              where td.StoreNo == storeNum && td.TruckStatusId == 1
                              select new PR.LocalLogisticsSolution.Model.Trucks { TruckNumber = td.FleetID, TruckStatus = "Active", TruckId = td.TruckId }).ToList();
                return result;
            }

        }

        public List<Trucks> GetAllTrucksByStoreNumber(string storeNumber)
        {
            int storeNum = Convert.ToInt32(storeNumber);
            using (var context1 = new PRSLLocalTestEntities())
            {
                context1.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var result = (from td in context1.TruckMasters
                              join status in context1.TruckStatus on td.TruckStatusId equals status.TruckStatusId
                              where td.StoreNo == storeNum //&& td.TruckStatusId == 1
                              select new PR.LocalLogisticsSolution.Model.Trucks { TruckNumber = td.FleetID, TruckStatus = status.TruckStatus, TruckId = td.TruckId }).ToList();
                return result;
            }
        }

        public long InsertActivityLog(int qorid, string activityType, string activityText, string activityBy, int count, int starsId, string applicationName)
        {
            long activityId = -1;
            using (var context = new PRSLLocalTestEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var activityptype = context.PRActivityTypes.Where(a => a.ActivityType == activityType).FirstOrDefault();

                if (activityptype != null)
                {
                    activityId = InsertActivityLog(qorid, activityptype.ActivityTypeId, activityText, activityBy, count, starsId, applicationName);
                }
            }

            return activityId;
        }

        public long InsertActivityLog(int qorid, int activityTypeId, string activityText, string activityBy, int count, int starsId, string applicationName)
        {
            using (var context = new PRSLLocalTestEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                PRActivityLog activityLog = new PRActivityLog();
                activityLog.QORID = qorid;
                activityLog.ActivityTypeId = activityTypeId;
                activityLog.ActivityText = activityText;
                activityLog.ActivityBy = activityBy;
                activityLog.Count = count;
                activityLog.ActivityDateTime = DateTime.Now;
                activityLog.STARSID = starsId;
                activityLog.ApplicationName = applicationName;

                context.PRActivityLogs.Add(activityLog);
                context.SaveChanges();

                return activityLog.ActivityLogId;
            }
        }

        public List<LogisticsUser> GetAllDrivers(string Name, int StoreNo, bool IsActive)
        {
            using (var context = new RouteOptimzationEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                List<LogisticsUser> listDrivers = new List<LogisticsUser>();
                LogisticsUser lstdriver = null;
                var drivers = context.LogisticsUser.Where(u =>
                    (string.IsNullOrEmpty(Name)
                        || (!string.IsNullOrEmpty(Name)
                            && (u.FirstName.Equals(Name, StringComparison.OrdinalIgnoreCase)
                                || u.LastName.Equals(Name, StringComparison.OrdinalIgnoreCase)
                                || (u.FirstName + " " + u.LastName).Equals(Name, StringComparison.OrdinalIgnoreCase))))
                    && (StoreNo <= 0 || (StoreNo > 0 && u.StoreNumber == StoreNo))
                    && IsActive == u.UserIsActive).ToList();

                foreach (var driver in drivers)
                {
                    lstdriver = new LogisticsUser()
                    {
                        DriverID = Convert.ToInt32(driver.DriverId),
                        FirstName = driver.FirstName,
                        LastName = driver.LastName,
                        UserName = driver.UserName,
                        StoreNo = Convert.ToInt32(driver.StoreNumber),
                        UserIsActive = driver.UserIsActive,
                        UserType = driver.UserTypeID,
                        Password = driver.Password,
                        ID = driver.PK_USerID
                    };

                    listDrivers.Add(lstdriver);
                }

                return listDrivers;
            }
        }

        public LogisticsUser GetDriverById(int ID)
        {
            using (var context = new RouteOptimzationEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var driver = context.LogisticsUser.Find(ID);

                return new LogisticsUser()
                {
                    DriverID = Convert.ToInt32(driver.DriverId),
                    FirstName = driver.FirstName,
                    LastName = driver.LastName,
                    UserName = driver.UserName,
                    StoreNo = Convert.ToInt32(driver.StoreNumber),
                    UserIsActive = driver.UserIsActive,
                    UserType = driver.UserTypeID,
                    Password = driver.Password,
                    ID = driver.PK_USerID
                };
            }
        }

        public int SaveDriverDetails(LogisticsUser driver)
        {
            int retDriverId = 0;

            using (var context = new RouteOptimzationEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                Infrastructure.Model.LogisticsUser objDriver = new Infrastructure.Model.LogisticsUser();

                if (driver.ID != 0)
                    objDriver = context.LogisticsUser.Where(x => x.PK_USerID == driver.ID).FirstOrDefault();

                if (objDriver.PK_USerID != 0 && driver.ID == objDriver.PK_USerID)
                {
                    objDriver.FirstName = driver.FirstName;
                    objDriver.LastName = driver.LastName;
                    objDriver.UserName = driver.UserName;
                    objDriver.StoreNumber = driver.StoreNo;
                    objDriver.Password = driver.Password;
                    objDriver.UserUpdatedBy = Convert.ToInt32(driver.updatedBy);
                    objDriver.UserUpdatedAt = DateTime.Now;

                    context.SaveChanges();
                }
                else
                {
                    objDriver = new Infrastructure.Model.LogisticsUser();
                    objDriver.FirstName = driver.FirstName;
                    objDriver.LastName = driver.LastName;
                    objDriver.UserName = driver.UserName;
                    objDriver.StoreNumber = driver.StoreNo;

                    objDriver.UserCreatedBy = Convert.ToInt32(driver.createdBy);
                    objDriver.UserCreatedAt = DateTime.Now;
                    objDriver.UserUpdatedAt = DateTime.Now;
                    objDriver.UserUpdatedBy = Convert.ToInt32(driver.updatedBy);
                    objDriver.UserIsActive = driver.UserIsActive;
                    objDriver.UserTypeID = 1; //default value, will need to change dynamically accrodingly in future

                    objDriver.Password = driver.Password;

                    context.LogisticsUser.Add(objDriver);

                    context.SaveChanges();

                    retDriverId = objDriver.PK_USerID;
                }

            }

            return retDriverId;
        }

        public bool SetDriverStatus(LogisticsUser driver)
        {
            bool isSaved = false;

            using (var context = new RouteOptimzationEntities())
            {
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                Infrastructure.Model.LogisticsUser objDriver = new Infrastructure.Model.LogisticsUser();

                objDriver = context.LogisticsUser.Where(x => x.PK_USerID == driver.ID).FirstOrDefault();

                if (objDriver != null && driver.ID == objDriver.PK_USerID)
                {
                    objDriver.UserIsActive = driver.UserIsActive;

                    objDriver.UserUpdatedBy = Convert.ToInt32(driver.updatedBy);
                    objDriver.UserUpdatedAt = DateTime.Now;
                }
                context.SaveChanges();

                isSaved = true;
            }

            return isSaved;
        }
    }
}
