//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PR.LocalLogisticsSolution.Infrastructure.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class BusyTimeInformation
    {
        public int FK_StopID { get; set; }
        public Nullable<System.TimeSpan> BreakTime { get; set; }
        public string Comment { get; set; }
        public int PF_BreakID { get; set; }
    
        public virtual StopInformation StopInformation { get; set; }
    }
}
