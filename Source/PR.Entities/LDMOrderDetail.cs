﻿namespace PR.Entities
{
    public class LDMOrderDetail
    {
        public int STARSID { get; set; }
        public int QORID { get; set; }
        public int SiteID { get; set; }
        public string LocationCode { get; set; }
        public int QTRentalID { get; set; }
        public int TenantID { get; set; }
        public int SLUnitID { get; set; }
        public string SLUnitName { get; set; }
        public string RentalStatus { get; set; }
        public string RentalType { get; set; }
        public int StarsUnitID { get; set; }
        public string LDMType { get; set; }

    }
}
