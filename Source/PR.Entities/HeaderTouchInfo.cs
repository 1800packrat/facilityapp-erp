using System;

namespace PR.Entities
{
    public class HeaderTouchInfo
    {
        private string _lastCompletedTouchType = String.Empty;
        private string _nextSceduledTouchType = String.Empty;
        private string _nextUnscheduledTouchType = String.Empty;

        public string LastCompletedTouchType
        {
            get { return _lastCompletedTouchType; }
            set { _lastCompletedTouchType = value; }
        }
        public string NextScheduledTouchType
        {
            get { return _nextSceduledTouchType; }
            set { _nextSceduledTouchType = value; }
        }
        public string NextUnscheduledTouchType
        {
            get { return _nextUnscheduledTouchType; }
            set { _nextUnscheduledTouchType = value; }
        }   
    }
}
