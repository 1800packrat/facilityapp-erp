﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    public class SpiritTouchUpdate
    {
        public int iSequenceNum { get; set; }

        public string TouchUniqueKey { get; set; }

        public string ScheduleDate { get { return UpdatedScheduledDate?.ToShortDateString(); } }

        public string TouchType { get; set; }

        public bool IsScheduleDateUpdated { get; set; }
        public DateTime? UpdatedScheduledDate { get; set; }

        public bool IsInstructionsUpdated { get; set; }
        public string UpdatedInstructions { get; set; }

        public bool IsTouchTimeUpdated { get; set; }
        public string UpdatedTouchTime { get; set; }

        public bool IsAddressUpdated { get; set; }
        public Address UpdatedTouchAddress { get; set; }

        public bool IsTouchMilesUpdated { get; set; }
        public decimal UpdatedTouchMiles { get; set; }


        public decimal TouchPrice { get; set; }

        public decimal MileageRate { get; set; }

        public decimal IncludedMiles { get; set; }

        public bool IsIncludeExcessMilePricing { get; set; }

        public string TouchTypeFullName
        {
            get
            {
                string type = string.Empty;
                switch (TouchType)
                {
                    case "DE":
                        type = "DE-Deliver Empty";
                        break;
                    case "DF":
                        type = "DF-Deliver Full";
                        break;
                    case "CC":
                        type = "CC-Curb To Curb";
                        break;
                    case "RE":
                        type = "RE-Return Empty";
                        break;
                    case "RF":
                        type = "RF-Return Full";
                        break;
                    case "TO":
                        type = "LDM:Out-LDM Transfer Out";
                        break;
                    case "TI":
                        type = "LDM:Out-LDM Transfer Out";
                        break;
                    case "WA":
                        type = "WA-Whse Access";
                        break;
                    case "IBO":
                        type = "IBO-In By Owner";
                        break;
                    case "OBO":
                        type = "OBO-Out By Owner";
                        break;
                }

                return type;
            }

        }


        public string TouchDescriptionForCharges {
            get
            {
                string type = string.Empty;
                switch (TouchType)
                {
                    case "DE":
                        type = "Delivery: DE";
                        break;
                    case "DF":
                        type = "Re-Delivery: DF";
                        break;
                    case "CC":
                        type = "Relocation: CC";
                        break;
                    case "RE":
                        type = "Final Pickup: RE";
                        break;
                    case "RF":
                        type = "Move-In: RF";
                        break;            
                     
                }

                return type;
            }
        }
    }
}
