﻿using System;

namespace PR.Entities
{
    public class PaymentReceipt
    {
        public int ReceiptId
        {
            get;
            set;
        }
        public int SiteId
        {
            get;
            set;
        }
        public int TenantId
        {
            get;
            set;
        }
        public decimal PaymentAmount
        {
            get;
            set;
        }
        public DateTime ReceiptDate
        {
            get;
            set;
        }
        public int ReceiptNumber
        {
            get;
            set;
        }
    }
}
