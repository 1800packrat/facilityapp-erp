﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
   public  class EsbEndPointConfig
    {      
        public string Url { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
    }
}
