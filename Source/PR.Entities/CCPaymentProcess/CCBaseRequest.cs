﻿using static PR.Entities.CCPaymentProcess.CCPaymentEnums;

namespace PR.Entities.CCPaymentProcess
{
    public class CCBaseRequest
    {
        public RequestedApp RequestedApp { get; set; } // Passport/CW/Stars/Cats

        public OrderType OrderType { get; set; }

        public RequestType RequestType { get; set; }

        public string CreatedBy { get; set; } //Login UserId

        public string RequestedServer { get; set; } //Server Name

        public string IPAddress { get; set; } //IP Address of the calling server    
    }
}
