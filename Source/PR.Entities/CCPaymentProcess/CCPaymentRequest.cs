﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.CCPaymentProcess
{
    public class CCPaymentRequest : CCBaseRequest
    {
        public int? PaymentRequestId { get; set; }

        public int OrderID { get; set; }

        public int CustId { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime? EndDateTime { get; set; }

        public string SessionID { get; set; }

        public string MasterTransactionID { get; set; }

        public decimal MaxAmountToAllow { get; set; }

        public int PaymentID { get; set; }//using to filter T121 table data  

        public string CCCardNumber { get; set; }

        public int FK_CardType { get; set; }

        public string ExpDate { get; set; }

        public string CVV { get; set; }

        public string NameOnCard { get; set; }

        public string BillingAddress { get; set; }

        public string BillingCity { get; set; }

        public string BillingState { get; set; }

        public string BillingZip { get; set; }

        public bool IsOnetimePayment { get; set; }

        public bool IsRental { get; set; }
         
        public int? RentalIncomeId { get; set; }

        public int? ContainerId { get; set; }
 
    }
}
