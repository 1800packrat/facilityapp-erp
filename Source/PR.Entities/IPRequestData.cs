﻿
using System;


namespace PR.Entities
{
    public class IPRequestData
    {
        public long pk_request_id { get; set; }

        public string strRequestIP { get; set; }

        public DateTime? dtDate { get; set; }

        public DateTime? dtDateTime { get; set; }

        public string strFromZip { get; set; }

        public string strToZip { get; set; }

        public string strSize { get; set; }

        public DateTime? dtTime { get; set; }

        public DateTime? ProjectedDeliveryDate { get; set; }

        public string WebQID { get; set; }

        public string WCID { get; set; }

        public string LandingPageId { get; set; }

        public string QuoteType { get; set; }

        public string TrackingPromoCode { get; set; }

        public string Email { get; set; }

        public string SLLocCode { get; set; }

        public int? TenantID { get; set; }

        public bool UserBanFlag { get; set; }

        public int? BusDevID { get; set; }

        public string AB_PageCode { get; set; }

        public bool? IsUserInitiatedChat { get; set; }

        public bool? IsProActiveChat { get; set; }

        public bool? IsChatRejected { get; set; }

        public string utm_source { get; set; }

        public string utm_medium { get; set; }

        public string utm_campaign { get; set; }

        public string utm_term { get; set; }

        public string utm_content { get; set; }

        public string CreatedBy { get; set; }

        public bool ToZipIsServiced { get; set; }
        public bool FromZipIsServiced { get; set; }
        public bool IsServiced { get; set; }
    }

}
