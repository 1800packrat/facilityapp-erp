using PR.UtilityLibrary;
using System;

namespace PR.Entities
{
    public class Address
    {
        private string _firstName = String.Empty;
        private string _lastName = String.Empty;
        private string _addressLine1 = String.Empty;
        private string _addressLine2 = String.Empty;
        private string _city = String.Empty;
        private string _state = String.Empty;
        private string _zip = String.Empty;
        private string _company = String.Empty;
        private string _phoneNumber = String.Empty;
        private string _alternatePhoneNumber = String.Empty;
        private string _mobilePhoneNumber = String.Empty;
        private string _latitude = String.Empty;
        private string _longitude = String.Empty;
        private string _email = String.Empty;
        private string _Country = "USA";
        private PREnums.AddressType _addressType;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string AddressLine1
        {
            get { return _addressLine1; }
            set { _addressLine1 = value; }
        }
        public string AddressLine2
        {
            get { return _addressLine2; }
            set { _addressLine2 = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }
        public string Company
        {
            get { return _company; }
            set { _company = value; }
        }
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }
        public string AlternatePhoneNumber
        {
            get { return _alternatePhoneNumber; }
            set { _alternatePhoneNumber = value; }
        }
        public string MobilePhoneNumber
        {
            get { return _mobilePhoneNumber; }
            set { _mobilePhoneNumber = value; }
        }
        public string Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }
        public string Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
        public string EMail
        {
            get { return _email; }
            set { _email = value; }
        }
        public PREnums.AddressType AddressType { get { return _addressType; } set { _addressType = value; } }

        public int ID { get; set; }
        
        public string Country { get { return _Country; } set { _Country = value; } }

        public string DisplayWarehouseName { get { return City + " - Warehouse"; } }

        public string FullAddress
        {
            get { return AddressLine1 + ", " + (AddressLine2 != null ? (AddressLine2 + (AddressLine2.Length > 0 ? ", " : "")) : "") + City + ", " + State + ", " + Zip; }
        }

        public bool IsGeoCodesValid
        {
            get
            {
                decimal lat = 0, lng = 0;
                return decimal.TryParse(this.Latitude, out lat) && lat != 0 &&
                    decimal.TryParse(this.Longitude, out lng) && lng != 0;
            }
        }

        public bool IsAddressValid {
            get
            {
                return !string.IsNullOrEmpty(this.Latitude) && (Convert.ToDouble(this.Latitude) != 0) 
                    && !string.IsNullOrEmpty(this.Longitude) && (Convert.ToDouble(this.Longitude) != 0) && (ExceedMaxDistanceFromFacility == false);
            }
        }

        public bool ExceedMaxDistanceFromFacility { get; set; }

        public string ValidationError { get; set; }

        public int? FacilityStoreNo { get; set; }

        public string SLLocCode { get; set; } //Primarily we are using this filed in direr app for market touches

        public bool? IsIntersectingLatLong { get; set; }

        public string ActualLatitude { get; set; }

        public string ActualLongitude { get; set; }
    }   
}
