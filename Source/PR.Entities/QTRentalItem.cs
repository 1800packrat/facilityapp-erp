namespace PR.Entities
{
    public class QTRentalItem
    {
        private int _qtRentalId = 0;
        private int _iqridGlobalNum = 0;

        public int QTRentalID
        {
            get { return _qtRentalId; }
            set { _qtRentalId = value; }
        }
        public int QRIDGlobalNum
        {
            get { return _iqridGlobalNum; }
            set { _iqridGlobalNum = value; }
        }
    }
}
