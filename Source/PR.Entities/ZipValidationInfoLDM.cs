﻿using System.Collections.Generic;
using PR.UtilityLibrary;

namespace PR.Entities
{
    public class ZipValidationInfoLDM
    {
       public string ErrorCode { get; set; }
       public string ErrorMessage { get; set; }
       public PREnums.MoveType MoveType { get; set; }
       public List<ZipCodeInfo> Zip1Info { get; set; }
       public List<ZipCodeInfo> Zip2Info { get; set; }
    }
}
