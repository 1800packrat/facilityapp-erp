﻿using System;
using PR.UtilityLibrary;

namespace PR.Entities
{
    public class TrailerWebQuote : BaseLDMTrailerWebQuote
    {

       
        public string EDCID { get; set; }

       
        public string ShipperName { get; set; }

       
        public string ShipperLastName { get; set; }

       
        public string Email { get; set; }

       
        public string Telephone { get; set; }

       
        public string MoveType { get; set; }

       
        public string MoveDate { get; set; }

        private string shipmentPickUpDate { get; set; }
       
        public string ShipmentPickUpDate
        {
            get
            {
                return shipmentPickUpDate == "19000101000000" ? string.Empty : shipmentPickUpDate;
            }
            // add this due json Serialization gives error 
            set { shipmentPickUpDate = value; }
        }

        private string shipmentDeliveryDate;
       
        public string ShipmentDeliveryDate
        {
            get
            {
                return shipmentDeliveryDate == "19000101000000" ? string.Empty : shipmentDeliveryDate;
            }
            // add this due json Serialization gives error 
            set { shipmentDeliveryDate = value; }
        }

       
        public string PickupCountry { get; set; }

       
        public string PickupAddress1 { get; set; }

       
        public string PickupAddress2 { get; set; }

       
        public string PickupCity { get; set; }

       
        public string PickupState { get; set; }

       
        public string PickupZip { get; set; }

       
        public string DeliveryAddress1 { get; set; }

       
        public string DeliveryAddress2 { get; set; }

       
        public string DeliveryCountry { get; set; }

       
        public string DeliveryCity { get; set; }


       
        public string DeliveryState { get; set; }


       
        public string DeliveryZip { get; set; }

       
        public string Quote { get; set; }

       
        public string Migrated { get; set; }

       
        public string Notes { get; set; }

       
        public string LinearFeet { get; set; }

       
        public string CPPCharge { get; set; }


       
        public string ODCharge { get; set; }

       
        public string CustPrice { get; set; }

       
        public string AdjustPrice { get; set; }

       
        public string EstTransitDays { get; set; }

       
        public string ODReference { get; set; }

       
        public string CustAdjustPrice { get; set; }

       
        public string QuoteNumber { get; set; }

       
        public string MoveSize { get; set; }

       
        public string StorageRate { get; set; }

       
        public string StorageMonths { get; set; }

       
        public string StorageExpRate { get; set; }

       
        public string ODTotalCharge { get; set; }


       
        public string StorageRevPrice { get; set; }

       
        public string StorageExpPrice { get; set; }

       
        public string CPPExpCharge { get; set; }

       
        public string ShipmentReferenceNumber { get; set; }

       
        public string ODBookingNumber { get; set; }

        private string quoteStatus = string.Empty;
       
        public string QuoteStatus
        {
            get
            {
                if (!string.IsNullOrEmpty(ShipmentStatus) && ShipmentStatus == "Cancelled")
                {
                    return ShipmentStatus;
                }
                if (!string.IsNullOrEmpty(TShipmentEDCID))
                {
                    if (!string.IsNullOrEmpty(ODBookingNumber))
                    {
                        return "Booked";
                    }
                    return "Sold";
                }




                return "Quoted";
            }
            // add this due json Serialization gives error 
            protected set { quoteStatus = value; }
        }

       
        public string ShipmentStatus { get; set; }


       
        public string ShipmentCouponCode { get; set; }


       
        public string QuotedBy { get; set; }

       
        public string SoldBy { get; set; }

       
        public string BookedBy { get; set; }

       
        public string DispositionReason { get; set; }

        private string dispositionReasonDisplay;

       
        public string DispositionReasonDisplay
        {
            get
            {
                if (string.IsNullOrEmpty(DispositionReason) || !DispositionReason.Contains("::"))
                {
                    return string.Empty;
                }

                string[] strSplit = DispositionReason.Split(new string[] { "::" },  2,  StringSplitOptions.None);
                return strSplit[1];

            }


            // add this due json Serialization gives error 
            protected set { dispositionReasonDisplay = value; }
        }

       
        public string CancelledBy { get; set; }

       
        public decimal QuoteTotalBalanceCalculated { get; set; }

       
        public string CustomerAddressLine1 { get; set; }

       
        public string CustomerAddressLine2 { get; set; }

       
        public string CustomerCity { get; set; }

       
        public string CustomerState { get; set; }

       
        public string CustomerZip { get; set; }

        private decimal totalDistance;
       
        public decimal TotalDistance
        {
            get
            {
                return Miles.ToDecimal();
            }


            // add this due json Serialization gives error 
            protected set { totalDistance = value; }
        }

       
        public int USSCustID { get; set; }

        private bool isQuoted = false;
       
        public bool IsSoldQuote
        {
            get
            {
                return String.IsNullOrEmpty(TShipmentEDCID) ? false : true;
            }
            // add this due json Serialization gives error 
            protected set { isQuoted = value; }
        }


       
        public string TShipmentEDCID { get; set; }

       
        public string Hauler { get; set; }

       
        public string Miles { get; set; }

       
        public string ODBookingDate { get; set; }


       
        public string ShipIncludeFiveStraps { get; set; }

       
        public string ShipRequestStorageOfMove { get; set; }
       
        public string ShipIncludeHandTruck { get; set; }
    }
}
