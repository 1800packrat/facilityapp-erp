﻿namespace PR.Entities
{
    public class LDMWebQuote : BaseLDMTrailerWebQuote
    {

        public string QuoteId { get; set; }

        public string Status { get; set; }

        public string ProcessDate { get; set; }

        public string BaseCost { get; set; }

        public string OTCompensation { get; set; }

        public string DTCompensation { get; set; }

        public string OZipUpcharge { get; set; }

        public string DZipUpcharge { get; set; }

        public string OExtraMiles { get; set; }

        public string DExtraMiles { get; set; }

        public string DLXlock { get; set; }

        public string DLXlockTax { get; set; }

        public string ExBlankets { get; set; }

        public string WgtTicket { get; set; }

        public string Transportation { get; set; }

        public string ShortHaulUp { get; set; }

        public string CPP { get; set; }

        public string Repositioning { get; set; }

        public string RepositioningTax { get; set; }

        public string ExpFee { get; set; }

        public string GuartdShip { get; set; }

        public string DelSurcharge { get; set; }

        public string MktCCFee { get; set; }

        public string GrossMarginMarkUP { get; set; }

        public string TotalSavings { get; set; }

        public string TotalMinusKit { get; set; }

        public string NumofUnits { get; set; }

        public string OrgFacility { get; set; }

        public string DestFacility { get; set; }

        public string OrderTakenBy { get; set; }

        public string StatusMessage { get; set; }

        public string TotalPrice { get; set; }

        

        public string UnBundledTotal { get; set; }

        public string BareBTotal { get; set; }

        public string BareBDiscount { get; set; }

        public string SixteenFtOriginRentalPrice { get; set; }

        public string SixteenFtDestRentalPrice { get; set; }

        public string EightFtOriginRentalPrice { get; set; }

        public string EightFtDestRentalPrice { get; set; }

        public string SixteenFtCount { get; set; }

        public string EightFtCount { get; set; }

        public string USS_CustId { get; set; }

    }
}
