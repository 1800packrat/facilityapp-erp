﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    public class BrandUnitInfo : Brand
    {
        public int UnitID { get; set; }
    }
}
