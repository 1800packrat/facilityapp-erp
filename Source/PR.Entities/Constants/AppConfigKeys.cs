﻿using System.Configuration;

namespace PR.Entities
{
    public static class AppConfigKeys
    {
        public const string TrailerServices = "TrailerServices";
        public const string AbfMinimumDistance = "ABF_MinimumDistance";
        public const string AbfServiceName = "ABF";
        public const string OdServiceName = "OD";

        public static string ERPFilesPath()
        {
            return ConfigurationManager.AppSettings["StubJsonFilePath"].ToString();
        }

        public static string EsbLogFilesPath()
        {
            return ConfigurationManager.AppSettings["EsblogFilePath"].ToString();
        }
    }
}
