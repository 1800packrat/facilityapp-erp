﻿namespace PR.Entities
{
    public static class PricingConstants
    {
        public const string Admin_Fee = "Admin Fee";
        public const string Per_Prop_Recovery_Fee = "Per Prop Recovery Fee";
        public const string Regulatory_License_Fee = "Regulatory License Fee";
        public const string Fuel_Subsidy = "Fuel Subsidy";
        public const string Fuel_Adjustment_DE = "Fuel Adjustment - DE";
        public const string Fuel_Adjustment_DF = "Fuel Adjustment - DF";
        public const string Fuel_Adjustment_RF = "Fuel Adjustment - RF";
        public const string Fuel_Adjustment_RE = "Fuel Adjustment - RE";
        public const string Fuel_Adjustment_CC = "Fuel Adjustment - CC";        
    }
}
