﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    public class LDMPricingDetailsRequest
    {
        public int? QuoteId { get; set; }

        public string FromZip { get; set; }

        public string ToZip { get; set; }

        public int FromZone { get; set; }

        public int ToZone { get; set; }

        public int NoOftUnits { get; set; }

        /// <summary>
        /// This variable will be set only for STARS/Passport application 
        /// </summary>
        public int? NoOftUnitsBT { get; set; }

        public int? NoOfUnitCounter { get; set; }

        public decimal OrgOneWayMiles { get; set; }

        public decimal DestOneWayMiles { get; set; }

        public decimal TotalMiles { get; set; }

        public decimal DeliverySurchargeTotal { get; set; }

        public int WeightTicketFlg { get; set; }

        public int? IntraState { get; set; }

        public int? OriginStateId { get; set; }

        public int? DestStateId { get; set; }

        public int OrgFacId { get; set; }

        public int DestFacId { get; set; }

        public int Ft8Containers { get; set; }

        public int Ft16Containers { get; set; }

        public DateTime DestDeliveryDate { get; set; }

        public DateTime DestOriginInitialDate { get; set; }

        public int? Speed { get; set; }

        public int? Type { get; set; }

        public int RoleID { get; set; }

        public int StorageMonthOrg { get; set; }

        public int StorageMonthDest { get; set; }

        public decimal PosUpSell { get; set; }
    }
}
