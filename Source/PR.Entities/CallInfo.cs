﻿using System;

namespace PR.Entities
{
    public class CallInfo
    {
        private int _rowid = 0;
        private int _qrid = 0;
        private string _ucid = string.Empty;
        private string _ani = string.Empty;
        private string _tfn = string.Empty;
        private string _zip1 = string.Empty;
        private string _zip2 = string.Empty;
        private string _promo = string.Empty;
        private string _wcid = string.Empty;
        private DateTime _callDate = DateTime.MinValue;
        private string _agentFirstName = string.Empty;
        private string _agentLastName = string.Empty;
        private string _ext = string.Empty;
        private string _source = string.Empty;
        private string _clid = string.Empty;
        private int _programId = 0;

        public int RowId
        {
            get{ return _rowid;}
            set{ _rowid = value;}
        }
        public int QRID
        {
            get{return _qrid;}
            set{_qrid = value;}
        }
        public string UCID
        {
            get{return _ucid;}
            set{_ucid = value;}
        }
        public string ANI
        {
            get{return _ani;}
            set{_ani = value;}
        }
        public string TFN
        {
            get{return _tfn;}
            set{_tfn = value;}
        }
        public string Zip1
        {
            get{return _zip1;}
            set{_zip1 = value;}
        }
        public string Zip2
        {
            get{return _zip2;}
            set{_zip2 = value;}
        }
        public string Promo
        {
            get { return _promo;}
            set { _promo = value; }
        }
        public string WCID
        {
            get { return _wcid; }
            set { _wcid = value; }
        }
        public DateTime CallDate
        {
            get { return _callDate; }
            set { _callDate = value; }
        }
        public string AgentFirstName
        {
            get { return _agentFirstName; }
            set { _agentFirstName = value; }
        }
        public string AgentLastName
        {
            get { return _agentLastName; }
            set { _agentLastName = value; }
        }
        public string Ext
        {
            get { return _ext; }
            set { _ext = value; }
        }
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }
        public string CLID
        {
            get { return _source; }
            set { _source = value; }
        }
        public int ProgramId
        {
            get { return _programId; }
            set { _programId = value; }
        }
    }
}
