﻿using System.Collections.Generic;

namespace PR.Entities
{
    public class LDMUnitEntity
    {
        /// <summary>
        /// site  link Origin unit ID
        /// </summary>
        public int OriginSLUnitID { get; set; }


        /// <summary>
        /// site link Destination unit ID
        /// </summary>
        public int DestinationSLUnitID { get; set; }


        /// <summary>
        /// site link Billing unit ID
        /// </summary>
        public int BillingSLUnitID { get; set; }

        /// <summary>
        /// promotion code for billing for first unit and cpp discount
        /// </summary>
        public int BillingPromoCode { get; set; }


        /// <summary>
        /// true if quote is bundled 
        /// </summary>
        public bool IsBundledQuote { get; set; }

        /// <summary>
        /// promotion code for  origin
        /// </summary>
        public int PromoCodeOrigin { get; set; }


        /// <summary>
        /// promotion code for  destination
        /// </summary>
        public int PromoCodeDestination { get; set; }

        /// <summary>
        /// Stars Unity Id 
        /// </summary>
        public int StarsUnitID { get; set; }


     
        /// <summary>
        /// Set to true for just LDM charges for dummy unit .
        /// </summary>
        public bool IsLDMBillingUnit { get; set; }

        /// <summary>
        /// Marchandise (POS) items list unit level
        /// </summary>
        public List<LDMPricingEntity> MarchandisItems { get; set; }

        /// <summary>
          /// recurring Items list  (unit level)
        /// </summary>
          public List<LDMPricingEntity> RecurringItems { get; set; }


          /// <summary>
          /// non-recurring Items(transportaion charges) list for quote level for just first unit 
          /// </summary>
          public List<LDMPricingEntity> NonRecurringItems { get; set; }


        /// <summary>
        /// List of touch information
        /// </summary>
          public List<LDMTouchesEntity> LDMToucheItems { get; set; }


        /// <summary>
        /// First unit billing QOR for this order , if its add unit on existing order
          /// T25_tblQuote.T25_BillingQORID
        /// </summary>
        public int? DummyUnitBillingQORID{get; set;}

        /// <summary>
        /// blling qorid of the unit from T26_tblContainer.T26_BillingQORID table 
        /// </summary>
        public int BillingQORIDOfUnit { get; set; }


        /// <summary>
        /// unit price Origin
        /// </summary>
        public decimal UnitPriceOrigin { get; set; }

        /// <summary>
        /// unit price Destination
        /// </summary>
        public decimal UnitPriceDestination { get; set; }

        public bool IsUnitAtOrigin { get; set; }


   

        

    }
}
