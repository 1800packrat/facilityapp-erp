﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    public class GoogleMapAuditLog
    {
        public string Request { get; set; }

        public string Response { get; set; }

        public  DateTime StartTime { get; set; }

        public  DateTime EndTime { get; set; }

        public  string MethodName{ get; set; }

        public  string ApplicationName{ get; set; }

        public  string ServerName { get; set; }
    }
}
