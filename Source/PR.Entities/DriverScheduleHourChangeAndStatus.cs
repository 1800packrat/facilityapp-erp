﻿namespace PR.Entities
{
    using System;
    using static PR.UtilityLibrary.PREnums;

    public class DriverScheduleHourChangeAndStatus
    {
        public DateTime ScheduleDate { get; set; }

        public DriverRouteStausForADay UpdateDriverScheduleHours { get; set; }
    }
}
