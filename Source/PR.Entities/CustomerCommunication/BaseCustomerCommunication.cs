﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace PR.Entities.CustomerCommunication
{
    public class BaseCustomerCommunication
    {
        [Required]
        public int USSCustID { get; set; }
        [Required]
        public string UserName { get; set; }
    }
}
