﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.CustomerCommunication
{
    public class CommunicationPreferences
    {
        public int CommunicationPreferenceID { get; set; }
        public int CommunicationLevelID { get; set; }
        public int CommunicationMethodID { get; set; }
        public string PreferenceValue { get; set; }
        public bool Is5DayPrior { get; set; }
        public bool Is3DayPrior { get; set; }
        public bool IsRoutelocked { get; set; }
        public bool IsDriverenroute { get; set; }
        public bool IsScheduledChange { get; set; }
        public bool IsScheduleInformation { get; set; }
        public bool IsDeleted { get; set; }
    }
}
