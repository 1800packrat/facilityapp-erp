#region Using directives
using System;
#endregion

namespace PR.Entities
{
    public class User : IComparable
    {
        #region Private variables
        private int _id;
        private string _userName;
        private string _pwd;
        private string _firstName;
        private string _lastName;
        private string _address1;
        private string _address2;
        private string _city;
        private string _state;
        private string _postalCode;
        private bool _isActive;
        private int _roleID;
        private string _role;

        private static SortOrder _order;
        #endregion

        #region Enums
        public enum SortOrder
        {
            Ascending = 0,
            Descending = 1
        }
        #endregion

        #region Properties
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Password
        {
            get { return _pwd; }
            set { _pwd = value; }
        }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public int RoleID
        {
            get { return _roleID; }
            set { _roleID = value; }
        }

        public string Role
        {
            get { return _role; }
            set { _role = value; }
        }
	
        #endregion

        #region Constructors
        public User()
        {
            _id = -1;
            _userName = string.Empty;
            _pwd = string.Empty;
            _firstName = string.Empty;
            _lastName = string.Empty;
            _address1 = string.Empty;
            _address2 = string.Empty;
            _city = string.Empty;
            _state = string.Empty;
            _postalCode = string.Empty;
            _isActive = false;
            _roleID = -1;
            _role = string.Empty;
        }
        //TODO: public User(int ID, string UserName, string Password, string
        #endregion

        #region Implicit Operator
        /// <summary>�access specifier� static implicit operator �converting type� (�convertible type� rhs)
        ///  Above signature states that the operator accepts �convertible type� and converts into �converting type�. 
        ///  </summary>
        public static implicit operator User(System.Data.DataRow _row)
        {
            User objUser = new User();
            try
            {
                if (_row != null)
                {
                    objUser.ID = Convert.ToInt32(_row["RowID"]);
                    objUser.UserName = _row["UserName"].ToString();
                    objUser.Password = _row["PWD"].ToString();
                    objUser.FirstName = _row["FirstName"].ToString();
                    objUser.LastName = _row["LastName"].ToString();
                    objUser.Address1 = _row["Address1"].ToString();
                    objUser.Address2 = _row["Address2"].ToString();
                    objUser.City = _row["City"].ToString();
                    objUser.State = _row["State"].ToString();
                    objUser.PostalCode = _row["PostalCode"].ToString();
                    objUser.Role = _row["Role"].ToString();
                    objUser.RoleID = Convert.ToInt32(_row["RoleID"]);
                    objUser.IsActive = Convert.ToBoolean(_row["IsActive"]);
                    //objUser.DateCreated = _row["dCreated"] != DBNull.Value ? (DateTime?)(_row["dCreated"]) : (DateTime?)null;
                    //objUser.DateDeleted = _row["dDeleted"] != DBNull.Value ? (DateTime?)(_row["dDeleted"]) : (DateTime?)null;
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }

            return objUser;
        }
        #endregion

        #region Methods
        public static SortOrder Order
        {
            get { return _order; }
            set { _order = value; }
        }

        public override bool Equals(Object obj)
        {
            bool retVal = false;
            if (obj != null)
            {
                //MktgSource objMktgSource = (MktgSource)obj;
                //if ((objMktgSource.Id == this.Id) &&
                //    (objMktgSource.SiteID.Equals(this.SiteID) &&
                //    (objMktgSource.MktgDesc.Equals(this.MktgDesc))))
                //    retVal = true;
            }
            return retVal;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this._userName;
        }

        public int CompareTo(Object obj)
        {
            switch (_order)
            {
                case SortOrder.Ascending:
                    return this.UserName.CompareTo(((MktgSource)obj).MktgDesc);
                case SortOrder.Descending:
                    return (((MktgSource)obj).MktgDesc).CompareTo(this.UserName);
                default:
                    return this.UserName.CompareTo(((MktgSource)obj).MktgDesc);
            }
        }
        #endregion
    }
}