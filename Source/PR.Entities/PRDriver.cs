﻿using System;

namespace PR.Entities
{
    public class PRDriver
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DriverNumber { get; set; }
        public int DriverStatus { get; set; }
        public int DriverId { get; set; }
        public string UserName { get; set; }
        public int LLogisticsUserID { get; set; }
        public string Password { get; set; }
        public string FacilityName { get; set; }
        public int StoreNo { get; set; }
        public System.TimeSpan ScheduleStartTime { get; set; }
        public System.TimeSpan ScheduleEndTime { get; set; }
        public DateTime ScheduleDate { get; set; }
        public int PrimaryStoreNo { get; set; }
        public string AccessType { get; set; }
        public string DayWork { get; set; }
    }
}
