﻿using System;
using PR.UtilityLibrary;

namespace PR.Entities
{
    public class LDMTouch
    {
        //public PREnums.TouchType TouchType { get; set; }
        public int SequenceNumber { get; set; }
        public bool IsOnlySchedule { get; set; }
        public bool IsOnlyTouchUpdate { get; set; }
        public DateTime ScheduleDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Direction { get; set; }
        public bool IsSloped { get; set; }
        public bool IsSoft { get; set; }
        public bool IsDirt { get; set; }
        public bool IsGravel { get; set; }
        public bool IsPaved { get; set; }
        public bool IsBricked { get; set; }
        public bool IsCurbed { get; set; }
        public string Instructions { get; set; }
        public string Comments { get; set; }
        public bool IsPowerLine { get; set; }
        public bool IsTree { get; set; }
        public bool IsLandscaping { get; set; }
        public bool IsSprinklers { get; set; }
        public bool IsSeptic { get; set; }
        public bool IsFence { get; set; }
        public bool IsDoorFacing { get; set; }
        public bool IsHold { get; set; }
    }
}
