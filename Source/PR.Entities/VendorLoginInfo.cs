﻿using System;

namespace PR.Entities
{
    public class VendorLoginInfo
    {
        string _agentId = String.Empty;
        string _vendorXMLName = String.Empty;
        string _vendorXMLCode = String.Empty;
        string _vendorXMLID = String.Empty;
        string _vendorXMLPassword = String.Empty;
        string _vendorPageTitle = String.Empty;
        string _vendorLongImagePath = String.Empty;
        string _vendorSmallImagePath = String.Empty;
        bool _sendEmailToCust = false;
        string _agentEMail = String.Empty;
        string _accesslevel = String.Empty;
        string _defaultPromoCode = String.Empty;

        public string AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public string VendorXMLName
        {
            get { return _vendorXMLName; }
            set { _vendorXMLName = value; }
        }
        public string VendorXMLCode
        {
            get { return _vendorXMLCode; }
            set { _vendorXMLCode = value; }
        }
        public string VendorXMLID
        {
            get { return _vendorXMLID; }
            set { _vendorXMLID = value; }
        }
        public string VendorXMLPassword
        {
            get { return _vendorXMLPassword; }
            set { _vendorXMLPassword = value; }
        }
        public string VendorPageTitle
        {
            get { return _vendorPageTitle; }
            set { _vendorPageTitle = value; }
        }
        public string VendorLongImagePath
        {
            get { return _vendorLongImagePath; }
            set { _vendorLongImagePath = value; }
        }
        public string VendorSmallImagePath
        {
            get { return _vendorSmallImagePath; }
            set { _vendorSmallImagePath = value; }
        }
        public bool SendEmailToCust
        {
            get { return _sendEmailToCust; }
            set { _sendEmailToCust = value; }
        }
        public string AgentEMail
        {
            get { return _agentEMail; }
            set { _agentEMail = value; }
        }
        public string AccessLevel
        {
            get { return _accesslevel; }
            set { _accesslevel = value; }
        }
        public string DefaultPromoCode
        {
            get { return _defaultPromoCode; }
            set { _defaultPromoCode = value; }
        }
        
    }
}
