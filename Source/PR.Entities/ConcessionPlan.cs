﻿using System;

namespace PR.Entities
{
    public class ConcessionPlan
    {
        int _concessionGlobalNumber =0;
        string _planName = String.Empty;
        bool _neverExpire = false;
        int _expireMonths = 0;
        decimal _chgAmt = 0;
        decimal _fixedAmt = 0;
        decimal _pcDiscount = 0;
        string _chgDesc = String.Empty;
        string _promoDesc = String.Empty;

        public int ConcessionGlobalNumber
        {
            get { return _concessionGlobalNumber; }
            set { _concessionGlobalNumber = value; }
        }
        public string PlanName
        {
            get { return _planName; }
            set { _planName = value; }
        }
        public bool NeverExpire
        {
            get { return _neverExpire; }
            set { _neverExpire = value; }
        }
        public int ExpireMonths
        {
            get { return _expireMonths; }
            set { _expireMonths = value; }
        }
        public decimal ChgAmt
        {
            get { return _chgAmt; }
            set { _chgAmt = value; }
        }
        public decimal FixedAmt
        {
            get { return _fixedAmt; }
            set { _fixedAmt = value; }
        }
        public decimal PCDiscount
        {
            get { return _pcDiscount; }
            set { _pcDiscount = value; }
        }
        public string ChgDesc
        {
            get { return _chgDesc; }
            set { _chgDesc = value; }
        }
        public string PromoDesc
        {
            get { return _promoDesc; }
            set { _promoDesc = value; }
        }
    }
}
