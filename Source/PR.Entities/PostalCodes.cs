﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    [Serializable]
    public class PostalCodes
    {
        public string Zipcode { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string County { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public DateTime UpdatedDate { get; set; }

        public PostalCodeValidationStatus Status { get; set; }
    }
}
