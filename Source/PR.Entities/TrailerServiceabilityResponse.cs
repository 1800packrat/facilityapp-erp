﻿using PR.UtilityLibrary;

namespace PR.Entities
{
    public class TrailerServiceabilityResponse
    {
        public PREnums.TrailerFailedCriteria FailedCriteria { get; set; }
        public Address OriginationAddress { get; set; }
        public Address DestinationAddress { get; set; }
        public AddressValidator AddressValidator { get; set; }
    }
}
