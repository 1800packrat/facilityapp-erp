﻿using PR.UtilityLibrary;

namespace PR.Entities
{
    public  class QuoteConfiguration
    {

        public int ID { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public bool IncludeQuoteCreation { get; set; }
        public bool ExcludeAdminFee { get; set; }
        public PREnums.ServiceType ServiceType { get; set; }
        public bool AgentCanDelete { get; set; }
        public bool SupervisorCanDelete { get; set; }
        public bool ManagerCanDelete { get; set; }
    }
}
