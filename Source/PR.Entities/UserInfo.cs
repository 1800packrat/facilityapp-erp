using System;

namespace PR.Entities
{
    public class UserInfo
    {
        string _userId = String.Empty;
        string _firstName = String.Empty;
        string _lastName = String.Empty;
        string _role = String.Empty;

        public string UserID
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string Role
        {
            get { return _role; }
            set { _role = value; }
        }

        public string EMail { get; set; }
    }
}