﻿using System;

namespace PR.Entities
{
    public class PRStopOffs
    {
        public int stopid { get; set; }
        public int newstopsequence { get; set; }
        public DateTime newplannedstarttime { get; set; }
        public DateTime newplannedendtime { get; set; }
        public bool IgnoreConstraints { get; set; }
        public int LoadId { get; set; }
        
    }
}
