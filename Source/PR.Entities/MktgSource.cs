#region Using directives
using System;
#endregion

namespace PR.Entities
{
    public class MktgSource : IComparable 
    {
        private int _id;
        private int _siteID;
        private string _mktgDesc;

        private string _corpCode = String.Empty;
        private string _locationCode = String.Empty;

        private static SortOrder _order;

        public enum SortOrder 
        {
            Ascending = 0,
            Descending = 1
        }

        public MktgSource(int ID, int SiteID) : this(ID, SiteID, "Other") 
        {
        }

        public MktgSource(int ID, int SiteID, string MktgDesc) 
        {
            this._id = ID;
            this._siteID = SiteID;
            this._mktgDesc = MktgDesc;
        }

        public int Id {
            get { return this._id; }
            set { this._id = value; }
        }

        public int SiteID {
            get { return this._siteID; }
            set { this._siteID = value; }
        }

        public string MktgDesc
        {
            get { return this._mktgDesc; }
            set { this._mktgDesc = value; }
        }

        public string CorpCode
        {
            get {return _corpCode; }
            set {_corpCode = value; }
        }

        public string LocationCode
        {
            get { return _locationCode; }
            set { _locationCode = value; }
        }

        public static SortOrder Order
        {
            get { return _order; }
            set { _order = value; }
        }

        public override bool Equals(Object obj)
        {
            bool retVal = false;
            if (obj != null)
            {
                //MktgSource objMktgSource = (MktgSource)obj;
                //if ((objMktgSource.Id == this.Id) &&
                //    (objMktgSource.SiteID.Equals(this.SiteID) &&
                //    (objMktgSource.MktgDesc.Equals(this.MktgDesc))))
                //    retVal = true;
            }
            return retVal;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this._mktgDesc;
        }

        public int CompareTo(Object obj) {
            switch (_order) {
                case SortOrder.Ascending:
                    return this.MktgDesc.CompareTo(((MktgSource)obj).MktgDesc);
                case SortOrder.Descending:
                    return (((MktgSource)obj).MktgDesc).CompareTo(this.MktgDesc);
                default:
                    return this.MktgDesc.CompareTo(((MktgSource)obj).MktgDesc);
            }
        }

    }
}
