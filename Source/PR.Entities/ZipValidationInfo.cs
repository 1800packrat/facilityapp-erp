using System;
using System.Collections.Generic;
using System.Windows.Forms;
using PR.UtilityLibrary;

namespace PR.Entities
{
    public class ZipValidationInfo
    {
        ZipCodeInfo _zip1 = new ZipCodeInfo();
        ZipCodeInfo _zip2 = new ZipCodeInfo();
        private PREnums.MoveType _moveType;
        private string _errorCode = String.Empty;
        private string _errorMessage = String.Empty;

        public ZipCodeInfo Zip1
        {
            get
            {
                return _zip1;
            }
            set
            {
                _zip1 = value;
            }
        }

        public ZipCodeInfo Zip2
        {
            get
            {
                return _zip2;
            }
            set
            {
                _zip2 = value;
            }
        }

        public PREnums.MoveType MoveType
        {
            get
            {
                return _moveType;
            }
            set
            {
                _moveType = value;
            }
        }

        public String ErrorCode
        {
            get
            {
                return _errorCode;
            }
            set
            {
                _errorCode = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                _errorMessage = value; 
            }
        }
    }

    public class ZipValidationInfoWithTrailers
    {
        public ZipCodeInfo Zip1 { get; set; } = new ZipCodeInfo();
        public ZipCodeInfo Zip2 { get; set; } = new ZipCodeInfo();
        public IList<PREnums.MoveType> MoveTypes { get; set; } = new List<PREnums.MoveType>();
        public string ErrorCode { get; set; } = string.Empty;
        public string ErrorMessage { get; set; } = string.Empty;
    }
}
