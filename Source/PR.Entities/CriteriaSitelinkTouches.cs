﻿namespace PR.Entities
{
    using System;

    public class CriteriaSitelinkTouches
    {
        public string QorIDs { get; set; }

        public string UnitNos { get; set; }
        
        public DateTime? ScheduleFromDate { get; set; } = DateTime.MinValue;

        public DateTime? ScheduleToDate { get; set; } = DateTime.MinValue;

        public string FLocationCodes { get; set; } = string.Empty;

        public string Customerfirstname { get; set; } = string.Empty;

        public string Customerlastname { get; set; } = string.Empty;

        public string CustomerCompanyName { get; set; } = string.Empty;

        //public int QTRentalstatusID { get; set; }
        public string QTRentalstatusIDs { get; set; }

        public string QTTypeIDs { get; set; }

        public string QTStatusIDs { get; set; }

        public string QTRentalTypeIDs { get; set; }

        /// <summary>
        /// If this flag is true, then we should get touches from Salesforce
        /// </summary>
        public bool IsGetLiveData { get; set; } = true;
    }
}
