﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    [DataContract]
    public class Brand : ICloneable
    {
        [DataMember]
        public int BrandID { get; set; }

        [DataMember]
        public string BrandName { get; set; }

        [DataMember]
        public bool DefaultFlag { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool IsShowNotify { get; set; }

        [DataMember]
        public string NotifyMessage { get; set; }

        [DataMember]
        public bool IsAttachTouchTicketInstructions { get; set; }

        [DataMember]
        public string TouchTicketInstructions { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public BrandType BrandType
        {
            get
            {
                return (BrandType)(BrandID);
            }
        }
    }
}
