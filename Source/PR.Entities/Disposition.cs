using System;

namespace PR.Entities
{
    public class Disposition : IComparable
    {
        private int _id;
        private string _dispositionDesc;

        private string _corpCode = String.Empty;
        private string _locationCode = String.Empty;

        private static SortOrder _order;

        public enum SortOrder 
        {
            Ascending = 0,
            Descending = 1
        }

        public Disposition(int ID, int SiteID) : this(ID, "Other") 
        {
        }

        public Disposition(int ID, string DispositionDesc) 
        {
            this._id = ID;
            this._dispositionDesc = DispositionDesc;
        }

        public int Id {
            get { return this._id; }
            set { this._id = value; }
        }

        public string DispositionDesc
        {
            get { return this._dispositionDesc; }
            set { this._dispositionDesc = value; }
        }

        public string CorpCode
        {
            get {return _corpCode; }
            set {_corpCode = value; }
        }

        public string LocationCode
        {
            get { return _locationCode; }
            set { _locationCode = value; }
        }

        public static SortOrder Order
        {
            get { return _order; }
            set { _order = value; }
        }

        public override bool Equals(Object obj)
        {
            bool retVal = false;
            if (obj != null)
            {
                //Disposition objDisposition = (Disposition)obj;
                //if ((objDisposition.Id == this.Id) &&
                //    (objDisposition.SiteID.Equals(this.SiteID) &&
                //    (objDisposition.DispositionDesc.Equals(this.DispositionDesc))))
                //    retVal = true;
            }
            return retVal;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this._dispositionDesc;
        }

        public int CompareTo(Object obj) {
            switch (_order) {
                case SortOrder.Ascending:
                    return this.DispositionDesc.CompareTo(((Disposition)obj).DispositionDesc);
                case SortOrder.Descending:
                    return (((Disposition)obj).DispositionDesc).CompareTo(this.DispositionDesc);
                default:
                    return this.DispositionDesc.CompareTo(((Disposition)obj).DispositionDesc);
            }
        }

    }
}
