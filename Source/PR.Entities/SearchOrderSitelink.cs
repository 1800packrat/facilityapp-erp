﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    public class SearchOrderSitelink
    {
        public string QORID { get; set; }

        public string UnitNumber { get; set; }

        public int StoreNumber { get; set; }
        
        public int SpiritStoreNumber { get; set; }

        public DateTime? FromDate { get; set; } = DateTime.MinValue;

        public DateTime? ToDate { get; set; } = DateTime.MinValue;

        public string SpiritStoreFirstName { get; set; } = string.Empty;

        public string SpiritStoreLastName { get; set; } = string.Empty;

        public string SpiritCompanyName { get; set; }  = string.Empty;

        public int QTRentalstatusID { get; set; }
    }
}
