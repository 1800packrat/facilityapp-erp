namespace PR.Entities
{
    public class TrailerInterstates
    {
        public long Id { get; set; }
        public string ProviderName { get; set; }
        public string StateCode { get; set; }
        public bool Allowed { get; set; }
    }
}
