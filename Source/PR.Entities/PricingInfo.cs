namespace PR.Entities
{
    public class PricingInfo
    {
        private int _qtRentalId = 0;
        private decimal _dueAtDelivery = 0M;
        private decimal _recurringMonthlyCharge = 0M;
        private decimal _futureTransportationCharge = 0M;
        private decimal _recurringMonthlyChargeDiscountedPeriod = 0M;
        private bool _payAsYouGo = false;

        public int QTRentalId
        {
            get { return _qtRentalId; }
            set { _qtRentalId = value; }
        }
        public decimal DueAtDelivery
        {
            get { return _dueAtDelivery; }
            set { _dueAtDelivery = value; }
        }
        public decimal RecurringMonthlyCharge
        {
            get { return _recurringMonthlyCharge; }
            set { _recurringMonthlyCharge = value; }
        }
        public decimal RecurringMonthlyChargeDiscountedPeriod
        {
            get { return _recurringMonthlyChargeDiscountedPeriod; }
            set { _recurringMonthlyChargeDiscountedPeriod = value; }
        }
        public decimal FutureTransportationCharge
        {
            get { return _futureTransportationCharge; }
            set { _futureTransportationCharge = value; }
        }
        public bool PayAsYouGo
        {
            get { return _payAsYouGo; }
            set { _payAsYouGo = value; }
        }
    }
}
