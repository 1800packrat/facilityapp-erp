﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities.Brand
{
    public class SFBrandInfo
    {
        public string BrandId { get; set; }
        public string BrandName { get; set; }
    }

    public class RootObject
    {
        public SFBrandInfo SF_BrandInfo { get; set; }
    }
}
