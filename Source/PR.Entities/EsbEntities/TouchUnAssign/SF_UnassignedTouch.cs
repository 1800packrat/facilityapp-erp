﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities.TouchUnAssign
{
    public class SFUnassignedTouch
    {
        public string QORId { get; set; }
        public string Customer_Name { get; set; }
        public string LDM_QuoteID { get; set; }
        public string LDM_Origin_QORID { get; set; }
        public string LDM_Destination_QORID { get; set; }
        public string LDM_Billing_QORID { get; set; }
        public string LDM_DummyBilling_QORID { get; set; }
        public string LDM_ArrivalDepartureTime { get; set; }
        public string LDM_Origin_LocationCode { get; set; }
        public string LDM_Destination_LocationCode { get; set; }
        public string UnitTypeId { get; set; }
        public string StoreNo { get; set; }
        public string PhoneNo { get; set; }
        public string TouchTypeFull { get; set; }
        public string SequenceNo { get; set; }
        public string UnitName { get; set; }
        public string TouchStatus { get; set; }
        public string IsWeightTicket { get; set; }
        public string UnitId { get; set; }
        public string TouchMiles { get; set; }
        public string Instructions { get; set; }
        public string TouchTime { get; set; }
        public string UnitSize { get; set; }
        public string DoorToFront { get; set; }
        public string DoorToRear { get; set; }
        public DateTime ScheduledDate { get; set; }
        public string Origin_Address1 { get; set; }
        public string Origin_City { get; set; }
        public string Origin_State { get; set; }
        public string Origin_Zip { get; set; }
        public string Origin_Company { get; set; }
        public string Destination_Address1 { get; set; }
        public string Destination_City { get; set; }
        public string Destination_State { get; set; }
        public string Destination_Zip { get; set; }
        public string Destination_Company { get; set; }
        public string IsZippyShellQuote { get; set; }
    }

    public class RootObject
    {
        public List<SFUnassignedTouch> SF_UnassignedTouch { get; set; }
    }
}
