﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities.TouchStage
{
    public class SFStagingTouch
    {
        public string QORId { get; set; }
        public string Customer_Name { get; set; }
        public string LDM_QuoteID { get; set; }
        public string LDM_TripID { get; set; }
        public string LDM_TripNumber { get; set; }
        public string LDM_Origin_QORID { get; set; }
        public string LDM_Destination_QORID { get; set; }
        public string LDM_Billing_QORID { get; set; }
        public string LDM_DummyBilling_QORID { get; set; }
        public string LDM_ArrivalDepartureTime { get; set; }
        public string LDM_Origin_LocationCode { get; set; }
        public string LDM_Destination_LocationCode { get; set; }
        public string UnitTypeId { get; set; }
        public string PhoneNo { get; set; }
        public string TouchTypeShort { get; set; }
        public string SequenceNo { get; set; }
        public string UnitName { get; set; }
        public string TouchStatus { get; set; }
        public string UnitId { get; set; }
        public string TouchMiles { get; set; }
        public string Instructions { get; set; }
        public string TouchTime { get; set; }
        public string UnitSize { get; set; }
        public string DoorToFront { get; set; }
        public string DoorToRear { get; set; }
        public string ScheduledDate { get; set; }
    }

    public class RootObject
    {
        public List<SFStagingTouch> SF_StagingTouch { get; set; }
    }
}
