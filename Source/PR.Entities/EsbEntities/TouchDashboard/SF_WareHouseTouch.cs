﻿using System;
using System.Collections.Generic;

namespace PR.Entities.EsbEntities.TouchDashboard
{
    public class SFWarehouseTouch
    {
        public string WorkOrderNumber { get; set; }
        public string QORId { get; set; }
        public string TouchKey { get; set; }
        public string UnitName { get; set; }
        public string SequenceNo { get; set; }
        public string OrderNo { get; set; }
        public string UnitId { get; set; }
        public string IsLDMOrderTouch { get; set; }
        public string TouchTypeFull { get; set; }
        public string TouchTypeShort { get; set; }
        public string CustomerId { get; set; }
        public string PhoneNo { get; set; }
        public DateTime ScheduledDate { get; set; }
        public string TouchStatusID { get; set; }
        public string TouchStatus { get; set; }
        public string Customer_FirstName { get; set; }
        public string Customer_LastName { get; set; }
        public string TouchMiles { get; set; }
        public string TouchTime { get; set; }
        public string Comments { get; set; }
        public string Customer_Address1 { get; set; }
        public string Customer_Address2 { get; set; }
        public string Customer_City { get; set; }
        public string Customer_Region { get; set; }
        public string Customer_Zip { get; set; }
        public string Origin_FirstName { get; set; }
        public string Origin_LastName { get; set; }
        public string Origin_PhoneNo { get; set; }
        public string Origin_Address1 { get; set; }
        public string Origin_Address2 { get; set; }
        public string Origin_City { get; set; }
        public string Origin_State { get; set; }
        public string Origin_Zip { get; set; }
        public string Destination_FirstName { get; set; }
        public string Destination_LastName { get; set; }
        public string Destination_PhoneNo { get; set; }
        public string Destination_Address1 { get; set; }
        public string Destination_Address2 { get; set; }
        public string Destination_City { get; set; }
        public string Destination_State { get; set; }
        public string Destination_Zip { get; set; }
    }

    public class RootObject
    {
        public List<SFWarehouseTouch> SF_WarehouseTouch { get; set; }
    }
}
