﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities.UpdateResult
{
    public class ResponseData
    {
        public string DestQorId { get; set; }
        public string OriginQorId { get; set; }
        public string OriginUnitName { get; set; }
    }

    public class RootObject
    {
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public ResponseData ResponseData { get; set; }
    }
}
