﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities.UnitData
{
    public class SFUnitInfo
    {
        public string UnitID { get; set; }
        public string UnitName { get; set; }
        public string UnitTypeID { get; set; }
        public string UnitTypeName { get; set; }
        public string IsRented { get; set; }
        public string IsRentable { get; set; }
        public string LocationCode { get; set; }
        public string TripNumber { get; set; }
        public string Origin_QORID { get; set; }
        public string Destination_QORID { get; set; }
    }

    public class RootObject
    {
        public SFUnitInfo SF_UnitInfo { get; set; }
    }
}
