﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities.TouchAvailable
{
    public class SFTouchAvailableSchedule
    {
        public string LocationCode { get; set; }
        public string TouchStatusId { get; set; }
        public string TouchTypeId { get; set; }
        public DateTime ScheduledDate { get; set; }
        public string TouchTime { get; set; }
        public string TouchMiles { get; set; }
    }

    public class RootObject
    {
        public List<SFTouchAvailableSchedule> SF_TouchAvailableSchedule { get; set; }
    }
}
