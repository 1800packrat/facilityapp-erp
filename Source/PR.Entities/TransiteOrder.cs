﻿using System;

namespace PR.Entities
{
    public class TransiteOrder
    {

        private string _qorid = String.Empty;
        private string _touchType = String.Empty;
        private string _status = String.Empty;
        private string _customerName = String.Empty;
        private string _phoneNumber = String.Empty;
        private DateTime _schDate = DateTime.MinValue;
        private string _storeno = String.Empty;
        private string _seqno = String.Empty;
        private string _unitNumber = string.Empty;

        public string QORID
        {
            get { return _qorid; }
            set { _qorid = value; }
        }
        public string TouchType
        {
            get { return _touchType; }
            set { _touchType = value; }
        }
        public string CustomerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }
        public string StoreNO
        {
            get { return _storeno; }
            set { _storeno = value; }
        }
        public DateTime ScheduledDate
        {
            get { return _schDate; }
            set { _schDate = value; }
        }
        public string SequenceNO
        {
            get { return _seqno; }
            set { _seqno = value; }
        }
        public string UnitNumber
        {
            get { return _unitNumber; }
            set { _unitNumber = value; }
        }
        public string StarsID { get; set; }


        public string Confirmed { get; set; }

        public string StartTime { get; set; }
        public string EndTime { get; set; }

    

        public string Comments { get; set; }

        public int OrderId { get; set; }

        public string IsWeightTicket { get; set; }

        public string Obstacles { get; set; }

        public string DriveWay { get; set; }

        public string DoorPOS { get; set; }

        public Address OriginAddress { get; set; }

        public  Address DestAddress { get; set; }

        public String Color { get; set; }

        public string mOrderNumber { get; set; }

        public DateTime mCannotDeliverBefore { get; set; }

        public DateTime mMustDeliverBy { get; set; }

        public DateTime mCannotShipBefore { get; set; }

        public DateTime mMustShipBy { get; set; }
    }
}
