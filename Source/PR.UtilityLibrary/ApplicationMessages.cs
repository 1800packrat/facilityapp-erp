namespace PR.UtilityLibrary
{
    public class ApplicationMessages
    {
        public const string CODE_CANNOT_PROVIDE_QUOTE = "001";
        public const string MSG_CANNOT_PROVIDE_QUOTE = "Due to the nature of your delivery we cannot provide you an online quote. <br /><br />Please call 1-800-Pack-Rat for further assistance.";
        public const string CODE_CANNOT_SCHEDULE_DELIVERY = "002";
        public const string MSG_CANNOT_SCHEDULE_DELIVERY = "Due to the immediate nature of your delivery, we would like to handle your move personally. <br /><br />Please call 1-800-Pack-Rat to receive your quote and delivery detail.";
        public const string CODE_QUOTE_POSSIBLE = "003";
        public const string MSG_QUOTE_POSSIBLE = "Quote Possible.";
        public const string CODE_SLOPE_PRESENT = "004";
        public const string MSG_SLOPE_PRESENT = "Due to the presence of slope in your driveway we may need a site survey before scheduling your delivery. <br /><br />Please call 1-800-Pack-Rat for further assistance.  When calling 1-800-Pack-Rat please be prepared to provide your quote id.";
        public const string CODE_OBSTACLE_PRESENT = "005";
        public const string MSG_OBSTACLE_PRESENT = "Due to the presence of an obstacle that may hinder delivery, we may need a site survey before scheduling your delivery. <br /><br />Please call 1-800-Pack-Rat for further assistance.  When calling 1-800-Pack-Rat please be prepared to provide your quote id.";
        public const string CODE_CREDIT_CARD_NOT_VALID = "006";
        public const string MSG_CREDIT_CARD_NOT_VALID = "The credit card infomation you have provided cannot be validated. <br /><br />Please call 1-800-Pack-Rat to schedule your delivery.  When calling 1-800-Pack-Rat please be prepared to provide your quote id.";
        public const string MSG_CANNOT_SCHEDULE_DELIVERY_SUNDAY = "Due the Sunday delivery nature of your needs, we want to handle your reservation personally.<br /><br /> Please call 1-800-Pack-Rat to complete your reservation. When calling 1-800-Pack-Rat, please be prepared to provide your Quote number.";
        public const string MSG_CALL_Facility = "Please contact ~Facilities~ either of these two facility managers to service the zipcodes provided.";
        public const string MSG_CALL_LDM = "Zipcodes provided are LDM Zipcodes. Please transfer this call to LDM Team.";
        public const string MSG_NO_SERVICE_AVAILABLE = "Service not available to the Zipcodes provided.";
    }
}
