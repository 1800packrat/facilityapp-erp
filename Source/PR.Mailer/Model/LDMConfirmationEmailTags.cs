﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Mailer.Model
{
    public class LDMConfirmationEmailTags : LDMQuoteEmailTags
    {
        public LDMConfirmationEmailTags()
        {
            PosItems = new POSItems();
            ReccuringItems = new ReccuringItems();
            MovingFrom = new MoveSection();
            MovingTo = new MoveSection();
            OtherItems = new OtherItems();
            CppItems = new ContentProtectionItems();
        }
      
        public string Todaysdate { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerPhoneNumber { get; set; }

        public string ScheduleArrivalDate { get; set; }
        public string ScheduleArrivalTime { get; set; }

        public string CancellationFee { get; set; }
        public string ChangeFee { get; set; }
        public string RepositioningFee { get; set; }
        public string FuelAdjustmentRate { get; set; }        

        public MoveSection MovingFrom { get; set; }
        public MoveSection MovingTo { get; set; }

        public POSItems PosItems { get; set; }
        public ReccuringItems ReccuringItems { get; set; }
        public OtherItems OtherItems { get; set; }
        public ContentProtectionItems CppItems { get; set; }

        //from Old type of order before 2016
        public string AdditionalBlankets { get; set; }
        public string MonthlyBlanketsCharge { get; set; }
        public string IncludedLocks { get; set; }
        public string IncludedBlankets { get; set; }
        public string AdditonalLocks { get; set; }
        public string AdditionalLockCharge { get; set; }
        public bool IsZipsyMothQuote { get; set; }
    }

    public class POSItems : PosCount
    {
        public POSItems()
        {
            Units = new List<Model.Unit>();
        }

        public List<Unit> Units;

        //public bool HasFirstMonthCPPCount { get; set; }
        //public bool HasFirstMonthBlanketCount { get; set; }
        //public bool HasFirstMonthLocksCount { get; set; }

        //public bool NoFirstMonthCPPCount { get; set; }
        //public bool NoFirstMonthBlanketCount { get; set; }
        //public bool NoFirstMonthLocksCount { get; set; }
    }

    public class ContentProtectionItems : PosCount// for Old Quote < 2016
    {
        public ContentProtectionItems()
        {
            Units = new List<Model.Unit>();
        }

        public List<Unit> Units;
    }

    public class ReccuringItems : PosCount
    {
        public ReccuringItems()
        {
            Units = new List<Model.Unit>();
        }

        public List<Unit> Units;

        public string BlancketRental { get; set; }
    }

    public class PosCount
    {
        public bool HasCPPCount { get; set; }
        public bool HasBlanketCount { get; set; }
        public bool HasLocksCount { get; set; }

        public bool NoCPPCount { get; set; }
        public bool NoBlanketCount { get; set; }
        public bool NoLocksCount { get; set; }
    }

    public class OtherItems
    {
        public string ExpeditedShipping { get; set; }
        public string ExpeditedCharge { get; set; }
        public string FlexibleTransport { get; set; }
        public string WeightTicketFlag { get; set; }
        public string WeightTicketCharges { get; set; }
         
    }
}
