﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Mailer.Model
{
    public class MailBaseRequest : BaseRequest
    {
        public string apiKey { get; set; }

        public EmailAddressRequest From { get; set; }

        public List<EmailAddressRequest> TOs { get; set; }

        public List<EmailAddressRequest> CCs { get; set; }

        public List<EmailAddressRequest> BCCs { get; set; }

        public string TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string TemplateType { get; set; }

        public Hashtable Tags { get; set; }

        public MailBaseRequest()
        {
            Tags = new Hashtable();
            TOs = new List<EmailAddressRequest>();
            CCs = new List<EmailAddressRequest>();
            BCCs = new List<EmailAddressRequest>();
            sgCat = new SendGridCategories();
        }

        public string MailType { get; set; }

        public SendGridCategories sgCat { private get; set; }

        public List<string> GetSendGridCategories
        {
            get
            {
                List<string> lstSendGridCat = new List<string>();
                if (!string.IsNullOrEmpty(sgCat.ApplicationType)) lstSendGridCat.Add(sgCat.ApplicationType);
                if (!string.IsNullOrEmpty(sgCat.Function)) lstSendGridCat.Add(sgCat.Function);
                if (!string.IsNullOrEmpty(sgCat.TemplateID)) lstSendGridCat.Add(sgCat.TemplateID);
                if (!string.IsNullOrEmpty(sgCat.EmailsTo)) lstSendGridCat.Add(sgCat.EmailsTo);

                if (!string.IsNullOrEmpty(sgCat.TemplateName)) lstSendGridCat.Add(sgCat.TemplateName);
                if (!string.IsNullOrEmpty(sgCat.TemplateType)) lstSendGridCat.Add(sgCat.TemplateType);
                if (!string.IsNullOrEmpty(sgCat.MoveType)) lstSendGridCat.Add(sgCat.MoveType);

                return lstSendGridCat.Count == 0 ? null : lstSendGridCat;
            }
        }

        public string JsonContent { get; set; }
    }

    public class MailBaseRequest<T> : MailBaseRequest where T : new()
    {
        public MailBaseRequest(string APIKey, string RestClientURL)
        {
            base.apiKey = APIKey;
            this.RestClientURL = RestClientURL;
        }

        public T TransactionData { get; set; }
        
        public string Subject { get; set; }

        public string RestClientURL { get; set; } 
    }
}
