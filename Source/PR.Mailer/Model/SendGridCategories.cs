﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Mailer.Model
{
    public class SendGridCategories
    {
        public SendGridCategories()
        { }

        public SendGridCategories(string ApplicationType, string Function, string TemplateID, string EmailsTo)
        {
            this.ApplicationType = ApplicationType;
            this.Function = Function;
            this.TemplateID = TemplateID;
            this.EmailsTo = EmailsTo;
        }

        public SendGridCategories(string ApplicationType, string Function, string TemplateID, string EmailsTo, string MoveType, string TemplateName, string TemplateType)
        {
            this.EmailsTo = EmailsTo;
            this.Function = string.IsNullOrWhiteSpace(Function) ? TemplateType : Function;
            this.TemplateID = TemplateID;
            this.TemplateName = TemplateName;
            this.TemplateType = TemplateType;
            this.ApplicationType = ApplicationType;
            this.MoveType = MoveType;
        }

        private string mApplicationType;

        public string ApplicationType
        {
            get { return string.IsNullOrEmpty(mApplicationType) ? string.Empty : "ApplicationType: " + mApplicationType; }
            set { mApplicationType = value; }
        }

        private string mFunction;

        public string Function
        {
            get { return string.IsNullOrEmpty(mFunction) ? string.Empty : "Function: " + mFunction; }
            set { mFunction = value; }
        }

        private string mTemplateID;

        public string TemplateID
        {
            get { return string.IsNullOrEmpty(mTemplateID) ? string.Empty : "TemplateID: " + mTemplateID; }
            set { mTemplateID = value; }
        }

        private string mEmailsTo;

        public string EmailsTo
        {
            get { return string.IsNullOrEmpty(mEmailsTo) ? string.Empty : "EmailsTO: " + mEmailsTo; }
            set { mEmailsTo = value; }
        }

        private string mMoveType;

        public string MoveType
        {
            get { return string.IsNullOrEmpty(mMoveType) ? string.Empty : "MoveType: " + mMoveType; }
            set { mMoveType = value; }
        }

        private string mTemplateType;

        public string TemplateType
        {
            get { return string.IsNullOrEmpty(mTemplateType) ? string.Empty : "TemplateType: " + mTemplateType; }
            set { mTemplateType = value; }
        }

        private string mTemplateName;

        public string TemplateName
        {
            get { return string.IsNullOrEmpty(mTemplateName) ? string.Empty : "TemplateName: " + mTemplateName; }
            set { mTemplateName = value; }
        }

    }
}
