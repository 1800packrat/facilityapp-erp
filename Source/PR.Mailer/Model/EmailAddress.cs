﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Mailer.Model
{
    public class EmailAddressRequest
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public EmailAddressRequest()
        {

        }
        public EmailAddressRequest(string email, string name = null)
        {
            Email = email;
            Name = name;
        }
    }
}
