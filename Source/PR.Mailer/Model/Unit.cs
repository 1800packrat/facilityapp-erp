﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Mailer.Model
{
    public class Unit
    {
        public int RentDelay { get; set; }        
        public string RentDelayText { get; set; }
        public string UnitType { get; set; }
        public string UnitId { get; set; }
        public string SerialNo { get; set; }
        public string Rent { get; set; }
        

        public string CPPLevel { get; set; }
        public string  CPPTotalPrice { get; set; } 
        public string LockQuantity { get; set; } 
        public string BlanketQuantity { get; set; }
        public string BlanketTotalPrice { get; set; }

        public bool CppIncluded { get; set; }
        public bool BlanketIncluded{ get; set; }
        public bool LockIncluded{ get; set; }

    }
}
