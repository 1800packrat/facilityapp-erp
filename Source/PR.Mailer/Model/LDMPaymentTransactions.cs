﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Mailer.Model
{
    public class LDMPaymentTransactions
    {
        public LDMPaymentTransactions()
        {
            TransportationTransactions = new List<PaymentTransaction>();
            RentalTransactions = new List<PaymentTransaction>();
        }
        public string InVoiceNumber { get; set; }
        public string DateReceived { get; set; }
        public string ADDRESS1 { get; set; }
        public string CustomerCityStateZip { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerEmail { get; set; }
        public string LdmMoveFrom { get; set; }
        public string LdmMovTo { get; set; }
        public string TotalMoveAmount { get; set; }
        public string TotalAmountPaid { get; set; }
        public string TotalCreditWriteOff { get; set; }
        public string LdmBalanceDue { get; set; }
        public List<PaymentTransaction> TransportationTransactions { get; set; }
        public List<PaymentTransaction> RentalTransactions { get; set; }
    }

    public class PaymentTransaction
    {
        public int SRN { get; set; }
        public string PayMode { get; set; }
        public string AmountPaid { get; set; }
        public string DateReceived { get; set; }
        public string CollectedBy { get; set; }
        public string CheckCardNumber { get; set; }
        public string PaymentTowards { get; set; }
    }
}
