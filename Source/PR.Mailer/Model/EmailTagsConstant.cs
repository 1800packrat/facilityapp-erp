﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Mailer.Model
{
    public class EmailTagsConstant
    {
        public const string Subject = "{{Subject}}";
        public const string ReccuringMonthlyDiscount = "{{RecMthlyDiscount}}";
        public const string FirstName = "{{FirstName}}";
        public const string LastName = "{{LastName}}";
        public const string Address = "{{ADDRESS}}";
        public const string Address1 = "{{ADDRESS1}}";
        public const string Address2 = "{{ADDRESS2}}";
        public const string City = "{{city}}";
        public const string Zip = "{{zip}}";
        public const string CustomerPhoneNumber = "{{CustomerPhoneNumber}}";
        public const string CustomerEmail = "{{CustomerEmail}}";
        public const string CompanyName = "{{CompanyName}}";
        public const string MobilePhone = "{{MobilePhone}}";
        public const string EmailAddress = "{{EmailAddress}}";
        public const string QORID = "{{QORID}}";
        public const string QORID_Global = "{{QORID_Global}}";
        public const string ExpirationDate = "{{ExpirationDate}}";
        public const string AnniversaryDate = "{{AnniversaryDate}}";
        public const string UnitSize = "{{UnitSize}}";
        public const string DeliveryFullAddress = "{{DeliveryFullAddress}}";
        public const string DeliveryAddress = "{{DeliveryAddress}}";
        public const string DeliveryCity = "{{DeliveryCity}}";
        public const string DeliveryState = "{{DeliveryState}}";
        public const string DeliveryZIP = "{{DeliveryZIP}}";
        public const string DeliveryDate = "{{DeliveryDate}}";
        public const string DeliveryTime = "{{DeliveryTime}}";
        public const string DeliveryCost = "{{DeliveryCost}}";
        public const string MoveInDate = "{{Move-InDate}}";
        public const string MoveInTime = "{{Move-InTime}}";
        public const string MoveInCost = "{{Move-InCost}}";
        public const string ReDeliveryDate = "{{Re-DeliveryDate}}";
        public const string ReDeliveryTime = "{{Re-DeliveryTime}}";
        public const string ReDeliveryCost = "{{Re-DeliveryCost}}";
        public const string FinalPickupDate = "{{FinalPickupDate}}";
        public const string FinalPickupTime = "{{FinalPickupTime}}";
        public const string FinalPickupCost = "{{FinalPickupCost}}";
        public const string WarehouseAccessDate = "{{WarehouseAccessDate}}";
        public const string WarehouseAccessTime = "{{WarehouseAccessTime}}";
        public const string WarehouseAccessCost = "{{WarehouseAccessCost}}";
        public const string OBODate = "{{OBODate}}";
        public const string OBOTime = "{{OBOTime}}";
        public const string OBOCost = "{{OBOCost}}";
        public const string RelocationDate = "{{RelocationDate}}";
        public const string RelocationTime = "{{RelocationTime}}";
        public const string RelocationCost = "{{RelocationCost}}";
        public const string BillingCity = "{{BillingCity}}";
        public const string BillingState = "{{BillingState}}";
        public const string BillingZip = "{{BillingZip}}";
        public const string TotalDueAtDelivery = "{{TotalDueAtDelivery}}";
        public const string TotalDueAtDeliveryPreTax = "{{TotalDueAtDeliveryPreTax}}";
        public const string FutureTransportation = "{{FutureTransportation}}";
        public const string FutureTransportationPreTax = "{{FutureTransportationPreTax}}";
        public const string Rental = "{{Rental}}";
        public const string RentalPreTax = "{{RentalPreTax}}";
        public const string RentalTax = "{{RentalTax}}";
        public const string Facility1ID = "{{Facility1ID}}";
        public const string CUSTNMBR = "{{CUSTNMBR}}";
        public const string StorageSku = "{{StorageSku}}";
        public const string GUID = "{{GUID}}";
        public const string BillingCycle = "{{BillingCycle}}";
        public const string FuelSurchage = "{{FuelSurchage}}";
        public const string PackratPhoneNumber = "{{PackratPhoneNumber}}";
        public const string PackratLink = "{{PackratLink}}";
        public const string TouchType = "{{TouchType}}";
        public const string TransportationTotal = "{{TransportationTotal}}";
        public const string OldEmployeeFullName = "{{OldEmployeeFullName}}";
        public const string NewEmployeeFullName = "{{NewEmployeeFullName}}";
        public const string OrderType = "{{OrderType}}";
        public const string BookedORAssignedBy = "{{BookedORAssignedBy}}";

        //Related to touchdashboard in facility app
        public const string UnitNumber = "{{UnitNumber}}";
        public const string Comments = "{{Comments}}";
        public const string CustomerName = "{{CustomerName}}";
        public const string TouchStatus = "{{TouchStatus}}";
        public const string DriverID = "{{DriverID}}";
        public const string DriverName = "{{DirverName}}";
        public const string CompletedDateTime = "{{CompletedDateTime}}";

        public const string PurchaseOrder = "{{PurchaseOrder}}";
        public const string TripNumber = "{{TripNumber}}";
        public const string DeliveredUnitNumber = "{{DeliveredUnitNumber}}";
        public const string UnitDescription = "{{UnitDescription}}";
        public const string FacilityName = "{{FacilityName}}";
        public const string LocationCode = "{{LocationCode}}";
        public const string CustRegion = "{{CustRegion}}";

        public const string FromFirstName = "{{FromFirstName}}";
        public const string FromLastName = "{{FromLastName}}";
        public const string FromPhone = "{{FromPhone}}";
        public const string FromAddr1 = "{{FromAddr1}}";
        public const string FromAddr2 = "{{FromAddr2}}";
        public const string FromCity = "{{FromCity}}";
        public const string FromState = "{{FromState}}";
        public const string FromZip = "{{FromZip}}";

        public const string ToFirstName = "{{ToFirstName}}";
        public const string ToLastName = "{{ToLastName}}";
        public const string ToPhone = "{{ToPhone}}";
        public const string ToAddr1 = "{{ToAddr1}}";
        public const string ToAddr2 = "{{ToAddr2}}";
        public const string ToCity = "{{ToCity}}";
        public const string ToState = "{{ToState}}";
        public const string ToZip = "{{ToZip}}";

        public const string TouchCompleteConfirm = "{{TouchCompleteConfirm}}";

        //added categories
        public const string ApplicationType = "{{ApplicationType}}";
        public const string Function = "{{Function}}";
        public const string TemplateID = "{{TemplateID}}";
        public const string EmailsTo = "{{EmailsTo}}";

        // Added for Payment receipt email.
        public const string InVoiceNumber = "{{InVoiceNumber}}";
        public const string CustomerCityStateZip = "{{CustomerCityStateZip}}";
        public const string LdmMoveFrom = "{{LdmMoveFrom}}";   // From Facility
        public const string LdmMovTo = "{{LdmMovTo}}";        // To Facility
        public const string AmountPaid = "{{AmountPaid}}";
        public const string DateReceived = "{{DateReceived}}";
        public const string CollectedBy = "{{CollectedBy}}";
        public const string CheckCardNumber = "{{CheckCardNumber}}";
        public const string PayMode = "{{PayMode}}";
        public const string PayTowards = "{{PaymentTowards}}";
        public const string TotalMoveAmount = "{{TotalMoveAmount}}";
        public const string TotalAmountPaid = "{{TotalAmountPaid}}";
        public const string TotalCreditWriteOff = "{{TotalCreditWriteOff}}";
        public const string LdmBalanceDue = "{{LdmBalanceDue}}";

        public const string LDMQuoteID = "{{LDMQuoteID}}";
        public const string TotalRentalBalanceDue = "{{TotalRentalBalanceDue}}";
    }

    public class EmailTagsForTransactionTemplate
    {
        public string Subject { get; set; }  // "{{Subject }}";
        public string ReccuringMonthlyDiscount { get; set; } // "{{RecMthlyDiscount}}";
        public string FirstName { get; set; } // "{{FirstName}}";
        public string LastName { get; set; } // "{{LastName}}";
        public string Address { get; set; } // "{{ADDRESS}}";
        public string Address1 { get; set; } // "{{ADDRESS1}}";
        public string Address2 { get; set; } // "{{ADDRESS2}}";
        public string City { get; set; } // "{{city}}";
        public string Zip { get; set; } // "{{zip}}";
        public string CustomerPhoneNumber { get; set; } // "{{CustomerPhoneNumber}}";
        public string CustomerEmail { get; set; } // "{{CustomerEmail}}";
        public string CompanyName { get; set; } // "{{CompanyName}}";
        public string MobilePhone { get; set; } // "{{MobilePhone}}";
        public string EmailAddress { get; set; } // "{{EmailAddress}}";
        public string QORID { get; set; } // "{{QORID}}";
        public string QORID_Global { get; set; } // "{{QORID_Global}}";
        public string ExpirationDate { get; set; } // "{{ExpirationDate}}";
        public string AnniversaryDate { get; set; } // "{{AnniversaryDate}}";
        public string UnitSize { get; set; } // "{{UnitSize}}";
        public string DeliveryFullAddress { get; set; } // "{{DeliveryFullAddress}}";
        public string DeliveryAddress { get; set; } // "{{DeliveryAddress}}";
        public string DeliveryCity { get; set; } // "{{DeliveryCity}}";
        public string DeliveryState { get; set; } // "{{DeliveryState}}";
        public string DeliveryZIP { get; set; } // "{{DeliveryZIP}}";
        public string DeliveryDate { get; set; } // "{{DeliveryDate}}";
        public string DeliveryTime { get; set; } // "{{DeliveryTime}}";
        public string DeliveryCost { get; set; } // "{{DeliveryCost}}";
        public string MoveInDate { get; set; } // "{{Move-InDate}}";
        public string MoveInTime { get; set; } // "{{Move-InTime}}";
        public string MoveInCost { get; set; } // "{{Move-InCost}}";
        public string ReDeliveryDate { get; set; } // "{{Re-DeliveryDate}}";
        public string ReDeliveryTime { get; set; } // "{{Re-DeliveryTime}}";
        public string ReDeliveryCost { get; set; } // "{{Re-DeliveryCost}}";
        public string FinalPickupDate { get; set; } // "{{FinalPickupDate}}";
        public string FinalPickupTime { get; set; } // "{{FinalPickupTime}}";
        public string FinalPickupCost { get; set; } // "{{FinalPickupCost}}";
        public string WarehouseAccessDate { get; set; } // "{{WarehouseAccessDate}}";
        public string WarehouseAccessTime { get; set; } // "{{WarehouseAccessTime}}";
        public string WarehouseAccessCost { get; set; } // "{{WarehouseAccessCost}}";
        public string OBODate { get; set; } // "{{OBODate}}";
        public string OBOTime { get; set; } // "{{OBOTime}}";
        public string OBOCost { get; set; } // "{{OBOCost}}";
        public string RelocationDate { get; set; } // "{{RelocationDate}}";
        public string RelocationTime { get; set; } // "{{RelocationTime}}";
        public string RelocationCost { get; set; } // "{{RelocationCost}}";
        public string BillingCity { get; set; } // "{{BillingCity}}";
        public string BillingState { get; set; } // "{{BillingState}}";
        public string BillingZip { get; set; } // "{{BillingZip}}";
        public string TotalDueAtDelivery { get; set; } // "{{TotalDueAtDelivery}}";
        public string TotalDueAtDeliveryPreTax { get; set; } // "{{TotalDueAtDeliveryPreTax}}";
        public string FutureTransportation { get; set; } // "{{FutureTransportation}}";
        public string FutureTransportationPreTax { get; set; } // "{{FutureTransportationPreTax}}";
        public string Rental { get; set; } // "{{Rental}}";
        public string RentalPreTax { get; set; } // "{{RentalPreTax}}";
        public string RentalTax { get; set; } // "{{RentalTax}}";
        public string Facility1ID { get; set; } // "{{Facility1ID}}";
        public string CUSTNMBR { get; set; } // "{{CUSTNMBR}}";
        public string StorageSku { get; set; } // "{{StorageSku}}";
        public string GUID { get; set; } // "{{GUID}}";
        public string BillingCycle { get; set; } // "{{BillingCycle}}";
        public string FuelSurchage { get; set; } // "{{FuelSurchage}}";
        public string PackratPhoneNumber { get; set; } // "{{PackratPhoneNumber}}";
        public string PackratLink { get; set; } // "{{PackratLink}}";
        public string TouchType { get; set; } // "{{TouchType}}";
        public string TransportationTotal { get; set; } // "{{TransportationTotal}}";
        public string OldEmployeeFullName { get; set; } // "{{OldEmployeeFullName}}";
        public string NewEmployeeFullName { get; set; } // "{{NewEmployeeFullName}}";
        public string OrderType { get; set; } // "{{OrderType}}";
        public string BookedORAssignedBy { get; set; } // "{{BookedORAssignedBy}}";

        //Related to touchdashboard in facility app
        public string UnitNumber { get; set; } // "{{UnitNumber}}";
        public string Comments { get; set; } // "{{Comments}}";
        public string CustomerName { get; set; } // "{{CustomerName}}";
        public string TouchStatus { get; set; } // "{{TouchStatus}}";
        public string DriverID { get; set; } // "{{DriverID}}";
        public string DriverName { get; set; } // "{{DirverName}}";
        public string CompletedDateTime { get; set; } // "{{CompletedDateTime}}";

        public string PurchaseOrder { get; set; } // "{{PurchaseOrder}}";
        public string TripNumber { get; set; } // "{{TripNumber}}";
        public string DeliveredUnitNumber { get; set; } // "{{DeliveredUnitNumber}}";
        public string UnitDescription { get; set; } // "{{UnitDescription}}";
        public string FacilityName { get; set; } // "{{FacilityName}}";
        public string LocationCode { get; set; } // "{{LocationCode}}";
        public string CustRegion { get; set; } // "{{CustRegion}}";

        public string FromFirstName { get; set; } // "{{FromFirstName}}";
        public string FromLastName { get; set; } // "{{FromLastName}}";
        public string FromPhone { get; set; } // "{{FromPhone}}";
        public string FromAddr1 { get; set; } // "{{FromAddr1}}";
        public string FromAddr2 { get; set; } // "{{FromAddr2}}";
        public string FromCity { get; set; } // "{{FromCity}}";
        public string FromState { get; set; } // "{{FromState}}";
        public string FromZip { get; set; } // "{{FromZip}}";

        public string ToFirstName { get; set; } // "{{ToFirstName}}";
        public string ToLastName { get; set; } // "{{ToLastName}}";
        public string ToPhone { get; set; } // "{{ToPhone}}";
        public string ToAddr1 { get; set; } // "{{ToAddr1}}";
        public string ToAddr2 { get; set; } // "{{ToAddr2}}";
        public string ToCity { get; set; } // "{{ToCity}}";
        public string ToState { get; set; } // "{{ToState}}";
        public string ToZip { get; set; } // "{{ToZip}}";

        public string TouchCompleteConfirm { get; set; } // "{{TouchCompleteConfirm}}";

        //added categories
        public string ApplicationType { get; set; } // "{{ApplicationType}}";
        public string Function { get; set; } // "{{Function}}";
        public string TemplateID { get; set; } // "{{TemplateID}}";
        public string EmailsTo { get; set; } // "{{EmailsTo}}";

        // Added for Payment receipt email.
        public string InVoiceNumber { get; set; } // "{{InVoiceNumber}}";
        public string CustomerCityStateZip { get; set; } // "{{CustomerCityStateZip}}";
        public string LdmMoveFrom { get; set; } // "{{LdmMoveFrom}}";   // From Facility
        public string LdmMovTo { get; set; } // "{{LdmMovTo}}";        // To Facility
        public string AmountPaid { get; set; } // "{{AmountPaid}}";
        public string DateReceived { get; set; } // "{{DateReceived}}";
        public string CollectedBy { get; set; } // "{{CollectedBy}}";
        public string CheckCardNumber { get; set; } // "{{CheckCardNumber}}";
        public string PayMode { get; set; } // "{{PayMode}}";
        public string PayTowards { get; set; } // "{{PaymentTowards}}";
        public string TotalMoveAmount { get; set; } // "{{TotalMoveAmount}}";
        public string TotalAmountPaid { get; set; } // "{{TotalAmountPaid}}";
        public string TotalCreditWriteOff { get; set; } // "{{TotalCreditWriteOff}}";
        public string LdmBalanceDue { get; set; } // "{{LdmBalanceDue}}";

        public string LDMQuoteID { get; set; } // "{{LDMQuoteID}}";
        public string TotalRentalBalanceDue { get; set; } // "{{TotalRentalBalanceDue}}";
    }

    public class SendGridFunctionType
    {
        public const string DriverApp = "DriverApp";
        public const string LocalDispatch = "LocalDispatch";
        public const string TouchDashboard = "TouchDashboard";

    }
}
