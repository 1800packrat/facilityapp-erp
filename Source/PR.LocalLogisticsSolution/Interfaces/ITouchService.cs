﻿using PR.Entities;
using PR.LocalLogisticsSolution.Model;
using System;
using System.Collections.Generic;

namespace PR.LocalLogisticsSolution.Interfaces
{
    public interface ITouchService
    {
        List<Stops> BreakIntoStop(Touch touch, Address FacilityAddress);

        bool MoveTouchToAnotherLoad(int touchID, int driverID, ref int loadID, DateTime date, SiteInfo site, int truckID);

        ////SEFRP-TODO-RMVNICD 
        //List<Load> FetchLoads(DateTime date, SiteInfo site, out List<Touch> unAssignedTouches);

        ////SEFRP-TODO-RMVNICD 
        //List<Model.Load> FetchLoads(DateTime date);

        ////SEFRP-TODO-RMVNICD 
        //List<Touch> FetchSiteLinkTouches(DateTime date, Entities.SiteInfo site);

        ////SEFRP-TODO-RMVNICD 
        //List<Touch> GetUnassignedTouches(SiteInfo thisSiteInfo, DateTime date);

        ////SEFRP-TODO-RMVNICD 
        //List<Touch> FetchAllUnassignedTouches(Entities.SiteInfo site, DateTime date);

        Touch FetchTouchByTouchId(int touchId, SiteInfo site);

        Touch FetchTouchByTouchId(int touchId);

        Load FetchLoadByLoadId(int loadId);

        bool DeleteLoad(int loadId);

       

        int CreateLoad(int driverId, DateTime date, SiteInfo site, int truckID);

        int AddTouchToLoad(DateTime date, Touch touch, SiteInfo site, int loadID, int driverID, int truckID);

        int AddBreakTouch(DateTime date, int loadId, SiteInfo site, int breakTimeInMinutes, string comment);

        bool RemoveTouchFromLoad(int touchId, SiteInfo site, string activityBy);

        bool RemoveBreakTouchAndStop(int stopId, string activityBy);

        List<Trucks> GetTruckStatus(string storeNumber);

        bool UpdateLoadTruck(int loadId, int truckId);

        bool UpdateLoadOptimized(int loadId, bool isOptimized);

        bool UpdateLoadLock(int loadId, bool isLock);

        List<TouchHandlingTime> FetchTouchHandlingTimes(string storeNo);

        TouchStopHandlingTime FetchTouchStopHandlingTimes(string storeNo);

        List<PR.Entities.PRDriver> GetAllDriversByFacility(string faciltyNumber, DateTime date);

        bool GroupTouches(int[] TouchIds, int[] AllTouchIds);

        bool UnGroupTouches(int loadid, Guid guid, Address FacilityAddress);

        bool SaveSortedTouchInfo(int loadId, List<Touch> seqTouches, string[] touchTrailerMapIds, Address facilityAddress);

        bool SaveOptimizedStops(List<Stops> seqStops, int loadId);//, bool isOptimized

        List<Trailers> GetTrailers(string storeNumber);

        bool SaveDriverStartTimeNTrailerRequired(int loadId, string startTime, bool isTrailerRequired);

        List<Entities.Address> FetchAllAddressForThisLoad(int loadId);

        bool TruckScheduleValidation(int loadId, DateTime scheduleTime);

        void UpdateAddressByID(Entities.Address address);

        void UpdateLatLongForIntesection(Entities.Address address);

        bool CheckTouchCanRemoveFromLoad(int touchId, out string reasons);

        List<Load> GetAllLiteLoadByStoreNumAndDate(DateTime date, string storeNumber);

        Address ManageAddress(Address address); 

        /// <summary>
        /// This method will return only Facility information it does not include markets info
        /// </summary>
        /// <returns></returns>
        List<SiteInfo> GetFaciltyInformation();

        /// <summary>
        /// This method is responsible to get Facility Information with markets info
        /// </summary>
        /// <returns></returns>
        List<SiteInfo> GetFaciltyInformationWithMarkets();



        void TouchCancledAtSL(Touch touch);

        void TouchUpdateFromSyncJob(Touch lTouch, Touch syncTouch);

        List<int> SyncSLUpdatesWithRO(List<Touch> dbTouches, List<Touch> slTouches, SiteInfo site, string actionFrom);

        void AddEvent(EventLogEntity evnt);

        void AddStackTrace(int UserId, string MessageType, string MessageBody, string MessageArea, DateTime CreateDate, string CallStack);
              
        List<Load> GetAllLiteLoadByMarketAndDate(DateTime date, SiteInfo currentSite);

        bool RemoveTouchFromMarketLoad(int touchId, SiteInfo site, string activityBy);

        bool MoveMarketTouchToAnotherLoad(int touchID, int driverID, ref int loadID, DateTime date, SiteInfo touchDriverFacility, SiteInfo currentSiteInfo, int truckID, string activityBy);

        int AddTouchToMarketLoad(DateTime date, Touch touch, SiteInfo touchDriverFacility, SiteInfo touchHomeFacility, int loadID, int driverID, int truckID, string activityBy);

        bool UnGroupMarketTouchesBase(int loadid, Guid guid, SiteInfo Facility);

        bool SaveSortedMarketTouchInfo(int loadId, List<Touch> seqTouches, string[] touchTrailerMapIds, SiteInfo facility);

        int HandleAddTouchToRoute(Touch exceptionTouch, SiteInfo facilityInfo, int loadId, int previousTouchId, int previousStopId, bool isExceptionalTouch = true);

        List<string> GetCompletedTouchesOrderNumberFacility(string storeNumber, DateTime date);

        List<Touch> GetAllTouchesForFacility(string storeNumber, DateTime date);

        Touch GetRecentlyCompletedTouchFromLoad(int loadId);

        //SFERP-TODO-CTUPD - TBD
        ScheduleCalendar GetCapacity(DateTime StartDate, DateTime EndDate, string LocationCode);

        void RescheduleSkippedTouch(int touchId, SiteInfo facilityInfo);

        List<Touch> GetTransportationTouches(Entities.SiteInfo site, DateTime date, bool isGetLiveSLData);

        List<Touch> GetTransportationTouches(List<SiteInfo> lstSites, DateTime date, bool isGetLiveSLData);

        List<Touch> GetTransportationTouches(List<SiteInfo> lstSites, DateTime startdate, DateTime enddate, bool isGetLiveSLData);

        List<Touch> GetTransportationTouches(CriteriaSitelinkTouches criteriaSitelinkTouches, List<SiteInfo> lstSiteInfo);

        Market GetUnassignedTouches(Entities.SiteInfo site, DateTime date, List<Touch> marketSLTouches);

        Market GetMarketLoads(DateTime date, SiteInfo site, List<Touch> marketSLTouches);

        List<Touch> GetTouchesForMarket(Entities.SiteInfo site, DateTime date, List<Touch> marketTouchesFromSL);

        void RemoveCachedAddress(Address addr);

        ScheduleCalendar GetFacilityCapacity(DateTime StartDate, DateTime endDate, string LocationCode);

        void AddRemoveTouchtoDriverESB(DateTime date, Touch touch, int driverID, string activityBy);
    }
}

