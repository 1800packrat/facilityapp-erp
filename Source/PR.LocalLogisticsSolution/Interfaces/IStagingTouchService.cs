﻿using PR.Entities;
using PR.LocalLogisticsSolution.Model;
using System;
using System.Collections.Generic;

namespace PR.LocalLogisticsSolution.Interfaces
{
    public interface IStagingTouchService
    {
        List<Touch> GetStagingTouches(SiteInfo site, DateTime StartDate, DateTime EndDate);

        void CompleteIBOTouch(int QORId, int sequenceNum, int StarsId, string applicationName, string activityBy, string unitNo);

        void CompleteWHTouch(int QORId, int sequenceNum, int StarsId, string applicationName, string activityBy, string unitNo);

        void CompleteOBOTouch(int QORId, int sequenceNum, int StarsId, string applicationName, string activityBy, string unitNo);

        bool TransferOut(int QORId, int dummyBillingQORId, int billingQORId, int TripId, int StarsId, string applicationName, string userName);

        //bool TransferIn(string unitName, int UnitId, int destQORId, int billingQORId, int TripId, int StarsId, string orgLocationCode, string destLocationCode, string sessionId, int ContainerType);

        bool TransferIn(string unitName, int UnitId, int orgQORId, int destQORId, int billingQORId, int TripId, int StarsId, string orgLocationCode, string destLocationCode, string sessionId, int ContainerType, string applicationName, string userName);
    }
}
