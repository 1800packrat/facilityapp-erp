﻿using System;
using System.Collections.Generic;
using PR.Entities;
using PR.LocalLogisticsSolution.Model;
using static PR.UtilityLibrary.PREnums;

namespace PR.LocalLogisticsSolution.Interfaces
{
    public interface IDBRepository
    {
        //Address GetWeightStation(string StoreNumber);

        //List<PRDriver> GetAllDriversByFacility(string faciltyNumber, DateTime date);
        List<PRDriver> GetAllDriversByFacility(string StoreNo, DateTime date, int? marketId = null);

        //PRDriver GetDriver(int driverId);

        DriverSchedule GetDriverSchedule(DateTime date, int driverId);

        //LogisticsUser Login(string username, string password);
        
        FADALoginStatus Login(string username, string encryptPassword, string IPAddress, Application Application, ref LogisticsUser user);

        //List<LogisticsUser> GetUserList();

        List<Trucks> GetTruckStatus(string storeNumber);

        List<Trucks> GetAllTrucksByStoreNumber(string storeNumber);

        List<SiteInfo> GetAllFacilities();

        long InsertActivityLog(int qorid, string activityType, string activityText, string activityBy, int count, int starsId, string applicationName);

        long InsertActivityLog(int qorid, int activityTypeId, string activityText, string activityBy, int count, int starsId, string applicationName);

        List<LogisticsUser> GetAllDrivers(string Name, int StoreNo, bool IsActive);

        LogisticsUser GetDriverById(int ID);

        int SaveDriverDetails(LogisticsUser driver);

        bool SetDriverStatus(LogisticsUser driver);

        List<DriverScheduleHourChangeAndStatus> CheckUpdateDriverScheduleHours(int driverId, List<DateTime> ListScheduleDates);

        bool CheckLoadExistForGivenDateRange(int driverId, DateTime StartDate, DateTime EndDate);

        List<DateTime> GetDriverLoadDates(int driverId, DateTime StartDate);
    }
}
