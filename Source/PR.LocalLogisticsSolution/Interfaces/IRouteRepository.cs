﻿using PR.Entities;
using PR.LocalLogisticsSolution.Model;
using System;
using System.Collections.Generic;

namespace PR.LocalLogisticsSolution.Interfaces
{
    public interface IRouteRepository
    {
        List<Touch> FetchTouches(DateTime date, string storeNumber);

        Touch FetchTouchByTouchId(int touchId);

        Load FetchLoadByLoadId(int loadId);

        List<Stops> GetAllStopsByDriver(int driverNumber, DateTime touchDate);

        List<Stops> GetAllStopsByLoad(int loadId);

        List<Load> GetAllLoadsByDate(DateTime date, string storeNumber);

        List<Load> FetchLoadsFromSP(DateTime date, string storeNumber);

        int CreateLoad(int driverId, DateTime date, string storeNumber, string storeName, int truckID);

        int SaveTouch(Touch touch);

        bool RemoveTouch(int touchId);

        bool RemoveBusyTouchAndStops(int stopId, string activityBy);

        void MoveTouchToAnotherLoad(int touchID, int loadID, bool IsExceptionalTouch = false);

        bool UpdateLoadTruck(int loadId, int truckId);

        bool UpdateLoadOptimized(int loadId, bool isOptimized);

        bool UpdateLoadLock(int loadId, bool isLock);

        bool DeleteLoad(int loadId);

        List<TouchHandlingTime> FetchTouchHandlingTimes(string storeNo);

        bool UpdateGroupingStatus(List<Touch> touches, Dictionary<int, int> touchesAndOrder);

        //bool UpdateUnGroupingStatus(List<Touch> touches);
        bool UpdateUnGroupingStatus(List<Touch> touches, bool isDriverAppUngroupCall = false);

        //bool SaveTouchSortOrder(List<int> seqTouchIds, string[] touchTrailerMapIds);

        //bool SaveSortedTouchInfo(List<Touch> seqTouches);
        bool SaveSortedTouchInfo(List<Touch> seqTouches, PR.Entities.Address FacilityAddress);

        bool SaveOptimizedStopSortOrder(List<Stops> seqStops, int loadId);

        Entities.Address ManageAddress(PR.Entities.Address address);

        //bool SaveOptimizedStopsFromStopIds(List<int> stopIds);

        List<Trailers> GetTrailers(string storeNumber);

        List<Touch> FetchTouchByLoadAndGuidId(int loadid, Guid guid);

        bool SaveDriverStartTimeNTrailerRequired(int loadId, string startTime, bool isTrailerRequired);

        List<Entities.Address> FetchAllAddressForThisLoad(int loadId);

        List<Load> GetAllLiteLoadByStoreNumAndDate(DateTime date, string storeNumber);

        void UpdateAddressByID(Entities.Address address);

        void UpdateLatLongForIntesection(Entities.Address address);

        LogisticsUser GetDriverformation(int driverId);

        bool CheckIfUserNameExsists(string userName);

        void CreateDriverLogin(PRDriver driver);

        bool CheckTouchCanRemoveFromLoad(int touchId, out string reasonTypes);

        List<Load> GetAllLoadsByDate(DateTime date);

        void TouchCancledAtSL(Touch touch);

        void TouchUpdateFromSyncJob(Touch touch, Touch tmpSyncTouch);

        void AddEvent(EventLogEntity evnt);

        void AddStackTrace(SystemMessagesEntity evnt);

        List<SiteInfo> GetMarketFacilities(int marketId);

        SiteInfo AddMarketFacilities(SiteInfo site);

        bool SaveSortedMarketTouchInfo(List<Touch> seqTouches, SiteInfo marketFacilites);

        void SaveTouchRouteSequenct(int loadId, int previousTouchId, int exceptionTouchId, bool isExistingTouch);

        Touch GetRecentlyCompletedTouchFromLoad(int loadId);

        List<string> GetCompletedTouchesOrderNumberFacility(string storeNumber, DateTime date);

        List<Touch> GetAllTouchesForFacility(string storeNumber, DateTime date);

        void UpdateGUIDForTouches(List<Touch> lstTouches, Guid guid);

        bool UpdateUnGroupingStatusOnlyPreviousTouch(Touch touch);

        void UpdateStartAddressAsFacilityForNextTouchOfExceptionTouch(Touch nextTouch);

        string CheckDriverLoad(int driverId, DateTime date);

        List<Touch> FetchTouchesFromSP(DateTime date, string storeNumber);
    }
}
