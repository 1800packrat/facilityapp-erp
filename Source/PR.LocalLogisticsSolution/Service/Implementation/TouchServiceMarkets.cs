﻿using PR.LocalLogisticsSolution.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using PR.LocalLogisticsSolution.Model;
using PR.Entities;
using PR.UtilityLibrary;

namespace PR.LocalLogisticsSolution.Service.Implementation
{
    partial class TouchService : ITouchService
    {
        private SiteInfo GetSiteInformationBySiteNumber(string siteNumber, SiteInfo marketSiteInfo)
        {
            return (marketSiteInfo.GlobalSiteNum == siteNumber ? marketSiteInfo : marketSiteInfo.MarketFacilities.Where(m => m.GlobalSiteNum == siteNumber).FirstOrDefault());
        }

        private List<Touch> FetchAllLoadTouchesForMarketFromSP(Entities.SiteInfo site, DateTime date)
        {
            //Update logic to send market globalsitenumbers
            string lstSite = site.GlobalSiteNum;

            if (site.IsMarket)
                lstSite = $"{lstSite},{string.Join(",", site.MarketFacilities.Select(m => m.GlobalSiteNum.ToString()).ToList())}";

            return _routeRepository.FetchTouchesFromSP(date, lstSite);
 
        }

        public bool MoveMarketTouchToAnotherLoad(int touchID, int driverID, ref int loadID, DateTime date, SiteInfo touchDriverFacility, SiteInfo currentSiteInfo, int truckID, string activityBy)
        {
            //call repo method to udpate the touch to another load
            //check if data exisits in  optimized stop info table and delete it???
            bool rtFlag = true;
            try
            {
                if (loadID <= 0) //New Load, need to create a load and then assign this touch to that load
                {
                    loadID = _routeRepository.CreateLoad(driverID, date, touchDriverFacility.GlobalSiteNum, touchDriverFacility.SiteName, truckID);
                }

                //If this touch is in any group then we have to ungroup it and then go for moving this touch to new load
                var touch = _routeRepository.FetchTouchByTouchId(touchID);
                if (touch.GroupGUID != null)
                {
                    UnGroupMarketTouches(touch.LoadId.Value, touch.GroupGUID.Value, currentSiteInfo);
                }

                _routeRepository.MoveTouchToAnotherLoad(touchID, loadID);

                //Update Salesforce touch for Touch Assignment. TG-729
                AddRemoveTouchtoDriverESB(date, touch, driverID, activityBy);

                if (touch != null && touch.LoadId > 0)
                    UpdateLoadOptimized(touch.LoadId.Value, false);

                UpdateLoadOptimized(loadID, false);
            }
            catch (Exception ex)
            {
                rtFlag = false;
                throw ex;
            }

            return rtFlag;
        }


        /// <summary>
        /// Method to assign or remove touch from Driver, Update detilas into Salesforce using ESB endpoint: TG-729
        /// </summary>
        /// <param name="date"></param>
        /// <param name="touch"></param>
        /// <param name="driverID"></param>
        /// <param name="activityBy"></param>
        public void AddRemoveTouchtoDriverESB(DateTime date, Touch touch, int driverID, string activityBy)
        {
            //Added by Sohan 
            _sitelinkRepository.UpdateTouchAssignment(date, touch, driverID, activityBy);
        }

        public int AddTouchToMarketLoad(DateTime date, Touch touch, SiteInfo touchDriverFacility, SiteInfo touchHomeFacility, int loadID, int driverID, int truckID, string activityBy)
        {
            // bool rtFlag = true;
            int TouchId = 0;
            try
            {
                if (loadID <= 0)
                {
                    touch.LoadId = _routeRepository.CreateLoad(driverID, touch.ScheduledDate, touchDriverFacility.GlobalSiteNum, touchDriverFacility.SiteName, truckID);
                }
                else //Since we have retrieved touch from session, we need to assign loadEd explicitly
                    touch.LoadId = loadID;

                touch.OriginAddress.AddressType = AddressComparison(touch.OriginAddress, touchHomeFacility.SiteAddress) ? PREnums.AddressType.Warehouse : PREnums.AddressType.Customer;
                touch.DestAddress.AddressType = AddressComparison(touch.DestAddress, touchHomeFacility.SiteAddress) ? PREnums.AddressType.Warehouse : PREnums.AddressType.Customer;
                touch.DestAddress.FacilityStoreNo = touch.OriginAddress.FacilityStoreNo = !string.IsNullOrEmpty(touchHomeFacility.GlobalSiteNum) ? Convert.ToInt32(touchHomeFacility.GlobalSiteNum) : (int?)null;

                touch.OriginAddress = _routeRepository.ManageAddress(touch.OriginAddress);
                touch.DestAddress = _routeRepository.ManageAddress(touch.DestAddress);
                Address facilityAddress = _routeRepository.ManageAddress(touchHomeFacility.SiteAddress);

                //Breake touch into stops as per the touch type
                BreakIntoStop(touch, facilityAddress);

                //Save touch to DB - TouchInformation Table
                TouchId = _routeRepository.SaveTouch(touch);

                //Update Salesforce touch for Touch Assignment. TG-729
                AddRemoveTouchtoDriverESB(date, touch, driverID, activityBy);

                if (touch.LoadId > 0)
                    UpdateLoadOptimized(touch.LoadId.Value, false);
            }
            catch (Exception ex)
            {
                //rtFlag = false;
                throw ex;
            }

            return TouchId;
        }

        public bool UnGroupMarketTouchesBase(int loadid, Guid guid, SiteInfo Facility)
        {
            bool rtVal = true;

            UnGroupMarketTouches(loadid, guid, Facility);
            //UnThisGroupTouches(loadid, guid, FacilityAddress);

            UpdateLoadOptimized(loadid, false);

            return rtVal;
        }

        public bool SaveSortedMarketTouchInfo(int loadId, List<Touch> seqTouches, string[] touchTrailerMapIds, SiteInfo marketFacilities)
        {
            bool rtFlag = false;
            try
            {
                Dictionary<int, int> dcTouchTrailerMap = new Dictionary<int, int>();
                if (touchTrailerMapIds != null && touchTrailerMapIds.Length > 0)
                {
                    foreach (var touchTrailerId in touchTrailerMapIds)
                    {
                        string[] temp = touchTrailerId.Split(',');
                        if (temp.Length == 2)
                        {
                            dcTouchTrailerMap.Add(Convert.ToInt32(temp[0]), Convert.ToInt32(temp[1]));
                        }
                    }
                }
                int seq = 0;
                foreach (Touch touch in seqTouches)
                {
                    seq++;
                    touch.TrailerId = (dcTouchTrailerMap != null && dcTouchTrailerMap.ContainsKey(touch.TouchId) ? dcTouchTrailerMap[touch.TouchId] : 0);
                    touch.RouteSequence = seq;
                }

                //facilityAddress = _routeRepository.ManageAddress(facilityAddress);
                UpdateGroupMarketTocuhesAddressUpdatedBeforeSave(loadId, seqTouches, marketFacilities);

                _routeRepository.SaveSortedMarketTouchInfo(seqTouches, marketFacilities);

                UpdateLoadOptimized(loadId, false);

                rtFlag = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtFlag;
        }

        public bool RemoveTouchFromMarketLoad(int touchId, SiteInfo site, string activityBy)
        {
            bool rtVal = false;
            try
            {
                if (touchId > 0)
                {
                    var touch = _routeRepository.FetchTouchByTouchId(touchId);
                    int? loadId = touch.LoadId;

                    //If this touch is in group then we have ungroup this group 
                    if (touch.GroupGUID != null && loadId > 0)
                    {
                        UnGroupMarketTouches(loadId.Value, touch.GroupGUID.Value, site);
                    }

                    _routeRepository.RemoveTouch(touchId);

                    ////Update Salesforce touch for Touch Assignment. TG-729
                    //AddRemoveTouchtoDriverESB(DateTime.Now, touch, 0, activityBy);

                    if (loadId.HasValue && loadId > 0)
                        UpdateLoadOptimized(loadId.Value, false);

                    rtVal = true;
                }
            }
            catch (Exception ex)
            {
                rtVal = true;
                throw ex;
            }

            return rtVal;
        }

        /// <summary>
        /// If we did not find load for a scheduled driver, then we should create an empty load 
        /// This method will create empty loads for the driver whihc are scheduled for today
        /// </summary>
        /// <param name="date"></param>
        /// <param name="site"></param>
        /// <param name="allPRDrivers"></param>
        /// <returns></returns>
        public List<Model.Load> CreateEmptyLoadForScheduledDrivers(DateTime date, SiteInfo site, List<PRDriver> allPRDrivers)
        {
            List<Load> lstLoads = new List<Load>();
            Load load = null;

            foreach (var driver in allPRDrivers)
            {
                load = null;
                DriverSchedule schedule = new DriverSchedule
                {
                    ScheduleDate = driver.ScheduleDate,
                    DayOfWeek = driver.ScheduleDate.DayOfWeek.ToString(),
                    ScheduleStartTime = driver.ScheduleStartTime,
                    ScheduleEndTime = driver.ScheduleEndTime
                };
                load = new Load(0, driver.DriverId, site.GlobalSiteNum, 0, false, false, false, date, null);//DriverNumber -TODO need to check
                load.DriverName = driver.FirstName + " " + driver.LastName;
                //load.FacCode = site.FacCode;
                load.Date = date.ToShortDateString();
                load.DriverSchedule = schedule;

                //Get driver start time from 3GTMS and assign his default time is as that.
                DateTime newtime = DateTime.Now;
                DateTime.TryParse(schedule.StartTime, out newtime);
                load.ScheduleStartDateTime = newtime;

                string checkLoadCreated = _routeRepository.CheckDriverLoad(driver.DriverId, date);

                if (!string.IsNullOrEmpty(checkLoadCreated))
                {
                    load.IsAlreadyLoadCreatedForOtherFacilityForThisDriver = true;
                    load.StoreNoAlreadyLoadCreatedForOtherFacilityForThisDriver = checkLoadCreated;
                }

                lstLoads.Add(load);
            }

            return lstLoads;
        }

        private void UpdateGroupMarketTocuhesAddressUpdatedBeforeSave(int loadId, List<Touch> seqTouches, SiteInfo MarketFacility)
        {
            Load load = _routeRepository.FetchLoadByLoadId(loadId);
            int[] allTouchIds = seqTouches.OrderBy(o => o.RouteSequence).Select(s => s.TouchId).ToArray();
            List<Touch> addressChangedTouches = load.Touches.Where(t => t.SLUpdatedStatusType == SLUpdateType.AddressAndInfoUpdated || t.SLUpdatedStatusType == SLUpdateType.OnlyAddressUpdated).ToList();
            List<Guid> groupIds = addressChangedTouches.Where(t => t.GroupGUID != null).Select(s => s.GroupGUID.Value).Distinct().ToList();

            if (groupIds != null && groupIds.Count > 0)
            {
                foreach (var groupId in groupIds)
                {
                    int[] touchIds = load.Touches.Where(t => t.GroupGUID == groupId).Select(ts => ts.TouchId).ToArray();

                    UnGroupMarketTouches(loadId, groupId, MarketFacility);

                    GroupTouches(touchIds, allTouchIds);
                }
            }
        }

        private bool UnGroupMarketTouches(int loadid, Guid guid, SiteInfo currentSite)
        {
            List<Touch> touches = _routeRepository.FetchTouchByLoadAndGuidId(loadid, guid);
            Address facilityAddress = new Address();

            for (int index = 0; index < touches.Count(); index++)
            {
                var facilityStoreNum = touches[index].GlobalSiteNum;
                Address FacilityAddress = new Address();

                if (!string.IsNullOrEmpty(facilityStoreNum))
                    FacilityAddress = (currentSite.GlobalSiteNum == facilityStoreNum ? currentSite.SiteAddress : currentSite.MarketFacilities.Where(m => m.GlobalSiteNum == facilityStoreNum).FirstOrDefault().SiteAddress);
                else
                    FacilityAddress = currentSite.SiteAddress;

                facilityAddress = _routeRepository.ManageAddress(FacilityAddress);

                touches[index].IsGrouped = false;
                touches[index].GroupGUID = null;

                //Need to implement this method to handle ungrouping touches
                UpdateStopsBasedonTouchUnGrouping(touches[index], facilityAddress);
            }

            return _routeRepository.UpdateUnGroupingStatus(touches);
        }


        //public void createDriver(int driverID, string facilityNumber)
        //{
        //    try
        //    {
        //        var userExists = _routeRepository.GetDriverformation(driverID);

        //        if (userExists == null)
        //        {
        //            PRDriver oPRDriver = _dbRepository.GetDriver(driverID);

        //            oPRDriver.UserName = _userService.FetchSuggestedDriverUserName(oPRDriver.FirstName, oPRDriver.LastName, facilityNumber);

        //            _userService.CreateDriverLogin(oPRDriver);
        //        }
        //    }
        //    catch (Exception ex) { }
        //}

        public void RescheduleSkippedTouch(int touchId, SiteInfo facilityInfo)
        {
            var skippedTouch = _routeRepository.FetchTouchByTouchId(touchId);

            if (!string.IsNullOrWhiteSpace(skippedTouch.GroupGUID.ToString()))
            {
                Load oExceptionload = _routeRepository.FetchLoadByLoadId(Convert.ToInt32(skippedTouch.LoadId));

                //Touch dbExceptionTouch = oExceptionload.Touches.Where(t => t.TouchId == exceptionTouch.TouchId).First();

                HandleUnGroupInExceptionTouchRoute(facilityInfo, skippedTouch, oExceptionload);
            }

            //3. Assign exception touch to current route
            _routeRepository.MoveTouchToAnotherLoad(skippedTouch.TouchId, skippedTouch.LoadId.Value, false);//
        }

        public Market GetUnassignedTouches(Entities.SiteInfo site, DateTime date, List<Touch> marketSLTouches)
        {
            Market market = new Market();
            List<Touch> marketLoadTouches = FetchAllLoadTouchesForMarketFromSP(site, date);

            FacilityInfo facilityInfo = new FacilityInfo
            {
                GlobalSiteNum = site.GlobalSiteNum,
                LegalName = site.LegalName,
                LocationCode = site.LocationCode,
                SiteName = site.SiteName,
                Touches = FilterFacilityUnAssignedTouches(site, date, marketLoadTouches, marketSLTouches)
            };

            market.Facilities.Add(facilityInfo);

            if (site.IsMarket == true)
            {
                foreach (var marketFacility in site.MarketFacilities)
                {
                    facilityInfo = new FacilityInfo
                    {
                        GlobalSiteNum = marketFacility.GlobalSiteNum,
                        LegalName = marketFacility.LegalName,
                        LocationCode = marketFacility.LocationCode,
                        SiteName = marketFacility.SiteName,
                        Touches = FilterFacilityUnAssignedTouches(marketFacility, date, marketLoadTouches, marketSLTouches)
                    };

                    market.Facilities.Add(facilityInfo);
                }
            }

            SyncSLUpdatesWithRO(ref marketLoadTouches, marketSLTouches, site, "FacilityApp-GridView");

            return market;
        }

        private List<Touch> FilterFacilityUnAssignedTouches(Entities.SiteInfo site, DateTime date, List<Touch> marketLoadTouches, List<Touch> marketSLTouches)
        {
            var rows = from t1 in marketSLTouches.Where(sl => sl.GlobalSiteNum == site.GlobalSiteNum).AsEnumerable()
                       join t2 in marketLoadTouches.AsEnumerable()
                       on t1.OrderNumber equals t2.OrderNumber into tg
                       from tcheck in tg.DefaultIfEmpty()
                       where tcheck == null
                       orderby t1.DisplayOrder ascending
                       select t1;

            rows.ToList().ForEach(t =>
            {
                t.GlobalSiteNum = site.GlobalSiteNum;
                SetColor(ref t, t.StartTimeZone);
            });

            return rows.ToList();
        }

        public Market GetMarketLoads(DateTime date, SiteInfo site, List<Touch> marketSLTouches)
        {
            Market market = new Market();

            //SFERP-TODO: Need to update SP for SF Tables/View
            List<PRDriver> allprdrivers = _userService.GetAllDriversByFacility(site.GlobalSiteNum, date, site.MarketId);

            string storeNumber = site.GlobalSiteNum;
            if (site.IsMarket)
                storeNumber = $"{storeNumber},{string.Join(",", site.MarketFacilities.Select(m => m.GlobalSiteNum.ToString()).ToList())}";

            List<Load> allDBLoads = _routeRepository.FetchLoadsFromSP(date, storeNumber);

            FacilityInfo facilityInfo = new FacilityInfo
            {
                GlobalSiteNum = site.GlobalSiteNum,
                LegalName = site.LegalName,
                SiteName = site.SiteName,
                LocationCode = site.LocationCode
            };

            ///get this facility drivers form allprdrivers list, allprdriver list may have market facility drivers.
            List<PRDriver> thisFacilityDrivers = allprdrivers.Where(d => d.StoreNo == Convert.ToInt32(facilityInfo.GlobalSiteNum)).ToList();
            List<Load> thisFacilityLoads = allDBLoads.Where(l => l.GlobalSiteNumber == site.GlobalSiteNum).ToList();
            facilityInfo.Loads = GetFacilityLoads(date, site, thisFacilityDrivers, marketSLTouches, thisFacilityLoads);

            market.Facilities.Add(facilityInfo);

            if (site.IsMarket)
            {
                foreach (var marketFacility in site.MarketFacilities)
                {
                    facilityInfo = new FacilityInfo
                    {
                        GlobalSiteNum = marketFacility.GlobalSiteNum,
                        LegalName = marketFacility.LegalName,
                        SiteName = marketFacility.SiteName,
                        LocationCode = marketFacility.LocationCode
                    };

                    ///get this facility drivers form allprdrivers list, allprdriver list may have market facility drivers.
                    thisFacilityDrivers = allprdrivers.Where(d => d.StoreNo == Convert.ToInt32(facilityInfo.GlobalSiteNum)).ToList();

                    thisFacilityLoads = allDBLoads.Where(l => l.GlobalSiteNumber == marketFacility.GlobalSiteNum).ToList();
                    facilityInfo.Loads = GetFacilityLoads(date, marketFacility, thisFacilityDrivers, marketSLTouches, thisFacilityLoads);

                    market.Facilities.Add(facilityInfo);
                }
            }

            return market;
        }

        private List<Load> GetFacilityLoads(DateTime date, SiteInfo site, List<PRDriver> PRDrivers, List<Touch> marketSLTouches, List<Load> lstDBLoads)
        {
            Load load;
            foreach (var driver in PRDrivers)
            {
                load = null;
                DriverSchedule schedule = new DriverSchedule
                {
                    ScheduleDate = driver.ScheduleDate,
                    DayOfWeek = driver.ScheduleDate.DayOfWeek.ToString(),
                    ScheduleStartTime = driver.ScheduleStartTime,
                    ScheduleEndTime = driver.ScheduleEndTime,
                    DayWork = ((driver.DriverStatus != (int)PREnums.UserStatus.Active) || (driver.ScheduleEndTime - driver.ScheduleStartTime).TotalMinutes <= 0) ? PREnums.DriverDayOfWork.O : (PREnums.DriverDayOfWork)Enum.Parse(typeof(PREnums.DriverDayOfWork), driver.DayWork, true)
                };
                if (!lstDBLoads.Any(x => x.DriverId == driver.DriverId))
                {
                    load = new Load(0, driver.DriverId, site.GlobalSiteNum, 0, false, false, false, date, null);
                    load.DriverName = driver.FirstName + " " + driver.LastName;
                    load.Date = date.ToShortDateString();

                    load.DriverSchedule = schedule;

                    //Get driver start time from 3GTMS and assign his default time is as that.
                    DateTime newtime = DateTime.Now;
                    DateTime.TryParse(schedule.StartTime, out newtime);
                    load.ScheduleStartDateTime = newtime;

                    lstDBLoads.Add(load);
                }
                else
                {
                    load = lstDBLoads.Where(x => x.DriverId == driver.DriverId).FirstOrDefault();

                    load.DriverName = $"{driver.FirstName} {driver.LastName}";
                    load.DriverSchedule = schedule;
                    load.Date = date.ToShortDateString();
                }
            }

            lstDBLoads.ToList().ForEach(l =>
            {
                if (l.Touches != null) l.Touches.ForEach(t =>
                {
                    var sitelinktouch = marketSLTouches.Where(st => st.OrderNumber == t.OrderNumber).FirstOrDefault();
                    if (sitelinktouch != null)
                    {
                        t.OriginAddress = (t.OriginAddress != null && t.OriginAddress.ID > 0 ? t.OriginAddress : sitelinktouch.OriginAddress);
                        t.DestAddress = (t.DestAddress != null && t.DestAddress.ID > 0 ? t.DestAddress : sitelinktouch.DestAddress);
                        t.CustomerName = (string.IsNullOrEmpty(t.CustomerName) ? sitelinktouch.CustomerName : t.CustomerName);
                        t.ScheduledDate = (t.ScheduledDate != null && t.ScheduledDate != DateTime.MinValue ? t.ScheduledDate : sitelinktouch.ScheduledDate);
                        t.ProvidedETASettings = t.ProvidedETASettings != null ? t.ProvidedETASettings : sitelinktouch.ProvidedETASettings;
                        //Some of the DE's completed with Zippy unit but not yet moved in SL - In this case, local db knows it is Zippy but not SL
                        t.IsZippyShellQuote = t.IsZippyShellQuote || sitelinktouch.IsZippyShellQuote;
                    }
                    else
                    {
                        t.ProvidedETASettings = new ETASettings();
                    }

                    SetColor(ref t, t.StartTimeZone);
                });
            });

            return lstDBLoads;
        }


        public List<Touch> GetTouchesForMarket(Entities.SiteInfo site, DateTime date, List<Touch> marketTouchesFromSL)
        {
            List<Touch> marketLoadTouches = FetchAllLoadTouchesForMarketFromSP(site, date);

            marketTouchesFromSL.ForEach(st =>
            {
                var dbtouch = marketLoadTouches.Where(t => t.OrderNumber == st.OrderNumber).FirstOrDefault();
                if (dbtouch != null)
                {
                    st.TouchId = dbtouch.TouchId;
                    st.LoadId = dbtouch.LoadId;

                    st.DriverId = dbtouch.DriverId;
                    st.DriverName = dbtouch.DriverName;
                    st.IsLocked = dbtouch.IsLocked;
                    st.TrailerId = dbtouch.TrailerId;

                    st.ScheduledDate = dbtouch.StopScheduledDateTime;

                    st.RouteSequence = dbtouch.RouteSequence;
                    st.SequenceNO = dbtouch.SequenceNO;
                }
            });


            SyncSLUpdatesWithRO(ref marketLoadTouches, marketTouchesFromSL, site, "FacilityApp-GridView");

            return marketTouchesFromSL;
        }

        public List<Touch> GetTransportationTouches(Entities.SiteInfo site, DateTime date, bool isGetLiveSLData)
        {
            return _sitelinkRepository.GetTransportationTouches(site, date, isGetLiveSLData);
        }

        public List<Touch> GetTransportationTouches(List<SiteInfo> lstSites, DateTime date, bool isGetLiveSLData)
        {
            return _sitelinkRepository.GetTransportationTouches(lstSites, date, isGetLiveSLData);
        }

        public List<Touch> GetTransportationTouches(List<SiteInfo> lstSites, DateTime startdate, DateTime enddate, bool isGetLiveSLData)
        {
            return _sitelinkRepository.GetTransportationTouches(lstSites, startdate, enddate, isGetLiveSLData);
        }

        public List<Touch> GetTransportationTouches(CriteriaSitelinkTouches criteriaSitelinkTouches, List<SiteInfo> lstSiteInfo)
        {
            return _sitelinkRepository.GetTransportationTouches(criteriaSitelinkTouches, lstSiteInfo);
        }

        public void RemoveCachedAddress(Address addr)
        {
            _sitelinkRepository.RemovedCachedAddress(addr);
        }
    }
}
