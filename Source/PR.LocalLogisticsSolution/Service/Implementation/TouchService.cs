﻿using PR.LocalLogisticsSolution.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using PR.LocalLogisticsSolution.Model;
using PR.Entities;
using PR.UtilityLibrary;
using System.Data;

namespace PR.LocalLogisticsSolution.Service.Implementation
{
    public partial class TouchService : ITouchService
    {
        private readonly IRouteRepository _routeRepository;
        private readonly ISitelinkRepository _sitelinkRepository;
        private readonly IUserService _userService;
        private readonly IDBRepository _dbRepository;

        public TouchService(IRouteRepository routeRepository, ISitelinkRepository iSitelinkRepository, IUserService userService, IDBRepository dbRepository)
        {
            _routeRepository = routeRepository;
            _sitelinkRepository = iSitelinkRepository;
            _userService = userService;
            _dbRepository = dbRepository;
        }

        public List<Stops> BreakIntoStop(Touch touch, Address FacilityAddress)
        {
            List<Stops> stops = new List<Stops>();
            Stops newStop = null;
            bool stopExists = true;

            switch (touch.TouchTypeAcronym)
            {

                case "DE":
                case "DF":
                    stopExists = true;
                    if (touch.Stops != null)
                        newStop = touch.Stops.Where(s => s.StopTypeId == Convert.ToInt32(StopType.PICKUP)).FirstOrDefault();
                    if (newStop == null)
                    {
                        newStop = new Stops(-1, touch.TouchId, StopType.PICKUP);
                        stopExists = false;
                    }

                    newStop.CustomerName = touch.CustomerName;
                    newStop.IsRequired = true;
                    newStop.TouchStatus = "Open";
                    newStop.AssignScheduledTime(touch.ScheduledDate, touch.ScheduledDate);
                    //newStop.AssignActualTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignEstimatedTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignOriginDetails(touch.OriginAddress);
                    newStop.AssignDestDetails(touch.DestAddress);// CustomerAddress);
                    if (!stopExists)
                        touch.AddStop(newStop);


                    //Create second leg for the same touch
                    stopExists = true;
                    if (touch.Stops != null)
                        newStop = touch.Stops.Where(s => s.StopTypeId == Convert.ToInt32(StopType.DROPOFF)).FirstOrDefault();
                    if (newStop == null)
                    {
                        newStop = new Stops(-1, touch.TouchId, StopType.DROPOFF);
                        stopExists = false;
                    }
                    newStop.CustomerName = touch.CustomerName;
                    newStop.IsRequired = true;
                    newStop.AssignScheduledTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignEstimatedTime(touch.ScheduledDate, touch.ScheduledDate);
                    //  newStop.AssignActualTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.TouchStatus = "Open";
                    newStop.AssignOriginDetails(touch.DestAddress);
                    newStop.AssignDestDetails(touch.OriginAddress);
                    if (!stopExists)
                        touch.AddStop(newStop);

                    break;

                case "CC":
                    stopExists = true;
                    if (touch.Stops != null)
                        newStop = touch.Stops.Where(s => s.StopTypeId == Convert.ToInt32(StopType.PICKUP)).FirstOrDefault();
                    if (newStop == null)
                    {
                        newStop = new Stops(-1, touch.TouchId, StopType.PICKUP);
                        stopExists = false;
                    }
                    //newStop = new Stops(-1, touch.TouchId, StopType.PICKUP);

                    newStop.CustomerName = touch.CustomerName;
                    newStop.IsRequired = true;
                    newStop.AssignScheduledTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignActualTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignEstimatedTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.TouchStatus = "Open";
                    newStop.AssignOriginDetails(FacilityAddress);
                    newStop.AssignDestDetails(touch.OriginAddress);// CustomerAddress);
                    if (!stopExists)
                        touch.AddStop(newStop);

                    //Create second leg for the same touch
                    stopExists = true;
                    if (touch.Stops != null)
                        newStop = touch.Stops.Where(s => s.StopTypeId == Convert.ToInt32(StopType.DROPOFF)).FirstOrDefault();
                    if (newStop == null)
                    {
                        newStop = new Stops(-1, touch.TouchId, StopType.DROPOFF);
                        stopExists = false;
                    }
                    //newStop = new Stops(-1, touch.TouchId, StopType.DROPOFF);
                    newStop.TouchStatus = "Open";
                    newStop.CustomerName = touch.CustomerName;
                    newStop.IsRequired = true;
                    newStop.AssignScheduledTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignEstimatedTime(touch.ScheduledDate, touch.ScheduledDate);
                    //  newStop.AssignActualTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignOriginDetails(touch.OriginAddress);
                    newStop.AssignDestDetails(touch.DestAddress);
                    if (!stopExists)
                        touch.AddStop(newStop);

                    //create backtofacility leg
                    stopExists = true;
                    if (touch.Stops != null)
                        newStop = touch.Stops.Where(s => s.StopTypeId == Convert.ToInt32(StopType.ToFacility)).FirstOrDefault();
                    if (newStop == null)
                    {
                        newStop = new Stops(-1, touch.TouchId, StopType.ToFacility);
                        stopExists = false;
                    }
                    //newStop = new Stops(-1, touch.TouchId, StopType.ToFacility);
                    newStop.TouchStatus = "Open";
                    newStop.CustomerName = touch.CustomerName;
                    newStop.IsRequired = true;
                    newStop.AssignScheduledTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignEstimatedTime(touch.ScheduledDate, touch.ScheduledDate);
                    //  newStop.AssignActualTime(touch.ScheduledDate, touch.ScheduledDate);

                    newStop.AssignOriginDetails(touch.DestAddress);
                    newStop.AssignDestDetails(FacilityAddress);
                    if (!stopExists)
                        touch.AddStop(newStop);
                    break;

                case "RE":
                case "RF":
                    stopExists = true;
                    if (touch.Stops != null)
                        newStop = touch.Stops.Where(s => s.StopTypeId == Convert.ToInt32(StopType.PICKUP)).FirstOrDefault();
                    if (newStop == null)
                    {
                        newStop = new Stops(-1, touch.TouchId, StopType.PICKUP);
                        stopExists = false;
                    }
                    //newStop = new Stops(-1, touch.TouchId, StopType.PICKUP);

                    newStop.CustomerName = touch.CustomerName;
                    newStop.IsRequired = true;
                    newStop.AssignScheduledTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignEstimatedTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignActualTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.TouchStatus = "Open";
                    newStop.AssignOriginDetails(touch.DestAddress);
                    newStop.AssignDestDetails(touch.OriginAddress);// CustomerAddress);
                    if (!stopExists)
                        touch.AddStop(newStop);

                    //Create second leg for the same touch
                    stopExists = true;
                    if (touch.Stops != null)
                        newStop = touch.Stops.Where(s => s.StopTypeId == Convert.ToInt32(StopType.DROPOFF)).FirstOrDefault();
                    if (newStop == null)
                    {
                        newStop = new Stops(-1, touch.TouchId, StopType.DROPOFF);
                        stopExists = false;
                    }
                    //newStop = new Stops(-1, touch.TouchId, StopType.DROPOFF);
                    newStop.IsRequired = true;
                    newStop.CustomerName = touch.CustomerName;
                    newStop.TouchStatus = "Open";
                    newStop.AssignScheduledTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignEstimatedTime(touch.ScheduledDate, touch.ScheduledDate);
                    newStop.AssignOriginDetails(touch.OriginAddress);
                    newStop.AssignDestDetails(touch.DestAddress);
                    if (!stopExists)
                        touch.AddStop(newStop);
                    break;
            }
            return stops;
        }

        private bool AddressComparison(Address baseAddress, Address compareAddress)
        {
            return IsStringsEqual(baseAddress.AddressLine1, compareAddress.AddressLine1) &&
                    IsStringsEqual(baseAddress.AddressLine2, compareAddress.AddressLine2) &&
                    IsStringsEqual(baseAddress.City, compareAddress.City) &&
                    IsStringsEqual(baseAddress.Zip, compareAddress.Zip) &&
                    IsStringsEqual(baseAddress.State, compareAddress.State);
        }

        public int AddTouchToLoad(DateTime date, Touch touch, SiteInfo site, int loadID, int driverID, int truckID)
        {
 
            int TouchId = 0;
            try
            {
                if (loadID <= 0)
                { 
                    //Load does not exists in the DB 
                    touch.LoadId = _routeRepository.CreateLoad(driverID, touch.ScheduledDate, site.GlobalSiteNum, site.SiteName, truckID);
                }
                else //Since we have retrieved touch from session, we need to assign loadEd explicitly
                    touch.LoadId = loadID;

                touch.OriginAddress.AddressType = AddressComparison(touch.OriginAddress, site.SiteAddress) ? PREnums.AddressType.Warehouse : PREnums.AddressType.Customer;
                //touch.OriginAddress.FacilityStoreNo = touch.OriginAddress.AddressType == PREnums.AddressType.Warehouse ? Convert.ToInt32(site.GlobalSiteNum) : (int?)null;
                touch.OriginAddress.FacilityStoreNo = Convert.ToInt32(site.GlobalSiteNum);

                touch.DestAddress.AddressType = AddressComparison(touch.DestAddress, site.SiteAddress) ? PREnums.AddressType.Warehouse : PREnums.AddressType.Customer;
                //touch.DestAddress.FacilityStoreNo = touch.DestAddress.AddressType == PREnums.AddressType.Warehouse ? Convert.ToInt32(site.GlobalSiteNum) : (int?)null;
                touch.DestAddress.FacilityStoreNo = Convert.ToInt32(site.GlobalSiteNum);

                touch.OriginAddress = _routeRepository.ManageAddress(touch.OriginAddress);
                touch.DestAddress = _routeRepository.ManageAddress(touch.DestAddress);
                Address facilityAddress = _routeRepository.ManageAddress(site.SiteAddress);

                //Breake touch into stops as per the touch type
                BreakIntoStop(touch, facilityAddress);

                //Save touch to DB 
                TouchId = _routeRepository.SaveTouch(touch);
                  
                if (touch.LoadId > 0)
                    UpdateLoadOptimized(touch.LoadId.Value, false);
            }
            catch (Exception ex)
            {
                //rtFlag = false;
                throw ex;
            }

            return TouchId;
        }

        public int AddBreakTouch(DateTime date, int loadId, SiteInfo site, int breakTimeInMinutes, string comment)
        {
            Touch touch = new Touch();
            touch.LoadId = loadId;
            touch.ScheduledDate = date;
            touch.WeightStnReq = false;
            touch.TouchType = "BT";// "BusyTime";
            touch.Instructions = "";

            Address facilityAddress = _routeRepository.ManageAddress(site.SiteAddress);
            touch.OriginAddress = facilityAddress;
            touch.DestAddress = facilityAddress;

            Stops newStop = new Stops(-1, -1, StopType.BUSYTIME);

            BusyTime bt = new BusyTime();
            bt.BreakTime = new TimeSpan(0, breakTimeInMinutes, 0);
            bt.Comment = comment;
            newStop.BusyTime = bt;

            //newStop.CustomerName = "";
            newStop.IsRequired = true;

            newStop.AssignScheduledTime(touch.ScheduledDate, touch.ScheduledDate);
            newStop.AssignEstimatedTime(touch.ScheduledDate, touch.ScheduledDate);

            newStop.AssignOriginDetails(touch.OriginAddress);
            newStop.AssignDestDetails(touch.DestAddress);// CustomerAddress);
            touch.AddStop(newStop);

            int TouchId = _routeRepository.SaveTouch(touch);

            UpdateLoadOptimized(loadId, false);

            return TouchId;
        }

        public bool RemoveBreakTouchAndStop(int stopId, string activityBy)
        {
            bool rtVal = _routeRepository.RemoveBusyTouchAndStops(stopId, activityBy);

            return rtVal;
        }

        public bool RemoveTouchFromLoad(int touchId, SiteInfo site, string activityBy)
        {
            bool rtVal = false;
            try
            {
                if (touchId > 0)
                {
                    var touch = _routeRepository.FetchTouchByTouchId(touchId);
                    int? loadId = touch.LoadId;

                    //If this touch is in group then we have ungroup this group 
                    if (touch.GroupGUID != null && loadId > 0)
                    {
                        UnThisGroupTouches(loadId.Value, touch.GroupGUID.Value, site.SiteAddress);
                    }

                    _routeRepository.RemoveTouch(touchId);

                    //Update Salesforce touch for Touch Assignment. TG-729
                    AddRemoveTouchtoDriverESB(DateTime.Now, touch, 0, activityBy);

                    if (loadId.HasValue && loadId > 0)
                        UpdateLoadOptimized(loadId.Value, false);

                    rtVal = true;
                }
            }
            catch (Exception ex)
            {
                rtVal = true;
                throw ex;
            }

            return rtVal;
        }

        public bool CheckTouchCanRemoveFromLoad(int touchId, out string reasons)
        {
            bool canEdit = _routeRepository.CheckTouchCanRemoveFromLoad(touchId, out reasons);

            if (canEdit == false && string.IsNullOrEmpty(reasons) == false)
            {
                reasons = "This touch cannot remove from this load. Because this touch is in use and its status is: " + reasons;
            }

            return canEdit;
        }

        public bool MoveTouchToAnotherLoad(int touchID, int driverID, ref int loadID, DateTime date, SiteInfo site, int truckID)
        {
            //call repo method to udpate the touch to another load
            //check if data exisits in  optimized stop info table and delete it???
            bool rtFlag = true;
            try
            {
                if (loadID <= 0) //New Load, need to create a load and then assign this touch to that load
                { 
                    loadID = _routeRepository.CreateLoad(driverID, date, site.GlobalSiteNum, site.SiteName, truckID);
                }

                //If this touch is in any group then we have to ungroup it and then go for moving this touch to new load
                var touch = _routeRepository.FetchTouchByTouchId(touchID);
                if (touch.GroupGUID != null)
                {
                    UnThisGroupTouches(touch.LoadId.Value, touch.GroupGUID.Value, site.SiteAddress);
                }

                _routeRepository.MoveTouchToAnotherLoad(touchID, loadID);

                //Update both loads to un optimized mode 
                if (touch != null && touch.LoadId > 0)
                    UpdateLoadOptimized(touch.LoadId.Value, false);

                UpdateLoadOptimized(loadID, false);
            }
            catch (Exception ex)
            {
                rtFlag = false;
                throw ex;
            }

            return rtFlag;
        } 

        public Load FetchLoadByLoadId(int loadId)
        {
            Load load = _routeRepository.FetchLoadByLoadId(loadId);

            DriverSchedule schedule = _userService.GetDriverSchedule(load.CreatedDateTime, load.DriverId);
            load.DriverSchedule = schedule;
            //Get driver start time from 3GTMS and assign his default time is as that.
            if (load.ScheduleStartDateTime.HasValue == false)
            //if (load.IsDriverStarted == false && load.IsLocked == false)
            {
                DateTime newtime = DateTime.Now;
                DateTime.TryParse(schedule.StartTime, out newtime);
                load.ScheduleStartDateTime = newtime;
            }

            if (load != null && load.Touches != null)
            {
                load.Touches.ForEach(t =>
                {
                    if (t.IsGrouped == true)
                        UpdateTouchOrgAndDestAddressFromStopsForGroupedTouches(t);

                    SetColor(ref t, t.StartTimeZone); 

                    if (t.Stops != null) t.Stops.ForEach(s => SetColor(ref s, t.OrderNumber, t.StartTimeZone));
                });
 
            }

            return load;
        }

        private Touch UpdateTouchOrgAndDestAddressFromStopsForGroupedTouches(Touch touch)
        {
            Stops pickupStop = touch.PickupStop;// touch.Stops.Where(s => s.StopType.ToLower() == StopType.PICKUP.ToString().ToLower()).FirstOrDefault();
            Stops dropoffStop = touch.DropoffStop;// touch.Stops.Where(s => s.StopType.ToLower() == StopType.DROPOFF.ToString().ToLower()).FirstOrDefault();

            if (pickupStop != null && dropoffStop != null)
            {
                touch.CustomerName = pickupStop.CustomerName;

                switch (touch.TouchTypeAcronym)
                {
                    case "DE":
                    case "DF":
                        touch.OriginAddress = pickupStop.OriginAddress;
                        touch.DestAddress = pickupStop.DestinationAddress;
                        break;

                    case "CC":
                        touch.OriginAddress = dropoffStop.OriginAddress;
                        touch.DestAddress = dropoffStop.DestinationAddress;
                        break;

                    case "RE":
                    case "RF": 
                        touch.OriginAddress = pickupStop.OriginAddress;
                        touch.DestAddress = pickupStop.DestinationAddress;
                        break;
                }
            }
            return touch;
        }

        public bool DeleteLoad(int loadId)
        {
            bool rtFlag = true;

            try
            {
                rtFlag = _routeRepository.DeleteLoad(loadId);
            }
            catch (Exception ex)
            {
                rtFlag = false;
            }

            return rtFlag;
        }

        public Touch FetchTouchByTouchId(int touchId)
        {
            Touch touch = _routeRepository.FetchTouchByTouchId(touchId);

            SetColor(ref touch, touch.StartTimeZone);

            return touch;
        }

        public Touch FetchTouchByTouchId(int touchId, SiteInfo marketSite)
        {
            Touch touch = _routeRepository.FetchTouchByTouchId(touchId);

            SetColor(ref touch, touch.StartTimeZone);

            return touch;
        }

        #region Commented due to unreachable condition, added by Sohan

        //public List<Touch> FetchAllUnassignedTouches(Entities.SiteInfo site, DateTime date)
        //{
        //    List<Touch> allTouches = new List<Touch>();

        //    List<Touch> sitelinkTouches = _sitelinkRepository.GetUnassignedTouches(site, date);

        //    List<Touch> touchesInLoad = _routeRepository.FetchTouches(date, site.GlobalSiteNum);

        //    SyncSLUpdatesWithRO(ref touchesInLoad, sitelinkTouches, site, "FacilityApp-MapView");

        //    sitelinkTouches.ForEach(st =>
        //    {
        //        var dbtouch = touchesInLoad.Where(t => t.OrderNumber == st.OrderNumber).FirstOrDefault();
        //        if (dbtouch != null)
        //        {
        //            st.TouchId = dbtouch.TouchId;
        //            st.LoadId = dbtouch.LoadId;

        //            st.ScheduledDate = dbtouch.StopScheduledDateTime;

        //            st.RouteSequence = dbtouch.RouteSequence;
        //            st.SequenceNO = dbtouch.SequenceNO;
        //        }
        //    });

        //    allTouches.AddRange(sitelinkTouches);

        //    return allTouches;
        //}

        #endregion

        public int CreateLoad(int driverId, DateTime date, SiteInfo site, int truckID)
        {
            return _routeRepository.CreateLoad(driverId, date, site.GlobalSiteNum, site.SiteName, truckID);
        }

        public List<Trucks> GetTruckStatus(string storeNumber)
        {
            return _dbRepository.GetTruckStatus(storeNumber);
        }

        public List<Trailers> GetTrailers(string storeNumber)
        {
            return _routeRepository.GetTrailers(storeNumber);
        }

        public bool UpdateLoadTruck(int loadId, int truckId)
        {
            bool rtFlag = false;
            try
            {
                _routeRepository.UpdateLoadTruck(loadId, truckId);

                rtFlag = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtFlag;
        }

        public bool UpdateLoadOptimized(int loadId, bool isOptimized)
        {
            bool rtFlag = false;
            try
            {
                _routeRepository.UpdateLoadOptimized(loadId, isOptimized);

                rtFlag = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtFlag;
        }

        public bool UpdateLoadLock(int loadId, bool isLock)
        {
            bool rtFlag = false;
            try
            {
                _routeRepository.UpdateLoadLock(loadId, isLock);

                rtFlag = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtFlag;
        }

        private void SetColor(ref Touch touch, string startTime)
        {
            switch (startTime.ToUpper())
            {
                case "ANYTIME":
                    touch.Color = "Green";
                    break;
                case "AM":
                    touch.Color = "Orange";
                    break;
                case "PM":
                    touch.Color = "#0069a1"; //Blue
                    break;
                default:
                    break;
            }
        }

        private void SetColor(ref Stops oStop, string orderNumber, string startTime)
        {
            switch (startTime)
            {
                case "Anytime":
                    oStop.Color = "#0dbbff"; //Magenta
                    break;
                case "AM":
                    oStop.Color = "#183b79"; //Dark Blue
                    break;
                case "PM":
                    oStop.Color = "#2e6cac"; // Light Blue
                    break;
                default:
                    break;
            }
        }

        public List<TouchHandlingTime> FetchTouchHandlingTimes(string StoreNo)
        {
            return _routeRepository.FetchTouchHandlingTimes(StoreNo).ToList();
        }

        public TouchStopHandlingTime FetchTouchStopHandlingTimes(string StoreNo)
        {
            List<TouchHandlingTime> oTouchHandlingTime = _routeRepository.FetchTouchHandlingTimes(StoreNo).ToList();

            TouchStopHandlingTime oTouchStopHandlingTime = new TouchStopHandlingTime(oTouchHandlingTime);

            return oTouchStopHandlingTime;
        }

        public List<PR.Entities.PRDriver> GetAllDriversByFacility(string faciltyNumber, DateTime date)
        {
            return _dbRepository.GetAllDriversByFacility(faciltyNumber, date);
        }

        public bool UnGroupTouches(int loadid, Guid guid, Address FacilityAddress)
        {
            bool rtVal = true;

            UnThisGroupTouches(loadid, guid, FacilityAddress);

            UpdateLoadOptimized(loadid, false);

            return rtVal;
        }

        private bool UnThisGroupTouches(int loadid, Guid guid, Address FacilityAddress)
        {
            List<Touch> touches = _routeRepository.FetchTouchByLoadAndGuidId(loadid, guid);
            Address facilityAddress = _routeRepository.ManageAddress(FacilityAddress);

            for (int index = 0; index < touches.Count(); index++)
            {
                touches[index].IsGrouped = false;
                touches[index].GroupGUID = null;

                //Need to implement this method to handle ungrouping touches
                UpdateStopsBasedonTouchUnGrouping(touches[index], facilityAddress);
            }

            return _routeRepository.UpdateUnGroupingStatus(touches);
        }

        public Touch UpdateStopsBasedonTouchUnGrouping(Touch touch, Address FacilityAddress)
        {
            switch (touch.TouchTypeAcronym)
            {

                case "DE":
                case "DF":
                    if (touch.PickupStop != null)
                    {
                        touch.PickupStop.IsRequired = true;
                        touch.PickupStop.AssignOriginDetails(touch.OriginAddress);
                        touch.PickupStop.AssignDestDetails(touch.DestAddress);// CustomerAddress);
                    }

                    if (touch.DropoffStop != null)
                    {
                        touch.DropoffStop.IsRequired = true;
                        touch.DropoffStop.AssignOriginDetails(touch.DestAddress);
                        touch.DropoffStop.AssignDestDetails(touch.OriginAddress);
                    }

                    break;

                case "CC":
                    if (touch.PickupStop != null)
                    {
                        touch.PickupStop.IsRequired = true;
                        touch.PickupStop.AssignOriginDetails(FacilityAddress);
                        touch.PickupStop.AssignDestDetails(touch.OriginAddress);// CustomerAddress);
                    }
                    if (touch.DropoffStop != null)
                    {
                        touch.DropoffStop.IsRequired = true;
                        touch.DropoffStop.AssignOriginDetails(touch.OriginAddress);
                        touch.DropoffStop.AssignDestDetails(touch.DestAddress);
                    }
                    if (touch.ToFacilityStop != null)
                    {
                        touch.ToFacilityStop.IsRequired = true;
                        touch.ToFacilityStop.AssignOriginDetails(touch.DestAddress);
                        touch.ToFacilityStop.AssignDestDetails(FacilityAddress);
                    }

                    break;

                case "RE":
                case "RF":
                    if (touch.PickupStop != null)
                    {
                        touch.PickupStop.IsRequired = true;
                        touch.PickupStop.AssignOriginDetails(touch.DestAddress);
                        touch.PickupStop.AssignDestDetails(touch.OriginAddress);
                    }

                    if (touch.DropoffStop != null)
                    {
                        touch.DropoffStop.IsRequired = true;
                        touch.DropoffStop.AssignOriginDetails(touch.OriginAddress);
                        touch.DropoffStop.AssignDestDetails(touch.DestAddress);
                    }

                    break;
            }
            return touch;
        }

        public bool GroupTouches(int[] TouchIds, int[] AllTouchIds)
        {
            List<Touch> touches = new List<Touch>();
            Guid guid = Guid.NewGuid();
            int? loadId = null;
            Dictionary<int, int> touchesAndOrder = (AllTouchIds != null ? AllTouchIds.ToList().Select((o, i) => new { o, i }).ToDictionary(a => a.o, a => a.i + 1) : new Dictionary<int, int>());

            foreach (var touchId in TouchIds)
            {
                var touch = _routeRepository.FetchTouchByTouchId(touchId);
                touches.Add(touch);
                loadId = touch.LoadId;

            }

            for (int i = 0; i <= TouchIds.Length - 1; i++)
            {
                if ((i + 1) <= (TouchIds.Length - 1))
                {
                    var sequenceString = touches[i].TouchTypeAcronym + touches[i + 1].TouchTypeAcronym;

                    if (!ValidateTouches(sequenceString))
                    {
                        throw new Exception("TouchTypes " + touches[i].TouchTypeAcronym + " " + touches[i + 1].TouchTypeAcronym + " cannot be combined");
                    }
                    else if (ValidateContainerRules(touches[i], touches[i + 1]))
                    {
                        touches[i].TouchGroupId = touches[i + 1].TouchId;
                        touches[i].IsGrouped = true;
                        touches[i + 1].IsGrouped = true;
                        touches[i].GroupGUID = guid;
                        touches[i + 1].GroupGUID = guid;
                        UpdateStopsBasedonTouchGrouping(touches[i], touches[i + 1]);
                    }
                }
            }

            var rtval = _routeRepository.UpdateGroupingStatus(touches, touchesAndOrder);

            if (loadId.HasValue && loadId.Value > 0)
                UpdateLoadOptimized(loadId.Value, false);

            return rtval;
        }

        private bool ValidateContainerRules(Touch touch1, Touch touch2)
        {
            switch (touch1.TouchTypeAcronym + touch2.TouchTypeAcronym)
            {
                case "REDE":
                    if ((int.Parse(touch1.Unitsize) < int.Parse(touch2.Unitsize)) && (touch1.Unitsize != touch2.Unitsize))
                        throw new Exception("Cannot group RE DE due to mismatch in container size: " + touch1.OrderNumberDisp + " (" + touch1.Unitsize + ") to " + touch2.OrderNumberDisp + " (" + touch2.Unitsize + ")");

                    if (!string.IsNullOrEmpty(touch2.StarsID))
                    {
                        int[] unitNumberInitialValues = new int[] { 1, 2, 3 };
                        if (unitNumberInitialValues.Contains(touch1.UnitNumber[0]))
                        {
                            throw new Exception("You have grouped a RE unit with a rollup door to a DE for an LDM customer. LDM moves should use containers with barn doors.");
                        }
                    }
                    break;

            }
            return true;
        }

        private void UpdateStopsBasedonTouchGrouping(Touch touch1, Touch touch2)
        {
            switch (touch1.TouchTypeAcronym + touch2.TouchTypeAcronym)
            {
                case "REDE":
                case "DERF":
                case "DECC":
                case "DERE":
                case "DFCC":
                case "DFRE":
                case "DFRF":
                    touch1.DropoffStop.IsRequired = false;
                    touch2.PickupStop.OriginAddress = touch1.DropoffStop.OriginAddress;
                    break;


                case "CCCC":
                case "CCRE":
                case "CCRF":
                    touch1.ToFacilityStop.IsRequired = false;
                    touch2.PickupStop.OriginAddress = touch1.DropoffStop.DestinationAddress;

                    break;
                default:
                    break;
            }
        }

        private bool ValidateTouches(string touchGrpSequence)
        {

            string[] validSequences = new string[] { "REDE", "DERE", "DECC", "DERF", "DFRE", "DFCC", "DFRF", "CCCC", "CCRE", "CCRF" };
            return validSequences.Contains(touchGrpSequence);
        }

        private bool ValidateTouches(List<Touch> touches)
        {
            string touchGrpSequence = "";
            string[] validSequences = new string[] { "REDE", "DERE", "DECC", "DERF", "DFRE", "DFCC", "DFRF", "CCCC", "CCRE", "CCRF" };
            foreach (var item in touches)
            {
                touchGrpSequence += item.TouchTypeAcronym;
            }

            return validSequences.Contains(touchGrpSequence);
        }

        public bool SaveDriverStartTimeNTrailerRequired(int loadId, string startTime, bool isTrailerRequired)
        {
            return _routeRepository.SaveDriverStartTimeNTrailerRequired(loadId, startTime, isTrailerRequired);
        }

        private void UpdateGroupTocuhesAddressUpdatedBeforeSave(int loadId, List<Touch> seqTouches, Address FacilityAddress)
        {
            Load load = _routeRepository.FetchLoadByLoadId(loadId);
            int[] allTouchIds = seqTouches.OrderBy(o => o.RouteSequence).Select(s => s.TouchId).ToArray();
            List<Touch> addressChangedTouches = load.Touches.Where(t => t.SLUpdatedStatusType == SLUpdateType.AddressAndInfoUpdated || t.SLUpdatedStatusType == SLUpdateType.OnlyAddressUpdated).ToList();
            List<Guid> groupIds = addressChangedTouches.Where(t => t.GroupGUID != null).Select(s => s.GroupGUID.Value).Distinct().ToList();

            if (groupIds != null && groupIds.Count > 0)
            {
                foreach (var groupId in groupIds)
                {
                    int[] touchIds = load.Touches.Where(t => t.GroupGUID == groupId).Select(ts => ts.TouchId).ToArray();

                    UnThisGroupTouches(loadId, groupId, FacilityAddress);

                    GroupTouches(touchIds, allTouchIds);
                }
            }
        }

        public bool SaveSortedTouchInfo(int loadId, List<Touch> seqTouches, string[] touchTrailerMapIds, Address facilityAddress)
        {
            bool rtFlag = false;
            try
            {
                Dictionary<int, int> dcTouchTrailerMap = new Dictionary<int, int>();
                if (touchTrailerMapIds != null && touchTrailerMapIds.Length > 0)
                {
                    foreach (var touchTrailerId in touchTrailerMapIds)
                    {
                        string[] temp = touchTrailerId.Split(',');
                        if (temp.Length == 2)
                        {
                            dcTouchTrailerMap.Add(Convert.ToInt32(temp[0]), Convert.ToInt32(temp[1]));
                        }
                    }
                }
                int seq = 0;
                foreach (Touch touch in seqTouches)
                {
                    seq++;
                    touch.TrailerId = (dcTouchTrailerMap != null && dcTouchTrailerMap.ContainsKey(touch.TouchId) ? dcTouchTrailerMap[touch.TouchId] : 0);
                    touch.RouteSequence = seq;
                }

                facilityAddress = _routeRepository.ManageAddress(facilityAddress);
                UpdateGroupTocuhesAddressUpdatedBeforeSave(loadId, seqTouches, facilityAddress);

                _routeRepository.SaveSortedTouchInfo(seqTouches, facilityAddress);

                UpdateLoadOptimized(loadId, false);

                rtFlag = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtFlag;
        }

        public bool SaveOptimizedStops(List<Stops> seqStops, int loadId)
        {
            bool rtVal = true;

            _routeRepository.SaveOptimizedStopSortOrder(seqStops, loadId);

            return rtVal;
        }

        public List<Entities.Address> FetchAllAddressForThisLoad(int loadId)
        {
            return _routeRepository.FetchAllAddressForThisLoad(loadId);
        }

        private bool ValidateOptimizedStops(List<int> StopIds, int loadId)
        {
            var load = _routeRepository.FetchLoadByLoadId(loadId);
            Dictionary<int, List<int>> dcTouchStops = new Dictionary<int, List<int>>();

            if (load != null && StopIds != null)
            {
                List<Stops> lstStops = new List<Stops>();

                foreach (var stopid in StopIds)
                {
                    var stop = load.GetThisLoadOptimizedStops.Where(s => s.TouchStopId == stopid).FirstOrDefault();
                    lstStops.Add(stop);
                }
            }

            return true;

        }

        public List<Load> GetAllLiteLoadByStoreNumAndDate(DateTime date, string storeNumber)
        {
            return _routeRepository.GetAllLiteLoadByStoreNumAndDate(date, storeNumber).ToList();
        }

        public List<Load> GetAllLiteLoadByMarketAndDate(DateTime date, SiteInfo currentSite)
        {
            //need to prepare the list of loads
            return _routeRepository.FetchLoadsFromSP(date, currentSite.GlobalSiteNum);
        }

        public bool TruckScheduleValidation(int loadId, DateTime ScheduleStartTime)
        {
            var result = true;
            Load loadInfo = _routeRepository.FetchLoadByLoadId(loadId);
            List<Load> filteredList = _routeRepository.GetAllLiteLoadByStoreNumAndDate(loadInfo.Touches[0].ScheduledDate, loadInfo.GlobalSiteNumber).Where(x => x.TruckID == loadInfo.TruckID && x.LoadId != loadId && x.IsLocked == true).ToList();
            if (filteredList != null && filteredList.Count > 0)
            {
                foreach (var item in filteredList)
                {
                    if (ScheduleStartTime.TimeOfDay >= item.ScheduleEndDateTime.Value.TimeOfDay)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                        var driverlist = _dbRepository.GetAllDriversByFacility(loadInfo.GlobalSiteNumber, ScheduleStartTime).ToList();
                        var name = (driverlist != null && driverlist.Count > 0) ? driverlist.Where(x => x.DriverId == item.DriverId).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault() : "";

                        throw new Exception("This truck is already scheduled for " + name + " from " + Convert.ToDateTime(item.ScheduleStartDateTime).ToShortTimeString() + " to  " + item.ScheduleEndDateTime.Value.ToShortTimeString() + ". Click CANCEL to adjust your route start time or select another truck or Click OK to continue.");
                    }
                }
            }
            return result;
        }

        public void UpdateAddressByID(Address address)
        {
            _routeRepository.UpdateAddressByID(address);
        }

        public void UpdateLatLongForIntesection(Entities.Address address)
        {
            _routeRepository.UpdateLatLongForIntesection(address);
        }

        public Address ManageAddress(Address address)
        {
            return _routeRepository.ManageAddress(address);
        }

        public ScheduleCalendar GetCapacity(DateTime StartDate, DateTime EndDate, string LocationCode)
        {
            return _sitelinkRepository.GetCapacity(StartDate, EndDate, LocationCode);
        }

        //Method for just testing purpose for SL touches.
        public ScheduleCalendar GetFacilityCapacity(DateTime StartDate, DateTime endDate, string LocationCode)
        {
            return _sitelinkRepository.GetFacilityCapacity(StartDate, endDate, LocationCode);
        }

        #region SL Sync job
         
        public List<SiteInfo> GetFaciltyInformation()
        {
            return _sitelinkRepository.GetFaciltyInformation();
        }

        public List<SiteInfo> GetFaciltyInformationWithMarkets()
        {
            return _sitelinkRepository.GetFaciltyInformationWithMarkets();
        }

        ////SEFRP-TODO-RMVNICD
        //public List<Model.Load> FetchLoads(DateTime date)
        //{
        //    return _routeRepository.GetAllLoadsByDate(date);
        //}

        ////SEFRP-TODO-RMVNICD 
        //public List<Touch> GetUnassignedTouches(SiteInfo thisSiteInfo, DateTime date)
        //{
        //    return _sitelinkRepository.GetUnassignedTouches(thisSiteInfo, date);
        //}

        ////SEFRP-TODO-RMVNICD 
        //public List<Touch> FetchSiteLinkTouches(DateTime date, Entities.SiteInfo site)
        //{
        //    return _sitelinkRepository.GetUnassignedTouches(site, date);
        //}


        ////SEFRP-TODO-RMVNICD
        //public List<Model.Load> FetchLoads(DateTime date, SiteInfo site, out List<Touch> unAssignedTouches)
        //{
        //    List<PRDriver> drivers = _userService.GetAllDriversByFacility(site.GlobalSiteNum, date, site.MarketId);
        //    List<Touch> sitelinkTouches = _sitelinkRepository.GetUnassignedTouches(site, date);
        //    List<Load> lstLoads = _routeRepository.GetAllLoadsByDate(date, site.GlobalSiteNum);
        //    Load load = null;

        //    foreach (var driver in drivers)
        //    {
        //        DriverSchedule schedule = _userService.GetDriverSchedule(date, driver.DriverId);
        //        if (schedule != null && schedule.Hours > 0)
        //        {
        //            load = null;
        //            if (!lstLoads.Any(x => x.DriverId == driver.DriverId))
        //            {
        //                load = new Load(0, driver.DriverId, site.GlobalSiteNum, 0, false, false, false, date, null);//DriverNumber -TODO need to check
        //                load.DriverName = driver.FirstName + " " + driver.LastName;
        //                //load.FacCode = site.FacCode;
        //                load.Date = date.ToShortDateString();

        //                load.DriverSchedule = schedule;
        //                //Get driver start time from 3GTMS and assign his default time is as that.
        //                DateTime newtime = DateTime.Now;
        //                DateTime.TryParse(schedule.StartTime, out newtime);
        //                load.ScheduleStartDateTime = newtime;

        //                // _routeRepository.CreateLoad(driver.DriverId, date, facilityId);
        //                lstLoads.Add(load);
        //            }
        //            else
        //            {
        //                load = lstLoads.Where(x => x.DriverId == driver.DriverId).FirstOrDefault();

        //                load.DriverName = driver.FirstName + " " + driver.LastName;
        //                //load.FacCode = site.FacCode;
        //                load.DriverSchedule = schedule;
        //                load.Date = date.ToShortDateString();
        //            }
        //        }
        //    }

        //    lstLoads.ToList().ForEach(l =>
        //    {
        //        if (l.Touches != null) l.Touches.ForEach(t =>
        //        {
        //            var sitelinktouch = sitelinkTouches.Where(st => st.OrderNumber == t.OrderNumber).FirstOrDefault();
        //            if (sitelinktouch != null)
        //            {
        //                t.OriginAddress = (t.OriginAddress != null && t.OriginAddress.ID > 0 ? t.OriginAddress : sitelinktouch.OriginAddress);
        //                t.DestAddress = (t.DestAddress != null && t.DestAddress.ID > 0 ? t.DestAddress : sitelinktouch.DestAddress);
        //                t.CustomerName = (string.IsNullOrEmpty(t.CustomerName) ? sitelinktouch.CustomerName : t.CustomerName);
        //                t.ScheduledDate = (t.ScheduledDate != null && t.ScheduledDate != DateTime.MinValue ? t.ScheduledDate : sitelinktouch.ScheduledDate);
        //            }
        //            //SetColor(ref t, t.StartTime);
        //            SetColor(ref t, t.StartTimeZone);
        //            //if (t.Stops != null) t.Stops.ForEach(s => SetColor(ref s, t.OrderNumber, t.StartTime));
        //        });
        //    });

        //    //if (unAssignedTouches != null)
        //    {
        //        List<string> orderNumber = lstLoads.SelectMany(l => (l.Touches ?? new List<Touch>())).Select(s => s.OrderNumber).ToList<string>();
        //        orderNumber = orderNumber ?? new List<string>();

        //        unAssignedTouches = (from unassgined in sitelinkTouches
        //                             where !orderNumber.Contains(unassgined.OrderNumber)
        //                             select unassgined).ToList();
        //    }
        //    return lstLoads;
        //}

        public void TouchCancledAtSL(Touch touch)
        {
            _routeRepository.TouchCancledAtSL(touch);
        }

        public void TouchUpdateFromSyncJob(Touch lTouch, Touch syncTouch)
        {
            _routeRepository.TouchUpdateFromSyncJob(lTouch, syncTouch);
        }

        private void SyncSLUpdatesWithRO(ref List<Touch> dbTouches, List<Touch> slTouches, SiteInfo site, string actionFrom)
        {
            List<int> updatedTouchIds = SyncSLUpdatesWithRO(dbTouches, slTouches, site, actionFrom);

            if (updatedTouchIds != null && updatedTouchIds.Count > 0)
            {
                foreach (var touchid in updatedTouchIds)
                {
                    Touch updatedTouch = FetchTouchByTouchId(touchid);
                    dbTouches.RemoveAll(t => t.TouchId == touchid);
                    dbTouches.Add(updatedTouch);
                }
            }
        }

        public List<int> SyncSLUpdatesWithRO(List<Touch> dbTouches, List<Touch> slTouches, SiteInfo site, string actionFrom)
        {
            List<int> updatedTouchIds = new List<int>();

            foreach (var lTouch in dbTouches.Where(t => t.Status.ToLower() != "completed"))
            {
                try
                {

                    var slTouch = slTouches.Where(st => st.OrderNumber == lTouch.OrderNumber).FirstOrDefault();

                    //If Salesforce touch is null, this touch should be reschedured or cancled
                    if (slTouch == null)
                    {
                        AddEvent(actionFrom, Convert.ToInt32(site.GlobalSiteNum), lTouch.Qorid, "Touch has been rescheduled or cancled from this date", "SyncSLUpdatesWithRO", null, DateTime.Now, Convert.ToString(lTouch.TouchId));
                        // do the logic to cancle touch
                        TouchCancledAtSL(lTouch);
                        updatedTouchIds.Add(lTouch.TouchId);
                    }
                    else
                    {
                        // check if touch addresses are same or not
                        Touch syncTouch = CheckTouchUpdates(lTouch, slTouch, site);

                        if (syncTouch.SLUpdatedStatusType != SLUpdateType.Nochanges && syncTouch.SLUpdatedStatusType != SLUpdateType.Sync)
                        {
                            AddEvent(actionFrom, Convert.ToInt32(site.GlobalSiteNum), lTouch.Qorid, "Touch has been updated in SL. Updated status is:" + syncTouch.SLUpdatedStatusType, "SyncSLUpdatesWithRO", null, DateTime.Now, Convert.ToString(lTouch.TouchId));
                            TouchUpdateFromSyncJob(lTouch, syncTouch);
                            updatedTouchIds.Add(lTouch.TouchId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    AddStackTrace(0, "Error", ex.Message, "Processing touch updates in SyncSLUpdatesWithRO", DateTime.Now, ex.StackTrace);
                }
            }

            return updatedTouchIds;
        }

        private Touch CheckTouchUpdates(Touch rdTouch, Touch slTouch, SiteInfo siteInfo)
        {
            Touch syncTouch = new Touch();

            syncTouch.SLUpdatedStatusType = SLUpdateType.Sync;

            bool origAddDiff = AddressComparison(rdTouch.OriginAddress, slTouch.OriginAddress);
            bool destAddDiff = AddressComparison(rdTouch.DestAddress, slTouch.DestAddress);

            if (origAddDiff == false)
            {
                slTouch.OriginAddress.AddressType = AddressComparison(slTouch.OriginAddress, siteInfo.SiteAddress) ? PR.UtilityLibrary.PREnums.AddressType.Warehouse : PR.UtilityLibrary.PREnums.AddressType.Customer;
                slTouch.OriginAddress.FacilityStoreNo = Convert.ToInt32(siteInfo.GlobalSiteNum);

                syncTouch.OriginAddress = ManageAddress(slTouch.OriginAddress);
            }

            if (destAddDiff == false)
            {
                slTouch.DestAddress.AddressType = AddressComparison(slTouch.DestAddress, siteInfo.SiteAddress) ? PR.UtilityLibrary.PREnums.AddressType.Warehouse : PR.UtilityLibrary.PREnums.AddressType.Customer;
                slTouch.DestAddress.FacilityStoreNo = Convert.ToInt32(siteInfo.GlobalSiteNum);

                syncTouch.DestAddress = ManageAddress(slTouch.DestAddress);
            }

            bool genInfoChange = false;
            if (IsStrNotEqual(rdTouch.CustomerName, slTouch.CustomerName))
            {
                syncTouch.CustomerName = slTouch.CustomerName;
                genInfoChange = true;
            }

            if (rdTouch.WeightStnReq != slTouch.WeightStnReq)
            {
                syncTouch.WeightStnReq = slTouch.WeightStnReq;
                genInfoChange = true;
            }

            if (IsStrNotEqual(rdTouch.StartTime, slTouch.StartTime))
            {
                syncTouch.StartTime = slTouch.StartTime;
                genInfoChange = true;
            }

            if (IsStrNotEqual(rdTouch.PhoneNumber, slTouch.PhoneNumber))
            {
                syncTouch.PhoneNumber = slTouch.PhoneNumber;
                genInfoChange = true;
            }

            if (IsStrNotEqual(rdTouch.Instructions, slTouch.Instructions))
            {
                syncTouch.Instructions = slTouch.Instructions;
                genInfoChange = true;
            }

            if (rdTouch.Status == "Open" && slTouch.Status == "Completed")
            {
                syncTouch.Status = slTouch.Status;
                genInfoChange = true;
            }

            if (IsStrNotEqual(rdTouch.DoorPOS, slTouch.DoorPOS))
            {
                syncTouch.DoorToFront = slTouch.DoorToFront;
                syncTouch.DoorToRear = slTouch.DoorToRear;
                genInfoChange = true;
                syncTouch.doorposSLupdated = true;
            }

            string[] defaultUnitNumber = { "000016", "000012", "000008" };
            if (!string.IsNullOrEmpty(slTouch.UnitNumber) && rdTouch.UnitNumber != slTouch.UnitNumber && !defaultUnitNumber.Contains(slTouch.UnitNumber))
            {
                syncTouch.UnitNumber = slTouch.UnitNumber;
                genInfoChange = true;
            }

            if (genInfoChange == true && (origAddDiff == false || destAddDiff == false))
            {
                syncTouch.SLUpdatedStatusType = SLUpdateType.AddressAndInfoUpdated;
            }
            else if (origAddDiff == false || destAddDiff == false)
            {
                if (rdTouch.SLUpdatedStatusType == SLUpdateType.AddressAndInfoUpdated || rdTouch.SLUpdatedStatusType == SLUpdateType.OnlyInfoUpdated)
                {
                    syncTouch.SLUpdatedStatusType = SLUpdateType.AddressAndInfoUpdated;
                }
                else
                    syncTouch.SLUpdatedStatusType = SLUpdateType.OnlyAddressUpdated;
            }
            else if (genInfoChange == true)
            {
                if (rdTouch.SLUpdatedStatusType == SLUpdateType.AddressAndInfoUpdated || rdTouch.SLUpdatedStatusType == SLUpdateType.OnlyAddressUpdated)
                {
                    syncTouch.SLUpdatedStatusType = SLUpdateType.AddressAndInfoUpdated;
                }
                else
                    syncTouch.SLUpdatedStatusType = SLUpdateType.OnlyInfoUpdated;
            }

            return syncTouch;
        }

        private bool IsStrNotEqual(string toCompare, string comparer)
        {
            return !IsStringsEqual(toCompare, comparer);
        }

        private bool IsStringsEqual(string toCompare, string comparer)
        {
            toCompare = string.IsNullOrEmpty(toCompare) ? "" : toCompare.Trim().ToLower();
            comparer = string.IsNullOrEmpty(comparer) ? "" : comparer.Trim().ToLower();

            return string.Equals(toCompare, comparer);
        }

        #endregion

        #region Event Log

        private void AddEvent(string ApplicationName, int? StoreNo, int? QORId, string Description, string Activity, string IPAddress, DateTime CreateDate, string RefID)
        {
            EventLogEntity evnt = new EventLogEntity
            {
                ApplicationName = ApplicationName,
                StoreNo = StoreNo,
                QORID = QORId,
                Description = Description,
                Activity = Activity,
                IPAddress = IPAddress,
                CreateDate = CreateDate,
                RefID = RefID
            };

            AddEvent(evnt);
        }

        public void AddStackTrace(int UserId, string MessageType, string MessageBody, string MessageArea, DateTime CreateDate, string CallStack)
        {
            SystemMessagesEntity evnt = new SystemMessagesEntity
            {
                UserId = UserId,
                MessageType = MessageType,
                MessageBody = MessageBody,
                MessageArea = MessageArea,
                CreateDate = CreateDate,
                CallStack = CallStack
            };

            AddStackTrace(evnt);
        }

        public void AddEvent(EventLogEntity evnt)
        {
            _routeRepository.AddEvent(evnt);
        }

        private void AddStackTrace(SystemMessagesEntity evnt)
        {
            _routeRepository.AddStackTrace(evnt);
        }

        #endregion

        #region Exceptional touch handle from Driver App

        public List<string> GetCompletedTouchesOrderNumberFacility(string storeNumber, DateTime date)
        {
            return _routeRepository.GetCompletedTouchesOrderNumberFacility(storeNumber, date);
        }

        public Touch GetRecentlyCompletedTouchFromLoad(int loadId)
        {
            return _routeRepository.GetRecentlyCompletedTouchFromLoad(loadId);
        }

        public List<Touch> GetAllTouchesForFacility(string storeNumber, DateTime date)
        {
            return _routeRepository.GetAllTouchesForFacility(storeNumber, date);
        }

        public int HandleAddTouchToRoute(Touch exceptionTouch, SiteInfo facilityInfo, int loadId, int previousTouchId, int previousStopId, bool isExceptionalTouch = true)
        {
            bool isTouchInaGroup = false;
            bool isExistingTouch = false;
            int exceptionTouchId = 0;
            //Touch previousTouch = oload.Touches.Where(t => t.TouchId == previousTouchId).FirstOrDefault();
            Load oload = _routeRepository.FetchLoadByLoadId(loadId);
            Touch previousTouch = _routeRepository.GetRecentlyCompletedTouchFromLoad(loadId);

            //Touch previousTouch = oload.Touches.Where(t => t.Status == "Completed" || t.Status == "Skipped" || t.Status == "Skipped").OrderByDescending(t => t.RouteSequence).FirstOrDefault();
            Touch nextActualTouch = null;

            //If the previous completed touch is in a group then need to check next touch is involved in a group or not
            if (previousTouch != null)
            {
                previousTouchId = previousTouch.TouchId;

                if (previousTouch.GroupGUID != null)
                {
                    nextActualTouch = oload.Touches.Where(t => t.RouteSequence == (previousTouch.RouteSequence + 1)).FirstOrDefault();

                    isTouchInaGroup = (nextActualTouch != null && nextActualTouch.GroupGUID != null && previousTouch.GroupGUID == nextActualTouch.GroupGUID);
                }
            }

            //1. Previous touch in a group    
            //2. Exception touch is in a group
            //3. Exception touch is non-group

            //1. Previous touch in a group    
            if (isTouchInaGroup)
            {
                //Ungroup previous touches
                HandleUnGroupInCurrentRoute(facilityInfo, previousTouch, nextActualTouch, oload);
            }

            //2
            //If the touch is assigned to other load then need to remove from that load and add it to this load
            if (exceptionTouch.TouchId > 0)
            {
                isExistingTouch = true;
                //2. Exception touch is in a group
                if (!string.IsNullOrWhiteSpace(exceptionTouch.GroupGUID.ToString()))
                {
                    Load oExceptionload = _routeRepository.FetchLoadByLoadId(Convert.ToInt32(exceptionTouch.LoadId));

                    Touch dbExceptionTouch = oExceptionload.Touches.Where(t => t.TouchId == exceptionTouch.TouchId).First();

                    HandleUnGroupInExceptionTouchRoute(facilityInfo, dbExceptionTouch, oExceptionload);
                }

                //3. Assign exception touch to current route
                _routeRepository.MoveTouchToAnotherLoad(exceptionTouch.TouchId, oload.LoadId, isExceptionalTouch);//

                exceptionTouchId = exceptionTouch.TouchId;
            }
            else
            {
                exceptionTouchId = AddNewTouchToRoute(exceptionTouch, facilityInfo, oload, previousTouch, previousStopId);
            }

            //Update consicute touchs route sequence
            if (exceptionTouchId > 0)
            {
                _routeRepository.SaveTouchRouteSequenct(oload.LoadId, previousTouchId, exceptionTouchId, isExistingTouch);
            }

            return exceptionTouchId;
        }

        private int AddNewTouchToRoute(Touch exceptionTouch, SiteInfo site, Load oload, Touch previousTouch, int previousStopId)
        {
            exceptionTouch.OriginAddress.AddressType = AddressComparison(exceptionTouch.OriginAddress, site.SiteAddress) ? PREnums.AddressType.Warehouse : PREnums.AddressType.Customer;

            exceptionTouch.OriginAddress.FacilityStoreNo = Convert.ToInt32(site.GlobalSiteNum);

            exceptionTouch.DestAddress.AddressType = AddressComparison(exceptionTouch.DestAddress, site.SiteAddress) ? PREnums.AddressType.Warehouse : PREnums.AddressType.Customer;
            exceptionTouch.DestAddress.FacilityStoreNo = Convert.ToInt32(site.GlobalSiteNum);

            exceptionTouch.OriginAddress = _routeRepository.ManageAddress(exceptionTouch.OriginAddress);
            exceptionTouch.DestAddress = _routeRepository.ManageAddress(exceptionTouch.DestAddress);
            Address facilityAddress = _routeRepository.ManageAddress(site.SiteAddress);

            //exceptionTouch is adding after previousTouch, so we 
            exceptionTouch.RouteSequence = previousTouch != null ? previousTouch.RouteSequence + 1 : 1;
            //Breake touch into stops as per the touch type
            BreakIntoStop(exceptionTouch, facilityAddress);

            //Save touch to DB 
            int TouchId = _routeRepository.SaveTouch(exceptionTouch);

            return TouchId;
        }

        //This method will take care ungrouping of existing exception touch route
        private void HandleUnGroupInExceptionTouchRoute(SiteInfo site, Touch exceptionTouch, Load oload)
        {
            List<Touch> groupPreviousTouchBeforeTouchesHaveInSameGroup = oload.Touches.Where(t => t.GroupGUID == exceptionTouch.GroupGUID && t.RouteSequence < exceptionTouch.RouteSequence).ToList();
            int groupPreviousTouchBeforeTouchesHaveInSameGroupCount = groupPreviousTouchBeforeTouchesHaveInSameGroup.Count();

            List<Touch> groupNextToNextTouchHaveInSameGroup = oload.Touches.Where(t => t.GroupGUID == exceptionTouch.GroupGUID && t.RouteSequence > exceptionTouch.RouteSequence).ToList();

            if (groupPreviousTouchBeforeTouchesHaveInSameGroupCount == 1)
            {
                UngroupTwoTouchInRoute(site, groupPreviousTouchBeforeTouchesHaveInSameGroup[0], exceptionTouch);
            }
            else if (groupPreviousTouchBeforeTouchesHaveInSameGroupCount > 1)
            {
                _routeRepository.UpdateGUIDForTouches(groupPreviousTouchBeforeTouchesHaveInSameGroup, Guid.NewGuid());

                //After making previous touches are a saperate group then we have to make just previous touch should be ungrouped with exception touch
                UngroupExceptionTouchInExistingRoute(site, groupPreviousTouchBeforeTouchesHaveInSameGroup[groupPreviousTouchBeforeTouchesHaveInSameGroupCount - 1], exceptionTouch);
            }

            if (groupNextToNextTouchHaveInSameGroup.Count() == 1)
                UngroupTwoTouchInRoute(site, exceptionTouch, groupNextToNextTouchHaveInSameGroup[0]);
            else if (groupNextToNextTouchHaveInSameGroup.Count() > 1)
            {
                _routeRepository.UpdateGUIDForTouches(groupNextToNextTouchHaveInSameGroup, Guid.NewGuid());

                UngroupOnlyExceptionTouch(site, exceptionTouch);

                Address facilityAddress = _routeRepository.ManageAddress(site.SiteAddress);
                groupNextToNextTouchHaveInSameGroup[0].PickupStop.OriginAddress = facilityAddress;
                //Here Make sure next touch start address from Facility
                _routeRepository.UpdateStartAddressAsFacilityForNextTouchOfExceptionTouch(groupNextToNextTouchHaveInSameGroup[0]);
            }
        }

        private void HandleUnGroupInCurrentRoute(SiteInfo site, Touch previousTouch, Touch nextActualTouch, Load oload)
        {
            List<Touch> groupPreviousTouchBeforeTouchesHaveInSameGroup = oload.Touches.Where(t => t.GroupGUID == previousTouch.GroupGUID && t.RouteSequence <= previousTouch.RouteSequence).ToList();

            List<Touch> groupNextToNextTouchHaveInSameGroup = oload.Touches.Where(t => t.GroupGUID == previousTouch.GroupGUID && t.RouteSequence >= nextActualTouch.RouteSequence).ToList();

            if (groupPreviousTouchBeforeTouchesHaveInSameGroup.Count() == 1 && groupNextToNextTouchHaveInSameGroup.Count() == 1)
            {
                UngroupTwoTouchInRoute(site, previousTouch, nextActualTouch);
            }
            else
            {
                if (groupPreviousTouchBeforeTouchesHaveInSameGroup.Count() > 1)
                    _routeRepository.UpdateGUIDForTouches(groupPreviousTouchBeforeTouchesHaveInSameGroup, Guid.NewGuid());

                if (groupNextToNextTouchHaveInSameGroup.Count() > 1)
                    _routeRepository.UpdateGUIDForTouches(groupNextToNextTouchHaveInSameGroup, Guid.NewGuid());
            }
        }

        private void UngroupOnlyExceptionTouch(SiteInfo site, Touch exceptionTouch)
        {
            exceptionTouch.Stops.ForEach(s => s.IsRequired = true);

            exceptionTouch.IsGrouped = false;
            exceptionTouch.GroupGUID = null;

            //Need to implement this method to handle ungrouping touches
            UpdateStopsBasedonTouchUnGrouping(exceptionTouch, site.SiteAddress);

            List<Touch> ltouch = new List<Touch>();
            ltouch.Add(exceptionTouch);
            _routeRepository.UpdateUnGroupingStatus(ltouch, true);
        }

        private void UngroupExceptionTouchInExistingRoute(SiteInfo site, Touch previousTouch, Touch exceptionTouch)
        {
            Address facilityAddress = _routeRepository.ManageAddress(site.SiteAddress);

            //We are trying to ungrouping only exception touch 
            //Need to implement this method to handle ungrouping touches
            UpdateStopsBasedonTouchUnGrouping(previousTouch, facilityAddress);

            _routeRepository.UpdateUnGroupingStatusOnlyPreviousTouch(previousTouch);


            UngroupOnlyExceptionTouch(site, exceptionTouch);
        }

        private void UngroupTwoTouchInRoute(SiteInfo site, Touch previousTouch, Touch nextActualTouch)
        {
            List<Touch> touches = new List<Touch>();
            previousTouch.Stops.ForEach(s => s.IsRequired = true);
            touches.Add(previousTouch);
            touches.Add(nextActualTouch);

            Address facilityAddress = _routeRepository.ManageAddress(site.SiteAddress);

            for (int index = 0; index < touches.Count(); index++)
            {
                touches[index].IsGrouped = false;
                touches[index].GroupGUID = null;

                //Need to implement this method to handle ungrouping touches
                UpdateStopsBasedonTouchUnGrouping(touches[index], facilityAddress);
            }

            _routeRepository.UpdateUnGroupingStatus(touches, true);
        }

        #endregion
    }
}
