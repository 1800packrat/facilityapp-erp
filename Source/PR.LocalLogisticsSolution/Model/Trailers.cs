﻿namespace PR.LocalLogisticsSolution.Model
{
    public class Trailers
    {
        //public string TrailerName { get; set; }
        //public string TrailerNumber { get; set; }
        //public int TrailerTypeID { get; set; }
        //public int TrailerStatus { get; set; }
        public int TrailerID { get; set; }
        
        public int TrailerUpdatedBy { get; set; }
        
        public System.DateTime TrailerUpdatedAt { get; set; }
        
        public int TrailerCreatedBy { get; set; }
        
        public System.DateTime TrailerCreatedAt { get; set; }
        
        public string FleetID { get; set; }
        
        public string TrailerVIN { get; set; }
        
        public string TrailerPlateNo { get; set; }
        
        public string TrailerState { get; set; }
    }
}
