﻿using System;

namespace PR.LocalLogisticsSolution.Model
{
    //Please update this information as per 
    public enum StopType
    {
        FromFacility = 1,
        ToFacility = 2,
        PICKUP = 3,
        DROPOFF = 4,
        WEIGHSTATION = 5,
        BUSYTIME=6,
        TRAILERHOOK=7,
        TRAILERUNHOOK=8
    }

    public class LoadHelper
    {
        public static string[] ExcudeTouchTyps = { "BT" };
    }

    public enum SLUpdateType
    {
        Nochanges = 1,
        Sync = 2,
        Canceled = 3,
        OnlyAddressUpdated = 4,
        OnlyInfoUpdated = 5,
        AddressAndInfoUpdated = 6
    }

    public class EventLogEntity
    {
        public int EventLogId { get; set; }
        public string ApplicationName { get; set; }
        public int? StoreNo { get; set; }
        public int? QORID { get; set; }
        public string Description { get; set; }
        public string Activity { get; set; }
        public string IPAddress { get; set; }
        public DateTime CreateDate { get; set; }
        public string RefID { get; set; }
    }

    public partial class SystemMessagesEntity
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public string MessageType { get; set; }
        public string MessageBody { get; set; }
        public string MessageArea { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CallStack { get; set; }
    }

    public static class PRStagingActivityType
    {
        public static string NonRecurringItemUpdated = "Non Recurring item updated";
        public static string SettleOrder = "Settle Order";
        public static string MoveOut = "Move Out";
        public static string LDMUnitTransfer = "LDM Unit Transfer";
        public static string MoveIn = "Move In";
        public static string TouchStatusUpdated ="Touch status updated";
    }
}
