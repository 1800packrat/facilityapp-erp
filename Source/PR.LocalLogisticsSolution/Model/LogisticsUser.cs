﻿using PR.Entities;
using System;
using System.Collections.Generic;

namespace PR.LocalLogisticsSolution.Model
{
    public class LogisticsUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        public List<SiteInfo> Facilities { get; set; }
        public string UserName { get; set; }
        public int UserType { get; set; }
        public int DriverID { get; set; }
        public int ID { get; set; }
        public string Password { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public bool UserIsActive { get; set; }
        public int StoreNo { get; set; }
        public string createdBy { get; set; }
        public string updatedBy { get; set; }
        public bool IsChangePasswordRequired { get; set; }
        public DateTime PasswordUpdatedDate { get; set; }
        public int? UniqueID { get; set; }
    }
}
