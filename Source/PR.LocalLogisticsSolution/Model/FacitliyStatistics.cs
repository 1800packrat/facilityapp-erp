﻿using System;

namespace PR.LocalLogisticsSolution.Model
{
    public class FacitliyStatistics
    {
        public string FacilityName { get; set; }

        public DateTime Date { get; set; }

        public string DateDisp { get { return Date.ToShortDateString(); } }

        public string FacilityId { get; set; }

        public string LocationCode { get; set; }

        public string SrOM { get; set; }

        public string OM { get; set; }

        public bool DayLocked { get; set; }

        public int Driverslocked { get; set; }

        public int DriversUnlocked { get; set; }

        public int TotalTouches { get; set; }

        public int LockedTouches { get; set; }

        public decimal LockedTouchesPercent { get; set; }

        public int UnlockedTouches { get; set; }

        public int TotalTrucks { get; set; }

        public int TrucksAvailable { get; set; }

        public int TrucksNotAvailable { get; set; }
    }
}
