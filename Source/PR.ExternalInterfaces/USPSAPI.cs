﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Xml;
using PR.ExternalInterfaces.USPS_XML_Objects;

namespace PR.ExternalInterfaces
{
    using System;
    using PR.Entities;
    using PR.DataHandler;
    using UtilityLibrary;
    using System.Text;

    class UspsApi
    {
        private const string CityStateLookUp = "CityStateLookUp";
        private const string OrgDestCityStateLookUp = "OrgDestCityStateLookUp";
        private readonly string _rawUrl = ConfigurationManager.AppSettings["USPS_API_URL"];
        private readonly string _userName = ConfigurationManager.AppSettings["USPS_UserName"];
        private readonly bool _logUspsRequestResponse = ConfigurationManager.AppSettings["Log_USPS_Request_Response"] == "1";
        public Address UsAddressLookUpByZipcode(string usZipcode)
        {
            try
            {
                var apiKey = ConfigurationManager.AppSettings["USPS_City_State_Lookup_API_Key"];
                var url = _rawUrl.Replace("{api_key}", apiKey).Replace("{xml}", GetXmlDocument(CityStateLookUp)
                    .Replace("{username}", _userName)
                    .Replace("{zipcode}", usZipcode));
                var response = CommonUtility.HttpGet(url);
                LogUspsRequestResponse(url, response, apiKey);
                var cityStateLookupResponse = response.ParseXml<CityStateLookupResponse>();
                if (!cityStateLookupResponse.ZipCode.Any(z=>z.City == null || z.State == null))
                {
                    return new Address()
                    {
                        City = cityStateLookupResponse.ZipCode.FirstOrDefault()?.City,
                        State = cityStateLookupResponse.ZipCode.FirstOrDefault()?.State,
                        Zip = Convert.ToString(cityStateLookupResponse.ZipCode.FirstOrDefault()?.Zip5)
                    };
                }
            }
            catch (Exception ex)
            {
                LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "USPS", $"USPS UsAddressLookUpByZipcode API Exception For Zip: {usZipcode}", ex.Message, ex.StackTrace, 0);
            }
            return null;
        }

        public IEnumerable<Address> UsAddressLookUpByOrgDestZipcodes(string usOrgZipcode, string usDestZipcode)
        {
            try
            {
                var apiKey = ConfigurationManager.AppSettings["USPS_City_State_Lookup_API_Key"];
                var url = _rawUrl.Replace("{api_key}", apiKey).Replace("{xml}", GetXmlDocument(OrgDestCityStateLookUp)
                    .Replace("{username}", _userName)
                    .Replace("{origin_zipcode}", usOrgZipcode)
                    .Replace("{destination_zipcode}", usDestZipcode));
                var response = CommonUtility.HttpGet(url);
                LogUspsRequestResponse(url, response, apiKey);
                var cityStateLookupResponse = response.ParseXml<CityStateLookupResponse>();
                if (cityStateLookupResponse != null)
                {
                    return cityStateLookupResponse.ZipCode.Select(z => new Address()
                    {
                        City = z?.City,
                        State = z?.State,
                        Zip = Convert.ToString(z?.Zip5)
                    });
                }
            }
            catch (Exception ex)
            {
                LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "USPS", "USPS UsAddressLookUpByOrgDestZipcodes API Exception", ex.Message, ex.StackTrace, 0);
            }
            return null;
        }

        string GetXmlDocument(string xmlName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            XmlDocument doc = new XmlDocument();
            using (XmlTextReader reader = new XmlTextReader(asm.GetManifestResourceStream($"PR.ExternalInterfaces.USPS_XMLs.{xmlName}.xml")))
            {
                StringBuilder sBuilder = new StringBuilder();

                while (reader.Read())
                    sBuilder.AppendLine(reader.ReadOuterXml());

                return sBuilder.ToString();
            }
        }

        void LogUspsRequestResponse(string request, string response, string methodName)
        {
            try
            {
                if (_logUspsRequestResponse)
                {
                    (new LocalDataHandler()).LogUspsRequestResponse(request, response, DateTime.Now, methodName);
                }
            }
            catch
            {
                // ignored
            }
        }
    }
}
