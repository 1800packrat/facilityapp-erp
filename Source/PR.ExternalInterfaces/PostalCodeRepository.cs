﻿using System;
using PR.Entities;
using PR.DataHandler;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace PR.ExternalInterfaces
{
    public static class PostalCodeRepository
    {
        #region "Variables"
        private static List<PostalCodes> _PostalCodes = new List<PostalCodes>();
        private static DateTime _lastUpdated = DateTime.Now;
        private static int CacheTimeoutForPostalCode = string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CacheTimeoutForPostalCode"]) ? 60 : Convert.ToInt16(ConfigurationManager.AppSettings["CacheTimeoutForPostalCode"]); 
        private static readonly object Lock = new object();
        #endregion

        #region "Public Methods"
        public static PostalCodes GetPostalCode(string Zipcode)
        {
            return ListPostalCodes?.Where(p => p.Zipcode == Zipcode).FirstOrDefault();
        }

        public static List<PostalCodes> ListPostalCodes
        {
            get
            {
                try
                {
                    if (_PostalCodes?.Count > 0 && _lastUpdated.AddMinutes(CacheTimeoutForPostalCode) >= DateTime.Now)
                        return _PostalCodes;

                    lock (Lock)
                    {
                        if (_PostalCodes?.Count > 0 && _lastUpdated.AddMinutes(CacheTimeoutForPostalCode) >= DateTime.Now)
                            return _PostalCodes;

                        _lastUpdated = DateTime.Now;
                        _PostalCodes.Clear();
                        _PostalCodes = RetrievePostalCodes();
                    }
                }
                catch (Exception ex)
                {
                    LocalDataHandler dh = new LocalDataHandler();
                    dh.InsertPRErrorLog(DateTime.Now, "PostalCode", "Postal Code Retrieval", ex.Message, ex.StackTrace, 0, string.Empty, string.Empty);
                }

                return _PostalCodes;
            }
        }

        public static void UpdatePostalCodeStatus(PostalCodes postalCode, PostalCodeValidationStatus status)
        {
            var pc = _PostalCodes.Where(p => p.Zipcode == postalCode.Zipcode).FirstOrDefault();
            if (pc != null)
            {
                pc.City = string.IsNullOrWhiteSpace(postalCode.City) ? pc.City : postalCode.City;
                pc.State = string.IsNullOrWhiteSpace(postalCode.State) ? pc.State : postalCode.State;
                pc.Status = status;
                
                //pc.Latitude = pc.Latitude <= 0 && postalCode.Latitude > 0 ? postalCode.Latitude : pc.Latitude;
                //pc.Latitude = pc.Longitude <= 0 && postalCode.Longitude > 0 ? postalCode.Longitude : pc.Longitude;

                LocalDataHandler dh = new LocalDataHandler();
                dh.UpdatePostalCodeStatus(postalCode.Zipcode, status);
            }
        }

        public static void InsertPostalCode(PostalCodes postalCode)
        {
            LocalDataHandler dh = new LocalDataHandler();
            dh.InsertPostalCode(postalCode);

            _PostalCodes.Add(postalCode);
        }

        #endregion
        #region "Private Methods"
        private static List<PostalCodes> RetrievePostalCodes()
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetPostalCodes();
        }
        #endregion
    }
}
