﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace PR.ExternalInterfaces
{
    public static class PrExternalInterfaceHelper
    {
        public static T ParseXml<T>(this string @this) where T : class
        {
            try
            {
                var reader = XmlReader.Create(@this.Trim().ToStream(), new XmlReaderSettings() { ConformanceLevel = ConformanceLevel.Document });
                return new XmlSerializer(typeof(T)).Deserialize(reader) as T;
            }
            catch(Exception ex)
            {
                // ignored
            }
            return null;
        }
        public static Stream ToStream(this string @this)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(@this);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
