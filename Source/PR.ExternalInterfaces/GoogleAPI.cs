﻿// -----------------------------------------------------------------------
// <CreatedBy file="GoogleAPI.cs" Name="Rajib basu" Date="July15,2014">
// GoogleAPI calles  maps.googleapis.com and retrieves various information related to address
// </CreatedBy>
// -----------------------------------------------------------------------

namespace PR.ExternalInterfaces
{
    using System;
    using System.Text;
    using PR.Entities;
    using PR.DataHandler;
    using System.Data;
    using System.Web;
    using System.Security.Cryptography;
    using System.Xml;
    using System.Threading;

    /// <summary>
    /// GoogleAPI uses Google API to validates Address, get Geocodeor to corrects Address
    /// </summary>
    public class GoogleAPI
    {
        //private string CLIENTID = string.Empty;
        //private string GOOGLE_KEY = string.Empty;
        //private string US_PostOffice_API_Key = string.Empty;
        private decimal ThresholdGeocodeMaxDiff;

        #region Static properties

        private static string CLIENTID = System.Configuration.ConfigurationManager.AppSettings["CLIENTID"].ToString();

        private static string GOOGLE_KEY = System.Configuration.ConfigurationManager.AppSettings["GOOGLE_KEY"].ToString(); 

        private static string US_PostOffice_API_Key =  System.Configuration.ConfigurationManager.AppSettings["US_PostOffice_API_Key"].ToString(); 

        private static string applicationName= string.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["ApplicationName"]) ? "ApplicationNameNotAvailable" : System.Configuration.ConfigurationManager.AppSettings["ApplicationName"]; 

        private static string serverName =  Environment.MachineName;

        #endregion

        string countryCode = string.Empty;
        public int recursionCounter = 0;

        public GoogleAPI()
        {
            if (string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["CLIENTID"])
                || string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["GOOGLE_KEY"])
                || string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["US_PostOffice_API_Key"]))
                throw new Exception("Could Not able to find Google API key (CLIENTID, GOOGLE_KEY, US_PostOffice_API_Key), Please check configuration file");

            //CLIENTID = System.Configuration.ConfigurationManager.AppSettings["CLIENTID"].ToString();
            //GOOGLE_KEY = System.Configuration.ConfigurationManager.AppSettings["GOOGLE_KEY"].ToString();
            //US_PostOffice_API_Key = System.Configuration.ConfigurationManager.AppSettings["US_PostOffice_API_Key"].ToString();

            ThresholdGeocodeMaxDiff = string.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["ThresholdGeocodeMaxDiff"]) ? (decimal)0.1 : Decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["ThresholdGeocodeMaxDiff"]);
        }

        private AddressValidator ParseDistanceResponse(XmlDocument xmlDoc, XmlNodeList distance, Address originationAddress, Address destinationAddress)
        {
            AddressValidator returnClass = new AddressValidator();

            if (distance[0].ChildNodes[1].InnerText.Trim().IndexOf("ft") != -1)
                returnClass.Distance = 0;
            else
                returnClass.Distance = Convert.ToDecimal(distance[0].ChildNodes[1].InnerText.Replace(" mi", ""));

            if (returnClass.Distance == 0 && originationAddress.Zip != destinationAddress.Zip)
                throw new Exception("Origin and destination zips are not same so expected distance should be more than zero miles.");

            XmlNodeList originAddressNode = xmlDoc.GetElementsByTagName("origin_address");
            string[] originAddArray = originAddressNode[0].InnerText.Split(',');

            //Some times Google returns address like, US-11, United State (single comma saperated address) or 35.797392699999996,-86.441343 (lat, long values)
            //In this case dont consider google return address, rather return back original address
            if (originAddArray.Length > 3)
            {
                returnClass.OriginationAddress.State = originAddArray[originAddArray.Length - 2].Trim().Split(' ')[0];
                returnClass.OriginationAddress.City = originAddArray[originAddArray.Length - 3].Trim();
            }
            else
            {
                returnClass.OriginationAddress.State = originationAddress.State;
                returnClass.OriginationAddress.City = originationAddress.City;
            }
            returnClass.OriginationAddress.Zip = originationAddress.Zip;


            XmlNodeList destAddressNode = xmlDoc.GetElementsByTagName("destination_address");
            string[] destAddArray = destAddressNode[0].InnerText.Split(',');

            //Some times Google returns address like, US-11, United State (single comma saperated address) or 35.797392699999996,-86.441343 (lat, long values)
            //In this case dont consider google return address, rather return back original address
            if (destAddArray.Length > 3)
            {
                returnClass.DestinationAddress.State = destAddArray[destAddArray.Length - 2].Trim().Split(' ')[0];
                returnClass.DestinationAddress.City = destAddArray[destAddArray.Length - 3].Trim();
            }
            else
            {
                returnClass.DestinationAddress.State = destinationAddress.State;
                returnClass.DestinationAddress.City = destinationAddress.City;
            }

            returnClass.DestinationAddress.Zip = destinationAddress.Zip;
            returnClass.IsStraightLineDistance = false;
            returnClass.ErrorMessage = string.Empty;

            XmlNodeList duration = xmlDoc.GetElementsByTagName("duration");
            returnClass.Duration = duration[0].ChildNodes[1].InnerText;

            return returnClass;
        }

        private AddressValidator InvokeDistancematrixCall(Address originationAddress, Address destinationAddress, bool isRecursiveCall)
        {
            AddressValidator returnClass = new AddressValidator();
            LocalDataHandler dh = new LocalDataHandler();
            XmlDocument xmlDoc = new XmlDocument();
            string url = string.Empty;
            DateTime startTime = DateTime.Now;
            DateTime endDateTime = DateTime.Now;
            string urlRequest = string.Empty;
            try
            {
                urlRequest = $"/maps/api/distancematrix/xml?origins={originationAddress.Latitude},{originationAddress.Longitude}";
                urlRequest = $"{urlRequest}&destinations={destinationAddress.Latitude},{destinationAddress.Longitude}";
                urlRequest = $"{urlRequest}&mode=driving&units=imperial&avoid=ferries&language=en-EN&client={HttpUtility.UrlEncode(CLIENTID)}";

                HMACSHA1 myhmacsha1 = new HMACSHA1();
                string usablePrivateKey = GOOGLE_KEY.Replace("-", "+").Replace("_", "/");
                myhmacsha1.Key = Convert.FromBase64String(usablePrivateKey);
                var hash = myhmacsha1.ComputeHash(Encoding.ASCII.GetBytes(urlRequest));
                //TODO : remove no need to use signature
                url = $"http://maps.googleapis.com{urlRequest}&signature={HttpUtility.UrlEncode(Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_"))}";

                startTime = DateTime.Now;
                xmlDoc.Load(url);
                endDateTime = DateTime.Now;

                if (xmlDoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OK")
                {
                    XmlNodeList distance = xmlDoc.GetElementsByTagName("distance");

                    if (distance[0] != null) // if google is able to calculate distance
                    {
                        returnClass = ParseDistanceResponse(xmlDoc, distance, originationAddress, destinationAddress);
                    }
                    else
                    {
                        Address oAddress = new Address();

                        if (string.IsNullOrWhiteSpace(originationAddress.AddressLine1) &&
                            string.IsNullOrWhiteSpace(originationAddress.AddressLine2) &&
                            !string.IsNullOrWhiteSpace(originationAddress.Zip)
                        ) // checking if only zip code is passed
                        {
                            originationAddress = RetPostOfficeAddress(originationAddress);
                        }
                        else if (!string.IsNullOrWhiteSpace(originationAddress.Latitude) &&
                                 !string.IsNullOrWhiteSpace(originationAddress.Longitude)
                        ) // validating address based on lat/long value or other values
                        {
                            oAddress.Zip = ValidateAddress(originationAddress).Zip;
                            originationAddress = RetPostOfficeAddress(oAddress);
                        }
                        else // validating address based on  other values
                        {
                            oAddress.Zip = ValidateAddress(originationAddress).Zip;
                            originationAddress = RetPostOfficeAddress(oAddress);
                        }
                        
                        oAddress = new Address();

                        if (string.IsNullOrWhiteSpace(destinationAddress.AddressLine1) &&
                            string.IsNullOrWhiteSpace(destinationAddress.AddressLine2) &&
                            !string.IsNullOrWhiteSpace(destinationAddress.Zip)) // checking dest address
                        {
                            destinationAddress = RetPostOfficeAddress(destinationAddress);
                        }
                        else if (!string.IsNullOrWhiteSpace(destinationAddress.Latitude) &&
                                 !string.IsNullOrWhiteSpace(destinationAddress.Longitude))
                        {
                            oAddress.Zip = ValidateAddress(destinationAddress).Zip;
                            destinationAddress = RetPostOfficeAddress(oAddress);
                        }
                        else if (string.IsNullOrWhiteSpace(destinationAddress.AddressLine1) &&
                                 string.IsNullOrWhiteSpace(destinationAddress.AddressLine2))
                        {
                            oAddress.Zip = ValidateAddress(destinationAddress).Zip;
                            destinationAddress = RetPostOfficeAddress(oAddress);
                        }

                        if (isRecursiveCall) // if isRecursiveCall is true. Should exit after one round of address correction
                        {
                            returnClass.Distance = 0;
                            returnClass.Duration = string.Empty;
                            returnClass.OriginationAddress.Zip = originationAddress.Zip;
                            returnClass.DestinationAddress.Zip = destinationAddress.Zip;
                            returnClass.IsStraightLineDistance = false;
                            returnClass.ErrorMessage = "Sorry, we could not calculate directions";
                        }
                        else
                        {
                            returnClass = GetDistance(originationAddress, destinationAddress, true);
                        }
                    }
                }
                else
                {
                    returnClass.Distance = 0;
                    returnClass.Duration = string.Empty;
                    returnClass.OriginationAddress.Zip = originationAddress.Zip;
                    returnClass.DestinationAddress.Zip = destinationAddress.Zip;
                    returnClass.IsStraightLineDistance = false;
                    returnClass.ErrorMessage = "Google API is unable to find distance";
                }
            }
            catch (Exception ex)
            {
                returnClass.Distance = 0;
                returnClass.Duration = string.Empty;
                returnClass.OriginationAddress.Zip = originationAddress.Zip;
                returnClass.DestinationAddress.Zip = destinationAddress.Zip;
                returnClass.IsStraightLineDistance = false;
                returnClass.ErrorMessage = ex.Message;
                //LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "google", "google api exception", ex.Message, ex.StackTrace, 0);
            }
            finally
            {
                // log request ,response to DB
                string googleResp = ((xmlDoc != null && !string.IsNullOrEmpty(xmlDoc.InnerXml)) ? xmlDoc.InnerXml : "Error in google response");
                string googleReq = string.IsNullOrWhiteSpace(url) ? $"Origin Address: {RetAddressAsString(originationAddress)}; Destination Address: {RetAddressAsString(destinationAddress)}; " : url;
                GoogleMapAuditLog googleMapAuditLog = new GoogleMapAuditLog { Request = googleReq, Response = googleResp, StartTime = startTime, EndTime = endDateTime, MethodName = "GetDistance", ApplicationName = applicationName, ServerName = serverName };
                dh.LogGAPIRequestResponse(googleMapAuditLog);
            }

            return returnClass;
        }

        /// <summary>
        /// GetDistance returns distance in mile between source and destination based on address or lat/lng values
        /// </summary>
        /// <param name="originationAddress"></param>
        /// <param name="destinationAddress"></param>
        /// <returns></returns>
        public AddressValidator GetDistance(Address originationAddress, Address destinationAddress)
        {
            return GetDistance(originationAddress, destinationAddress, false);
        }
        
        private AddressValidator GetDistance(Address originationAddress, Address destinationAddress, bool isRecursiveCall)
        {
            try
            {
                AddressValidator returnClass = new AddressValidator();

                /*if (!string.IsNullOrWhiteSpace(originationAddress.Zip) && !string.IsNullOrWhiteSpace(destinationAddress.Zip))
                {
                    if (originationAddress.Zip.IndexOf('-') > 0 && originationAddress.Zip.Length > 5)
                        originationAddress.Zip = originationAddress.Zip.Substring(0, 5);
                    if ((destinationAddress.Zip.IndexOf('-') > 0) && destinationAddress.Zip.Length > 5)
                        destinationAddress.Zip = destinationAddress.Zip.Substring(0, 5);

                    GenerateLatLongBasedOnZip(ref destinationAddress);

                    //Without destination lat/long values, there is no use of origin lat/long values
                    if (!string.IsNullOrWhiteSpace(destinationAddress.Latitude) && !string.IsNullOrWhiteSpace(destinationAddress.Longitude))
                    {
                        if (!string.IsNullOrWhiteSpace(originationAddress.AddressLine1) && !string.IsNullOrWhiteSpace(originationAddress.Zip))
                            GenerateLatLongBasedOnAddress(originationAddress, false);
                        else
                            GenerateLatLongBasedOnZip(ref originationAddress);
                    }
                }*/

                if (destinationAddress.IsGeoCodesValid == false && !string.IsNullOrWhiteSpace(destinationAddress.Zip))
                {
                    if ((destinationAddress.Zip.IndexOf('-') > 0) && destinationAddress.Zip.Length > 5)
                        destinationAddress.Zip = destinationAddress.Zip.Substring(0, 5);

                    GenerateLatLongBasedOnZip(ref destinationAddress);
                }
                
                //Without destination lat/long values, there is no use of origin lat/long values
                if (destinationAddress.IsGeoCodesValid && (originationAddress.IsGeoCodesValid == false && !string.IsNullOrWhiteSpace(originationAddress.Zip)))
                {
                    if ((originationAddress.Zip.IndexOf('-') > 0) && originationAddress.Zip.Length > 5)
                        originationAddress.Zip = originationAddress.Zip.Substring(0, 5);

                    if (!string.IsNullOrWhiteSpace(originationAddress.AddressLine1) && !string.IsNullOrWhiteSpace(originationAddress.Zip))
                        GenerateLatLongBasedOnAddress(originationAddress, false);
                    else
                        GenerateLatLongBasedOnZip(ref originationAddress);
                }

                //if (string.IsNullOrWhiteSpace(originationAddress.Latitude) || string.IsNullOrWhiteSpace(originationAddress.Longitude))
                if(originationAddress.IsGeoCodesValid == false)
                {
                    returnClass.Distance = 0;
                    returnClass.Duration = string.Empty;
                    returnClass.OriginationAddress.Zip = originationAddress.Zip;
                    returnClass.DestinationAddress.Zip = destinationAddress.Zip;
                    returnClass.IsStraightLineDistance = false;
                    returnClass.ErrorMessage = "No original address provided";
                }
                //else if (string.IsNullOrWhiteSpace(destinationAddress.Latitude) || string.IsNullOrWhiteSpace(destinationAddress.Longitude))
                else if (destinationAddress.IsGeoCodesValid == false)
                {
                    returnClass.Distance = 0;
                    returnClass.Duration = string.Empty;
                    returnClass.OriginationAddress.Zip = originationAddress.Zip;
                    returnClass.DestinationAddress.Zip = destinationAddress.Zip;
                    returnClass.IsStraightLineDistance = false;
                    returnClass.ErrorMessage = "No destination address provided";
                }
                else
                    returnClass = InvokeDistancematrixCall(originationAddress, destinationAddress, isRecursiveCall);

                return returnClass;
            }
            catch (Exception ex)
            {
                LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "google", "google api exception", ex.Message, ex.StackTrace, 0);
            }
            return null;
        }


        /// <summary>
        /// This method validates address lat long values with database lat long values by straighline distance. 
        /// If  straightline distnace is more is more than ThresholdMiles then use database lat long values for this zip
        /// else return true
        /// If zipcode does not find in database then insert new zip code to db with google lat long value
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private Address ValidateLatLongIsValid(Address address, bool isRecursiveCall, string googleRespStatus = "")
        {
            try
            {
                LocalDataHandler dh = new LocalDataHandler();
                //var dbPostalCodes = dh.GetPostalCodeByZip(address.Zip);
                PostalCodes dbPostalCodes = PostalCodeRepository.GetPostalCode(address.Zip);
                bool noGeocodesFromGoogle = string.IsNullOrEmpty(address.Latitude) || string.IsNullOrEmpty(address.Longitude);

                if (dbPostalCodes != null && dbPostalCodes.Latitude != 0 && dbPostalCodes.Longitude != 0)
                {
                    Address dbAddress = new Address
                    {
                        Zip = dbPostalCodes.Zipcode,
                        Latitude = dbPostalCodes.Latitude.ToString(),
                        Longitude = dbPostalCodes.Longitude.ToString()
                    };

                    bool isLatLongValuesAreNear = IsLatLongValuesAreNear(address, dbAddress);

                    //IF no lat long from google or if the distance between google lat long and db lat long is not equal them update status and return db lat long values
                    if (noGeocodesFromGoogle || isLatLongValuesAreNear == false)
                    {
                        address.Latitude = dbAddress.Latitude;
                        address.Longitude = dbAddress.Longitude;

                        PostalCodeRepository.UpdatePostalCodeStatus(dbPostalCodes, PostalCodeValidationStatus.InvalidGeocodes);
                    }
                }
                else if (noGeocodesFromGoogle)
                {
                    if (!isRecursiveCall && NoGeocodesFromGoogle(address))
                    {
                        //IsRecursiveGenerateLatLongBasedOnZip = true;
                        GenerateLatLongBasedOnZip(ref address, true);
                    }
                    else
                    {
                        //Since this code is added after the fact, to make sure existing scenarios work with google ZERO_RESULTS response we are not throwing an exception.
                        //I know this is not good but we can not change all applications which are using this method.
                        if (googleRespStatus != "ZERO_RESULTS")
                            throw new Exception($"Invalid zip ({address.Zip}) code.");
                    }
                }
                else if (dbPostalCodes == null)
                {
                    if (string.IsNullOrWhiteSpace(address.City) || !string.IsNullOrWhiteSpace(address.State))
                    {
                        var apiAddress = (new PRAddressAPI()).UsAddressLookUpByZipcode(address.Zip);
                        if (apiAddress != null)
                        {
                            address.City = apiAddress.City;
                            address.State = apiAddress.State;
                        }
                    }
                    
                    decimal lat = 0;
                    decimal.TryParse(address.Latitude, out lat);
                    decimal lng = 0;
                    decimal.TryParse(address.Longitude, out lng);

                    PostalCodes pc = new PostalCodes
                    {
                        City = address.City,
                        State = address.State,
                        Zipcode = address.Zip,
                        Latitude = lat,
                        Longitude = lng,
                        Status = PostalCodeValidationStatus.NewGeocodes
                    };

                    PostalCodeRepository.InsertPostalCode(pc);
                }
            }
            catch (Exception ex)
            {
                LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "PostalCode", "Postalcode ValidateLatLongIsValid", ex.Message, (ex.StackTrace + $"; Address: {RetAddressAsString(address)}"), 0);
            }

            return address;
        }

        private bool NoGeocodesFromGoogle(Address address)
        {
            //Validate zip code with USPS, If it is valid then insert into db with no lat long values.
            var apiAddress = (new PRAddressAPI()).UsAddressLookUpByZipcode(address.Zip);

            if (apiAddress != null)
            {
                address.City = apiAddress.City;
                address.State = apiAddress.State;

                decimal lat = 0;
                decimal.TryParse(address.Latitude, out lat);
                decimal lng = 0;
                decimal.TryParse(address.Longitude, out lng);

                PostalCodes pc = new PostalCodes
                {
                    City = address.City,
                    State = address.State,
                    Zipcode = address.Zip,
                    Latitude = lat,
                    Longitude = lng,
                    Status = PostalCodeValidationStatus.NoGeocode
                };

                PostalCodeRepository.InsertPostalCode(pc);

                return true;
            }

            return false;
        }

        private void GenerateLatLongBasedOnAddress(Address address, bool isRecursiveCall)
        {
            XmlDocument xmlDoc = new XmlDocument();
            LocalDataHandler dh = new LocalDataHandler();
            string url = string.Empty;
            DateTime startTime = DateTime.Now;
            DateTime endDateTime = DateTime.Now;
            string strAddress = RetAddressAsString(address);
            bool googleCallexecutedForAuditLog = false;

            try
            {
                if (string.IsNullOrWhiteSpace(address.Latitude) || string.IsNullOrWhiteSpace(address.Longitude))
                {
                    string urlRequest = string.Empty;

                    if (!string.IsNullOrWhiteSpace(address.AddressLine1) && !string.IsNullOrWhiteSpace(address.City) && !string.IsNullOrWhiteSpace(address.State))
                    {
                        urlRequest = $"/maps/api/geocode/xml?address={HttpUtility.UrlEncode(strAddress)}&client={HttpUtility.UrlEncode(CLIENTID)}";
                    }
                    else
                    {
                        countryCode = address.Zip.Trim().Length > 5 ? "CA" : "US";
                        string strComponents = string.Format("country:{0}|postal_code:{1}", countryCode, address.Zip);
                        urlRequest = String.Format("/maps/api/geocode/xml?components={0}&client={1}", HttpUtility.UrlEncode(strComponents), HttpUtility.UrlEncode(CLIENTID));
                    }

                    HMACSHA1 myhmacsha1 = new HMACSHA1();
                    string usablePrivateKey = GOOGLE_KEY.Replace("-", "+").Replace("_", "/");
                    myhmacsha1.Key = Convert.FromBase64String(usablePrivateKey);
                    var hash = myhmacsha1.ComputeHash(Encoding.ASCII.GetBytes(urlRequest));

                    url = $"https://maps.googleapis.com{urlRequest}&signature={(Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_"))}";

                    googleCallexecutedForAuditLog = true;

                    startTime = DateTime.Now;
                    xmlDoc.Load(url);
                    endDateTime = DateTime.Now;

                    if (xmlDoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OK")
                    {
                        //"OK" indicates that no errors occurred; the address was successfully parsed and at least one geocode was returned.
                        XmlNodeList lat = xmlDoc.GetElementsByTagName("lat");
                        address.Latitude = lat[0].InnerText.Trim();

                        XmlNodeList lng = xmlDoc.GetElementsByTagName("lng");
                        address.Longitude = lng[0].InnerText.Trim();
                    }
                    ///If google does not return City State then check with USPS api 
                    else if (isRecursiveCall == false && !string.IsNullOrWhiteSpace(address.Zip))
                    {
                        var apiAddress = (new PRAddressAPI()).UsAddressLookUpByZipcode(address.Zip);
                        if (apiAddress != null && !string.IsNullOrWhiteSpace(apiAddress.City) && !string.IsNullOrWhiteSpace(apiAddress.State))
                        {
                            address.City = apiAddress.City;
                            address.State = apiAddress.State;
                            GenerateLatLongBasedOnAddress(address, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "google", "google api exception", ex.Message, (ex.StackTrace + $"; Address: {strAddress}; URL: {url}"), 0);
                throw new Exception("Google API is unable to find lat/long value");
            }
            finally
            {
                if (googleCallexecutedForAuditLog)
                {
                    string googleResp = ((xmlDoc != null && !string.IsNullOrEmpty(xmlDoc.InnerXml)) ? xmlDoc.InnerXml : "Error in google response");
                    string googleReq = string.IsNullOrWhiteSpace(url) ? $"Address: {strAddress}" : url;
                    GoogleMapAuditLog googleMapAuditLog = new GoogleMapAuditLog { Request = googleReq, Response = googleResp, StartTime = startTime, EndTime = endDateTime, MethodName = "GenerateLatLongBasedOnAddress", ApplicationName = applicationName, ServerName = serverName };
                    dh.LogGAPIRequestResponse(googleMapAuditLog);
                }
            }
        }

        private void GenerateLatLongBasedOnZip(ref Address address)
        {
            GenerateLatLongBasedOnZip(ref address, false);
        }

        private void GenerateLatLongBasedOnZip(ref Address address, bool isRecursiveCall)
        {
            XmlDocument xmlDoc = new XmlDocument();
            LocalDataHandler dh = new LocalDataHandler();
            string url = string.Empty;
            DateTime startTime = DateTime.Now;
            DateTime endDateTime = DateTime.Now;
            string strAddress = RetAddressAsString(address);
            bool googleCallexecutedForAuditLog = false;

            try
            {
                if (string.IsNullOrEmpty(address.Latitude) || string.IsNullOrEmpty(address.Longitude))
                {
                    string urlRequest = string.Empty;
                    if (!string.IsNullOrWhiteSpace(address.City) && !string.IsNullOrWhiteSpace(address.State) && !string.IsNullOrWhiteSpace(address.Zip))
                    {
                        urlRequest = $"/maps/api/geocode/xml?address={HttpUtility.UrlEncode(strAddress)}&client={HttpUtility.UrlEncode(CLIENTID)}";
                    }
                    else
                    {
                        countryCode = address.Zip.Trim().Length > 5 ? "CA" : "US";
                        string strComponents = string.Format("country:{0}|postal_code:{1}", countryCode, address.Zip);
                        urlRequest = String.Format("/maps/api/geocode/xml?components={0}&client={1}", HttpUtility.UrlEncode(strComponents), HttpUtility.UrlEncode(CLIENTID));
                    }

                    HMACSHA1 myhmacsha1 = new HMACSHA1();
                    string usablePrivateKey = GOOGLE_KEY.Replace("-", "+").Replace("_", "/");
                    myhmacsha1.Key = Convert.FromBase64String(usablePrivateKey);
                    var hash = myhmacsha1.ComputeHash(Encoding.ASCII.GetBytes(urlRequest));

                    url = $"https://maps.googleapis.com{urlRequest}&signature={(Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_"))}";

                    googleCallexecutedForAuditLog = true;
                    startTime = DateTime.Now;
                    xmlDoc.Load(url);
                    endDateTime = DateTime.Now;

                    string googleRespStatus = xmlDoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText;

                    if (googleRespStatus == "OK")
                    {
                        //"OK" indicates that no errors occurred; the address was successfully parsed and at least one geocode was returned.
                        XmlNodeList lat = xmlDoc.GetElementsByTagName("lat");
                        address.Latitude = lat[0].InnerText.Trim();

                        XmlNodeList lng = xmlDoc.GetElementsByTagName("lng");
                        address.Longitude = lng[0].InnerText.Trim();
                    }
                    //else if (xmlDoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "ZERO_RESULTS")
                    //{
                    // Continue with the flow
                    //ZERO_RESULTS indicates that the geocode was successful but returned no results. This may occur if the geocoder was passed a non-existent address
                    //}
                    //else
                    //{
                    //    throw new Exception("Google API is unable to find lat/long value");
                    //}
                    
                    ValidateLatLongIsValid(address, isRecursiveCall, googleRespStatus);
                }
            }
            catch (Exception ex)
            {
                //LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "google", "google api exception", ex.Message, (ex.StackTrace + $"; Address: {strAddress}; URL: {url}"), 0);
                throw new Exception("Google API is unable to find lat/long value");
            }
            finally
            {
                if (googleCallexecutedForAuditLog)
                {
                    string googleResp = ((xmlDoc != null && !string.IsNullOrEmpty(xmlDoc.InnerXml)) ? xmlDoc.InnerXml : "Error in google response");
                    string googleReq = string.IsNullOrWhiteSpace(url) ? $"Address: {strAddress}" : url;
                    GoogleMapAuditLog googleMapAuditLog = new GoogleMapAuditLog { Request = googleReq, Response = googleResp, StartTime = startTime, EndTime = endDateTime, MethodName = "GenerateLatLongBasedOnZip", ApplicationName = applicationName, ServerName = serverName };
                    dh.LogGAPIRequestResponse(googleMapAuditLog);
                }
            }
        }

        public bool IsLatLongValuesAreNear(Address address1, Address address2)
        {
            if (!string.IsNullOrWhiteSpace(address1.Latitude) && !string.IsNullOrWhiteSpace(address1.Longitude) && !string.IsNullOrWhiteSpace(address2.Latitude) && !string.IsNullOrWhiteSpace(address2.Longitude))
            {
                return Math.Abs(Convert.ToDecimal(address1.Latitude) - Convert.ToDecimal(address2.Latitude)) <= ThresholdGeocodeMaxDiff &&
                    Math.Abs(Convert.ToDecimal(address1.Longitude) - Convert.ToDecimal(address2.Longitude)) <= ThresholdGeocodeMaxDiff;

                //return (Math.Ceiling(Convert.ToDecimal(address1.Latitude) * 100) / 100) == (Math.Ceiling(Convert.ToDecimal(address2.Latitude) * 100) / 100) &&
                //    (Math.Ceiling(Convert.ToDecimal(address1.Longitude) * 100) / 100) == (Math.Ceiling(Convert.ToDecimal(address2.Longitude) * 100) / 100);
            }
            return false;
        }

        public decimal GetStraightLineDistance(Address originationAddress, Address destinationAddress)
        {
            try
            {
                double origLat = 0D;
                double origLong = 0D;
                double destLat = 0D;
                double destLong = 0D;

                if ((originationAddress.Latitude != String.Empty) && (originationAddress.Longitude != String.Empty))
                {
                    origLat = Convert.ToDouble(originationAddress.Latitude);
                    origLong = Convert.ToDouble(originationAddress.Longitude);
                }
                else if (originationAddress.Zip != String.Empty)
                {
                    DataSet dsGeoCode = GetGeoCode(originationAddress.Zip);
                    if ((dsGeoCode != null) && (dsGeoCode.Tables.Count > 0) && (dsGeoCode.Tables[0].Rows.Count > 0))
                    {
                        origLat = Convert.ToDouble(dsGeoCode.Tables[0].Rows[0]["Latitude"]);
                        origLong = Convert.ToDouble(dsGeoCode.Tables[0].Rows[0]["Longitude"]);
                    }
                }

                if ((destinationAddress.Latitude != String.Empty) && (destinationAddress.Longitude != String.Empty))
                {
                    destLat = Convert.ToDouble(destinationAddress.Latitude);
                    destLong = Convert.ToDouble(destinationAddress.Longitude);
                }
                else if (destinationAddress.Zip != String.Empty)
                {
                    DataSet dsGeoCode = GetGeoCode(destinationAddress.Zip);
                    if ((dsGeoCode != null) && (dsGeoCode.Tables.Count > 0) && (dsGeoCode.Tables[0].Rows.Count > 0))
                    {
                        destLat = Convert.ToDouble(dsGeoCode.Tables[0].Rows[0]["Latitude"]);
                        destLong = Convert.ToDouble(dsGeoCode.Tables[0].Rows[0]["Longitude"]);
                    }
                }

                //Check if any of the geo codes are zero.
                if (origLat == 0 && origLong == 0)
                {
                    throw new Exception("The origination geo code is (0,0), distance cannot be calculated for the touch.");
                }
                else if (destLat == 0 && destLong == 0)
                {
                    throw new Exception("The destination geo code is (0,0), distance cannot be calculated for the touch.");
                }

                return Math.Round(GetDistanceFromGeoCodes(origLat, origLong, destLat, destLong), 2);
            }
            catch (Exception ex)
            {
                LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "google", "google api exception", ex.Message, ex.StackTrace, 0);
                throw ex;
            }
        }

        private DataSet GetGeoCode(string zip)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetGeocode(zip);
        }

        private decimal GetDistanceFromGeoCodes(double decLatitude1, double decLongitude1, double decLatitude2, double decLongitude2)
        {
            try
            {
                double dblRadius = 3981.875D;
                //6371D / (1.6) 
                double dblDiffLatitude = 0;
                double dblDiffLongitude = 0;
                double dblAngleFactor = 0;
                double declatitude1A = 0;
                double declatitude2A = 0;
                double dblCurvature = 0;
                decimal decDistance = 0;


                dblDiffLatitude = DegreesToRadians(decLatitude2 - decLatitude1);
                dblDiffLongitude = DegreesToRadians(decLongitude2 - decLongitude1);

                declatitude1A = DegreesToRadians(decLatitude1);
                declatitude2A = DegreesToRadians(decLatitude2);

                dblAngleFactor = Math.Sin(dblDiffLatitude / 2) * Math.Sin(dblDiffLatitude / 2) + Math.Cos(declatitude1A) * Math.Cos(declatitude2A) * Math.Sin(dblDiffLongitude / 2) * Math.Sin(dblDiffLongitude / 2);

                dblCurvature = 2 * Math.Atan2(Math.Sqrt(dblAngleFactor), Math.Sqrt(1 - dblAngleFactor));

                decDistance = (decimal)(dblRadius * dblCurvature);

                //decDistance = CType(((10 - decDistance * (0.01)) * (0.01) * decDistance) + decDistance, Decimal) 


                return decDistance;
            }
            catch (Exception ex)
            {
                LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "google", "google api exception", ex.Message, ex.StackTrace, 0);
                throw ex;
            }
        }

        private double DegreesToRadians(double dblDegrees)
        {
            return 2 * Math.PI * dblDegrees / 360.0;
        }

        /// <summary>
        /// GetLatitudeLongitude returns Lat/Long value for the address
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public GeoInfo GetLatitudeLongitude(Address address, int count = 0)
        {
            XmlDocument xmlDoc = new XmlDocument();
            LocalDataHandler dh = new LocalDataHandler();
            string url = string.Empty;
            DateTime startTime = DateTime.Now;
            DateTime endDateTime = DateTime.Now;
            string strAddress = string.Empty;
            bool googleCallexecutedForAuditLog = false;

            try
            {
                GeoInfo geoinfo = new GeoInfo();
                strAddress = RetAddressAsString(address);

                string urlRequest = string.Empty;
                if (strAddress != string.Empty)
                {
                    try
                    {
                        //Exit the block if limit exceeds 3 time
                        if (count > 3)
                        {
                            throw new Exception("OVER_QUERY_LIMIT: " + address.FullAddress);
                        }

                        urlRequest = String.Format("/maps/api/geocode/xml?address={0}&client={1}", HttpUtility.UrlEncode(strAddress), HttpUtility.UrlEncode(CLIENTID));
                        HMACSHA1 myhmacsha1 = new HMACSHA1();
                        string usablePrivateKey = GOOGLE_KEY.Replace("-", "+").Replace("_", "/");
                        myhmacsha1.Key = Convert.FromBase64String(usablePrivateKey);
                        var hash = myhmacsha1.ComputeHash(Encoding.ASCII.GetBytes(urlRequest));

                        url = String.Format("http://maps.googleapis.com{0}&signature={1}", urlRequest, HttpUtility.UrlEncode(Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_")));
                        googleCallexecutedForAuditLog = true;

                        startTime = DateTime.Now;
                        xmlDoc.Load(url);
                        endDateTime = DateTime.Now;

                        if (xmlDoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OK")
                        {
                            XmlNodeList lat = xmlDoc.GetElementsByTagName("lat");
                            geoinfo.Latitude = lat[0].InnerText.Trim();

                            XmlNodeList lng = xmlDoc.GetElementsByTagName("lng");
                            geoinfo.Longitude = lng[0].InnerText.Trim();
                        }
                        else if (xmlDoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OVER_QUERY_LIMIT")
                        {
                            Thread.Sleep(2000);
                            count++;
                            GetLatitudeLongitude(address, count);
                        }
                        else
                        {
                            geoinfo.Latitude = string.Empty;
                            geoinfo.Longitude = string.Empty;
                            geoinfo.Error = "Google API is unable to find lat/long value";
                        }
                    }
                    catch (Exception ex)
                    {
                        geoinfo.Latitude = string.Empty;
                        geoinfo.Longitude = string.Empty;
                        geoinfo.Error = ex.Message;
                    }
                }
                else
                {
                    geoinfo.Latitude = string.Empty;
                    geoinfo.Longitude = string.Empty;
                    geoinfo.Error = "No address provided";
                }
                return geoinfo;
            }
            catch (Exception ex)
            {
                //LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "google", "google api exception", ex.Message, (ex.StackTrace + $"; Address: {strAddress}; URL: {url}"), 0);
                throw ex;
            }
            finally
            {
                if (googleCallexecutedForAuditLog)
                {
                    string googleResp = ((xmlDoc != null && !string.IsNullOrEmpty(xmlDoc.InnerXml)) ? xmlDoc.InnerXml : "Error in google response");
                    string googleReq = string.IsNullOrWhiteSpace(url) ? $"Address: {strAddress}" : url;

                    GoogleMapAuditLog googleMapAuditLog = new GoogleMapAuditLog { Request = googleReq, Response = googleResp, StartTime = startTime, EndTime = endDateTime, MethodName = "GetlatitudeLongitude", ApplicationName = applicationName, ServerName = serverName };
                    dh.LogGAPIRequestResponse(googleMapAuditLog);
                }
            }
        }

        /// <summary>
        /// This method accepts google api resonse xmlDoc for maps/api/geocode/xml call 
        /// It extracts address from response and assing to given address object
        /// Here is the reference link for google response: https://developers.google.com/maps/documentation/geocoding/intro#Results
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="vAddress"></param>
        /// <returns></returns>
        private ValidatedAddress ParseGoogleGeocodeXMLResponse(XmlDocument xmlDoc, ValidatedAddress vAddress)
        {
            XmlNodeList address_components = xmlDoc.GetElementsByTagName("address_component");
            string route = string.Empty;
            string streetNumber = string.Empty;
            string subpremise = string.Empty;
            string neighborhood = string.Empty;
            string sublocality = string.Empty;
            string administrative_area_level_3 = string.Empty;

            foreach (XmlElement address_component in address_components)
            {
                XmlNodeList types = address_component.GetElementsByTagName("type");
                XmlNodeList short_names = address_component.GetElementsByTagName("short_name");
                XmlNodeList long_names = address_component.GetElementsByTagName("long_name");

                string shorname = (short_names != null && short_names.Count > 0 ? short_names[0].InnerText : string.Empty);
                string longname = (long_names != null && long_names.Count > 0 ? long_names[0].InnerText : string.Empty);

                if (types != null)
                {
                    foreach (XmlElement type in types)
                    {
                        bool foundReqElement = false;
                        switch (type.InnerText)
                        {
                            case "street_number":
                                streetNumber = shorname;
                                foundReqElement = true;
                                break;
                            case "subpremise":
                                subpremise = shorname;
                                foundReqElement = true;
                                break;
                            case "route":
                                route = shorname;
                                foundReqElement = true;
                                break;
                            case "street_address": //As per the document,  this indicates a precise street address. We can consider it as AddressLine 2. Please correct if you find any other field as address line 2.
                                vAddress.AddressLine2 = shorname;
                                foundReqElement = true;
                                break;
                            case "locality":
                                vAddress.City = longname;
                                foundReqElement = true;
                                break;
                            case "neighborhood":
                                neighborhood = longname;
                                foundReqElement = true;
                                break;
                            case "sublocality":
                                sublocality = longname;
                                foundReqElement = true;
                                break;
                            case "administrative_area_level_3":
                                administrative_area_level_3 = longname;
                                foundReqElement = true;
                                break;
                            case "administrative_area_level_1":
                                vAddress.State = shorname;
                                foundReqElement = true;
                                break;
                            case "postal_code":
                                vAddress.Zip = shorname;
                                foundReqElement = true;
                                break;
                            case "country":
                                vAddress.Country = shorname == "US" ? "USA" : shorname;
                                foundReqElement = true;
                                break;
                        }

                        if (foundReqElement)
                            break;
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(streetNumber) || !string.IsNullOrWhiteSpace(route))
                vAddress.AddressLine1 = ($"{streetNumber} {route} {subpremise}").Trim();

            ///Some addresses google api does not return city (ie.,locality) in that case look for neighborhood or sublocality or administrative_area_level_3 and treat it as city
            if (string.IsNullOrWhiteSpace(vAddress.City))
            {
                if (!string.IsNullOrWhiteSpace(neighborhood))
                    vAddress.City = neighborhood;
                else if (!string.IsNullOrWhiteSpace(sublocality))
                    vAddress.City = sublocality;
                else if (!string.IsNullOrWhiteSpace(administrative_area_level_3))
                    vAddress.City = administrative_area_level_3;
            }

            return vAddress;
        }

        /// <summary>
        /// ValidateAddress returns validated address 
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public ValidatedAddress ValidateAddress(Address address)
        {
            return ValidateAddress(address, false);
        }
        public ValidatedAddress ValidateAddress(Address address, bool isRecursiveCall)
        {
            string strAddress = string.Empty;
            bool googleCallexecutedForAuditLog = false;

            try
            {
                ValidatedAddress vAddress = new ValidatedAddress();
                string urlRequest = string.Empty;
                strAddress = RetAddressAsString(address);
                if (strAddress != string.Empty)
                {
                    var url = string.Empty;
                    DateTime startTime = DateTime.Now;
                    DateTime endTime = DateTime.Now;
                    XmlDocument xmlDoc = new XmlDocument();

                    try
                    {
                        if ((!string.IsNullOrWhiteSpace(address.AddressLine1) && !string.IsNullOrWhiteSpace(address.Zip)) ||
                            (!string.IsNullOrWhiteSpace(address.City) && !string.IsNullOrWhiteSpace(address.State) && !string.IsNullOrWhiteSpace(address.Zip)))
                        {
                            urlRequest = $"/maps/api/geocode/xml?address={HttpUtility.UrlEncode(strAddress)}&client={HttpUtility.UrlEncode(CLIENTID)}";
                        }
                        else
                        {
                            countryCode = address.Zip.Trim().Length > 5 ? "CA" : "US";
                            string strComponents = string.Format("country:{0}|postal_code:{1}", countryCode, address.Zip);
                            urlRequest = String.Format("/maps/api/geocode/xml?components={0}&client={1}", HttpUtility.UrlEncode(strComponents), HttpUtility.UrlEncode(CLIENTID));
                        }

                        //urlRequest = $"/maps/api/geocode/xml?address={HttpUtility.UrlEncode(strAddress)}&client={HttpUtility.UrlEncode(CLIENTID)}";

                        HMACSHA1 myhmacsha1 = new HMACSHA1();
                        string usablePrivateKey = GOOGLE_KEY.Replace("-", "+").Replace("_", "/");
                        myhmacsha1.Key = Convert.FromBase64String(usablePrivateKey);
                        var hash = myhmacsha1.ComputeHash(Encoding.ASCII.GetBytes(urlRequest));

                        url = $"http://maps.googleapis.com{urlRequest}&signature={HttpUtility.UrlEncode(Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_"))}";
                        googleCallexecutedForAuditLog = true;

                        startTime = DateTime.Now;
                        xmlDoc.Load(url);
                        endTime = DateTime.Now;

                        if (xmlDoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OK")
                        {
                            try
                            {
                                ParseGoogleGeocodeXMLResponse(xmlDoc, vAddress);

                                //"OK" indicates that no errors occurred; the address was successfully parsed and at least one geocode was returned.
                                //XmlNodeList lat = xmlDoc.GetElementsByTagName("lat");
                                //vAddress.Latitude = lat[0].InnerText.Trim();

                                //XmlNodeList lng = xmlDoc.GetElementsByTagName("lng");
                                //vAddress.Longitude = lng[0].InnerText.Trim();

                                #region code commented, for reference
                                //XmlNodeList formattedAddress = xmlDoc.GetElementsByTagName("formatted_address");
                                //string[] fAddress = formattedAddress[0].InnerText.Trim().Split(',');
                                //int aSize = fAddress.Length - 1;

                                //vAddress.Country = fAddress[aSize].Trim();

                                //if (fAddress.Length > 1)
                                //{
                                //    string[] StateZip = fAddress[aSize - 1].Trim().Split(' ');
                                //    if (StateZip.Length == 2)
                                //    {
                                //        vAddress.Zip = StateZip[1].Trim();
                                //        vAddress.State = StateZip[0].Trim();
                                //    }
                                //    vAddress.City = fAddress[aSize - 2].Trim();


                                //    for (int count = 1; count <= aSize - 3; count++)
                                //    {
                                //        vAddress.AddressLine2 = vAddress.AddressLine2 + fAddress[count].ToString();
                                //    }
                                //    vAddress.AddressLine2 = vAddress.AddressLine2.Trim();
                                //    vAddress.AddressLine1 = fAddress[0].Trim();
                                //} 
                                #endregion

                                XmlNodeList LocationType = xmlDoc.GetElementsByTagName("location_type");
                                string LocationTypeStatus = string.Empty;
                                if (LocationType[0] != null)
                                    LocationTypeStatus = LocationType[0].InnerText.Trim();
                                else
                                    LocationTypeStatus = string.Empty;

                                if (LocationTypeStatus != "ROOFTOP" && LocationTypeStatus != "RANGE_INTERPOLATED")
                                {
                                    vAddress.IsAddressValid = false;
                                    vAddress.Error = "No matching address found.";
                                }
                                else
                                {
                                    if (address.Zip != vAddress.Zip)
                                    {
                                        vAddress.IsAddressValid = false;
                                        vAddress.Error = "Found matching address " + vAddress.AddressLine1 + "," + vAddress.AddressLine2 + "," + vAddress.City + "," + vAddress.State + "," + vAddress.Zip + " with a different zip code. Please verify the new zip code with the customer.";
                                    }
                                    else
                                    {
                                        vAddress.IsAddressValid = true;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Google API is not able to validate the address. Please try again with a valid address!");
                            }
                        }
                        else
                        {
                            vAddress.AddressLine1 = address.AddressLine1;
                            vAddress.AddressLine2 = address.AddressLine2;
                            vAddress.City = address.City;
                            vAddress.Country = address.Country;
                            vAddress.Zip = address.Zip;
                            vAddress.State = address.State;
                            vAddress.Country = address.Country;
                            vAddress.IsAddressValid = false;
                            vAddress.Error = "Google API is not able to validate the address.";
                        }
                        /*
                        ///If google does not return City State then check with USPS api 
                        if (isRecursiveCall == false && vAddress.IsAddressValid == false && !string.IsNullOrWhiteSpace(vAddress.Zip) && (string.IsNullOrWhiteSpace(vAddress.State) || string.IsNullOrWhiteSpace(vAddress.City)))
                        {
                            var apiAddress = (new PRAddressAPI()).UsAddressLookUpByZipcode(address.Zip);
                            if (apiAddress != null && !string.IsNullOrWhiteSpace(apiAddress.City) && !string.IsNullOrWhiteSpace(apiAddress.State))
                            {
                                address.City = apiAddress.City;
                                address.State = apiAddress.State;
                                vAddress = ValidateAddress(address, true);
                            }
                        }

                        //AFter all, still if we dont have city or state in vAddress object then try to get it from postal codes API
                        if (!string.IsNullOrWhiteSpace(vAddress.Zip) && (string.IsNullOrWhiteSpace(vAddress.State) || string.IsNullOrWhiteSpace(vAddress.City)))
                        {
                            var apiAddress = (new PRAddressAPI()).UsAddressLookUpByZipcode(address.Zip);
                            if (apiAddress != null && !string.IsNullOrWhiteSpace(apiAddress.City) && !string.IsNullOrWhiteSpace(apiAddress.State))
                            {
                                vAddress.City = string.IsNullOrWhiteSpace(vAddress.City) ? apiAddress.City : vAddress.City;
                                vAddress.State = string.IsNullOrWhiteSpace(vAddress.State) ? apiAddress.State : vAddress.State;
                            }
                        }*/
                        if (!string.IsNullOrWhiteSpace(vAddress.Zip) && (string.IsNullOrWhiteSpace(vAddress.State) || string.IsNullOrWhiteSpace(vAddress.City)))
                        {
                            var apiAddress = (new PRAddressAPI()).UsAddressLookUpByZipcode(address.Zip);
                            if (apiAddress != null && !string.IsNullOrWhiteSpace(apiAddress.City) && !string.IsNullOrWhiteSpace(apiAddress.State))
                            {
                                address.City = apiAddress.City;
                                address.State = apiAddress.State;

                                vAddress.City = string.IsNullOrWhiteSpace(vAddress.City) ? apiAddress.City : vAddress.City;
                                vAddress.State = string.IsNullOrWhiteSpace(vAddress.State) ? apiAddress.State : vAddress.State;

                                if (isRecursiveCall == false && vAddress.IsAddressValid == false)
                                {
                                    vAddress = ValidateAddress(address, true);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        vAddress.AddressLine1 = address.AddressLine1;
                        vAddress.AddressLine2 = address.AddressLine2;
                        vAddress.City = address.City;
                        vAddress.Country = address.Country;
                        vAddress.Zip = address.Zip;
                        vAddress.State = address.State;
                        vAddress.Country = address.Country;
                        vAddress.IsAddressValid = false;
                        vAddress.Error = ex.Message;
                    }
                    finally
                    {
                        if (googleCallexecutedForAuditLog)
                        {
                            LocalDataHandler dh = new LocalDataHandler();
                            string googleResp = ((xmlDoc != null && !string.IsNullOrEmpty(xmlDoc.InnerXml)) ? xmlDoc.InnerXml : "Error in google response");
                            string googleReq = string.IsNullOrWhiteSpace(url) ? $"Address: {strAddress}" : url;
                            GoogleMapAuditLog googleMapAuditLog = new GoogleMapAuditLog { Request = googleReq, Response = googleResp, StartTime = startTime, EndTime = endTime, MethodName = "ValidateAddress", ApplicationName = applicationName, ServerName = serverName };
                            dh.LogGAPIRequestResponse(googleMapAuditLog);
                        }
                    }
                }
                else
                {
                    vAddress.AddressLine1 = address.AddressLine1;
                    vAddress.AddressLine2 = address.AddressLine2;
                    vAddress.City = address.City;
                    vAddress.Country = address.Country;
                    vAddress.Zip = address.Zip;
                    vAddress.State = address.State;
                    vAddress.Country = address.Country;
                    vAddress.IsAddressValid = false;
                    vAddress.Error = "No address provided";
                }
                return vAddress;
            }
            catch (Exception ex)
            {
                LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "google", "google api exception", ex.Message, (ex.StackTrace + $"; Address: {strAddress};"), 0);
                throw ex;
            }
        }

        /// <summary>
        /// RetAddressAsString returns address in a single line for Google API
        /// </summary>
        /// <param name="oAddress"></param>
        /// <returns></returns>
        private string RetAddressAsString(Address oAddress)
        {
            string retAddress = string.Empty;
            retAddress = retAddress + oAddress.AddressLine1;
            retAddress = retAddress + (string.IsNullOrEmpty(oAddress.AddressLine1) ? "" : ", ") + oAddress.AddressLine2;
            retAddress = retAddress + (string.IsNullOrEmpty(oAddress.AddressLine2) ? "" : ", ") + oAddress.City;
            retAddress = retAddress.Trim() + (string.IsNullOrEmpty(oAddress.City) ? "" : ", ") + oAddress.State;
            retAddress = retAddress + (string.IsNullOrEmpty(oAddress.State) ? "" : ", ") + oAddress.Zip;
            retAddress = retAddress + (string.IsNullOrEmpty(oAddress.Zip) ? "" : ", ") + (oAddress.Country == "USA" ? "US" : oAddress.Country);
            return retAddress.Trim();
        }

        /// <summary>
        /// Returns Post office address for the zip code
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private Address RetPostOfficeAddress(Address address)
        {
            string addressString = string.Empty;
            LocalDataHandler dh = new LocalDataHandler();
            XmlDocument xmlDoc = new XmlDocument();
            string url = string.Empty;
            DateTime startTime = DateTime.Now;
            DateTime endDateTime = DateTime.Now;
            string urlRequest = string.Empty;

            try
            {
                Address vAddress = new Address();
                address.AddressLine1 = "US POST Office";
                addressString = RetAddressAsString(address);

                //var url = String.Format("https://maps.googleapis.com/maps/api/place/textsearch/xml?query={0},&types=post_office&key=AIzaSyA67JIj41Ze0lbc2KidOgQMgqLOAZOcybE", addressString);
                url = String.Format("https://maps.googleapis.com/maps/api/place/textsearch/xml?query={0},&types=post_office&key={1}", addressString, US_PostOffice_API_Key);

                xmlDoc = new XmlDocument();
                startTime = DateTime.Now;
                xmlDoc.Load(url);
                endDateTime = DateTime.Now;

                if (xmlDoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OK")
                {
                    XmlNodeList formattedAddress = xmlDoc.GetElementsByTagName("formatted_address");

                    string[] fAddress = formattedAddress[0].InnerText.Trim().Split(',');
                    int aSize = fAddress.Length - 1;
                    vAddress.Country = fAddress[aSize].Trim();
                    string[] StateZip = fAddress[aSize - 1].Trim().Split(' ');
                    vAddress.Zip = StateZip[1].Trim();
                    vAddress.State = StateZip[0].Trim();
                    vAddress.City = fAddress[aSize - 2].Trim();
                    vAddress.Latitude = xmlDoc.GetElementsByTagName("lat")[0].InnerText;
                    vAddress.Longitude = xmlDoc.GetElementsByTagName("lng")[0].InnerText;
                    for (int count = 1; count <= aSize - 3; count++)
                    {
                        vAddress.AddressLine2 = vAddress.AddressLine2 + fAddress[count].ToString();
                    }
                    vAddress.AddressLine2 = vAddress.AddressLine2.Trim();
                    vAddress.AddressLine1 = fAddress[0].Trim();
                }

                return vAddress;
            }
            catch (Exception ex)
            {
                //LocalDataHandler dh = new LocalDataHandler();
                dh.InsertPRErrorLog(DateTime.Now, "google", "google api exception", ex.Message, (ex.StackTrace + $"; Address: {addressString};"), 0);
                throw ex;
            }
            finally
            {
                string googleResp = ((xmlDoc != null && !string.IsNullOrEmpty(xmlDoc.InnerXml)) ? xmlDoc.InnerXml : "Error in google response");
                string googleReq = string.IsNullOrWhiteSpace(url) ? $"Address: {addressString}" : url;
                GoogleMapAuditLog googleMapAuditLog = new GoogleMapAuditLog { Request = googleReq, Response = googleResp, StartTime = startTime, EndTime = endDateTime, MethodName = "RetPostOfficeAddress", ApplicationName = applicationName, ServerName = serverName };
                dh.LogGAPIRequestResponse(googleMapAuditLog);
            }
        }



        /// <summary>
        /// Return distance between 2 point using Latitude/Longitude from google API 
        /// </summary>
        /// <param name="originLatitude">origin Latitude</param>
        /// <param name="originLongitude">Origin Longitude</param>
        /// <param name="destinationLatitude">destination Latitude</param>
        /// <param name="destinationLongitude">destination Longitude</param>
        /// <returns>distance between 2 points</returns>
        public decimal GetDistanceByLatitudeLongitude(double originLatitude, double originLongitude, double destinationLatitude, double destinationLongitude)
        {
            decimal distance = 0m;
            LocalDataHandler dh = new LocalDataHandler();
            XmlDocument xmlDoc = new XmlDocument();
            string url = string.Empty;
            DateTime startTime = DateTime.Now;
            DateTime endDateTime = DateTime.Now;
            string urlRequest = string.Empty;
            
            urlRequest = String.Format("/maps/api/distancematrix/xml?origins={0},{1}", originLatitude, originLongitude);
            
            urlRequest = String.Format("{0}&destinations={1},{2}", urlRequest, destinationLatitude, destinationLongitude);
            
            //Call Google distancematrix service
            try
            {
                urlRequest = String.Format("{0}&mode=driving&units=imperial&language=en-EN&client={1}", urlRequest, HttpUtility.UrlEncode(CLIENTID));
                HMACSHA1 myhmacsha1 = new HMACSHA1();
                string usablePrivateKey = GOOGLE_KEY.Replace("-", "+").Replace("_", "/");
                myhmacsha1.Key = Convert.FromBase64String(usablePrivateKey);
                var hash = myhmacsha1.ComputeHash(Encoding.ASCII.GetBytes(urlRequest));

                url = String.Format("http://maps.googleapis.com{0}&signature={1}", urlRequest, HttpUtility.UrlEncode(Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_")));

                xmlDoc = new XmlDocument();
                startTime = DateTime.Now;
                xmlDoc.Load(url);
                endDateTime = DateTime.Now;

                if (xmlDoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OK")
                {
                    XmlNodeList distanceXml = xmlDoc.GetElementsByTagName("distance");

                    if (distanceXml[0] != null) // if google is able to calculate distance
                    {
                        distance = distanceXml[0].ChildNodes[1].InnerText.Trim().IndexOf("ft") != -1 ? 0 : Convert.ToDecimal(distanceXml[0].ChildNodes[1].InnerText.Replace(" mi", ""));

                        return distance;
                    }
                }

                throw new Exception("Google API is unable to get distance");
            }
            catch (Exception ex)
            {
                dh.InsertPRErrorLog(DateTime.Now, "google", "google api exception", ex.Message, ex.StackTrace, 0);
                throw ex;
            }
            finally
            {
                string googleResp = ((xmlDoc != null && !string.IsNullOrEmpty(xmlDoc.InnerXml)) ? xmlDoc.InnerXml : "Error in google response");
                string googleReq = string.IsNullOrWhiteSpace(url) ? $"Lat Long values: {originLatitude}, {originLongitude}; {destinationLatitude}, {destinationLongitude}" : url;
                GoogleMapAuditLog googleMapAuditLog = new GoogleMapAuditLog { Request = googleReq, Response = googleResp, StartTime = startTime, EndTime = endDateTime, MethodName = "GetDistanceByLatitudeLongitude", ApplicationName = applicationName, ServerName = serverName };
                dh.LogGAPIRequestResponse(googleMapAuditLog);
            }
        }
    }
}
